<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('company_info')){
    
    function institution_info(){
        //$this->load->database('main',TRUE);
        $CI = & get_instance();
        return $CI->db->query("SELECT i.id,i.name,i.website,i.phone,i.city,i.postal,i.logoname,i.printer,ic.name as categ FROM institution as i INNER JOIN institution_categories as ic ON i.category=ic.id")->row();
       
    }
}
?>
