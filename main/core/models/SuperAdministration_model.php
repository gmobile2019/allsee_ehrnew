<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class SuperAdministration_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);//load main database configuration
            $this->store_salt      = $this->config->item('store_salt', 'ion_auth');
            $this->salt_length     = $this->config->item('salt_length', 'ion_auth');
    }
    
    function current_user_info(){

           return $this->main->query("SELECT users.first_name,users.middle_name,users.last_name,users.msisdn,users.email,users.photo "
                   . "FROM users "
                   . "WHERE users.id='".$this->session->userdata('user_id')."'")->row();
    }
    
    function profile_data(){
        
        return $this->main->query("SELECT users.id,users.password,users.salt,users.first_name,users.middle_name,"
                . "users.last_name,users.username,users.email,"
                . "users.msisdn,users.photo,users_groups.group_id "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id "
                . "WHERE users.id='".$this->session->userdata('user_id')."'")->row();
    }
    
    function member_info_count($name,$group){
        
        if($name <> null){
           $where .=" AND (users.first_name LIKE '%$name%' OR users.middle_name LIKE '%$name%' OR users.last_name LIKE '%$name%')"; 
        }
        
        if($group <> null){
            
            $where .=" AND users_groups.group_id='$group'"; 
        }
        
        return count($this->main->query("SELECT users.id,users.first_name,"
                . "users.middle_name,users.last_name,users.email,"
                . "users.msisdn,users.username,users_groups.group_id "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id $where "
                . "ORDER BY users.first_name ASC")->result());
    }
    
    function member_info($name,$group,$page,$limit){
        
        if($name <> null){
           $where .=" AND (users.first_name LIKE '%$name%' OR users.middle_name LIKE '%$name%' OR users.last_name LIKE '%$name%')"; 
        }
        
        if($group <> null){
            
            $where .=" AND users_groups.group_id='$group'"; 
        }
        
        return $this->main->query("SELECT users.id,users.first_name,"
                . "users.middle_name,users.last_name,users.email,"
                . "users.msisdn,users.username,users_groups.group_id,users.active "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id $where "
                . "ORDER BY users.first_name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function groups($id){
        
        if($id <> null){
            $where=" WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,name,description FROM groups $where")->result();
    }
    
    function registration_groups($id){
        
        if($id <> null){
            $where=" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,name,description FROM groups WHERE id !=1 $where")->result();
    }
    
    function get_registration_groups($group_id){
        
        if($group_id == 1){
            $where =" WHERE id IN ('1','2')";
        }
        
        if($group_id == 2){
            
            $where =" WHERE id NOT IN ('1','2')";
        }
        
        return $this->main->query("SELECT id,name FROM groups $where ORDER BY name ASC")->result();
    }
    
    function get_member_info($id){
        
        if($id <> null){
            
            $where =" WHERE users.id='$id'";
            
        }
       
        return $this->main->query("SELECT users.id,users.first_name,"
                . "users.middle_name,users.last_name,users.email,"
                . "users.msisdn,users.username,users_groups.group_id,users.active "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id $where "
                . "ORDER BY users.first_name ASC ")->result();
       
    }
    
    function system_user_registration($user,$user_group,$id,$password,$addon,$nurse_incharge){
        
       if($password <> null){
           $salt= $this->store_salt ? $this->salt() : FALSE;
       
            

            $password= $this->hash_password($password, $salt);
            
            $user['password']=$password;
            $user['salt']=$salt;
       }
            
      if($id == null){
          
          $user['createdon']=date('Y-m-d H:i:s');
          $user['createdby']=$this->session->userdata('user_id');
          $usr=$this->main->insert('users',$user);
          
          if($user){
             
              $id=$this->main->insert_id();
              $user_group['user_id']=$id;
              
              if($addon <> null){
                  
                  $this->save_addon($addon,$id,$user_group['group_id']);
              }
              
              if($user_group['group_id'] == 3){
                  
                    $this->main->update('nurses_incharge',
                                      array('status'=>0,'modifiedby'=>$this->session->userdata('user_id'),'modifiedon'=>date('Y-m-d H:i:s'),
                                      array('userid'=>$id)));
                }
        
              if($nurse_incharge){
                  
                  $array=array(
                      'userid'=>$id,
                      'createdby'=>$this->session->userdata('user_id'),
                  );
                  
                  $this->main->insert('nurses_incharge',$array);
              }
              
              return $this->main->insert('users_groups',$user_group);
          }
          return FALSE;
      } 
              $user['modifiedon']=date('Y-m-d H:i:s');
              $user['modifiedby']=$this->session->userdata('user_id');
              
              if($addon <> null){
                  
                  $this->save_addon($addon,$id,$user_group['group_id']);
              }
              
              if($user_group['group_id'] == 3){
                  
                    $this->main->update('nurses_incharge',
                                      array('status'=>0,'modifiedby'=>$this->session->userdata('user_id'),'modifiedon'=>date('Y-m-d H:i:s'),
                                      array('userid'=>$id)));
                }
              
              if($nurse_incharge){
                  
                  $array=array(
                      'userid'=>$id,
                      'createdby'=>$this->session->userdata('user_id'),
                  );
                  
                  
                  $this->main->insert('nurses_incharge',$array);
              }
              
              $this->main->update('users',$user,array('id'=>$id));
       return $this->main->update('users_groups',$user_group,array('user_id'=>$id));
    }
    
    function save_addon($data,$id,$group){
        
        $this->main->update('inventory_unit_users',array('status'=>0,'lastupdate'=>date('Y-m-d H:i:s'),'lastupdater'=>$this->session->userdata('user_id')),array('userid'=>$id));//deactivate user if in an inventory store
        $this->main->update('examiner_users',array('status'=>0,'lastupdate'=>date('Y-m-d H:i:s'),'lastupdater'=>$this->session->userdata('user_id')),array('userid'=>$id));//deactivate user if in has another investigation category
        
        if($group == 4){
            $check=$this->main->get_where('users_doctors',array('userid'=>$id))->row();
            
            if($check <> null){
                return $this->main->update('users_doctors',$data,array('userid'=>$id));
            }
                   $data['userid']=$id;
            return $this->main->insert('users_doctors',$data);
        }
        
        if($group == 9){
            $check=$this->main->get_where('examiner_users',array('userid'=>$id))->row();
            
            if($check <> null){
                return $this->main->update('examiner_users',$data,array('userid'=>$id));
            }
                   $data['userid']=$id;
            return $this->main->insert('examiner_users',$data);
        }
        
        if(($group == 3 || $group == 5) && $data['inventoryunit'] <> null){
            
            $data['userid']=$id;
            return $this->main->insert('inventory_unit_users',$data);
        }
    }
    
    function change_password($password){
        
        $salt=$this->profile_data();
        $password=$this->hash_password($password,$salt->salt);
        
        return $this->main->update('users',array('password'=>$password),array('id'=>$this->session->userdata('user_id')));
    }
    
    public function salt()
    {
            return substr(sha1(uniqid(rand(), true)), 0, $this->salt_length);
    }
    
    function hash_password($password, $salt=false)
    {
        
           if ($this->store_salt && $salt)
            {
               return  sha1($password . $salt);
            }
            else
            {
                
                    $salt = $this->salt();
                    return  $salt.substr(sha1($salt.$password), 0, -$this->salt_length);
            }
    }
    
    function hash_db_password($password,$hash_password_db){
       
        if ($this->store_salt)
		{
			return sha1($password .$hash_password_db->salt);
		}
		else
		{
			$salt = substr($hash_password_db->password,0,$this->salt_length);
                return  $salt.substr(sha1($salt.$password), 0, -$this->salt_length);
		}
    }
        
    function check_username($username,$id){
        
        if($id <> null){
            
            $usernames=$this->main->query("SELECT COUNT(id) username_count FROM users WHERE username LIKE '$username' AND id<>'$id'")->row();
        }else{
            
            $usernames=$this->main->query("SELECT COUNT(id) username_count FROM users WHERE username LIKE '$username'")->row();
        }
        
        if($usernames->username_count > 0){
            
            return FALSE;
        }
        
        return TRUE;
        
    }
    
    function activate_deactivate_users($id,$status){
        $new_status=$status == 1?0:1;
        
        return $this->main->update('users',array('active'=>$new_status),array('id'=>$id));
    }
    
    function institution_categories($id){
        
        if($id <> null){
            $where=" WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,name FROM institution_categories $where")->result();
    }
    
    function save_institution_profile($data){
        
        return $this->main->update('institution',$data);
        return $this->main->insert('institution',$data);
    }
    
    function institution_information(){
        
        return $this->main->query("SELECT id,name,institution_id,"
                . "category,email,phone,website,postal,city,logoname,status "
                . "FROM institution")->row();
    }
    
    function activate_deactivate_institution($status){
        
        return $this->main->update('institution',array('status'=>$status));
    }
    
    function save_cover_photos($data,$id){
        
        if($id <> null){
            
            return $this->main->update('institution_cover_images',$data,array('id'=>$id));
        }
        
               $this->main->query("DELETE FROM institution_cover_images");
        return $this->main->insert_batch('institution_cover_images',$data);
    }
    
    function institution_cover_images($status){
        
        if($status <> null){
            
           $where .=" AND status='$status'"; 
        }
        
        return $this->main->query("SELECT id,imagename,imagestring,"
                . "sequence,status,lastupdate "
                . "FROM institution_cover_images "
                . "WHERE id is not null $where "
                . "ORDER BY sequence ASC")->result();
    }
    
    function activate_deactivate_image($id,$status){
        $new_status=$status == 1?0:1;
        
        return $this->main->update('institution_cover_images',array('status'=>$new_status),array('id'=>$id));
    }
    
    function save_service_charge($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('service_charges',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('service_charges',$data);
    }
    
    function activate_deactivate_service_charge($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('service_charges',array('status'=>$status),array('id'=>$id));
    }
    
    function service_code_check($code,$id){
        
        if($id <> null){
            return $this->main->query("SELECT id FROM service_charges WHERE service_code='$code' AND id!='$id'")->row();
        }
        
        return $this->main->query("SELECT id FROM service_charges WHERE service_code='$code'")->row();
    }
    
    function service_charge($id){
        
        if($id <> null){
            $where .=" WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,name,description,service_code,status FROM service_charges $where ORDER BY name ASC")->result();
    }
    
    function get_service_charge_by_code($code){
       
        return $this->main->query("SELECT id,name,description,service_code,status FROM service_charges WHERE service_code='$code' ORDER BY name ASC")->result();
    }
    
    function service_charge_info_count(){
        
        
        return count($this->main->query("SELECT id,name,description,"
                . "service_code,status "
                . "FROM service_charges "
                . "$where "
                . "ORDER BY name ASC")->result());
    }
    
    function service_charge_info($page,$limit){
        
        
        return $this->main->query("SELECT id,name,description,"
                . "service_code,status "
                . "FROM service_charges "
                . "$where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function save_sponsor($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('sponsors',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('sponsors',$data);
    }
    
    function activate_deactivate_sponsor($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('sponsors',array('status'=>$status),array('id'=>$id));
    }
    
    function sponsor_code_check($code,$id){
        
        if($id <> null){
            return $this->main->query("SELECT id FROM sponsors WHERE sponsorcode='$code' AND id!='$id'")->row();
        }
        
        return $this->main->query("SELECT id FROM sponsors WHERE sponsorcode='$code'")->row();
    }
    
    function sponsor_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (shortname LIKE '%$name%' OR fullname LIKE '%$name%') "; 
        }
        
        
        
        return count($this->main->query("SELECT id,shortname,"
                . "fullname,sponsorcode,description,status "
                . "FROM sponsors WHERE id !=0 $where "
                . "ORDER BY fullname ASC")->result());
    }
    
    function sponsor_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (shortname LIKE '%$name%' OR fullname LIKE '%$name%') "; 
        }
        
        
        return $this->main->query("SELECT id,shortname,"
                . "fullname,sponsorcode,description,status "
                . "FROM sponsors WHERE id !=0 $where "
                . "ORDER BY fullname ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function sponsors($id){
        
        if($id <> null){
            $where =" WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,shortname,"
                                . "fullname,sponsorcode,description,status "
                                . "FROM sponsors $where "
                                . "ORDER BY fullname ASC")->result();
    }
    
    function get_sponsor_by_code($code){
        
        return $this->main->query("SELECT id,shortname,"
                                . "fullname,sponsorcode,description,status "
                                . "FROM sponsors WHERE sponsorcode='$code' "
                                . "ORDER BY fullname ASC")->result();
    }
    
    function active_sponsors($id){
      if($id <> null){
            $where =" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,shortname,"
                                . "fullname,sponsorcode,description,status "
                                . "FROM sponsors WHERE status='1' $where "
                                . "ORDER BY fullname ASC")->result();  
    }
    
    function save_paymentMode($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('payment_modes',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('payment_modes',$data);
    }
    
    function activate_deactivate_paymentMode($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('payment_modes',array('status'=>$status),array('id'=>$id));
    }
    
    function paymentMode_code_check($code,$id){
        
        if($id <> null){
            return $this->main->query("SELECT id FROM payment_modes WHERE paymentmodecode='$code' AND id!='$id'")->row();
        }
        
        return $this->main->query("SELECT id FROM payment_modes WHERE paymentmodecode='$code'")->row();
    }
    
    function paymentMode_info_count($name,$sponsor){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND sponsor_id='$sponsor' "; 
        }
        
        return count($this->main->query("SELECT id,name,"
                . "paymentmodecode,sponsor_id,description,status "
                . "FROM payment_modes WHERE id !=0 $where "
                . "ORDER BY name ASC")->result());
    }
    
    function paymentMode_info($name,$sponsor,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND sponsor_id='$sponsor' "; 
        }
        
        return $this->main->query("SELECT id,name,"
                . "paymentmodecode,sponsor_id,description,status "
                . "FROM payment_modes WHERE id !=0 $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function paymentModes($id){
        
        if($id <> null){
            $where =" WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                                . "paymentmodecode,sponsor_id,description,status "
                                . "FROM payment_modes $where "
                                . "ORDER BY name ASC")->result();
    }
    
    function active_paymentModes($id){
        
        if($id <> null){
            $where =" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                                . "paymentmodecode,sponsor_id,description,status "
                                . "FROM payment_modes WHERE status='1' $where "
                                . "ORDER BY name ASC")->result();
    }
    
    function get_paymentMode_by_code($code){
        
        return $this->main->query("SELECT id,name,"
                                . "paymentmodecode,sponsor_id,description,status "
                                . "FROM payment_modes WHERE paymentmodecode='$code' "
                                . "ORDER BY name ASC")->row();
    }
    
    function get_paymentModes_by_sponsor($sponsor){
       
        return $this->main->query("SELECT id,name,"
                                . "paymentmodecode,sponsor_id,description,status "
                                . "FROM payment_modes WHERE sponsor_id='$sponsor' AND status='1' "
                                . "ORDER BY name ASC")->result();
    }
    
    function printers($id){
        if($id <> null){
            $where =" WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,model "
                                . "FROM printers $where "
                                . "ORDER BY id ASC")->result();
    }
    
    function institution_configs(){
        
        return $this->main->query("SELECT * FROM institution_config")->row();
    }
    
    function save_institution_config($data){
        
        return $this->main->update('institution_config',$data);
    }
    
    function item_categories_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        return count($this->main->query("SELECT id,name,"
                . "code,description,status "
                . "FROM inventory_item_categories WHERE id !=0 $where "
                . "ORDER BY name ASC")->result());
    }
    
    function item_categories_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        return $this->main->query("SELECT id,name,"
                . "code,description,status "
                . "FROM inventory_item_categories WHERE id !=0 $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function item_categories($id){
        
        if($id <> null){
            $where =" WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                                . "code,description,status "
                                . "FROM inventory_item_categories $where "
                                . "ORDER BY name ASC")->result();
    }
    
    function save_item_category($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('inventory_item_categories',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('inventory_item_categories',$data);
    }
    
    function activate_deactivate_item_category($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('inventory_item_categories',array('status'=>$status),array('id'=>$id));
    }
    
    function item_category_code_check($code,$id){
        
        if($id <> null){
            return $this->main->query("SELECT id FROM inventory_item_categories WHERE code='$code' AND id!='$id'")->row();
        }
        
        return $this->main->query("SELECT id FROM inventory_item_categories WHERE code='$code'")->row();
    }
    
    function unit_categories_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        return count($this->main->query("SELECT id,name,"
                . "code,description,status "
                . "FROM inventory_unit_categories WHERE id !=0 $where "
                . "ORDER BY name ASC")->result());
    }
    
    function unit_categories_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        return $this->main->query("SELECT id,name,"
                . "code,description,status "
                . "FROM inventory_unit_categories WHERE id !=0 $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function unit_categories($id){
        
        if($id <> null){
            $where =" WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                                . "code,description,status "
                                . "FROM inventory_unit_categories $where "
                                . "ORDER BY name ASC")->result();
    }
    
    function save_unit_category($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('inventory_unit_categories',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('inventory_unit_categories',$data);
    }
    
    function activate_deactivate_unit_category($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('inventory_unit_categories',array('status'=>$status),array('id'=>$id));
    }
    
    function unit_category_code_check($code,$id){
        
        if($id <> null){
            return $this->main->query("SELECT id FROM inventory_unit_categories WHERE code='$code' AND id!='$id'")->row();
        }
        
        return $this->main->query("SELECT id FROM inventory_unit_categories WHERE code='$code'")->row();
    }
    
    function nurses_incharge($userid,$status){
        
        if($userid <> null){
            
            $where .=" AND ni.userid='$userid'";
        }
        
        if($status <> null){
            
            if($status == 2){
                
                $where .=" AND ni.status='0'";
            }else{
                
                $where .=" AND ni.status='$status'";
            }
        }
        
        return $this->main->query("SELECT ni.id,ni.userid,ni.status,ug.group_id FROM nurses_incharge as ni INNER JOIN users_groups as ug ON ni.userid=ug.user_id WHERE ug.group_id='3' $where")->result();
    }
    
    function investigation_categories_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        return count($this->main->query("SELECT id,name,"
                                . "code,status,description "
                                . "FROM investigation_categories "
                                . "WHERE id is not null $where")->result());
    }
    
    function investigation_categories_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        return $this->main->query("SELECT id,name,"
                                . "code,status,description "
                                . "FROM investigation_categories "
                                . "WHERE id is not null $where"
                                . "ORDER BY name ASC "
                                . "LIMIT $page,$limit")->result();
    } 
    
    function investigation_categories($id,$code,$status){
        
        if($id <> null){
            $where =" AND id='$id'";
        }
        
        if($code <> null){
            
            $where =" AND code='$code'";
        }
        
        if($status <> null){
            
            $where =" AND status='$status'";
        }
        
        return $this->main->query("SELECT id,name,"
                                . "code,status,description "
                                . "FROM investigation_categories "
                                . "WHERE id is not null $where "
                                . "ORDER BY name ASC")->result();
    }
    
    function save_investigation_category($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('investigation_categories',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        $insrt=$this->main->insert('investigation_categories',$data);
        
        if($insrt <> null){
            
            $id=$this->main->insert_id();
            $code= str_pad($id,3,'0', STR_PAD_RIGHT);
            return $this->main->update('investigation_categories',array('code'=>$code),array('id'=>$id));
        }
        return FALSE;
    }
    
    function activate_deactivate_investigation_category($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('investigation_categories',array('status'=>$status),array('id'=>$id));
    }
    
    function investigation_category_services_info_count($name,$category){
        
        if($name <> null){
            
           $where .=" AND (s.name LIKE '%$name%') "; 
        }
        
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category' "; 
        }
        
        return count($this->main->query("SELECT ics.id,ics.investigationcategory,"
                                . "ics.serviceid,ics.status,s.name "
                                . "FROM investigation_category_services as ics "
                                . "INNER JOIN services as s "
                                . "ON ics.serviceid=s.id "
                                . "WHERE ics.id is not null $where ")->result());
    }
    
    function investigation_category_services_info($name,$category,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (s.name LIKE '%$name%') "; 
        }
        
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category' "; 
        }
        
        return $this->main->query("SELECT ics.id,ics.investigationcategory,"
                                . "ics.serviceid,ics.status,s.name "
                                . "FROM investigation_category_services as ics "
                                . "INNER JOIN services as s "
                                . "ON ics.serviceid=s.id "
                                . "WHERE ics.id is not null $where "
                                . "ORDER BY ics.investigationcategory,s.name ASC "
                                . "LIMIT $page,$limit")->result();
    } 
    
    function investigation_category_services($id,$category,$service){
        
        if($id <> null){
            $where =" AND id='$id'";
        }
        
        if($category <> null){
            
            $where =" AND investigationcategory='$category'";
        }
        
        if($service <> null){
            
            $where =" AND serviceid='$service'";
        }
        
        return $this->main->query("SELECT id,investigationcategory,"
                                . "serviceid,status "
                                . "FROM investigation_category_services "
                                . "WHERE id is not null $where "
                                . "ORDER BY investigationcategory ASC")->result();
    }
    
    function save_investigation_category_service($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('investigation_category_services',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('investigation_category_services',$data);
    }
    
    function activate_deactivate_investigation_category_service($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('investigation_category_services',array('status'=>$status),array('id'=>$id));
    }
    
    function remove_investigation_category_service($id){
       
        return $this->main->delete('investigation_category_services',array('id'=>$id));
    }
}


?>
