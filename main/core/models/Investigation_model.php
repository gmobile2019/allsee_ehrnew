<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Investigation_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);//load main database configuration
    }
    
    function services_info_count($name,$category){
        
        
        if($name <> null){
            
           $where .=" AND s.name LIKE '%$name%'"; 
        }
        
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category'"; 
        }
        
        return count($this->main->query("SELECT ics.serviceid,ics.investigationcategory,"
                . "ics.status,s.name,s.departmentid,"
                . "s.subdepartmentid "
                . "FROM investigation_category_services as ics "
                . "INNER JOIN services as s ON ics.serviceid=s.id "
                . "WHERE ics.status ='1' AND s.status='1' $where "
                . "ORDER BY s.name ASC")->result());
    }
    
    function services_info($name,$category,$page,$limit){
        
       if($name <> null){
            
           $where .=" AND s.name LIKE '%$name%'"; 
        }
        
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category'"; 
        }
        
      
        return $this->main->query("SELECT ics.serviceid,ics.investigationcategory,"
                . "ics.status,s.name,s.departmentid,"
                . "s.subdepartmentid "
                . "FROM investigation_category_services as ics "
                . "INNER JOIN services as s ON ics.serviceid=s.id "
                . "WHERE ics.status ='1' AND s.status='1' $where "
                . "ORDER BY s.name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function services($name,$category){
        
       if($name <> null){
            
           $where .=" AND s.name LIKE '%$name%'"; 
        }
        
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category'"; 
        }
        
      
        return $this->main->query("SELECT ics.serviceid,ics.investigationcategory,"
                . "ics.status,s.name,s.departmentid,"
                . "s.subdepartmentid "
                . "FROM investigation_category_services as ics "
                . "INNER JOIN services as s ON ics.serviceid=s.id "
                . "WHERE ics.status ='1' AND s.status='1' $where "
                . "ORDER BY s.name ASC")->result();
    }
    
    function examiner_users($userid){
        
        if($userid <> null){
            
           $where .=" AND userid='$userid'"; 
        }
        
        return $this->main->query("SELECT id,userid,investigationcategory,"
                                . "status,supervisor,lastupdater,lastupdate "
                                . "FROM examiner_users "
                                . "WHERE id is not null $where "
                                . "ORDER BY investigationcategory ASC")->result();
    }
    
    function tests_info_count($name,$service,$category){
        
        
        if($name <> null){
            
           $where .=" AND t.name LIKE '%$name%'"; 
        }
        
        if($service <> null){
            
           $where .=" AND t.serviceid='$service'"; 
        }
        
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category'"; 
        }
        
        return count($this->main->query("SELECT t.id,t.serviceid,t.name as test,t.status,"
                . "t.resultcategory,t.unit,ics.investigationcategory,"
                . "s.name,s.departmentid,"
                . "s.subdepartmentid "
                . "FROM investigation_tests as t "
                . "INNER JOIN investigation_category_services as ics ON ics.serviceid=t.serviceid "
                . "INNER JOIN services as s ON s.id=t.serviceid "
                . "WHERE ics.status ='1' AND s.status='1' $where")->result());
    }
    
    function tests_info($name,$service,$category,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND t.name LIKE '%$name%'"; 
        }
        
         if($service <> null){
            
           $where .=" AND t.serviceid='$service'"; 
        }
        
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category'"; 
        }
        
      
        return $this->main->query("SELECT t.id,t.serviceid,t.name as test,t.status,"
                . "t.resultcategory,t.unit,ics.investigationcategory,"
                . "s.name,s.departmentid,"
                . "s.subdepartmentid "
                . "FROM investigation_tests as t "
                . "INNER JOIN investigation_category_services as ics ON ics.serviceid=t.serviceid "
                . "INNER JOIN services as s ON s.id=t.serviceid "
                . "WHERE ics.status ='1' AND s.status='1' $where "
                . "ORDER BY ics.investigationcategory,s.name,t.name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function tests($id,$name,$service,$category,$status,$resultcateg){
        
        if($id <> null){
            
           $where .=" AND t.id='$id'"; 
        }
        
        if($name <> null){
            
           $where .=" AND t.name LIKE '%$name%'"; 
        }
        
         if($service <> null){
            
           $where .=" AND t.serviceid='$service'"; 
        }
        
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category'"; 
        }
        
        if($status <> null){
            
           $where .=" AND t.status='$status'"; 
        }
        
        if($resultcateg <> null){
            
           $where .=" AND t.resultcategory='$resultcateg'"; 
        }
      
        return $this->main->query("SELECT t.id,t.serviceid,t.name as test,t.status,"
                . "t.resultcategory,t.unit,ics.investigationcategory,"
                . "s.name,s.departmentid,"
                . "s.subdepartmentid "
                . "FROM investigation_tests as t "
                . "INNER JOIN investigation_category_services as ics ON ics.serviceid=t.serviceid "
                . "INNER JOIN services as s ON s.id=t.serviceid "
                . "WHERE ics.status ='1' AND s.status='1' $where "
                . "ORDER BY ics.investigationcategory,s.name,t.name ASC")->result();
    }
    
    function add_investigation_service_test($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('investigation_tests',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('investigation_tests',$data);
    }
    
    function activate_deactivate_investigation_service_test($status,$id){
        
                    $status=$status == 1?0:1;
            return  $this->main->update('investigation_tests',array('status'=>$status),array('id'=>$id));
    }
    
    function test_results_info_count($name,$category){
        
        
        if($name <> null){
            
           $where .=" AND ts.name LIKE '%$name%'"; 
        }
       
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category'"; 
        }
        
        return count($this->main->query("SELECT ts.id,ts.testid,ts.name,t.name as test,ts.status,"
                . "ics.investigationcategory "
                . "FROM investigation_test_result_sets as ts "
                . "INNER JOIN investigation_tests as t ON t.id=ts.testid "
                . "INNER JOIN investigation_category_services as ics ON ics.serviceid=t.serviceid "
                . "INNER JOIN services as s ON s.id=t.serviceid "
                . "WHERE ics.status ='1' AND s.status='1' AND t.status='1' AND t.resultcategory='Fixed' $where "
                . "ORDER BY ics.investigationcategory,s.name,t.name,ts.name ASC")->result());
    }
    
    function test_results_info($name,$category,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND ts.name LIKE '%$name%'"; 
        }
       
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category'"; 
        }
        
      
        return $this->main->query("SELECT ts.id,ts.testid,ts.name,t.name as test,ts.status,"
                . "ics.investigationcategory "
                . "FROM investigation_test_result_sets as ts "
                . "INNER JOIN investigation_tests as t ON t.id=ts.testid "
                . "INNER JOIN investigation_category_services as ics ON ics.serviceid=t.serviceid "
                . "INNER JOIN services as s ON s.id=t.serviceid "
                . "WHERE ics.status ='1' AND s.status='1' AND t.status='1' AND t.resultcategory='Fixed' $where "
                . "ORDER BY ics.investigationcategory,s.name,t.name,ts.name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function test_results($id,$name,$category,$status,$test){
        
        if($id <> null){
            
           $where .=" AND ts.id='$id'"; 
        }
        
        if($name <> null){
            
           $where .=" AND ts.name LIKE '%$name%'"; 
        }
       
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category'"; 
        }
        
        if($status <> null){
            
           $where .=" AND ts.status='$status'"; 
        }
        
        if($test <> null){
            
           $where .=" AND ts.testid='$test'"; 
        }
        
        return $this->main->query("SELECT ts.id,ts.testid,ts.name,t.name as test,ts.status,"
                . "ics.investigationcategory "
                . "FROM investigation_test_result_sets as ts "
                . "INNER JOIN investigation_tests as t ON t.id=ts.testid "
                . "INNER JOIN investigation_category_services as ics ON ics.serviceid=t.serviceid "
                . "INNER JOIN services as s ON s.id=t.serviceid "
                . "WHERE ics.status ='1' AND s.status='1' AND t.status='1' AND t.resultcategory='Fixed' $where "
                . "ORDER BY ics.investigationcategory,s.name,t.name,ts.name ASC")->result();
    }
    
    function add_test_result($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('investigation_test_result_sets',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('investigation_test_result_sets',$data);
    }
    
    function activate_deactivate_test_result($status,$id){
        
                    $status=$status == 1?0:1;
            return  $this->main->update('investigation_test_result_sets',array('status'=>$status),array('id'=>$id));
    }
    
    function test_normal_ranges_info_count($test,$category){
        
        
        if($test <> null){
            
           $where .=" AND ts.testid='$test'"; 
        }
       
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category'"; 
        }
        
        return count($this->main->query("SELECT ts.id,ts.testid,ts.name,ts.normalrange,t.name as test,ts.status,"
                . "ics.investigationcategory "
                . "FROM investigation_test_ranges as ts "
                . "INNER JOIN investigation_tests as t ON t.id=ts.testid "
                . "INNER JOIN investigation_category_services as ics ON ics.serviceid=t.serviceid "
                . "INNER JOIN services as s ON s.id=t.serviceid "
                . "WHERE ics.status ='1' AND s.status='1' AND t.status='1' $where "
                . "ORDER BY ics.investigationcategory,s.name,t.name,ts.name ASC")->result());
    }
    
    function test_normal_ranges_info($test,$category,$page,$limit){
        
        if($test <> null){
            
           $where .=" AND ts.testid='$test'"; 
        }
       
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category'"; 
        }
        
      
        return $this->main->query("SELECT ts.id,ts.testid,ts.normalrange,ts.name,t.name as test,ts.status,"
                . "ics.investigationcategory "
                . "FROM investigation_test_ranges as ts "
                . "INNER JOIN investigation_tests as t ON t.id=ts.testid "
                . "INNER JOIN investigation_category_services as ics ON ics.serviceid=t.serviceid "
                . "INNER JOIN services as s ON s.id=t.serviceid "
                . "WHERE ics.status ='1' AND s.status='1' AND t.status='1' $where "
                . "ORDER BY ics.investigationcategory,s.name,t.name,ts.name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function test_normal_ranges($id,$test,$category,$status){
        
        if($id <> null){
            
           $where .=" AND ts.id='$id'"; 
        }
        
        if($test <> null){
            
           $where .=" AND ts.testid='$test'"; 
        }
       
        if($category <> null){
            
           $where .=" AND ics.investigationcategory='$category'"; 
        }
        
        if($status <> null){
            
           $where .=" AND ts.status='$status'"; 
        }
        
        return $this->main->query("SELECT ts.id,ts.testid,ts.normalrange,ts.name,t.name as test,ts.status,"
                . "ics.investigationcategory "
                . "FROM investigation_test_ranges as ts "
                . "INNER JOIN investigation_tests as t ON t.id=ts.testid "
                . "INNER JOIN investigation_category_services as ics ON ics.serviceid=t.serviceid "
                . "INNER JOIN services as s ON s.id=t.serviceid "
                . "WHERE ics.status ='1' AND s.status='1' AND t.status='1' $where "
                . "ORDER BY ics.investigationcategory,s.name,t.name,ts.name ASC")->result();
    }
    
    function add_test_normal_range($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('investigation_test_ranges',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('investigation_test_ranges',$data);
    }
    
    function activate_deactivate_test_normal_range($status,$id){
        
                    $status=$status == 1?0:1;
            return  $this->main->update('investigation_test_ranges',array('status'=>$status),array('id'=>$id));
    }
    
    function patient_investigation_tests($patientid,$patientvisitid,$status,$category,$id,$orderid,$testid){
        
        if($patientid <> null){
            
            $where .=" AND pir.patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
            $where .=" AND pir.patientvisitid='$patientvisitid'";
        }
        
        if($status <> null){
            
            $where .=" AND pir.status='$status'";
        }
        
        if($category <> null){
            
            $where .=" AND ics.investigationcategory='$category'";
        }
        
        if($id <> null){
            
            $where .=" AND pir.id='$id'";
        }
        
        if($orderid <> null){
            
            $where .=" AND pir.orderid='$orderid'";
        }
        
        if($testid <> null){
            
            $where .=" AND pir.testid='$testid'";
        }
       
        return $this->main->query("SELECT pir.id,pir.patientid,pir.patientvisitid,"
                        . "pir.serviceid,pir.orderid,pir.testid,"
                        . "pir.status,pir.result,pir.approvedby,"
                        . "pir.approvedon,pir.createdby,pir.createdon,"
                        . "pir.modifiedby,pir.modifiedon,ics.investigationcategory,s.name,t.name as test FROM patient_investigation_results as pir "
                        . "INNER JOIN investigation_category_services as ics ON ics.serviceid=pir.serviceid "
                        . "INNER JOIN services as s ON s.id=pir.serviceid "
                        . "INNER JOIN investigation_tests as t ON t.id=pir.testid "
                        . "WHERE pir.id is not null $where ORDER BY pir.serviceid,pir.createdon ASC")->result();
    }
    
    function reject_approve_patient_test_result($status,$id){
        
                    if($status == 2){
                        
                        $investigationstatus='completed';
                    }else{
                        
                        $investigationstatus='partial-completed';
                    }
                    
                    $orderid=$this->patient_investigation_tests(null,null,null,null,$id);
                    
                    $this->main->update('service_orders',array('investigationstatus'=>$investigationstatus),array('id'=>$orderid[0]->orderid,'serviceid'=>$orderid[0]->serviceid));
            return  $this->main->update('patient_investigation_results',array('status'=>$status,'approvedby'=>$this->session->userdata('user_id'),'approvedon'=>date('Y-m-d H:i:s')),array('id'=>$id));
    }
    
    function patient_investigation_services($patientid,$patientvisitid,$status,$category,$id){
        
        if($patientid <> null){
            
            $where .=" AND so.patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
            $where .=" AND so.patientvisitid='$patientvisitid'";
        }
        
        if($status <> null){
            
            if($status =='completed'){
                
                $where .=" AND so.investigationstatus='completed'";
            }else{
                
                $where .=" AND (so.investigationstatus='partial-completed' OR so.investigationstatus is null OR so.investigationstatus='')";
            }
            
        }
        
        if($category <> null){
            
            $where .=" AND ics.investigationcategory='$category'";
        }
        
        if($id <> null){
            
            $where .=" AND so.id='$id'";
        }
       
        return $this->main->query("SELECT so.id,so.patientid,so.patientvisitid,"
                        . "so.serviceid,"
                        . "so.status,so.investigationstatus,ics.investigationcategory "
                        . "FROM service_orders as so "
                        . "INNER JOIN investigation_category_services as ics ON ics.serviceid=so.serviceid "
                        . "WHERE so.status='1' $where ORDER BY so.createdon ASC")->result();
    }
    
    function process_investigation_result($array){
        
        $check=$this->patient_investigation_tests($array['patientid'], $array['patientvisitid'], null, null, null,$array['orderid'],$array['testid']);
        
        if($check <> null){
            
                    $array['modifiedby']=$this->session->userdata('user_id');
                    $array['modifiedon']=date('Y-m-d H:i:s');
                    $this->main->update('service_orders',array('investigationstatus'=>'complete'),array('id'=>$array['orderid'],'serviceid'=>$array['serviceid']));
            return $this->main->update('patient_investigation_results',$array,array('id'=>$check[0]->id));
        }
                $array['createdby']=$this->session->userdata('user_id');
                $this->main->update('service_orders',array('investigationstatus'=>'complete'),array('id'=>$array['orderid'],'serviceid'=>$array['serviceid']));
         return $this->main->insert('patient_investigation_results',$array);
    }
    
    function patients_tests_report_info_count($pid,$srv,$start,$end){
        
        
         if($pid <> null){
            
           $where .=" AND pir.patientid LIKE '%$pid%'"; 
        }
       
        if($srv <> null){
            
           $where .=" AND s.name LIKE '%$srv%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pir.createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pir.createdon <='$end'"; 
        }
        
        return count($this->main->query("SELECT DISTINCT pir.patientid,pir.patientvisitid,"
                                . "pir.serviceid,pir.createdon,s.name "
                                . "FROM patient_investigation_results pir "
                                . "INNER JOIN services as s "
                                . "ON s.id=pir.serviceid "
                                . "WHERE pir.status >=1 $where "
                                . "ORDER BY pir.createdon DESC")->result());
    }
    
    function patients_tests_report_info($pid,$srv,$start,$end,$page,$limit){
        
        if($pid <> null){
            
           $where .=" AND pir.patientid LIKE '%$pid%'"; 
        }
       
        if($srv <> null){
            
           $where .=" AND s.name LIKE '%$srv%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pir.createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pir.createdon <='$end'"; 
        }
        
      
        return $this->main->query("SELECT DISTINCT pir.patientid,pir.patientvisitid,"
                                . "pir.serviceid,pir.createdon,s.name "
                                . "FROM patient_investigation_results pir "
                                . "INNER JOIN services as s "
                                . "ON s.id=pir.serviceid "
                                . "WHERE pir.status >=1 $where "
                                . "ORDER BY pir.createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function patients_tests_report($id,$pid,$srv,$start,$end){
        
        if($id <> null){
            
           $where .=" AND pir.id='$id'"; 
        }
        
         if($pid <> null){
            
           $where .=" AND pir.patientid LIKE '%$pid%'"; 
        }
       
        if($srv <> null){
            
           $where .=" AND s.name LIKE '%$srv%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pir.createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pir.createdon <='$end'"; 
        }
        
        return $this->main->query("SELECT DISTINCT pir.patientid,pir.patientvisitid,"
                                . "pir.serviceid,pir.createdon,s.name "
                                . "FROM patient_investigation_results pir "
                                . "INNER JOIN services as s "
                                . "ON s.id=pir.serviceid "
                                . "WHERE pir.status >=1 $where "
                                . "ORDER BY pir.createdon DESC")->result();
    }
}