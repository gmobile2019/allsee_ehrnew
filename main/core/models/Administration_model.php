<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Administration_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);//load main database configuration
            $this->store_salt      = $this->config->item('store_salt', 'ion_auth');
            $this->salt_length     = $this->config->item('salt_length', 'ion_auth');
    }
    
    function current_user_info(){

           return $this->main->query("SELECT users.first_name,users.middle_name,users.last_name "
                   . "FROM users "
                   . "WHERE users.id='".$this->session->userdata('user_id')."'")->row();
    }
    
    function profile_data(){
        
        return $this->main->query("SELECT users.id,users.password,users.salt,users.first_name,users.middle_name,"
                . "users.last_name,users.username,users.email,"
                . "users.msisdn,users_groups.group_id "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id "
                . "WHERE users.id='".$this->session->userdata('user_id')."'")->row();
    }
    
    function member_info_count($name,$group){
        
        if($name <> null){
           $where .=" AND (users.first_name LIKE '%$name%' OR users.middle_name LIKE '%$name%' OR users.last_name LIKE '%$name%')"; 
        }
        
        if($group <> null){
            
            $where .=" AND users_groups.group_id='$group'"; 
        }
        
        return count($this->main->query("SELECT users.id,users.first_name,"
                . "users.middle_name,users.last_name,users.email,"
                . "users.msisdn,users.username,users_groups.group_id "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id AND users_groups.group_id !=1 AND users_groups.group_id !=2 $where "
                . "ORDER BY users.first_name ASC")->result());
    }
    
    function member_info($name,$group,$page,$limit){
        
        if($name <> null){
           $where .=" AND (users.first_name LIKE '%$name%' OR users.middle_name LIKE '%$name%' OR users.last_name LIKE '%$name%')"; 
        }
        
        if($group <> null){
            
            $where .=" AND users_groups.group_id='$group'"; 
        }
        
        return $this->main->query("SELECT users.id,users.first_name,"
                . "users.middle_name,users.last_name,users.email,"
                . "users.msisdn,users.username,users_groups.group_id,users.active "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id AND users_groups.group_id !=1 AND users_groups.group_id !=2 $where "
                . "ORDER BY users.first_name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function get_member_info($id){
        
        if($id <> null){
            
            $where =" WHERE users.id='$id'";
            
        }
       
        return $this->main->query("SELECT users.id,users.first_name,"
                . "users.middle_name,users.last_name,users.email,"
                . "users.msisdn,users.username,users_groups.group_id,users.active "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id AND users_groups.group_id !=1 AND users_groups.group_id !=2 $where "
                . "ORDER BY users.first_name ASC ")->result();
    }
    
    function department_info_count($name){
        
        if($name <> null){
           $where .=" WHERE name LIKE '%$name%'"; 
        }
        
        return count($this->main->query("SELECT id,name,code,servicechargeid,status "
                . "FROM departments $where "
                . "ORDER BY name ASC")->result());
    }
    
    function department_info($name,$page,$limit){
        
        if($name <> null){
           $where .=" WHERE name LIKE '%$name%'"; 
        }
        
        return $this->main->query("SELECT id,name,code,servicechargeid,status "
                . "FROM departments $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function departments($id){
        
        if($id <> null){
            
            $where =" WHERE id='$id'";
            
        }
       
        return $this->main->query("SELECT id,name,code,servicechargeid,status "
                . "FROM departments $where "
                . "ORDER BY name ASC")->result();
    }
    
    function active_departments($id){
        
        if($id <> null){
            
            $where =" AND id='$id'";
            
        }
       
        return $this->main->query("SELECT id,name,code,servicechargeid,status "
                . "FROM departments WHERE status='1' $where "
                . "ORDER BY name ASC")->result();
    }
    
    function save_department($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('departments',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        $this->main->insert('departments',$data);
        
        $id=$this->main->insert_id();
        $code=str_pad($id,3,0,STR_PAD_LEFT);
        
        return $this->main->update('departments',array('code'=>$code),array('id'=>$id));
    }
    
    function activate_deactivate_department($id,$status){
        
            $status=$status == 1?0:1;
                    $this->main->update('departments',array('status'=>$status),array('id'=>$id));
                    $this->main->update('sub_departments',array('status'=>$status),array('departmentid'=>$id));
            return  $this->main->update('services',array('status'=>$status),array('departmentid'=>$id));
    }
    
    function active_dispose_reasons($id){
        
        if($id <> null){
            
            $where =" AND id='$id'";
            
        }
       
        return $this->main->query("SELECT id,name "
                . "FROM inventory_dispose_reasons WHERE status='1' $where "
                . "ORDER BY name ASC")->result();
    }
    
    function dispose_reasons($id){
        
        if($id <> null){
            
            $where =" AND id='$id'";
            
        }
       
        return $this->main->query("SELECT id,name "
                . "FROM inventory_dispose_reasons WHERE $where "
                . "ORDER BY name ASC")->result();
    }
    
    function save_dispose_reason($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('inventory_dispose_reasons',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('inventory_dispose_reasons',$data);
    }
    
    function activate_deactivate_dispose_reason($id,$status){
        
                    $status=$status == 1?0:1;
            return  $this->main->update('inventory_dispose_reasons',array('status'=>$status),array('departmentid'=>$id));
    }
    
    function service_charge($id){
        
        if($id <> null){
            $where .=" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,name,description,service_code,status FROM service_charges WHERE status='1' $where ORDER BY name ASC")->result();
    }
    
    function subdepartment_info_count($name,$department){
        
        if($name <> null){
           $where .=" AND name LIKE '%$name%'"; 
        }
        
        if($department <> null){
            
           $where .=" AND departmentid='$department'"; 
        }
        
        return count($this->main->query("SELECT id,name,code,departmentid,status "
                . "FROM sub_departments WHERE id!=0 $where "
                . "ORDER BY name ASC")->result());
    }
    
    function subdepartment_info($name,$department,$page,$limit){
        
        if($name <> null){
           $where .=" AND name LIKE '%$name%'"; 
        }
        
        if($department <> null){
            
           $where .=" AND departmentid='$department'"; 
        }
        
        return $this->main->query("SELECT id,name,code,departmentid,status "
                . "FROM sub_departments WHERE id!=0 $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function subdepartments($id){
        
        if($id <> null){
            
            $where =" AND id='$id'";
            
        }
       
        return $this->main->query("SELECT id,name,code,departmentid,status "
                . "FROM sub_departments WHERE id !=0 $where "
                . "ORDER BY name ASC")->result();
    }
    
    function active_subdepartments($id){
        
        if($id <> null){
            
            $where =" AND id='$id'";
            
        }
       
        return $this->main->query("SELECT id,name,code,departmentid,status "
                . "FROM sub_departments WHERE status='1' $where "
                . "ORDER BY name ASC")->result();
    }
    
    function save_subdepartment($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('sub_departments',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        $this->main->insert('sub_departments',$data);
        
        $id=$this->main->insert_id();
        $code=str_pad($id,3,0,STR_PAD_LEFT);
        
        return $this->main->update('sub_departments',array('code'=>$code),array('id'=>$id));
    }
    
    function activate_deactivate_subdepartment($id,$status){
        $status=$status == 1?0:1;
                $this->main->update('sub_departments',array('status'=>$status),array('id'=>$id));
        return  $this->main->update('services',array('status'=>$status),array('subdepartmentid'=>$id));
        
    }
    
    function get_service_by_name($name){
        
        return $this->main->query("SELECT id,departmentid,subdepartmentid,name FROM services WHERE name LIKE '$name'")->row();
    }
    
    function save_service($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('services',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('services',$data);
    }
    
    function service_info_count($name,$department,$subdepartment){
        
        if($name <> null){
           $where .=" AND name LIKE '%$name%'"; 
        }
        
        if($department <> null){
            
           $where .=" AND departmentid='$department'"; 
        }
        
        if($subdepartment <> null){
            
           $where .=" AND subdepartmentid='$subdepartment'"; 
        }
        
        return count($this->main->query("SELECT id,name,subdepartmentid,departmentid,status "
                . "FROM services WHERE id!=0 $where "
                . "ORDER BY name ASC")->result());
    }
    
    function service_info($name,$department,$subdepartment,$page,$limit){
        
        if($name <> null){
           $where .=" AND name LIKE '%$name%'"; 
        }
        
        if($department <> null){
            
           $where .=" AND departmentid='$department'"; 
        }
        
        if($subdepartment <> null){
            
           $where .=" AND subdepartmentid='$subdepartment'"; 
        }
        
        return $this->main->query("SELECT id,name,subdepartmentid,departmentid,status "
                . "FROM services WHERE id!=0 $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function service_info_report($name,$department,$subdepartment){
        
        if($name <> null){
           $where .=" AND name LIKE '%$name%'"; 
        }
        
        if($department <> null){
            
           $where .=" AND departmentid='$department'"; 
        }
        
        if($subdepartment <> null){
            
           $where .=" AND subdepartmentid='$subdepartment'"; 
        }
        
        return $this->main->query("SELECT id,name,subdepartmentid,departmentid,status "
                . "FROM services WHERE id!=0 $where "
                . "ORDER BY name ASC")->result();
    }
    
    function services($id,$name){
        
        if($id <> null){
            $where .=" AND id='$id'";
        }
        
        if($name <> null){
            
            $where .=" AND name LIKE '%$name%'";
        }
        
        return $this->main->query("SELECT id,name,subdepartmentid,departmentid,status FROM services WHERE id !=0 $where ORDER BY name ASC")->result();
    }
    
    function active_services($id){
        
        if($id <> null){
            $where .=" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,name,subdepartmentid,departmentid,status FROM services WHERE status ='1' $where ORDER BY name ASC")->result();
    }
    
    function activate_deactivate_service($id,$status){
        $status=$status == 1?0:1;
             
        return $this->main->update('services',array('status'=>$status),array('id'=>$id));
    }
    
    
    function service_cost_info_count($name,$department,$subdepartment,$sponsor){
        
        if($name <> null){
           $where .=" AND sv.name LIKE '%$name%'"; 
        }
        
        if($department <> null){
            
           $where .=" AND sc.departmentid='$department'"; 
        }
        
        if($subdepartment <> null){
            
           $where .=" AND sc.subdepartmentid='$subdepartment'"; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND sc.sponsorid='$sponsor'"; 
        }
        
        $depts=null;
        $department=$this->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
        
        foreach($department as $key=>$value){
            
            $depts .="'$value->id',";
        }
        $depts=  rtrim($depts,',');
        
        return count($this->main->query("SELECT sc.id,sc.serviceid,sc.cost,sc.subdepartmentid,sc.departmentid,sc.status,sc.sponsorid,sv.name "
                . "FROM service_costs as sc INNER JOIN services as sv ON sc.serviceid=sv.id WHERE sc.departmentid NOT IN ($depts) $where "
                . "ORDER BY sv.name ASC")->result());
    }
    
    function service_cost_info($name,$department,$subdepartment,$sponsor,$page,$limit){
        
        if($name <> null){
           $where .=" AND sv.name LIKE '%$name%'"; 
        }
        
        if($department <> null){
            
           $where .=" AND sc.departmentid='$department'"; 
        }
        
        if($subdepartment <> null){
            
           $where .=" AND sc.subdepartmentid='$subdepartment'"; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND sc.sponsorid='$sponsor'"; 
        }
        
        $depts=null;
        $department=$this->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
        
        foreach($department as $key=>$value){
            
            $depts .="'$value->id',";
        }
        $depts=  rtrim($depts,',');
        
        return $this->main->query("SELECT sc.id,sc.serviceid,sc.cost,sc.subdepartmentid,sc.departmentid,sc.status,sc.sponsorid,sv.name "
                . "FROM service_costs as sc INNER JOIN services as sv ON sc.serviceid=sv.id WHERE sc.departmentid NOT IN ($depts) $where "
                . "ORDER BY sv.name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function services_costs_info_report($name,$department,$subdepartment,$sponsor){
        
        if($name <> null){
           $where .=" AND sv.name LIKE '%$name%'"; 
        }
        
        if($department <> null){
            
           $where .=" AND sc.departmentid='$department'"; 
        }
        
        if($subdepartment <> null){
            
           $where .=" AND sc.subdepartmentid='$subdepartment'"; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND sc.sponsorid='$sponsor'"; 
        }
        
        $depts=null;
        $department=$this->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
        
        foreach($department as $key=>$value){
            
            $depts .="'$value->id',";
        }
        $depts=  rtrim($depts,',');
        
        return $this->main->query("SELECT sc.id,sc.serviceid,sc.cost,sc.subdepartmentid,sc.departmentid,sc.status,sc.sponsorid,sv.name "
                . "FROM service_costs as sc INNER JOIN services as sv ON sc.serviceid=sv.id WHERE sc.departmentid NOT IN ($depts) $where "
                . "ORDER BY sv.name ASC")->result();
    }
    
    function service_costs($id){
        
        if($id <> null){
            $where .=" AND id='$id'";
        }
        
        $depts=null;
        $department=$this->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
        
        foreach($department as $key=>$value){
            
            $depts .="'$value->id',";
        }
        $depts=  rtrim($depts,',');
        return $this->main->query("SELECT id,serviceid,cost,subdepartmentid,departmentid,status,sponsorid "
                . "FROM service_costs WHERE departmentid NOT IN ($depts) $where")->result();
    }
    
    function check_service_cost($data){
        
        return $this->main->get_where('service_costs',$data)->row();
    }
    
    function save_service_cost($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('service_costs',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('service_costs',$data);
    }
    
     function activate_deactivate_service_cost($id,$status){
        $status=$status == 1?0:1;
             
        return $this->main->update('service_costs',array('status'=>$status),array('id'=>$id));
    }
    
    function items_cost_info_count($code,$name,$sponsor,$mincost,$maxcost){
        
        if($name <> null){
            
           $where .=" AND (it.name LIKE '%$name%') "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND sc.sponsorid='$sponsor'"; 
        }
        
         if($mincost <> null){
            
           $where .=" AND cost >='$mincost' "; 
        }
        
        if($maxcost <> null){
            
           $where .=" AND cost <='$maxcost' "; 
        }
        $depts=null;
        $department=$this->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
        
        foreach($department as $key=>$value){
            
            $depts .="'$value->id',";
        }
        $depts=  rtrim($depts,',');
        
        return count($this->main->query("SELECT sc.id,sc.cost,sc.sponsorid,sc.status,it.name,"
                . "it.category "
                . "FROM  service_costs as sc "
                . "INNER JOIN inventory_items as it "
                . "ON sc.serviceid=it.id "
                . "WHERE it.category='$code' AND departmentid IN ($depts) $where "
                . "ORDER BY it.name ASC")->result());
    }
    
    function items_cost_info($code,$name,$sponsor,$mincost,$maxcost,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (it.name LIKE '%$name%') "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND sc.sponsorid='$sponsor'"; 
        }
        
         if($mincost <> null){
            
           $where .=" AND cost >='$mincost' "; 
        }
        
        if($maxcost <> null){
            
           $where .=" AND cost <='$maxcost' "; 
        }
        $depts=null;
        $department=$this->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
        
        foreach($department as $key=>$value){
            
            $depts .="'$value->id',";
        }
        $depts=  rtrim($depts,',');
        
        return $this->main->query("SELECT sc.id,sc.cost,sc.sponsorid,sc.status,it.name,"
                . "it.category "
                . "FROM  service_costs as sc "
                . "INNER JOIN inventory_items as it "
                . "ON sc.serviceid=it.id "
                . "WHERE it.category='$code' AND departmentid IN ($depts) $where "
                . "ORDER BY it.name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function items_cost($code,$name,$sponsor,$min,$max){
        
        if($name <> null){
            
           $where .=" AND (it.name LIKE '%$name%') "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND sc.sponsorid='$sponsor'"; 
        }
        
         if($mincost <> null){
            
           $where .=" AND cost >='$mincost' "; 
        }
        
        if($maxcost <> null){
            
           $where .=" AND cost <='$maxcost' "; 
        }
        $depts=null;
        $department=$this->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
        
        foreach($department as $key=>$value){
            
            $depts .="'$value->id',";
        }
        $depts=  rtrim($depts,',');
        
        return $this->main->query("SELECT sc.id,sc.cost,sc.sponsorid,sc.status,it.name,"
                . "it.category "
                . "FROM  service_costs as sc "
                . "INNER JOIN inventory_items as it "
                . "ON sc.serviceid=it.id "
                . "WHERE it.category='$code' AND departmentid IN ($depts) $where "
                . "ORDER BY it.name ASC")->result();
    }
    
    function get_item_by_name($name){
        
        return $this->main->query("SELECT id,name,drug_type,drug_group,category FROM inventory_items WHERE name LIKE '$name' AND status='1' AND consumptioncategory='1'")->row();
    }
    /*
     * retrieve all the doctors
     * @param doc_categ=doctor category
     * @param user_id=system user id
     */
    function doctors($doc_categ,$user_id){
        
        if($user_id <> null){
            
            $where =" AND ud.userid='$user_id'";
        }
        
        if($doc_categ <> null){
            
            $where =" AND ud.doctorcategory='$doc_categ'";
        }

        return $this->main->query("SELECT ud.doctorcategory,ud.availability,ud.status,ud.userid,ud.id,u.first_name,u.last_name FROM users_doctors as ud INNER JOIN users as u ON ud.userid=u.id $where ORDER BY u.first_name ASC")->result();
    }
    
   
    /*
     * retrieve consultation category
     * @param department=department of consultation categories
     * @param id=consultation category unique id
     */
    function consultation_categories($department,$id){
        
        if($id <> null){
            
            $where =" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,code,name,departmentid,status FROM sub_departments WHERE departmentid='$department' $where ORDER BY name ASC")->result();
    }
    
    /*
     * retrieve a department by code
     * @param servicechargeid=service charge code
     */
    function get_department_by_servicecharge_code($servicechargecode){
        
        return $this->main->get_where('departments',array('servicechargeid'=>$servicechargecode))->result();
    }
    
    function get_subdepartment_by_department($departmentid){
        
        return $this->main->get_where('sub_departments',array('departmentid'=>$departmentid))->result();
    }
    
    function inventory_unit_users($id){
        
        return $this->main->get_where('inventory_unit_users',array('userid'=>$id,'status'=>1))->result();
    }
    
    function packages($id){
        $where=null;
        if($id <> null){
            $where .=" AND packageid='$id'";
        } 
        
        return $this->main->query("SELECT id,packageid,name,cost,status FROM packages WHERE id!=0 $where ORDER BY name ASC")->result();
    }
    
    function package_service_item($packageid,$service_item_id,$subdepartmentid,$departmentid,$status){
        $where=null;
        if($packageid <> null){
            $where .=" AND packageid='$packageid'";
        }
        
         if($service_item_id <> null){
             
            $where .=" AND item_service_id='$service_item_id'";
        }
        
         if($subdepartmentid <> null){
             
            $where .=" AND subdepartmentid='$subdepartmentid'";
        }
        
         if($departmentid <> null){
             
            $where .=" AND departmentid='$departmentid'";
        }
       
        if($status <> null){
             
            $where .=" AND status='$status'";
        }
       
        return $this->main->query("SELECT id,packageid,item_service_id,subdepartmentid,departmentid,status,quantity FROM package_items_services WHERE id!=0 $where ORDER BY departmentid,subdepartmentid ASC")->result();
    }
    
    function save_package($package,$package_item_service,$id){
        
        if($id <> null){
            $package['modifiedby']=$this->session->userdata('user_id');
            $package['modifiedon']=date('Y-m-d H:i:s');
            $this->main->update('packages',$package,array('packageid'=>$id));
            
            $this->main->delete('package_items_services',array('packageid'=>$id));
            foreach ($package_item_service as $key=>$value){
                
                    $value['createdby']=$this->session->userdata('user_id');
                    $this->main->insert('package_items_services',$value);
            }
           
            return TRUE;
        }
        
        $package['createdby']=$this->session->userdata('user_id');
        $this->main->insert('packages',$package);
         
         foreach ($package_item_service as $key=>$value){
             
                $value['createdby']=$this->session->userdata('user_id');
                $this->main->insert('package_items_services',$value);
         }
        return TRUE;
    }
    
    function packages_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        return count($this->main->query("SELECT id,name,packageid,cost,status "
                . "FROM  packages "
                . "WHERE id!=0 $where "
                . "ORDER BY name ASC")->result());
    }
    
    function packages_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
       
        return $this->main->query("SELECT id,name,packageid,cost,status "
                . "FROM  packages "
                . "WHERE id!=0 $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function activate_deactivate_package($id,$status){
        $status=$status == 1?0:1;
             
        return $this->main->update('packages',array('status'=>$status),array('packageid'=>$id));
    }
    
    function package_items_services_info_count($id){
        
        if($id <> null){
            
           $where .=" AND pis.packageid='$id'"; 
        }
        
        return count($this->main->query("SELECT pis.id,p.name,pis.packageid,"
                . "pis.item_service_id,pis.status,"
                . "d.name as department,sd.name as subdepartment,pis.quantity "
                . "FROM  package_items_services as pis "
                . "INNER JOIN packages as p ON pis.packageid=p.packageid "
                . "INNER JOIN departments as d ON pis.departmentid=d.id "
                . "INNER JOIN sub_departments as sd ON pis.subdepartmentid=sd.id "
                . "WHERE pis.id!=0 $where "
                . "ORDER BY d.name,sd.name ASC")->result());
    }
    
    function package_details_info($id,$page,$limit){
        
        if($id <> null){
            
           $where .=" AND pis.packageid='$id'"; 
        }
        
       
        return $this->main->query("SELECT pis.id,p.name,pis.packageid,pis.departmentid,"
                . "pis.item_service_id,pis.status,"
                . "d.name as department,sd.name as subdepartment,pis.quantity "
                . "FROM  package_items_services as pis "
                . "INNER JOIN packages as p ON pis.packageid=p.packageid "
                . "INNER JOIN departments as d ON pis.departmentid=d.id "
                . "INNER JOIN sub_departments as sd ON pis.subdepartmentid=sd.id "
                . "WHERE pis.id!=0 $where "
                . "ORDER BY d.name,sd.name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
     function activate_deactivate_package_detail($id,$status){
        $status=$status == 1?0:1;
             
        return $this->main->update('package_items_services',array('status'=>$status),array('id'=>$id));
    }
    
    function ward_beds_info_count($identity,$ward){
        
        if($identity <> null){
            
           $where .=" AND (identity LIKE '%$identity%') "; 
        }
        
        if($ward <> null){
            
           $where .=" AND wardid='$ward' "; 
        }
        
        return count($this->main->query("SELECT id,bedidentity,"
                . "wardid,status "
                . "FROM inpatient_ward_beds WHERE id !=0 $where")->result());
    }
    
    function ward_beds_info($identity,$ward,$page,$limit){
        
        if($identity <> null){
            
           $where .=" AND (identity LIKE '%$identity%') "; 
        }
        
        if($ward <> null){
            
           $where .=" AND wardid='$ward' "; 
        }
        
        return $this->main->query("SELECT id,bedidentity,"
                . "wardid,status "
                . "FROM inpatient_ward_beds WHERE id !=0 $where "
                . "ORDER BY wardid,bedidentity ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function ward_beds($id,$status){
        
        
        if($id <> null){
            
           $where .=" AND id='$id' "; 
        }
        
        if($status <> null){
            
           $where .=" AND status='$status' "; 
        }
        
        return $this->main->query("SELECT id,bedidentity,"
                . "wardid,status "
                . "FROM inpatient_ward_beds WHERE id !=0 $where "
                . "ORDER BY wardid,bedidentity ASC")->result();
    }
    
    function save_ward_bed($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('inpatient_ward_beds',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('inpatient_ward_beds',$data);
    }
    
    function activate_deactivate_ward_bed($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('inpatient_ward_beds',array('status'=>$status),array('id'=>$id));
    }
    
    function nurses_info_count($name,$ward){
        
        if($name <> null){
           $where .=" AND (u.first_name LIKE '%$name%' OR u.middle_name LIKE '%$name%' OR u.last_name LIKE '%$name%')"; 
        }
        
        if($ward <> null){
            
            $where .=" AND ius.inventoryunit='$ward'"; 
        }
        
                
        return count($this->main->query("SELECT ius.id,u.id AS userid,"
                . "ius.inventoryunit,u.first_name,u.middle_name,"
                . "u.last_name,u.active,ug.group_id "
                . "FROM users AS u "
                . "INNER JOIN users_groups AS ug ON u.id=ug.user_id "
                . "LEFT OUTER JOIN inventory_unit_users AS ius ON ius.userid=u.id "
                . "WHERE ((ius.status=1 AND ug.group_id =3) OR (ug.group_id =3 AND ius.inventoryunit is null))  $where")->result());
    }
    
    function nurses_info($name,$ward,$page,$limit){
        
        if($name <> null){
           $where .=" AND (u.first_name LIKE '%$name%' OR u.middle_name LIKE '%$name%' OR u.last_name LIKE '%$name%')"; 
        }
        
        if($ward <> null){
            
            $where .=" AND ius.inventoryunit='$ward'"; 
        }
        
        return $this->main->query("SELECT ius.id,u.id AS userid,"
                . "ius.inventoryunit,u.first_name,u.middle_name,"
                . "u.last_name,u.active,ug.group_id "
                . "FROM users AS u "
                . "INNER JOIN users_groups AS ug ON u.id=ug.user_id "
                . "LEFT OUTER JOIN inventory_unit_users AS ius ON ius.userid=u.id "
                . "WHERE ((ius.status=1 AND ug.group_id =3) OR (ug.group_id =3 AND ius.inventoryunit is null))  $where "
                . "ORDER BY ius.inventoryunit ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    /*
     * retrieve all the examiners
     * @param inv_categ=investigation category
     * @param user_id=system user id
     */
    function examiners($inv_categ,$user_id){
        
        if($user_id <> null){
            
            $where =" AND ui.userid='$user_id'";
        }
        
        if($doc_categ <> null){
            
            $where =" AND ui.investigationcategory='$inv_categ'";
        }

        return $this->main->query("SELECT ui.investigationcategory,ui.supervisor,ui.status,ui.userid,u.id,u.first_name,u.last_name FROM examiner_users as ui INNER JOIN users as u ON ui.userid=u.id $where ORDER BY u.first_name ASC")->result();
    }
}


?>
