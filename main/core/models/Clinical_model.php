<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Clinical_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);//load main database configuration
    }
    
    function save_physcial_examination($data){
        
        return $this->main->insert('patient_physical_examinations',$data);
    }
    
    function patient_physcial_examinations($patientid,$patientvisitid,$start,$end){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        return $this->main->query("SELECT patientid,patientvisitid,cardiovascularsystem,"
                                . " respiratorysystem,abdomen,centralnervoussystem,"
                                . " muscularskeletalsystem,createdon,createdby"
                                . " FROM patient_physical_examinations "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC")->result();
    }
    
    function patient_physcial_examinations_info_count($patientid,$patientvisitid,$start,$end){
        
       if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        return count($this->main->query("SELECT patientid,patientvisitid,cardiovascularsystem,"
                                . " respiratorysystem,abdomen,centralnervoussystem,"
                                . " muscularskeletalsystem,createdon,createdby"
                                . " FROM patient_physical_examinations "
                                . " WHERE id is not null $where")->result());
    }
    
    function patient_physcial_examinations_info($patientid,$patientvisitid,$start,$end,$page,$limit){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        return $this->main->query("SELECT patientid,patientvisitid,cardiovascularsystem,"
                                . " respiratorysystem,abdomen,centralnervoussystem,"
                                . " muscularskeletalsystem,createdon,createdby"
                                . " FROM patient_physical_examinations "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function save_vitals_examination($data){
        
        return $this->main->insert('patient_vitals_examinations',$data);
    }
    
    function patient_vitals_examinations($patientid,$patientvisitid,$start,$end){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        return $this->main->query("SELECT patientid,patientvisitid,weight,"
                                . " height,pulserate,bloodpressure,"
                                . " temperature,spo2,respirationrate,createdon,createdby"
                                . " FROM patient_vitals_examinations "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC")->result();
    }
    
    function patient_vitals_examinations_info_count($patientid,$patientvisitid,$start,$end){
        
       if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        return count($this->main->query("SELECT patientid,patientvisitid,weight,"
                                . " height,pulserate,bloodpressure,"
                                . " temperature,spo2,respirationrate,createdon,createdby"
                                . " FROM patient_vitals_examinations "
                                . " WHERE id is not null $where")->result());
    }
    
    function patient_vitals_examinations_info($patientid,$patientvisitid,$start,$end,$page,$limit){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        return $this->main->query("SELECT patientid,patientvisitid,weight,"
                                . " height,pulserate,bloodpressure,"
                                . " temperature,spo2,respirationrate,createdon,createdby"
                                . " FROM patient_vitals_examinations "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function standard_complains($id,$code){
         if($id <> null){
            
             $where .=" AND id='$id'";
        }
        
        if($code <> null){
            
             $where .=" AND code='$code'";
        }
        
        return $this->main->query("SELECT id,code,name,"
                                . " remark,status"
                                . " FROM complaints_icd10_standard "
                                . " WHERE id is not null $where "
                                . " ORDER BY code ASC")->result();
    }
    
    function patient_diagnosis($patientid,$patientvisitid){
        
        if($patientid <> null){
            
            $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
            $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        return $this->main->query("SELECT patientid,patientvisitid,complains,"
                                    . "provisionaldiagnosis,finaldiagnosis,finalcomment,"
                                    . "createdby,modifiedby,createdon,modifiedon "
                                    . "FROM patients_diagnosis "
                                    . "WHERE id is not null $where "
                                    . "ORDER BY createdon DESC")->result();
    }
    
    function save_patient_diagnosis($data,$diagnosis){
        
        if($diagnosis){
            
            return $this->main->insert('patients_diagnosis',$data);
        }
        
        return $this->main->update('patients_diagnosis',$data,array('patientvisitid'=>$data['patientvisitid']));
    }
    
    function patient_allegies_info_count($patientid,$patientvisitid,$start,$end,$status){
        
       if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> null){
            
             $where .=" AND allegystatus='$status'";
        }
        
        return count($this->main->query("SELECT id,patientid,patientvisitid,allegy,allegystatus,remarks,"
                                . " createdon,createdby"
                                . " FROM  patients_allegies "
                                . " WHERE id is not null $where")->result());
    }
    
    function patient_allegies_info($patientid,$patientvisitid,$start,$end,$status,$page,$limit){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> null){
            
             $where .=" AND allegystatus='$status'";
        }
        
        return $this->main->query("SELECT id,patientid,patientvisitid,allegy,allegystatus,remarks,"
                                . " createdon,createdby"
                                . " FROM  patients_allegies "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function patient_allegies($patientid,$patientvisitid,$start,$end,$status){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> null){
            
             $where .=" AND allegystatus='$status'";
        }
        
        return $this->main->query("SELECT id,patientid,patientvisitid,allegy,allegystatus,remarks,"
                                . " createdon,createdby"
                                . " FROM  patients_allegies "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC")->result();
    }
    
    function save_patient_allegy($data){
        
        return $this->main->insert('patients_allegies',$data);
    }
    
    function patient_service_orders_info_count($patientid,$patientvisitid,$start,$end,$status){
        
       if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> null){
            
             $where .=" AND status='$status'";
        }
        
        return count($this->main->query("SELECT id,patientid,patientvisitid,departmentid,subdepartmentid,serviceid,"
                                . " status,createdon,createdby"
                                . " FROM  service_orders "
                                . " WHERE id is not null $where")->result());
    }
    
    function patient_service_orders_info($patientid,$patientvisitid,$start,$end,$status,$page,$limit){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> null){
            
             $where .=" AND status='$status'";
        }
        
        return $this->main->query("SELECT id,patientid,patientvisitid,departmentid,subdepartmentid,serviceid,"
                                . " status,createdon,createdby"
                                . " FROM  service_orders "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function patient_service_orders($patientid,$patientvisitid,$start,$end,$status){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        
        return $this->main->query("SELECT id,patientid,patientvisitid,departmentid,subdepartmentid,serviceid,"
                                . " status,createdon,createdby"
                                . " FROM  service_orders "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC")->result();
    }
    
    function patient_prescriptions_info_count($patientid,$patientvisitid,$start,$end,$status){
        
       if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> null){
            
             $where .=" AND status='$status'";
        }
        
        return count($this->main->query("SELECT id,patientid,patientvisitid,frequencyid,dosageid,quantity,days,itemid,"
                                . " status,createdon,createdby"
                                . " FROM  patients_dispense_orders "
                                . " WHERE id is not null $where")->result());
    }
    
    function patient_prescriptions_info($patientid,$patientvisitid,$start,$end,$status,$page,$limit){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> null){
            
             $where .=" AND status='$status'";
        }
        
        return $this->main->query("SELECT id,patientid,patientvisitid,frequencyid,dosageid,quantity,days,itemid,"
                                . " status,createdon,createdby"
                                . " FROM  patients_dispense_orders "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function patient_prescriptions($patientid,$patientvisitid){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> null){
            
             $where .=" AND status='$status'";
        }
        
        return $this->main->query("SELECT id,patientid,patientvisitid,frequencyid,dosageid,quantity,days,itemid,"
                                . " status,createdon,createdby"
                                . " FROM  patients_dispense_orders "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC")->result();
    }
    
    function patient_appointments_info_count($patientid,$patientvisitid,$start,$end,$status){
        
       if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> null){
            
             $where .=" AND status='$status'";
        }
        
        return count($this->main->query("SELECT id,patientid,patientvisitid,appointmentdoctor,appointmentdate,"
                                . " appointmenttime,createdon,createdby,status"
                                . " FROM patients_appointments "
                                . " WHERE id is not null $where")->result());
    }
    
    function patient_appointments_info($patientid,$patientvisitid,$start,$end,$status,$page,$limit){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> null){
            
             $where .=" AND status='$status'";
        }
        
        return $this->main->query("SELECT id,patientid,patientvisitid,appointmentdoctor,appointmentdate,"
                                . " appointmenttime,createdon,createdby,status"
                                . " FROM patients_appointments "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function patient_appointments($patientid,$patientvisitid){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        return $this->main->query("SELECT id,patientid,patientvisitid,appointmentdoctor,appointmentdate,"
                                . " appointmenttime,createdon,createdby,status"
                                . " FROM patients_appointments "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC")->result();
    }
    
    function save_clinical_appointment($data){
        
        return $this->main->insert('patients_appointments',$data);
    }
    
    function cancel_appointment($id){
        
        return $this->main->update('patients_appointments',array('status'=>3),array('id'=>$id));
    }
    
    function update_consultation_status($patientid,$patientvisitid){
        
        return $this->main->update("patients_visit",array('consultationstatus'=>1,'modifiedby'=>$this->session->userdata('user_id'),'modifiedon'=>date('Y-m-d H:i:s')),array('patientid'=>$patientid,'patientvisitid'=>$patientvisitid));
    }
    
    function patient_admission($patientid,$patientvisitid){
        
        return $this->main->update("patients_visit",array('patientcategory'=>3,'modifiedby'=>$this->session->userdata('user_id'),'modifiedon'=>date('Y-m-d H:i:s')),array('patientid'=>$patientid,'patientvisitid'=>$patientvisitid));
    }
    
    function inpatient_doctor_notes_info_count($patientid,$patientvisitid,$start,$end){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        return count($this->main->query("SELECT id,patientid,patientvisitid,admissionid,overallprogress,complains,recommendations,"
                                . " createdon,createdby"
                                . " FROM  doctor_inpatient_care "
                                . " WHERE id is not null $where")->result());
    }
    
    function inpatient_doctor_notes_info($patientid,$patientvisitid,$start,$end,$page,$limit){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
                
        return $this->main->query("SELECT id,patientid,patientvisitid,admissionid,overallprogress,complains,recommendations,"
                                . " createdon,createdby"
                                . " FROM  doctor_inpatient_care "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function inpatient_doctor_notes($patientid,$patientvisitid,$start,$end){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
                
        return $this->main->query("SELECT id,patientid,patientvisitid,admissionid,"
                                . "overallprogress,complains,recommendations,"
                                . " createdon,createdby"
                                . " FROM  doctor_inpatient_care "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC")->result();
    }
    
    function save_doctor_note($data){
        
        return $this->main->insert('doctor_inpatient_care',$data);
    }
    
    function family_planning_sessions_info_count($patientid,$name,$start,$end){
        
        if($patientid <> null){
            
             $where .=" AND fp.patientid LIKE '%$patientid%'";
        }
        
        if($name <> null){
            
             $where .=" AND p.name LIKE '%$name%'";
        }
        
        if($start <> null){
            
             $where .=" AND fp.createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND fp.createdon <='$end 23:59:59'";
        }
        
                
        return count($this->main->query("SELECT fp.id,fp.patientid,fp.patientvisitid,"
                                . "fp.createdon,fp.createdby,p.name "
                                . "FROM  family_planning_sessions as fp "
                                . "INNER JOIN patients as p "
                                . "ON fp.patientid=p.patientid "
                                . "WHERE fp.id is not null $where")->result());
    }
    
    function family_planning_sessions_info($patientid,$name,$start,$end,$page,$limit){
        
       if($patientid <> null){
            
             $where .=" AND fp.patientid LIKE '%$patientid%'";
        }
        
        if($name <> null){
            
             $where .=" AND p.name LIKE '%$name%'";
        }
        
        if($start <> null){
            
             $where .=" AND fp.createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND fp.createdon <='$end 23:59:59'";
        }
        
                
        return $this->main->query("SELECT fp.id,fp.patientid,fp.patientvisitid,"
                                . "fp.createdon,fp.createdby,p.name "
                                . "FROM  family_planning_sessions as fp "
                                . "INNER JOIN patients as p "
                                . "ON fp.patientid=p.patientid "
                                . "WHERE fp.id is not null $where "
                                . "ORDER BY fp.createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function family_planning_sessions($patientid,$patientvisitid,$name,$start,$end){
        
        if($patientid <> null){
            
             $where .=" AND fp.patientid LIKE '%$patientid%'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND fp.patientvisitid LIKE '$patientvisitid'";
        }
        
        if($name <> null){
            
             $where .=" AND p.name LIKE '%$name%'";
        }
        
        if($start <> null){
            
             $where .=" AND fp.createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND fp.createdon <='$end 23:59:59'";
        }
        
                
        return $this->main->query("SELECT fp.id,fp.patientid,fp.patientvisitid,"
                                . "fp.partner,fp.prevpregnancies,fp.birthcount,"
                                . "fp.stillbirthcount,fp.abortions,fp.childrencount,"
                                . "fp.sevendaychildrendeathcount,fp.agelastchild,fp.contraceptionmethod,"
                                . "fp.nextconsultation,fp.remarks,fp.createdon,fp.createdby,p.name "
                                . "FROM  family_planning_sessions as fp "
                                . "INNER JOIN patients as p "
                                . "ON fp.patientid=p.patientid "
                                . "WHERE fp.id is not null $where "
                                . "ORDER BY fp.createdon DESC")->result();
    }
    
    function save_family_planning_session($data){
        
        return $this->main->insert('family_planning_sessions',$data);
    }
    
    function contraceptive_methods($id,$name){
        
        if($patientid <> null){
            
             $where .=" AND id='$id'";
        }
        
        if($name <> null){
            
             $where .=" AND name LIKE '%$name%'";
        }
        
        
                
        return $this->main->query("SELECT id,name "
                                . "FROM  contraceptive_methods "
                                . "WHERE id is not null $where "
                                . "ORDER BY name DESC")->result();
    }
}