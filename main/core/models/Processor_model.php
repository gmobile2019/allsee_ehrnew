<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Processor_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);//load main database configuration
    }
    
    function process_expired_visits(){
        
        return $this->main->query("UPDATE patients_visit SET exitstatus='1' WHERE createdon <'".$this->config->item('expired_visit_date')."' AND patientcategory='".$this->config->item('out_patient_id')."'");
    }
    
    function process_expired_appointments(){
        
         return $this->main->query("UPDATE patients_appointments SET status='4',modifiedon='".date('Y-m-d H:i:s')."' WHERE CONCAT (appointmentdate,' ',appointmenttime) <='NOW() + INTERVAL ".$this->config->item('expiry_hours')." HOUR'");
    }
      
}