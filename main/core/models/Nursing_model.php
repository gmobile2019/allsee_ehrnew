<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Nursing_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);//load main database configuration
            $this->store_salt      = $this->config->item('store_salt', 'ion_auth');
            $this->salt_length     = $this->config->item('salt_length', 'ion_auth');
    }
    
    function admission_details($patientid,$patientvisitid,$admissionid,$status,$wardid,$bedid){
        
        if($patientid <> null){
            
           $where .=" AND patientid='$patientid'"; 
        }
        
        if($patientvisitid <> null){
            
            $where .=" AND patientvisitid='$patientvisitid'"; 
        }
        
        if($admissionid <> null){
            
            $where .=" AND admissionid='$admissionid'"; 
        }
        
        if($status <> null){
            
            $where .=" AND status='$status'"; 
        }
       
        if($wardid <> null){
            
            $where .=" AND wardid='$wardid'"; 
        }
        
        if($bedid <> null){
            
            $where .=" AND bedid='$bedid'"; 
        }
        
        return $this->main->query("SELECT patientid,patientvisitid,admissionid,"
                                    . "status,wardid,bedid,admissiontype,admissionsource,"
                                    . "createdby,createdon,"
                                    . "dischargedby,dischargedon,dischargingdoctor,"
                                    . "dischargeinitiationtimestamp,dischargereason,dischargenotes "
                                    . "FROM patient_admission_details "
                                    . "WHERE id is not null $where "
                                    . "ORDER BY createdon DESC")->result();
    }
    
    function process_admission($data,$admssionStatus){
        
        if(!$admssionStatus){
            return $this->main->insert('patient_admission_details',$data);
        }
        
        $admissionid=$data['admissionid'];
        
        return $this->inpatient_transfer($data,$admissionid);
    }
    
    function inpatient_transfer($data,$admissionid){
        
        $current_admsn_details=$this->admission_details(null,null,$admissionid,null,null,null);
        
        if($data['wardid'] == $current_admsn_details[0]->wardid && $data['bedid'] == $current_admsn_details[0]->bedid){
           
            return FALSE;
        }
        
        $array=array(
            'patientid'=>$current_admsn_details[0]->patientid,
            'patientvisitid'=>$current_admsn_details[0]->patientvisitid,
            'admissionid'=>$current_admsn_details[0]->admissionid,
            'wardid'=>$current_admsn_details[0]->wardid,
            'bedid'=>$current_admsn_details[0]->bedid,
            'status'=>'TRANSFERED',
            'admissiontype'=>$current_admsn_details[0]->admissiontype,
            'admissionsource'=>$current_admsn_details[0]->admissionsource,
            'admittedby'=>$current_admsn_details[0]->createdby,
            'admittedon'=>$current_admsn_details[0]->createdon,
            'modifiedby'=>$current_admsn_details[0]->modifiedby,
            'modifiedon'=>$current_admsn_details[0]->modifiedon,
            'transferedby'=>$this->session->userdata('user_id'),
        );
        
        $transfer=$this->main->insert('patient_admission_transfer_details',$array);
        
        if($transfer){
            
            return $this->main->update('patient_admission_details',$data,array('admissionid'=>$admissionid));
        }
        
        return FALSE;
    }
    
    function inpatient_nurse_notes_info_count($patientid,$patientvisitid,$start,$end){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
        return count($this->main->query("SELECT id,patientid,patientvisitid,admissionid,"
                                        . "currentdiagnosis,treatment,procedures,medications,"
                                        . "medicationeffect,medicationcomments,intakes,"
                                        . "outputs,overallprogress,remarks,"
                                        . " createdon,createdby"
                                        . " FROM  nurse_inpatient_care "
                                        . " WHERE id is not null $where")->result());
    }
    
    function inpatient_nurse_notes_info($patientid,$patientvisitid,$start,$end,$page,$limit){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
                
        return $this->main->query("SELECT id,patientid,patientvisitid,admissionid,"
                                . "currentdiagnosis,treatment,procedures,medications,"
                                . "medicationeffect,medicationcomments,intakes,"
                                . "outputs,overallprogress,remarks,"
                                . " createdon,createdby"
                                . " FROM  nurse_inpatient_care "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function inpatient_nurse_notes($patientid,$patientvisitid,$start,$end){
        
        if($patientid <> null){
            
             $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
             $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
        
                
        return $this->main->query("SELECT id,patientid,patientvisitid,admissionid,"
                                . "currentdiagnosis,treatment,procedures,medications,"
                                . "medicationeffect,medicationcomments,intakes,"
                                . "outputs,overallprogress,remarks,"
                                . " createdon,createdby"
                                . " FROM  nurse_inpatient_care "
                                . " WHERE id is not null $where "
                                . " ORDER BY createdon DESC")->result();
    }
    
    function save_nurse_note($data){
        
        return $this->main->insert('nurse_inpatient_care',$data);
    }
    
    function admissiontypes_info_count($name){
        
        if($name <> null){
            
           $where .=" AND name LIKE '%$name%'"; 
        }
        
        
        return count($this->main->query("SELECT id,name,status "
                . "FROM admissiontypes "
                . "WHERE id !='0' $where")->result());
    }
    
    function admissiontypes_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND name LIKE '%$name%'"; 
        }
        
       
        return $this->main->query("SELECT id,name,status "
                . "FROM admissiontypes "
                . "WHERE id !='0' $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function admissiontypes($id,$status){
        
        if($id <> null){
            
           $where .=" AND id='$id'"; 
        }
        
        if($status <> null){
            
           $where .=" AND status='$status'"; 
        }
       
        return $this->main->query("SELECT id,name,status "
                . "FROM admissiontypes "
                . "WHERE id !='0' $where "
                . "ORDER BY name ASC")->result();
    } 
    
    function save_admissiontype($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('admissiontypes',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('admissiontypes',$data);
    }
    
    function activate_deactivate_admissiontype($status,$id){
        
                    $status=$status == 1?0:1;
            return  $this->main->update('admissiontypes',array('status'=>$status),array('id'=>$id));
    }
    
    function admissionsources_info_count($name){
        
        if($name <> null){
            
           $where .=" AND name LIKE '%$name%'"; 
        }
        
        
        return count($this->main->query("SELECT id,name,status "
                . "FROM admissionsources "
                . "WHERE id !='0' $where")->result());
    }
    
    function admissionsources_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND name LIKE '%$name%'"; 
        }
        
       
        return $this->main->query("SELECT id,name,status "
                . "FROM admissionsources "
                . "WHERE id !='0' $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function admissionsources($id,$status){
        
        if($id <> null){
            
           $where .=" AND id='$id'"; 
        }
        
        if($status <> null){
            
           $where .=" AND status='$status'"; 
        }
       
        return $this->main->query("SELECT id,name,status "
                . "FROM admissionsources "
                . "WHERE id !='0' $where "
                . "ORDER BY name ASC")->result();
    } 
    
    function save_admissionsource($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('admissionsources',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('admissionsources',$data);
    }
    
    function activate_deactivate_admissionsource($status,$id){
        
                    $status=$status == 1?0:1;
            return  $this->main->update('admissionsources',array('status'=>$status),array('id'=>$id));
    }
    
    function admission_report_count($start,$end,$ward,$bed,$status){
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
       
       
        if($ward <> null){
            
            $where .=" AND wardid='$ward'"; 
        }
        
        if($bed <> null){
            
            $where .=" AND bedid='$bed'"; 
        }
        
        if($status <> null){
            
            $where .=" AND status LIKE '%$status%'"; 
        }
       
        return count($this->main->query("SELECT patientid,patientvisitid,admissionid,"
                                    . "status,wardid,bedid,admissiontype,admissionsource,"
                                    . "createdby,createdon,"
                                    . "dischargedby,dischargedon,dischargingdoctor,"
                                    . "dischargeinitiationtimestamp,dischargereason,dischargenotes "
                                    . "FROM patient_admission_details "
                                    . "WHERE id is not null $where")->result());
    }
    
    function admission_report($start,$end,$ward,$bed,$status,$page,$limit){
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
       
       
        if($ward <> null){
            
            $where .=" AND wardid='$ward'"; 
        }
        
        if($bed <> null){
            
            $where .=" AND bedid='$bed'"; 
        }
        
        if($status <> null){
            
            $where .=" AND status LIKE '%$status%'"; 
        }
   
        return $this->main->query("SELECT patientid,patientvisitid,admissionid,"
                                . "status,wardid,bedid,admissiontype,admissionsource,"
                                . "createdby,createdon,"
                                . "dischargedby,dischargedon,dischargingdoctor,"
                                . "dischargeinitiationtimestamp,dischargereason,dischargenotes "
                                . "FROM patient_admission_details "
                                . "WHERE id is not null $where "
                                . "ORDER BY createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function admissions($start,$end,$ward,$bed,$status){
        
        if($start <> null){
            
             $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND createdon <='$end 23:59:59'";
        }
       
       
        if($ward <> null){
            
            $where .=" AND wardid='$ward'"; 
        }
        
        if($bed <> null){
            
            $where .=" AND bedid='$bed'"; 
        }
        
        if($status <> null){
            
            $where .=" AND status LIKE '%$status%'"; 
        }
        
        return $this->main->query("SELECT patientid,patientvisitid,admissionid,"
                                    . "status,wardid,bedid,admissiontype,admissionsource,"
                                    . "createdby,createdon,"
                                    . "dischargedby,dischargedon,dischargingdoctor,"
                                    . "dischargeinitiationtimestamp,dischargereason,dischargenotes "
                                    . "FROM patient_admission_details "
                                    . "WHERE id is not null $where"
                                    . "ORDER BY createdon DESC ")->result();
    }
    
    function allocate_reallocate_nurse($data,$id){
        
        $this->main->update('inventory_unit_users',
                array('status'=>0,'lastupdate'=>date('Y-m-d H:i:s'),'lastupdater'=>$this->session->userdata('user_id')),
                array('userid'=>$id));//deactivate user
        $data['userid']=$id;
        return $this->main->insert('inventory_unit_users',$data);
    }
    
     function admission_transfer_report_count($start,$end,$ward,$bed,$status){
        
        if($start <> null){
            
             $where .=" AND transferedon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND transferedon <='$end 23:59:59'";
        }
       
       
        if($ward <> null){
            
            $where .=" AND wardid='$ward'"; 
        }
        
        if($bed <> null){
            
            $where .=" AND bedid='$bed'"; 
        }
        
        if($pid <> null){
            
            $where .=" AND patientid LIKE '%$pid%'"; 
        }
       
        return count($this->main->query("SELECT patientid,patientvisitid,admissionid,"
                                . "status,wardid,bedid,admissiontype,admissionsource,"
                                . "transferedby,transferedon,"
                                . "admittedby,admittedon "
                                . "FROM patient_admission_transfer_details "
                                . "WHERE id is not null $where")->result());
    }
    
    function admission_transfer_report($start,$end,$ward,$bed,$pid,$page,$limit){
        
        if($start <> null){
            
             $where .=" AND transferedon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND transferedon <='$end 23:59:59'";
        }
       
       
        if($ward <> null){
            
            $where .=" AND wardid='$ward'"; 
        }
        
        if($bed <> null){
            
            $where .=" AND bedid='$bed'"; 
        }
        
        if($pid <> null){
            
            $where .=" AND patientid LIKE '%$pid%'"; 
        }
   
        return $this->main->query("SELECT patientid,patientvisitid,admissionid,"
                                . "status,wardid,bedid,admissiontype,admissionsource,"
                                . "transferedby,transferedon,"
                                . "admittedby,admittedon "
                                . "FROM patient_admission_transfer_details "
                                . "WHERE id is not null $where "
                                . "ORDER BY transferedon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function admission_transfers($start,$end,$ward,$bed,$status){
        
        if($start <> null){
            
             $where .=" AND transferedon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND transferedon <='$end 23:59:59'";
        }
       
       
        if($ward <> null){
            
            $where .=" AND wardid='$ward'"; 
        }
        
        if($bed <> null){
            
            $where .=" AND bedid='$bed'"; 
        }
        
        if($pid <> null){
            
            $where .=" AND patientid LIKE '%$pid%'"; 
        }
        
        return $this->main->query("SELECT patientid,patientvisitid,admissionid,"
                                . "status,wardid,bedid,admissiontype,admissionsource,"
                                . "transferedby,transferedon,"
                                . "admittedby,admittedon "
                                . "FROM patient_admission_transfer_details "
                                . "WHERE id is not null $where "
                                . "ORDER BY transferedon DESC ")->result();
    }
}