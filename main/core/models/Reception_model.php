<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Reception_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);//load main database configuration
    }
    
    /*
     * name = get_patientid
     * description = generates a patient's id
     * @param patientType=the category a patient belongs to i.e walkin or permanent
     */
    function get_patientid($patientType){
        
       if($patientType == 1){
           
            $id=$this->main->query("SELECT patientidpermanent FROM patientids_control")->row();
           
            if($id <> null){
                
                $new_id=str_pad($id->patientidpermanent,7,'0', STR_PAD_LEFT);
                $this->main->update('patientids_control',array('patientidpermanent'=>($id->patientidpermanent+1),'pidpermlastupdate'=>date('Y-m-d H:i:s')));
            }
           
        }
        
        if($patientType == 2){
           
            $id=$this->main->query("SELECT patientidwalkin FROM patientids_control")->row();
            
            if($id <> null){
                
                $new_id=str_pad($id->patientidwalkin,7,'0',STR_PAD_LEFT);
                $this->main->update('patientids_control',array('patientidwalkin'=>($id->patientidwalkin+1),'pidwalklastupdate'=>date('Y-m-d H:i:s')));
            }
        }
        
        return $new_id;
    }
    
    /*
     * name = get_patientvisitid
     * description = generates a patient's visit id
     * @param patientType=the category a patient belongs to i.e walkin or permanent
     */
    function get_patientvisitid($patientType){
       if($patientType == 1){
           
            $id=$this->main->query("SELECT patientvisitpermanent FROM patientids_control")->row();
           
            if($id <> null){
                
                $new_id=str_pad($id->patientvisitpermanent,7,'0', STR_PAD_LEFT);
                $this->main->update('patientids_control',array('patientvisitpermanent'=>($id->patientvisitpermanent+1),'pvidpermlastupdate'=>date('Y-m-d H:i:s')));
            }
           
        }
        
        if($patientType == 2){
           
            $id=$this->main->query("SELECT patientvisitidwalkin FROM patientids_control")->row();
            
            if($id <> null){
                
                $new_id=str_pad($id->patientvisitidwalkin,7,'0',STR_PAD_LEFT);
                $this->main->update('patientids_control',array('patientvisitidwalkin'=>($id->patientvisitidwalkin+1),'pvidwalklastupdate'=>date('Y-m-d H:i:s')));
            }
        }
        
        return $new_id; 
    }
    
    function get_admissionid(){
        
        $id=$this->main->query("SELECT admissionid FROM patientids_control")->row();

        if($id <> null){

            $new_id=str_pad($id->admissionid,7,'0', STR_PAD_LEFT);
            $this->main->update('patientids_control',array('admissionid'=>($id->admissionid+1),'admissionidlastupdate'=>date('Y-m-d H:i:s')));
        }
           
       
        return $new_id; 
    }
    
    /*
     * name = patients
     * description = retrieves all the patients
     * @param id=patients unique database identifier
     */
    function patients($id){
        
        if($id<> null){
            $where .=" AND id='$id'";
        }
        return $this->main->query("SELECT id,patientid,name,"
                . "genderid,titleid,dob,"
                . "maritalid,tribe,email,"
                . "phone,region,district,"
                . "street,occupation,patienttype"
                . " FROM patients WHERE id!='0' $where ORDER BY name ASC")->result();
    }
    
    /*
     * name = patient
     * description = retrieves a patient
     * @param patientid=patient's id
     */
    function patient($patientid){
        
        return $this->main->query("SELECT id,patientid,name,"
                . "genderid,titleid,dob,"
                . "maritalid,tribe,email,"
                . "phone,region,district,"
                . "street,occupation,patienttype"
                . " FROM patients WHERE patientid ='$patientid' ORDER BY name ASC")->row();
    }
    
    /*
     * name = patient
     * description = retrieves a patient
     * @param patientid=patient's id
     */
    function patient_ids($patientid){
        
        if($patientid <> null){
            
            $where =" WHERE patientid  LIKE '%$patientid%'";
        }
        
        return $this->main->query("SELECT patientid "
                . " FROM patients $where ORDER BY patientid ASC")->result();
    }
    
    /*
     * name = get_transaction_id
     * description = retrieves a transaction id
     */
    function get_transaction_id(){
        
        $txn=$this->main->get('bill_transaction_ids')->row();
        
        if($txn <> null){
            
            $this->main->query("UPDATE bill_transaction_ids SET next=next+1,lastupdate='".date('Y-m-d H:i:s')."'");
            $txn=str_pad($txn->next,7,'0', STR_PAD_LEFT);
            return $txn;
        }
    }
    
    function get_postpaid_transaction_id(){
        
        $txn=$this->main->get('postpaid_bill_transaction_ids')->row();
        
        if($txn <> null){
            
            $this->main->query("UPDATE postpaid_bill_transaction_ids SET next=next+1,lastupdate='".date('Y-m-d H:i:s')."'");
            $txn=str_pad($txn->next,7,'0', STR_PAD_LEFT);
            return $txn;
        }
    }
    
    function patient_info_count($name,$gender,$marital,$phone){
        
        if($name <> null){
            
           $where .=" AND p.name LIKE '%$name%'"; 
        }
        
        if($gender <> null){
            
           $where .=" AND p.genderid='$gender'"; 
        }
        
        if($marital <> null){
            
           $where .=" AND p.maritalid= '$marital'"; 
        }
        
        if($phone <> null){
            
           $where .=" AND p.phone LIKE '%$phone%'"; 
        }
        
        return count($this->main->query("SELECT p.id,p.patientid,p.dob,p.name,p.phone,"
                . "p.patienttype,m.name as marital,"
                . "g.name as gender "
                . "FROM patients as p "
                . "INNER JOIN genders as g ON p.genderid=g.id "
                . "INNER JOIN marital_status as m ON p.maritalid=m.id  "
                . "WHERE p.id !='0' $where "
                . "ORDER BY p.name ASC")->result());
    }
    
    function patient_info($name,$gender,$marital,$phone,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND p.name LIKE '%$name%'"; 
        }
        
        if($gender <> null){
            
           $where .=" AND p.genderid='$gender'"; 
        }
        
        if($marital <> null){
            
           $where .=" AND p.maritalid= '$marital'"; 
        }
        
        if($phone <> null){
            
           $where .=" AND p.phone LIKE '%$phone%'"; 
        }
       
        return $this->main->query("SELECT p.id,p.patientid,p.dob,p.name,p.phone,"
                . "p.patienttype,m.name as marital,"
                . "g.name as gender "
                . "FROM patients as p "
                . "INNER JOIN genders as g ON p.genderid=g.id "
                . "INNER JOIN marital_status as m ON p.maritalid=m.id  "
                . "WHERE p.id !='0' $where "
                . "ORDER BY p.name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function all_patients($name,$gender,$marital,$phone){
        if($name <> null){
            
           $where .=" AND p.name LIKE '%$name%'"; 
        }
        
        if($gender <> null){
            
           $where .=" AND p.genderid='$gender'"; 
        }
        
        if($marital <> null){
            
           $where .=" AND p.maritalid= '$marital'"; 
        }
        
        if($phone <> null){
            
           $where .=" AND p.phone LIKE '%$phone%'"; 
        }
        
        return $this->main->query("SELECT p.id,p.patientid,p.dob,p.name,p.phone,"
                . "p.patienttype,m.name as marital,"
                . "g.name as gender "
                . "FROM patients as p "
                . "INNER JOIN genders as g ON p.genderid=g.id "
                . "INNER JOIN marital_status as m ON p.maritalid=m.id  "
                . "WHERE p.id !='0' $where "
                . "ORDER BY p.name ASC")->result();
    }
    
    function patient_registration($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('patients',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('patients',$data);
    }
    
    /*
     * name = walk_ins_patient_info_count
     * description = retrieves the total count of all the walk in patient visits
     * @param pid=patient id
     * @param name=patient name
     * @param start=start date
     * @param end=end date
     */
    function walk_ins_patient_info_count($pid,$name,$start,$end){
        
        if($pid <> null){
            
           $where .=" AND pv.patientid LIKE '%$pid%'"; 
        }
        
        if($name <> null){
            
           $where .=" AND p.name LIKE '%$name%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pv.createdon >='$start 00:00:00'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pv.createdon <= '$end 23:59:59'"; 
        }
        
        return count($this->main->query("SELECT pv.patientid,pv.patientvisitid,"
                . "pv.visitcategory,pv.assigneddoctor,pv.consultationstatus,"
                . "pv.sponsorid,pv.id,pv.createdon,pv.quenumber,p.name,p.id,vc.name as categ "
                . "FROM patients_visit as pv "
                . "INNER JOIN patients as p ON pv.patientid=p.patientid "
                . "INNER JOIN visit_categories as vc ON pv.visitcategory=vc.id  "
                . "WHERE pv.patientcategory ='".$this->config->item('walkin_patient_id')."' AND pv.exitstatus=0 $where "
                . "ORDER BY pv.id DESC")->result());
    }
    
     /*
     * name = walk_ins_patient_info
     * description = retrieves all the walk in patient visits
     * @param pid=patient id
     * @param name=patient name
     * @param start=start date
     * @param end=end date
     * @param page=display page
     * @param limit=limit record
     */
    function walk_ins_patient_info($pid,$name,$start,$end,$page,$limit){
        
        if($pid <> null){
            
           $where .=" AND pv.patientid LIKE '%$pid%'"; 
        }
        
        if($name <> null){
            
           $where .=" AND p.name LIKE '%$name%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pv.createdon >='$start 00:00:00'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pv.createdon <= '$end 23:59:59'"; 
        }
       
        return $this->main->query("SELECT pv.patientid,pv.patientvisitid,"
                . "pv.visitcategory,pv.assigneddoctor,pv.consultationstatus,"
                . "pv.sponsorid,pv.id,pv.createdon,pv.quenumber,p.name,p.id,vc.name as categ "
                . "FROM patients_visit as pv "
                . "INNER JOIN patients as p ON pv.patientid=p.patientid "
                . "INNER JOIN visit_categories as vc ON pv.visitcategory=vc.id  "
                . "WHERE pv.patientcategory ='".$this->config->item('walkin_patient_id')."' AND pv.exitstatus=0 $where "
                . "ORDER BY pv.id DESC "
                . "LIMIT $page,$limit")->result();
    } 
    
    /*
     * name = out_patient_info_count
     * description = retrieves the total count of all the out patients
     * @param pid=patient id
     * @param name=patient name
     * @param start=start date
     * @param end=end date
     */
    function out_patient_info_count($pid,$name,$start,$end){
        
        if($pid <> null){
            
           $where .=" AND pv.patientid LIKE '%$pid%'"; 
        }
        
        if($name <> null){
            
           $where .=" AND p.name LIKE '%$name%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pv.createdon >='$start 00:00:00'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pv.createdon <= '$end 23:59:59'"; 
        }
        
        return count($this->main->query("SELECT pv.patientid,pv.patientvisitid,"
                . "pv.visitcategory,pv.assigneddoctor,pv.consultationstatus,"
                . "pv.sponsorid,pv.id,pv.createdon,pv.quenumber,p.name,p.id,vc.name as categ "
                . "FROM patients_visit as pv "
                . "INNER JOIN patients as p ON pv.patientid=p.patientid "
                . "INNER JOIN visit_categories as vc ON pv.visitcategory=vc.id  "
                . "WHERE pv.patientcategory ='".$this->config->item('out_patient_id')."' AND pv.exitstatus=0 $where "
                . "ORDER BY pv.id DESC")->result());
    }
    
     /*
     * name = out_patient_info
     * description = retrieves all the out patients
     * @param pid=patient id
     * @param name=patient name
     * @param start=start date
     * @param end=end date
     * @param page=display page
     * @param limit=limit record
     */
    function out_patient_info($pid,$name,$start,$end,$page,$limit){
        
        if($pid <> null){
            
           $where .=" AND pv.patientid LIKE '%$pid%'"; 
        }
        
        if($name <> null){
            
           $where .=" AND p.name LIKE '%$name%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pv.createdon >='$start 00:00:00'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pv.createdon <= '$end 23:59:59'"; 
        }
       
        return $this->main->query("SELECT pv.patientid,pv.patientvisitid,"
                . "pv.visitcategory,pv.assigneddoctor,pv.consultationstatus,"
                . "pv.sponsorid,pv.id,pv.createdon,pv.quenumber,p.name,p.id,vc.name as categ "
                . "FROM patients_visit as pv "
                . "INNER JOIN patients as p ON pv.patientid=p.patientid "
                . "INNER JOIN visit_categories as vc ON pv.visitcategory=vc.id  "
                . "WHERE pv.patientcategory ='".$this->config->item('out_patient_id')."' AND pv.exitstatus=0 $where "
                . "ORDER BY pv.id DESC "
                . "LIMIT $page,$limit")->result();
    } 
    
    /*
     * name = out_patient_info_count
     * description = retrieves the total count of all the out patients
     * @param pid=patient id
     * @param name=patient name
     * @param start=start date
     * @param end=end date
     */
    function in_patient_info_count($pid,$name,$start,$end){
        
        if($pid <> null){
            
           $where .=" AND pv.patientid LIKE '%$pid%'"; 
        }
        
        if($name <> null){
            
           $where .=" AND p.name LIKE '%$name%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pv.createdon >='$start 00:00:00'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pv.createdon <= '$end 23:59:59'"; 
        }
        
        return count($this->main->query("SELECT pv.patientid,pv.patientvisitid,"
                . "pv.visitcategory,pv.assigneddoctor,pv.consultationstatus,"
                . "pv.sponsorid,pv.id,pv.createdon,pv.quenumber,p.name,p.id,vc.name as categ "
                . "FROM patients_visit as pv "
                . "INNER JOIN patients as p ON pv.patientid=p.patientid "
                . "INNER JOIN visit_categories as vc ON pv.visitcategory=vc.id  "
                . "WHERE pv.patientcategory ='".$this->config->item('in_patient_id')."' AND pv.exitstatus=0 $where "
                . "ORDER BY pv.id DESC")->result());
    }
    
     /*
     * name = out_patient_info
     * description = retrieves all the out patients
     * @param pid=patient id
     * @param name=patient name
     * @param start=start date
     * @param end=end date
     * @param page=display page
     * @param limit=limit record
     */
    function in_patient_info($pid,$name,$start,$end,$page,$limit){
        
        if($pid <> null){
            
           $where .=" AND pv.patientid LIKE '%$pid%'"; 
        }
        
        if($name <> null){
            
           $where .=" AND p.name LIKE '%$name%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pv.createdon >='$start 00:00:00'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pv.createdon <= '$end 23:59:59'"; 
        }
       
        return $this->main->query("SELECT pv.patientid,pv.patientvisitid,"
                . "pv.visitcategory,pv.assigneddoctor,pv.consultationstatus,"
                . "pv.sponsorid,pv.id,pv.createdon,pv.quenumber,p.name,p.id,vc.name as categ "
                . "FROM patients_visit as pv "
                . "INNER JOIN patients as p ON pv.patientid=p.patientid "
                . "INNER JOIN visit_categories as vc ON pv.visitcategory=vc.id  "
                . "WHERE pv.patientcategory ='".$this->config->item('in_patient_id')."' AND pv.exitstatus=0 $where "
                . "ORDER BY pv.id DESC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function appointments_info_count($pid,$name,$start,$end,$status){
        
        if($pid <> null){
            
           $where .=" AND pa.patientid LIKE '%$pid%'"; 
        }
        
        if($name <> null){
            
           $where .=" AND p.name LIKE '%$name%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pa.appointmentdate >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pa.appointmentdate <= '$end'"; 
        }
        
        if($status <> null){
            
           $where .=" AND pa.status= '$status'"; 
        }
        
        return count($this->main->query("SELECT pa.id,pa.patientid,pa.patientvisitid,"
                                . "pa.appointmentdoctor,pa.appointmentdate,"
                                . "pa.appointmenttime,pa.createdon,pa.createdby,"
                                . "pa.status,p.name "
                                . "FROM patients_appointments AS pa "
                                . "INNER JOIN patients as p ON pa.patientid=p.patientid "
                                . "WHERE pa.id is not null $where")->result());
    }
    
    function appointments_info($pid,$name,$start,$end,$status,$page,$limit){
        
        if($pid <> null){
            
           $where .=" AND pa.patientid LIKE '%$pid%'"; 
        }
        
        if($name <> null){
            
           $where .=" AND p.name LIKE '%$name%'"; 
        }
        
         if($start <> null){
            
           $where .=" AND pa.appointmentdate >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pa.appointmentdate <= '$end'"; 
        }
        
         if($status <> null){
            
           $where .=" AND pa.status= '$status'"; 
        }
        
        return $this->main->query("SELECT pa.id,pa.patientid,pa.patientvisitid,"
                                . "pa.appointmentdoctor,pa.appointmentdate,"
                                . "pa.appointmenttime,pa.createdon,pa.createdby,"
                                . "pa.status,p.name,p.id as pid "
                                . "FROM patients_appointments AS pa "
                                . "INNER JOIN patients as p ON pa.patientid=p.patientid "
                                . "WHERE pa.id is not null $where "
                                . "ORDER BY pa.createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function appointments($pid,$name,$start,$end,$status){
        
        if($pid <> null){
            
           $where .=" AND pa.patientid LIKE '%$pid%'"; 
        }
        
        if($name <> null){
            
           $where .=" AND p.name LIKE '%$name%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pa.appointmentdate >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pa.appointmentdate <= '$end'"; 
        }
        
        if($status <> null){
            
           $where .=" AND pa.status= '$status'"; 
        }
        
        return $this->main->query("SELECT pa.id,pa.patientid,pa.patientvisitid,"
                                . "pa.appointmentdoctor,pa.appointmentdate,"
                                . "pa.appointmenttime,pa.createdon,pa.createdby,"
                                . "pa.status,p.name,p.id as pid "
                                . "FROM patients_appointments AS pa "
                                . "INNER JOIN patients as p ON pa.patientid=p.patientid "
                                . "WHERE pa.id is not null $where "
                                . "ORDER BY pa.createdon DESC")->result();
    }
    
     /*
     * name = consultation_services
     * description = retrieves all the consultation active services
     * @param departmentid=department unique database identifier
     */
    function consultation_services($departmentid){
       
        return $this->main->query("SELECT id,name,departmentid,subdepartmentid,status FROM services WHERE departmentid ='$departmentid' AND status='1'")->result();
    }
    
    /*
     * name = non_consultation_services
     * description = retrieves all the non consultation active services
     * @param departmentid=department unique database identifier
     */
    function non_consultation_services($departmentid){
       
        return $this->main->query("SELECT id,name,departmentid,subdepartmentid,status FROM services WHERE departmentid !='$departmentid' AND status='1' ORDER BY subdepartmentid,name ASC")->result();
    }
    
    /*
     * name = non_consultation_services
     * description = retrieves all the non consultation active sub departments
     * @param departmentid=department unique database identifier
     */
    function non_counsulation_active_subdepartments($consultation,$pharmacy){
        
        if($id <> null){
            
            $where =" ";
            
        }
       
        return $this->main->query("SELECT id,name,code,departmentid,status "
                . "FROM sub_departments WHERE status='1' AND departmentid NOT IN ('$consultation','$pharmacy') "
                . "ORDER BY name ASC")->result();
    }
    
    /*
     * name = available_doctors
     * description = retrieve all the doctors on duty ready for consultation
     * @param doc_categ=doctor category
     * @param user_id=system user id
     */
    function available_doctors($doc_categ,$user_id){
        
        if($user_id <> null){
            
            $where =" AND ud.userid='$user_id'";
        }
        
        if($doc_categ <> null){
            
            $where =" AND ud.doctorcategory='$doc_categ'";
        }

        return $this->main->query("SELECT ud.doctorcategory,ud.availability,ud.status,ud.userid,ud.id,u.first_name,u.last_name FROM users_doctors as ud INNER JOIN users as u ON ud.userid=u.id WHERE ud.availability='1' $where ORDER BY u.first_name ASC")->result();
    }
    
    /*
     * name = consultation_services
     * description = retrieves all the consultation services
     * @param departmentid=department unique database identifier
     */
    function get_active_service_cost($subdepartment,$service,$sponsor){
       
        return $this->main->get_where('service_costs',array('subdepartmentid'=>$subdepartment,'serviceid'=>$service,'sponsorid'=>$sponsor,'status'=>1))->row();
    }
    
    /*
     * name = get_quenumber
     * description = retrieves the next que number
     * 
     */
    function  get_quenumber(){
        $dte=date('Y-m-d');
        $next=$this->main->get_where('patients_que_control',array('actiondate'=>$dte))->row();
        if($next <> null){
            
            $this->main->query("UPDATE patients_que_control SET next=next+1,lastupdate='".date('Y-m-d H:i:s')."' WHERE id='$next->id'");
            return $next->next;
        }
        $next=1;
        $array=array(
            'next'=>2,
            'lastupdate'=>date('Y-m-d H:i:s'),
            'actiondate'=>$dte
        );
        
        $this->main->insert('patients_que_control',$array);
        return $next;
    }
    
    /*
     * name = save_patient_visit
     * description = saves the patient visit record
     * @param data=array data with patient visit details
     */
    function save_patient_visit($data,$appointment){
        
        if($appointment){
            
            $this->main->update('patients_appointments',array('status'=>2),array('id'=>$appointment));
        }
        
        return $this->main->insert('patients_visit',$data);
    }
    
    /*
     * name = patient_visit_details
     * description = retrieves patient visit details
     * @param patientvisitid=patient's visit id
     */
    function patient_visit_details($patientvisitid){
        
        return $this->main->get_where('patients_visit',array('patientvisitid'=>$patientvisitid))->row();
    }
    
     /*
     * name = patient_visits
     * description = retrieves patient visits details
     * @param patientid=patient's id
     * @param patientvisitid=patient's visit id
     */
    function patient_visits($patientid,$patientvisitid,$from,$to){
        
        if($patientid <> null){
            
            $where .=" AND patientid='$patientid'";
        }
        
        if($patientvisitid <> null){
            
            $where .=" AND patientvisitid='$patientvisitid'";
        }
        
        if($from <> null){
            
            $where .=" AND createdon >='$from 00:00:00'";
        }
        
        if($to <> null){
            
            $where .=" AND createdon <='$to 23:59:59'";
        }
        
        return $this->main->query("SELECT patientid,patientvisitid,visitcategory,"
                                . "patientcategory,consultationcategory,assigneddoctor,"
                                . "consultationstatus,sponsorid,quenumber,exitstatus,createdon "
                                . "FROM patients_visit "
                                . "WHERE id is not null $where "
                                . "ORDER BY createdon DESC")->result();
    }
    
    /*
     * name = check_visit_validty
     * description = checks the validity of a visit for the creation of a next visit
     * @param patientid=patient's id
     */
    function check_visit_validty($patientid){
        
        $visit=$this->get_patients_latest_visit($patientid);
        
        if($visit->patientcategory == $this->config->item('walkin_patient_id')){
            return FALSE;
        }
        
        if(($visit->patientcategory == $this->config->item('in_patient_id')) && ($visit->exitstatus == 0)){
            return FALSE;
        }
        
        if(($visit->patientcategory == $this->config->item('out_patient_id')) && ($visit->exitstatus == 0)){
            return FALSE;
        }
        
        return TRUE;
    }
    
    /*
     * name = get_patients_latest_visit
     * description = retrieves the latest patient's visit information
     * @param patientid=patient's id
     */
    function get_patients_latest_visit($patientid){
        
        return $this->main->query("SELECT patientvisitid,patientid,visitcategory,"
                . "patientcategory,consultationcategory,assigneddoctor,"
                . "consultationstatus,sponsorid,quenumber,"
                . "exitstatus,createdon "
                . "FROM patients_visit "
                . "WHERE patientid='$patientid' "
                . "ORDER BY createdon DESC "
                . "LIMIT 0,1")->row();
    }
    
    /*
     * name = bill_transaction
     * description =retrieves transaction details for a particular transaction
     * @param transactionid=transaction id
     */
    function bill_transaction($transactionid,$postpaid_transactionid){
        
        if($transactionid <> null){
            return $this->main->get_where('patients_bill',array('transactionid'=>$transactionid))->result();
        }
        
        return $this->main->get_where('patients_bill',array('postpaid_transactionid'=>$postpaid_transactionid))->result();
    }
    
    function item_service_transaction($item,$transactionid,$postpaid_transactionid){
        
        if($transactionid <> null){
            
            return $this->main->get_where('patients_bill',array('transactionid'=>$transactionid,'item_service_id'=>$item))->row();
        }
        
        return $this->main->get_where('patients_bill',array('postpaid_transactionid'=>$postpaid_transactionid,'item_service_id'=>$item))->row();
    }
    
    function titles_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (shortname LIKE '%$name%' OR fullname LIKE '%$name%')"; 
        }
        
        
        return count($this->main->query("SELECT id,shortname,fullname,status FROM titles WHERE id !='0' $where "
                . "ORDER BY fullname ASC")->result());
    }
    
    function titles_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (shortname LIKE '%$name%' OR fullname LIKE '%$name%')"; 
        }
        
       
        return $this->main->query("SELECT id,shortname,fullname,status FROM titles WHERE id !='0' $where "
                . "ORDER BY fullname ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function titles($id){
        
        if($id <> null){
            
           $where .=" AND id='$id'"; 
        }
        
       
        return $this->main->query("SELECT id,shortname,fullname,status FROM titles WHERE id !='0' $where "
                . "ORDER BY fullname ASC")->result();
    } 
    
    function active_titles($id){
        
        if($id <> null){
            
           $where .=" AND id='$id'"; 
        }
        
       
        return $this->main->query("SELECT id,shortname,fullname,status FROM titles WHERE status='1' $where "
                . "ORDER BY fullname ASC")->result();
    } 
    
    function save_title($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('titles',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('titles',$data);
    }
    
    function activate_deactivate_title($status,$id){
        
                    $status=$status == 1?0:1;
            return  $this->main->update('titles',array('status'=>$status),array('id'=>$id));
    }
    
    function genders_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%')"; 
        }
        
        
        return count($this->main->query("SELECT id,name,description,status FROM genders WHERE id !='0' $where "
                . "ORDER BY name ASC")->result());
    }
    
    function genders_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%')"; 
        }
        
       
        return $this->main->query("SELECT id,name,description,status FROM genders WHERE id !='0' $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function genders($id){
        
        if($id <> null){
            
           $where .=" AND id='$id'"; 
        }
        
       
        return $this->main->query("SELECT id,name,description,status FROM genders WHERE id !='0' $where "
                . "ORDER BY name ASC")->result();
    } 
    
    function active_genders($id){
        
        if($id <> null){
            
           $where .=" AND id='$id'"; 
        }
        
       
        return $this->main->query("SELECT id,name,description,status FROM genders WHERE status='1' $where "
                . "ORDER BY name ASC")->result();
    } 
    
    function save_gender($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('genders',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('genders',$data);
    }
    
    function activate_deactivate_gender($status,$id){
        
                    $status=$status == 1?0:1;
            return  $this->main->update('genders',array('status'=>$status),array('id'=>$id));
    }
    
    function maritals_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%')"; 
        }
        
        
        return count($this->main->query("SELECT id,name,description,status FROM marital_status WHERE id !='0' $where "
                . "ORDER BY name ASC")->result());
    }
    
    function maritals_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%')"; 
        }
        
       
        return $this->main->query("SELECT id,name,description,status FROM marital_status WHERE id !='0' $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function maritals($id){
        
        if($id <> null){
            
           $where .=" AND id='$id'"; 
        }
        
       
        return $this->main->query("SELECT id,name,description,status FROM marital_status WHERE id !='0' $where "
                . "ORDER BY name ASC")->result();
    } 
    
    function active_maritals($id){
        
        if($id <> null){
            
           $where .=" AND id='$id'"; 
        }
        
       
        return $this->main->query("SELECT id,name,description,status FROM marital_status WHERE status='1' $where "
                . "ORDER BY name ASC")->result();
    } 
    
    function save_marital($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('marital_status',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('marital_status',$data);
    }
    
    function activate_deactivate_marital($status,$id){
        
                    $status=$status == 1?0:1;
            return  $this->main->update('marital_status',array('status'=>$status),array('id'=>$id));
    }
    
     function visit_categories_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%')"; 
        }
        
        
        return count($this->main->query("SELECT id,name,description,status FROM visit_categories WHERE id !='0' $where "
                . "ORDER BY name ASC")->result());
    }
    
    function visit_categories_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%')"; 
        }
        
       
        return $this->main->query("SELECT id,name,description,status FROM visit_categories WHERE id !='0' $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function visit_categories($id,$status){
        
        if($id <> null){
            
           $where .=" AND id='$id'"; 
        }
        
        if($status <> null){
            
            if($status == 2){
                $where .=" AND status='0'";
            }else{
                $where .=" AND status='$status'";
            }
            
        }
        
        return $this->main->query("SELECT id,name,description,status FROM visit_categories WHERE id !='0' $where "
                . "ORDER BY name ASC")->result();
    } 
    
    function active_visit_categories($id){
        
        if($id <> null){
            
           $where .=" AND id='$id'"; 
        }
        
       
        return $this->main->query("SELECT id,name,description,status FROM visit_categories WHERE status='1' $where "
                . "ORDER BY name ASC")->result();
    } 
    
    function save_visit_category($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('marital_status',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('visit_categories',$data);
    }
    
    function activate_deactivate_visit_category($status,$id){
        
                    $status=$status == 1?0:1;
            return  $this->main->update('visit_categories',array('status'=>$status),array('id'=>$id));
    }
    
    function regions($id){
        
        if($id <> null){
            $where .=" AND id='$id'";
        }
        return $this->main->query("SELECT id,name FROM location WHERE parent='0' $where ORDER BY name ASC")->result();
    }
    
    function districts($id,$parent){
        
        if($id <> null){
            $where .=" AND id='$id'";
        }
        
        if($parent <> null){
            $where .=" AND parent='$parent'";
        }
        
        return $this->main->query("SELECT id,name,parent FROM location WHERE parent!='0' $where ORDER BY name ASC")->result();
    }
    
    function process_bill($bill){
        
        return $this->main->insert('patients_bill',$bill);
    }
    
    function pending_orders($patientid,$patientvisitid){
        
        return $this->main->get_where('service_orders',array('patientid'=>$patientid,'patientvisitid'=>$patientvisitid,'status'=>0))->result();
    }
    
    function save_order($order){
        
        return $this->main->insert('service_orders',$order);
    }
    
     /*
     * name = remove_service_order
     * description = removes the service order previously added
     * @param id=order id
     */
    function remove_service_order($id){
        
        $this->main->delete('service_orders',array('id'=>$id));
    }
    
    /*
     * name = check_pending_order
     * description = check to see if ther is any pending srvice order for the patient
     * @param patientid=patient's id
     * @param patientvisitid=patient's visit id
     * @param serviceid=service id
     */
    function check_pending_order($patientid,$patientvisitid,$serviceid){
        
        $data=$this->main->query("SELECT id "
                . "FROM service_orders "
                . "WHERE patientid='$patientid' "
                . "AND patientvisitid='$patientvisitid' "
                . "AND serviceid='$serviceid' "
                . "AND status='0'")->row();
        
        return $data <>null?TRUE:FALSE;
    }
    
    function update_service_order($order,$patientid,$patientvisitid,$service){
        
        return $this->main->update('service_orders',$order,array('patientid'=>$patientid,'patientvisitid'=>$patientvisitid,'serviceid'=>$service,'status'=>0));
    }
    
    /*
     * name = credit_debit_prepaid_account
     * description = debit or credit a patient's prepaid account
     * @param data=credit or prepaid data
     */
    function credit_debit_prepaid_account($data){
        
                $this->main->query("UPDATE patients_prepaid_balance "
                        . "SET balance=balance+".$data['amount'].",lastupdate='".date('Y-m-d H:i:s')."' "
                        . "WHERE patientid='".$data['patientid']."'");
                
        return $this->main->insert('patients_prepaid_accounts',$data);
    }
    
    /*
     * name = credit_debit_package_account
     * description = debit or credit a patient's package account
     * @param data=credit or prepaid data
     */
    function credit_debit_package_account($data){
        
        return $this->main->insert('patients_package_accounts',$data);
    }
    
    /*
     * name = save_credit_bill
     * description = save credit bill details
     * @param data=credit bill data
     */
    function save_credit_bill($data){
        
        return $this->main->insert('patients_credit_bills',$data);
    }
    
    /*
     * name = save_postpaid_bill
     * description = save postpaid bill details
     * @param data=postpaid bill data
     */
    function save_postpaid_bill($data){
        
        return $this->main->insert('postpaid_bills',$data);
    }
    
    function patient_transactions_count($patientid,$start,$end,$sponsor){
        
        if($patientid <> null){
            
           $where .=" AND patientid LIKE '$patientid'"; 
        }
        
        if($start <> null){
            
           $where .=" AND b.createdon >='$start 00:00:00'"; 
        }
        
        if($end <> null){
            
           $where .=" AND b.createdon <='$end 23:59:59'"; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND b.sponsorid='$sponsor'"; 
        }
        
        return count($this->main->query("SELECT b.id,b.patientid,b.patientvisitid,b.transactionid,b.amount,b.service_item_id,b.paymentstatus,b.departmentid,"
                . "s.shortname,"
                . "p.name as pmode "
                . "FROM patients_bill as b "
                . "INNER JOIN sponsors as s ON b.sponsorid=s.sponsorcode  "
                . "INNER JOIN payment_modes as p ON b.paymentmodeid=p.paymentmodecode  "
                . "WHERE b.id !=0 $where")->result());
    }
    
    function patient_transactions($patientid,$start,$end,$sponsor,$page,$limit){
        
        if($patientid <> null){
            
           $where .=" AND patientid LIKE '$patientid'"; 
        }
        
        if($start <> null){
            
           $where .=" AND b.createdon >='$start 00:00:00'"; 
        }
        
        if($end <> null){
            
           $where .=" AND b.createdon <='$end 23:59:59'"; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND b.sponsorid='$sponsor'"; 
        }
       
        return $this->main->query("SELECT b.id,b.patientid,b.patientvisitid,b.transactionid,b.amount,b.createdon,b.service_item_id,b.paymentstatus,b.departmentid,"
                . "s.shortname,"
                . "p.name as pmode "
                . "FROM patients_bill as b "
                . "INNER JOIN sponsors as s ON b.sponsorid=s.sponsorcode  "
                . "INNER JOIN payment_modes as p ON b.paymentmodeid=p.paymentmodecode  "
                . "WHERE b.id !=0 $where "
                . "ORDER BY b.createdon DESC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function patient_transactions_details($patientid,$start,$end,$sponsor){
        
        if($patientid <> null){
            
           $where .=" AND patientid LIKE '$patientid'"; 
        }
        
        if($start <> null){
            
           $where .=" AND b.createdon >='$start 00:00:00'"; 
        }
        
        if($end <> null){
            
           $where .=" AND b.createdon <='$end 23:59:59'"; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND b.sponsorid='$sponsor'"; 
        }
       
        return $this->main->query("SELECT b.id,b.patientid,b.sponsorid,b.paymentmodeid,b.patientvisitid,b.transactionid,b.amount,b.createdon,b.service_item_id,b.paymentstatus,b.departmentid,b.subdepartmentid,"
                . "s.shortname,"
                . "p.name as pmode "
                . "FROM patients_bill as b "
                . "INNER JOIN sponsors as s ON b.sponsorid=s.sponsorcode  "
                . "INNER JOIN payment_modes as p ON b.paymentmodeid=p.paymentmodecode  "
                . "WHERE b.id !=0 $where "
                . "ORDER BY b.createdon DESC")->result();
    } 
    
    function transaction($id){
        
        return $this->main->get_where('patients_bill',array('id'=>$id))->row();
    }
    
    function patient_debtors_count($patientid,$start,$end){
        
        if($patientid <> null){
            
           $where .=" AND patientid LIKE '%$patientid%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND createdon >='$start 00:00:00'"; 
        }
        
        if($end <> null){
            
           $where .=" AND createdon <='$end 23:59:59'"; 
        }
        
        return count($this->main->query("SELECT DISTINCT patientid "
                . "FROM  postpaid_bills"
                . " WHERE status='0' $where "
                . "ORDER BY createdon DESC")->result());
    }
    
    function patient_debtors($patientid,$start,$end,$page,$limit){
        
        if($patientid <> null){
            
           $where .=" AND patientid LIKE '%$patientid%'"; 
        }
        
        if($start <> null){
            
           $where .=" AND createdon >='$start 00:00:00'"; 
        }
        
        if($end <> null){
            
           $where .=" AND createdon <='$end 23:59:59'"; 
        }
       
        return $this->main->query("SELECT DISTINCT patientid "
                . "FROM  postpaid_bills"
                . " WHERE status='0' $where "
                . "ORDER BY createdon DESC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function patient_debt_details($patientid){
        
        return $this->main->get_where("postpaid_bills",array('patientid'=>$patientid,'status'=>0))->result();
    }
    
    function clear_debt($bill_txn,$debt_bill_txn,$item_service_id,$postpaid_transactionid){
        
        $updt=$this->main->update('patients_bill',$bill_txn,array('service_item_id'=>$item_service_id,'postpaid_transactionid'=>$postpaid_transactionid));
        
        if($updt){
            
            return $this->main->update('postpaid_bills',$debt_bill_txn,array('service_item_id'=>$item_service_id,'transactionid'=>$postpaid_transactionid));
        }
    }
    
    function update_service_item_order($txnid,$postpaid_transactionid,$item_service_id,$boolean){
      
        if($boolean){
            
            return $this->main->update('service_orders',array('transactionid'=>$txnid),array('postpaid_transactionid'=>$postpaid_transactionid,'serviceid'=>$item_service_id));
        }
        
        return $this->main->update('patients_dispense_orders',array('transactionid'=>$txnid),array('postpaid_transactionid'=>$postpaid_transactionid,'itemid'=>$item_service_id));
    }
    
    function subscriptions($id){
        
        $where=null;
        if($id <> null){
            
            $where .=" AND subscriptionid='$id'";
        } 
        
        return $this->main->query("SELECT id,packageid,subscriptionid,transactionid,"
                                    . "patientid,amount,patientvisitid,"
                                    . "transactiontype,departmentid,subdepartmentid,"
                                    . "service_item_id,quantity,createdon "
                                    . "FROM patients_package_accounts "
                                    . "WHERE id!=0 $where "
                                    . "ORDER BY createdon DESC")->result();
    }
   
    function package_subscriptions($id){
        
        $where=null;
        if($id <> null){
            
            $where .=" AND subscriptionid='$id'";
        } 
        
        return $this->main->query("SELECT id,packageid,subscriptionid,transactionid,"
                                    . "patientid,amount,patientvisitid,"
                                    . "transactiontype,departmentid,subdepartmentid,"
                                    . "service_item_id,quantity,createdon "
                                    . "FROM patients_package_accounts "
                                    . "WHERE transactiontype LIKE 'CREDIT' $where "
                                    . "ORDER BY createdon DESC")->result();
    }
    
    function save_package_subscription($data,$pckgbalance){
        
               $this->main->insert('patients_package_balance',$pckgbalance);
        return $this->main->insert('patients_package_accounts',$data);
    }
    
    function save_prepaid_subscription($data,$prebalance,$balanceUpdate){
        
                if($balanceUpdate){
                    $this->main->query("UPDATE patients_prepaid_balance SET balance=balance+".$prebalance['balance'].",lastupdate='".date('Y-m-d H:i:s')."' WHERE patientid='".$prebalance['patientid']."'");
                }else{
                    $this->main->insert('patients_prepaid_balance',$prebalance);
                }
               
        return $this->main->insert('patients_prepaid_accounts',$data);
    }
    
    function prepaid_accounts($id){
        
        $where=null;
        if($id <> null){
            $where .=" AND subscriptionid='$id'";
        } 
        
        return $this->main->query("SELECT id,subscriptionid,transactionid,"
                                    . "patientid,amount,patientvisitid,"
                                    . "status,departmentid,subdepartmentid,"
                                    . "service_item_id,createdon "
                                    . "FROM patients_prepaid_accounts "
                                    . "WHERE id!=0 $where "
                                    . "ORDER BY createdon DESC")->result();
    }
   
    function prepaid_subscriptions($id,$txnid){
        
        $where=null;
        if($id <> null){
            
            $where .=" AND subscriptionid='$id'";
        } 
        
        if($txnid <> null){
            
            $where .=" AND transactionid='$txnid'";
        }
        
        return $this->main->query("SELECT id,subscriptionid,transactionid,"
                                    . "patientid,amount,patientvisitid,"
                                    . "status,departmentid,subdepartmentid,"
                                    . "service_item_id,createdon "
                                    . "FROM patients_prepaid_accounts "
                                    . "WHERE status LIKE 'CREDIT' $where "
                                    . "ORDER BY createdon DESC")->result();
    }
    
    function patient_packages_count($patientid,$subscriptionid,$packageid){
        
        if($patientid <> null){
            
           $where .=" AND patientid LIKE '%$patientid%'"; 
        }
        
        if($subscriptionid <> null){
            
           $where .=" AND subscriptionid LIKE '%$subscriptionid%'"; 
        }
        
        if($packageid <> null){
            
           $where .=" AND packageid='$packageid'"; 
        }
        
        return count($this->main->query("SELECT DISTINCT subscriptionid "
                . "FROM  patients_package_accounts"
                . " WHERE transactiontype ='CREDIT' $where "
                . "ORDER BY createdon DESC")->result());
    }
    
    function patient_packages($patientid,$subscriptionid,$packageid,$page,$limit){
        
        if($patientid <> null){
            
           $where .=" AND patientid LIKE '%$patientid%'"; 
        }
        
        if($subscriptionid <> null){
            
           $where .=" AND subscriptionid LIKE '%$subscriptionid%'"; 
        }
        
        if($packageid <> null){
            
           $where .=" AND packageid='$packageid'"; 
        }
       
        return $this->main->query("SELECT DISTINCT subscriptionid "
                . "FROM  patients_package_accounts"
                . " WHERE transactiontype ='CREDIT' $where "
                . "ORDER BY createdon DESC "
                . "LIMIT $page,$limit")->result();
    }
    
    function patient_package_status($subscriptionid,$patientid,$package,$status){
        
         if($subscriptionid <> null){
            
            $where .=" AND subscriptionid='$subscriptionid'";
        }
        
        if($patientid <> null){
            
            $where .=" AND patientid='$patientid'";
        }
        
        if($package <> null){
            
            $where .=" AND packageid='$package'";
        }
        
        if($status <> null){
            
            $where .=" AND status='$status'";
        }
        
        return $this->main->query("SELECT subscriptionid,packageid,status,lastupdate FROM patients_package_balance WHERE subscriptionid is not null $where ORDER BY lastupdate DESC")->result();
    }
    
    function patient_package_item_status($department,$subdepartment,$service,$subscriptionid){
        
        return $this->main->query("SELECT SUM(quantity) as quantity "
                . "FROM patients_package_accounts "
                . "WHERE departmentid='$department' AND "
                . "subdepartmentid='$subdepartment' AND "
                . "service_item_id='$service' AND "
                . "subscriptionid='$subscriptionid'")->row();
        
        
    }
    
    function patients_prepaid_accounts_count($patientid,$subscriptionid){
        
        if($patientid <> null){
            
           $where .=" AND patientid LIKE '%$patientid%'"; 
        }
        
        if($subscriptionid <> null){
            
           $where .=" AND subscriptionid LIKE '%$subscriptionid%'"; 
        }
        
        return count($this->main->query("SELECT DISTINCT subscriptionid "
                . "FROM  patients_prepaid_accounts"
                . " WHERE status ='CREDIT' $where "
                . "ORDER BY createdon DESC")->result());
    }
    
    function patients_prepaid_accounts($patientid,$subscriptionid,$page,$limit){
        
        if($patientid <> null){
            
           $where .=" AND patientid LIKE '%$patientid%'"; 
        }
        
        if($subscriptionid <> null){
            
           $where .=" AND subscriptionid LIKE '%$subscriptionid%'"; 
        }
        
       
        return $this->main->query("SELECT DISTINCT subscriptionid "
                . "FROM  patients_prepaid_accounts"
                . " WHERE status ='CREDIT' $where "
                . "ORDER BY createdon DESC "
                . "LIMIT $page,$limit")->result();
    }
    
    function patient_prepaid_status($subscriptionid,$patientid,$status){
        
        if($subscriptionid <> null){
            
            $where .=" AND subscriptionid='$subscriptionid'";
        }
        
        if($patientid <> null){
            
            $where .=" AND patientid='$patientid'";
        }
        
        if($status <> null){
            
            $where .=" AND status='$status'";
        }
        
        return $this->main->query("SELECT patientid,subscriptionid,balance,status,lastupdate FROM patients_prepaid_balance WHERE patientid is not null $where")->row();
    }
    
    function general_collection($start,$end,$sponsor,$department,$subdepartment,$userid,$serviceid){
        
       
        if($start <> null){
            $where .=" AND createdon >='$start 00:00:00'";
            $whr .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            $where .=" AND createdon <='$end 23:59:59'";
            $whr .=" AND createdon <='$end 23:59:59'";
        }
        
        if($sponsor <> null){
            $where .=" AND sponsorid='$sponsor'";
        }
        
        if($department <> null){
            
            $where .=" AND departmentid='$department'";
        }
        
        if($subdepartment <> null){
            
            $where .=" AND subdepartmentid='$subdepartment'";
        }
        
        if($userid <> null){
            
            $where .=" AND createdby='$userid'";
            $whr .=" AND createdby='$userid'";
        }
       
        $service_payments=$this->main->query("SELECT patientid,patientvisitid,departmentid,subdepartmentid,sponsorid,transactionid,createdon,amount,service_item_id FROM patients_bill WHERE paymentstatus='1' $where ORDER BY createdon DESC")->result();
        $prepaid_subscriptions=$this->main->query("SELECT patientid,transactionid,createdon,amount FROM patients_prepaid_accounts WHERE status='CREDIT' $whr ORDER BY createdon DESC")->result();
        $package_subscriptions=$this->main->query("SELECT patientid,packageid,transactionid,createdon,amount FROM patients_package_accounts WHERE transactiontype='CREDIT' $whr ORDER BY createdon DESC")->result();
        
        return array('service_items'=>$service_payments,'prepaid_subscriptions'=>$prepaid_subscriptions,'package_subscriptions'=>$package_subscriptions);
    }
    
    function aggregated_collection($start,$end,$sponsor,$department,$subdepartment,$userid){
        
        if($start <> null){
            $where .=" AND createdon >='$start 00:00:00'";
            $whr .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            $where .=" AND createdon <='$end 23:59:59'";
            $whr .=" AND createdon <='$end 23:59:59'";
        }
        
        if($sponsor <> null){
            $where .=" AND sponsorid='$sponsor'";
        }
        
        if($department <> null){
            
            $where .=" AND departmentid='$department'";
        }
        
        if($subdepartment <> null){
            
            $where .=" AND subdepartmentid='$subdepartment'";
        }
        
        if($userid <> null){
            
            $where .=" AND createdby='$userid'";
            $whr .=" AND createdby='$userid'";
        }
        
        $service_payments=$this->main->query("SELECT SUM(amount) AS amount,departmentid,subdepartmentid FROM patients_bill WHERE paymentstatus='1' $where GROUP BY departmentid,subdepartmentid ORDER BY departmentid,subdepartmentid ASC")->result();
        $prepaid_subscriptions=$this->main->query("SELECT SUM(amount) AS amount,patientid FROM patients_prepaid_accounts WHERE status='CREDIT' $whr GROUP BY patientid ORDER BY patientid ASC")->result();
        $package_subscriptions=$this->main->query("SELECT SUM(amount) AS amount,packageid FROM patients_package_accounts WHERE transactiontype='CREDIT' $whr GROUP BY packageid ORDER BY packageid DESC")->result();
    
        return array('service_items'=>$service_payments,'prepaid_subscriptions'=>$prepaid_subscriptions,'package_subscriptions'=>$package_subscriptions);
    }
   
    function check_out_patient($patientid,$patientvisitid,$data){
        
        return $this->main->update("patient_admission_details",$data,array('patientid'=>$patientid,'patientvisitid'=>$patientvisitid));
    }
    
    function update_patient_visit_status($patientvisitid,$data){
        
        return $this->main->update("patients_visit",$data,array('patientvisitid'=>$patientvisitid));
    }
    
    function outpatients_report_count($start,$end,$visitcategory,$consultationcategory){
        
        if($start <> null){
            
             $where .=" AND pv.createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND pv.createdon <='$end 23:59:59'";
        }
       
       
        if($visitcategory <> null){
            
            $where .=" AND pv.visitcategory='$visitcategory'"; 
        }
        
        if($consultationcategory <> null){
            
            $where .=" AND pv.consultationcategory='$consultationcategory'"; 
        }
        
       
        return count($this->main->query("SELECT pv.patientid,pv.patientvisitid,"
                . "pv.visitcategory,pv.assigneddoctor,pv.consultationstatus,"
                . "pv.sponsorid,pv.id,pv.createdon,pv.consultationcategory,"
                . "pv.quenumber,p.name,p.id,vc.name as categ "
                . "FROM patients_visit as pv "
                . "INNER JOIN patients as p ON pv.patientid=p.patientid "
                . "INNER JOIN visit_categories as vc ON pv.visitcategory=vc.id  "
                . "WHERE pv.patientcategory ='".$this->config->item('out_patient_id')."' $where ")->result());
    }
    
    function outpatients_report($start,$end,$visitcategory,$consultationcategory,$page,$limit){
        
        if($start <> null){
            
             $where .=" AND pv.createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND pv.createdon <='$end 23:59:59'";
        }
       
       
        if($visitcategory <> null){
            
            $where .=" AND pv.visitcategory='$visitcategory'"; 
        }
        
        if($consultationcategory <> null){
            
            $where .=" AND pv.consultationcategory='$consultationcategory'"; 
        }
       
        return $this->main->query("SELECT pv.patientid,pv.patientvisitid,"
                . "pv.visitcategory,pv.assigneddoctor,pv.consultationstatus,"
                . "pv.sponsorid,pv.id,pv.createdon,pv.consultationcategory,"
                . "pv.quenumber,p.name,p.id,vc.name as categ "
                . "FROM patients_visit as pv "
                . "INNER JOIN patients as p ON pv.patientid=p.patientid "
                . "INNER JOIN visit_categories as vc ON pv.visitcategory=vc.id  "
                . "WHERE pv.patientcategory ='".$this->config->item('out_patient_id')."' $where "
                . "ORDER BY pv.createdon DESC "
                . "LIMIT $page,$limit")->result();
    }
   
    function outpatients($start,$end,$visitcategory,$consultationcategory){
        
        if($start <> null){
            
             $where .=" AND pv.createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND pv.createdon <='$end 23:59:59'";
        }
       
       
        if($visitcategory <> null){
            
            $where .=" AND pv.visitcategory='$visitcategory'"; 
        }
        
        if($consultationcategory <> null){
            
            $where .=" AND pv.consultationcategory='$consultationcategory'"; 
        }
        
       
        return $this->main->query("SELECT pv.patientid,pv.patientvisitid,"
                . "pv.visitcategory,pv.assigneddoctor,pv.consultationstatus,"
                . "pv.sponsorid,pv.id,pv.createdon,pv.consultationcategory,"
                . "pv.quenumber,p.name,p.id,vc.name as categ "
                . "FROM patients_visit as pv "
                . "INNER JOIN patients as p ON pv.patientid=p.patientid "
                . "INNER JOIN visit_categories as vc ON pv.visitcategory=vc.id  "
                . "WHERE pv.patientcategory ='".$this->config->item('out_patient_id')."' $where "
                . "ORDER BY pv.createdon DESC")->result();
    }
    
    function service_report_info_count($start,$end,$service){
        
        if($start <> null){
            
             $where .=" AND so.createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND so.createdon <='$end 23:59:59'";
        }
       
       
        if($service <> null){
            
            $where .=" AND so.serviceid='$service'"; 
        }
        
        
        
       
        return count($this->main->query("SELECT so.patientid,so.patientvisitid,"
                . "so.serviceid,so.departmentid,so.subdepartmentid,"
                . "so.quantity,so.id,so.status,so.investigationstatus,"
                . "so.createdby,so.createdon,s.name "
                . "FROM service_orders as so "
                . "INNER JOIN services as s ON so.serviceid=s.id "
                . "WHERE so.status='1' $where ")->result());
    }
    
    function service_report_info($start,$end,$service,$page,$limit){
        
        if($start <> null){
            
             $where .=" AND so.createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND so.createdon <='$end 23:59:59'";
        }
       
       
        if($service <> null){
            
            $where .=" AND so.serviceid='$service'"; 
        }
       
        return $this->main->query("SELECT so.patientid,so.patientvisitid,"
                . "so.serviceid,so.departmentid,so.subdepartmentid,"
                . "so.quantity,so.id,so.status,so.investigationstatus,"
                . "so.createdby,so.createdon,s.name "
                . "FROM service_orders as so "
                . "INNER JOIN services as s ON so.serviceid=s.id "
                . "WHERE so.status='1' $where "
                . "ORDER BY so.createdon DESC "
                . "LIMIT $page,$limit")->result();
    }
   
    function service_report($start,$end,$service){
        
        if($start <> null){
            
             $where .=" AND so.createdon >='$start 00:00:00'";
        }
        
        if($end <> null){
            
             $where .=" AND so.createdon <='$end 23:59:59'";
        }
       
       
        if($service <> null){
            
            $where .=" AND so.serviceid='$service'"; 
        }
        
       
        return $this->main->query("SELECT so.patientid,so.patientvisitid,"
                . "so.serviceid,so.departmentid,so.subdepartmentid,"
                . "so.quantity,so.id,so.status,so.investigationstatus,"
                . "so.createdby,so.createdon,s.name "
                . "FROM service_orders as so "
                . "INNER JOIN services as s ON so.serviceid=s.id "
                . "WHERE so.status='1' $where "
                . "ORDER BY so.createdon DESC")->result();
    }
}