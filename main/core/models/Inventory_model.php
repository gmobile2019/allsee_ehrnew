<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Inventory_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);//load main database configuration
    }
    
    function active_unit_categories($id){
        
        if($id <> null){
            $where =" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                                . "code,description,status "
                                . "FROM inventory_unit_categories WHERE status='1' $where "
                                . "ORDER BY name ASC")->result();
    }
    
    function active_item_categories($id,$code){
        
        if($id <> null){
            $where .=" AND id='$id'";
        }
        
        if($code <> null){
            $where .=" AND code='$code'";
        }
        
        return $this->main->query("SELECT id,name,"
                                . "code,description,status "
                                . "FROM inventory_item_categories WHERE status='1' $where "
                                . "ORDER BY name ASC")->result();
    }
    
    function get_unit_category_by_code($code){
        
        return $this->main->query("SELECT id,name,"
                                . "code,description,status "
                                . "FROM inventory_unit_categories WHERE code='$code'")->row();
    }
    
    function get_item_category_by_code($code){
        
         return $this->main->query("SELECT id,name,"
                                . "code,description,status "
                                . "FROM inventory_item_categories WHERE code='$code'")->row();
    }
    
    function units($id,$category){
        
        if($id <> null){
            
           $where .=" AND id='$id' "; 
        }
        
        if($category <> null){
            
           $where .=" AND category='$category' "; 
        }
        
        return $this->main->query("SELECT id,name,"
                . "category,description,status "
                . "FROM inventory_units WHERE id !=0 $where "
                . "ORDER BY category,name ASC")->result();
    }
    
    function active_units($id,$category){
        
        if($id <> null){
            
           $where .=" AND id='$id' "; 
        }
        
        if($category <> null){
            
           $where .=" AND category='$category' "; 
        }
        
        
        return $this->main->query("SELECT id,name,"
                . "category,description,status "
                . "FROM inventory_units WHERE status=1 $where "
                . "ORDER BY category,name ASC")->result();
    }
    
    function units_info_count($name,$category){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        if($category <> null){
            
           $where .=" AND category='$category' "; 
        }
        
        return count($this->main->query("SELECT id,name,"
                . "category,description,status "
                . "FROM inventory_units WHERE id !=0 $where "
                . "ORDER BY category,name ASC")->result());
    }
    
    function units_info($name,$category,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        if($category <> null){
            
           $where .=" AND category='$category' "; 
        }
        
        return $this->main->query("SELECT id,name,"
                . "category,description,status "
                . "FROM inventory_units WHERE id !=0 $where "
                . "ORDER BY category,name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function save_unit($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('inventory_units',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('inventory_units',$data);
    }
    
    function activate_deactivate_unit($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('inventory_units',array('status'=>$status),array('id'=>$id));
    }
    
    function unit_name_check($name,$id){
        
        if($id <> null){
            return $this->main->query("SELECT id FROM inventory_units WHERE name='$name' AND id!='$id'")->row();
        }
        
        return $this->main->query("SELECT id FROM inventory_units WHERE name='$name'")->row();
    }
    
    function items_info_count($code,$name,$drug_type,$drug_grp){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        if($category <> null){
            
           $where .=" AND category='$category'"; 
        }
        
         if($drug_type <> null){
            
           $where .=" AND drug_type='$drug_type' "; 
        }
        
        if($drug_grp <> null){
            
           $where .=" AND drug_group='$drug_grp' "; 
        }
        
        return count($this->main->query("SELECT id,name,consumptioncategory,"
                . "drug_type,drug_group,category,status "
                . "FROM inventory_items WHERE category='$code' $where "
                . "ORDER BY name ASC")->result());
    }
    
    function items_info($code,$name,$drug_type,$drug_grp,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        if($category <> null){
            
           $where .=" AND category='$category'"; 
        }
        
        if($drug_type <> null){
            
           $where .=" AND drug_type='$drug_type' "; 
        }
        
        if($drug_grp <> null){
            
           $where .=" AND drug_group='$drug_grp' "; 
        }
        
        return $this->main->query("SELECT id,name,consumptioncategory,"
                . "drug_type,drug_group,category,status "
                . "FROM inventory_items WHERE category='$code' $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function inventory_items($code,$name,$type,$grp){
         if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        if($category <> null){
            
           $where .=" AND category='$category'"; 
        }
        
        if($drug_type <> null){
            
           $where .=" AND drug_type='$drug_type' "; 
        }
        
        if($drug_grp <> null){
            
           $where .=" AND drug_group='$drug_grp' "; 
        }
        
        return $this->main->query("SELECT id,name,consumptioncategory,"
                . "drug_type,drug_group,category,status "
                . "FROM inventory_items WHERE category='$code' $where "
                . "ORDER BY name ASC")->result();
    }
    
    function items($id){
        
        if($id <> null){
            
           $where .=" AND id='$id' "; 
        }
        
        
        return $this->main->query("SELECT id,name,drug_type,drug_group,consumptioncategory,"
                . "category,status "
                . "FROM inventory_items WHERE id !=0 $where "
                . "ORDER BY category,name ASC")->result();
    }
    
    function active_items($id){
        
        if($id <> null){
            
           $where .=" AND id='$id' "; 
        }
        
        
        return $this->main->query("SELECT id,name,drug_type,drug_group,consumptioncategory,"
                . "category,status "
                . "FROM inventory_items WHERE status =1 $where "
                . "ORDER BY category,name ASC")->result();
    }
    
    function active_selling_items($id){
        if($id <> null){
            
           $where .=" AND id='$id' "; 
        }
        
        
        return $this->main->query("SELECT id,name,drug_type,drug_group,consumptioncategory,"
                . "category,status "
                . "FROM inventory_items WHERE status =1 AND consumptioncategory=1 $where "
                . "ORDER BY category,name ASC")->result();
    }
    
    function drug_types_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        return count($this->main->query("SELECT id,name,"
                . "description,status "
                . "FROM drug_types WHERE id !=0 $where "
                . "ORDER BY name ASC")->result());
    }
    
    function drug_types_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        
        
        return $this->main->query("SELECT id,name,"
                . "description,status "
                . "FROM drug_types WHERE id !=0 $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function drug_groups_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        return count($this->main->query("SELECT id,name,"
                . "description,status "
                . "FROM drug_groups WHERE id !=0 $where "
                . "ORDER BY name ASC")->result());
    }
    
    function drug_groups_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        
        
        return $this->main->query("SELECT id,name,"
                . "description,status "
                . "FROM drug_groups WHERE id !=0 $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function drug_groups($id){
        
        if($id <> null){
            
            $where ="WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                . "status,description "
                . "FROM drug_groups $where "
                . "ORDER BY name ASC")->result();
    }
    
    function drug_types($id){
        
        if($id <> null){
            
            $where ="WHERE id='$id'";
        }
        return $this->main->query("SELECT id,name,"
                . "status,description "
                . "FROM drug_types $where "
                . "ORDER BY name ASC")->result();
    }
    
    function active_drug_groups($id){
        
        if($id <> null){
            
            $where =" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                . "status,description "
                . "FROM drug_groups WHERE status='1' $where "
                . "ORDER BY name ASC")->result();
    }
    
    function active_drug_types($id){
        
        if($id <> null){
            
            $where =" AND id='$id'";
        }
        return $this->main->query("SELECT id,name,"
                . "status,description "
                . "FROM drug_types WHERE status='1' $where "
                . "ORDER BY name ASC")->result();
    }
    
    function drug_group_name_check($name,$id){
        
        if($id <> null){
            return $this->main->query("SELECT id FROM drug_groups WHERE name='$name' AND id!='$id'")->row();
        }
        
        return $this->main->query("SELECT id FROM drug_groups WHERE name='$name'")->row();
    }
    
    function drug_type_name_check($name,$id){
        
        if($id <> null){
            return $this->main->query("SELECT id FROM drug_types WHERE name='$name' AND id!='$id'")->row();
        }
        
        return $this->main->query("SELECT id FROM drug_types WHERE name='$name'")->row();
    }
    
    function save_drug_type($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('drug_types',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('drug_types',$data);
    }
    
    function save_drug_group($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('drug_groups',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('drug_groups',$data);
    }
    
    function activate_deactivate_drugType($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('drug_types',array('status'=>$status),array('id'=>$id));
    }
    
    function activate_deactivate_drugGroup($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('drug_groups',array('status'=>$status),array('id'=>$id));
    }
    
    function item_name_check($name,$id){
        
        if($id <> null){
            
            return $this->main->query("SELECT id FROM inventory_items WHERE name='$name' AND id!='$id'")->row();
        }
        
        return $this->main->query("SELECT id FROM inventory_items WHERE name='$name'")->row();
    }
    
    function save_item($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('inventory_items',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('inventory_items',$data);
    }
    
    function get_inventory_item_by_name($name){
        
        return $this->main->query("SELECT id,name,status,drug_group,drug_type FROM inventory_items WHERE name LIKE '$name'")->row();
    }
    
    function activate_deactivate_item($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('inventory_items',array('status'=>$status),array('id'=>$id));
    }
    
    function suppliers_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        return count($this->main->query("SELECT id,name,"
                . "status,category,phone,email,physicaladdress "
                . "FROM inventory_suppliers $where "
                . "ORDER BY name ASC")->result());
    }
    
    function suppliers_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        
        
        return $this->main->query("SELECT id,name,"
                . "status,category,phone,email,physicaladdress "
                . "FROM inventory_suppliers $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function suppliers($id){
        
        if($id <> null){
            
            $where ="WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                . "status,category,phone,email,physicaladdress "
                . "FROM inventory_suppliers $where "
                . "ORDER BY name ASC")->result();
    }
    
    function active_suppliers($id,$code){
        
        if($id <> null){
            
            $where =" AND id='$id'";
        }
        
        if($code <> null){
            
            $where =" AND (category='$code' OR category is null)";
        }
        return $this->main->query("SELECT id,name,"
                . "status,category,phone,email,physicaladdress "
                . "FROM inventory_suppliers WHERE status='1' $where "
                . "ORDER BY name ASC")->result();
    }
    
     function supplier_name_check($name,$id){
        
        if($id <> null){
            return $this->main->query("SELECT id FROM inventory_suppliers WHERE name='$name' AND id!='$id'")->row();
        }
        
        return $this->main->query("SELECT id FROM inventory_suppliers WHERE name='$name'")->row();
    }
    
    function save_supplier($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('inventory_suppliers',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('inventory_suppliers',$data);
    }
    
    function activate_deactivate_supplier($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('inventory_suppliers',array('status'=>$status),array('id'=>$id));
    }
    
    function check_batch($batch){
        
        return $this->main->query("SELECT id FROM inventory_stock WHERE batch='$batch'")->row();
    }
    
    function process_stocking_destocking($stocking,$destocking){
        
        if($stocking <> null){
            
            $sv=$this->main->insert('inventory_stock',$stocking);
            if(!$sv){
                return FALSE;
            }
            
            $stk_id=$this->main->insert_id();
        }
        
        if($destocking <> null){
            
            $sv=$this->main->insert('inventory_stock',$destocking);
            
            if(!$sv){
                
                $this->main->delete('inventory_stock',array('id'=>$stk_id));
                return FALSE;
            }
        }
        
        return TRUE;
    }
    
     /*
     * name=inventory_stock
     * description=retrieves the stock quantity per batch to fullfill the required quantity of an item available in a unit
     * @param itemid=item identity
     * @param unit=unit identity
     * @parama quantity=the required quantity
     */
    function inventory_stock($unit,$itemid,$quantity){
        $data=array();
        $today=date('Y-m-d');
        $where =" AND inventoryunit is null";
        if($unit <> null){
            
            $where =" AND inventoryunit='$unit'";
        }
        
        
        $check_stock=$this->inventory_item_stock($itemid,$unit);
        
        if($check_stock->quantity < $quantity){
            
            return FALSE;
        }
        
        $batches=$this->main->query("SELECT DISTINCT storebatch,batch,expiredate,supplierid,unitprice FROM inventory_stock WHERE itemid='$itemid' AND status IN (".$this->config->item('stocking_status').") AND  inventoryunit is null AND expiredate>'$today' ORDER BY expiredate ASC")->result();
        
        foreach($batches as $key=>$value){
            
            
            $item_stock=$this->main->query("SELECT SUM(quantity) AS quantity FROM inventory_stock WHERE batch='$value->batch' $where")->row();
            
            if($item_stock->quantity > $quantity){
                
                $data[]=array(
                    'batch'=>$value->batch,
                    'storebatch'=>$value->storebatch,
                    'expiredate'=>$value->expiredate,
                    'supplierid'=>$value->supplierid,
                    'quantity'=>$quantity,
                    'totalvalue'=>($quantity*$value->unitprice),
                    'unitprice'=>$value->unitprice,
                );
                
                break;
            }
            
            if($item_stock->quantity == $quantity){
                
                $data[]=array(
                    'batch'=>$value->batch,
                    'storebatch'=>$value->storebatch,
                    'expiredate'=>$value->expiredate,
                    'supplierid'=>$value->supplierid,
                    'quantity'=>$quantity,
                    'totalvalue'=>($quantity*$value->unitprice),
                    'unitprice'=>$value->unitprice,
                );
                
                break;
            }
            
            if($item_stock->quantity < $quantity){
                
                $data[]=array(
                    'batch'=>$value->batch,
                    'storebatch'=>$value->storebatch,
                    'expiredate'=>$value->expiredate,
                    'supplierid'=>$value->supplierid,
                    'quantity'=>$item_stock->quantity,
                    'totalvalue'=>($item_stock->quantity*$value->unitprice),
                    'unitprice'=>$value->unitprice,
                );
            
                $quantity=$quantity-$item_stock->quantity;
            }
        }
        return $data;
    }
    
    /*
     * name=inventory_item_stock
     * description=retrieves the general total stock of an item available in a unit
     * @param itemid=item identity
     * @param unit=unit identity
     */
    function inventory_item_stock($itemid,$unit){
        
        $where =" AND inventoryunit is null";
        $today=date('Y-m-d');
        if($unit <> null){
            
            $where =" AND inventoryunit='$unit'";
        }
        return $this->main->query("SELECT SUM(quantity) AS quantity FROM inventory_stock WHERE itemid='$itemid' $where AND expiredate>'$today'")->row();
    }
    
     /*
     * name=inventory_item_stock
     * description=retrieves the general total stock of an item available in a unit
     * @param itemid=item identity
     * @param unit=unit identity
     */
    function units_inventory_item_stock($itemid,$unit){
        
        $today=date('Y-m-d');
        if($unit <> null){
            
            $where =" AND inventoryunit='$unit'";
        }
        return $this->main->query("SELECT SUM(quantity) AS quantity FROM inventory_stock WHERE itemid='$itemid' AND inventoryunit is not null $where AND expiredate>'$today'")->row();
    }
    
    function latest_stocking_details($itemid,$unit){
        
        $where =" AND inventoryunit is null";
        if($unit <> null){
            
            $where =" AND inventoryunit='$unit'";
        }
        
        $lateststocking=$this->main->query("SELECT DISTINCT storebatch,batch,expiredate,supplierid,unitprice,lastupdate FROM inventory_stock WHERE itemid='$itemid' AND status IN (".$this->config->item('stocking_status').") $where ORDER BY lastupdate DESC")->row();
        $latestdestocking=$this->main->query("SELECT DISTINCT storebatch,batch,expiredate,supplierid,unitprice,lastupdate FROM inventory_stock WHERE itemid='$itemid' AND status IN (".$this->config->item('destocking_status').") $where ORDER BY lastupdate DESC")->row();
        
        $array=array(
            'lasteststocking'=>$lateststocking->lastupdate,
            'latestdestocking'=>$latestdestocking->lastupdate,
        );
        return $array;
    }
    
    function item_inventory_details($itemid,$unit){
        $data=array();
        
        $where =" AND inventoryunit is null";
        if($unit <> null){
            
            $where =" AND inventoryunit='$unit'";
        }
        
        $batches=$this->main->query("SELECT DISTINCT storebatch,batch,expiredate,supplierid,unitprice FROM inventory_stock WHERE itemid='$itemid' AND status IN (".$this->config->item('stocking_status').") $where ORDER BY lastupdate ASC LIMIT 0,3")->result();
        
        foreach($batches as $key=>$value){
            
            $item_stock=$this->main->query("SELECT SUM(quantity) AS quantity FROM inventory_stock WHERE batch='$value->batch' $where")->row();
            $lateststocking=$this->main->query("SELECT DISTINCT batch,expiredate,supplierid,unitprice,lastupdate FROM inventory_stock WHERE batch='$value->batch' AND status IN (".$this->config->item('stocking_status').") $where ORDER BY lastupdate DESC")->row();
            $latestdestocking=$this->main->query("SELECT DISTINCT batch,expiredate,supplierid,unitprice,lastupdate FROM inventory_stock WHERE batch='$value->batch' AND status IN (".$this->config->item('destocking_status').") $where ORDER BY lastupdate DESC")->row();
            
            $data[]=array(
                   'batch'=>$value->batch,
                   'storebatch'=>$value->storebatch,
                   'expiredate'=>$value->expiredate,
                   'supplierid'=>$value->supplierid,
                   'quantity'=>$item_stock->quantity,
                   'totalvalue'=>($item_stock->quantity*$value->unitprice),
                   'unitprice'=>$value->unitprice,
                   'stocking'=>$lateststocking->lastupdate,
                   'destocking'=>$latestdestocking->lastupdate,
               );
        }
        
        return $data;
    }
    
    function check_store_batch($storebatch){
        
        return $this->main->query("SELECT id FROM inventory_stock WHERE storebatch LIKE '$storebatch'")->row();
    }
    
    function batches($itemid){
        
        if($itemid <> null){
            
            $where =" AND itemid='$itemid'";
        }
        return $this->main->query("SELECT DISTINCT storebatch,batch FROM inventory_stock WHERE  status IN (".$this->config->item('stocking_status').") AND  inventoryunit is null $where ORDER BY expiredate ASC")->result();
        
    }
    
    function get_batch_details($batch){
        
        return $this->main->query("SELECT storebatch,batch,expiredate,supplierid,unitprice,totalvalue,quantity,lastupdate,userid FROM inventory_stock WHERE  status IN (".$this->config->item('stocking_status').") AND  inventoryunit is null AND storebatch LIKE '$batch' ORDER BY lastupdate ASC")->row();
        
    }
    
    function check_disposal($itemid,$batch,$unit){
        
        if($unit <> null){
            $where=" AND inventoryunit='$unit'";
        }
        
        if($unit == null){
            $where=" AND inventoryunit is null";
        }
       
       return $this->main->query("SELECT SUM(quantity) AS qty FROM inventory_stock WHERE  itemid='$itemid' AND storebatch='$batch' $where")->row();
    }
    
    function process_stock_disposal($stocking,$disposal){
        
        if($stocking <> null){
            
            $sv=$this->main->insert('inventory_stock',$stocking);
            if(!$sv){
                return FALSE;
            }
            
            $stk_id=$this->main->insert_id();
        }
        
        if($disposal <> null){
            
            $sv=$this->main->insert('disposed_inventory',$disposal);
            
            if(!$sv){
                
                $this->main->delete('inventory_stock',array('id'=>$stk_id));
                return FALSE;
            }
        }
        
        return TRUE;
    }
    
    function expired_items($category,$item,$unit,$start,$end){
        $array=array();
        $item_whr=null;
        if($category <> null){
            $item_whr .=" AND category='$category'";
        }
        
        if($item <> null){
            $item_whr .=" AND name LIKE '%$item%'";
        }
        
        $today=date('Y-m-d');
        $items=$this->main->query("SELECT id,name,category FROM inventory_items WHERE id is not null $item_whr")->result();
        
        foreach($items as $key=>$value){
           
            $batch_whr=null;
            
            if($unit <> null){
                
                $batch_whr .=" AND inventoryunit='$unit'";
            }
            
            $batches=$this->main->query("SELECT DISTINCT storebatch,batch,expiredate,supplierid,unitprice,inventoryunit FROM inventory_stock WHERE itemid='$value->id' AND status IN (".$this->config->item('stocking_status').") $batch_whr ORDER BY lastupdate,inventoryunit ASC")->result();
        
            foreach($batches as $ky=>$val){
                
                $inv_stk_whr=null;
                
                if($val->inventoryunit <> null){
                
                    $inv_stk_whr .=" AND inventoryunit='$val->inventoryunit'";
                }else{
                    
                    $inv_stk_whr .=" AND inventoryunit is null";
                }
                
                
                if($start <> null){
                
                    $inv_stk_whr .=" AND expiredate >='$start'";
                }
                
                if($end <> null){
                
                    $inv_stk_whr .=" AND expiredate <='$end'";
                }
                
                $exp_qty=$this->main->query("SELECT SUM(quantity) as qty FROM inventory_stock WHERE storebatch='$val->storebatch' AND itemid='$value->id' AND expiredate <='$today' $inv_stk_whr")->row();
                
                if($exp_qty->qty > 0){
                    
                    $array[]=array(
                        'item'=>$value->name,
                        'category'=>$value->category,
                        'itemid'=>$value->id,
                        'batch'=>$val->storebatch,
                        'expiry'=>$val->expiredate,
                        'quantity'=>$exp_qty->qty,
                        'value'=>ceil($val->unitprice*$exp_qty->qty),
                        'unit'=>$val->inventoryunit,
                    );
                }
            }
        }
        
        return $array;
    }
    
    function stock_transfer_info_count($unit,$item,$start,$end){
        
        $where=null;
        if($unit <> null){
            
            if($unit == 'main'){
                
               $where .=" AND s.source_inventoryunit is null"; 
            }else{
                
                $where .=" AND s.source_inventoryunit='$unit'";
            }
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND s.lastupdate>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND s.lastupdate<= '$end 23:59:59'";
        }
        
        return count($this->main->query("SELECT s.itemid,s.storebatch,"
                . "s.quantity,s.lastupdate,s.source_inventoryunit,s.inventoryunit,"
                . "s.batch,it.name "
                . "FROM inventory_stock AS s INNER JOIN inventory_items as it "
                . "ON s.itemid=it.id "
                . "WHERE s.inventoryunit is not null "
                . "AND s.status IN (".$this->config->item('stocking_status').") $where ")->result());
    }
    
    function stock_transfer_info($unit,$item,$start,$end,$page,$limit){
        $where=null;
        if($unit <> null){
            
            if($unit == 'main'){
               $where .=" AND s.source_inventoryunit is null"; 
            }else{
                $where .=" AND s.source_inventoryunit='$unit'";
            }
            
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND s.lastupdate>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND s.lastupdate<= '$end 23:59:59'";
        }
        
        return $this->main->query("SELECT s.itemid,s.storebatch,"
                . "s.quantity,s.lastupdate,s.source_inventoryunit,s.inventoryunit,s.unitprice,"
                . "s.batch,it.name "
                . "FROM inventory_stock AS s INNER JOIN inventory_items as it "
                . "ON s.itemid=it.id "
                . "WHERE s.inventoryunit is not null "
                . "AND s.status IN (".$this->config->item('stocking_status').") $where "
                . "ORDER BY s.lastupdate ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function stock_tranfers_report($unit,$item,$start,$end){
        $where=null;
        if($unit <> null){
            
            if($unit == 'main'){
               $where .=" AND s.source_inventoryunit is null"; 
            }else{
                $where .=" AND s.source_inventoryunit='$unit'";
            }
            
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND s.lastupdate>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND s.lastupdate<= '$end 23:59:59'";
        }
        
        return $this->main->query("SELECT s.itemid,s.storebatch,"
                . "s.quantity,s.lastupdate,s.source_inventoryunit,s.inventoryunit,s.unitprice,"
                . "s.batch,it.name "
                . "FROM inventory_stock AS s INNER JOIN inventory_items as it "
                . "ON s.itemid=it.id "
                . "WHERE s.inventoryunit is not null "
                . "AND s.status IN (".$this->config->item('stocking_status').") $where "
                . "ORDER BY s.lastupdate ASC ")->result();
    }
    
    function stock_taking_info_count($unit,$item,$start,$end){
        
        $where=null;
        if($unit <> null){
            
            if($unit == 'main'){
                
               $where .=" AND s.inventoryunit is null"; 
            }else{
                $where .=" AND s.inventoryunit='$unit'";
            }
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND s.lastupdate>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND s.lastupdate<= '$end 23:59:59'";
        }
        
        return count($this->main->query("SELECT s.itemid,s.storebatch,s.supplierid,"
                . "s.quantity,s.lastupdate,s.source_inventoryunit,s.inventoryunit,"
                . "s.batch,it.name,it.category "
                . "FROM inventory_stock AS s INNER JOIN inventory_items as it "
                . "ON s.itemid=it.id "
                . "WHERE s.status IN (".$this->config->item('stocking_status').") $where ")->result());
    }
    
    function stock_taking_info($unit,$item,$start,$end,$page,$limit){
        $where=null;
        if($unit <> null){
            
            if($unit == 'main'){
               $where .=" AND s.inventoryunit is null"; 
            }else{
                $where .=" AND s.inventoryunit='$unit'";
            }
            
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND s.lastupdate>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND s.lastupdate<= '$end 23:59:59'";
        }
        
        return $this->main->query("SELECT s.itemid,s.storebatch,s.supplierid,"
                . "s.quantity,s.lastupdate,s.source_inventoryunit,s.inventoryunit,s.unitprice,"
                . "s.batch,it.name "
                . "FROM inventory_stock AS s INNER JOIN inventory_items as it "
                . "ON s.itemid=it.id "
                . "WHERE s.status IN (".$this->config->item('stocking_status').") $where "
                . "ORDER BY s.lastupdate ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function stock_takings_report($unit,$item,$start,$end){
        $where=null;
        if($unit <> null){
            
            if($unit == 'main'){
               $where .=" AND s.inventoryunit is null"; 
            }else{
                $where .=" AND s.inventoryunit='$unit'";
            }
            
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND s.lastupdate>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND s.lastupdate<= '$end 23:59:59'";
        }
        
        return $this->main->query("SELECT s.itemid,s.storebatch,s.supplierid,"
                . "s.quantity,s.lastupdate,s.source_inventoryunit,s.inventoryunit,s.unitprice,"
                . "s.batch,it.name "
                . "FROM inventory_stock AS s INNER JOIN inventory_items as it "
                . "ON s.itemid=it.id "
                . "WHERE s.status IN (".$this->config->item('stocking_status').") $where "
                . "ORDER BY s.lastupdate ASC ")->result();
    }
    
    function disposed_items_info_count($unit,$item,$start,$end){
        $where=null;
        if($unit <> null){
            
            if($unit == 'main'){
                
               $where .=" AND d.source is null"; 
            }else{
                $where .=" AND d.source='$unit'";
            }
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND d.actiondate>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND d.actiondate<= '$end 23:59:59'";
        }
        
        return count($this->main->query("SELECT d.itemid,d.storebatch,"
                . "d.quantity,d.actiondate,d.source,d.reason,"
                . "d.systembatch,it.name "
                . "FROM disposed_inventory AS d INNER JOIN inventory_items as it "
                . "ON d.itemid=it.id "
                . "WHERE d.id is not null $where ")->result());
    }
    
    function disposed_items_info($unit,$item,$start,$end,$page,$limit){
       $where=null;
        if($unit <> null){
            
            if($unit == 'main'){
                
               $where .=" AND d.source is null"; 
            }else{
                $where .=" AND d.source='$unit'";
            }
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND d.actiondate>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND d.actiondate<= '$end 23:59:59'";
        }
        
        return $this->main->query("SELECT d.itemid,d.storebatch,"
                . "d.quantity,d.actiondate,d.source,d.reason,"
                . "d.systembatch,it.name,it.category "
                . "FROM disposed_inventory AS d INNER JOIN inventory_items as it "
                . "ON d.itemid=it.id "
                . "WHERE d.id is not null $where "
                . "ORDER BY d.actiondate ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function disposed_items_report($unit,$item,$start,$end){
        $where=null;
        if($unit <> null){
            
            if($unit == 'main'){
                
               $where .=" AND d.source is null"; 
            }else{
                $where .=" AND d.source='$unit'";
            }
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND d.actiondate>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND d.actiondate<= '$end 23:59:59'";
        }
        
        return $this->main->query("SELECT d.itemid,d.storebatch,"
                . "d.quantity,d.actiondate,d.source,d.reason,"
                . "d.systembatch,it.name,it.category "
                . "FROM disposed_inventory AS d INNER JOIN inventory_items as it "
                . "ON d.itemid=it.id "
                . "WHERE d.id is not null $where "
                . "ORDER BY d.actiondate ASC ")->result();
    }
    
    function items_consumption_info_count($unit,$item,$start,$end){
        
        $where=null;
        if($unit <> null){
            
            
                $where .=" AND s.inventoryunit='$unit'";
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND s.lastupdate>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND s.lastupdate<= '$end 23:59:59'";
        }
       
        return count($this->main->query("SELECT s.itemid,s.storebatch,s.supplierid,"
                . "s.quantity,s.lastupdate,s.inventoryunit,"
                . "s.batch,it.name,it.category "
                . "FROM inventory_stock AS s INNER JOIN inventory_items as it "
                . "ON s.itemid=it.id "
                . "WHERE s.status='DISPENSE' $where ")->result());
    }
    
    function items_consumption_info($unit,$item,$start,$end,$page,$limit){
        $where=null;
        if($unit <> null){
            
             $where .=" AND s.inventoryunit='$unit'";
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND s.lastupdate>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND s.lastupdate<= '$end 23:59:59'";
        }
        
        return $this->main->query("SELECT s.itemid,s.storebatch,s.supplierid,"
                . "s.quantity,s.lastupdate,s.inventoryunit,s.unitprice,"
                . "s.batch,it.name "
                . "FROM inventory_stock AS s INNER JOIN inventory_items as it "
                . "ON s.itemid=it.id "
                . "WHERE s.status='DISPENSE' $where "
                . "ORDER BY s.lastupdate ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function items_consumption_report($unit,$item,$start,$end){
        $where=null;
        if($unit <> null){
            
            
                $where .=" AND s.inventoryunit='$unit'";
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND s.lastupdate>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND s.lastupdate<= '$end 23:59:59'";
        }
        
        return $this->main->query("SELECT s.itemid,s.storebatch,s.supplierid,"
                . "s.quantity,s.lastupdate,s.inventoryunit,s.unitprice,"
                . "s.batch,it.name "
                . "FROM inventory_stock AS s INNER JOIN inventory_items as it "
                . "ON s.itemid=it.id "
                . "WHERE s.status='DISPENSE' $where "
                . "ORDER BY s.lastupdate ASC ")->result();
    }
    
    function stock_reconcilliation($unit,$category,$item){
        $today=date('Y-m-d');
        $array=array();
        if($unit <> null){
           
            $invStk .=" AND inventoryunit='$unit'";
        }
        
        if($item <> null){
           
            $it .=" AND name LIKE '%$item%'";
        }
        
        if($category <> null){
            
            $it .=" AND category LIKE '$category'";
        }
        
        $invItems=$this->main->query("SELECT id,name,category FROM inventory_items WHERE id is not null $it ORDER BY name ASC")->result();
        
        foreach($invItems as $key=>$value){
            
            /*
             * item batches
             */
            $batches=$this->batches($value->id);
           
            foreach($batches as $ky=>$val){
                
                $batchDetails=$this->get_batch_details($val->storebatch);
                
                if($unit == null || $unit == 'main'){
                    /*
                    * main store
                    */
                  
                    //current stock (expired inclusive)
                    $all=$this->main->query("SELECT SUM(quantity) as qty "
                            . "FROM inventory_stock "
                            . "WHERE inventoryunit is null AND itemid='$value->id' AND storebatch LIKE '$val->storebatch'")->row();
                    
                    //current stock (expired exclusive)
                    $fresh=$this->main->query("SELECT SUM(quantity) as qty "
                            . "FROM inventory_stock "
                            . "WHERE inventoryunit is null AND itemid='$value->id' AND storebatch LIKE '$val->storebatch' AND expiredate > '$today'")->row();
                    
                    //current expired stock
                    $exp=$this->main->query("SELECT SUM(quantity) as qty "
                            . "FROM inventory_stock "
                            . "WHERE inventoryunit is null AND itemid='$value->id' AND storebatch LIKE '$val->storebatch' AND expiredate <= '$today'")->row();
                    
                    //disposed stock
                    $disp=$this->main->query("SELECT SUM(quantity) as qty "
                            . "FROM inventory_stock "
                            . "WHERE inventoryunit is null AND itemid='$value->id' AND storebatch LIKE '$val->storebatch' AND status IN (".$this->config->item('dispose_status').")")->row();
                
                    
                    $array[]=array(
                        'storebatch'=>$val->storebatch,
                        'unit'=>'Main Store',
                        'item'=>$value->name,
                        'category'=>$value->category,
                        'total_stock'=>$all->qty,
                        'total_value'=>ceil($all->qty*$batchDetails->unitprice),
                        'fresh_stock'=>$fresh->qty,
                        'fresh_value'=>ceil($fresh->qty*$batchDetails->unitprice),
                        'exp_stock'=>$exp->qty,
                        'exp_value'=>ceil($exp->qty*$batchDetails->unitprice),
                        'disp_stock'=>0-$disp->qty,
                        'disp_value'=>0-ceil($disp->qty*$batchDetails->unitprice),
                    );
                }
                
                if($unit == null || $unit <> 'main'){
                    /*
                    * units
                    */
                   
                    $units=$this->units($unit);
                    
                    foreach($units as $un=>$unt){
                        //current stock (expired inclusive)
                        $all=$this->main->query("SELECT SUM(quantity) as qty "
                                . "FROM inventory_stock "
                                . "WHERE inventoryunit='$unt->id' AND itemid='$value->id' AND storebatch LIKE '$val->storebatch'")->result();

                        //current stock (expired exclusive)
                        $fresh=$this->main->query("SELECT SUM(quantity) as qty "
                                . "FROM inventory_stock "
                                . "WHERE inventoryunit='$unt->id' AND itemid='$value->id' AND storebatch LIKE '$val->storebatch' AND expiredate > '$today' $invStk")->row();

                        //current expired stock
                        $exp=$this->main->query("SELECT SUM(quantity) as qty "
                                . "FROM inventory_stock "
                                . "WHERE inventoryunit='$unt->id' AND itemid='$value->id' AND storebatch LIKE '$val->storebatch' AND expiredate <= '$today' $invStk")->row();

                        //disposed stock
                        $disp=$this->main->query("SELECT SUM(quantity) as qty "
                                . "FROM inventory_stock "
                                . "WHERE inventoryunit='$unt->id' AND itemid='$value->id' AND storebatch LIKE '$val->storebatch' AND status IN (".$this->config->item('dispose_status').") $invStk")->row();


                         $array[]=array(
                            'storebatch'=>$val->storebatch,
                            'unit'=>$unt->name,
                            'item'=>$value->name,
                            'category'=>$value->category,
                            'total_stock'=>$all->qty,
                            'total_value'=>ceil($all->qty*$batchDetails->unitprice),
                            'fresh_stock'=>$fresh->qty,
                            'fresh_value'=>ceil($fresh->qty*$batchDetails->unitprice),
                            'exp_stock'=>$exp->qty,
                            'exp_value'=>ceil($exp->qty*$batchDetails->unitprice),
                            'disp_stock'=>0-$disp->qty,
                            'disp_value'=>0-ceil($disp->qty*$batchDetails->unitprice),
                        );
                    }
                     
                }
            }
        }
        return $array;
    }
    
    function stock_requesition_orders_info_count($unit,$item,$start,$end,$status){
        
        $where=null;
        if($unit <> null){
            
            if($unit == 'main'){
                
               $where .=" AND (ro.inventoryunit is null OR ro.supplyunit is null)"; 
            }else{
                
                $where .=" AND (ro.inventoryunit='$unit' OR ro.supplyunit='$unit')";
            }
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND ro.createdon>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND ro.createdon<= '$end 23:59:59'";
        }
        
        if($status <> null){
            
            $where .=" AND ro.status='$status'";
        }
        
        return count($this->main->query("SELECT ro.requesitionid,ro.itemid,ro.requestqty,"
                . "ro.supplyqty,ro.status,ro.createdon,ro.modifiedon,"
                . "ro.inventoryunit,ro.supplyunit,"
                . "it.name,it.category "
                . "FROM inventory_requesition_orders AS ro INNER JOIN inventory_items as it "
                . "ON ro.itemid=it.id "
                . "WHERE ro.requesitionid is not null $where")->result());
    }
    
    function stock_requesition_orders_info($unit,$item,$start,$end,$status,$page,$limit){
        $where=null;
       if($unit <> null){
            
            if($unit == 'main'){
                
               $where .=" AND (ro.inventoryunit is null OR ro.supplyunit is null)"; 
            }else{
                
                $where .=" AND (ro.inventoryunit='$unit' OR ro.supplyunit='$unit')";
            }
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND ro.createdon>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND ro.createdon<= '$end 23:59:59'";
        }
        
        if($status <> null){
            
            $where .=" AND ro.status='$status'";
        }
        
        return $this->main->query("SELECT ro.requesitionid,ro.itemid,ro.requestqty,"
                . "ro.supplyqty,ro.status,ro.createdon,ro.modifiedon,"
                . "ro.inventoryunit,ro.supplyunit,"
                . "it.name,it.category "
                . "FROM inventory_requesition_orders AS ro INNER JOIN inventory_items as it "
                . "ON ro.itemid=it.id "
                . "WHERE ro.requesitionid is not null $where "
                . "ORDER BY ro.createdon DESC "
                . "LIMIT $page,$limit")->result();
    }
    
    function stock_requesition_orders_report($unit,$item,$start,$end,$status){
        $where=null;
        if($unit <> null){
            
            if($unit == 'main'){
                
               $where .=" AND (ro.inventoryunit is null OR ro.supplyunit is null)"; 
            }else{
                
                $where .=" AND (ro.inventoryunit='$unit' OR ro.supplyunit='$unit')";
            }
        }
        
        if($item <> null){
            
            $where .=" AND it.name LIKE '%$item%'";
        }
        
        if($start <> null){
            
            $where .=" AND ro.createdon>= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND ro.createdon<= '$end 23:59:59'";
        }
        
        if($status <> null){
            
            $where .=" AND ro.status='$status'";
        }
        
        return $this->main->query("SELECT ro.requesitionid,ro.itemid,ro.requestqty,"
                . "ro.supplyqty,ro.status,ro.createdon,ro.modifiedon,"
                . "ro.inventoryunit,ro.supplyunit,"
                . "it.name,it.category "
                . "FROM inventory_requesition_orders AS ro INNER JOIN inventory_items as it "
                . "ON ro.itemid=it.id "
                . "WHERE ro.requesitionid is not null $where "
                . "ORDER BY ro.createdon DESC ")->result();
    }
    
    function stock_requesition_order($reqid,$itemid){
        
        return $this->main->query("SELECT ro.requesitionid,ro.itemid,ro.requestqty,"
                . "ro.supplyqty,ro.status,ro.createdon,ro.modifiedon,"
                . "ro.inventoryunit,ro.supplyunit,"
                . "it.name,it.category "
                . "FROM inventory_requesition_orders AS ro INNER JOIN inventory_items as it "
                . "ON ro.itemid=it.id "
                . "WHERE ro.requesitionid='$reqid' AND ro.itemid='$itemid'")->row();
    }
    
    function process_requesition_order($data,$reqid,$itemid){
        
        return $this->main->update('inventory_requesition_orders',$data,array('requesitionid'=>$reqid,'itemid'=>$itemid));
    }
}