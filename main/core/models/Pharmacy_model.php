<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Pharmacy_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);//load main database configuration
    }
    
    function item_orders($patientid,$patientvisitid,$orderid,$item,$status){
        if($patientid <> null){
            $where .=" AND patientid='$txn_id'";
        }
        
        if($patientvisitid <> null){
            
            $where .=" AND patientvisitid='$patientid'";
        }
        
        if($orderid <> null){
            
            $where .=" AND id='$orderid'";
        }
        
        if($itemid <> null){
            
            $where .=" AND itemid='$itemid'";
        }
        
        return $this->main->query("SELECT id,patientid,patientvisitid,"
                                . "dispenseinventoryunit,itemid,"
                                . "frequencyid,dosageid,days,"
                                . "quantity,transactionid,status "
                                . "FROM patients_dispense_orders WHERE id !='0' $where")->result();
    }
    
    function pending_dispense_orders($patientid,$patientvisitid){
        
        return $this->main->get_where('patients_dispense_orders',array('patientid'=>$patientid,'patientvisitid'=>$patientvisitid,'status'=>0))->result();
    }
    
    function check_pending_order($patientid,$patientvisitid,$itemid){
        
        $data=$this->main->query("SELECT id "
                . "FROM patients_dispense_orders "
                . "WHERE patientid='$patientid' "
                . "AND patientvisitid='$patientvisitid' "
                . "AND itemid='$itemid' "
                . "AND status='0'")->row();
        
        return $data <>null?TRUE:FALSE;
    }
    
    function save_dispense_order($data){
        
        return $this->main->insert('patients_dispense_orders',$data);
    }
    
    function remove_dispense_order($id){
        
        $this->main->delete('patients_dispense_orders',array('id'=>$id));
    }
    
    function process_destocking($destocking){
        
         return $this->main->insert_batch('inventory_stock',$destocking);
    }
    
    function update_dispense_order($dispense_data,$id){
        
        return $this->main->update('patients_dispense_orders',$dispense_data,array('id'=>$id));
    }
    
    function confirmed_item_dispense_order($txn_id,$postpaid_txn_id,$itemid){
        
        if($txn_id <> null){
            $where .=" AND transactionid='$txn_id'";
        }
        
        if($postpaid_txn_id <> null){
            $where .=" AND postpaid_transactionid='$postpaid_txn_id'";
        }
        
        if($itemid <> null){
            $where .=" AND itemid='$itemid'";
        }
        
        return $this->main->query("SELECT id,patientid,patientvisitid,"
                                . "dispenseinventoryunit,itemid,"
                                . "frequencyid,dosageid,days,"
                                . "quantity,transactionid,status "
                                . "FROM patients_dispense_orders WHERE status='1' $where")->result();
    }
    
    function drug_dosages_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        return count($this->main->query("SELECT id,name,"
                . "factor,status "
                . "FROM drug_dosages WHERE id !=0 $where "
                . "ORDER BY name ASC")->result());
    }
    
    function drug_dosages_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        
        
        return $this->main->query("SELECT id,name,"
                . "factor,status "
                . "FROM drug_dosages WHERE id !=0 $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function drug_dosages($id){
        
        if($id <> null){
            
            $where .=" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                . "factor,status "
                . "FROM drug_dosages WHERE id !=0 $where "
                . "ORDER BY name ASC ")->result();
    }
    
    function active_drug_dosages($id){
        
        if($id <> null){
            
            $where .=" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                . "factor,status "
                . "FROM drug_dosages WHERE status='1' $where "
                . "ORDER BY name ASC ")->result();
    }
    
    function drug_frequencies_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        return count($this->main->query("SELECT id,name,"
                . "factor,status "
                . "FROM drug_frequencies WHERE id !=0 $where "
                . "ORDER BY name ASC")->result());
    }
    
    function drug_frequencies_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        
        
        return $this->main->query("SELECT id,name,"
                . "factor,status "
                . "FROM drug_frequencies WHERE id !=0 $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function drug_frequencies($id){
        
        if($id <> null){
            
            $where .=" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                . "factor,status "
                . "FROM drug_frequencies WHERE id !=0 $where "
                . "ORDER BY name ASC ")->result();
    }
    
    function active_drug_frequencies($id){
        
        if($id <> null){
            
            $where .=" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                . "factor,status "
                . "FROM drug_frequencies WHERE status='1' $where "
                . "ORDER BY name ASC ")->result();
    }
    
    function drug_dosage_name_check($name,$id){
        
        if($id <> null){
            return $this->main->query("SELECT id FROM drug_dosages WHERE name='$name' AND id!='$id'")->row();
        }
        
        return $this->main->query("SELECT id FROM drug_dosages WHERE name='$name'")->row();
    }
    
    function drug_frequency_name_check($name,$id){
        
        if($id <> null){
            return $this->main->query("SELECT id FROM drug_frequencies WHERE name='$name' AND id!='$id'")->row();
        }
        
        return $this->main->query("SELECT id FROM drug_frequencies WHERE name='$name'")->row();
    }
    
    function save_drug_dosage($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('drug_dosages',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('drug_dosages',$data);
    }
    
    function save_drug_frequency($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('drug_frequencies',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('drug_frequencies',$data);
    }
    
    function activate_deactivate_drugDosage($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('drug_dosages',array('status'=>$status),array('id'=>$id));
    }
    
    function activate_deactivate_drugFrequency($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('drug_frequencies',array('status'=>$status),array('id'=>$id));
    }
    
    function check_requesitionid($reqid){
        
        return $this->main->query("SELECT id FROM inventory_requesition_orders WHERE requesitionid='$reqid'")->row();
    }
    
    function post_requesition_order($req_order){
        
        if($req_order <> null){
            
            $sv=$this->main->insert('inventory_requesition_orders',$req_order);
            if(!$sv){
                
                return FALSE;
            }
            
            $req_order=$this->main->insert_id();
        }
        
        return TRUE;
    }
}