<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['consultation_servicecode']='102';

$config['registration_servicecode']='101';

$config['pharmacy_servicecode']='105';

$config['out_patient_id']='1';

$config['walkin_patient_id']='2';

$config['in_patient_id']='3';

$config['OPD_visit_category_id']='1';

$config['instant_cash_code']='201';

$config['completed_payment_modes']=array(301,304,302,306,305);

$config['completed_prepaid_payment_modes']=array(302,305);

$config['mpesa_payment_modes']=304;

$config['prepaid_bill_pmode_code']=302;

$config['package_bill_pmode_code']=305;

$config['postpaid_bill_pmode_code']=303;

$config['instant_cash_bill_pmode_code']=301;

$config['drug_code']=601;

$config['stocking_status']="'STOCKING'";

$config['destocking_status']="'TRANSFER','DISPOSE','DISPENSE'";

$config['expiry_reason']="EXPIRY";

$config['dispose_status']="'DISPOSE'";

$config['multi_diagnosis_glue']='&&';

$config['ward_unit_category_code']='602';

$config['admission_status']=array(
                                'ADMITTED'=>'ADMITTED',
                                'PENDING DISCHARGE'=>'PENDING DISCHARGE',
                                'DISCHARGED'=>'DISCHARGED',
                            );

$config['patient_medical_file_detail_types']=array(
                                                    'physical_examinations'=>'PF001',
                                                    'vital_examinations'=>'PF002',
                                                    'diagnosis'=>'PF003',
                                                    'presciptions'=>'PF004',
                                                    'services'=>'PF005',
                                                    'nurse_notes'=>'PF006',
                                                    'doctor_notes'=>'PF007',
                                                    'allegies'=>'PF008',
                                                );

$config['expired_visit_date']=date('Y-m-d')." 00:00:00";

$config['expiry_hours']=2;


/*
 * patient appoitnments status
 * pending=1
 * cancelled=3
 * confirmed=2
 * expired=4
 */
