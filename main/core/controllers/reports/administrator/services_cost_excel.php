<?php
$this->load->library('Excel');
$this->excel->setActiveSheetIndex(0);
$i=1;
$this->excel->getActiveSheet()->setTitle('Services Costs');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Services Costs Report');
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$i +=2;

$this->excel->getActiveSheet()->setCellValue("A$i", 'Name');
$this->excel->getActiveSheet()->setCellValue("B$i", 'Department');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Sub Department');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Sponsor');
$this->excel->getActiveSheet()->setCellValue("E$i", 'Status');
$this->excel->getActiveSheet()->setCellValue("F$i", 'Cost');

$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);	

   //print_r($data);exit;
    $total_sale=0;
    $i++;
    foreach($data as $key=>$value){
        $dp=$this->Administration_model->departments($value->departmentid);
        $sub_dp=$this->Administration_model->subdepartments($value->subdepartmentid);
        $sp=$this->SuperAdministration_model->get_sponsor_by_code($value->sponsorid);
        $status=$value->status == 1?'Active':'Suspended';
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$value->name);
        $this->excel->getActiveSheet()->setCellValue('B'.$i,$dp[0]->name);
        $this->excel->getActiveSheet()->setCellValue('C'.$i,$sub_dp[0]->name);
        $this->excel->getActiveSheet()->setCellValue('D'.$i,$sp[0]->shortname);
        $this->excel->getActiveSheet()->setCellValue('E'.$i,$status);
        $this->excel->getActiveSheet()->setCellValue('F'.$i,$value->cost);
        
        //fomatting
        $this->excel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('#,##0.00');
        
        $i++;
    }
  
    $filename="Services Costs.xlsx";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    //ob_end_clean();
    //Excel2007
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
   $objWriter->save('php://output');
   exit;
