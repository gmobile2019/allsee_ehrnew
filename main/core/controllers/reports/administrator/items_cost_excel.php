<?php
$this->load->library('Excel');
$this->excel->setActiveSheetIndex(0);
$i=1;
$this->excel->getActiveSheet()->setTitle('Items Costs');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Items Costs Report');
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$i +=2;

if($min <> null){
    $this->excel->getActiveSheet()->setCellValue("A$i", 'Minimum Cost');
    $this->excel->getActiveSheet()->setCellValue("B$i", $min);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

if($max <> null){
    
    $this->excel->getActiveSheet()->setCellValue("A$i", 'Maximum Cost');
    $this->excel->getActiveSheet()->setCellValue("B$i", $max);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

$this->excel->getActiveSheet()->setCellValue("A$i", 'Item');
$this->excel->getActiveSheet()->setCellValue("B$i", 'Category');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Sponsor');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Status');
$this->excel->getActiveSheet()->setCellValue("E$i", 'Cost');

$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setBold(true);	

   //print_r($data);exit;
    $total_sale=0;
    $i++;
    foreach($data as $key=>$value){
        $ct=$this->Inventory_model->get_item_category_by_code($value->category);
        $sp=$this->SuperAdministration_model->get_sponsor_by_code($value->sponsorid);
        $status=$value->status == 1?'Active':'Suspended';
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$value->name);
        $this->excel->getActiveSheet()->setCellValue('B'.$i,$ct->name);
        $this->excel->getActiveSheet()->setCellValue('C'.$i,$sp[0]->shortname);
        $this->excel->getActiveSheet()->setCellValue('D'.$i,$status);
        $this->excel->getActiveSheet()->setCellValue('E'.$i,$value->cost);
        
        //fomatting
        $this->excel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('#,##0.00');
        
        $i++;
    }
  
    $filename="Items Costs.xlsx";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    //ob_end_clean();
    //Excel2007
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
   $objWriter->save('php://output');
   exit;
