<?php
$this->load->library('Excel');
$this->excel->setActiveSheetIndex(0);
$i=1;
$this->excel->getActiveSheet()->setTitle('Services');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Services Report');
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$i +=2;


$this->excel->getActiveSheet()->setCellValue("A$i", 'Name');
$this->excel->getActiveSheet()->setCellValue("B$i", 'Department');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Sub Department');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Status');

$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);	

    $i++;
    foreach($data as $key=>$value){
       $dp=$this->Administration_model->departments($value->departmentid);
        $sub_dp=$this->Administration_model->subdepartments($value->subdepartmentid);
        $status=$value->status == 1?'Active':'Suspended';
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$value->name);
        $this->excel->getActiveSheet()->setCellValue('B'.$i,$dp[0]->name);
        $this->excel->getActiveSheet()->setCellValue('C'.$i,$sub_dp[0]->name);
        $this->excel->getActiveSheet()->setCellValue('D'.$i,$status);
        
        //fomatting
        $this->excel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        
        $i++;
    }
  
    $filename="Services.xlsx";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    //ob_end_clean();
    //Excel2007
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
   $objWriter->save('php://output');
   exit;
