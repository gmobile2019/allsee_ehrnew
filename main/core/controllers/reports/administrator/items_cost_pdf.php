<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Items Costs Report</h3>';

if($min <> null){
    $html .= '<h3 align="left">Minimum Cost : '.$min.'</h3>';
}

if($max <> null){
    
    $html .= '<h3 align="left">Maximum Cost : '.$max.'</h3>';
}

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Item</b></td>
                    <td style="width:300pxtext-align:center;"><b> &nbsp;Category</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Sponsor</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Status</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Cost</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
    $ct=$this->Inventory_model->get_item_category_by_code($value->category);
    $sp=$this->SuperAdministration_model->get_sponsor_by_code($value->sponsorid);
    $status=$value->status == 1?'Active':'Suspended';
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' . $value->name . '</td>
                    <td>&nbsp;&nbsp;' . $ct->name.'</td>
                    <td>&nbsp;&nbsp;'.$sp[0]->shortname.'</td>
                    <td>&nbsp;&nbsp;'.$status.'</td>
                    <td align="right">'.number_format($value->cost,2).'&nbsp;&nbsp;</td>
                </tr>';
      }

$html.='</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Items Costs.pdf', 'D');
exit;
?>