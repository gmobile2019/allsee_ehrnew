<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Patient File</h3>';

    if($data <> null){
        foreach ($data as $key => $value) {

            $patient=$this->Reception_model->patient($value->patientid);
            $gender=$this->Reception_model->genders($patient->genderid);
            $adm_id=$this->Nursing_model->admission_details($value->patientid,$value->patientvisitid,null,null,null,null);
            $visitdate=explode(' ',$value->createdon);
            $html.='<p></p><table>
                    <tr>
                        <td style="width:300px;">&nbsp;&nbsp;Patient Id</td>
                        <td style="width:300px;"><b>&nbsp;&nbsp;'.$patientid.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:300px;">&nbsp;&nbsp;Name</td>
                        <td style="width:300px;"><b>&nbsp;&nbsp;'.$patient->name.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:300px;">&nbsp;&nbsp;Gender</td>
                        <td style="width:300px"><b>&nbsp;&nbsp;'.$gender[0]->name.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:300px">&nbsp;&nbsp;Date of Birth</td>
                        <td style="width:300px"><b>&nbsp;&nbsp;'.$patient->dob.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:300px">&nbsp;&nbsp;Mobile</td>
                        <td style="width:300px"><b>&nbsp;&nbsp;'.$patient->phone.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:300px">&nbsp;&nbsp;Email</td>
                        <td style="width:400px"><b>&nbsp;&nbsp;'.$patient->email.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:300px">&nbsp;&nbsp;Visit Date</td>
                        <td style="width:400px"><b>&nbsp;&nbsp;'.$visitdate[0].'</b></td>
                    </tr>
                </table>
                <p></p>';

            //physical examinations
            $physicalExamintaions=$this->Clinical_model->patient_physcial_examinations($value->patientid,$value->patientvisitid,null,null);
            
            if($physicalExamintaions <> null){
                
           
            $html .='<h4 align="center">Physical Examinations</h4>';
            
            
           $html.='<table border="1">
                    <tr>
                        <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                        <td style="width:330px;text-align:center"><b> &nbsp;Cardiovascular/S</b></td>
                        <td style="width:330px;text-align:center"><b> &nbsp;Respiratory/S</b></td>
                        <td style="width:330px;text-align:center;"><b> &nbsp;Abdomen</b></td>
                        <td style="width:330px;text-align:center"><b> &nbsp;Central Nervous/S</b></td>
                        <td style="width:330px;text-align:center"><b> &nbsp;Muscular Skeletal/S</b></td>
                        <td style="width:250px;text-align:center"><b> &nbsp;Examiner</b></td>
                        <td style="width:280px;text-align:center"><b> &nbsp;Timestamp</b></td>
                    </tr>';
           foreach ($physicalExamintaions as $ky => $val) {
            $examiner=$this->SuperAdministration_model->get_member_info($val->createdby);
            $examiner=$examiner[0]->first_name.' '.$examiner[0]->last_name;
            $html .='<tr>
                        <td>&nbsp;&nbsp;' . $i++ .'</td>
                        <td>&nbsp;&nbsp;' .$val->cardiovascularsystem . ' &nbsp; </td>
                        <td>&nbsp;&nbsp;' . $val->respiratorysystem.'</td>
                        <td>&nbsp;&nbsp;'.$val->abdomen.'</td>
                        <td>&nbsp;&nbsp;'.$val->centralnervoussystem.'</td>
                        <td>&nbsp;&nbsp;'.$val->muscularskeletalsystem.'</td>
                        <td>&nbsp;&nbsp;'.$examiner.'</td>
                        <td align="right">'.$val->createdon.'&nbsp;&nbsp;</td>
                    </tr>';
          }

        $html.='</table>';
        $html .='<br pagebreak="true"/>';
        }

        //vitals examinations
        $vitals=$this->Clinical_model->patient_vitals_examinations($value->patientid,$value->patientvisitid,null,null);
        
        if($vitals <> null){
            
        
        $html .='<p></p><h4 align="center">Vitals Examinations</h4>';
        
        $html.='<table border="1">
                    <tr>
                        <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                        <td style="width:170px;text-align:center"><b> &nbsp;Weight(m)</b></td>
                        <td style="width:170px;text-align:center"><b> &nbsp;Height(Kg)</b></td>
                        <td style="width:250px;text-align:center;"><b> &nbsp;Temperature(C)</b></td>
                        <td style="width:250px;text-align:center"><b> &nbsp;Pulse Rate(b/m)</b></td>
                        <td style="width:350px;text-align:center"><b> &nbsp;Blood Pressure(mmHg)</b></td>
                        <td style="width:320px;text-align:center"><b> &nbsp;Respiration Rate(c/m)</b></td>
                        <td style="width:150px;text-align:center"><b> &nbsp;SP02(%)</b></td>
                        <td style="width:250px;text-align:center"><b> &nbsp;Examiner</b></td>
                        <td style="width:280px;text-align:center"><b> &nbsp;Timestamp</b></td>
                    </tr>';

        foreach ($vitals as $ky => $val) {
            $examiner=$this->SuperAdministration_model->get_member_info($val->createdby);
            $examiner=$examiner[0]->first_name.' '.$examiner[0]->last_name;
            $html .='<tr>
                        <td>&nbsp;&nbsp;' . $i++ .'</td>
                        <td>&nbsp;&nbsp;' .$val->weight . ' &nbsp; </td>
                        <td>&nbsp;&nbsp;' . $val->height.'</td>
                        <td>&nbsp;&nbsp;'.$val->temperature.'</td>
                        <td>&nbsp;&nbsp;'.$val->pulserate.'</td>
                        <td>&nbsp;&nbsp;'.$val->bloodpressure.'</td>
                        <td>&nbsp;&nbsp;'.$val->respirationrate.'</td>
                        <td>&nbsp;&nbsp;'.$val->spo2.'</td>
                        <td>&nbsp;&nbsp;'.$examiner.'</td>
                        <td align="right">'.$val->createdon.'&nbsp;&nbsp;</td>
                    </tr>';
          }

    $html.='</table>';
    $html .='<br pagebreak="true"/>';
    }
    
    //diagnosis
    $diagnosis=$this->Clinical_model->patient_diagnosis($value->patientid,$value->patientvisitid);
    
    if($diagnosis <> null){
        
   
    $html .='<p></p><h4 align="center">Diagnosis</h4>';
    
     $html.='<table border="1">
                    <tr>
                        <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                        <td style="width:350px;text-align:center"><b> &nbsp;Complains</b></td>
                        <td style="width:400px;text-align:center"><b> &nbsp;Provisional Diagnosis</b></td>
                        <td style="width:350px;text-align:center;"><b> &nbsp;Final Diagnosis</b></td>
                        <td style="width:350px;text-align:center"><b> &nbsp;Comments</b></td>
                        <td style="width:350px;text-align:center"><b> &nbsp;Action Date</b></td>
                        <td style="width:350px;text-align:center"><b> &nbsp;Doctor</b></td>
                    </tr>';
     $i=1;
     foreach($diagnosis as $ky=>$val){
                        $doc1=$this->SuperAdministration_model->get_member_info($val->createdby);
                        $doc1=$doc1[0]->first_name.' '.$doc1[0]->last_name;

                        $doc2=$this->SuperAdministration_model->get_member_info($val->modifiedby);
                        $doc2=$doc2[0]->first_name.' '.$doc2[0]->last_name;
                        
                       $provisional=explode($this->config->item('multi_diagnosis_glue'),$val->provisionaldiagnosis);
                       $final=explode($this->config->item('multi_diagnosis_glue'),$val->finaldiagnosis);
                        $html .='<tr>
                            <td>&nbsp;&nbsp;'.$i++.'</td>
                            <td>&nbsp;&nbsp;'.$val->complains.'</td>
                            <td>
                                <ul>';


                                foreach($provisional as $v){
                                    $name=$this->Clinical_model->standard_complains(null,$v);
                                    $html.='<li>'.$name[0]->name.'</li>';
                                }
                                $html .='</ul>
                            </td>
                            <td>
                                <ul>';


                                foreach($final as $v){
                                    $name=$this->Clinical_model->standard_complains(null,$v);
                                    $html .='<li>'.$name[0]->name.'</li>';
                                }

                                $html .='</ul>
                            </td>
                            <td>&nbsp;&nbsp;'.$val->finalcomment.'</td>
                            <td>&nbsp;&nbsp;'.$val->createdon.'</td>
                            <td>&nbsp;&nbsp;'.$doc1.'</td>
                        </tr>'; 
                    }
        $html.='</table>';
         $html .='<br pagebreak="true"/>';
        }
        
        //prescriptions
        $prescriptions=$this->Clinical_model->patient_prescriptions($value->patientid,$value->patientvisitid,null,null,null);
        
        if($prescriptions <> null){
            
        
        $html .='<p></p><h4 align="center">Prescriptions</h4>';
        
        $html.='<table border="1">
                    <tr>
                        <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                        <td style="width:500px;text-align:center"><b> &nbsp;Item</b></td>
                        <td style="width:250px;text-align:center"><b> &nbsp;Frequency</b></td>
                        <td style="width:250px;text-align:center;"><b> &nbsp;Dosage</b></td>
                        <td style="width:250px;text-align:center"><b> &nbsp;No Days</b></td>
                        <td style="width:250px;text-align:center"><b> &nbsp;Quantity</b></td>
                        <td style="width250px;text-align:center"><b> &nbsp;Status</b></td>
                        <td style="width:350px;text-align:center"><b> &nbsp;Action Date</b></td>
                    </tr>';

        foreach ($prescriptions as $ky => $val) {
            $itm=$this->Inventory_model->items($val->itemid);
            $freq=$this->Pharmacy_model->drug_frequencies($val->frequencyid);
            $dosage=$this->Pharmacy_model->drug_dosages($val->dosageid);
            $stat=$value->status ==1?'Confirmed':'Pending';

            $html .='<tr>
                        <td>&nbsp;&nbsp;' . $i++ .'</td>
                        <td>&nbsp;&nbsp;' .$itm[0]->name. ' &nbsp; </td>
                        <td>&nbsp;&nbsp;' .$freq[0]->name.'</td>
                        <td>&nbsp;&nbsp;'.$dosage[0]->name.'</td>
                        <td>&nbsp;&nbsp;'.$value->days.'</td>
                        <td>&nbsp;&nbsp;'.$value->quantity.'</td>
                        <td>&nbsp;&nbsp;'.$stat.'</td>
                        <td>&nbsp;&nbsp;'.$value->createdon.'</td>
                    </tr>';
          }

    $html.='</table>';
    $html .='<br pagebreak="true"/>';
    }
    
    //services
     $srvs=$this->Clinical_model->patient_service_orders($value->patientid,$value->patientvisitid,null,null,null);
     
     if($srvs <> null){
         
     
        $html .='<p></p><h4 align="center">Services</h4>';
       
        $html.='<table border="1">
                    <tr>
                        <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                        <td style="width:350px;text-align:center"><b> &nbsp;Department</b></td>
                        <td style="width:350px;text-align:center;"><b> &nbsp;Sub Department</b></td>
                        <td style="width:350px;text-align:center"><b> &nbsp;Service</b></td>
                        <td style="width:350px;text-align:center"><b> &nbsp;Status</b></td>
                        <td style="width:350px;text-align:center"><b> &nbsp;Action Date</b></td>
                    </tr>';

        foreach ($srvs as $ky => $val) {
           $dp=$this->Administration_model->departments($val->departmentid);
            $sub_dp=$this->Administration_model->subdepartments($val->subdepartmentid);
            $srv=$this->Administration_model->services($val->serviceid);

            $html .='<tr>
                        <td>&nbsp;&nbsp;' . $i++ .'</td>
                        <td>&nbsp;&nbsp;' . $dp[0]->name.'</td>
                        <td>&nbsp;&nbsp;'.$sub_dp[0]->name.'</td>
                        <td>&nbsp;&nbsp;'.$srv[0]->name.'</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;'.$val->createdon.'</td>
                    </tr>';
          }

    $html.='</table>';
    $html .='<br pagebreak="true"/>';
    }
    
    //nurse notes
    $nurseNotes=$this->Nursing_model->inpatient_nurse_notes($value->patientid,$value->patientvisitid,null,null);
    if($nurseNotes <> null){
        
    
        $html .='<p></p><h4 align="center">Nurse Notes</h4>';
        

        $rounds=count($nurseNotes);
        foreach ($nurseNotes as $ky => $val) {
           $nurse=$this->SuperAdministration_model->get_member_info($val->createdby);
            $nurse=$nurse[0]->first_name.' '.$nurse[0]->last_name;

            $html.='<table border="1">
                    <tr>
                        <td style="width:2000px;text-align:center;" colspan="2"><b> Round : '.$rounds--.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Current Diagnosis</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->currentdiagnosis.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Treatment</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->treatment.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Procedures</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->procedures.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Medication</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->medications.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Medication Effect</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->medicationeffect.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Medication Comments</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->medicationcomments.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Intakes</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->intakes.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Outputs</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->outputs.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Overall Progress</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->overallprogress.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Remarks</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->remarks.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Timestamp</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->createdon.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Nurse</td>
                        <td style="width:1000px;"><b> &nbsp;'.$nurse.'</b></td>
                    </tr></table><p></p><p></p>';

          }

    $html.='</table>';
    $html .='<br pagebreak="true"/>';
    }
    
    //doctor  notes
    $docNotes=$this->Clinical_model->inpatient_doctor_notes($value->patientid,$value->patientvisitid,null,null);
    if($docNotes <> null){
        
   
    $html .='<p></p><h4 align="center">Doctor Notes</h4>';
    

        $rounds=count($docNotes);
        foreach ($docNotes as $ky => $val) {

            $doc=$this->SuperAdministration_model->get_member_info($val->createdby);
            $doc=$doc[0]->first_name.' '.$doc[0]->last_name;

            $html.='<table border="1">
                    <tr>
                        <td style="width:2000px;text-align:center;" colspan="2"><b> Round : '.$rounds--.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Overall Progress</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->overallprogress.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Complains</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->complains.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Recommendations</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->recommendations.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Timestamp</td>
                        <td style="width:1000px;"><b> &nbsp;'.$val->createdon.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:1000px;">&nbsp;Doctor</td>
                        <td style="width:1000px;"><b> &nbsp;'.$doc.'</b></td>
                    </tr></table><p></p>';

          }

        $html .='<br pagebreak="true"/>';
        }
        
        //investigation results
        $invResults=$this->Investigation_model->patient_investigation_tests($value->patientid,$value->patientvisitid,2,null);
        
        if($invResults <> null){
            
       
        $html .='<p></p><h4 align="center">Investigations</h4>';
        
        $html.='<table border="1">
                    <tr>
                        <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                        <td style="width:330px;text-align:center"><b> &nbsp;Service Name</b></td>
                        <td style="width:330px;text-align:center"><b> &nbsp;Test</b></td>
                        <td style="width:350px;text-align:center;"><b> &nbsp;Result</b></td>
                        <td style="width:330px;text-align:center"><b> &nbsp;Examiner</b></td>
                        <td style="width:330px;text-align:center"><b> &nbsp;Approver</b></td>
                        <td style="width:350px;text-align:center"><b> &nbsp;Timestamp</b></td>
                    </tr>';
            $i = 1;

        foreach ($invResults as $key => $value) {
            $examiner=$this->SuperAdministration_model->get_member_info($value->createdby);
            $examiner=$examiner[0]->first_name.' '.$examiner[0]->last_name;

            $approver=$this->SuperAdministration_model->get_member_info($value->approvedby);
            $approver=$approver[0]->first_name.' '.$approver[0]->last_name;
            $html .='<tr>
                        <td>&nbsp;&nbsp;' . $i++ .'</td>
                        <td>&nbsp;&nbsp;' .$value->name . ' &nbsp; </td>
                        <td>&nbsp;&nbsp;' . $value->test.'</td>
                        <td>&nbsp;&nbsp;'.$value->result.'</td>
                        <td>&nbsp;&nbsp;'.$examiner.'</td>
                        <td>&nbsp;&nbsp;'.$approver.'</td>
                        <td align="right">'.$value->createdon.'&nbsp;&nbsp;</td>
                    </tr>';
          }

     $html.='</table>';
     }
     
     $html .='<br pagebreak="true"/>';
    }
}

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Patient Medical File.pdf', 'D');
exit;
?>