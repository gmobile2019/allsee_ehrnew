<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 10);
//heading
$html  = '<h3 align="center">Patient Discharge Form</h3>';


$basicdetails=$this->Reception_model->patient($patientid);
$gender=$this->Reception_model->genders($basicdetails->genderid);

$adm=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'DISCHARGED');
$admType=$this->Nursing_model->admissiontypes($adm[0]->admissiontype);
$ward=$this->Inventory_model->units($adm[0]->wardid);
$bed=$this->Administration_model->ward_beds($adm[0]->bedid);

$dischargereason=preg_replace('/\s+/'," ", $adm[0]->dischargereason);
$dischargereason=str_replace("<br />","\n", $dischargereason);

$dischargenotes=preg_replace('/\s+/'," ", $adm[0]->dischargenotes);
$dischargenotes=str_replace("<br />","\n", $dischargenotes);

$doctor=$this->SuperAdministration_model->get_member_info($adm[0]->dischargingdoctor);
$nurse=$this->SuperAdministration_model->get_member_info($adm[0]->dischargedby);
    $html  .= '<p></p><h4 align="center">Basic Details</h4>';
    $html.='<table>
                <tr>
                    <td style="width:1500px;text-align:left">&nbsp;Patient id :&nbsp;&nbsp;<b>'.$patientid.'</b></td>
                    <td style="width:1500px;text-align:left">&nbsp;Name :&nbsp;&nbsp;<b>'.$basicdetails->name.'</b></td>
                </tr>
                <tr>
                    <td style="width:1500px;text-align:left">&nbsp;Gender :&nbsp;&nbsp;<b>'.$gender[0]->name.'</b></td>
                    <td style="width:1500px;text-align:left">&nbsp;Date of Birth :&nbsp;&nbsp;<b>'.$basicdetails->dob.'</b></td>
                </tr>
            </table>';
    $html  .= '<p></p><h4 align="center">Admission Details</h4>';
    $html.='<table>
            <tr>
                <td style="text-align:left">&nbsp;Admission id :&nbsp;&nbsp;<b>'.$admissionid.'</b></td>
                <td style="text-align:left">&nbsp;Admission Type :&nbsp;&nbsp;<b>'.$admType[0]->name.'</b></td>
            </tr>
            <tr>
                <td style="text-align:left">&nbsp;Ward :&nbsp;&nbsp;<b>'.$ward[0]->name.'</b></td>
                <td style="text-align:left">&nbsp;Bed :&nbsp;&nbsp;<b>'.$bed[0]->bedidentity.'</b></td>
            </tr>
        </table>';
        $html  .= '<p></p><h4 align="center">Discharge Details</h4>';
        $html.='<table>
                    <tr>
                        <td style="text-align:left">&nbsp;Discharge Reason</td>
                        <td style="text-align:left">&nbsp;<b>'.$dischargereason.'</b></td>
                    </tr>
                    <tr>
                        <td style="text-align:left">&nbsp;Discharge Notes</td>
                        <td style="text-align:left">&nbsp;<b>'.$dischargenotes.'</b></td>
                    </tr>
                </table>';
        $html  .= '<p></p><h4 align="left">'
                . '<span>Doctor</span>&nbsp;&nbsp;&nbsp;'
                . '<span>Name : '.$doctor[0]->first_name.' '.$doctor[0]->last_name.',</span>&nbsp;&nbsp;&nbsp;'
                . '<span>Signature _______________</span></h4>';
        $html  .= '<p></p><h4 align="left">'
                . '<span>Nurse</span>&nbsp;&nbsp;&nbsp;'
                . '<span>Name : '.$nurse[0]->first_name.' '.$nurse[0]->last_name.',</span>&nbsp;&nbsp;&nbsp;'
                . '<span>Signature _______________</span></h4>';
        $html  .= '<p></p><h4 align="left">'
                . '<span>Patient/Relative</span>&nbsp;&nbsp;&nbsp;'
                . '<span>Name : _________________,</span>&nbsp;&nbsp;&nbsp;'
                . '<span>Signature _______________</span></h4>';
$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Patient_Discharge_Form_'.$patientid.'.pdf', 'D');
exit;
?>