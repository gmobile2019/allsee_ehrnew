<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 7);
//heading
$html  = '<h3 align="center">Patient Physical Examinations</h3>';

$patient=$this->Reception_model->patient($patientid);
$gender=$this->Reception_model->genders($patient->genderid);

$html.='<table>
                <tr>
                    <td style="width:300px;">&nbsp;&nbsp;Patient Id</td>
                    <td style="width:300px;"><b>&nbsp;&nbsp;'.$patientid.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px;">&nbsp;&nbsp;Name</td>
                    <td style="width:300px;"><b>&nbsp;&nbsp;'.$patient->name.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px;">&nbsp;&nbsp;Gender</td>
                    <td style="width:300px"><b>&nbsp;&nbsp;'.$gender[0]->name.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Date of Birth</td>
                    <td style="width:300px"><b>&nbsp;&nbsp;'.$patient->dob.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Mobile</td>
                    <td style="width:300px"><b>&nbsp;&nbsp;'.$patient->phone.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Email</td>
                    <td style="width:400px"><b>&nbsp;&nbsp;'.$patient->email.'</b></td>
                </tr>';
            if($inpatient){
                $adm_id=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,null,null,null);
                $html .='<tr>
                            <td style="width:300px">&nbsp;&nbsp;Admission Id</td>
                            <td style="width:400px"><b>&nbsp;&nbsp;'.$adm_id[0]->admissionid.'</b></td>
                        </tr>';
            }
$html .='</table>';
$html .='<p></p>';

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Cardiovascular/S</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Respiratory/S</b></td>
                    <td style="width:330px;text-align:center;"><b> &nbsp;Abdomen</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Central Nervous/S</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Muscular Skeletal/S</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Examiner</b></td>
                    <td style="width:280px;text-align:center"><b> &nbsp;Timestamp</b></td>
                </tr>';
$i = 1;

    foreach ($data as $key => $value) {
        $examiner=$this->SuperAdministration_model->get_member_info($value->createdby);
        $examiner=$examiner[0]->first_name.' '.$examiner[0]->last_name;
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$value->cardiovascularsystem . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' . $value->respiratorysystem.'</td>
                    <td>&nbsp;&nbsp;'.$value->abdomen.'</td>
                    <td>&nbsp;&nbsp;'.$value->centralnervoussystem.'</td>
                    <td>&nbsp;&nbsp;'.$value->muscularskeletalsystem.'</td>
                    <td>&nbsp;&nbsp;'.$examiner.'</td>
                    <td align="right">'.$value->createdon.'&nbsp;&nbsp;</td>
                </tr>';
      }

$html.='</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Patient Physical Examinations.pdf', 'D');
exit;
?>