<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Patient Nurse Notes</h3>';

    $rounds=count($data);
    foreach ($data as $key => $value) {
        
        $patient=$this->Reception_model->patient($patientid);
        $gender=$this->Reception_model->genders($patient->genderid);
        $adm_id=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,null,null,null);

        $html.='<p></p><table>
                <tr>
                    <td style="width:300px;">&nbsp;&nbsp;Patient Id</td>
                    <td style="width:300px;"><b>&nbsp;&nbsp;'.$patientid.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px;">&nbsp;&nbsp;Name</td>
                    <td style="width:300px;"><b>&nbsp;&nbsp;'.$patient->name.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px;">&nbsp;&nbsp;Gender</td>
                    <td style="width:300px"><b>&nbsp;&nbsp;'.$gender[0]->name.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Date of Birth</td>
                    <td style="width:300px"><b>&nbsp;&nbsp;'.$patient->dob.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Mobile</td>
                    <td style="width:300px"><b>&nbsp;&nbsp;'.$patient->phone.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Email</td>
                    <td style="width:400px"><b>&nbsp;&nbsp;'.$patient->email.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Admission Id</td>
                    <td style="width:400px"><b>&nbsp;&nbsp;'.$adm_id[0]->admissionid.'</b></td>
                </tr>
            </table>
            <p></p>';

        $nurse=$this->SuperAdministration_model->get_member_info($value->createdby);
        $nurse=$nurse[0]->first_name.' '.$nurse[0]->last_name;
        
        $html.='<table border="1">
                <tr>
                    <td style="width:2000px;text-align:center;" colspan="2"><b> Round : '.$rounds--.'</b></td>
                </tr>
                <tr>
                    <td style="width:1000px;">&nbsp;Current Diagnosis</td>
                    <td style="width:1000px;"><b> &nbsp;'.$value->currentdiagnosis.'</b></td>
                </tr>
                <tr>
                    <td style="width:1000px;">&nbsp;Treatment</td>
                    <td style="width:1000px;"><b> &nbsp;'.$value->treatment.'</b></td>
                </tr>
                <tr>
                    <td style="width:1000px;">&nbsp;Procedures</td>
                    <td style="width:1000px;"><b> &nbsp;'.$value->procedures.'</b></td>
                </tr>
                <tr>
                    <td style="width:1000px;">&nbsp;Medication</td>
                    <td style="width:1000px;"><b> &nbsp;'.$value->medications.'</b></td>
                </tr>
                <tr>
                    <td style="width:1000px;">&nbsp;Medication Effect</td>
                    <td style="width:1000px;"><b> &nbsp;'.$value->medicationeffect.'</b></td>
                </tr>
                <tr>
                    <td style="width:1000px;">&nbsp;Medication Comments</td>
                    <td style="width:1000px;"><b> &nbsp;'.$value->medicationcomments.'</b></td>
                </tr>
                <tr>
                    <td style="width:1000px;">&nbsp;Intakes</td>
                    <td style="width:1000px;"><b> &nbsp;'.$value->intakes.'</b></td>
                </tr>
                <tr>
                    <td style="width:1000px;">&nbsp;Outputs</td>
                    <td style="width:1000px;"><b> &nbsp;'.$value->outputs.'</b></td>
                </tr>
                <tr>
                    <td style="width:1000px;">&nbsp;Overall Progress</td>
                    <td style="width:1000px;"><b> &nbsp;'.$value->overallprogress.'</b></td>
                </tr>
                <tr>
                    <td style="width:1000px;">&nbsp;Remarks</td>
                    <td style="width:1000px;"><b> &nbsp;'.$value->remarks.'</b></td>
                </tr>
                <tr>
                    <td style="width:1000px;">&nbsp;Timestamp</td>
                    <td style="width:1000px;"><b> &nbsp;'.$value->createdon.'</b></td>
                </tr>
                <tr>
                    <td style="width:1000px;">&nbsp;Nurse</td>
                    <td style="width:1000px;"><b> &nbsp;'.$nurse.'</b></td>
                </tr></table>';
                $html .='<br pagebreak="true"/>';
      }

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Patient Nurse Notes.pdf', 'D');
exit;
?>