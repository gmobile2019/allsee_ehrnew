<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 7);
//heading
$html  = '<h3 align="center">Patient Service Orders</h3>';

$patient=$this->Reception_model->patient($patientid);
$gender=$this->Reception_model->genders($patient->genderid);

$html.='<table>
                <tr>
                    <td style="width:300px;">&nbsp;&nbsp;Patient Id</td>
                    <td style="width:300px;"><b>&nbsp;&nbsp;'.$patientid.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px;">&nbsp;&nbsp;Name</td>
                    <td style="width:300px;"><b>&nbsp;&nbsp;'.$patient->name.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px;">&nbsp;&nbsp;Gender</td>
                    <td style="width:300px"><b>&nbsp;&nbsp;'.$gender[0]->name.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Date of Birth</td>
                    <td style="width:300px"><b>&nbsp;&nbsp;'.$patient->dob.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Mobile</td>
                    <td style="width:300px"><b>&nbsp;&nbsp;'.$patient->phone.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Email</td>
                    <td style="width:400px"><b>&nbsp;&nbsp;'.$patient->email.'</b></td>
                </tr>';
            if($inpatient){
                $adm_id=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,null,null,null);
                $html .='<tr>
                            <td style="width:300px">&nbsp;&nbsp;Admission Id</td>
                            <td style="width:400px"><b>&nbsp;&nbsp;'.$adm_id[0]->admissionid.'</b></td>
                        </tr>';
            }
$html .='</table>';
$html .='<p></p>';

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Visit Id</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Department</b></td>
                    <td style="width:350px;text-align:center;"><b> &nbsp;Sub Department</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Service</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Status</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Action Date</b></td>
                </tr>';
$i = 1;

    foreach ($data as $key => $value) {
        $dp=$this->Administration_model->departments($value->departmentid);
        $sub_dp=$this->Administration_model->subdepartments($value->subdepartmentid);
        $srv=$this->Administration_model->services($value->serviceid);
        
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$value->patientvisitid . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' . $dp[0]->name.'</td>
                    <td>&nbsp;&nbsp;'.$sub_dp[0]->name.'</td>
                    <td>&nbsp;&nbsp;'.$srv[0]->name.'</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;'.$value->createdon.'</td>
                </tr>';
      }

$html.='</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Patient Service Orders.pdf', 'D');
exit;
?>