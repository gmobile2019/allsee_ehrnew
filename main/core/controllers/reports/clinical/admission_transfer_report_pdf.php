<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 7);
//heading
$html  = '<h3 align="center">Admission Transfer Report</h3>';

if($start <> null){
    $html .= '<h3 align="left">Start : '.$start.'</h3>';
}

if($end <> null){
    
    $html .= '<h3 align="left">End : '.$end.'</h3>';
}

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Name</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Patient Id</b></td>
                    <td style="width:200pxtext-align:center;"><b> &nbsp;Admission Id</b></td>
                    <td style="width:270px;text-align:center"><b> &nbsp;Ward</b></td>
                    <td style="width:180px;text-align:center"><b> &nbsp;Bed</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Status</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Admission Timestamp</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Transfer Timestamp</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
                $wrd=$this->Inventory_model->units($value->wardid);
                $bd=$this->Administration_model->ward_beds($value->bedid);
                $patient=$this->Reception_model->patient($value->patientid);
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$patient->name . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;'.$value->patientid.'</td>
                    <td>&nbsp;&nbsp;'.$value->admissionid.'</td>
                    <td>&nbsp;&nbsp;' . $wrd[0]->name . '</td>
                    <td>&nbsp;&nbsp;' . $bd[0]->bedidentity.'</td>
                    <td>&nbsp;&nbsp;'.$value->status.'</td>
                    <td>&nbsp;&nbsp;'.$value->admittedon.'</td>
                    <td>&nbsp;&nbsp;'.$value->transferedon.'</td>
                </tr>';
      }

$html.='</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Admission Transfer Report.pdf', 'D');
exit;
?>