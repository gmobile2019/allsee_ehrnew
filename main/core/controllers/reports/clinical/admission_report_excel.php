<?php
$this->load->library('Excel');
$this->excel->setActiveSheetIndex(0);
$i=1;
$this->excel->getActiveSheet()->setTitle('Admission Report');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Admission Report');
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);
$i +=2;

if($start <> null){
    $this->excel->getActiveSheet()->setCellValue("A$i", 'Start Date');
    $this->excel->getActiveSheet()->setCellValue("B$i", $start);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

if($end <> null){
    
    $this->excel->getActiveSheet()->setCellValue("A$i", 'End Date');
    $this->excel->getActiveSheet()->setCellValue("B$i", $end);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

$this->excel->getActiveSheet()->setCellValue("A$i", 'Name');
$this->excel->getActiveSheet()->setCellValue("B$i", 'Patient Id');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Admission Id');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Ward');
$this->excel->getActiveSheet()->setCellValue("E$i", 'Bed');
$this->excel->getActiveSheet()->setCellValue("F$i", 'Status');
$this->excel->getActiveSheet()->setCellValue("G$i", 'Admission Timestamp');
$this->excel->getActiveSheet()->setCellValue("H$i", 'Discharge Timestamp');

$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("G$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setBold(true);	

   //print_r($data);exit;
    $total_sale=0;
    $i++;
    foreach($data as $key=>$value){
        
        $wrd=$this->Inventory_model->units($value->wardid);
        $bd=$this->Administration_model->ward_beds($value->bedid);
        $patient=$this->Reception_model->patient($value->patientid);
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$patient->name);
        $this->excel->getActiveSheet()->setCellValue('B'.$i,$value->patientid);
        $this->excel->getActiveSheet()->setCellValue('C'.$i,$value->admissionid);
        $this->excel->getActiveSheet()->setCellValue('D'.$i,$wrd[0]->name);
        $this->excel->getActiveSheet()->setCellValue('E'.$i,$bd[0]->bedidentity);
        $this->excel->getActiveSheet()->setCellValue('F'.$i,$value->status);
        $this->excel->getActiveSheet()->setCellValue('G'.$i,$value->createdon);
        $this->excel->getActiveSheet()->setCellValue('H'.$i,$value->dischargedon);
        
        //fomatting
        $this->excel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        
        $i++;
    }
    
    $filename="Admission Report";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    //ob_end_clean();
    //Excel2007
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
   $objWriter->save('php://output');
   exit;
