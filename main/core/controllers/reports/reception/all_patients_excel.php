<?php
$this->load->library('Excel');
$this->excel->setActiveSheetIndex(0);
$i=1;
$this->excel->getActiveSheet()->setTitle('Patients Report');

$this->excel->getActiveSheet()->setCellValue("D$i", 'Patients Report');
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);
$i +=2;

$this->excel->getActiveSheet()->setCellValue("A$i", 'Patient Id');
$this->excel->getActiveSheet()->setCellValue("B$i", 'Full Name');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Gender');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Marital Status');
$this->excel->getActiveSheet()->setCellValue("E$i", 'Date of Birth');
$this->excel->getActiveSheet()->setCellValue("F$i", 'Phone');
$this->excel->getActiveSheet()->setCellValue("G$i", 'Patient Category');

$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("G$i")->getFont()->setBold(true);	

   //print_r($data);exit;
    $total_sale=0;
    $i++;
    foreach($data as $key=>$value){
        $pType=$value->patienttype == 1?"Permanent":"Walk In";
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$value->patientid);
        $this->excel->getActiveSheet()->setCellValue('B'.$i,$value->name);
        $this->excel->getActiveSheet()->setCellValue('C'.$i,$value->gender);
        $this->excel->getActiveSheet()->setCellValue('D'.$i,$value->marital);
        $this->excel->getActiveSheet()->setCellValue('E'.$i,$value->dob);
        $this->excel->getActiveSheet()->setCellValue('F'.$i,$value->phone);
        $this->excel->getActiveSheet()->setCellValue('G'.$i,$pType);
        
        //fomatting
        $this->excel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        
        $i++;
    }
    
  
    $filename="Patients.xlsx";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    //ob_end_clean();
    //Excel2007
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
   $objWriter->save('php://output');
   exit;
