<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Patients Report</h3>';


    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Patient Id</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Full Name</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Gender</b></td>
                    <td style="width:250pxtext-align:center;"><b> &nbsp;Marital Status</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Date of Birth</b></td>
                    <td style="width:280px;text-align:center"><b> &nbsp;Phone</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Patient Category</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
        $pType=$value->patienttype == 1?"Permanent":"Walk In";
     $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;'.$value->patientid. ' &nbsp; </td>
                    <td>&nbsp;&nbsp;'.$value->name. '</td>
                    <td>&nbsp;&nbsp;'.$value->gender.'</td>
                    <td>&nbsp;&nbsp;'.$value->marital.'</td>
                    <td>&nbsp;&nbsp;'.$value->dob.'</td>
                    <td>&nbsp;&nbsp;'.$value->phone.'</td>
                    <td>&nbsp;&nbsp;'.$pType.'</td>
                </tr>';
      }

$html.='</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Patients.pdf', 'D');
exit;
?>