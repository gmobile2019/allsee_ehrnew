<?php
$this->load->library('Excel');
$this->excel->setActiveSheetIndex(0);
$i=1;
$this->excel->getActiveSheet()->setTitle('Patient Prepaid Account Report');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Patient Prepaid Account Report');
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);
$i +=2;


$this->excel->getActiveSheet()->setCellValue("A$i", 'Patient Id');
$this->excel->getActiveSheet()->setCellValue("B$i", 'Visit Id');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Service/Item');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Status');
$this->excel->getActiveSheet()->setCellValue("E$i", 'Transaction Id');
$this->excel->getActiveSheet()->setCellValue("F$i", 'Action Timestamp');
$this->excel->getActiveSheet()->setCellValue("G$i", 'Amount');

$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("G$i")->getFont()->setBold(true);	

   //print_r($data);exit;
    $total_sale=0;
    $i++;
    foreach($data as $key=>$value){
       $srv=null;
        if($value->departmentid <> null){
           $srv=$this->Inventory_model->items($value->service_item_id);
            $dpt=$this->Administration_model->departments($value->departmentid);
            $subdpt=$this->Administration_model->subdepartments($value->subdepartmentid);
            $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));

            if($dept[0]->id <> $value->departmentid){

                $srv=$this->Administration_model->services($value->service_item_id);
                
            } 
        }
        $total +=$value->amount;
       
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$value->patientid);
        $this->excel->getActiveSheet()->setCellValue('B'.$i, $value->patientvisitid);
        $this->excel->getActiveSheet()->setCellValue('C'.$i,$srv[0]->name);
        $this->excel->getActiveSheet()->setCellValue('D'.$i,$value->status);
        $this->excel->getActiveSheet()->setCellValue('E'.$i,$value->transactionid);
        $this->excel->getActiveSheet()->setCellValue('F'.$i,$value->createdon);
        $this->excel->getActiveSheet()->setCellValue('G'.$i,$value->amount);
        
        //fomatting
        $this->excel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('#,##0.00');
        
        $i++;
    }
    
   $this->excel->getActiveSheet()->setCellValue('F'.$i,'TOTAL');
   $this->excel->getActiveSheet()->setCellValue('G'.$i,$total);
   
   $this->excel->getActiveSheet()->getStyle('F'.$i)->getFont()->setBold(true);
   $this->excel->getActiveSheet()->getStyle('G'.$i)->getFont()->setBold(true);
   $this->excel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('#,##0.00');
   
    $filename="Patient Prepaid Account Report.xlsx";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    //ob_end_clean();
    //Excel2007
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
   $objWriter->save('php://output');
   exit;
