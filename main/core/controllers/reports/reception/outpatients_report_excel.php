<?php
$this->load->library('Excel');
$this->excel->setActiveSheetIndex(0);
$i=1;
$this->excel->getActiveSheet()->setTitle('Out Patients Report');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Out Patients Report');
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);
$i +=2;

if($start <> null){
    $this->excel->getActiveSheet()->setCellValue("A$i", 'Start Date');
    $this->excel->getActiveSheet()->setCellValue("B$i", $start);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

if($end <> null){
    
    $this->excel->getActiveSheet()->setCellValue("A$i", 'End Date');
    $this->excel->getActiveSheet()->setCellValue("B$i", $end);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

$this->excel->getActiveSheet()->setCellValue("A$i", 'Name');
$this->excel->getActiveSheet()->setCellValue("B$i", 'Patient Id');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Visit Id');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Visit Category');
$this->excel->getActiveSheet()->setCellValue("E$i", 'Consultation Category');
$this->excel->getActiveSheet()->setCellValue("F$i", 'Assigned Doctor');
$this->excel->getActiveSheet()->setCellValue("G$i", 'Consultation Status');
$this->excel->getActiveSheet()->setCellValue("H$i", 'Visit Timestamp');

$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("G$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setBold(true);	

   //print_r($data);exit;
    $total_sale=0;
    $i++;
    foreach($data as $key=>$value){
        
        $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
        $consultationcateg=$this->Administration_model->consultation_categories($dept[0]->id,$value->consultationcategory);
        $vstC=$this->Reception_model->visit_categories($value->visitcategory);
        $patient=$this->Reception_model->patient($value->patientid);
        $doc=$this->SuperAdministration_model->get_member_info($value->assigneddoctor);
        $status=$value->consultationstatus ==1?'Seen':'Not Seen';
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$patient->name);
        $this->excel->getActiveSheet()->setCellValue('B'.$i,$value->patientid);
        $this->excel->getActiveSheet()->setCellValue('C'.$i,$value->patientvisitid);
        $this->excel->getActiveSheet()->setCellValue('D'.$i,$vstC[0]->name);
        $this->excel->getActiveSheet()->setCellValue('E'.$i,$consultationcateg[0]->name);
        $this->excel->getActiveSheet()->setCellValue('F'.$i,$doc[0]->first_name.' '.$doc[0]->last_name);
        $this->excel->getActiveSheet()->setCellValue('G'.$i,$status);
        $this->excel->getActiveSheet()->setCellValue('H'.$i,$value->createdon);
        
        //fomatting
        $this->excel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        
        $i++;
    }
    
    $filename="Out Patients Report";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    //ob_end_clean();
    //Excel2007
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
   $objWriter->save('php://output');
   exit;
