<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 7);
//heading
$html  = '<h3 align="center">Patients Appointments</h3>';


    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Patient Id</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Full Name</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Appointment Doctor</b></td>
                    <td style="width:300pxtext-align:center;"><b> &nbsp;Appointment Date</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Appointment Time</b></td>
                    <td style="width:280px;text-align:center"><b> &nbsp;Timestamp</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Status</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
        $doctor = $this->ion_auth->user($value->appointmentdoctor)->row();
        
        if($value->status == 1){
            $status='Pending';
        }

        if($value->status == 2){

            $status='Confirmed';
        }

        if($value->status == 3){

            $status='Cancelled';
        }
                    
     $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;'.$value->patientid. ' &nbsp; </td>
                    <td>&nbsp;&nbsp;'.$value->name. '</td>
                    <td>&nbsp;&nbsp;'.$doctor->first_name.' '.$doctor->last_name.'</td>
                    <td>&nbsp;&nbsp;'.$value->appointmentdate.'</td>
                    <td>&nbsp;&nbsp;'.$value->appointmenttime.'</td>
                    <td>&nbsp;&nbsp;'.$value->createdon.'</td>
                    <td>&nbsp;&nbsp;'.$status.'</td>
                </tr>';
      }

$html.='</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Appointments.pdf', 'D');
exit;
?>