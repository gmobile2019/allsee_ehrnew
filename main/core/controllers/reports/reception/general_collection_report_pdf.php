<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
$usr=null;
if($userid <> null){
   
    $usr=$this->SuperAdministration_model->get_member_info($userid);
    $usr=$usr[0]->first_name.' '.$usr[0]->middle_name.' '.$usr[0]->last_name;
    $usr= '<h3 align="left">Operator : '.$usr.'</h3>';
}
//heading
$html  = '<h2 align="center">Service/Item General Collections</h2>';
$html .=$usr;


if($data['service_items'] <> null || (($data['prepaid_subscriptions'] <> null || $data['package_subscriptions'] <> null) && ($sponsor == null || $sponsor==$this->config->item('instant_cash_code')) )){
    if($data['service_items'] <> null){
        $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:270px;text-align:center"><b> &nbsp;Patient Id</b></td>
                    <td style="width:270px;text-align:center"><b> &nbsp;Visit Id</b></td>
                    <td style="width:400pxtext-align:center;"><b> &nbsp;Service/Item</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Sponsor</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Receipt No</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Action Timestamp</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Amount</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data['service_items'] as $ky => $val) {
        $srv=$this->Inventory_model->items($val->service_item_id);
        $sp=$this->SuperAdministration_model->get_sponsor_by_code($val->sponsorid);
        $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));

        if($dept[0]->id <> $val->departmentid){
            $srv=$this->Administration_model->services($val->service_item_id);
        }

        $srv_itm_total +=$val->amount;
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$val->patientid . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' . $val->patientvisitid.'</td>
                    <td>&nbsp;&nbsp;' . $srv[0]->name.'</td>
                    <td>&nbsp;&nbsp;' . $sp[0]->shortname.'</td>
                    <td>&nbsp;&nbsp;'.$val->transactionid.'</td>
                    <td>&nbsp;&nbsp;'.$val->createdon.'</td>
                    <td align="right">'.number_format($val->amount,2).'&nbsp;&nbsp;</td>
                </tr>';
      }

        $html.='<tr>'
            . '<td align="right" colspan="7"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
            . '<td align="right"><b>'.  number_format($srv_itm_total,2).'&nbsp;&nbsp;</b> </td>'
            . '</tr>'
            . '</table>';
        
    }
    

    if($data['prepaid_subscriptions'] <> null && ($sponsor == null || $sponsor==$this->config->item('instant_cash_code'))){ 
                $i = 1;
        $this->pdf->writeHTML($html);            
        $this->pdf->AddPage();
        $this->pdf->SetY(35);
        $this->pdf->SetX(2);
        $this->pdf->SetFont('', '', 8);

        $html = '<h2 align="center">Prepaid Subscriptions General Collections</h2>';
        $html .=$usr;
        $html.='<table border="1">
                    <tr>
                        <td style="width:150px;text-align:center"><b> &nbsp;S/No</b></td>
                        <td style="width:400px;text-align:center"><b> &nbsp;Patient Id</b></td>
                        <td style="width:400pxtext-align:center;"><b> &nbsp;Service</b></td>
                        <td style="width:400px;text-align:center"><b> &nbsp;Receipt No</b></td>
                        <td style="width:400px;text-align:center"><b> &nbsp;Action Timestamp</b></td>
                        <td style="width:400px;text-align:center"><b> &nbsp;Amount</b></td>
                    </tr>';

        foreach ($data['prepaid_subscriptions'] as $ky => $val) {
            $pr_total +=$val->amount;
            $html .='<tr>
                        <td>&nbsp;&nbsp;' . $i++ .'</td>
                        <td>&nbsp;&nbsp;' .$val->patientid . ' &nbsp; </td>
                        <td>&nbsp;&nbsp;Prepaid</td>
                        <td>&nbsp;&nbsp;'.$val->transactionid.'</td>
                        <td>&nbsp;&nbsp;'.$val->createdon.'</td>
                        <td align="right">'.number_format($val->amount,2).'&nbsp;&nbsp;</td>
                    </tr>';
          }
          $html.='<tr>'
            . '<td align="right" colspan="5"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
            . '<td align="right"><b>'.  number_format($pr_total,2).'&nbsp;&nbsp;</b> </td>'
            . '</tr>'
            . '</table>';
    }
    
     if($data['package_subscriptions'] <> null && ($sponsor == null || $sponsor==$this->config->item('instant_cash_code'))){ 
            $i = 1;
            $this->pdf->writeHTML($html);
            $this->pdf->AddPage();
            $this->pdf->SetY(35);
            $this->pdf->SetX(2);
            $this->pdf->SetFont('', '', 8);

            $html  = '<h2 align="center">Package Subscriptions General Collections</h2>';
            $html .=$usr;
            $html.='<table border="1">
                        <tr>
                            <td style="width:150px;text-align:center"><b> &nbsp;S/No</b></td>
                            <td style="width:400px;text-align:center"><b> &nbsp;Patient Id</b></td>
                            <td style="width:400pxtext-align:center;"><b> &nbsp;Package</b></td>
                            <td style="width:400px;text-align:center"><b> &nbsp;Receipt No</b></td>
                            <td style="width:400px;text-align:center"><b> &nbsp;Action Timestamp</b></td>
                            <td style="width:400px;text-align:center"><b> &nbsp;Amount</b></td>
                        </tr>';
            foreach ($data['package_subscriptions'] as $ky => $val) {
                $srv=$this->Administration_model->packages($val->packageid);
                $pckg_total +=$val->amount;
                $html .='<tr>
                            <td>&nbsp;&nbsp;' . $i++ .'</td>
                            <td>&nbsp;&nbsp;' .$val->patientid . ' &nbsp; </td>
                            <td>&nbsp;&nbsp;'.$srv[0]->name.'</td>
                            <td>&nbsp;&nbsp;'.$val->transactionid.'</td>
                            <td>&nbsp;&nbsp;'.$val->createdon.'</td>
                            <td align="right">'.number_format($val->amount,2).'&nbsp;&nbsp;</td>
                        </tr>';
              }
            
             $html.='<tr>'
            . '<td align="right" colspan="5"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
            . '<td align="right"><b>'.  number_format($pckg_total,2).'&nbsp;&nbsp;</b> </td>'
            . '</tr>'
            . '</table>';
     }

     $html .='<h2 style="color:black;text-align:left;font-weight:bold">Total Collections : '.number_format(($srv_itm_total + $pr_total + $pckg_total), 2).'</h2>';
     $this->pdf->writeHTML($html);
}else{
    
    $html .='<h4 style="color:black;text-align:center">No data found</h4>';
    $this->pdf->writeHTML($html);
}
     


ob_end_clean();
$this->pdf->Output('General Collection Report.pdf', 'D');
exit;
?>