<?php
$name=$this->Reception_model->patient($data[0]->patientid);
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);

$name=$this->Reception_model->patient($data[0]->patientid);
$gender=$this->Reception_model->genders($name->genderid);

$html ='<h3 align="center">Services Patient Statement</h3>';
$html .="<h4>Start Date :&nbsp;&nbsp;$start</h4>";
$html .="<h4>End Date :&nbsp;&nbsp;$end</h4>";
$html .='<table>
             <tr>
                 <td>Patient Id :<b>&nbsp;&nbsp;'.$data[0]->patientid.'</b></td>
             </tr>
             <tr>
                 <td>Name :&nbsp;&nbsp;<b>'.$name->name.'</b> </td>
             </tr>
             <tr>
                 <td>Date of Birth :&nbsp;&nbsp;<b>'. $name->dob.'</b></td>
             </tr>
            <tr>
                 <td>Gender :&nbsp;&nbsp;<b>'. $gender[0]->name.'</b></td>
             </tr>
         </table><p><p>';

$html .= '<table border="1">';
           $html .='<tr>
                        <td width="400px"><b>&nbsp;&nbsp;Department</b></td>
                        <td width="400px"><b>&nbsp;&nbsp;Service</b></td>
                        <td width="250px"><b>&nbsp;&nbsp;Sponsor</b></td>
                        <td width="290px"><b>&nbsp;&nbsp;Payment Mode</b></td>
                        <td width="340px"><b>&nbsp;&nbsp;Timestamp</b></td>
                        <td width="290px"><b>&nbsp;&nbsp;Amount</b></td>
                        <td width="290px"><b>&nbsp;&nbsp;Overdue</b></td>
                    </tr>';
           
           foreach($data as $key=>$value){
                $srv=$this->Inventory_model->items($value->service_item_id);
                    
                $department=$this->Administration_model->departments($value->departmentid);
                $subdepartment=$this->Administration_model->subdepartments($value->subdepartmentid);

                $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
                
                if($dept[0]->id <> $value->departmentid){
                    $srv=$this->Administration_model->services($value->service_item_id);
                }
                
                if($value->paymentstatus == 2){
                     $total_overdue +=$value->amount;
                     $overdue=$value->amount;
                     $amount=null;
                }else{
                     $total_amount +=$value->amount;
                     $amount=$value->amount;
                     $overdue=null;
                }
               
               $html .='<tr>
                            <td>&nbsp;&nbsp;'.$department[0]->name.'</td>
                            <td>&nbsp;&nbsp;'.$srv[0]->name.'</td>
                            <td>&nbsp;&nbsp;'.$value->shortname.'</td>
                            <td>&nbsp;&nbsp;'.$value->pmode.'</td>
                            <td>&nbsp;&nbsp;'.$value->createdon.'</td>
                            <td style="text-align: right">'.number_format($amount,2).'&nbsp;&nbsp;</td>
                            <td style="text-align: right">'.number_format($overdue,2).'&nbsp;&nbsp;</td>
                         </tr>';
               
            }
           
                 $html .='<tr>
                            <th colspan="5" style="text-align: right">
                                <b>Total</b>&nbsp;&nbsp;
                            </th>
                            <th style="text-align: right">
                                <b>'.number_format($total_amount,2).'</b>&nbsp;&nbsp;
                            </th>
                            <th style="text-align: right">
                                <b>'.number_format($total_overdue,2).'</b>&nbsp;&nbsp;
                            </th>
                        </tr>
                      </table>';
$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('PatientStatement_'.$data[0]->patientid.'_'.$data[0]->transactionid.'.pdf', 'D');
exit;