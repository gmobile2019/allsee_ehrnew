<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 7);
//heading
$html  = '<h3 align="center">Out Patients Report</h3>';

if($start <> null){
    $html .= '<h3 align="left">Start : '.$start.'</h3>';
}

if($end <> null){
    
    $html .= '<h3 align="left">End : '.$end.'</h3>';
}

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Name</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Patient Id</b></td>
                    <td style="width:250pxtext-align:center;"><b> &nbsp;Visit Id</b></td>
                    <td style="width:190px;text-align:center"><b> &nbsp;Visit/C</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Consultation/C</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Assigned Doctor</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Consultation Status</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Visit Timestamp</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
        
                $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
                $consultationcateg=$this->Administration_model->consultation_categories($dept[0]->id,$value->consultationcategory);
                $vstC=$this->Reception_model->visit_categories($value->visitcategory);
                $patient=$this->Reception_model->patient($value->patientid);
                $doc=$this->SuperAdministration_model->get_member_info($value->assigneddoctor);
                $status=$value->consultationstatus ==1?'Seen':'Not Seen';
                
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$patient->name . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;'.$value->patientid.'</td>
                    <td>&nbsp;&nbsp;'.$value->patientvisitid.'</td>
                    <td>&nbsp;&nbsp;' . $vstC[0]->name . '</td>
                    <td>&nbsp;&nbsp;' . $consultationcateg[0]->name.'</td>
                    <td>&nbsp;&nbsp;'.$doc[0]->first_name.' '.$doc[0]->last_name.'</td>
                    <td>&nbsp;&nbsp;'.$status.'</td>
                    <td>&nbsp;&nbsp;'.$value->createdon.'</td>
                </tr>';
      }

$html.='</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Out Patients Report.pdf', 'D');
exit;
?>