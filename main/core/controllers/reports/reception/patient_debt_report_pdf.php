<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Patient Debt Report</h3>';

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:270px;text-align:center"><b> &nbsp;Patient Id</b></td>
                    <td style="width:270px;text-align:center"><b> &nbsp;Visit Id</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Department</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Sub Department</b></td>
                    <td style="width:400pxtext-align:center;"><b> &nbsp;Service/Item</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Action Timestamp</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Amount</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
        $srv=$this->Inventory_model->items($value->service_item_id);
        $dpt=$this->Administration_model->departments($value->departmentid);
        $subdpt=$this->Administration_model->subdepartments($value->subdepartmentid);
        $clear=FALSE;
        $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));

        if($dept[0]->id <> $value->departmentid){

            $srv=$this->Administration_model->services($value->service_item_id);
            $clear=TRUE;
        }
        $total +=$value->amount;
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$value->patientid . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' . $value->patientvisitid.'</td>
                    <td>&nbsp;&nbsp;' . $dpt[0]->name.'</td>
                    <td>&nbsp;&nbsp;' . $subdpt[0]->name.'</td>
                    <td>&nbsp;&nbsp;'.$srv[0]->name.'</td>
                    <td>&nbsp;&nbsp;'.$value->createdon.'</td>
                    <td align="right">'.number_format($value->amount,2).'&nbsp;&nbsp;</td>
                </tr>';
      }

$html.='<tr>'
        . '<td align="right" colspan="7"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
        . '<td align="right"><b>'.  number_format($total,2).'&nbsp;&nbsp;</b> </td>'
        . '</tr>'
        . '</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Patient Debt Report.pdf', 'D');
exit;
?>