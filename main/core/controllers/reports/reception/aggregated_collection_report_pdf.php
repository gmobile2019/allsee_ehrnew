<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 11);
$usr=null;
if($userid <> null){
   
    $usr=$this->SuperAdministration_model->get_member_info($userid);
    $usr=$usr[0]->first_name.' '.$usr[0]->middle_name.' '.$usr[0]->last_name;
    $usr= '<h3 align="left">Operator : '.$usr.'</h3>';
}
//heading
$html =$usr;
$html .= '<h2 align="left">Service/Item Aggregated Collections</h2>';



if($data['service_items'] <> null || (($data['prepaid_subscriptions'] <> null || $data['package_subscriptions'] <> null) && ($sponsor == null || $sponsor==$this->config->item('instant_cash_code')) )){
    if($data['service_items'] <> null){
        $html.='<table border="1">
                <tr>
                    <td style="width:250px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:500pxtext-align:center;"><b> &nbsp;Department</b></td>
                    <td style="width:500px;text-align:center"><b> &nbsp;Sub Department</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Amount</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data['service_items'] as $ky => $val) {
        $dept=$this->Administration_model->departments($val->departmentid);
        $sub_dept=$this->Administration_model->subdepartments($val->subdepartmentid);
        $srv_itm_total +=$val->amount;
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' . $dept[0]->name.'</td>
                    <td>&nbsp;&nbsp;' . $sub_dept[0]->name.'</td>
                    <td align="right">'.number_format($val->amount,2).'&nbsp;&nbsp;</td>
                </tr>';
      }

        $html.='<tr>'
            . '<td align="right" colspan="3"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
            . '<td align="right"><b>'.  number_format($srv_itm_total,2).'&nbsp;&nbsp;</b> </td>'
            . '</tr>'
            . '</table>';
       
    }
    

    if($data['prepaid_subscriptions'] <> null && ($sponsor == null || $sponsor==$this->config->item('instant_cash_code'))){ 
                $i = 1;
                    
       
        $html .= '<br/><h3 align="left">Prepaid Subscriptions General Collections</h3>';
        $html.='<table border="1">
                    <tr>
                        <td style="width:250px;text-align:center"><b> &nbsp;S/No</b></td>
                        <td style="width:500px;text-align:center"><b> &nbsp;Patient</b></td>
                        <td style="width:500px;text-align:center"><b> &nbsp;Amount</b></td>
                    </tr>';

        foreach ($data['prepaid_subscriptions'] as $ky => $val) {
            $pt=$this->Reception_model->patient($val->patientid);
            $pr_total +=$val->amount;
            $html .='<tr>
                        <td>&nbsp;&nbsp;' . $i++ .'</td>
                        <td>&nbsp;&nbsp;' .$pt->name . ' &nbsp; </td>
                        <td align="right">'.number_format($val->amount,2).'&nbsp;&nbsp;</td>
                    </tr>';
          }
          $html.='<tr>'
            . '<td align="right" colspan="2"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
            . '<td align="right"><b>'.  number_format($pr_total,2).'&nbsp;&nbsp;</b> </td>'
            . '</tr>'
            . '</table>';
        
    }
    
     if($data['package_subscriptions'] <> null && ($sponsor == null || $sponsor==$this->config->item('instant_cash_code'))){ 
            $i = 1;
            
            $html  .= '<br/><h3 align="left">Package Subscriptions General Collections</h3>';
            $html.='<table border="1">
                        <tr>
                            <td style="width:250px;text-align:center"><b> &nbsp;S/No</b></td>
                            <td style="width:500pxtext-align:center;"><b> &nbsp;Package</b></td>
                            <td style="width:500px;text-align:center"><b> &nbsp;Amount</b></td>
                        </tr>';
            foreach ($data['package_subscriptions'] as $ky => $val) {
                $srv=$this->Administration_model->packages($val->packageid);
                $pckg_total +=$val->amount;
                $html .='<tr>
                            <td>&nbsp;&nbsp;' . $i++ .'</td>
                            <td>&nbsp;&nbsp;'.$srv[0]->name.'</td>
                            <td align="right">'.number_format($val->amount,2).'&nbsp;&nbsp;</td>
                        </tr>';
              }
            
             $html.='<tr>'
            . '<td align="right" colspan="2"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
            . '<td align="right"><b>'.  number_format($pckg_total,2).'&nbsp;&nbsp;</b> </td>'
            . '</tr>'
            . '</table>';
     }

     $html .='<h2 style="color:black;text-align:left;font-weight:bold">Total Collections : '.number_format(($srv_itm_total + $pr_total + $pckg_total), 2).'</h2>';
}else{
    
    $html .='<h4 style="color:black;text-align:center">No data found</h4>';
}
     

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Aggregated Collection Report.pdf', 'D');
exit;
?>