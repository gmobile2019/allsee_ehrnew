<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Patient Package Account Report</h3>';

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Patient Id</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Visit Id</b></td>
                    <td style="width:350pxtext-align:center;"><b> &nbsp;Service/Item</b></td>
                    <td style="width:170px;text-align:center"><b> &nbsp;Quantity</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Transaction Type</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Transaction Id</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Action Timestamp</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Amount</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
        $srv=null;
        if($value->departmentid <> null){
           $srv=$this->Inventory_model->items($value->service_item_id);
            $dpt=$this->Administration_model->departments($value->departmentid);
            $subdpt=$this->Administration_model->subdepartments($value->subdepartmentid);
            $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));

            if($dept[0]->id <> $value->departmentid){

                $srv=$this->Administration_model->services($value->service_item_id);
            } 
        }

        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;' .$value->patientid . ' &nbsp; </td>
                    <td>&nbsp;' . $value->patientvisitid.'</td>
                    <td>&nbsp;&nbsp;'.$srv[0]->name.'</td>
                    <td>&nbsp;&nbsp;'.$value->quantity.'</td>
                    <td>&nbsp;&nbsp;'.$value->transactiontype.'</td>
                    <td>&nbsp;&nbsp;'.$value->transactionid.'</td>
                    <td>&nbsp;&nbsp;'.$value->createdon.'</td>
                    <td align="right">'.number_format($value->amount,2).'&nbsp;&nbsp;</td>
                </tr>';
      }

$html.='</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Patient Package Account Report.pdf', 'D');
exit;
?>