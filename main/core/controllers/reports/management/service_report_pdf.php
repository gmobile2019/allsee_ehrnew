<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 7);
//heading
$html  = '<h3 align="center">Services Report</h3>';

if($start <> null){
    $html .= '<h3 align="left">Start : '.$start.'</h3>';
}

if($end <> null){
    
    $html .= '<h3 align="left">End : '.$end.'</h3>';
}

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Name</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Patient Id</b></td>
                    <td style="width:250pxtext-align:center;"><b> &nbsp;Visit Id</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Service</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Ordered By</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Status</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Visit Timestamp</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
        
                $patient=$this->Reception_model->patient($value->patientid);
                $user=$this->SuperAdministration_model->get_member_info($value->createdby);
                $status=$value->status ==1?'Paid':'Not Paid';
                
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$patient->name . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;'.$value->patientid.'</td>
                    <td>&nbsp;&nbsp;'.$value->patientvisitid.'</td>
                    <td>&nbsp;&nbsp;' .$value->name . '</td>
                    <td>&nbsp;&nbsp;'.$user[0]->first_name.' '.$user[0]->last_name.'</td>
                    <td>&nbsp;&nbsp;'.$status.'</td>
                    <td>&nbsp;&nbsp;'.$value->createdon.'</td>
                </tr>';
      }

$html.='</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Services Report.pdf', 'D');
exit;
?>