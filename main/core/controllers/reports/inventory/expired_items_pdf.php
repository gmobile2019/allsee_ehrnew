<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Expired Inventory Items Report</h3>';

if($start <> null){
    $html .= '<h3 align="left">Start : '.$start.'</h3>';
}

if($end <> null){
    
    $html .= '<h3 align="left">End : '.$end.'</h3>';
}

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Inventory Unit</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Item</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Category</b></td>
                    <td style="width:300pxtext-align:center;"><b> &nbsp;Store Batch</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Expiry Date</b></td>
                    <td style="width:200px;text-align:center"><b> &nbsp;Quantity</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Value</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
    $total +=$value['value'];
    $total_qty +=$value['quantity'];
    $un=$value['unit'] <> null?$this->Inventory_model->units($value['unit']):null;
    $un=$un==null?'Main Store':$un[0]->name;
    $cat=$this->Inventory_model->get_item_category_by_code($value['category']);
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$un . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' . $value['item'] . '</td>
                    <td>&nbsp;&nbsp;' . $cat->name . '</td>
                    <td>&nbsp;&nbsp;' . $value['batch'].'</td>
                    <td>&nbsp;&nbsp;'.$value['expiry'].'</td>
                    <td align="right">'.$value['quantity'].'&nbsp;&nbsp;</td>
                    <td align="right">'.number_format($value['value'],2).'&nbsp;&nbsp;</td>
                </tr>';
      }

$html.='<tr>'
        . '<td align="right" colspan="6"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
        . '<td align="right"><b>'.$total_qty.'&nbsp;&nbsp;</b> </td>'
        . '<td align="right"><b>'.  number_format($total,2).'&nbsp;&nbsp;</b> </td>'
        . '</tr>'
        . '</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Expired Inventory Items.pdf', 'D');
exit;
?>