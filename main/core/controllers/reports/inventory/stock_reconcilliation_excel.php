<?php
$this->load->library('Excel');
$this->excel->setActiveSheetIndex(0);
$i=1;
$this->excel->getActiveSheet()->setTitle('Stock Reconcilliation');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Stock Reconcilliation Report');
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);
$i +=2;

if($category <> null){
    
    $cat=$this->Inventory_model->get_item_category_by_code($category);
    $this->excel->getActiveSheet()->setCellValue("A$i", 'Item Category');
    $this->excel->getActiveSheet()->setCellValue("B$i", $cat->name);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

$this->excel->getActiveSheet()->setCellValue("A$i", 'Inventory Unit');
$this->excel->getActiveSheet()->setCellValue("B$i", 'Item');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Store Batch');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Current Stock');
$this->excel->getActiveSheet()->setCellValue("E$i", 'Fresh Stock');
$this->excel->getActiveSheet()->setCellValue("F$i", 'Expired Stock');
$this->excel->getActiveSheet()->setCellValue("G$i", 'Disposed Stock');

$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("G$i")->getFont()->setBold(true);	

   //print_r($data);exit;
    $total_sale=0;
    $i++;
    foreach($data as $key=>$value){
        $total_stock +=$value['total_stock'];
        $total_value +=$value['total_value'];
        $fresh_stock +=$value['fresh_stock'];
        $fresh_value +=$value['fresh_value'];
        $exp_stock +=$value['exp_stock'];
        $exp_value +=$value['exp_value'];
        $disp_stock +=$value['disp_stock'];
        $disp_value +=$value['disp_value'];
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$value['unit']);
        $this->excel->getActiveSheet()->setCellValue('B'.$i,$value['item']);
        $this->excel->getActiveSheet()->setCellValue('C'.$i,$value['storebatch']);
        $this->excel->getActiveSheet()->setCellValue('D'.$i,$value['total_stock'].'('.number_format($value['total_value'],2).')');
        $this->excel->getActiveSheet()->setCellValue('E'.$i,$value['fresh_stock'].'('.number_format($value['fresh_value'],2).')');
        $this->excel->getActiveSheet()->setCellValue('F'.$i,$value['exp_stock'].'('.number_format($value['exp_value'],2).')');
        $this->excel->getActiveSheet()->setCellValue('G'.$i,$value['disp_stock'].'('.number_format($value['disp_value'],2).')');
        
        //fomatting
        $this->excel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        
        $i++;
    }
    
   $this->excel->getActiveSheet()->setCellValue('C'.$i,'TOTAL');
   $this->excel->getActiveSheet()->setCellValue('D'.$i,$total_stock.'('.number_format($total_value, 2).')');
   $this->excel->getActiveSheet()->setCellValue('E'.$i,$fresh_stock.'('.number_format($fresh_value, 2).')');
   $this->excel->getActiveSheet()->setCellValue('F'.$i,$exp_stock.'('.number_format($exp_value, 2).')');
   $this->excel->getActiveSheet()->setCellValue('G'.$i,$disp_stock.'('.number_format($disp_value, 2).')');
   
   $this->excel->getActiveSheet()->getStyle('C'.$i)->getFont()->setBold(true);
   $this->excel->getActiveSheet()->getStyle('D'.$i)->getFont()->setBold(true);
   $this->excel->getActiveSheet()->getStyle('E'.$i)->getFont()->setBold(true);
   $this->excel->getActiveSheet()->getStyle('F'.$i)->getFont()->setBold(true);
   $this->excel->getActiveSheet()->getStyle('G'.$i)->getFont()->setBold(true);
   
    $filename="Stock Reconcilliation.xlsx";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    //ob_end_clean();
    //Excel2007
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
   $objWriter->save('php://output');
   exit;
