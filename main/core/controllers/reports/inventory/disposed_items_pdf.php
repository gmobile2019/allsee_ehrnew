<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Disposed Inventory Items Report</h3>';

if($start <> null){
    $html .= '<h3 align="left">Start : '.$start.'</h3>';
}

if($end <> null){
    
    $html .= '<h3 align="left">End : '.$end.'</h3>';
}

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Inventory Unit</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Item</b></td>
                    <td style="width:280pxtext-align:center;"><b> &nbsp;Store Batch</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Action Timestamp</b></td>
                    <td style="width:200px;text-align:center"><b> &nbsp;Reason</b></td>
                    <td style="width:200px;text-align:center"><b> &nbsp;Quantity</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Value</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
    $batchDetails=$this->Inventory_model->get_batch_details($value->storebatch);
    $itemValue=$batchDetails->unitprice*$value->quantity;
    $total +=$itemValue;
    $total_qty +=$value->quantity;
    $un=$value->source <> null?$this->Inventory_model->units($value->source):'Main Store';
    $un=$value->source==null?'Main Store':$un[0]->name;
    $cat=$this->Inventory_model->get_item_category_by_code($value->category);
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$un . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' . $value->name . '</td>
                    <td>&nbsp;&nbsp;' . $value->storebatch.'</td>
                    <td>&nbsp;&nbsp;'.$value->actiondate.'</td>
                    <td>&nbsp;&nbsp;'.$value->reason.'</td>
                    <td align="right">'.$value->quantity.'&nbsp;&nbsp;</td>
                    <td align="right">'.number_format($itemValue,2).'&nbsp;&nbsp;</td>
                </tr>';
      }

$html.='<tr>'
        . '<td align="right" colspan="6"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
        . '<td align="right"><b>'.$total_qty.'&nbsp;&nbsp;</b> </td>'
        . '<td align="right"><b>'.  number_format($total,2).'&nbsp;&nbsp;</b> </td>'
        . '</tr>'
        . '</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Disposed Inventory Items.pdf', 'D');
exit;
?>