<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Stock Transfers Report</h3>';

if($start <> null){
    $html .= '<h3 align="left">Start : '.$start.'</h3>';
}

if($end <> null){
    
    $html .= '<h3 align="left">End : '.$end.'</h3>';
}

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Inventory Unit</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Destination</b></td>
                    <td style="width:390px;text-align:center"><b> &nbsp;Item</b></td>
                    <td style="width:230pxtext-align:center;"><b> &nbsp;Store Batch</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Action Timestamp</b></td>
                    <td style="width:200px;text-align:center"><b> &nbsp;Quantity</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Value</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
        $itemValue=ceil($value->quantity*$value->unitprice);
        $total +=$itemValue;
        $total_qty +=$value->quantity;
        $source=$value->source_inventoryunit <> null?$this->Inventory_model->units($value->source_inventoryunit):'Main Store';
        $source=$source=='Main Store'?'Main Store':$source[0]->name;
        $destination=$value->inventoryunit <> null?$this->Inventory_model->units($value->inventoryunit):'Main Store';
        $destination=$destination==null?null:$destination[0]->name;
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$source . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' . $destination . '</td>
                    <td>&nbsp;&nbsp;' . $value->name.'</td>
                    <td>&nbsp;&nbsp;'.$value->storebatch.'</td>
                    <td>&nbsp;&nbsp;'.$value->lastupdate.'</td>
                    <td align="right">'.$value->quantity.'&nbsp;&nbsp;</td>
                    <td align="right">'.number_format($itemValue,2).'&nbsp;&nbsp;</td>
                </tr>';
      }

$html.='<tr>'
        . '<td align="right" colspan="6"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
        . '<td align="right"><b>'.$total_qty.'&nbsp;&nbsp;</b> </td>'
        . '<td align="right"><b>'.  number_format($total,2).'&nbsp;&nbsp;</b> </td>'
        . '</tr>'
        . '</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Stock Transfers.pdf', 'D');
exit;
?>