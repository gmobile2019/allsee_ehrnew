<?php
$this->load->library('Excel');
$this->excel->setActiveSheetIndex(0);
$i=1;
$this->excel->getActiveSheet()->setTitle('Inventory Items');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Inventory Items Report');
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$i +=2;


$this->excel->getActiveSheet()->setCellValue("A$i", 'Item');
$this->excel->getActiveSheet()->setCellValue("B$i", 'Category');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Drug Group');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Drug Type');
$this->excel->getActiveSheet()->setCellValue("E$i", 'Consumption/C');
$this->excel->getActiveSheet()->setCellValue("F$i", 'Status');

$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);	

   //print_r($data);exit;
    $total_sale=0;
    $i++;
    foreach($data as $key=>$value){
        $ct=$this->Inventory_model->get_item_category_by_code($value->category);
    if($code == $this->config->item('drug_code')){
        $drug_grp=$this->Inventory_model->drug_groups($value->drug_group);
        $drug_type=$this->Inventory_model->drug_types($value->drug_type);
    }
    $status=$value->status == 1?'Active':'Suspended';
    $consumption=$value->consumptioncategory == 1?'Selling':'Not Selling';
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$value->name);
        $this->excel->getActiveSheet()->setCellValue('B'.$i,$ct->name);
        $this->excel->getActiveSheet()->setCellValue('C'.$i,$drug_grp[0]->name);
        $this->excel->getActiveSheet()->setCellValue('D'.$i,$drug_type[0]->name);
        $this->excel->getActiveSheet()->setCellValue('E'.$i,$consumption);
        $this->excel->getActiveSheet()->setCellValue('F'.$i,$status);
        
        //fomatting
        $this->excel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        
        $i++;
    }
  
    $filename="Inventory Items.xlsx";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    //ob_end_clean();
    //Excel2007
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
   $objWriter->save('php://output');
   exit;
