<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Stock Reconcilliation Report</h3>';

if($category <> null){
    
    $cat=$this->Inventory_model->get_item_category_by_code($category);
    $html .= '<h3 align="left">Item Category : '.$cat->name.'</h3>';
}

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Inventory Unit</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Item</b></td>
                    <td style="width:220pxtext-align:center;"><b> &nbsp;Store Batch</b></td>
                    <td style="width:280px;text-align:center"><b> &nbsp;Current Stock</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Fresh Stock</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Expired Stock</b></td>
                    <td style="width:290px;text-align:center"><b> &nbsp;Disposed Stock</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
        $total_stock +=$value['total_stock'];
        $total_value +=$value['total_value'];
        $fresh_stock +=$value['fresh_stock'];
        $fresh_value +=$value['fresh_value'];
        $exp_stock +=$value['exp_stock'];
        $exp_value +=$value['exp_value'];
        $disp_stock +=$value['disp_stock'];
        $disp_value +=$value['disp_value'];
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$value['unit'] . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' . $value['item'] . '</td>
                    <td>&nbsp;&nbsp;' . $value['storebatch'] . '</td>
                    <td align="right">&nbsp;&nbsp;' . $value['total_stock'].'('.number_format($value['total_value'],2).')&nbsp;&nbsp;</td>
                    <td align="right">&nbsp;&nbsp;'.$value['fresh_stock'].'('.number_format($value['fresh_value'],2).')&nbsp;&nbsp;</td>
                    <td align="right">'.$value['exp_stock'].'('.number_format($value['exp_value'],2).')&nbsp;&nbsp;</td>
                    <td align="right">'.$value['disp_stock'].'('.number_format($value['disp_value'],2).')&nbsp;&nbsp;</td>
                </tr>';
      }

$html.='<tr>'
        . '<td align="right" colspan="4"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
        . '<td align="right"><b>'.$total_stock.'('.number_format($total_value, 2).')&nbsp;&nbsp;</b> </td>'
        . '<td align="right"><b>'.$fresh_stock.'('.number_format($fresh_value, 2).')&nbsp;&nbsp;</b> </td>'
        . '<td align="right"><b>'.$exp_stock.'('.number_format($exp_value, 2).')&nbsp;&nbsp;</b> </td>'
        . '<td align="right"><b>'.$disp_stock.'('.number_format($disp_value, 2).')&nbsp;&nbsp;</b> </td>'
        . '</tr>'
        . '</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Stock Reconcilliation.pdf', 'D');
exit;
?>