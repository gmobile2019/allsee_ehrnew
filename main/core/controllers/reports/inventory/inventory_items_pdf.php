<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Inventory Items Report</h3>';


    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Item</b></td>
                    <td style="width:300pxtext-align:center;"><b> &nbsp;Category</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Drug Group</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Drug Type</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Consumption/C</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Status</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
    $ct=$this->Inventory_model->get_item_category_by_code($value->category);
    if($code == $this->config->item('drug_code')){
        $drug_grp=$this->Inventory_model->drug_groups($value->drug_group);
        $drug_type=$this->Inventory_model->drug_types($value->drug_type);
    }
    $status=$value->status == 1?'Active':'Suspended';
    $consumption=$value->consumptioncategory == 1?'Selling':'Not Selling';
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' . $value->name . '</td>
                    <td>&nbsp;&nbsp;' . $ct->name.'</td>
                    <td>&nbsp;&nbsp;'.$drug_grp[0]->name.'</td>
                    <td>&nbsp;&nbsp;'.$drug_type[0]->name.'</td>
                    <td>&nbsp;&nbsp;'.$consumption.'</td>
                    <td>&nbsp;&nbsp;'.$status.'</td>
                </tr>';
      }

$html.='</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Inventory Items.pdf', 'D');
exit;
?>