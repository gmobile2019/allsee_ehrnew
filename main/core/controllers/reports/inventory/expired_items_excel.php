<?php
$this->load->library('Excel');
$this->excel->setActiveSheetIndex(0);
$i=1;
$this->excel->getActiveSheet()->setTitle('Expired Inventory Items');

$this->excel->getActiveSheet()->setCellValue("D$i", 'Expired Inventory Items Report');
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);
$i +=2;

if($start <> null){
    $this->excel->getActiveSheet()->setCellValue("A$i", 'Start Date');
    $this->excel->getActiveSheet()->setCellValue("B$i", $start);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

if($end <> null){
    
    $this->excel->getActiveSheet()->setCellValue("A$i", 'End Date');
    $this->excel->getActiveSheet()->setCellValue("B$i", $end);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

$this->excel->getActiveSheet()->setCellValue("A$i", 'Inventory Unit');
$this->excel->getActiveSheet()->setCellValue("B$i", 'Item');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Category');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Store Batch');
$this->excel->getActiveSheet()->setCellValue("E$i", 'Expiry Date');
$this->excel->getActiveSheet()->setCellValue("F$i", 'Quantity');
$this->excel->getActiveSheet()->setCellValue("G$i", 'Value');

$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("G$i")->getFont()->setBold(true);	

   //print_r($data);exit;
    $total_sale=0;
    $i++;
    foreach($data as $key=>$value){
        $total +=$value['value'];
        $total_qty +=$value['quantity'];
        $un=$value['unit'] <> null?$this->Inventory_model->units($value['unit']):null;
        $un=$un==null?'Main Store':$un[0]->name;
        $cat=$this->Inventory_model->get_item_category_by_code($value['category']);
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$un);
        $this->excel->getActiveSheet()->setCellValue('B'.$i,$value['item']);
        $this->excel->getActiveSheet()->setCellValue('C'.$i,$cat->name);
        $this->excel->getActiveSheet()->setCellValue('D'.$i,$value['batch']);
        $this->excel->getActiveSheet()->setCellValue('E'.$i,$value['expiry']);
        $this->excel->getActiveSheet()->setCellValue('F'.$i,$value['quantity']);
        $this->excel->getActiveSheet()->setCellValue('G'.$i,$value['value']);
        
        //fomatting
        $this->excel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $this->excel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('#,##0.00');
        
        $i++;
    }
    
   $this->excel->getActiveSheet()->setCellValue('E'.$i,'TOTAL');
   $this->excel->getActiveSheet()->setCellValue('F'.$i,$total_qty);
   $this->excel->getActiveSheet()->setCellValue('G'.$i,$total);
   
   $this->excel->getActiveSheet()->getStyle('E'.$i)->getFont()->setBold(true);
   $this->excel->getActiveSheet()->getStyle('F'.$i)->getFont()->setBold(true);
   $this->excel->getActiveSheet()->getStyle('G'.$i)->getFont()->setBold(true);
   $this->excel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('#,##0');
   $this->excel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('#,##0.00');
   
    $filename="Expired Inventory Items.xlsx";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    //ob_end_clean();
    //Excel2007
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
   $objWriter->save('php://output');
   exit;
