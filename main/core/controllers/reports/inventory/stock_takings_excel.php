<?php
$this->load->library('Excel');
$this->excel->setActiveSheetIndex(0);
$i=1;
$this->excel->getActiveSheet()->setTitle('Stock Takings');

$this->excel->getActiveSheet()->setCellValue("D$i", 'Stock Takings Report');
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);
$i +=2;

if($start <> null){
    $this->excel->getActiveSheet()->setCellValue("A$i", 'Start Date');
    $this->excel->getActiveSheet()->setCellValue("B$i", $start);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

if($end <> null){
    
    $this->excel->getActiveSheet()->setCellValue("A$i", 'End Date');
    $this->excel->getActiveSheet()->setCellValue("B$i", $end);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

$this->excel->getActiveSheet()->setCellValue("A$i", 'Inventory Unit');
$this->excel->getActiveSheet()->setCellValue("B$i", 'Supplier');
$this->excel->getActiveSheet()->setCellValue("C$i", 'Item');
$this->excel->getActiveSheet()->setCellValue("D$i", 'Store Batch');
$this->excel->getActiveSheet()->setCellValue("E$i", 'Action Timestamp');
$this->excel->getActiveSheet()->setCellValue("F$i", 'Quantity');
$this->excel->getActiveSheet()->setCellValue("G$i", 'Value');

$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("G$i")->getFont()->setBold(true);	

   //print_r($data);exit;
    $total_sale=0;
    $i++;
    foreach($data as $key=>$value){
        $itemValue=ceil($value->quantity*$value->unitprice);
        $total +=$itemValue;
        $total_qty +=$value->quantity;

        if($value->source_inventoryunit == null && $value->inventoryunit == null){
            //supplier
            $supplier=$this->Inventory_model->suppliers($value->supplierid);
            $supplier=$supplier[0]->name;
        }

        if($value->source_inventoryunit == null && $value->inventoryunit <> null){
            $supplier='Main Store';
        }

        if($value->source_inventoryunit <> null && $value->inventoryunit <> null){
            $supplier=$this->Inventory_model->units($value->source_inventoryunit);
            $supplier=$supplier[0]->name;
        }

        $inventoryUnit=$value->inventoryunit <> null?$this->Inventory_model->units($value->inventoryunit):'Main Store';
        $inventoryUnit=$value->inventoryunit==null?$inventoryUnit:$inventoryUnit[0]->name;
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$inventoryUnit);
        $this->excel->getActiveSheet()->setCellValue('B'.$i,$supplier);
        $this->excel->getActiveSheet()->setCellValue('C'.$i,$value->name);
        $this->excel->getActiveSheet()->setCellValue('D'.$i,$value->storebatch);
        $this->excel->getActiveSheet()->setCellValue('E'.$i,$value->lastupdate);
        $this->excel->getActiveSheet()->setCellValue('F'.$i,$value->quantity);
        $this->excel->getActiveSheet()->setCellValue('G'.$i,$itemValue);
        
        //fomatting
        $this->excel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $this->excel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('#,##0.00');
        
        $i++;
    }
    
   $this->excel->getActiveSheet()->setCellValue('E'.$i,'TOTAL');
   $this->excel->getActiveSheet()->setCellValue('F'.$i,$total_qty);
   $this->excel->getActiveSheet()->setCellValue('G'.$i,$total);
   
   $this->excel->getActiveSheet()->getStyle('E'.$i)->getFont()->setBold(true);
   $this->excel->getActiveSheet()->getStyle('F'.$i)->getFont()->setBold(true);
   $this->excel->getActiveSheet()->getStyle('G'.$i)->getFont()->setBold(true);
   $this->excel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('#,##0');
   $this->excel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('#,##0.00');
   
    $filename="Stock Takings.xlsx";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    //ob_end_clean();
    //Excel2007
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
   $objWriter->save('php://output');
   exit;
