<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Stock Requesition Orders Report</h3>';

if($start <> null){
    $html .= '<h3 align="left">Start : '.$start.'</h3>';
}

if($end <> null){
    
    $html .= '<h3 align="left">End : '.$end.'</h3>';
}

    $html.='<table border="1">
                <tr>
                   <td style="width:370px;text-align:center"><b> &nbsp;Item</b></td>
                    <td style="width:340px;text-align:center"><b> &nbsp;R/Inventory Unit</b></td>
                    <td style="width:195px;text-align:center"><b> &nbsp;R/Quantity</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;R/Action Timestamp</b></td>
                    <td style="width:340px;text-align:center"><b> &nbsp;S/Inventory Unit</b></td>
                    <td style="width:195px;text-align:center"><b> &nbsp;S/Quantity</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;S/Action Timestamp</b></td>
                    <td style="width:200px;text-align:center"><b> &nbsp;Status</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
         $p_unit='Main Store';
        if($value->supplyunit <> null){
            //supplier
            $p_unit=$this->Inventory_model->units($value->supplyunit);
            $p_unit=$p_unit[0]->name;
        }

        $r_unit=$this->Inventory_model->units($value->inventoryunit);


        if($value->status == 1){
            $r_status='Confirmed';
        }
        if($value->status == 2){
            $r_status='Declined';
        }
        if($value->status == 3){
            $r_status='Pending';
        }
                    
        $html .='<tr>
                    <td>&nbsp;&nbsp;' .$value->name . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' . $r_unit[0]->name . '</td>
                    <td>&nbsp;&nbsp;' .$value->requestqty.'</td>
                    <td>&nbsp;&nbsp;'.$value->createdon.'</td>
                    <td>&nbsp;&nbsp;'.$p_unit.'</td>
                    <td>&nbsp;&nbsp;'.$value->supplyqty.'</td>
                    <td>&nbsp;&nbsp;'.$value->modifiedon.'</td>
                    <td>&nbsp;&nbsp;'.$r_status.'</td>
                </tr>';
      }

$html.='</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Stock Requesition Orders.pdf', 'D');
exit;
?>