<?php
$this->load->library('Excel');
$this->excel->setActiveSheetIndex(0);
$i=1;
$this->excel->getActiveSheet()->setTitle('Stock Requesition Orders');

$this->excel->getActiveSheet()->setCellValue("D$i", 'Stock Requesition Orders Report');
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);
$i +=2;

if($start <> null){
    $this->excel->getActiveSheet()->setCellValue("A$i", 'Start Date');
    $this->excel->getActiveSheet()->setCellValue("B$i", $start);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

if($end <> null){
    
    $this->excel->getActiveSheet()->setCellValue("A$i", 'End Date');
    $this->excel->getActiveSheet()->setCellValue("B$i", $end);
    $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
    $i++;
}

$this->excel->getActiveSheet()->setCellValue("A$i", 'Itemt');
$this->excel->getActiveSheet()->setCellValue("B$i", 'R/Inventory Unit');
$this->excel->getActiveSheet()->setCellValue("C$i", 'R/Quantity');
$this->excel->getActiveSheet()->setCellValue("D$i", 'R/Action Timestamp');
$this->excel->getActiveSheet()->setCellValue("E$i", 'S/Inventory Unit');
$this->excel->getActiveSheet()->setCellValue("F$i", 'S/Quantity');
$this->excel->getActiveSheet()->setCellValue("G$i", 'S/Action Timestamp');
$this->excel->getActiveSheet()->setCellValue("H$i", 'Status');

$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("G$i")->getFont()->setBold(true);	
$this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setBold(true);	

   //print_r($data);exit;
    $total_sale=0;
    $i++;
    foreach($data as $key=>$value){
        $p_unit='Main Store';
        if($value->supplyunit <> null){
            //supplier
            $p_unit=$this->Inventory_model->units($value->supplyunit);
            $p_unit=$p_unit[0]->name;
        }

        $r_unit=$this->Inventory_model->units($value->inventoryunit);


        if($value->status == 1){
            $r_status='Confirmed';
        }
        if($value->status == 2){
            $r_status='Declined';
        }
        if($value->status == 3){
            $r_status='Pending';
        }
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$value->name);
        $this->excel->getActiveSheet()->setCellValue('B'.$i,$r_unit[0]->name);
        $this->excel->getActiveSheet()->setCellValue('C'.$i,$value->requestqty);
        $this->excel->getActiveSheet()->setCellValue('D'.$i,$value->createdon);
        $this->excel->getActiveSheet()->setCellValue('E'.$i,$p_unit);
        $this->excel->getActiveSheet()->setCellValue('F'.$i,$value->supplyqty);
        $this->excel->getActiveSheet()->setCellValue('G'.$i,$value->modifiedon);
        $this->excel->getActiveSheet()->setCellValue('H'.$i,$r_status);
        
        //fomatting
        $this->excel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $this->excel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->excel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        
        $i++;
    }
   
    $filename="Stock Requesition Orders.xlsx";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    //ob_end_clean();
    //Excel2007
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
   $objWriter->save('php://output');
   exit;
