<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Stock Takings Report</h3>';

if($start <> null){
    $html .= '<h3 align="left">Start : '.$start.'</h3>';
}

if($end <> null){
    
    $html .= '<h3 align="left">End : '.$end.'</h3>';
}

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Inventory Unit</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Supplier</b></td>
                    <td style="width:390px;text-align:center"><b> &nbsp;Item</b></td>
                    <td style="width:230pxtext-align:center;"><b> &nbsp;Store Batch</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Action Timestamp</b></td>
                    <td style="width:200px;text-align:center"><b> &nbsp;Quantity</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Value</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
        $itemValue=ceil($value->quantity*$value->unitprice);
        $total +=$itemValue;
        $total_qty +=$value->quantity;

        if($value->source_inventoryunit == null && $value->inventoryunit == null){
            //supplier
            $supplier=$this->Inventory_model->suppliers($value->supplierid);
            $supplier=$supplier[0]->name;
        }

        if($value->source_inventoryunit == null && $value->inventoryunit <> null){
            $supplier='Main Store';
        }

        if($value->source_inventoryunit <> null && $value->inventoryunit <> null){
            $supplier=$this->Inventory_model->units($value->source_inventoryunit);
            $supplier=$supplier[0]->name;
        }

        $inventoryUnit=$value->inventoryunit <> null?$this->Inventory_model->units($value->inventoryunit):'Main Store';
        $inventoryUnit=$value->inventoryunit==null?$inventoryUnit:$inventoryUnit[0]->name;
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$inventoryUnit . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' . $supplier . '</td>
                    <td>&nbsp;&nbsp;' . $value->name.'</td>
                    <td>&nbsp;&nbsp;'.$value->storebatch.'</td>
                    <td>&nbsp;&nbsp;'.$value->lastupdate.'</td>
                    <td align="right">'.$value->quantity.'&nbsp;&nbsp;</td>
                    <td align="right">'.number_format($itemValue,2).'&nbsp;&nbsp;</td>
                </tr>';
      }

$html.='<tr>'
        . '<td align="right" colspan="6"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
        . '<td align="right"><b>'.$total_qty.'&nbsp;&nbsp;</b> </td>'
        . '<td align="right"><b>'.  number_format($total,2).'&nbsp;&nbsp;</b> </td>'
        . '</tr>'
        . '</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Stock Takings.pdf', 'D');
exit;
?>