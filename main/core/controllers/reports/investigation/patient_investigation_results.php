<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 7);
//heading
$html  = '<h3 align="center">Investigation Results</h3>';
$testids=array();
$patient=$this->Reception_model->patient($patientid);
$gender=$this->Reception_model->genders($patient->genderid);

$html.='<table>
                <tr>
                    <td style="width:300px;">&nbsp;&nbsp;Patient Id</td>
                    <td style="width:300px;"><b>&nbsp;&nbsp;'.$patientid.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px;">&nbsp;&nbsp;Name</td>
                    <td style="width:300px;"><b>&nbsp;&nbsp;'.$patient->name.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px;">&nbsp;&nbsp;Gender</td>
                    <td style="width:300px"><b>&nbsp;&nbsp;'.$gender[0]->name.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Date of Birth</td>
                    <td style="width:300px"><b>&nbsp;&nbsp;'.$patient->dob.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Mobile</td>
                    <td style="width:300px"><b>&nbsp;&nbsp;'.$patient->phone.'</b></td>
                </tr>
                <tr>
                    <td style="width:300px">&nbsp;&nbsp;Email</td>
                    <td style="width:400px"><b>&nbsp;&nbsp;'.$patient->email.'</b></td>
                </tr>';
            if($inpatient){
                $adm_id=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,null,null,null);
                $html .='<tr>
                            <td style="width:300px">&nbsp;&nbsp;Admission Id</td>
                            <td style="width:400px"><b>&nbsp;&nbsp;'.$adm_id[0]->admissionid.'</b></td>
                        </tr>';
            }
$html .='</table>';
$html .='<p></p>';

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Service Name</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Test</b></td>
                    <td style="width:350px;text-align:center;"><b> &nbsp;Result</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Examiner</b></td>
                    <td style="width:330px;text-align:center"><b> &nbsp;Approver</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Timestamp</b></td>
                </tr>';
$i = 1;

    foreach ($data as $key => $value) {
         
         $testids[$value->testid]=$value->test;
        $examiner=$this->SuperAdministration_model->get_member_info($value->createdby);
        $examiner=$examiner[0]->first_name.' '.$examiner[0]->last_name;
        
        $approver=$this->SuperAdministration_model->get_member_info($value->approvedby);
        $approver=$approver[0]->first_name.' '.$approver[0]->last_name;
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$value->name . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' . $value->test.'</td>
                    <td>&nbsp;&nbsp;'.$value->result.'</td>
                    <td>&nbsp;&nbsp;'.$examiner.'</td>
                    <td>&nbsp;&nbsp;'.$approver.'</td>
                    <td align="right">'.$value->createdon.'&nbsp;&nbsp;</td>
                </tr>';
      }

$html.='</table>';

foreach($testids as $k=>$v){
    $normal_ranges=$this->Investigation_model->test_normal_ranges(null,$k,$inv_grp[0]->investigationcategory,1);

    if($normal_ranges <> null){
        $html .="<p></p><h5>".$v."</h5><ul>";
        foreach($normal_ranges as $normal=>$range){
             $html .="<li>".$range->name." : ".$range->normalrange."</li>";
        }
        $html .="</ul>";
    }
}
        
$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Patient Investigation Results.pdf', 'D');
exit;
?>