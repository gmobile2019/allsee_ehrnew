<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Investigation Patients Report</h3>';


    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Full Name</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Patient Id</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;VIsit Id</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Service Name</b></td>
                    <td style="width:400px;text-align:center;"><b> &nbsp;Timestamp</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
        
        $patient=$this->Reception_model->patient($value->patientid);
        $service=$this->Administration_model->services($value->serviceid);
        
     $html .='<tr>
                <td>&nbsp;&nbsp;' . $i++ .'</td>
                <td>&nbsp;&nbsp;'.$patient->name. ' &nbsp; </td>
                <td>&nbsp;&nbsp;'.$value->patientid. ' &nbsp; </td>
                <td>&nbsp;&nbsp;'.$value->patientvisitid. '</td>
                <td>&nbsp;&nbsp;'.$service[0]->name.'</td>
                <td>&nbsp;&nbsp;'.$value->createdon.'</td>
            </tr>';
      }

$html.='</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Investigation_Patients_Report.pdf', 'D');
exit;
?>