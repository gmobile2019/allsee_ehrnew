<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Nursing extends CI_Controller {

    function __construct() {
        parent::__construct();
       
	$this->load->model('Inventory_model');	
	$this->load->model('Pharmacy_model');	
	$this->load->model('Reception_model');	
	$this->load->model('Clinical_model');	
	$this->load->model('Nursing_model');	
		
        
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Home';
            $this->data['content']='nurse/dashboard';
            $this->load->view('nurse/template3',$this->data);
    }
    
    public function out_patients(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'Out Patients','link'=>'Nursing/out_patients'),
                                      array('title'=>'In Patients','link'=>'Nursing/in_patients'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Nursing/out_patients/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->out_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->out_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="Out Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='nurse/all_out_patients';
           $this->load->view('nurse/template',$this->data);
   }
   
    public function in_patients(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                      array('title'=>'Out Patients','link'=>'Nursing/out_patients'),
                                      array('title'=>'In Patients','link'=>'Nursing/in_patients'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Nursing/in_patients/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->in_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->in_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="In Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='nurse/all_in_patients';
           $this->load->view('nurse/template',$this->data);
   }
   
    public function patient_overview($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
           $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Nursing/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            
            $this->data['physical']=$this->Clinical_model->patient_physcial_examinations($patientid,$patientvisitid,null,null);
            $this->data['vitals']=$this->Clinical_model->patient_vitals_examinations($patientid,$patientvisitid,null,null);
            $this->data['diagnosis']=$this->Clinical_model->patient_diagnosis($patientid,$patientvisitid);
            $this->data['allegies']=$this->Clinical_model->patient_allegies($patientid,$patientvisitid);
            $this->data['service_orders']=$this->Clinical_model->patient_service_orders($patientid,$patientvisitid);
            $this->data['prescriptions']=$this->Clinical_model->patient_prescriptions($patientid,$patientvisitid);
            
            $this->data['physical_content']='clinical/patient_visit_physical_examinations';
            $this->data['vitals_content']='clinical/patient_visit_vitals';
            $this->data['diagnosis_content']='clinical/patient_visit_diagnosis';
            $this->data['allegies_content']='clinical/patient_visit_allegies';
            $this->data['service_orders_content']='clinical/patient_visit_service_orders';
            $this->data['prescription_orders_content']='clinical/patient_visit_prescription_orders';
            
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Patient Visit Overview<br/>Visit Id : '.$patientvisitid;
            $this->data['content']='nurse/patient_overview';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function patient_physical_examinations($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
           $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $docType=$seg[2];
            }
            
            if($docType == 1){
                
                    $data=$this->Clinical_model->patient_physcial_examinations($patientid,null,$start,$end);
                    require_once 'reports/clinical/patient_physcial_examinations_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Nursing/patient_physical_examinations/$patientid/$patientvisitid/".$start."_".$end."/";
            $config["total_rows"] =$this->Clinical_model->patient_physcial_examinations_info_count($patientid,null,$start,$end);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['physical_examinations'] = $this->Clinical_model->patient_physcial_examinations_info($patientid,null,$start,$end,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Patient Physical Examinations';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_physcial_examinations';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function add_physical_examination($patientid,$patientvisitid,$inpatientCare){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            if($inpatientCare){
                
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            }else{
                 $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            }
            
            $this->form_validation->set_rules('cardiovascular_system','Cardiovascular System','xss_clean');
            $this->form_validation->set_rules('respiratory_system','Respiratory System','xss_clean');
            $this->form_validation->set_rules('abdomen','Abdomen','xss_clean');
            $this->form_validation->set_rules('central_nervous_system','Central Nervous System','xss_clean');
            $this->form_validation->set_rules('muscular_skeletal_system','Muscular Skeletal System','xss_clean');

            if($this->form_validation->run() == true){
                $cardiovascular_system=$this->input->post('cardiovascular_system');
                $respiratory_system=$this->input->post('respiratory_system');
                $abdomen=$this->input->post('abdomen');
                $central_nervous_system=$this->input->post('central_nervous_system');
                $muscular_skeletal_system=$this->input->post('muscular_skeletal_system');

                $array=array(
                    'patientid'=>$patientid,
                    'patientvisitid'=>$patientvisitid,
                    'cardiovascularsystem'=>nl2br($cardiovascular_system),
                    'respiratorysystem'=>nl2br($respiratory_system),
                    'abdomen'=>nl2br($abdomen),
                    'centralnervoussystem'=>nl2br($central_nervous_system),
                    'muscularskeletalsystem'=>nl2br($muscular_skeletal_system),
                    'createdby'=>$this->session->userdata('user_id'),
                );

                $insrt=$this->Clinical_model->save_physcial_examination($array);

                if($insrt){
                    if($inpatientCare){
                        redirect('Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid);
                    }
                    redirect('Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid);
                }
                
                $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
            }
        
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=$inpatientCare;
            $this->data['title']='Record Patient Physical Examination';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/add_physcial_examination';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function patient_vitals_examinations($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
             $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $docType=$seg[2];
            }
            
            if($docType == 1){
                    $data=$this->Clinical_model->patient_vitals_examinations($patientid,null,$start,$end);
                    require_once 'reports/clinical/patient_vitals_examinations_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Nursing/patient_vitals_examinations/$patientid/$patientvisitid/".$start."_".$end."/";
            $config["total_rows"] =$this->Clinical_model->patient_vitals_examinations_info_count($patientid,null,$start,$end);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['vitals_examinations'] = $this->Clinical_model->patient_vitals_examinations_info($patientid,null,$start,$end,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Patient Vitals Examinations';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_vitals_examinations';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function add_vitals_examination($patientid,$patientvisitid,$inpatientCare){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            if($inpatientCare){
                
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            }else{
                 $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            }
            
            $this->form_validation->set_rules('weight','Weight','xss_clean');
            $this->form_validation->set_rules('height','Height','xss_clean');
            $this->form_validation->set_rules('temperature','Temperature','xss_clean');
            $this->form_validation->set_rules('pulserate','Pulse Rate','xss_clean');
            $this->form_validation->set_rules('bloodprssure','Blood Pressure','xss_clean');
            $this->form_validation->set_rules('respirationrate','Respiration Rate','xss_clean');
            $this->form_validation->set_rules('spo2','SPO2','xss_clean');

            if($this->form_validation->run() == true){
                $weight=$this->input->post('weight');
                $height=$this->input->post('height');
                $temperature=$this->input->post('temperature');
                $pulserate=$this->input->post('pulserate');
                $bloodpressure=$this->input->post('bloodpressure');
                $respirationrate=$this->input->post('respirationrate');
                $spo2=$this->input->post('spo2');

                $array=array(
                    'patientid'=>$patientid,
                    'patientvisitid'=>$patientvisitid,
                    'weight'=>$weight,
                    'height'=>$height,
                    'temperature'=>$temperature,
                    'pulserate'=>$pulserate,
                    'bloodpressure'=>$bloodpressure,
                    'respirationrate'=>$respirationrate,
                    'spo2'=>$spo2,
                    'createdby'=>$this->session->userdata('user_id'),
                );

                $insrt=$this->Clinical_model->save_vitals_examination($array);

                if($insrt){
                    if($inpatientCare){
                        redirect('Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid);
                    }
                    redirect('Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid);
                }
                
                $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
            }
        
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['inpatientCare']=$inpatientCare;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Record Patient Vitals Examination';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/add_vitals_examination';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function patient_allegies($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
         $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Nursing/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                $docType=$seg[3];
            }
            
            if($docType == 1){
                    
                    $data=$this->Clinical_model->patient_allegies($patientid,null,$start,$end,$status);
                    require_once 'reports/clinical/patient_allergies_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Nursing/patient_allegies/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_allegies_info_count($patientid,null,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['patient_allegies'] = $this->Clinical_model->patient_allegies_info($patientid,null,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Patient Allergies';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_allegies';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function add_allegy($patientid,$patientvisitid,$inpatientCare){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            if($inpatientCare){
                
               $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            }else{
                
                 $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            }
            
            
            $this->form_validation->set_rules('allegy','Allergy','xss_clean|required');
            $this->form_validation->set_rules('allegystatus','Status','xss_clean|required');
            $this->form_validation->set_rules('remarks','Rmarks','xss_clean');

            if($this->form_validation->run() == true){
                
                $allegy=$this->input->post('allegy');
                $allegystatus=$this->input->post('allegystatus');
                $remarks=  nl2br($this->input->post('remarks'));

                $array=array(
                    'patientid'=>$patientid,
                    'patientvisitid'=>$patientvisitid,
                    'allegy'=>$allegy,
                    'allegystatus'=>$allegystatus,
                    'remarks'=>$remarks,
                    'createdby'=>$this->session->userdata('user_id'),
                );

                $insrt=$this->Clinical_model->save_patient_allegy($array);

                if($insrt){
                    
                    if($inpatientCare){
                        
                        redirect('Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid);
                    }
                    
                    redirect('Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid);
                }
                
                $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
            }
        
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=$inpatientCare;
            $this->data['title']='Record Patient Allergy';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/add_allegy';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function patient_service_orders($patientid,$patientvisitid){
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                $docType=$seg[3];
            }
            
            if($docType == 1){
                
                    $data=$this->Clinical_model->patient_service_orders($patientid,null,$start,$end,$status);
                    require_once 'reports/clinical/patient_service_orders_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Nursing/patient_service_orders/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_service_orders_info_count($patientid,null,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['service_orders'] = $this->Clinical_model->patient_service_orders_info($patientid,null,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Patient Service Orders';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_service_orders';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function create_service_order($patientid,$patientvisitid,$inpatientCare){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           if($inpatientCare){
                
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            }else{
                 $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            }
           
           
         
           
            if($this->input->post('add_service')){
                $order=$this->input->post('order');
               
                if(count($order) > 0){
                    $error=TRUE;
                    foreach($order as $value){
                        $orderinfo=explode('_',$value);
                        $array=array(
                            'patientid'=>$patientid,
                            'patientvisitid'=>$patientvisitid,
                            'departmentid'=>$orderinfo[2],
                            'subdepartmentid'=>$orderinfo[1],
                            'serviceid'=>$orderinfo[0],
                            'createdby'=>$this->session->userdata('user_id'),
                        );
                        
                        $check=$this->Reception_model->check_pending_order($patientid,$patientvisitid,$orderinfo[0]);
                        
                        if(!$check){
                            $resp=$this->Reception_model->save_order($array);
                            if($resp){

                                $error=FALSE;
                            }
                        }
                        
                    }
                    
                    
                    if(!$error){
                        
                        if($inpatientCare){
                            
                            redirect('Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid,'refresh');
                        }
                        redirect('Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid,'refresh');
                    }
                    
                    if($error){
                        $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">service order addtion failed!</div>';
                    }
                    
                }else{
                    
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">select atleast one service!</div>';
                }
            }
            
            
            //consultation department
           $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
           $dept2=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
           $patient=$this->Reception_model->patient($patientid);
           $patient=$this->Reception_model->patients($patient->id);
           $this->data['patient']=$patient;
           $this->data['patientid']=$patientid;
           $this->data['patientvisitid']=$patientvisitid;
           $this->data['inpatientCare']=$inpatientCare;
           $this->data['subdepartments']=$this->Reception_model->non_counsulation_active_subdepartments($dept[0]->id,$dept2[0]->id);
           $this->data['services']=$this->Reception_model->non_consultation_services($dept[0]->id);
           $this->data['patient_visit']=$this->Reception_model->patient_visit_details($patientvisitid);
           $this->data['title']="Investigation/Diagnosis/Other";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='nurse/create_service_order';
           $this->load->view('nurse/template2',$this->data);
    }
    
    public function remove_service_order($id,$patientid,$patientvisitid,$inpatientCare){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->Reception_model->remove_service_order($id);
           if($inpatientCare){
              redirect('Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid,'refresh'); 
           }
           redirect('Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid,'refresh');
    }
    
    public function patient_prescriptions($patientid,$patientvisitid){
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                $docType=$seg[3];
            }
            
            if($docType == 1){
                   
                    $data=$this->Clinical_model->patient_prescriptions($patientid,null,$start,$end,$status);
                    require_once 'reports/clinical/patient_prescription_orders_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Nursing/patient_prescriptions/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_prescriptions_info_count($patientid,null,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['prescription_orders'] = $this->Clinical_model->patient_prescriptions_info($patientid,null,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Patient Prescription Orders';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_prescription_orders';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function create_dispense_order($patientid,$patientvisitid,$inpatientCare){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        
            if($inpatientCare){
                
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            }else{
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            }
            
        if($this->input->post('add_dispense_order')){
            
             $this->form_validation->set_rules('item_category','Item Category','required|xss_clean');
             $this->form_validation->set_rules('item','Item','required|xss_clean');
             
             if($this->input->post('item_category') == $this->config->item('drug_code')){
                $this->form_validation->set_rules('dosage','Dosage','required|xss_clean');
                $this->form_validation->set_rules('frequency','Frequency','required|xss_clean');
                $this->form_validation->set_rules('no_days','No Days','required|xss_clean');
             }else{
                 
                 $this->form_validation->set_rules('quantity','Quantity','required|xss_clean');
             }
            
             if ($this->form_validation->run() == TRUE)
                {
                 $itemid=$this->input->post('item');
                 
                 $dispense_array=array(
                     'patientid'=>$patientid,
                     'patientvisitid'=>$patientvisitid,
                     'itemid'=>$itemid,
                     'createdby'=>$this->session->userdata('user_id')
                 );
                 
                 if($this->input->post('item_category') == $this->config->item('drug_code')){
                    $dosage=$this->input->post('dosage');
                    $frequency=$this->input->post('frequency');
                    $no_days=$this->input->post('no_days');
                    $quantity=$dosage*$frequency*$no_days;
                    
                    $dispense_array['dosageid']=$dosage;
                    $dispense_array['frequencyid']=$frequency;
                    $dispense_array['days']=$no_days;
                 }else{

                     $quantity=$this->input->post('quantity');
                 }
                 
                 $dispense_array['quantity']=$quantity;
                 
                    $check=$this->Pharmacy_model->check_pending_order($patientid,$patientvisitid,$itemid);
                    
                    if(!$check){
                        $sv=$this->Pharmacy_model->save_dispense_order($dispense_array);
                    
                        if($sv){
                            if($inpatientCare){
                                
                                redirect('Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid,'refresh');
                            }
                            redirect('Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid,'refresh');
                        }
                        
                        $this->data['disp_msg']='<div class="alert alert-danger " role="alert" style="text-align:center;">posting failed!</div>';
                    }else{
                        $this->data['disp_msg']='<div class="alert alert-danger " role="alert" style="text-align:center;">clear pending item order!</div>';
                    }
                    
                }
             }
             
        $patient=$this->Reception_model->patient($patientid);
        $patient=$this->Reception_model->patients($patient->id);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['patientvisitid']=$patientvisitid;
        $this->data['inpatientCare']=$inpatientCare;
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories(null,$this->config->item('drug_code'));
        $this->data['items'] = $this->Inventory_model->active_items();
        $this->data['drug_frequencies'] = $this->Pharmacy_model->active_drug_frequencies();
        $this->data['drug_dosages'] = $this->Pharmacy_model->active_drug_dosages();
        $this->data['title']="Create Prescription Order";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['profile']=$this->SuperAdministration_model->profile_data();
        $this->data['content']='nurse/create_prescription_order';
        $this->load->view('nurse/template2',$this->data);
    }
    
    public function remove_dispense_order($id,$patientid,$patientvisitid,$inpatientCare){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->Pharmacy_model->remove_dispense_order($id);
           if($inpatientCare){
                redirect('Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid,'refresh');
           }
           redirect('Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid,'refresh');
    }
    
    public function patient_appointments($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                 $docType=$seg[3];
            }
            
//            if($docType == 1){
//                   
//                    $data=$this->Clinical_model->patient_appointments($patientid,null,$start,$end,$status);
//                    require_once 'reports/reception/appointments_pdf.php';
//            }
            
            $config["base_url"] = base_url() . "index.php/Nursing/patient_appointments/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_appointments_info_count($patientid,null,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['appointments'] = $this->Clinical_model->patient_appointments_info($patientid,null,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Patient Appointments';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_appointments';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function add_appointment($patientid,$patientvisitid){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Nursing/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            $this->form_validation->set_rules('appointment_date','Appointment Date','xss_clean|required|callback_appointment_date');
            $this->form_validation->set_rules('appointment_time','Appointment Time','xss_clean|required|callback_appointment_time');
            $this->form_validation->set_rules('doctor','Doctor','xss_clean|required');

            if($this->form_validation->run() == true){
                $appointment_date=$this->input->post('appointment_date');
                $appointment_time=$this->input->post('appointment_time');
                $doctor=$this->input->post('doctor');
                
                if($appointment_date > date('Y-m-d')){
                    $array=array(
                    'patientid'=>$patientid,
                    'patientvisitid'=>$patientvisitid,
                    'appointmentdate'=>$appointment_date,
                    'appointmenttime'=>$appointment_time.':00',
                    'appointmentdoctor'=>$doctor,
                    'createdby'=>$this->session->userdata('user_id'),
                    );

                    $insrt=$this->Clinical_model->save_clinical_appointment($array);

                    if($insrt){
                        $this->Reception_model->check_out_patient($patientid,$patientvisitid);
                        $this->Clinical_model->update_consultation_status($patientid,$patientvisitid);
                        redirect('Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid);
                    }

                    $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
                }else{
                    $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">appointment date must be greater than current date ('.date('Y-m-d').')</div>';
                }
                
            }
        
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Record Appointment';
            $this->data['doctors']=$this->Administration_model->doctors();
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/add_appointment';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function cancel_appointment($id,$patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Clinical_model->cancel_appointment($id);
        redirect('Nursing/patient_appointments/'.$patientid.'/'.$patientvisitid,'refresh');
    }
    
/***********************************************************inpatient*************************************************************************************/
    
    public function inpatient_overview($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'ADMITTED');
            if($admission <> null){
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            }else{
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                               );
            }
           
            
            
            $this->data['physical']=$this->Clinical_model->patient_physcial_examinations($patientid,$patientvisitid,null,null);
            $this->data['vitals']=$this->Clinical_model->patient_vitals_examinations($patientid,$patientvisitid,null,null);
            $this->data['diagnosis']=$this->Clinical_model->patient_diagnosis($patientid,$patientvisitid);
            $this->data['allegies']=$this->Clinical_model->patient_allegies($patientid,$patientvisitid);
            $this->data['service_orders']=$this->Clinical_model->patient_service_orders($patientid,$patientvisitid);
            $this->data['prescriptions']=$this->Clinical_model->patient_prescriptions($patientid,$patientvisitid);
            
            $this->data['physical_content']='clinical/patient_visit_physical_examinations';
            $this->data['vitals_content']='clinical/patient_visit_vitals';
            $this->data['diagnosis_content']='clinical/patient_visit_diagnosis';
            $this->data['allegies_content']='clinical/patient_visit_allegies';
            $this->data['service_orders_content']='clinical/patient_visit_service_orders';
            $this->data['prescription_orders_content']='clinical/patient_visit_prescription_orders';
            
            
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['admission']=$admission;
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['wards']=$this->Inventory_model->active_units(null,$this->config->item('ward_unit_category_code'));
            $this->data['beds']=$this->Administration_model->ward_beds(null,1);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']=$admission == null?'Patient Admission':'Patient Overview<br/>Admission Id : '.$admission[0]->admissionid;
            $this->data['admissionsources']=$this->Nursing_model->admissionsources(null,1);
            $this->data['admissiontypes']=$this->Nursing_model->admissiontypes(null,1);
            $this->data['patientAdmission']='nurse/patient_admission';
            $this->data['content']='nurse/inpatient_overview';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function patient_admission($patientid,$patientvisitid,$admsnid){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            $this->form_validation->set_rules('ward','Ward','xss_clean|required');
            $this->form_validation->set_rules('bed','Bed','xss_clean|required');
            $this->form_validation->set_rules('admissiontype','Admission Type','xss_clean|required');
            $this->form_validation->set_rules('admissionsource','Admission Source','xss_clean|required');

            if($this->form_validation->run() == true){
                $ward=$this->input->post('ward');
                $bed=$this->input->post('bed');
                $admissiontype=$this->input->post('admissiontype');
                $admissionsource=$this->input->post('admissionsource');
                $bed_occupancy=$this->Nursing_model->admission_details(null,null,null,'ADMITTED',$ward,$bed);
                
                if($bed_occupancy == null){
                    
                    if($admsnid <> null){
                        
                        $admissionStatus=TRUE;
                        $admissionid=$admsnid;
                    }else{
                        $admissionStatus=FALSE;
                        $admissionid=$this->Reception_model->get_admissionid();
                    } 
                
                    $array=array(
                        'patientid'=>$patientid,
                        'patientvisitid'=>$patientvisitid,
                        'admissionid'=>$admissionid,
                        'wardid'=>$ward,
                        'bedid'=>$bed,
                        'admissiontype'=>$admissiontype,
                        'admissionsource'=>$admissionsource,
                        'createdby'=>$this->session->userdata('user_id'),
                    );

                    $insrt=$this->Nursing_model->process_admission($array,$admissionStatus);

                    if($insrt){

                       redirect('Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid);
                    }

                    $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">admission failed!</div>';
                }else{
                    $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">admission failed, bed occupied!</div>';
                }
               
            }
        
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['admissionid']=$admsnid;
            $this->data['admissionsources']=$this->Nursing_model->admissionsources(null,1);
            $this->data['admissiontypes']=$this->Nursing_model->admissiontypes(null,1);
            $this->data['wards']=$this->Inventory_model->active_units(null,$this->config->item('ward_unit_category_code'));
            $this->data['beds']=$this->Administration_model->ward_beds(null,1);
            $this->data['title']=$admsnid <> null?'Admission Transfer':'Patient Admission';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_admission';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function inpatient_physical_examinations($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
           $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'ADMITTED');
           if($admission == null){
               redirect('Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid,'refresh');
           }
           
           $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $docType=$seg[2];
            }
            
            if($docType == 1){
                    $inpatient=TRUE;
                    $data=$this->Clinical_model->patient_physcial_examinations($patientid,$patientvisitid,$start,$end);
                    require_once 'reports/clinical/patient_physcial_examinations_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Nursing/inpatient_physical_examinations/$patientid/$patientvisitid/".$start."_".$end."/";
            $config["total_rows"] =$this->Clinical_model->patient_physcial_examinations_info_count($patientid,$patientvisitid,$start,$end);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['physical'] = $this->Clinical_model->patient_physcial_examinations_info($patientid,$patientvisitid,$start,$end,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=TRUE;
            $this->data['title']='Patient Physical Examinations';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_visit_physical_examinations';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function inpatient_vitals_examinations($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'ADMITTED');
            if($admission == null){
                redirect('Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid,'refresh');
            }
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $docType=$seg[2];
            }
            
            if($docType == 1){
                    $inpatient=TRUE;
                    $data=$this->Clinical_model->patient_vitals_examinations($patientid,$patientvisitid,$start,$end);
                    require_once 'reports/clinical/patient_vitals_examinations_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/inpatient_vitals_examinations/$patientid/$patientvisitid/".$start."_".$end."/";
            $config["total_rows"] =$this->Clinical_model->patient_vitals_examinations_info_count($patientid,$patientvisitid,$start,$end);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['vitals'] = $this->Clinical_model->patient_vitals_examinations_info($patientid,$patientvisitid,$start,$end,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=TRUE;
            $this->data['title']='Patient Vitals Examinations';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_visit_vitals';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function inpatient_allegies($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'ADMITTED');
        if($admission == null){
            redirect('Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid,'refresh');
        }    
        $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                $docType=$seg[3];
            }
            
            if($docType == 1){
                    $inpatient=TRUE;
                    $data=$this->Clinical_model->patient_allegies($patientid,$patientvisitid,$start,$end,$status);
                    require_once 'reports/clinical/patient_allergies_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/inpatient_allegies/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_allegies_info_count($patientid,$patientvisitid,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['allegies'] = $this->Clinical_model->patient_allegies_info($patientid,$patientvisitid,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=TRUE;
            $this->data['title']='Patient Allergies';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_visit_allegies';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function inpatient_service_orders($patientid,$patientvisitid){
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'ADMITTED');
           if($admission == null){
               redirect('Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid,'refresh');
           }
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                $docType=$seg[3];
            }
            
            if($docType == 1){
                    $inpatient=TRUE;
                    $data=$this->Clinical_model->patient_service_orders($patientid,$patientvisitid,$start,$end,$status);
                    require_once 'reports/clinical/patient_service_orders_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/inpatient_service_orders/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_service_orders_info_count($patientid,$patientvisitid,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['service_orders'] = $this->Clinical_model->patient_service_orders_info($patientid,$patientvisitid,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=TRUE;
            $this->data['title']='Patient Service Orders';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_visit_service_orders';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function inpatient_prescriptions($patientid,$patientvisitid){
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
           $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'ADMITTED');
           if($admission == null){
               redirect('Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid,'refresh');
           }
           $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                $docType=$seg[3];
            }
            
            if($docType == 1){
                
                    $inpatient=TRUE;
                    $data=$this->Clinical_model->patient_prescriptions($patientid,$patientvisitid,$start,$end,$status);
                    require_once 'reports/clinical/patient_prescription_orders_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/inpatient_prescriptions/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_prescriptions_info_count($patientid,$patientvisitid,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['prescriptions'] = $this->Clinical_model->patient_prescriptions_info($patientid,$patientvisitid,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=TRUE;
            $this->data['title']='Patient Prescription Orders';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_visit_prescription_orders';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function inpatient_doctor_notes($patientid,$patientvisitid){
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'ADMITTED');
           if($admission == null){
               redirect('Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid,'refresh');
           }
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
           
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $docType=$seg[2];
            }
            
            if($docType == 1){
                
                $data=$this->Clinical_model->inpatient_doctor_notes($patientid,$patientvisitid,$start,$end);
                require_once 'reports/clinical/doctor_notes_pdf.php';
            }
        
            $config["base_url"] = base_url() . "index.php/Nursing/inpatient_doctor_notes/$patientid/$patientvisitid/".$start."_".$end."/";
            $config["total_rows"] =$this->Clinical_model->inpatient_doctor_notes_info_count($patientid,$patientvisitid,$start,$end);
            $config["per_page"] =5;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['doctorNotes'] = $this->Clinical_model->inpatient_doctor_notes_info($patientid,$patientvisitid,$start,$end,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Doctor Notes';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/inpatient_doctor_notes';
            $this->load->view('nurse/template2',$this->data);
    }
    
    public function inpatient_nurse_notes($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'ADMITTED');
        if($admission == null){
            redirect('Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid,'refresh');
        }    
        $this->data['side_menu']=array(
                              array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                          );

        if($this->input->post('start')){
            $start=$this->input->post('start');
            $this->data['start']=$start;
        }

        if($this->input->post('end')){
            $end=$this->input->post('end');
            $this->data['end']=$end;
        }


        if($this->uri->segment(5)){
            $seg=explode('_',$this->uri->segment(5));

            $start=$seg[0];
            $this->data['start']=$start;

            $end=$seg[1];
            $this->data['end']=$end;

            $docType=$seg[2];
        }

        if($docType == 1){
                
                $data=$this->Nursing_model->inpatient_nurse_notes($patientid,$patientvisitid,$start,$end);
                require_once 'reports/clinical/nurse_notes_pdf.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Nursing/inpatient_nurse_notes/$patientid/$patientvisitid/".$start."_".$end."/";
        $config["total_rows"] =$this->Nursing_model->inpatient_nurse_notes_info_count($patientid,$patientvisitid,$start,$end);
        $config["per_page"] =5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =6;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['nurseNotes'] = $this->Nursing_model->inpatient_nurse_notes_info($patientid,$patientvisitid,$start,$end,$page,$limit);

        $patient=$this->Reception_model->patient($patientid);
        $this->data['patient']=$this->Reception_model->patients($patient->id);
        $this->data['patientid']=$patientid;
        $this->data['patientvisitid']=$patientvisitid;
        $this->data['patientvisitid']=$patientvisitid;
        $this->data['title']='Nurse Notes';
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='nurse/inpatient_nurse_notes';
        $this->load->view('nurse/template2',$this->data);
    }
    
    public function write_nurse_notes($patientid,$patientvisitid){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'ADMITTED');
           if($admission == null){
               redirect('Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid,'refresh');
           }
            $this->data['side_menu']=array(
                              array('title'=>'Patient Care','link'=>'Nursing/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Physical Examinations','link'=>'Nursing/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Vitals','link'=>'Nursing/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Allergies','link'=>'Nursing/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Service Orders','link'=>'Nursing/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Prescriptions','link'=>'Nursing/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Doctor Notes','link'=>'Nursing/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                              array('title'=>'Nurse Notes','link'=>'Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                          );

           $this->form_validation->set_rules('currentdiagnosis','Current Diagnosis','xss_clean|required');
           $this->form_validation->set_rules('treatment','Treatment','xss_clean|required');
           $this->form_validation->set_rules('procedures','Procedures','xss_clean|required');
           $this->form_validation->set_rules('medications','Medications','xss_clean|required');
           $this->form_validation->set_rules('medicationeffect','Medication Effects','xss_clean|required');
           $this->form_validation->set_rules('medicationcomments','Medication Comments','xss_clean|required');
           $this->form_validation->set_rules('intakes','Intakes','xss_clean|required');
           $this->form_validation->set_rules('outputs','Outputs','xss_clean|required');
           $this->form_validation->set_rules('overallprogress','Overall Progress','xss_clean|required');
           $this->form_validation->set_rules('remarks','Remarks','xss_clean|required');
           $this->form_validation->set_rules('admissionid','Admission Id','xss_clean|required');

           if($this->form_validation->run() == true){
               $currentdiagnosis=trim(nl2br($this->input->post('currentdiagnosis')));
               $treatment=trim(nl2br($this->input->post('treatment')));
               $procedures=trim(nl2br($this->input->post('procedures')));
               $medications=trim(nl2br($this->input->post('medications')));
               $medicationeffect=trim(nl2br($this->input->post('medicationeffect')));
               $medicationcomments=trim(nl2br($this->input->post('medicationcomments')));
               $intakes=trim(nl2br($this->input->post('intakes')));
               $outputs=trim(nl2br($this->input->post('outputs')));
               $overallprogress=trim(nl2br($this->input->post('overallprogress')));
               $remarks=trim(nl2br($this->input->post('remarks')));
               $admissionid=$this->input->post('admissionid');

               $array=array(
                   'patientid'=>$patientid,
                   'patientvisitid'=>$patientvisitid,
                   'admissionid'=>$admissionid,
                   'currentdiagnosis'=>$currentdiagnosis,
                   'treatment'=>$treatment,
                   'procedures'=>$procedures,
                   'medications'=>$medications,
                   'medicationeffect'=>$medicationeffect,
                   'medicationcomments'=>$medicationcomments,
                   'intakes'=>$intakes,
                   'outputs'=>$outputs,
                   'overallprogress'=>$overallprogress,
                   'remarks'=>$remarks,
                   'createdby'=>$this->session->userdata('user_id'),
               );

               $insrt=$this->Nursing_model->save_nurse_note($array);

               if($insrt){
                   redirect('Nursing/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid);
               }

               $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
           }
           
           $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'ADMITTED');
           $patient=$this->Reception_model->patient($patientid);
           $this->data['patient']=$this->Reception_model->patients($patient->id);
           $this->data['patientid']=$patientid;
           $this->data['patientvisitid']=$patientvisitid;
           $this->data['admissionid']=$admission[0]->admissionid;
           $this->data['title']='Write Nurse Notes';
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='nurse/write_nurse_notes';
           $this->load->view('nurse/template2',$this->data);
    }
    
    public function inpatient_discharge($patientid,$patientvisitid){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->form_validation->set_rules('reason','Discharge Reason','xss_clean|required');
            $this->form_validation->set_rules('dischargenotes','Discharge Notes','xss_clean|required');
            $this->form_validation->set_rules('patientid','Patient Id','xss_clean|required');
            $this->form_validation->set_rules('patientvisitid','Patient Visit Id','xss_clean|required');
            $this->form_validation->set_rules('admissionid','Admission Id','xss_clean|required');

            if($this->form_validation->run() == true){
                
                $reason=trim(nl2br($this->input->post('reason')));
                $dischargenotes=trim(nl2br($this->input->post('dischargenotes')));
                $admissionid=$this->input->post('admissionid');
                
                $array=array(
                    'status'=>'DISCHARGED',
                    'dischargereason'=>$reason,
                    'dischargenotes'=>$dischargenotes,
                    'dischargedby'=>$this->session->userdata('user_id'),
                    'dischargedon'=>date('Y-m-d H:i:s'),
                );

                $insrt=$this->Reception_model->check_out_patient($patientid,$patientvisitid,$array);

                if($insrt){
                    
                    $array=array(
                        'exitstatus'=>1,
                        'modifiedby'=>$this->session->userdata('user_id'),
                        'modifiedon'=>date('Y-m-d H:i:s'),
                    );
                    
                   $this->Reception_model->update_patient_visit_status($patientvisitid,$array);
                   require_once 'reports/clinical/patient_discharge_form.php';
                   redirect('Nursing/in_patients/','refresh');
                }
                
                $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">discharge failed!</div>';
            }
            
            $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'PENDING DISCHARGE');
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['admissionid']=$admission[0]->admissionid;;
            $this->data['title']='Patient Discharge';
            $this->data['pending_debts']=$this->Reception_model->patient_debt_details($patientid);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/patient_discharge';
            $this->load->view('nurse/template2',$this->data);
    }
    
/**********************************************reports****************************************************************************************************/
    
    public function inpatients_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'Admission Report','link'=>'Nursing/inpatients_report'),
                                      array('title'=>'In Patient Transfers','link'=>'Nursing/inpatient_transfers_report'),
                                      array('title'=>'Out Patients Report','link'=>'Nursing/outpatients_report'),
                                      array('title'=>'Patient File','link'=>'Nursing/patient_medical_file'),
                                      
                                  );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('ward')) {
               $key['ward'] = $this->input->post('ward');
               $this->data['ward']=$this->input->post('ward');
           }
           
           if ($this->input->post('bed')) {
               $key['bed'] = $this->input->post('bed');
               $this->data['bed']=$this->input->post('bed');
           }

           if ($this->input->post('status')) {
               $key['status'] = $this->input->post('status');
               $this->data['status']=$this->input->post('status');
           }
           
           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['ward'] = $exp[5];
               $this->data['ward']=$key['ward'];
               
               $key['bed'] = $exp[7];
               $this->data['bed']=$key['bed'];
               
               $key['status'] = $exp[9];
               $this->data['status']=$key['status'];
               
               $docType = $exp[11];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $ward =$key['ward'];
            $bed =$key['bed'];
            $status =$key['status'];
            
            if($docType == 1){
                
                    $data=$this->Nursing_model->admissions($start,$end,$ward,$bed,$status);
                    require_once 'reports/clinical/admission_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Nursing_model->admissions($start,$end,$ward,$bed,$status);
                 require_once 'reports/clinical/admission_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Nursing/inpatients_report/start_".$key['start']."_end_".$key['end']."_ward_".$key['ward']."_bed_".$key['bed']."_status_".$status."/";
           $config["total_rows"] =$this->Nursing_model->admission_report_count($start,$end,$ward,$bed,$status);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Nursing_model->admission_report($start,$end,$ward,$bed,$status,$page,$limit);
           
           $this->data['title']="Admission Report";
           $this->data['wards']=$this->Inventory_model->units(null,$this->config->item('ward_unit_category_code'));
           $this->data['admission_status']=$this->config->item('admission_status');
           $this->data['beds']=$this->Administration_model->ward_beds();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='nurse/admission_report';
           $this->load->view('nurse/template',$this->data);
   }
    
    public function inpatient_transfers_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'Admission Report','link'=>'Nursing/inpatients_report'),
                                      array('title'=>'In Patient Transfers','link'=>'Nursing/inpatient_transfers_report'),
                                      array('title'=>'Out Patients Report','link'=>'Nursing/outpatients_report'),
                                      array('title'=>'Patient File','link'=>'Nursing/patient_medical_file'),
                                      
                                  );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
            }
           
           if ($this->input->post('bed')) {
               $key['bed'] = $this->input->post('bed');
               $this->data['bed']=$this->input->post('bed');
           }

           if ($this->input->post('ward')) {
               $key['ward'] = $this->input->post('ward');
               $this->data['ward']=$this->input->post('ward');
           }
           
           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['ward'] = $exp[5];
               $this->data['ward']=$key['ward'];
               
               $key['bed'] = $exp[7];
               $this->data['bed']=$key['bed'];
               
               $key['pid'] = $exp[9];
               $this->data['pid']=$key['pid'];
               
               $docType = $exp[11];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $ward =$key['ward'];
            $bed =$key['bed'];
            $pid =$key['pid'];
            
            if($docType == 1){
                
                    $data=$this->Nursing_model->admission_transfers($start,$end,$ward,$bed,$pid);
                    require_once 'reports/clinical/admission_transfer_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Nursing_model->admission_transfers($start,$end,$ward,$bed,$pid);
                 require_once 'reports/clinical/admission_transfer_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Nursing/inpatient_transfers_report/start_".$start."_end_".$end."_ward_".$ward."_bed_".$bed."_pid_".$pid."/";
           $config["total_rows"] =$this->Nursing_model->admission_transfer_report_count($start,$end,$ward,$bed,$pid);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Nursing_model->admission_transfer_report($start,$end,$ward,$bed,$pid,$page,$limit);
           
           $this->data['title']="In Patient Transfers Report";
           $this->data['wards']=$this->Inventory_model->units(null,$this->config->item('ward_unit_category_code'));
           $this->data['beds']=$this->Administration_model->ward_beds();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='nurse/admission_transfer_report';
           $this->load->view('nurse/template',$this->data);
   }
   
    public function outpatients_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'Admission Report','link'=>'Nursing/inpatients_report'),
                                      array('title'=>'In Patient Transfers','link'=>'Nursing/inpatient_transfers_report'),
                                      array('title'=>'Out Patients Report','link'=>'Nursing/outpatients_report'),
                                      array('title'=>'Patient File','link'=>'Nursing/patient_medical_file'),
                                      
                                  );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('visitcategory')) {
               $key['visitcategory'] = $this->input->post('visitcategory');
               $this->data['visitcategory']=$this->input->post('visitcategory');
           }
           
           if ($this->input->post('consultationcategory')) {
               $key['consultationcategory'] = $this->input->post('consultationcategory');
               $this->data['consultationcategory']=$this->input->post('consultationcategory');
           }

           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['visitcategory'] = $exp[5];
               $this->data['visitcategory']=$key['visitcategory'];
               
               $key['consultationcategory'] = $exp[7];
               $this->data['consultationcategory']=$key['consultationcategory'];
               
               $docType = $exp[9];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $visitcategory =$key['visitcategory'];
            $consultationcategory =$key['consultationcategory'];
            
            if($docType == 1){
                
                    $data=$this->Reception_model->outpatients($start,$end,$visitcategory,$consultationcategory);
                    require_once 'reports/reception/outpatients_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->outpatients($start,$end,$visitcategory,$consultationcategory);
                 require_once 'reports/reception/outpatients_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Nursing/outpatients_report/start_".$key['start']."_end_".$key['end']."_visiticateg_".$key['visitcategory']."_consultationcateg_".$key['consultationcategory']."/";
           $config["total_rows"] =$this->Reception_model->outpatients_report_count($start,$end,$visitcategory,$consultationcategory);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Reception_model->outpatients_report($start,$end,$visitcategory,$consultationcategory,$page,$limit);
           
           //consultation department
           $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
           
           $this->data['title']="Out Patients Report";
           $this->data['consultationcategories']=$this->Administration_model->consultation_categories($dept[0]->id,null);
           $this->data['visitcategories']=$this->Reception_model->visit_categories();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='nurse/outpatients_report';
           $this->load->view('nurse/template',$this->data);
   }
   
    public function patient_medical_file($patientid,$export){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'Admission Report','link'=>'Nursing/inpatients_report'),
                                      array('title'=>'In Patient Transfers','link'=>'Nursing/inpatient_transfers_report'),
                                      array('title'=>'Out Patients Report','link'=>'Nursing/outpatients_report'),
                                      array('title'=>'Patient File','link'=>'Nursing/patient_medical_file'),
                                  );
            
            if($patientid <> null){
                
                $data=$this->Reception_model->patient_visits($patientid);
                
                if($export == TRUE){
                    
                    require_once 'report/clinical/patient_medical_file.php';
                }
                
            }
            
            $this->form_validation->set_rules('patientid','Patient Id','xss_clean|required');

            if($this->form_validation->run() == true){
                
                $patientid=$this->input->post('patientid');
                 $data=$this->Reception_model->patient_visits($patientid);
                
            }
           
           $this->data['title']="Patient File";
           $this->data['patientid'] =$patientid;
           $this->data['visits']=$data;
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['patientFile']='nurse/patient_file_data';
           $this->data['content']='nurse/patient_file';
           $this->load->view('nurse/template',$this->data);
   }
    
    public function patient_file_details($patientid,$patientvisitid,$detailType){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
        
        if($detailType == 'PTOO1'){
            
            $this->data['filedata']=$this->Clinical_model->patient_physcial_examinations($patientid,$patientvisitid,null,null);
            $this->data['content']='common/patient_file_physical_examinations';
            
        }else if($detailType == 'PTOO2'){
            $this->data['filedata']=$this->Clinical_model->patient_vitals_examinations($patientid,$patientvisitid,null,null);
            $this->data['content']='common/patient_file_vital_examinations';
            
        }else if($detailType == 'PTOO3'){
            $this->data['filedata']=$this->Clinical_model->patient_diagnosis($patientid,$patientvisitid);
            $this->data['content']='common/patient_file_diagnosis';
            
        }else if($detailType == 'PTOO4'){
            $this->data['filedata']=$this->Clinical_model->patient_prescriptions($patientid,$patientvisitid,null,null,null);
            $this->data['content']='common/patient_file_prescriptions';
            
        }else if($detailType == 'PTOO5'){
            $this->data['filedata']=$this->Clinical_model->patient_service_orders($patientid,$patientvisitid,null,null,null);
            $this->data['content']='common/patient_file_services';
            
        }else if($detailType == 'PTOO6'){
            $this->data['filedata']=$this->Nursing_model->inpatient_nurse_notes($patientid,$patientvisitid,null,null);
            $this->data['content']='common/patient_file_nurse_notes';
            
        }else if($detailType == 'PTOO7'){
            $this->data['filedata']=$this->Clinical_model->inpatient_doctor_notes($patientid,$patientvisitid,null,null);
            $this->data['content']='common/patient_file_doctor_notes';
            
        }else{
            
            $this->data['filedata']=$this->Clinical_model->patient_allegies($patientid,$patientvisitid,null,null,null);
            $this->data['content']='common/patient_file_allegies';
        }
        
        $this->data['title']="Patient File";
        $patient=$this->Reception_model->patient($patientid);
        $this->data['patient']=$this->Reception_model->patients($patient->id);
        $this->data['patientid']=$patientid;
        $this->data['patientvisitid']=$patientvisitid;
        $this->data['identifier']='ID01';
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->load->view('nurse/template2',$this->data);
    }
    
/**********************************************configurations********************************************************************************************/
    
    public function admission_sources(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'Admission Sources','link'=>'Nursing/admission_sources'),
                                      array('title'=>'Admission Types','link'=>'Nursing/admission_types'),
                                      array('title'=>'Nurses','link'=>'Nursing/nurses'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           
           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
            }

           $name =$key['name'];

           $config["base_url"] = base_url() . "index.php/Nursing/admission_sources/name_".$key['name']."/";
           $config["total_rows"] =$this->Nursing_model->admissionsources_info_count($name);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['admissionsources'] = $this->Nursing_model->admissionsources_info($name,$page,$limit);

           $this->data['title']="Admission Sources";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='nurse/admission_sources';
           $this->load->view('nurse/template',$this->data);
   }
    
    public function add_admission_source($id){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                      array('title'=>'Admission Sources','link'=>'Nursing/admission_sources'),
                                      array('title'=>'Admission Types','link'=>'Nursing/admission_types'),
                                      array('title'=>'Nurses','link'=>'Nursing/nurses'),
                                  );
            
            $this->form_validation->set_rules('name','Name','xss_clean|required');

            if($this->form_validation->run() == true){
                $name=$this->input->post('name');
                
                $data=array(
                          'name'=>$name,
                       );
                
                //do saving
                $resp=$this->Nursing_model->save_admissionsource($data,$id);

                  if($resp){

                      redirect(current_url());
                  }else{

                      $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                  }
            }
        
            $this->data['id']=$id;
            $this->data['admissionsource']=$this->Nursing_model->admissionsources($id);
            $this->data['title']='Add Admission Source';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/add_admission_source';
            $this->load->view('nurse/template',$this->data);
    }
    
    public function activate_deactivate_admissionsource($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Nursing_model->activate_deactivate_admissionsource($status,$id);
        redirect('Nursing/admission_sources','refresh');
    }
    
    public function activate_deactivate_admissiontype($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Nursing_model->activate_deactivate_admissiontype($status,$id);
        redirect('Nursing/admission_types','refresh');
    }
    
    public function admission_types(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'Admission Sources','link'=>'Nursing/admission_sources'),
                                      array('title'=>'Admission Types','link'=>'Nursing/admission_types'),
                                      array('title'=>'Nurses','link'=>'Nursing/nurses'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           
           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
            }

           $name =$key['name'];

           $config["base_url"] = base_url() . "index.php/Nursing/admission_sources/name_".$key['name']."/";
           $config["total_rows"] =$this->Nursing_model->admissiontypes_info_count($name);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['admissiontypes'] = $this->Nursing_model->admissiontypes_info($name,$page,$limit);

           $this->data['title']="Admission Types";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='nurse/admission_types';
           $this->load->view('nurse/template',$this->data);
   }
   
    public function add_admission_type($id){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
             $this->data['side_menu']=array(
                                      array('title'=>'Admission Sources','link'=>'Nursing/admission_sources'),
                                      array('title'=>'Admission Types','link'=>'Nursing/admission_types'),
                                      array('title'=>'Nurses','link'=>'Nursing/nurses'),
                                  );
            
            $this->form_validation->set_rules('name','Name','xss_clean|required');

            if($this->form_validation->run() == true){
                $name=$this->input->post('name');
                
                $data=array(
                          'name'=>$name,
                       );
                
                //do saving
                $resp=$this->Nursing_model->save_admissiontype($data,$id);

                  if($resp){

                      redirect(current_url());
                  }else{

                      $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                  }
            }
        
            $this->data['id']=$id;
            $this->data['admissiontype']=$this->Nursing_model->admissiontypes($id);
            $this->data['title']='Add Admission Type';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/add_admission_type';
            $this->load->view('nurse/template',$this->data);
    }
    
    public function nurses(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'Admission Sources','link'=>'Nursing/admission_sources'),
                                      array('title'=>'Admission Types','link'=>'Nursing/admission_types'),
                                      array('title'=>'Nurses','link'=>'Nursing/nurses'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('ward')) {
               $key['ward'] = $this->input->post('ward');
               $this->data['ward']=$this->input->post('ward');
           }
           
           
           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['ward'] = $exp[3];
               $this->data['ward']=$key['ward'];
            }

           $name =$key['name'];
           $ward =$key['ward'];

           $config["base_url"] = base_url() . "index.php/Nursing/nurses/name_".$name."_ward_".$ward."/";
           $config["total_rows"] =$this->Administration_model->nurses_info_count($name,$ward);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] = $this->Administration_model->nurses_info($name,$ward,$page,$limit);

           $this->data['title']="Nurses";
           $this->data['wards']=$this->Inventory_model->units(null,$this->config->item('ward_unit_category_code'));
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='nurse/nurses';
           $this->load->view('nurse/template',$this->data);
   }
   
    public function allocate_reallocate_nurse($id){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                      array('title'=>'Admission Sources','link'=>'Nursing/admission_sources'),
                                      array('title'=>'Admission Types','link'=>'Nursing/admission_types'),
                                      array('title'=>'Nurses','link'=>'Nursing/nurses'),
                                  );
            
            $this->form_validation->set_rules('ward','Ward','xss_clean|required');

            if($this->form_validation->run() == true){
                $ward=$this->input->post('ward');
                
                $data=array(
                          'inventoryunit'=>$ward,
                       );
                
                //do saving
                $resp=$this->Nursing_model->allocate_reallocate_nurse($data,$id);

                if($resp){

                    redirect('Nursing/nurses','refresh');
                }else{

                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                }
            }
        
            $this->data['id']=$id;
            $this->data['wards']=$this->Inventory_model->units(null,$this->config->item('ward_unit_category_code'));
            $this->data['title']='Nurse Allocation/Re Allocation';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/allocate_reallocate_nurse';
            $this->load->view('nurse/template',$this->data);
    }

/**************************************************************************user account*************************************************************************/
    
    public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Nursing/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Nursing/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['profile']=$this->SuperAdministration_model->profile_data();
                $this->data['content']='nurse/profile';
		$this->load->view('nurse/template',$this->data);
        }
        
    public function edit_profile(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Nursing/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Nursing/change_password'),
                                        );
            
            $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
            $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
            $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
            $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
            $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if($this->form_validation->run() == true){

                 $firstname=$this->input->post('first_name');
                 $middlename=$this->input->post('middle_name');
                 $lastname=$this->input->post('last_name');
                 $username=$this->input->post('username');
                 $email=$this->input->post('email');
                 $mobile=$this->input->post('mobile');

                $user=array(
                            'FIRST_NAME'=>$firstname,
                            'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                            'LAST_NAME'=>$lastname,
                            'USERNAME'=>$username,
                            'EMAIL'=>$email<>null?$email:null,
                            'MSISDN'=>$mobile,
                        );

                $user_group=array(
                            'GROUP_ID'=>$this->session->userdata('group')
                        );

                $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                if($username_check){


                    //do registration
                     $resp=$this->SuperAdministration_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                       if($resp){

                           redirect('Nursing/profile','refresh');
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                       }
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                }
            }

            $this->data['title']="Edit Profile";
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['content']='nurse/edit_profile';
            $this->load->view('nurse/template',$this->data);
    }
        
    public function change_password(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Nursing/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Nursing/change_password'),
                                        );

            $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
            $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 

            if($this->form_validation->run() == true){

                $password=$this->input->post('password');
                $pchange=$this->SuperAdministration_model->change_password($password);

                if($pchange){
                           
                        $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">password change successfull!</div>';
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">password change failed!</div>';
                }
            }

            $this->data['title']="Change Password";
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='nurse/change_password';
            $this->load->view('nurse/template',$this->data);
    }
    
/**************************************************************************************other**************************************************************/ 
    
    function cur_password($password){
            
            $pf=$this->SuperAdministration_model->profile_data();
            $hashed_old=$this->SuperAdministration_model->hash_db_password($password,$pf);
           
             if($pf->password <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
    }
    
    function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('Nurse')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
    
    function username_uniqueness($username,$id){
            
        return $this->SuperAdministration_model->check_username($username,$id);
    }
    
    function appointment_date($date){
        
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
    }
    
    function appointment_time($time) {
        
            if (is_object(DateTime::createFromFormat('H:i', $time))) {
                return TRUE;
            } else {
                $this->form_validation->set_message('appointment_time', "The %s must contain hours:minutes in 24 hours");
                return FALSE;
            }
    }
    
    function modal_patient_admission_details(){
        $id=$this->input->post('admissionid');
        $patientadmission=$this->Nursing_model->admission_details(null,null,$id,null);
        $data="";
        
        $ward=$this->Inventory_model->active_units($patientadmission[0]->wardid,$this->config->item('ward_unit_category_code'));
        $bed=$this->Administration_model->ward_beds($patientadmission[0]->bedid);
        $admissiontype=$this->Nursing_model->admissiontypes($patientadmission[0]->admissiontype);
        $admissionsource=$this->Nursing_model->admissionsources($patientadmission[0]->admissionsource);
        $data .=$patientadmission[0]->patientid.'=_'.$patientadmission[0]->patientvisitid.'=_'.$patientadmission[0]->admissionid.'=_'.$ward[0]->name.'=_'.$bed[0]->bedidentity.'=_';
        $data .=$admissionsource[0]->name.'=_'.$admissiontype[0]->name;
        
        echo $data;
        exit;
    }
    
    
}