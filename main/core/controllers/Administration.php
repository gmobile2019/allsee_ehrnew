<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller {

    function __construct() {
        parent::__construct();
       
        $this->load->library('upload');
	$this->load->model('Inventory_model');
	$this->load->model('Nursing_model');
	$this->load->model('Reception_model');
        
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Home';
            $this->data['content']='administrator/dashboard';
            $this->load->view('administrator/template3',$this->data);
    }
    
    public function view_users()
	{
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                 $this->data['side_menu']=array(
                                            array('title'=>'Create Users','link'=>'Administration/create_users'),
                                            array('title'=>'View Users','link'=>'Administration/view_users'),
                                        );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('group')) {
                    $key['group'] = $this->input->post('group');
                    $this->data['group']=$this->input->post('group');
                }

               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['group'] = $exp[3];
                    $this->data['group']=$key['group'];
                }
                
                $name =$key['name'];
                $group =$key['group'];
                
                $config["base_url"] = base_url() . "index.php/Administration/view_users/name_".$key['name']."_group_".$key['group']."/";
                $config["total_rows"] =$this->Administration_model->member_info_count($name,$group);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['members'] = $this->Administration_model->member_info($name,$group,$page,$limit);
                
                $this->data['title']="System Users";
                $this->data['groups']=$this->SuperAdministration_model->get_registration_groups($this->session->userdata('group'));
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='administrator/view_users';
		$this->load->view('administrator/template',$this->data);
	}
    
    public function create_users($id=null)
   {
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'Create Users','link'=>'Administration/create_users'),
                                            array('title'=>'View Users','link'=>'Administration/view_users'),
                                        );
           $error=FALSE;
           $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
           $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
           $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
           $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
           $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
           $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');
           $this->form_validation->set_rules('group','User Group','required|xss_clean');
           $this->form_validation->set_rules('nurse_pharmacist_category', 'Inventory Unit', 'xss_clean');
           $this->form_validation->set_rules('nurse_incharge', 'Incharge', 'xss_clean');
           
            if($id <> null){

               if($this->input->post('password') <> null){

                 $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
                 $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');   
               }
           }else{

             $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
             $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');  
           }

           $group=$this->input->post('group');
           
           if($group == 4){
               $this->form_validation->set_rules('doctor_categ', 'Doctor Category', 'required|xss_clean');
           }

            if($group == 9){
               $this->form_validation->set_rules('investigation_categ', 'Investigation Category', 'required|xss_clean');
            }
           
            if ($this->form_validation->run() == TRUE)
           {

                $nurse_incharge=FALSE;
                $firstname=$this->input->post('first_name');
                $middlename=$this->input->post('middle_name');
                $lastname=$this->input->post('last_name');
                $username=$this->input->post('username');
                $email=$this->input->post('email');
                $mobile=$this->input->post('mobile');
                
                $password=$this->input->post('password');

                

                       $user=array(
                           'first_name'=>$firstname,
                           'middle_name'=>$middlename<>null?$middlename:null,
                           'ip_address'=>$this->input->ip_address(),
                           'last_name'=>$lastname,
                           'username'=>$username,
                           'email'=>$email<>null?$email:null,
                           'msisdn'=>$mobile,
                       );

                       $user_group=array(
                           'group_id'=>$group
                       );
                       
                       if($group == 4){
                           
                           $addon=array(
                               'doctorcategory'=>$this->input->post('doctor_categ'),
                               'lastupdate'=>date('Y-m-d H:i:s'),
                           );
                       }
                       
                       if($group == 9){
                           
                           $addon=array(
                               'investigationcategory'=>$this->input->post('investigation_categ'),
                               'lastupdate'=>date('Y-m-d H:i:s'),
                               'supervisor'=>$this->input->post('examiner_incharge')<>null?1:0,
                           );
                       }
                       
                       if(($group == 3 && $this->input->post('nurse_incharge') == null) || $group == 5){
                           
                           $addon=array(
                               'inventoryunit'=>$this->input->post('inv_unit'),
                               'createdby'=>$this->session->userdata('user_id'),
                           );
                       }
                       
                       if($group == 3 && $this->input->post('nurse_incharge') <> null){
                            $nurse_incharge=TRUE;
                        }
                       //check username uniqueness
                       $username_check=$this->username_uniqueness($username,$id);
                       if($username_check){


                           //do registration
                            $resp=$this->SuperAdministration_model->system_user_registration($user,$user_group,$id,$password,$addon,$nurse_incharge);

                              if($resp){
                                  
                                  
                                  redirect(current_url());
                              }else{
                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                              }
                       }else{
                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                        }
               
           }
           
           //consultation department
           $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
          
           $this->data['title']="Create Users";
           $this->data['id']="$id";
           $this->data['member']=$this->SuperAdministration_model->get_member_info($id);
           $this->data['doctor_categs']=$this->Administration_model->consultation_categories($dept[0]->id,null);
           $this->data['investigation_categs']=$this->SuperAdministration_model->investigation_categories(null,null,1);
           $this->data['units']=$this->Inventory_model->active_units();
           $this->data['groups']=$this->SuperAdministration_model->get_registration_groups($this->session->userdata('group'));
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/create_users';
           $this->load->view('administrator/template',$this->data);
    }
     
    public function activate_deactivate_users($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_users($id,$status);
        redirect('Administration/view_users','refresh');
    }
    
    public function fill_institution_profile()
   {
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'Fill Profile','link'=>'Administration/fill_institution_profile'),
                                            array('title'=>'View Profile','link'=>'Administration/view_institution_profile'),
                                        );
           $error=FALSE;
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('category','Category','xss_clean|required');
           $this->form_validation->set_rules('phone','Phone','required|xss_clean|numeric');
           $this->form_validation->set_rules('postal','Postal Address','xss_clean|numeric');
           $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
           $this->form_validation->set_rules('city', 'City', 'required|xss_clean');
           $this->form_validation->set_rules('website','Website','xss_clean');
            

           

            if ($this->form_validation->run() == TRUE)
           {
                
                $name=$this->input->post('name');
                $phone=$this->input->post('phone');
                $email=$this->input->post('email');
                $postal=$this->input->post('postal');
                $city=$this->input->post('city');
                $website=$this->input->post('website');
                $category=$this->input->post('category');
                
                $institution=array(
                   'name'=>$name,
                   'category'=>$category,
                   'phone'=>$phone,
                   'postal'=>$postal<>null?$postal:null,
                   'city'=>$city,
                   'website'=>$website<>null?$website:null,
                   'email'=>$email,
               );
                        
                if($_FILES['logo']['name'] <> null){
                        $config['upload_path'] = './logo/';
                        $config['allowed_types'] = 'jpeg|jpg|png';
                        $filename=explode('.',$_FILES['logo']['name']);
                        $photo_ext=end($filename);
                        $logoname=$institution_id;
                        $filename=$logoname.".".$photo_ext;
                        $config['file_name']=$filename;
                        $config['overwrite']=true;
                        $this->upload->initialize($config);

                       if($this->upload->do_upload('logo')){



                           $logo=file_get_contents('./logo/'.$filename);
                           $logostring=base64_encode($logo);

                           $institution['logoname']=$filename;
                           $institution['logostring']=$logostring;
                        }else{
                          $this->data['message']=$this->upload->display_errors('<div class="alert alert-danger " role="alert" style="text-align:center;">','</div>');
                          $error=TRUE;
                       }
                   }

                   if(!$error){


                       //do registration
                        $resp=$this->SuperAdministration_model->save_institution_profile($institution);

                          if($resp){

                              redirect(current_url());
                          }else{

                              $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">not saved!</div>';
                          }
                   }
           }

           $this->data['title']="Fill Institution Profile";
           $this->data['id']="$id";
           $this->data['categories']=$this->SuperAdministration_model->institution_categories();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['content']='administrator/fill_Institution_profile';
           $this->load->view('administrator/template',$this->data);
    }
    
    public function view_institution_profile(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                       array('title'=>'Fill Profile','link'=>'Administration/fill_institution_profile'),
                                       array('title'=>'View Profile','link'=>'Administration/view_institution_profile'),
                                   );

           $this->data['title']="Institution Profile";
           $this->data['content']='administrator/health_institution_profile';
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->load->view('administrator/template',$this->data);
   }
      
    public function view_sponsors(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                      array('title'=>'View Sponsor(s)','link'=>'Administration/view_sponsors'),
                                      array('title'=>'View Payment Mode(s)','link'=>'Administration/view_paymentModes'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
           }

           $name =$key['name'];

           $config["base_url"] = base_url() . "index.php/Administration/view_sponsors/name_".$key['name']."/";
           $config["total_rows"] =$this->SuperAdministration_model->sponsor_info_count($name);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['sponsors'] = $this->SuperAdministration_model->sponsor_info($name,$page,$limit);

           $this->data['title']="Sponsors";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/view_sponsors';
           $this->load->view('administrator/template',$this->data);
   }
        
    public function activate_deactivate_sponsor($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_sponsor($status,$id);
        redirect('Administration/view_sponsors','refresh');
    }
    
    public function view_paymentModes(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                      array('title'=>'View Sponsor(s)','link'=>'Administration/view_sponsors'),
                                      array('title'=>'View Payment Mode(s)','link'=>'Administration/view_paymentModes'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

           if ($this->input->post('sponsor')) {
               $key['sponsor'] = $this->input->post('sponsor');
               $this->data['sponsor']=$this->input->post('sponsor');
           }

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];

               $key['sponsor'] = $exp[3];
               $this->data['sponsor']=$key['sponsor'];
           }

           $name =$key['name'];
           $sponsor =$key['sponsor'];

           $config["base_url"] = base_url() . "index.php/Administration/view_paymentModes/name_".$key['name']."_sponsor_".$key['sponsor']."/";
           $config["total_rows"] =$this->SuperAdministration_model->paymentMode_info_count($name,$sponsor);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['paymentModes'] = $this->SuperAdministration_model->paymentMode_info($name,$sponsor,$page,$limit);

           $this->data['title']="Payment Modes";
           $this->data['sponsors']=$this->SuperAdministration_model->sponsors();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/view_paymentModes';
           $this->load->view('administrator/template',$this->data);
   }
     
    public function activate_deactivate_paymentMode($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_paymentMode($status,$id);
        redirect('Administration/view_paymentModes','refresh');
    }
    
    public function view_departments(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                 $this->data['side_menu']=array(
                                            array('title'=>'View Department(s)','link'=>'Administration/view_departments'),
                                            array('title'=>'Add Department','link'=>'Administration/add_department'),
                                        );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                

               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                }
                
                $name =$key['name'];
                
                $config["base_url"] = base_url() . "index.php/Administration/view_departments/name_".$key['name']."/";
                $config["total_rows"] =$this->Administration_model->department_info_count($name);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['departments'] = $this->Administration_model->department_info($name,$page,$limit);
                
                $this->data['title']="Department(s)";
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='administrator/view_departments';
		$this->load->view('administrator/template',$this->data);
	}
    
    public function add_department($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'View Department(s)','link'=>'Administration/view_departments'),
                                            array('title'=>'Add Department','link'=>'Administration/add_department'),
                                        );
          
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('service_charge','Service Charge','xss_clean|required');
            
            if ($this->form_validation->run() == TRUE)
           {
                $name=$this->input->post('name');
                $servicechargeid=$this->input->post('service_charge');

                

                $department=array(
                    'name'=>$name,
                    'servicechargeid'=>$servicechargeid,
                );

                //save department details
                 $resp=$this->Administration_model->save_department($department,$id);

                   if($resp){

                       redirect(current_url());
                   }else{
                       $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                   }
            }

           $this->data['title']="Add Department";
           $this->data['id']="$id";
           $this->data['department']=$this->Administration_model->departments($id);
           $this->data['service_charges']=$this->Administration_model->service_charge();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/add_department';
           $this->load->view('administrator/template',$this->data);
    }
     
    public function activate_deactivate_department($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Administration_model->activate_deactivate_department($id,$status);
        redirect('Administration/view_departments','refresh');
    }
    
    public function view_subdepartments(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                 $this->data['side_menu']=array(
                                            array('title'=>'View Sub Department(s)','link'=>'Administration/view_subdepartments'),
                                            array('title'=>'Add Sub Department','link'=>'Administration/add_subdepartment'),
                                        );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('department')) {
                    $key['department'] = $this->input->post('department');
                    $this->data['department']=$this->input->post('department');
                }
                

               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['department'] = $exp[3];
                    $this->data['department']=$key['department'];
                    
                }
                
                $name =$key['name'];
                $department =$key['department'];
                
                $config["base_url"] = base_url() . "index.php/Administration/view_subdepartments/name_".$key['name']."_department_".$key['department']."/";
                $config["total_rows"] =$this->Administration_model->subdepartment_info_count($name,$department);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['subdepartments'] = $this->Administration_model->subdepartment_info($name,$department,$page,$limit);
                
                $this->data['title']="Sub Department(s)";
                $this->data['departments']=$this->Administration_model->active_departments();
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='administrator/view_subdepartments';
		$this->load->view('administrator/template',$this->data);
	}
    
    public function add_subdepartment($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'View Sub Department(s)','link'=>'Administration/view_subdepartments'),
                                            array('title'=>'Add Sub Department','link'=>'Administration/add_subdepartment'),
                                        );
          
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('department','Department','xss_clean|required');
            
            if ($this->form_validation->run() == TRUE)
           {
                $name=$this->input->post('name');
                $servicechargeid=$this->input->post('department');

                

                $subdepartment=array(
                    'name'=>$name,
                    'departmentid'=>$servicechargeid,
                );

                //save department details
                 $resp=$this->Administration_model->save_subdepartment($subdepartment,$id);

                   if($resp){

                       redirect(current_url());
                   }else{
                       $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                   }
            }

           $this->data['title']="Add Sub Department";
           $this->data['id']="$id";
           $this->data['subdepartment']=$this->Administration_model->subdepartments($id);
           $this->data['departments']=$this->Administration_model->active_departments();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/add_subdepartment';
           $this->load->view('administrator/template',$this->data);
    }
     
    public function activate_deactivate_subdepartment($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Administration_model->activate_deactivate_subdepartment($id,$status);
        redirect('Administration/view_subdepartments','refresh');
    }
    
    public function view_services(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                            array('title'=>'View Service(s)','link'=>'Administration/view_services'),
                                            array('title'=>'Add Service','link'=>'Administration/add_service'),
                                            array('title'=>'View Service(s) Cost','link'=>'Administration/view_service_costs'),
                                            array('title'=>'Add Service Cost','link'=>'Administration/add_service_cost'),
                                        );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('department')) {
                    $key['department'] = $this->input->post('department');
                    $this->data['department']=$this->input->post('department');
                }
                
                if ($this->input->post('subdepartment')) {
                    $key['subdepartment'] = $this->input->post('subdepartment');
                    $this->data['subdepartment']=$this->input->post('subdepartment');
                }

               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['department'] = $exp[3];
                    $this->data['department']=$key['department'];
                    
                    $key['subdepartment'] = $exp[5];
                    $this->data['subdepartment']=$key['subdepartment'];
                    
                    $docType = $exp[7];
                    
                }
                
                $name =$key['name'];
                $department =$key['department'];
                $subdepartment =$key['subdepartment'];
                
                if($docType == 1){
                    $data=$this->Administration_model->service_info_report($name,$department,$subdepartment);
                    require_once 'reports/administrator/services_pdf.php';
                }

                if($docType == 2){

                     $data=$this->Administration_model->service_info_report($name,$department,$subdepartment);
                     require_once 'reports/administrator/services_excel.php';
                }
        
                $config["base_url"] = base_url() . "index.php/Administration/view_services/name_".$key['name']."_department_".$key['department']."_subdepartment_".$key['subdepartment']."/";
                $config["total_rows"] =$this->Administration_model->service_info_count($name,$department,$subdepartment);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['services'] = $this->Administration_model->service_info($name,$department,$subdepartment,$page,$limit);
                
                $this->data['title']="Service(s)";
                $this->data['departments']=$this->Administration_model->active_departments();
                $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='administrator/view_services';
		$this->load->view('administrator/template',$this->data);
	}
    
    public function add_service($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'View Service(s)','link'=>'Administration/view_services'),
                                            array('title'=>'Add Service','link'=>'Administration/add_service'),
                                            array('title'=>'View Service(s) Cost','link'=>'Administration/view_service_costs'),
                                            array('title'=>'Add Service Cost','link'=>'Administration/add_service_cost'),
                                        );
          
           
            $this->form_validation->set_rules('addType','Addition Type','xss_clean|required');
            $this->form_validation->set_rules('name','Name','xss_clean|required');
            $this->form_validation->set_rules('subdepartment','Sub Department','xss_clean|required');

           
            if ($this->form_validation->run() == TRUE)
           {
                $error=TRUE;
                $subdepartment=$this->input->post('subdepartment');
                $department=$this->Administration_model->subdepartments($subdepartment);
                $addType=$this->input->post('addType');
                if($addType == 1){
                    
                    $name=$this->input->post('name');
                    $subdepartment_array=array(
                                            'name'=>$name,
                                            'departmentid'=>$department[0]->departmentid,
                                            'subdepartmentid'=>$subdepartment,
                                        );

                   
                        //save service details
                        $resp=$this->Administration_model->save_service($subdepartment_array,$id);
                        if($resp){
                            $error=FALSE;
                            redirect(current_url());
                        }
                }else{
                    
                        $config['upload_path'] = './excel/';
                        $config['allowed_types'] = 'xls';
                        $filename=explode('.',$_FILES['upload']['name']);
                        $excel_ext=end($filename);
                        $excelname=date('YmdHis');
                        $filename='Services'.$excelname.".".$excel_ext;
                        $config['file_name']=$filename;
                        $config['overwrite']=true;
                        $this->upload->initialize($config);
                      
                    if($this->upload->do_upload('upload')){

                        $this->excel_reader->read('./excel/'.$filename);
                        $data = $this->excel_reader->worksheets[0];


                        //loop through the worksheet
                        foreach($data as $value){

                            $id=$this->Administration_model->get_service_by_name($value[0]);
                            
                             $subdepartment_array=array(
                                                    'name'=>$value[0],
                                                    'departmentid'=>$department[0]->departmentid,
                                                    'subdepartmentid'=>$subdepartment,
                                                );
                             
                            //save service details
                            $resp=$this->Administration_model->save_service($subdepartment_array,$id->id); 
                            if($resp){
                                $error=FALSE;
                            }
                        }
                        
                        if($error == FALSE){
                            redirect(current_url());
                        }
                    }else{
                        
                        $this->data['message']=$this->upload->display_errors('<div class="alert alert-danger " role="alert" style="text-align:center;">','</div>');
                       
                    }
                }
            }

           $this->data['title']="Add Service";
           $this->data['id']="$id";
           $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
           $this->data['service']=$this->Administration_model->services($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/add_service';
           $this->load->view('administrator/template',$this->data);
    }
    
    public function activate_deactivate_service($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
               
        $this->Administration_model->activate_deactivate_service($id,$status);
        redirect('Administration/view_services','refresh');
    }
    
    public function view_service_costs(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                            array('title'=>'View Service(s)','link'=>'Administration/view_services'),
                                            array('title'=>'Add Service','link'=>'Administration/add_service'),
                                            array('title'=>'View Service(s) Cost','link'=>'Administration/view_service_costs'),
                                            array('title'=>'Add Service Cost','link'=>'Administration/add_service_cost'),
                                        );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('department')) {
                    $key['department'] = $this->input->post('department');
                    $this->data['department']=$this->input->post('department');
                }
                
                if ($this->input->post('subdepartment')) {
                    $key['subdepartment'] = $this->input->post('subdepartment');
                    $this->data['subdepartment']=$this->input->post('subdepartment');
                }

                if ($this->input->post('sponsor')) {
                    $key['sponsor'] = $this->input->post('sponsor');
                    $this->data['sponsor']=$this->input->post('sponsor');
                }
                
               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['department'] = $exp[3];
                    $this->data['department']=$key['department'];
                    
                    $key['subdepartment'] = $exp[5];
                    $this->data['subdepartment']=$key['subdepartment'];
                    
                    $key['sponsor'] = $exp[7];
                    $this->data['sponsor']=$key['sponsor'];
                    
                    $docType = $exp[9];
                    
                }
                
                $name =$key['name'];
                $department =$key['department'];
                $subdepartment =$key['subdepartment'];
                $sponsor =$key['sponsor'];
                
                if($docType == 1){
                    $data=$this->Administration_model->services_costs_info_report($name,$department,$subdepartment,$sponsor);
                    require_once 'reports/administrator/services_cost_pdf.php';
                }

                if($docType == 2){

                     $data=$this->Administration_model->services_costs_info_report($name,$department,$subdepartment,$sponsor);
                     require_once 'reports/administrator/services_cost_excel.php';
                }
                
                $config["base_url"] = base_url() . "index.php/Administration/view_service_costs/name_".$key['name']."_department_".$key['department']."_subdepartment_".$key['subdepartment']."_sponsor_".$key['sponsor']."/";
                $config["total_rows"] =$this->Administration_model->service_cost_info_count($name,$department,$subdepartment,$sponsor);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['service_costs'] = $this->Administration_model->service_cost_info($name,$department,$subdepartment,$sponsor,$page,$limit);
                
                $this->data['title']="Service Cost(s)";
                $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
                $this->data['departments']=$this->Administration_model->active_departments();
                $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='administrator/view_service_costs';
		$this->load->view('administrator/template',$this->data);
	}
    
    public function add_service_cost($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'View Service(s)','link'=>'Administration/view_services'),
                                            array('title'=>'Add Service','link'=>'Administration/add_service'),
                                            array('title'=>'View Service(s) Cost','link'=>'Administration/view_service_costs'),
                                            array('title'=>'Add Service Cost','link'=>'Administration/add_service_cost'),
                                        );
          
           
            $this->form_validation->set_rules('sponsor','Sponsor','xss_clean');
            $this->form_validation->set_rules('addType','Addition Type','xss_clean|required');

            $addType=$this->input->post('addType');
            if($addType == 1){
                
                $this->form_validation->set_rules('service','Service','required|xss_clean');
                $this->form_validation->set_rules('cost','Cost','required|xss_clean|numeric');
            }
            
            if ($this->form_validation->run() == TRUE)
           {
                
                $addType=$this->input->post('addType');
                $sponsor=$this->input->post('sponsor');
                $cost=$this->input->post('cost');
                $error=TRUE;
                
                
                if($addType == 1){
                    
                    $service=$this->input->post('service');
                    $department=$this->Administration_model->services($service);
                    
                    $service_cost_array=array(
                                            'serviceid'=>$service,
                                            'departmentid'=>$department[0]->departmentid,
                                            'subdepartmentid'=>$department[0]->subdepartmentid,
                                        );

                        if($sponsor == null && $id == null){
                            
                            $sponsors=$this->SuperAdministration_model->active_sponsors();
                            foreach($sponsors as $key=>$value){
                                
                                $service_cost_array['sponsorid']=$value->sponsorcode;
                                $check=$this->Administration_model->check_service_cost($service_cost_array);
                                $service_cost_array['cost']=$cost;
                                //save service details
                                $resp=$this->Administration_model->save_service_cost($service_cost_array,$check->id);
                                if($resp){
                                    $error=FALSE;
                              }
                            }
                            
                        }else{
                            
                            $service_cost_array['sponsorid']=$sponsor;
                             $check=$this->Administration_model->check_service_cost($service_cost_array);
                             $service_cost_array['cost']=$cost;
                            //save service details
                            $resp=$this->Administration_model->save_service_cost($service_cost_array,$check->id);
                            if($resp){
                              $error=FALSE;
                            }
                        }
                }else{
                    
                        $config['upload_path'] = './excel/';
                        $config['allowed_types'] = 'xls';
                        $filename=explode('.',$_FILES['upload']['name']);
                        $excel_ext=end($filename);
                        $excelname=date('YmdHis');
                        $filename='ServicesCosts'.$excelname.".".$excel_ext;
                        $config['file_name']=$filename;
                        $config['overwrite']=true;
                        $this->upload->initialize($config);
                      
                    if($this->upload->do_upload('upload')){

                        $this->excel_reader->read('./excel/'.$filename);
                        $data = $this->excel_reader->worksheets[0];


                        //loop through the worksheet
                        foreach($data as $value){

                            $id=$this->Administration_model->get_service_by_name($value[0]);
                            
                            if($id <> null){
                                $service_cost_array=array(
                                            'serviceid'=>$service,
                                            'departmentid'=>$department[0]->departmentid,
                                            'subdepartmentid'=>$department[0]->subdepartmentid,
                                        );
                                
                                if($sponsor == null){
                                    $sponsors=$this->SuperAdministration_model->active_sponsors();
                                    foreach($sponsors as $key=>$value){

                                        $service_cost_array['sponsorid']=$value->sponsorcode;
                                        $check=$this->Administration_model->check_service_cost($service_cost_array);
                                        $service_cost_array['cost']=(float)$value[1];
                                        //save service details
                                        $resp=$this->Administration_model->save_service_cost($service_cost_array,$check->id);
                                        if($resp){
                                            $error=FALSE;
                                      }
                                    }
                                }else{

                                    $service_cost_array['sponsorid']=$sponsor;
                                    $check=$this->Administration_model->check_service_cost($service_cost_array);
                                    $service_cost_array['cost']=(float)$value[1];
                                    //save service cost details
                                    $resp=$this->Administration_model->save_service_cost($service_cost_array,$check->id);
                                    if($resp){
                                      $error=FALSE;
                                    }
                                }
                            }
                        }
                    }else{
                        $this->data['message']=$this->upload->display_errors('<div class="alert alert-danger " role="alert" style="text-align:center;">','</div>');
                       
                    }
                }
            }

           $this->data['title']="Add Service Cost";
           $this->data['id']="$id";
           $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
           $this->data['services']=$this->Administration_model->active_services();
           $this->data['service_cost']=$this->Administration_model->service_costs($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/add_service_cost';
           $this->load->view('administrator/template',$this->data);
    }
     
    public function activate_deactivate_service_cost($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
               
        $this->Administration_model->activate_deactivate_service_cost($id,$status);
        redirect('Administration/view_service_costs','refresh');
    }
    
    public function view_items($code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'Add Item Cost','link'=>'Administration/add_item_cost'),
                                   array('title'=>'View Items','link'=>'Administration/view_items'),
                                   array('title'=>'View Items Cost','link'=>'Administration/view_items_cost'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(4)) {
            $exp = explode("_", $this->uri->segment(4));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];
            
            $docType = $exp[7];
            $this->data['drug_type']=$docType;

        }

        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        if($docType == 1){
            $data=$this->Inventory_model->inventory_items($code,$name,$type,$grp);
            require_once 'reports/inventory/inventory_items_pdf.php';
        }

        if($docType == 2){
            
             $data=$this->Inventory_model->inventory_items($code,$name,$type,$grp);
             require_once 'reports/inventory/inventory_items_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Administration/view_items/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =5;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['items_view']='administrator/items';
        $this->data['title']='View Items';
        $this->data['content']='administrator/view_items';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function view_items_cost($code){
           
           $this->data['side_menu']=array(
                                   array('title'=>'Add Item Cost','link'=>'Administration/add_item_cost'),
                                   array('title'=>'View Items','link'=>'Administration/view_items'),
                                   array('title'=>'View Items Cost','link'=>'Administration/view_items_cost'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('sponsor')) {
            $key['sponsor'] = $this->input->post('sponsor');
            $this->data['sponsor']=$this->input->post('sponsor');
        }
        
        if ($this->input->post('min')) {
            $key['min'] = $this->input->post('min');
            $this->data['min']=$this->input->post('min');
        }
        
        if ($this->input->post('max')) {
            $key['max'] = $this->input->post('max');
            $this->data['max']=$this->input->post('max');
        }
         
            
            
        if ($this->uri->segment(4)) {
            $exp = explode("_", $this->uri->segment(4));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['sponsor'] = $exp[3];
            $this->data['sponsor']=$key['sponsor'];
            
            $key['min'] = $exp[5];
            $this->data['min']=$key['min'];
            
            $key['max'] = $exp[7];
            $this->data['max']=$key['max'];
            
            $docType= $exp[9];
            $this->data['docType']=$docType;

        }

        
        
        $name =$key['name'];
        $sponsor =$key['sponsor'];
        $min =$key['min'];
        $max =$key['max'];
        
        if($docType == 1){
            
            $data=$this->Administration_model->items_cost($code,$name,$sponsor,$min,$max);
           require_once 'reports/administrator/items_cost_pdf.php';
        }
        
        if($docType == 2){
            $data=$this->Administration_model->items_cost($code,$name,$sponsor,$min,$max);
            require_once 'reports/administrator/items_cost_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Administration/view_items_cost/$code/name_".$key['name']."_sponsor_".$key['sponsor']."_min_".$key['min']."_max_".$key['max']."/";
        $config["total_rows"] =$this->Administration_model->items_cost_info_count($code,$name,$sponsor,$mincost,$maxcost);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =5;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Administration_model->items_cost_info($code,$name,$sponsor,$mincost,$maxcost,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['sponsors'] = $this->SuperAdministration_model->active_sponsors();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['items_view']='administrator/items_cost';
        $this->data['title']='View Items Cost';
        $this->data['content']='administrator/view_items_cost';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function add_item_cost($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                   array('title'=>'Add Item Cost','link'=>'Administration/add_item_cost'),
                                   array('title'=>'View Items','link'=>'Administration/view_items'),
                                   array('title'=>'View Items Cost','link'=>'Administration/view_items_cost'),
                                );
          
           
            $this->form_validation->set_rules('sponsor','Sponsor','xss_clean');
            $this->form_validation->set_rules('addType','Addition Type','xss_clean|required');

            $addType=$this->input->post('addType');
            if($addType == 1){
                
                $this->form_validation->set_rules('item','Item','required|xss_clean');
                $this->form_validation->set_rules('cost','Cost','required|xss_clean|numeric');
            }
            
            if ($this->form_validation->run() == TRUE)
           {
                
                $addType=$this->input->post('addType');
                $sponsor=$this->input->post('sponsor');
                $cost=$this->input->post('cost');
                $error=TRUE;
                
                
                if($addType == 1){
                    
                    $item=$this->input->post('item');
                    $department=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
                    $subdepartment=$this->Administration_model->get_subdepartment_by_department($department[0]->id);
                    
                    $item_cost_array=array(
                                            'serviceid'=>$item,
                                            'departmentid'=>$department[0]->id,
                                            'subdepartmentid'=>$subdepartment[0]->id,
                                        );

                        if($sponsor == null && $id == null){
                            
                            $sponsors=$this->SuperAdministration_model->active_sponsors();
                            foreach($sponsors as $key=>$value){
                                
                                $item_cost_array['sponsorid']=$value->sponsorcode;
                                $check=$this->Administration_model->check_service_cost($item_cost_array);
                                $item_cost_array['cost']=$cost;
                                //save service details
                                $resp=$this->Administration_model->save_service_cost($item_cost_array,$check->id);
                                if($resp){
                                    $error=FALSE;
                              }
                            }
                            
                        }else{
                            
                            $service_cost_array['sponsorid']=$sponsor;
                             $check=$this->Administration_model->check_service_cost($item_cost_array);
                             $service_cost_array['cost']=$cost;
                            //save service details
                            $resp=$this->Administration_model->save_service_cost($item_cost_array,$check->id);
                            if($resp){
                              $error=FALSE;
                            }
                        }
                }else{
                    
                        $config['upload_path'] = './excel/';
                        $config['allowed_types'] = 'xls';
                        $filename=explode('.',$_FILES['upload']['name']);
                        $excel_ext=end($filename);
                        $excelname=date('YmdHis');
                        $filename='ServicesCosts'.$excelname.".".$excel_ext;
                        $config['file_name']=$filename;
                        $config['overwrite']=true;
                        $this->upload->initialize($config);
                      
                    if($this->upload->do_upload('upload')){

                        $this->excel_reader->read('./excel/'.$filename);
                        $data = $this->excel_reader->worksheets[0];


                        //loop through the worksheet
                        foreach($data as $value){

                            $id=$this->Administration_model->get_item_by_name($value[0]);
                            
                            if($id <> null){
                                
                                $department=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
                                $subdepartment=$this->Administration_model->get_subdepartment_by_department($department[0]->id);
                                
                                $item_cost_array=array(
                                            'serviceid'=>$id->id,
                                            'departmentid'=>$department[0]->id,
                                            'subdepartmentid'=>$subdepartment[0]->id,
                                        );
                                
                                if($sponsor == null){
                                    
                                    $sponsors=$this->SuperAdministration_model->active_sponsors();
                                    foreach($sponsors as $key=>$value){

                                        $item_cost_array['sponsorid']=$value->sponsorcode;
                                        $check=$this->Administration_model->check_service_cost($item_cost_array);
                                        $item_cost_array['cost']=(float)$value[1];
                                        //save service details
                                        $resp=$this->Administration_model->save_service_cost($item_cost_array,$check->id);
                                        if($resp){
                                            $error=FALSE;
                                      }
                                    }
                                }else{

                                    $item_cost_array['sponsorid']=$sponsor;
                                    $check=$this->Administration_model->check_service_cost($item_cost_array);
                                    $item_cost_array['cost']=(float)$value[1];
                                    //save service cost details
                                    $resp=$this->Administration_model->save_service_cost($item_cost_array,$check->id);
                                    if($resp){
                                      $error=FALSE;
                                    }
                                }
                            }
                        }
                    }else{
                        $this->data['message']=$this->upload->display_errors('<div class="alert alert-danger " role="alert" style="text-align:center;">','</div>');
                       
                    }
                }
            }

           $this->data['title']="Add Item Cost";
           $this->data['id']="$id";
           $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
           $this->data['items']=$this->Inventory_model->active_selling_items();
           $this->data['item_cost']=$this->Administration_model->service_costs($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/add_item_cost';
           $this->load->view('administrator/template',$this->data);
    }
     
    public function activate_deactivate_item_cost($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
               
        $this->Administration_model->activate_deactivate_service_cost($id,$status);
        redirect('Administration/view_items_cost','refresh');
    }
    
    public function investigation_services(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
        } 
        
        $this->data['side_menu']=array(
                                        array('title'=>'View Investigation Services','link'=>'Administration/investigation_services'),
                                        array('title'=>'Configure Investigation Service','link'=>'Administration/configure_investigation_category_service'),
                                       );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('category')) {
                    $key['category'] = $this->input->post('category');
                    $this->data['category']=$this->input->post('category');
                }
               
                if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['category'] = $exp[1];
                    $this->data['category']=$key['category'];
                    
                }
                
                $name =$key['name'];
                $category =$key['category'];
                
                $config["base_url"] = base_url() . "index.php/Administration/investigation_services/name_".$key['name']."_category_".$key['category']."/";
                $config["total_rows"] =$this->SuperAdministration_model->investigation_category_services_info_count($name.$category);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['investigation_services'] = $this->SuperAdministration_model->investigation_category_services_info($name,$category,$page,$limit);
                
                $this->data['title']="Investigation Services";
                $this->data['categories']=$this->SuperAdministration_model->investigation_categories();
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='administrator/investigation_services';
		$this->load->view('administrator/template',$this->data);
    }
    
    public function configure_investigation_category_service($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

            $this->data['side_menu']=array(
                                        array('title'=>'View Investigation Services','link'=>'Administration/investigation_services'),
                                        array('title'=>'Configure Investigation Service','link'=>'Administration/configure_investigation_category_service'),
                                       );
           
           $this->form_validation->set_rules('department','Department','required|xss_clean');
           $this->form_validation->set_rules('subdepartment','Sub Department','required|xss_clean');
           
           if($this->input->post('configure')){
               $srv=$this->input->post('srv');
               $count=count($srv);
               
               if($count > 0){
                   $i=0;
                   foreach($srv as $key=>$value){
                        
                        $this->form_validation->set_rules('investigationcategory','Investigation Category','required|xss_clean');
                        $this->form_validation->set_rules('department','Department','required|xss_clean');
                        $this->form_validation->set_rules('subdepartment','Sub Department','required|xss_clean');
                        $this->form_validation->set_rules('srv['.$i.']','','xss_clean');
                        
                        if ($this->form_validation->run() == TRUE)
                            {
                                 $inv_service=array(
                                     'serviceid'=>$value,
                                     'investigationcategory'=>$this->input->post('investigationcategory')
                                 );
                                 
                                 //do saving
                                  $this->SuperAdministration_model->save_investigation_category_service($inv_service,$id);

                            }
                   }
               }
           }
           
           $this->data['title']="Configure Investigation Category Service(s)";
           $this->data['id']="$id";
           $this->data['categories']=$this->SuperAdministration_model->investigation_categories(null,null,1);
           $this->data['departments']=$this->Administration_model->active_departments();
           $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
           $this->data['services']=$this->Administration_model->active_services();
           $this->data['investigationCategorySrv']=$this->SuperAdministration_model->investigation_category_services($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/add_investigation_category_services';
           $this->load->view('administrator/template',$this->data);
    }
    
    public function activate_deactivate_investigation_category_service($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_investigation_category_service($status,$id);
        redirect('Administration/investigation_services','refresh');
    }
    
    public function remove_investigation_category_service($id){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->remove_investigation_category_service($id);
        redirect('Administration/investigation_services','refresh');
    }
    
    public function view_packages(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $this->data['side_menu']=array(
                                    array('title'=>'View Packages(s)','link'=>'Administration/view_packages'),
                                    array('title'=>'Add Package','link'=>'Administration/add_package'),
                                );
        
        
            if($this->input->post('name')) {
                $key['name'] = $this->input->post('name');
                $this->data['name']=$this->input->post('name');
            }
            
           
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            
            $this->data['name']=$key['name'];
        }

        
        $name =$key['name'];

        
        $config["base_url"] = base_url() . "index.php/Administration/view_packages/name_".$key['name']."/";
        $config["total_rows"] =$this->Administration_model->packages_info_count($name);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['packages'] = $this->Administration_model->packages_info($name,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Packages';
        $this->data['content']='administrator/view_packages';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function add_package($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'View Packages(s)','link'=>'Administration/view_packages'),
                                            array('title'=>'Add Package','link'=>'Administration/add_package'),
                                        );
          
           $services=$this->input->post('service');
           $items=$this->input->post('item');
           
           if($this->input->post('save_package')){
              
               if(count($services) > 0 || count($items) > 0){
               
                    $this->form_validation->set_rules('name','Name','required|xss_clean');
                    $this->form_validation->set_rules('cost','Cost','required|xss_clean');
                    $this->form_validation->set_rules('packageid','Package Id','required|xss_clean');
                    if($this->form_validation->run() == TRUE)
                         {
                             $package_item_service=array();
                             $packageid=$this->input->post('packageid');
                             $cost=$this->input->post('cost');
                             $name=$this->input->post('name');

                             $package=array(
                                 'packageid'=>$packageid,
                                 'cost'=>$cost,
                                 'name'=>$name,
                             );
                             
                             foreach($services as $key=>$value){
                                 $qty=$this->input->post('qty_'.$value);
                                 if($qty == null || $qty <= 0){
                                     continue;
                                 }

                                 $service_item=explode('_', $value);
                                 $package_item_service[]=array(
                                     'packageid'=>$packageid,
                                     'item_service_id'=>$service_item[0],
                                     'departmentid'=>$service_item[2],
                                     'subdepartmentid'=>$service_item[1],
                                     'quantity'=>$qty,
                                 );
                             }

                             foreach($items as $key=>$value){
                                 $qty=$this->input->post('qty_'.$value);
                                 if($qty == null || $qty <= 0){
                                     continue;
                                 }

                                 $service_item=explode('_', $value);
                                 $package_item_service[]=array(
                                     'packageid'=>$packageid,
                                     'item_service_id'=>$service_item[0],
                                     'departmentid'=>$service_item[2],
                                     'subdepartmentid'=>$service_item[1],
                                     'quantity'=>$qty,
                                 );
                             }
                             
                             if($package_item_service <> null){
                               //save package details
                               $resp=$this->Administration_model->save_package($package,$package_item_service,$id);
                               if($resp){

                                     redirect(current_url());
                                 }else{

                                     $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                                 }
                             }else{
                                 $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                             }
                         }
                 }else{
                     $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">check atleast one service/item!</div>';
                 }
           }
           
           

           $this->data['title']="Add Package";
           $this->data['id']="$id";
           $this->data['packageid']=$this->get_package_id();
           $this->data['services']=$this->Administration_model->active_services();
           $this->data['items']=$this->Inventory_model->active_selling_items();
           $this->data['packg']=$this->Administration_model->packages($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/add_package';
           $this->load->view('administrator/template',$this->data);
    }
    
    public function activate_deactivate_package($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
               
        $this->Administration_model->activate_deactivate_package($id,$status);
        redirect('Administration/view_packages','refresh');
    }
    
    public function package_details($id){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $this->data['side_menu']=array(
                                    array('title'=>'View Packages(s)','link'=>'Administration/view_packages'),
                                    array('title'=>'Add Package','link'=>'Administration/add_package'),
                                );
        
        
        $config["base_url"] = base_url() . "index.php/Administration/package_details/$id/";
        $config["total_rows"] =$this->Administration_model->package_items_services_info_count($id);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['pckg_details'] = $this->Administration_model->package_details_info($id,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Package Details';
        $this->data['content']='administrator/view_package_details';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function activate_deactivate_package_detail($packageid,$id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
               
        $this->Administration_model->activate_deactivate_package_detail($id,$status);
        redirect('Administration/package_details/'.$packageid,'refresh');
    }
    
    public function items_consumption(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
       $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Administration/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Administration/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Administration/expired_items'),
                           array('title'=>'Main Stock','link'=>'Administration/main_stock'),
                           array('title'=>'Units Stock','link'=>'Administration/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Administration/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Administration/stock_transfer_report'),
                           
                        );
        
       
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
            
            if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            $key['unit'] = $exp[5];
            
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
            $this->data['unit']=$key['unit'];
        }

        $unit=$key['unit'];;
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->items_consumption_report($unit,$item,$start,$end);
            require_once 'reports/inventory/items_consumption_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->items_consumption_report($unit,$item,$start,$end);
            require_once 'reports/inventory/items_consumption_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Administration/items_consumption/0_".$unit."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->items_consumption_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['consumptions'] = $this->Inventory_model->items_consumption_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units']=$this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']=' Items Consumption';
        $this->data['content']='administrator/items_consumption';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function expired_items(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
       $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Administration/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Administration/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Administration/expired_items'),
                           array('title'=>'Main Stock','link'=>'Administration/main_stock'),
                           array('title'=>'Units Stock','link'=>'Administration/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Administration/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Administration/stock_transfer_report'),
                           
                        );
        
        $segment=$this->uri->segment(3);
        $seg=explode('_',$segment);
        $docType=$seg[0];
        $category=$seg[1];
        $item=$seg[2];
        $unit=$seg[3];
        $start=$seg[4];
        $end=$seg[5];
        
        if($this->input->post('search')){
            
            $unit=$this->input->post('unit');
            $category=$this->input->post('category');
            $item=$this->input->post('item');
            $start=$this->input->post('start');
            $end=$this->input->post('end');
        }
        
        $data=$this->Inventory_model->expired_items($category,$item,$unit,$start,$end);
        
        if($docType == 1){
            
            require_once 'reports/inventory/expired_items_pdf.php';
        }
        
        if($docType == 2){
            
            require_once 'reports/inventory/expired_items_excel.php';
        }
        
        $this->data['category']=$category;
        $this->data['item']=$item;
        $this->data['unit']=$unit;
        $this->data['start']=$start;
        $this->data['end']=$end;
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['expired'] = $data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Expired Items';
        $this->data['content']='administrator/expired_items';
        $this->load->view('administrator/template',$this->data);
    }
   
    public function disposed_items(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
         $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Administration/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Administration/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Administration/expired_items'),
                           array('title'=>'Main Stock','link'=>'Administration/main_stock'),
                           array('title'=>'Units Stock','link'=>'Administration/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Administration/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Administration/stock_transfer_report'),
                           
                        );
        
        if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['unit'] = $exp[1];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['unit']=$key['unit'];
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        
        $unit =$key['unit'];
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->disposed_items_report($unit,$item,$start,$end);
            require_once 'reports/inventory/disposed_items_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->disposed_items_report($unit,$item,$start,$end);
            require_once 'reports/inventory/disposed_items_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Administration/disposed_items/0_".$key['unit']."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->disposed_items_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['disposes'] = $this->Inventory_model->disposed_items_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Disposed Items';
        $this->data['content']='administrator/disposed_items';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function main_stock($code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
         $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Administration/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Administration/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Administration/expired_items'),
                           array('title'=>'Main Stock','link'=>'Administration/main_stock'),
                           array('title'=>'Units Stock','link'=>'Administration/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Administration/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Administration/stock_transfer_report'),
                           
                        );
        
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(4)) {
            $exp = explode("_", $this->uri->segment(4));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];

        }

        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        $config["base_url"] = base_url() . "index.php/Administration/main_stock/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =5;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['main_stock_items']='administrator/main_stock_items';
        $this->data['title']='Main Stock';
        $this->data['content']='administrator/main_stock';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function inventory_item_stock_details($itemid,$code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
         $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Administration/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Administration/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Administration/expired_items'),
                           array('title'=>'Main Stock','link'=>'Administration/main_stock'),
                           array('title'=>'Units Stock','link'=>'Administration/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Administration/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Administration/stock_transfer_report'),
                           
                        );
         
        $this->data['item_inventory_details']=$this->Inventory_model->item_inventory_details($itemid,null);   
        $item_name=$this->Inventory_model->items($itemid);
        $this->data['title']=$item_name[0]->name.' Main Inventory Details';
        $this->data['code']=$code;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='administrator/item_main_inventory_details';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function units_stock($unit,$code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
         $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Administration/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Administration/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Administration/expired_items'),
                           array('title'=>'Main Stock','link'=>'Administration/main_stock'),
                           array('title'=>'Units Stock','link'=>'Administration/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Administration/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Administration/stock_transfer_report'),
                           
                        );
        
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(5)) {
            $exp = explode("_", $this->uri->segment(5));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];

        }

        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        $config["base_url"] = base_url() . "index.php/Administration/main_stock/$unit/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =6;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['unit']=$unit;
        $this->data['units_stock_items']='administrator/units_stock_items';
        $this->data['title']='Units Stock';
        $this->data['content']='administrator/units_stock';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function inventory_unit_item_stock_details($itemid,$code,$unit){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
        $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Administration/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Administration/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Administration/expired_items'),
                           array('title'=>'Main Stock','link'=>'Administration/main_stock'),
                           array('title'=>'Units Stock','link'=>'Administration/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Administration/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Administration/stock_transfer_report'),
                           
                        );
        $this->data['item_inventory_details']=$this->Inventory_model->item_inventory_details($itemid,$unit);   
        $item_name=$this->Inventory_model->items($itemid);
        $unit_name=$this->Inventory_model->units($unit);
        $this->data['title']=$item_name[0]->name.' '.$unit_name[0]->name.' Inventory Details';
        $this->data['code']=$code;
        $this->data['unit']=$unit;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='administrator/item_unit_inventory_details';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function stock_transfer_report(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
       $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Administration/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Administration/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Administration/expired_items'),
                           array('title'=>'Main Stock','link'=>'Administration/main_stock'),
                           array('title'=>'Units Stock','link'=>'Administration/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Administration/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Administration/stock_transfer_report'),
                           
                        );
        
        
            if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['unit'] = $exp[1];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['unit']=$key['unit'];
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        
        $unit =$key['unit'];
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->stock_tranfers_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_transfers_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->stock_tranfers_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_transfers_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Administration/stock_transfer_report/0_".$key['unit']."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->stock_transfer_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['transfers'] = $this->Inventory_model->stock_transfer_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Stock Tranfers';
        $this->data['content']='administrator/stock_transfers_report';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function stock_taking_report(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
         $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Administration/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Administration/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Administration/expired_items'),
                           array('title'=>'Main Stock','link'=>'Administration/main_stock'),
                           array('title'=>'Units Stock','link'=>'Administration/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Administration/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Administration/stock_transfer_report'),
                           
                        );
        
            if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['unit'] = $exp[1];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['unit']=$key['unit'];
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        
        $unit =$key['unit'];
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->stock_takings_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_takings_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->stock_takings_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_takings_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Administration/stock_taking_report/0_".$key['unit']."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->stock_taking_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['takings'] = $this->Inventory_model->stock_taking_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Stock Takings';
        $this->data['content']='administrator/stock_takings_report';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function aggregated_collection(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
            $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Administration/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Administration/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Administration/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Administration/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Administration/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Administration/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Administration/prepaid_accounts'),
                           );
        
        $start=$this->input->post('start') <> null?$this->input->post('start'):date('Y-m-d');
        $end=$this->input->post('end') <> null?$this->input->post('end'):date('Y-m-d');
        $department=$this->input->post('department');
        $subdepartment=$this->input->post('subdepartment');
        $sponsor=$this->input->post('sponsor');
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $start = $exp[1];
            $end = $exp[3];
            $sponsor = $exp[5];
            $department = $exp[7];
            $subdepartment = $exp[9];
            $docType = $exp[11];
        }
        
        $data=$this->Reception_model->aggregated_collection($start,$end,$sponsor,$department,$subdepartment,null);
        if($docType == 1){
                
            require_once 'reports/reception/aggregated_collection_report_pdf.php';
        }

        if($docType == 2){

             require_once 'reports/reception/aggegated_collection_report_excel.php';
        }
        
        $this->data['title']="Aggregated Collections";
        $this->data['departments']=$this->Administration_model->active_departments();
        $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
        $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
        $this->data['sponsor']=$sponsor;
        $this->data['subdepartment']=$subdepartment;
        $this->data['department']=$department;
        $this->data['end']=$end;
        $this->data['start']=$start;
        $this->data['aggregated_collections']=$data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='administrator/aggregated_collection';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function general_collection(){
        if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Administration/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Administration/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Administration/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Administration/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Administration/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Administration/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Administration/prepaid_accounts'),
                           );
               
        $start=$this->input->post('start') <> null?$this->input->post('start'):date('Y-m-d');
        $end=$this->input->post('end') <> null?$this->input->post('end'):date('Y-m-d');
        $department=$this->input->post('department');
        $subdepartment=$this->input->post('subdepartment');
        $sponsor=$this->input->post('sponsor');
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $start = $exp[1];
            $end = $exp[3];
            $sponsor = $exp[5];
            $department = $exp[7];
            $subdepartment = $exp[9];
            $docType = $exp[11];
        }
        
        $data=$this->Reception_model->general_collection($start,$end,$sponsor,$department,$subdepartment,null);
        if($docType == 1){
                
            require_once 'reports/reception/general_collection_report_pdf.php';
        }

        if($docType == 2){

             require_once 'reports/reception/general_collection_report_excel.php';
        }
        
        $this->data['title']="General Collections";
        $this->data['departments']=$this->Administration_model->active_departments();
        $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
        $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
        $this->data['sponsor']=$sponsor;
        $this->data['subdepartment']=$subdepartment;
        $this->data['department']=$department;
        $this->data['end']=$end;
        $this->data['start']=$start;
        $this->data['general_collections']=$data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='administrator/general_collection';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function package_accounts(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
        $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Administration/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Administration/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Administration/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Administration/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Administration/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Administration/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Administration/prepaid_accounts'),
                           );
        
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('package')){
            $key['package'] = $this->input->post('package');
            $this->data['package']=$this->input->post('package');
        }

        if ($this->input->post('subscriptionid')) {
            $key['subscriptionid'] = $this->input->post('subscriptionid');
            $this->data['subscriptionid']=$this->input->post('subscriptionid');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
             $key['subscriptionid'] = $exp[3];
            $this->data['subscriptionid']=$key['subscriptionid'];

            $key['package'] = $exp[5];
            $this->data['package']=$key['package'];
        }

        $patientid =$key['patientid'];
        $subscriptionid =$key['subscriptionid'];
        $package =$key['package'];
        
        $config["base_url"] = base_url() . "index.php/Administration/package_accounts/patientid_".$key['patientid']."_subscriptionid_".$key['subscriptionid']."_package_".$key['package']."/";
        $config["total_rows"] =$this->Reception_model->patient_packages_count($patientid,$subscriptionid,$package);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_packages'] = $this->Reception_model->patient_packages($patientid,$subscriptionid,$package,$page,$limit);

        $this->data['title']="Patient Packages";
        $this->data['packages']=$this->Administration_model->packages();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='administrator/patient_packages';
        $this->load->view('administrator/template',$this->data);       
    }
    
    public function patient_package_details($subscriptionid,$patientid,$docType){
        if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Administration/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Administration/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Administration/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Administration/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Administration/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Administration/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Administration/prepaid_accounts'),
                           );
               
        if($docType == 1){
                
                $data=$this->Reception_model->subscriptions($subscriptionid);
                require_once 'reports/reception/package_account_report_pdf.php';
        }

        if($docType == 2){

             $data=$this->Reception_model->subscriptions($subscriptionid);
             require_once 'reports/reception/package_account_report_excel.php';
        }    
        $patient=$this->Reception_model->patient($patientid);       
        $patient=$this->Reception_model->patients($patient->id);       
        $this->data['patient_package_details'] =$this->Reception_model->subscriptions($subscriptionid);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['subscriptionid']=$subscriptionid;
        $this->data['title']=$subscriptionid." Subscription Details";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='administrator/patient_package_details';
        $this->load->view('administrator/template2',$this->data);
    }
    
    public function patients_debtors(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Administration/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Administration/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Administration/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Administration/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Administration/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Administration/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Administration/prepaid_accounts'),
                           );
               
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('start')){
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
             $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];
        }

        $patientid =$key['patientid'];
        $start =$key['start'];
        $end =$key['end'];
        
        $config["base_url"] = base_url() . "index.php/Administration/patients_debtors/patientid_".$key['patientid']."_start_".$key['start']."_end_".$key['end']."/";
        $config["total_rows"] =$this->Reception_model->patient_debtors_count($patientid,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_debtors'] = $this->Reception_model->patient_debtors($patientid,$start,$end,$page,$limit);

        $this->data['title']="Patient Debtors";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='administrator/patient_debtors';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function patient_debt_details($patientid,$docType){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Administration/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Administration/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Administration/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Administration/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Administration/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Administration/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Administration/prepaid_accounts'),
                           );
            
            if($docType == 1){
                
                $data=$this->Reception_model->patient_debt_details($patientid);
                require_once 'reports/reception/patient_debt_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->patient_debt_details($patientid);
                 require_once 'reports/reception/patient_debt_report_excel.php';
            }
            
               if($this->input->post('clear_debt')){
                   $debts=$this->input->post('debt');
                   
                   if(count($debts) > 0){
                       
                       foreach($debts as $key=>$value){
                           
                           $data=explode('_',$value);
                           $array[]=array(
                               'item_service_id'=>$data[0],
                               'departmentid'=>$data[1],
                               'postpaid_transactionid'=>$data[2],
                            );
                       }
                       
                       if($array <> null){
                           
                           $txngenerate=FALSE;
                           foreach($array as $key=>$value){
                               
                               if($txngenerate==FALSE){
                                   
                                        $txnid=$this->Reception_model->get_transaction_id();
                                        $txngenerate=TRUE;
                                }
                                
                                $bill_txn=array(
                                    'transactionid'=>$txnid,
                                    'paymentmodeid'=>$this->config->item('instant_cash_bill_pmode_code'),
                                    'createdon'=>date('Y-m-d H:i:s'),
                                    'createdby'=>$this->session->userdata('user_id'),
                                    'paymentstatus'=>1,
                                );
                                
                                $debt_bill_txn=array(
                                    'reftransactionid'=>$txnid,
                                    'status'=>1,
                                    'modifiedby'=>$this->session->userdata('user_id'),
                                    'modifiedon'=>date('Y-m-d H:i:s'),
                                );
                                
                                $clear=$this->Reception_model->clear_debt($bill_txn,$debt_bill_txn,$value['item_service_id'],$value['postpaid_transactionid']);
                                
                                if($clear){
                                    
                                    $this->Reception_model->update_service_item_order($txnid,$value['postpaid_transactionid'],$value['item_service_id'],TRUE);
                                    redirect('Reception/service_order_receipt/'.$txnid.'/'.$patientid,'refresh');
                                }
                            }
                       }
                   }else{
                       $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">confirm atleast one debt!</div>';
                   }
               }
          
        $patient=$this->Reception_model->patient($patientid);       
        $patient=$this->Reception_model->patients($patient->id);  
        $this->data['patient_debt_details'] =$this->Reception_model->patient_debt_details($patientid);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['title']="Patient Debt Details";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='administrator/patient_debt_details';
        $this->load->view('administrator/template2',$this->data);
    }
    
    public function patient_statement($details){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Administration/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Administration/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Administration/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Administration/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Administration/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Administration/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Administration/prepaid_accounts'),
                           );
        
        $details=explode("_",$details);
        $patientid=$details[1];
        $start=$details[3];
        $end=$details[5];
        $docType=$details[7];
        
        if($docType == 1){

            $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end);
            require_once 'reports/reception/patients_statement_report_pdf.php';
        }

        if($docType == 2){

             $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end);
             require_once 'reports/reception/patients_statement_report_excel.php';
        }
        
        $this->form_validation->set_rules('patientid','Patient Id','xss_clean|required|callback_patientid');
        $this->form_validation->set_rules('start','Start Date','xss_clean|required|callback_start');
        $this->form_validation->set_rules('end','End Date','xss_clean|required|callback_end');

        if($this->form_validation->run() == TRUE){
            
            $patientid=$this->input->post('patientid');
            $start=$this->input->post('start');
            $end=$this->input->post('end');
            
            $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end);
        }
           
        $this->data['title']="Patient Statement";
        $this->data['patientid']=$patientid;
        $this->data['start']=$start;
        $this->data['end']=$end;
        $this->data['data']=$data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['statement']='common/statement';
        $this->data['content']='administrator/patient_statement';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function patient_transactions(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
        
        $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Administration/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Administration/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Administration/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Administration/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Administration/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Administration/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Administration/prepaid_accounts'),
                           );
               
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('start')){
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }

        if ($this->input->post('sponsor')) {
            $key['sponsor'] = $this->input->post('sponsor');
            $this->data['sponsor']=$this->input->post('sponsor');
        }

        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
            $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];

            $key['sponsor'] = $exp[7];
            $this->data['sponsor']=$key['sponsor'];
            
            $docType = $exp[9];
        }

        $start =$key['start'];
        $end =$key['end'];
        $sponsor =$key['sponsor'];
        $patientid =$key['patientid'];
           
        if($docType == 1){
                
            $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end,$sponsor);
            require_once 'reports/reception/patients_transactions_report_pdf.php';
        }

        if($docType == 2){

             $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end,$sponsor);
             require_once 'reports/reception/patients_transactions_report_excel.php';
        }
            
        $config["base_url"] = base_url() . "index.php/Administration/patient_transactions/patientid_".$key['patientid']."_start_".$key['start']."_end_".$key['end']."_sponsor_".$key['sponsor']."/";
        $config["total_rows"] =$this->Reception_model->patient_transactions_count($patientid,$start,$end,$sponsor);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_transactions'] = $this->Reception_model->patient_transactions($patientid,$start,$end,$sponsor,$page,$limit);

        $this->data['title']="Patient Transactions";
        $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='administrator/patient_transactions';
        $this->load->view('administrator/template',$this->data);
    }
   
    public function prepaid_accounts(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
        $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Administration/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Administration/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Administration/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Administration/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Administration/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Administration/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Administration/prepaid_accounts'),
                           );
        
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('subscriptionid')) {
            $key['subscriptionid'] = $this->input->post('subscriptionid');
            $this->data['subscriptionid']=$this->input->post('subscriptionid');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
             $key['subscriptionid'] = $exp[3];
            $this->data['subscriptionid']=$key['subscriptionid'];

        }

        $patientid =$key['patientid'];
        $subscriptionid =$key['subscriptionid'];
        
        $config["base_url"] = base_url() . "index.php/Administration/prepaid_accounts/patientid_".$key['patientid']."_subscriptionid_".$key['subscriptionid']."/";
        $config["total_rows"] =$this->Reception_model->patients_prepaid_accounts_count($patientid,$subscriptionid);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_prepaid'] = $this->Reception_model->patients_prepaid_accounts($patientid,$subscriptionid,$page,$limit);

        $this->data['title']="Patient Prepaid Accounts";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='administrator/patient_prepaid_accounts';
        $this->load->view('administrator/template',$this->data);       
    }
    
    public function patient_prepaid_account_details($subscriptionid,$patientid,$docType){
        if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Administration/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Administration/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Administration/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Administration/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Administration/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Administration/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Administration/prepaid_accounts'),
                           );
         
            
            if($docType == 1){
                
                $data=$this->Reception_model->prepaid_accounts($subscriptionid);
                require_once 'reports/reception/prepaid_account_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->prepaid_accounts($subscriptionid);
                 require_once 'reports/reception/prepaid_account_report_excel.php';
            }
            
        $patient=$this->Reception_model->patient($patientid);       
        $patient=$this->Reception_model->patients($patient->id);       
        $this->data['patient_prepaid_details'] =$this->Reception_model->prepaid_accounts($subscriptionid);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['subscriptionid']=$subscriptionid;
        $this->data['title']=$subscriptionid." Subscription Details";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='administrator/patient_prepaid_details';
        $this->load->view('administrator/template2',$this->data);
    }
    
    public function inpatients_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
             $this->data['side_menu']=array(
                               array('title'=>'Admissions','link'=>'Administration/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Administration/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Administration/outpatients_report'),
                               array('title'=>'Service Report','link'=>'Administration/service_report'),
                               array('title'=>'Diagnosis Report','link'=>'Administration/diagnosis_report'),
                           );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('ward')) {
               $key['ward'] = $this->input->post('ward');
               $this->data['ward']=$this->input->post('ward');
           }
           
           if ($this->input->post('bed')) {
               $key['bed'] = $this->input->post('bed');
               $this->data['bed']=$this->input->post('bed');
           }

           if ($this->input->post('status')) {
               $key['status'] = $this->input->post('status');
               $this->data['status']=$this->input->post('status');
           }
           
           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['ward'] = $exp[5];
               $this->data['ward']=$key['ward'];
               
               $key['bed'] = $exp[7];
               $this->data['bed']=$key['bed'];
               
               $key['status'] = $exp[9];
               $this->data['status']=$key['status'];
               
               $docType = $exp[11];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $ward =$key['ward'];
            $bed =$key['bed'];
            $status =$key['status'];
            
            if($docType == 1){
                
                    $data=$this->Nursing_model->admissions($start,$end,$ward,$bed,$status);
                    require_once 'reports/clinical/admission_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Nursing_model->admissions($start,$end,$ward,$bed,$status);
                 require_once 'reports/clinical/admission_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Administration/inpatients_report/start_".$key['start']."_end_".$key['end']."_ward_".$key['ward']."_bed_".$key['bed']."_status_".$status."/";
           $config["total_rows"] =$this->Nursing_model->admission_report_count($start,$end,$ward,$bed,$status);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Nursing_model->admission_report($start,$end,$ward,$bed,$status,$page,$limit);
           
           $this->data['title']="Admission Report";
           $this->data['wards']=$this->Inventory_model->units(null,$this->config->item('ward_unit_category_code'));
           $this->data['admission_status']=$this->config->item('admission_status');
           $this->data['beds']=$this->Administration_model->ward_beds();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/admission_report';
           $this->load->view('administrator/template',$this->data);
   }
    
    public function inpatient_transfers_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                               array('title'=>'Admissions','link'=>'Administration/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Administration/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Administration/outpatients_report'),
                               array('title'=>'Service Report','link'=>'Administration/service_report'),
                               array('title'=>'Diagnosis Report','link'=>'Administration/diagnosis_report'),
                           );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
            }
           
           if ($this->input->post('bed')) {
               $key['bed'] = $this->input->post('bed');
               $this->data['bed']=$this->input->post('bed');
           }

           if ($this->input->post('ward')) {
               $key['ward'] = $this->input->post('ward');
               $this->data['ward']=$this->input->post('ward');
           }
           
           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['ward'] = $exp[5];
               $this->data['ward']=$key['ward'];
               
               $key['bed'] = $exp[7];
               $this->data['bed']=$key['bed'];
               
               $key['pid'] = $exp[9];
               $this->data['pid']=$key['pid'];
               
               $docType = $exp[11];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $ward =$key['ward'];
            $bed =$key['bed'];
            $pid =$key['pid'];
            
            if($docType == 1){
                
                    $data=$this->Nursing_model->admission_transfers($start,$end,$ward,$bed,$pid);
                    require_once 'reports/clinical/admission_transfer_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Nursing_model->admission_transfers($start,$end,$ward,$bed,$pid);
                 require_once 'reports/clinical/admission_transfer_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Administration/inpatient_transfers_report/start_".$start."_end_".$end."_ward_".$ward."_bed_".$bed."_pid_".$pid."/";
           $config["total_rows"] =$this->Nursing_model->admission_transfer_report_count($start,$end,$ward,$bed,$pid);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Nursing_model->admission_transfer_report($start,$end,$ward,$bed,$pid,$page,$limit);
           
           $this->data['title']="In Patient Transfers Report";
           $this->data['wards']=$this->Inventory_model->units(null,$this->config->item('ward_unit_category_code'));
           $this->data['beds']=$this->Administration_model->ward_beds();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/admission_transfer_report';
           $this->load->view('administrator/template',$this->data);
   }
   
    public function outpatients_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                               array('title'=>'Admissions','link'=>'Administration/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Administration/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Administration/outpatients_report'),
                               array('title'=>'Service Report','link'=>'Administration/service_report'),
                               array('title'=>'Diagnosis Report','link'=>'Administration/diagnosis_report'),
                           );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('visitcategory')) {
               $key['visitcategory'] = $this->input->post('visitcategory');
               $this->data['visitcategory']=$this->input->post('visitcategory');
           }
           
           if ($this->input->post('consultationcategory')) {
               $key['consultationcategory'] = $this->input->post('consultationcategory');
               $this->data['consultationcategory']=$this->input->post('consultationcategory');
           }

           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['visitcategory'] = $exp[5];
               $this->data['visitcategory']=$key['visitcategory'];
               
               $key['consultationcategory'] = $exp[7];
               $this->data['consultationcategory']=$key['consultationcategory'];
               
               $docType = $exp[9];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $visitcategory =$key['visitcategory'];
            $consultationcategory =$key['consultationcategory'];
            
            if($docType == 1){
                
                    $data=$this->Reception_model->outpatients($start,$end,$visitcategory,$consultationcategory);
                    require_once 'reports/reception/outpatients_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->outpatients($start,$end,$visitcategory,$consultationcategory);
                 require_once 'reports/reception/outpatients_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Administration/outpatients_report/start_".$key['start']."_end_".$key['end']."_visiticateg_".$key['visitcategory']."_consultationcateg_".$key['consultationcategory']."/";
           $config["total_rows"] =$this->Reception_model->outpatients_report_count($start,$end,$visitcategory,$consultationcategory);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Reception_model->outpatients_report($start,$end,$visitcategory,$consultationcategory,$page,$limit);
           
           //consultation department
           $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
           
           $this->data['title']="Out Patients Report";
           $this->data['consultationcategories']=$this->Administration_model->consultation_categories($dept[0]->id,null);
           $this->data['visitcategories']=$this->Reception_model->visit_categories();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/outpatients_report';
           $this->load->view('administrator/template',$this->data);
   }
   
   public function service_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                               array('title'=>'Admissions','link'=>'Administration/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Administration/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Administration/outpatients_report'),
                               array('title'=>'Service Report','link'=>'Administration/service_report'),
                               array('title'=>'Diagnosis Report','link'=>'Administration/diagnosis_report'),
                           );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('service')) {
               $key['service'] = $this->input->post('service');
               $this->data['service']=$this->input->post('service');
           }
           
          

           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['service'] = $exp[5];
               $this->data['service']=$key['service'];
               
               $docType = $exp[7];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $service =$key['service'];
            
            if($docType == 1){
                   
                    $data=$this->Reception_model->service_report($start,$end,$service);
                    require_once 'reports/management/service_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->service_report($start,$end,$service);
                 require_once 'reports/management/service_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Administration/service_report/start_".$key['start']."_end_".$key['end']."_service_".$key['service']."/";
           $config["total_rows"] =$this->Reception_model->service_report_info_count($start,$end,$service);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Reception_model->service_report_info($start,$end,$service,$page,$limit);
           
           $this->data['title']="Services Report";
           $this->data['services']=$this->Administration_model->services();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/service_report';
           $this->load->view('administrator/template',$this->data);
   }
   
    public function view_wards(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'View Wards','link'=>'Administration/view_wards'),
                                   array('title'=>'View Beds','link'=>'Administration/view_ward_beds'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
        }

        $name =$key['name'];

        $config["base_url"] = base_url() . "index.php/Administration/view_wards/name_".$key['name']."/";
        $config["total_rows"] =$this->Inventory_model->units_info_count($name,$this->config->item('ward_unit_category_code'));
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['units'] = $this->Inventory_model->units_info($name,$this->config->item('ward_unit_category_code'),$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='View Wards';
        $this->data['content']='administrator/view_wards';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function add_ward($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                   array('title'=>'View Wards','link'=>'Administration/view_wards'),
                                   array('title'=>'View Beds','link'=>'Administration/view_ward_beds'),
                                );
           
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('description','Description','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {

                $name=$this->input->post('name');
                $description=nl2br(trim($this->input->post('description')));

                

                       $unit=array(
                           'category'=>$this->config->item('ward_unit_category_code'),
                           'name'=>$name,
                           'description'=>$description
                       );


                       //code uniqueness
                       $name_check=$this->Inventory_model->unit_name_check($name,$id);
                       if($name_check == null){


                           //do saving
                            $resp=$this->Inventory_model->save_unit($unit,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">ward name exists already!</div>';
                       }
               
           }

           $this->data['title']="Add Ward";
           $this->data['id']="$id";
           $this->data['unit']=$this->Inventory_model->units($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/add_ward';
           $this->load->view('administrator/template',$this->data);
    }
    
    public function activate_deactivate_ward($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Inventory_model->activate_deactivate_unit($status,$id);
        redirect('Administration/view_wards','refresh');
    }
    
     public function view_ward_beds(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'View Wards','link'=>'Administration/view_wards'),
                                   array('title'=>'View Beds','link'=>'Administration/view_ward_beds'),
                                );
        
        if ($this->input->post('identity')) {
            $key['identity'] = $this->input->post('identity');
            $this->data['identity']=$this->input->post('identity');
        }
        
        if ($this->input->post('ward')) {
            $key['ward'] = $this->input->post('ward');
            $this->data['ward']=$this->input->post('ward');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['identity'] = $exp[1];
            $this->data['identity']=$key['identity'];
            
            $key['ward'] = $exp[3];
            $this->data['ward']=$key['ward'];
        }

        $identity =$key['identity'];
        $ward=$key['ward'];

        $config["base_url"] = base_url() . "index.php/Administration/view_ward_beds/identity_".$key['identity']."_ward_".$key['ward']."/";
        $config["total_rows"] =$this->Administration_model->ward_beds_info_count($identity,$ward);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['ward_beds'] = $this->Administration_model->ward_beds_info($identity,$ward,$page,$limit);
        
        $this->data['wards']=$this->Inventory_model->active_units(null,$this->config->item('ward_unit_category_code'));
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='View Ward Beds';
        $this->data['content']='administrator/view_ward_beds';
        $this->load->view('administrator/template',$this->data);
    }
    
    public function add_ward_bed($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                   array('title'=>'View Wards','link'=>'Administration/view_wards'),
                                   array('title'=>'View Beds','link'=>'Administration/view_ward_beds'),
                                );
           
           $this->form_validation->set_rules('identity','Bed Identity','required|xss_clean');
           $this->form_validation->set_rules('ward','Ward','xss_clean|required');
           


            if ($this->form_validation->run() == TRUE)
           {

                $identity=$this->input->post('identity');
                $ward=$this->input->post('ward');
                

                       $bed=array(
                           'bedidentity'=>$identity,
                           'wardid'=>$ward
                       );


                           //do saving
                            $resp=$this->Administration_model->save_ward_bed($bed,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed,check for duplicate bed identity!</div>';
                              }
           }

           $this->data['title']="Add Ward Bed";
           $this->data['id']="$id";
           $this->data['wards']=$this->Inventory_model->active_units(null,$this->config->item('ward_unit_category_code'));
           $this->data['ward_bed']=$this->Administration_model->ward_beds($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='administrator/add_ward_bed';
           $this->load->view('administrator/template',$this->data);
    }
    
    public function activate_deactivate_ward_bed($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Administration_model->activate_deactivate_ward_bed($status,$id);
        redirect('Administration/view_ward_beds','refresh');
    }
   
    
    public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Administration/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Administration/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['content']='administrator/profile';
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['profile']=$this->SuperAdministration_model->profile_data();
		$this->load->view('administrator/template',$this->data);
        }
        
    public function edit_profile(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Administration/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Administration/change_password'),
                                        );
            
            $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
            $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
            $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
            $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
            $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if($this->form_validation->run() == true){

                 $firstname=$this->input->post('first_name');
                 $middlename=$this->input->post('middle_name');
                 $lastname=$this->input->post('last_name');
                 $username=$this->input->post('username');
                 $email=$this->input->post('email');
                 $mobile=$this->input->post('mobile');

                $user=array(
                            'FIRST_NAME'=>$firstname,
                            'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                            'LAST_NAME'=>$lastname,
                            'USERNAME'=>$username,
                            'EMAIL'=>$email<>null?$email:null,
                            'MSISDN'=>$mobile,
                        );

                $user_group=array(
                            'GROUP_ID'=>$this->session->userdata('group')
                        );

                $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                if($username_check){


                    //do registration
                     $resp=$this->SuperAdministration_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                       if($resp){

                           redirect('Administration/profile','refresh');
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                       }
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                }
            }

            $this->data['title']="Edit Profile";
            $this->data['content']='administrator/edit_profile';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->load->view('administrator/template',$this->data);
    }
        
    public function change_password(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Administration/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Administration/change_password'),
                                        );

            $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
            $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 

            if($this->form_validation->run() == true){

                $password=$this->input->post('password');
                $pchange=$this->SuperAdministration_model->change_password($password);

                if($pchange){
                           
                        $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">password change successfull!</div>';
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">password change failed!</div>';
                }
            }

            $this->data['title']="Change Password";
            $this->data['content']='administrator/change_password';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->load->view('administrator/template',$this->data);
    }
    
    function cur_password($password){
            
            $pf=$this->SuperAdministration_model->profile_data();
            $hashed_old=$this->SuperAdministration_model->hash_db_password($password,$pf);
           
             if($pf->password <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
    }
    
    function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('Administrator')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
    
    function username_uniqueness($username,$id){
            
        return $this->SuperAdministration_model->check_username($username,$id);
    }
    
    function get_package_id(){
        
        $inst=$this->SuperAdministration_model->institution_information();
        $pckg_id=$inst->institution_id.mt_rand(10,99);
        
        while(true){
            $chk=$this->Administration_model->packages($pckg_id);
            
            if($chk == null){
                break;
            }
        }
        
        return $pckg_id;
    }
}