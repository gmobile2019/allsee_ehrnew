<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Clinical extends CI_Controller {

    function __construct() {
        parent::__construct();
       
	$this->load->model('Inventory_model');	
	$this->load->model('Pharmacy_model');	
	$this->load->model('Reception_model');	
	$this->load->model('Clinical_model');	
	$this->load->model('Nursing_model');	
	$this->load->model('Investigation_model');	
		
        
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Home';
            $this->data['content']='clinical/dashboard';
            $this->load->view('clinical/template3',$this->data);
    }
    
    public function out_patients(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'Out Patients','link'=>'Clinical/out_patients'),
                                      array('title'=>'In Patients','link'=>'Clinical/in_patients'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Clinical/out_patients/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->out_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->out_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="Out Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='clinical/all_out_patients';
           $this->load->view('clinical/template',$this->data);
   }
   
    public function in_patients(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                      array('title'=>'Out Patients','link'=>'Clinical/out_patients'),
                                      array('title'=>'In Patients','link'=>'Clinical/in_patients'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Clinical/in_patients/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->in_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->in_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="In Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='clinical/all_in_patients';
           $this->load->view('clinical/template',$this->data);
   }
   
    public function patient_discharge_initiation($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $data=array(
               'status'=>'PENDING DISCHARGE',
               'dischargingdoctor'=>$this->session->userdata('user_id'),
               'dischargeinitiationtimestamp'=>date('Y-m-d H:i:s')
               );
           
           $this->Reception_model->check_out_patient($patientid,$patientvisitid,$data);
           redirect('Clinical/in_patients','refresh');
    }
    
    public function patient_admission($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->Clinical_model->patient_admission($patientid,$patientvisitid);
           redirect('Clinical/out_patients','refresh');
    }
    
    public function patient_overview($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
           $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            
            $this->data['physical']=$this->Clinical_model->patient_physcial_examinations($patientid,$patientvisitid,null,null);
            $this->data['vitals']=$this->Clinical_model->patient_vitals_examinations($patientid,$patientvisitid,null,null);
            $this->data['diagnosis']=$this->Clinical_model->patient_diagnosis($patientid,$patientvisitid);
            $this->data['allegies']=$this->Clinical_model->patient_allegies($patientid,$patientvisitid);
            $this->data['service_orders']=$this->Clinical_model->patient_service_orders($patientid,$patientvisitid);
            $this->data['prescriptions']=$this->Clinical_model->patient_prescriptions($patientid,$patientvisitid);
            
            $this->data['physical_content']='clinical/patient_visit_physical_examinations';
            $this->data['vitals_content']='clinical/patient_visit_vitals';
            $this->data['diagnosis_content']='clinical/patient_visit_diagnosis';
            $this->data['allegies_content']='clinical/patient_visit_allegies';
            $this->data['service_orders_content']='clinical/patient_visit_service_orders';
            $this->data['prescription_orders_content']='clinical/patient_visit_prescription_orders';
            
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Patient Visit Overview<br/>Visit Id : '.$patientvisitid;
            $this->data['content']='clinical/patient_overview';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function patient_physical_examinations($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
           $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $docType=$seg[2];
            }
            
            if($docType == 1){
                
                    $data=$this->Clinical_model->patient_physcial_examinations($patientid,null,$start,$end);
                    require_once 'reports/clinical/patient_physcial_examinations_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/patient_physical_examinations/$patientid/$patientvisitid/".$start."_".$end."/";
            $config["total_rows"] =$this->Clinical_model->patient_physcial_examinations_info_count($patientid,null,$start,$end,$start,$end);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['physical_examinations'] = $this->Clinical_model->patient_physcial_examinations_info($patientid,null,$start,$end,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Patient Physical Examinations';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/patient_physcial_examinations';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function add_physical_examination($patientid,$patientvisitid,$inpatientCare){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            if($inpatientCare){
                
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            }else{
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            }
            
            $this->form_validation->set_rules('cardiovascular_system','Cardiovascular System','xss_clean');
            $this->form_validation->set_rules('respiratory_system','Respiratory System','xss_clean');
            $this->form_validation->set_rules('abdomen','Abdomen','xss_clean');
            $this->form_validation->set_rules('central_nervous_system','Central Nervous System','xss_clean');
            $this->form_validation->set_rules('muscular_skeletal_system','Muscular Skeletal System','xss_clean');

            if($this->form_validation->run() == true){
                $cardiovascular_system=$this->input->post('cardiovascular_system');
                $respiratory_system=$this->input->post('respiratory_system');
                $abdomen=$this->input->post('abdomen');
                $central_nervous_system=$this->input->post('central_nervous_system');
                $muscular_skeletal_system=$this->input->post('muscular_skeletal_system');

                $array=array(
                    'patientid'=>$patientid,
                    'patientvisitid'=>$patientvisitid,
                    'cardiovascularsystem'=>nl2br($cardiovascular_system),
                    'respiratorysystem'=>nl2br($respiratory_system),
                    'abdomen'=>nl2br($abdomen),
                    'centralnervoussystem'=>nl2br($central_nervous_system),
                    'muscularskeletalsystem'=>nl2br($muscular_skeletal_system),
                    'createdby'=>$this->session->userdata('user_id'),
                );

                $insrt=$this->Clinical_model->save_physcial_examination($array);

                if($insrt){
                    if($inpatientCare){
                        redirect('Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid,'refresh');
                    }
                    redirect('Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid,'refresh');
                }
                
                $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
            }
        
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=$inpatientCare;
            $this->data['title']='Record Patient Physical Examination';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/add_physcial_examination';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function patient_vitals_examinations($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                                  
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $docType=$seg[2];
            }
            
            if($docType == 1){
                    $data=$this->Clinical_model->patient_vitals_examinations($patientid,null,$start,$end);
                    require_once 'reports/clinical/patient_vitals_examinations_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/patient_vitals_examinations/$patientid/$patientvisitid/".$start."_".$end."/";
            $config["total_rows"] =$this->Clinical_model->patient_vitals_examinations_info_count($patientid,null,$start,$end);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['vitals_examinations'] = $this->Clinical_model->patient_vitals_examinations_info($patientid,null,$start,$end,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Patient Vitals Examinations';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/patient_vitals_examinations';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function add_vitals_examination($patientid,$patientvisitid,$inpatientCare){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            if($inpatientCare){
                
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            }else{
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            }
            
            $this->form_validation->set_rules('weight','Weight','xss_clean');
            $this->form_validation->set_rules('height','Height','xss_clean');
            $this->form_validation->set_rules('temperature','Temperature','xss_clean');
            $this->form_validation->set_rules('pulserate','Pulse Rate','xss_clean');
            $this->form_validation->set_rules('bloodprssure','Blood Pressure','xss_clean');
            $this->form_validation->set_rules('respirationrate','Respiration Rate','xss_clean');
            $this->form_validation->set_rules('spo2','SPO2','xss_clean');

            if($this->form_validation->run() == true){
                $weight=$this->input->post('weight');
                $height=$this->input->post('height');
                $temperature=$this->input->post('temperature');
                $pulserate=$this->input->post('pulserate');
                $bloodpressure=$this->input->post('bloodpressure');
                $respirationrate=$this->input->post('respirationrate');
                $spo2=$this->input->post('spo2');

                $array=array(
                    'patientid'=>$patientid,
                    'patientvisitid'=>$patientvisitid,
                    'weight'=>$weight,
                    'height'=>$height,
                    'temperature'=>$temperature,
                    'pulserate'=>$pulserate,
                    'bloodpressure'=>$bloodpressure,
                    'respirationrate'=>$respirationrate,
                    'spo2'=>$spo2,
                    'createdby'=>$this->session->userdata('user_id'),
                );

                $insrt=$this->Clinical_model->save_vitals_examination($array);

                if($insrt){
                    if($inpatientCare){
                        redirect('Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid,'refresh');
                    }
                    redirect('Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid,'refresh');
                }
                
                $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
            }
        
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['inpatientCare']=$inpatientCare;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Record Patient Vitals Examination';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/add_vitals_examination';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function patient_allegies($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
        $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                $docType=$seg[3];
            }
            
             if($docType == 1){
                    
                    $data=$this->Clinical_model->patient_allegies($patientid,null,$start,$end,$status);
                    require_once 'reports/clinical/patient_allergies_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/patient_allegies/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_allegies_info_count($patientid,null,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['patient_allegies'] = $this->Clinical_model->patient_allegies_info($patientid,null,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Patient Allergies';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/patient_allegies';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function add_allegy($patientid,$patientvisitid,$inpatientCare){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            if($inpatientCare){
                
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            }else{
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            }
            
            
            $this->form_validation->set_rules('allegy','Allergy','xss_clean|required');
            $this->form_validation->set_rules('allegystatus','Status','xss_clean|required');
            $this->form_validation->set_rules('remarks','Rmarks','xss_clean');

            if($this->form_validation->run() == true){
                $allegy=$this->input->post('allegy');
                $allegystatus=$this->input->post('allegystatus');
                $remarks=  nl2br($this->input->post('remarks'));

                $array=array(
                    'patientid'=>$patientid,
                    'patientvisitid'=>$patientvisitid,
                    'allegy'=>$allegy,
                    'allegystatus'=>$allegystatus,
                    'remarks'=>$remarks,
                    'createdby'=>$this->session->userdata('user_id'),
                );

                $insrt=$this->Clinical_model->save_patient_allegy($array);

                if($insrt){
                    if($inpatientCare){
                        redirect('Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid);
                    }
                    redirect('Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid);
                }
                
                $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
            }
        
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatienCaret']=$inpatientCare;
            $this->data['title']='Record Patient Allergy';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/add_allegy';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function patient_diagnosis($patientid,$patientvisitid){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
        
            if($this->input->post('save')){
                
                if($this->input->post('diagnosis')){
                    $this->form_validation->set_rules('complains','Complains','required|xss_clean'); 
                    $provisional_diagnosis=$this->input->post('provisional_diagnosis');
                }
                
                if(!$this->input->post('diagnosis')){
                    
                    $this->form_validation->set_rules('final_comments','Final Comments','required|xss_clean'); 
                    $final_diagnosis=$this->input->post('final_diagnosis');
                }
               
                if(count($provisional_diagnosis) > 0 || count($final_diagnosis) > 0){
                    if($this->form_validation->run() == true){
                       
                       $diagnosis=$this->input->post('diagnosis');
                       $array=array(
                           'patientid'=>$patientid,
                           'patientvisitid'=>$patientvisitid,
                       );

                       if($diagnosis){
                           
                           $provisional_diagnosis=implode($this->config->item('multi_diagnosis_glue'), $provisional_diagnosis);
                           $array['complains']=  nl2br($this->input->post('complains'));
                           $array['provisionaldiagnosis']=$provisional_diagnosis;
                           $array['createdby']=$this->session->userdata('user_id');
                       }

                       if(!$diagnosis){
                           
                           $final_diagnosis=implode($this->config->item('multi_diagnosis_glue'), $final_diagnosis);
                           $array['finaldiagnosis']=$final_diagnosis;
                           $array['finalcomment']=nl2br($this->input->post('final_comments'));
                           $array['modifiedby']=$this->session->userdata('user_id');
                           $array['modifiedon']=date('Y-m-d H:i:s');
                       }
                     
                       $sv=$this->Clinical_model->save_patient_diagnosis($array,$diagnosis);

                       if($sv){
                           
                           $this->Clinical_model->update_consultation_status($patientid,$patientvisitid);
                           redirect('Clinical/patient_overview/'.$patientid.'/'.$patientvisitid,'refresh');
                       }
                       $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
                   }
                }else{
                    
                    $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">no diagnosis selected!</div>';
                }
                
            }
            

            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;    
            $this->data['title']='Patient Diagnosis';
            $this->data['patient_diagnosis']=$this->Clinical_model->patient_diagnosis($patientid,$patientvisitid);
            $this->data['complaints']=$this->Clinical_model->standard_complains();
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/patient_diagnosis';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function patient_service_orders($patientid,$patientvisitid){
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                $docType=$seg[3];
            }
            
            if($docType == 1){
                
                    $data=$this->Clinical_model->patient_service_orders($patientid,null,$start,$end,$status);
                    require_once 'reports/clinical/patient_service_orders_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/patient_service_orders/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_service_orders_info_count($patientid,null,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['service_orders'] = $this->Clinical_model->patient_service_orders_info($patientid,null,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Patient Service Orders';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/patient_service_orders';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function create_service_order($patientid,$patientvisitid,$inpatientCare){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           if($inpatientCare){
                
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            }else{
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            }
           
           
         
           
            if($this->input->post('add_service')){
                $order=$this->input->post('order');
               
                if(count($order) > 0){
                    $error=TRUE;
                    foreach($order as $value){
                        $orderinfo=explode('_',$value);
                        $array=array(
                            'patientid'=>$patientid,
                            'patientvisitid'=>$patientvisitid,
                            'departmentid'=>$orderinfo[2],
                            'subdepartmentid'=>$orderinfo[1],
                            'serviceid'=>$orderinfo[0],
                            'createdby'=>$this->session->userdata('user_id'),
                        );
                        
                        $check=$this->Reception_model->check_pending_order($patientid,$patientvisitid,$orderinfo[0]);
                        
                        if(!$check){
                            $resp=$this->Reception_model->save_order($array);
                            if($resp){

                                $error=FALSE;
                            }
                        }
                        
                    }
                    
                    
                    if(!$error){
                        
                        if($inpatientCare){
                            
                            redirect('Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid,'refresh');
                        }
                        redirect('Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid,'refresh');
                    }
                    
                    if($error){
                        $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">service order addtion failed!</div>';
                    }
                    
                }else{
                    
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">select atleast one service!</div>';
                }
            }
            
            
            //consultation department
           $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
           $dept2=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
           $patient=$this->Reception_model->patient($patientid);
           $patient=$this->Reception_model->patients($patient->id);
           $this->data['patient']=$patient;
           $this->data['patientid']=$patientid;
           $this->data['patientvisitid']=$patientvisitid;
           $this->data['inpatientCare']=$inpatientCare;
           $this->data['subdepartments']=$this->Reception_model->non_counsulation_active_subdepartments($dept[0]->id,$dept2[0]->id);
           $this->data['services']=$this->Reception_model->non_consultation_services($dept[0]->id);
           $this->data['patient_visit']=$this->Reception_model->patient_visit_details($patientvisitid);
           $this->data['title']="Investigation/Diagnosis/Other";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='clinical/create_service_order';
           $this->load->view('clinical/template2',$this->data);
    }
    
    public function remove_service_order($id,$patientid,$patientvisitid,$inpatientCare){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->Reception_model->remove_service_order($id);
           if($inpatientCare){
              redirect('Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid,'refresh'); 
           }
           redirect('Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid,'refresh');
    }
    
    public function patient_prescriptions($patientid,$patientvisitid){
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                $docType=$seg[3];
            }
            
            if($docType == 1){
                   
                    $data=$this->Clinical_model->patient_prescriptions($patientid,null,$start,$end,$status);
                    require_once 'reports/clinical/patient_prescription_orders_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/patient_prescriptions/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_prescriptions_info_count($patientid,null,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['prescription_orders'] = $this->Clinical_model->patient_prescriptions_info($patientid,null,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Patient Prescription Orders';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/patient_prescription_orders';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function create_dispense_order($patientid,$patientvisitid,$inpatientCare){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        
            if($inpatientCare){
                
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            }else{
                $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            }
            
        if($this->input->post('add_dispense_order')){
            
             $this->form_validation->set_rules('item_category','Item Category','required|xss_clean');
             $this->form_validation->set_rules('item','Item','required|xss_clean');
             
             if($this->input->post('item_category') == $this->config->item('drug_code')){
                $this->form_validation->set_rules('dosage','Dosage','required|xss_clean');
                $this->form_validation->set_rules('frequency','Frequency','required|xss_clean');
                $this->form_validation->set_rules('no_days','No Days','required|xss_clean');
             }else{
                 
                 $this->form_validation->set_rules('quantity','Quantity','required|xss_clean');
             }
            
             if ($this->form_validation->run() == TRUE)
                {
                 $itemid=$this->input->post('item');
                 
                 $dispense_array=array(
                     'patientid'=>$patientid,
                     'patientvisitid'=>$patientvisitid,
                     'itemid'=>$itemid,
                     'createdby'=>$this->session->userdata('user_id')
                 );
                 
                 if($this->input->post('item_category') == $this->config->item('drug_code')){
                    $dosage=$this->input->post('dosage');
                    $frequency=$this->input->post('frequency');
                    $no_days=$this->input->post('no_days');
                    $quantity=$dosage*$frequency*$no_days;
                    
                    $dispense_array['dosageid']=$dosage;
                    $dispense_array['frequencyid']=$frequency;
                    $dispense_array['days']=$no_days;
                 }else{

                     $quantity=$this->input->post('quantity');
                 }
                 
                 $dispense_array['quantity']=$quantity;
                 
                    $check=$this->Pharmacy_model->check_pending_order($patientid,$patientvisitid,$itemid);
                    
                    if(!$check){
                        $sv=$this->Pharmacy_model->save_dispense_order($dispense_array);
                    
                        if($sv){
                            if($inpatientCare){
                                
                                redirect('Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid,'refresh');
                            }
                            redirect('Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid,'refresh');
                        }
                        
                        $this->data['disp_msg']='<div class="alert alert-danger " role="alert" style="text-align:center;">posting failed!</div>';
                    }else{
                        $this->data['disp_msg']='<div class="alert alert-danger " role="alert" style="text-align:center;">clear pending item order!</div>';
                    }
                    
                }
             }
             
        $patient=$this->Reception_model->patient($patientid);
        $patient=$this->Reception_model->patients($patient->id);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['patientvisitid']=$patientvisitid;
        $this->data['inpatientCare']=$inpatientCare;
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories(null,$this->config->item('drug_code'));
        $this->data['items'] = $this->Inventory_model->active_items();
        $this->data['drug_frequencies'] = $this->Pharmacy_model->active_drug_frequencies();
        $this->data['drug_dosages'] = $this->Pharmacy_model->active_drug_dosages();
        $this->data['title']="Create Prescription Order";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['profile']=$this->SuperAdministration_model->profile_data();
        $this->data['content']='clinical/create_prescription_order';
        $this->load->view('clinical/template2',$this->data);
    }
    
    public function remove_dispense_order($id,$patientid,$patientvisitid,$inpatientCare){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->Pharmacy_model->remove_dispense_order($id);
           if($inpatientCare){
                redirect('Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid,'refresh');
           }
           redirect('Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid,'refresh');
    }
    
    public function investigation_results($patientid,$patientvisitid,$from,$to,$export=null){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            
            if($this->input->post('start')){
                
                $from=$this->input->post('start');
            }
            
            if($this->input->post('end')){
                
                $to=$this->input->post('end');
            }
                
            $data=$this->Reception_model->patient_visits($patientid,$patientvisitid,$from,$to);
               
            if($export == TRUE){

                require_once 'reports/investigation/investigation_results.php';
            }
            
           $this->data['title']="Patient Investigation Results";
           $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
           $this->data['patientid'] =$patientid;
           $this->data['patientvisitid'] =$patientvisitid;
           $this->data['start']=$from;
           $this->data['end']=$to;
           $this->data['visits']=$data;
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
          $this->data['content']='clinical/investigation_results2';
           $this->load->view('clinical/template2',$this->data);
   }
   
    public function patient_appointments($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/patient_appointments/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_appointments_info_count($patientid,null,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['appointments'] = $this->Clinical_model->patient_appointments_info($patientid,null,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Patient Appointments';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/patient_appointments';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function add_appointment($patientid,$patientvisitid){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Clinical/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/patient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/patient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Diagnosis','link'=>'Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/patient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/patient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/patient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Appointments','link'=>'Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid),
                              );
            
            $this->form_validation->set_rules('appointment_date','Appointment Date','xss_clean|required|callback_appointment_date');
            $this->form_validation->set_rules('appointment_time','Appointment Time','xss_clean|required|callback_appointment_time');

            if($this->form_validation->run() == true){
                $appointment_date=$this->input->post('appointment_date');
                $appointment_time=$this->input->post('appointment_time');
                
                if($appointment_date > date('Y-m-d')){
                    $array=array(
                    'patientid'=>$patientid,
                    'patientvisitid'=>$patientvisitid,
                    'appointmentdate'=>$appointment_date,
                    'appointmenttime'=>$appointment_time.':00',
                    'appointmentdoctor'=>$this->session->userdata('user_id'),
                    'createdby'=>$this->session->userdata('user_id'),
                    );

                    $insrt=$this->Clinical_model->save_clinical_appointment($array);

                    if($insrt){
                        $this->Clinical_model->update_consultation_status($patientid,$patientvisitid);
                        redirect('Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid);
                    }

                    $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
                }else{
                    $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">appointment date must be greater than current date ('.date('Y-m-d').')</div>';
                }
                
            }
        
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Record Appointment';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/add_appointment';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function cancel_appointment($id,$patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Clinical_model->cancel_appointment($id);
        redirect('Clinical/patient_appointments/'.$patientid.'/'.$patientvisitid,'refresh');
    }

/************************************************************************inpatient************************************************************************/
     
    public function inpatient_overview($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
           $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            
            $this->data['physical']=$this->Clinical_model->patient_physcial_examinations($patientid,$patientvisitid,null,null);
            $this->data['vitals']=$this->Clinical_model->patient_vitals_examinations($patientid,$patientvisitid,null,null);
            $this->data['diagnosis']=$this->Clinical_model->patient_diagnosis($patientid,$patientvisitid);
            $this->data['allegies']=$this->Clinical_model->patient_allegies($patientid,$patientvisitid);
            $this->data['service_orders']=$this->Clinical_model->patient_service_orders($patientid,$patientvisitid);
            $this->data['prescriptions']=$this->Clinical_model->patient_prescriptions($patientid,$patientvisitid);
            
            $this->data['physical_content']='clinical/patient_visit_physical_examinations';
            $this->data['vitals_content']='clinical/patient_visit_vitals';
            $this->data['diagnosis_content']='clinical/patient_visit_diagnosis';
            $this->data['allegies_content']='clinical/patient_visit_allegies';
            $this->data['service_orders_content']='clinical/patient_visit_service_orders';
            $this->data['prescription_orders_content']='clinical/patient_visit_prescription_orders';
            
            $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'ADMITTED');
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Patient Overview<br/>Admission Id : '.$admission[0]->admissionid;
            $this->data['content']='clinical/inpatient_overview';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function inpatient_physical_examinations($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
           $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $docType=$seg[2];
            }
            
            if($docType == 1){
                    $inpatient=TRUE;
                    $data=$this->Clinical_model->patient_physcial_examinations($patientid,$patientvisitid,$start,$end);
                    require_once 'reports/clinical/patient_physcial_examinations_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/inpatient_physical_examinations/$patientid/$patientvisitid/".$start."_".$end."/";
            $config["total_rows"] =$this->Clinical_model->patient_physcial_examinations_info_count($patientid,$patientvisitid,$start,$end,$start,$end);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['physical'] = $this->Clinical_model->patient_physcial_examinations_info($patientid,$patientvisitid,$start,$end,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=TRUE;
            $this->data['title']='Patient Physical Examinations';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/patient_visit_physical_examinations';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function inpatient_vitals_examinations($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $docType=$seg[2];
            }
            
            if($docType == 1){
                    $inpatient=TRUE;
                    $data=$this->Clinical_model->patient_vitals_examinations($patientid,$patientvisitid,$start,$end);
                    require_once 'reports/clinical/patient_vitals_examinations_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/inpatient_vitals_examinations/$patientid/$patientvisitid/".$start."_".$end."/";
            $config["total_rows"] =$this->Clinical_model->patient_vitals_examinations_info_count($patientid,$patientvisitid,$start,$end);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['vitals'] = $this->Clinical_model->patient_vitals_examinations_info($patientid,$patientvisitid,$start,$end,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=TRUE;
            $this->data['title']='Patient Vitals Examinations';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/patient_visit_vitals';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function inpatient_allegies($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
        $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                $docType=$seg[3];
            }
            
            if($docType == 1){
                    $inpatient=TRUE;
                    $data=$this->Clinical_model->patient_allegies($patientid,$patientvisitid,$start,$end,$status);
                    require_once 'reports/clinical/patient_allergies_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/inpatient_allegies/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_allegies_info_count($patientid,$patientvisitid,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['allegies'] = $this->Clinical_model->patient_allegies_info($patientid,$patientvisitid,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=TRUE;
            $this->data['title']='Patient Allergies';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/patient_visit_allegies';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function inpatient_service_orders($patientid,$patientvisitid){
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                $docType=$seg[3];
            }
            
            if($docType == 1){
                    $inpatient=TRUE;
                    $data=$this->Clinical_model->patient_service_orders($patientid,$patientvisitid,$start,$end,$status);
                    require_once 'reports/clinical/patient_service_orders_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/inpatient_service_orders/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_service_orders_info_count($patientid,$patientvisitid,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['service_orders'] = $this->Clinical_model->patient_service_orders_info($patientid,$patientvisitid,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=TRUE;
            $this->data['title']='Patient Service Orders';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/patient_visit_service_orders';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function inpatient_prescriptions($patientid,$patientvisitid){
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $status=$seg[2];
                $this->data['status']=$status;
                
                $docType=$seg[3];
            }
            
            if($docType == 1){
                
                    $inpatient=TRUE;
                    $data=$this->Clinical_model->patient_prescriptions($patientid,$patientvisitid,$start,$end,$status);
                    require_once 'reports/clinical/patient_prescription_orders_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/inpatient_prescriptions/$patientid/$patientvisitid/".$start."_".$end."_".$status."/";
            $config["total_rows"] =$this->Clinical_model->patient_prescriptions_info_count($patientid,$patientvisitid,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['prescriptions'] = $this->Clinical_model->patient_prescriptions_info($patientid,$patientvisitid,$start,$end,$status,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['inpatientCare']=TRUE;
            $this->data['title']='Patient Prescription Orders';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/patient_visit_prescription_orders';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function inpatient_investigation_results($patientid,$patientvisitid,$from,$to,$export=null){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                        array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                        array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                        array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                        array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                        array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                        array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                        array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                        array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                        array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                                    );
            
            
            if($this->input->post('start')){
                
                $from=$this->input->post('start');
            }
            
            if($this->input->post('end')){
                
                $to=$this->input->post('end');
            }
                
            $data=$this->Reception_model->patient_visits($patientid,$patientvisitid,$from,$to);
               
            if($export == TRUE){

                require_once 'reports/investigation/investigation_results.php';
            }
            
           $this->data['title']="Patient Investigation Results";
           $patient=$this->Reception_model->patient($patientid);
           $this->data['patient']=$this->Receptionpatient_file_details_model->patients($patient->id);
           $this->data['patientid'] =$patientid;
           $this->data['patientvisitid'] =$patientvisitid;
           $this->data['start']=$from;
           $this->data['end']=$to;
           $this->data['visits']=$data;
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
          $this->data['content']='clinical/investigation_results';
           $this->load->view('clinical/template2',$this->data);
   }
   
    public function inpatient_doctor_notes($patientid,$patientvisitid){
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
           
            if($this->uri->segment(5)){
                $seg=explode('_',$this->uri->segment(5));
                
                $start=$seg[0];
                $this->data['start']=$start;
                
                $end=$seg[1];
                $this->data['end']=$end;
                
                $docType=$seg[2];
            }
            
            if($docType == 1){
                
                $data=$this->Clinical_model->inpatient_doctor_notes($patientid,$patientvisitid,$start,$end);
                require_once 'reports/clinical/doctor_notes_pdf.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Clinical/inpatient_doctor_notes/$patientid/$patientvisitid/".$start."_".$end."/";
            $config["total_rows"] =$this->Clinical_model->inpatient_doctor_notes_info_count($patientid,$patientvisitid,$start,$end);
            $config["per_page"] =5;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['doctorNotes'] = $this->Clinical_model->inpatient_doctor_notes_info($patientid,$patientvisitid,$start,$end,$page,$limit);
           
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['title']='Doctor Notes';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/inpatient_doctor_notes';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function write_doctor_notes($patientid,$patientvisitid){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );
            
            $this->form_validation->set_rules('overallprogress','Overall Progress','xss_clean|required');
            $this->form_validation->set_rules('complains','Complains','xss_clean|required');
            $this->form_validation->set_rules('recommendations','Recommendations','xss_clean|required');
            $this->form_validation->set_rules('admissionid','Admission Id','xss_clean|required');

            if($this->form_validation->run() == true){
                $overallprogress=$this->input->post('overallprogress');
                $complains=$this->input->post('complains');
                $recommendations=$this->input->post('recommendations');
                $admissionid=$this->input->post('admissionid');
                
                $array=array(
                    'patientid'=>$patientid,
                    'patientvisitid'=>$patientvisitid,
                    'admissionid'=>$admissionid,
                    'overallprogress'=>nl2br($overallprogress),
                    'complains'=>nl2br($complains),
                    'recommendations'=>nl2br($recommendations),
                    'createdby'=>$this->session->userdata('user_id'),
                );

                $insrt=$this->Clinical_model->save_doctor_note($array);

                if($insrt){
                    redirect('Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid);
                }
                
                $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
            }
        
            $admission=$this->Nursing_model->admission_details($patientid,$patientvisitid,null,'ADMITTED');
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['admissionid']=$admission[0]->admissionid;
            $this->data['title']='Write Doctor Notes';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/write_doctor_notes';
            $this->load->view('clinical/template2',$this->data);
    }
    
    public function inpatient_nurse_notes($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
        $this->data['side_menu']=array(
                                  array('title'=>'Patient Care','link'=>'Clinical/inpatient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Physical Examinations','link'=>'Clinical/inpatient_physical_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Vitals','link'=>'Clinical/inpatient_vitals_examinations/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Allergies','link'=>'Clinical/inpatient_allegies/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Service Orders','link'=>'Clinical/inpatient_service_orders/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Prescriptions','link'=>'Clinical/inpatient_prescriptions/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Investigation Results','link'=>'Clinical/inpatient_investigation_results/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Doctor Notes','link'=>'Clinical/inpatient_doctor_notes/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Nurse Notes','link'=>'Clinical/inpatient_nurse_notes/'.$patientid.'/'.$patientvisitid),
                              );

        if($this->input->post('start')){
            $start=$this->input->post('start');
            $this->data['start']=$start;
        }

        if($this->input->post('end')){
            $end=$this->input->post('end');
            $this->data['end']=$end;
        }


        if($this->uri->segment(5)){
            $seg=explode('_',$this->uri->segment(5));

            $start=$seg[0];
            $this->data['start']=$start;

            $end=$seg[1];
            $this->data['end']=$end;

            $docType=$seg[2];
        }
        
        if($docType == 1){
                
                $data=$this->Nursing_model->inpatient_nurse_notes($patientid,$patientvisitid,$start,$end);
                require_once 'reports/clinical/nurse_notes_pdf.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Clinical/inpatient_nurse_notes/$patientid/$patientvisitid/".$start."_".$end."/";
        $config["total_rows"] =$this->Nursing_model->inpatient_nurse_notes_info_count($patientid,$patientvisitid,$start,$end);
        $config["per_page"] =5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =6;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['nurseNotes'] = $this->Nursing_model->inpatient_nurse_notes_info($patientid,$patientvisitid,$start,$end,$page,$limit);

        $patient=$this->Reception_model->patient($patientid);
        $this->data['patient']=$this->Reception_model->patients($patient->id);
        $this->data['patientid']=$patientid;
        $this->data['patientvisitid']=$patientvisitid;
        $this->data['patientvisitid']=$patientvisitid;
        $this->data['title']='Nurse Notes';
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='clinical/inpatient_nurse_notes';
        $this->load->view('clinical/template2',$this->data);
    }
/****************************************************clinics**********************************************************************************************/
    
    public function family_planning_sessions(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
        $this->data['side_menu']=array(
                                  array('title'=>'Family Planning Sessions','link'=>'Clinical/family_planning_sessions/'),
                                  array('title'=>'New Session','link'=>'Clinical/new_family_planning_session/'),
                               );

        if($this->input->post('patientid')){
            $patientid=$this->input->post('patientid');
            $this->data['patientid']=$patientid;
        }
        
        if($this->input->post('name')){
            $name=$this->input->post('name');
            $this->data['name']=$name;
        }
        
        if($this->input->post('start')){
            $start=$this->input->post('start');
            $this->data['start']=$start;
        }

        if($this->input->post('end')){
            $end=$this->input->post('end');
            $this->data['end']=$end;
        }


        if($this->uri->segment(3)){
            $seg=explode('_',$this->uri->segment(3));

            $patientid=$seg[0];
            $this->data['patientid']=$patientid;

            $name=$seg[1];
            $this->data['name']=$name;
            
            $start=$seg[2];
            $this->data['start']=$start;
            
            $end=$seg[3];
            $this->data['end']=$end;
            
        }
        
        
        $config["base_url"] = base_url() . "index.php/Clinical/family_planning_sessions/".$patientid."_".$name."_".$start."_".$end."/";
        $config["total_rows"] =$this->Clinical_model->family_planning_sessions_info_count($patientid,$name,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['sessions'] = $this->Clinical_model->family_planning_sessions_info($patientid,$name,$start,$end,$page,$limit);

        $this->data['title']='Family Planning Sessions';
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='clinical/family_planning_sessions';
        $this->load->view('clinical/template',$this->data);
    }
    
    public function new_family_planning_session(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                  array('title'=>'Family Planning Sessions','link'=>'Clinical/family_planning_sessions/'),
                                  array('title'=>'New Session','link'=>'Clinical/new_family_planning_session/'),
                               );
            
            $this->form_validation->set_rules('patientid','Patient Id','xss_clean|required');
            $this->form_validation->set_rules('partner','Partner','xss_clean|required');
            $this->form_validation->set_rules('prevpregnancies','Previous Pregnancy Count','xss_clean|required|numeric');
            $this->form_validation->set_rules('birthcount','Birth Count','xss_clean|required|numeric');
            $this->form_validation->set_rules('stillbirthcount','Still birth Count','xss_clean|required|numeric');
            $this->form_validation->set_rules('abortions','Abortion Count','xss_clean|required|numeric');
            $this->form_validation->set_rules('childrencount','Children Count','xss_clean|required|numeric');
            $this->form_validation->set_rules('daychildrendeathcount','Seven day death count','xss_clean|required|numeric');
            $this->form_validation->set_rules('agelastchild','Last Child Age','xss_clean|required');
            $this->form_validation->set_rules('nextconsultation','Next Consultation','xss_clean|required');
            $this->form_validation->set_rules('remarks','Remarks','xss_clean');

            if($this->form_validation->run() == true){
                
                $patientid=$this->input->post('patientid');
                
                $check=$this->Reception_model->patient($patientid);
                
                if($check <> null){
                    
                    $patientvisitid=$this->Reception_model->patient_visits($patientid);
                    
                    if($patientvisitid[0]->exitstatus == 0){
                        
                        $partner=$this->input->post('partner');
                        $prevpregnancies=$this->input->post('prevpregnancies');
                        $birthcount=$this->input->post('birthcount');
                        $stillbirthcount=$this->input->post('stillbirthcount');
                        $abortions=$this->input->post('abortions');
                        $childrencount=$this->input->post('childrencount');
                        $daychildrendeathcount=$this->input->post('daychildrendeathcount');
                        $agelastchild=$this->input->post('agelastchild');
                        $contraceptionmethod=$this->input->post('contraceptionmethod');
                        $cmethod=null;
                        foreach($contraceptionmethod as $value){
                            
                            $cmethod .="$value||";
                        }
                        $cmethod= rtrim($cmethod,"||");
                        $nextconsultation=$this->input->post('nextconsultation');
                        $remarks=$this->input->post('remarks');

                        $array=array(
                            'patientid'=>$patientid,
                            'patientvisitid'=>$patientvisitid[0]->patientvisitid,
                            'partner'=>$partner,
                            'prevpregnancies'=>$prevpregnancies,
                            'birthcount'=>$birthcount,
                            'stillbirthcount'=>$stillbirthcount,
                            'abortions'=>$abortions,
                            'childrencount'=>$childrencount,
                            'sevendaychildrendeathcount'=>$daychildrendeathcount,
                            'agelastchild'=>$agelastchild,
                            'contraceptionmethod'=>$cmethod,
                            'nextconsultation'=>$nextconsultation,
                            'remarks'=>nl2br($remarks),
                            'createdby'=>$this->session->userdata('user_id'),
                        );
                        
                        $insrt=$this->Clinical_model->save_family_planning_session($array);

                        if($insrt){
                            
                            redirect('Clinical/family_planning_session_details/'.$patientid.'/'.$patientvisitid[0]->patientvisitid);
                        }

                        $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
                    }else{
                        
                        $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">invalid session!</div>';
                    }
                    
                }else{
                    $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">patient does not exist (check patient id)!</div>';
                }
            }
        
            $this->data['contraceptions']=$this->Clinical_model->contraceptive_methods();
            $this->data['title']='New Family Planning Session';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/new_family_planning_session';
            $this->load->view('clinical/template',$this->data);
    }
    
    public function family_planning_session_details($patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
           
        $this->data['side_menu']=array(
                                  array('title'=>'Family Planning Sessions','link'=>'Clinical/family_planning_sessions/'),
                                  array('title'=>'New Session','link'=>'Clinical/new_family_planning_session/'),
                               );

        
        $this->data['fpsessions']=$this->Clinical_model->family_planning_sessions($patientid,$patientvisitid);
        $this->data['title']='Family Planning Session(s) Details';
        $patient=$this->Reception_model->patient($patientid);
        $this->data['patient']=$this->Reception_model->patients($patient->id);
        $this->data['patientid']=$patientid;
        $this->data['patientvisitid']=$patientvisitid;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='clinical/family_planning_session_details';
        $this->load->view('clinical/template2',$this->data);
    }
    
    /**********************************************reports****************************************************************************************************/
    
    public function inpatients_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'Admission Report','link'=>'Clinical/inpatients_report'),
                                      array('title'=>'In Patient Transfers','link'=>'Clinical/inpatient_transfers_report'),
                                      array('title'=>'Out Patients Report','link'=>'Clinical/outpatients_report'),
                                      array('title'=>'Patient File','link'=>'Clinical/patient_medical_file'),
                                      
                                  );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('ward')) {
               $key['ward'] = $this->input->post('ward');
               $this->data['ward']=$this->input->post('ward');
           }
           
           if ($this->input->post('bed')) {
               $key['bed'] = $this->input->post('bed');
               $this->data['bed']=$this->input->post('bed');
           }

           if ($this->input->post('status')) {
               $key['status'] = $this->input->post('status');
               $this->data['status']=$this->input->post('status');
           }
           
           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['ward'] = $exp[5];
               $this->data['ward']=$key['ward'];
               
               $key['bed'] = $exp[7];
               $this->data['bed']=$key['bed'];
               
               $key['status'] = $exp[9];
               $this->data['status']=$key['status'];
               
               $docType = $exp[11];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $ward =$key['ward'];
            $bed =$key['bed'];
            $status =$key['status'];
            
            if($docType == 1){
                
                    $data=$this->Nursing_model->admissions($start,$end,$ward,$bed,$status);
                    require_once 'reports/clinical/admission_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Nursing_model->admissions($start,$end,$ward,$bed,$status);
                 require_once 'reports/clinical/admission_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Nursing/inpatients_report/start_".$key['start']."_end_".$key['end']."_ward_".$key['ward']."_bed_".$key['bed']."_status_".$status."/";
           $config["total_rows"] =$this->Nursing_model->admission_report_count($start,$end,$ward,$bed,$status);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Nursing_model->admission_report($start,$end,$ward,$bed,$status,$page,$limit);
           
           $this->data['title']="Admission Report";
           $this->data['wards']=$this->Inventory_model->units(null,$this->config->item('ward_unit_category_code'));
           $this->data['admission_status']=$this->config->item('admission_status');
           $this->data['beds']=$this->Administration_model->ward_beds();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='clinical/admission_report';
           $this->load->view('clinical/template',$this->data);
   }
    
    public function inpatient_transfers_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'Admission Report','link'=>'Clinical/inpatients_report'),
                                      array('title'=>'In Patient Transfers','link'=>'Clinical/inpatient_transfers_report'),
                                      array('title'=>'Out Patients Report','link'=>'Clinical/outpatients_report'),
                                      array('title'=>'Patient File','link'=>'Clinical/patient_medical_file'),
                                      
                                  );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
            }
           
           if ($this->input->post('bed')) {
               $key['bed'] = $this->input->post('bed');
               $this->data['bed']=$this->input->post('bed');
           }

           if ($this->input->post('ward')) {
               $key['ward'] = $this->input->post('ward');
               $this->data['ward']=$this->input->post('ward');
           }
           
           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['ward'] = $exp[5];
               $this->data['ward']=$key['ward'];
               
               $key['bed'] = $exp[7];
               $this->data['bed']=$key['bed'];
               
               $key['pid'] = $exp[9];
               $this->data['pid']=$key['pid'];
               
               $docType = $exp[11];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $ward =$key['ward'];
            $bed =$key['bed'];
            $pid =$key['pid'];
            
            if($docType == 1){
                
                    $data=$this->Nursing_model->admission_transfers($start,$end,$ward,$bed,$pid);
                    require_once 'reports/clinical/admission_transfer_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Nursing_model->admission_transfers($start,$end,$ward,$bed,$pid);
                 require_once 'reports/clinical/admission_transfer_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Nursing/inpatient_transfers_report/start_".$start."_end_".$end."_ward_".$ward."_bed_".$bed."_pid_".$pid."/";
           $config["total_rows"] =$this->Nursing_model->admission_transfer_report_count($start,$end,$ward,$bed,$pid);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Nursing_model->admission_transfer_report($start,$end,$ward,$bed,$pid,$page,$limit);
           
           $this->data['title']="In Patient Transfers Report";
           $this->data['wards']=$this->Inventory_model->units(null,$this->config->item('ward_unit_category_code'));
           $this->data['beds']=$this->Administration_model->ward_beds();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='clinical/admission_transfer_report';
           $this->load->view('clinical/template',$this->data);
   }
   
    public function outpatients_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'Admission Report','link'=>'Clinical/inpatients_report'),
                                      array('title'=>'In Patient Transfers','link'=>'Clinical/inpatient_transfers_report'),
                                      array('title'=>'Out Patients Report','link'=>'Clinical/outpatients_report'),
                                      array('title'=>'Patient File','link'=>'Clinical/patient_medical_file'),
                                      
                                  );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('visitcategory')) {
               $key['visitcategory'] = $this->input->post('visitcategory');
               $this->data['visitcategory']=$this->input->post('visitcategory');
           }
           
           if ($this->input->post('consultationcategory')) {
               $key['consultationcategory'] = $this->input->post('consultationcategory');
               $this->data['consultationcategory']=$this->input->post('consultationcategory');
           }

           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['visitcategory'] = $exp[5];
               $this->data['visitcategory']=$key['visitcategory'];
               
               $key['consultationcategory'] = $exp[7];
               $this->data['consultationcategory']=$key['consultationcategory'];
               
               $docType = $exp[9];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $visitcategory =$key['visitcategory'];
            $consultationcategory =$key['consultationcategory'];
            
            if($docType == 1){
                
                    $data=$this->Reception_model->outpatients($start,$end,$visitcategory,$consultationcategory);
                    require_once 'reports/reception/outpatients_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->outpatients($start,$end,$visitcategory,$consultationcategory);
                 require_once 'reports/reception/outpatients_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Nursing/outpatients_report/start_".$key['start']."_end_".$key['end']."_visiticateg_".$key['visitcategory']."_consultationcateg_".$key['consultationcategory']."/";
           $config["total_rows"] =$this->Reception_model->outpatients_report_count($start,$end,$visitcategory,$consultationcategory);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Reception_model->outpatients_report($start,$end,$visitcategory,$consultationcategory,$page,$limit);
           
           //consultation department
           $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
           
           $this->data['title']="Out Patients Report";
           $this->data['consultationcategories']=$this->Administration_model->consultation_categories($dept[0]->id,null);
           $this->data['visitcategories']=$this->Reception_model->visit_categories();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='clinical/outpatients_report';
           $this->load->view('clinical/template',$this->data);
   }
   
    public function patient_medical_file($patientid,$export){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'Admission Report','link'=>'Clinical/inpatients_report'),
                                      array('title'=>'In Patient Transfers','link'=>'Clinical/inpatient_transfers_report'),
                                      array('title'=>'Out Patients Report','link'=>'Clinical/outpatients_report'),
                                      array('title'=>'Patient File','link'=>'Clinical/patient_medical_file'),
                                      
                                  );
            
            if($patientid <> null){
                
                $data=$this->Reception_model->patient_visits($patientid);
                
                if($export == TRUE){
                    
                    require_once 'reports/clinical/patient_medical_file.php';
                }
                
            }
            
            $this->form_validation->set_rules('patientid','Patient Id','xss_clean|required');

            if($this->form_validation->run() == true){
                
                $patientid=$this->input->post('patientid');
                 $data=$this->Reception_model->patient_visits($patientid);
                
            }
           
           $this->data['title']="Patient File";
           $this->data['patientid'] =$patientid;
           $this->data['visits']=$data;
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['patientFile']='clinical/patient_file_data';
           $this->data['content']='clinical/patient_file';
           $this->load->view('clinical/template',$this->data);
   }
    
    public function patient_file_details($patientid,$patientvisitid,$detailType){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
        
        if($detailType == 'PTOO1'){
            
            $this->data['filedata']=$this->Clinical_model->patient_physcial_examinations($patientid,$patientvisitid,null,null);
            $this->data['content']='common/patient_file_physical_examinations';
            
        }else if($detailType == 'PTOO2'){
            $this->data['filedata']=$this->Clinical_model->patient_vitals_examinations($patientid,$patientvisitid,null,null);
            $this->data['content']='common/patient_file_vital_examinations';
            
        }else if($detailType == 'PTOO3'){
           
            $this->data['filedata']=$this->Clinical_model->patient_diagnosis($patientid,$patientvisitid);
            $this->data['content']='common/patient_file_diagnosis';
            
        }else if($detailType == 'PTOO4'){
            $this->data['filedata']=$this->Clinical_model->patient_prescriptions($patientid,$patientvisitid,null,null,null);
            $this->data['content']='common/patient_file_prescriptions';
            
        }else if($detailType == 'PTOO5'){
            $this->data['filedata']=$this->Clinical_model->patient_service_orders($patientid,$patientvisitid,null,null,null);
            $this->data['content']='common/patient_file_services';
            
        }else if($detailType == 'PTOO6'){
            $this->data['filedata']=$this->Nursing_model->inpatient_nurse_notes($patientid,$patientvisitid,null,null);
            $this->data['content']='common/patient_file_nurse_notes';
            
        }else if($detailType == 'PTOO7'){
            $this->data['filedata']=$this->Clinical_model->inpatient_doctor_notes($patientid,$patientvisitid,null,null);
            $this->data['content']='common/patient_file_doctor_notes';
            
        }else if($detailType == 'PTOO8'){
            
            $this->data['filedata']=$this->Clinical_model->patient_allegies($patientid,$patientvisitid,null,null,null);
            $this->data['content']='common/patient_file_allegies';
        }else{
            
            $this->data['filedata']=$this->Reception_model->patient_visits($patientid,$patientvisitid,null,null);
            $this->data['content']='common/patient_file_investigation_results';
        }
        
        $this->data['title']="Patient File";
        $patient=$this->Reception_model->patient($patientid);
        $this->data['patient']=$this->Reception_model->patients($patient->id);
        $this->data['patientid']=$patientid;
        $this->data['patientvisitid']=$patientvisitid;
        $this->data['identifier']='ID02';
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->load->view('clinical/template2',$this->data);
    }
    
    public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Clinical/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Clinical/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['profile']=$this->SuperAdministration_model->profile_data();
                $this->data['content']='clinical/profile';
		$this->load->view('clinical/template',$this->data);
        }
        
    public function edit_profile(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Clinical/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Clinical/change_password'),
                                        );
            
            $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
            $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
            $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
            $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
            $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if($this->form_validation->run() == true){

                 $firstname=$this->input->post('first_name');
                 $middlename=$this->input->post('middle_name');
                 $lastname=$this->input->post('last_name');
                 $username=$this->input->post('username');
                 $email=$this->input->post('email');
                 $mobile=$this->input->post('mobile');

                $user=array(
                            'FIRST_NAME'=>$firstname,
                            'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                            'LAST_NAME'=>$lastname,
                            'USERNAME'=>$username,
                            'EMAIL'=>$email<>null?$email:null,
                            'MSISDN'=>$mobile,
                        );

                $user_group=array(
                            'GROUP_ID'=>$this->session->userdata('group')
                        );

                $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                if($username_check){


                    //do registration
                     $resp=$this->SuperAdministration_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                       if($resp){

                           redirect('Clinical/profile','refresh');
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                       }
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                }
            }

            $this->data['title']="Edit Profile";
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['content']='clinical/edit_profile';
            $this->load->view('clinical/template',$this->data);
    }
        
    public function change_password(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Clinical/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Clinical/change_password'),
                                        );

            $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
            $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 

            if($this->form_validation->run() == true){

                $password=$this->input->post('password');
                $pchange=$this->SuperAdministration_model->change_password($password);

                if($pchange){
                           
                        $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">password change successfull!</div>';
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">password change failed!</div>';
                }
            }

            $this->data['title']="Change Password";
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='clinical/change_password';
            $this->load->view('clinical/template',$this->data);
    }
    
    function cur_password($password){
            
            $pf=$this->SuperAdministration_model->profile_data();
            $hashed_old=$this->SuperAdministration_model->hash_db_password($password,$pf);
           
             if($pf->password <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
    }
    
    function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('Doctor')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
    
    function username_uniqueness($username,$id){
            
        return $this->SuperAdministration_model->check_username($username,$id);
    }
    
    function appointment_date($date){
        
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
    }
    
    function appointment_time($time) {
        
            if (is_object(DateTime::createFromFormat('H:i', $time))) {
                return TRUE;
            } else {
                $this->form_validation->set_message('appointment_time', "The %s must contain hours:minutes in 24 hours");
                return FALSE;
            }
    }
   
}