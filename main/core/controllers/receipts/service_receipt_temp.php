<?php

$sp=$this->SuperAdministration_model->get_sponsor_by_code($bill_data[0]->sponsorid);
$pm=$this->SuperAdministration_model->get_paymentMode_by_code($bill_data[0]->paymentmodeid);
$name=$this->Reception_model->patient($bill_data[0]->patientid);
$user_info = $this->ion_auth->user($bill_data[0]->createdby)->row();
$category=$this->SuperAdministration_model->institution_categories($institution->category);
$inst_name=$institution <> null?$institution->name.' '.$category[0]->name:"HOSPITAL/DISPENSARY NAME";
$this->load->library('Pdf');
$this->pdf->set_subtitle('');
$this->pdf->start_pdf(FALSE);
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');
$this->pdf->AddPage();
$this->pdf->SetY(9);
$this->pdf->SetX(2);
$this->pdf->SetFont('times', '', 20);
$date = explode(" ", $bill_data[0]->createdon);
        $dtime=explode("-",$date[0]);
$html = '<div style="text-align:left;">
         <table>
           <tr>
                <td> <img  src="./logo/' . $institution->logoname . '" style="width: 800px; height: 300px; "/></td>
                <td>'.$inst_name . '.<br/>
                     P.O.Box ' . $institution->postal . ',<br/>' . $institution->city .'.<br/>
                     Phone no :'.$institution->phone.'.
                </td>
            </tr>
            <tr>
                <td>Date : <b>'.$dtime[2] . '/' . $dtime[1] . '/' . $dtime[0].'</b></td>
                <td>Time : <b>'.$date[1].'</b></td>
            </tr>
            <tr>
                <td>Patient Id :  <b>' . $bill_data[0]->patientid. '</b></td>
                <td>Name : <b>' . $name->name . '</b></td>
            </tr>
            <tr>
                <td>Sponsor : <b>'.$sp[0]->shortname.'</b></td>
                <td>Payment Mode : <b>' . $pm->name . '</b> </td>
            </tr>
            <tr>
                <td colspan="2">Receipt No : <b>'.$bill_data[0]->transactionid.'</b> </td>
            </tr>
         </table>
         <hr/> 
           <table>';
           $html .='<tr><td><b>Service</b></td><td style="text-align:center;"><b>Amount</b></td></tr>';
           
           foreach($bill_data as $key=>$value){
               $total +=$value->amount;
               $srv=$this->Administration_model->services($value->service_item_id);
               $html .='<tr>
                            <td>'.$srv[0]->name.'</td>
                            <td style="text-align:center;">
                            '. number_format($value->amount, 2) . '</td>'
                       .'</tr>';
               
            }
           
                 $html .='<tr>'
                         . '<td> <b>Total : </b> &nbsp;&nbsp;</td>'
                         . '<td style="text-align:center;">' . number_format($total, 2) . '</td>
                        </tr>
                      </table>
                     </div>
                    <hr/>              
                    Issued by : ';

                    $html.= $user_info->first_name . ' ' . $user_info->middle_name . ' ' . $user_info->last_name . '
        </div>';


$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Service_'.$bill_data[0]->patientid.'.pdf', 'I');
exit;