<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Processor extends CI_Controller {

    function __construct() {
        parent::__construct();
       	$this->load->model('Processor_model');
    }
    
    /*
     * process expired visits
     */
    
    function process_expired_visits(){
        $destination="./logs/Expired_Visits_".date('Y-m-d').'.log'; 
        error_log("Processing Expired Visits ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        $status=$this->Processor_model->process_expired_visits();
        
        if($status){
            error_log("Success ".date('Y-m-d H:i:s')."\n", 3, $destination);
        }else{
           error_log("Fail ".date('Y-m-d H:i:s')."\n", 3, $destination); 
        }
    }
    
    /*
     * process expired appointments
     */
    
    function process_expired_appointments(){
        $destination="./logs/Expired_Appointments_".date('Y-m-d').'.log'; 
        error_log("Processing Expired Appointments ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        $status=$this->Processor_model->process_expired_appointments();
        
        if($status){
            error_log("Success ".date('Y-m-d H:i:s')."\n", 3, $destination);
        }else{
           error_log("Fail ".date('Y-m-d H:i:s')."\n", 3, $destination); 
        }
    }
}