<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Management extends CI_Controller {

    function __construct() {
        parent::__construct();
       
        $this->load->model('Inventory_model');	
	$this->load->model('Pharmacy_model');	
	$this->load->model('Reception_model');	
	$this->load->model('Clinical_model');	
	$this->load->model('Nursing_model');	
	$this->load->model('Investigation_model');	

        
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Home';
            $this->data['content']='management/dashboard';
            $this->load->view('management/template3',$this->data);
    }
    
    public function aggregated_collection(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
            $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Management/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Management/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Management/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Management/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Management/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Management/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Management/prepaid_accounts'),
                           );
        
        $start=$this->input->post('start') <> null?$this->input->post('start'):date('Y-m-d');
        $end=$this->input->post('end') <> null?$this->input->post('end'):date('Y-m-d');
        $department=$this->input->post('department');
        $subdepartment=$this->input->post('subdepartment');
        $sponsor=$this->input->post('sponsor');
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $start = $exp[1];
            $end = $exp[3];
            $sponsor = $exp[5];
            $department = $exp[7];
            $subdepartment = $exp[9];
            $docType = $exp[11];
        }
        
        $data=$this->Reception_model->aggregated_collection($start,$end,$sponsor,$department,$subdepartment,null);
        if($docType == 1){
                
            require_once 'reports/reception/aggregated_collection_report_pdf.php';
        }

        if($docType == 2){

             require_once 'reports/reception/aggegated_collection_report_excel.php';
        }
        
        $this->data['title']="Aggregated Collections";
        $this->data['departments']=$this->Administration_model->active_departments();
        $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
        $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
        $this->data['sponsor']=$sponsor;
        $this->data['subdepartment']=$subdepartment;
        $this->data['department']=$department;
        $this->data['end']=$end;
        $this->data['start']=$start;
        $this->data['aggregated_collections']=$data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='management/aggregated_collection';
        $this->load->view('management/template',$this->data);
    }
    
    public function general_collection(){
        if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Management/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Management/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Management/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Management/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Management/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Management/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Management/prepaid_accounts'),
                           );
               
        $start=$this->input->post('start') <> null?$this->input->post('start'):date('Y-m-d');
        $end=$this->input->post('end') <> null?$this->input->post('end'):date('Y-m-d');
        $department=$this->input->post('department');
        $subdepartment=$this->input->post('subdepartment');
        $sponsor=$this->input->post('sponsor');
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $start = $exp[1];
            $end = $exp[3];
            $sponsor = $exp[5];
            $department = $exp[7];
            $subdepartment = $exp[9];
            $docType = $exp[11];
        }
        
        $data=$this->Reception_model->general_collection($start,$end,$sponsor,$department,$subdepartment,null);
        if($docType == 1){
                
            require_once 'reports/reception/general_collection_report_pdf.php';
        }

        if($docType == 2){

             require_once 'reports/reception/general_collection_report_excel.php';
        }
        
        $this->data['title']="General Collections";
        $this->data['departments']=$this->Administration_model->active_departments();
        $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
        $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
        $this->data['sponsor']=$sponsor;
        $this->data['subdepartment']=$subdepartment;
        $this->data['department']=$department;
        $this->data['end']=$end;
        $this->data['start']=$start;
        $this->data['general_collections']=$data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='management/general_collection';
        $this->load->view('management/template',$this->data);
    }
    
    public function package_accounts(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
        $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Management/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Management/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Management/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Management/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Management/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Management/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Management/prepaid_accounts'),
                           );
        
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('package')){
            $key['package'] = $this->input->post('package');
            $this->data['package']=$this->input->post('package');
        }

        if ($this->input->post('subscriptionid')) {
            $key['subscriptionid'] = $this->input->post('subscriptionid');
            $this->data['subscriptionid']=$this->input->post('subscriptionid');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
             $key['subscriptionid'] = $exp[3];
            $this->data['subscriptionid']=$key['subscriptionid'];

            $key['package'] = $exp[5];
            $this->data['package']=$key['package'];
        }

        $patientid =$key['patientid'];
        $subscriptionid =$key['subscriptionid'];
        $package =$key['package'];
        
        $config["base_url"] = base_url() . "index.php/Management/package_accounts/patientid_".$key['patientid']."_subscriptionid_".$key['subscriptionid']."_package_".$key['package']."/";
        $config["total_rows"] =$this->Reception_model->patient_packages_count($patientid,$subscriptionid,$package);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_packages'] = $this->Reception_model->patient_packages($patientid,$subscriptionid,$package,$page,$limit);

        $this->data['title']="Patient Packages";
        $this->data['packages']=$this->Administration_model->packages();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='management/patient_packages';
        $this->load->view('management/template',$this->data);       
    }
    
    public function patient_package_details($subscriptionid,$patientid,$docType){
        if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Management/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Management/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Management/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Management/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Management/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Management/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Management/prepaid_accounts'),
                           );
               
        if($docType == 1){
                
                $data=$this->Reception_model->subscriptions($subscriptionid);
                require_once 'reports/reception/package_account_report_pdf.php';
        }

        if($docType == 2){

             $data=$this->Reception_model->subscriptions($subscriptionid);
             require_once 'reports/reception/package_account_report_excel.php';
        }    
        $patient=$this->Reception_model->patient($patientid);       
        $patient=$this->Reception_model->patients($patient->id);       
        $this->data['patient_package_details'] =$this->Reception_model->subscriptions($subscriptionid);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['subscriptionid']=$subscriptionid;
        $this->data['title']=$subscriptionid." Subscription Details";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='management/patient_package_details';
        $this->load->view('management/template2',$this->data);
    }
    
    public function patients_debtors(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Management/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Management/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Management/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Management/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Management/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Management/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Management/prepaid_accounts'),
                           );
               
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('start')){
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
             $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];
        }

        $patientid =$key['patientid'];
        $start =$key['start'];
        $end =$key['end'];
        
        $config["base_url"] = base_url() . "index.php/Management/patients_debtors/patientid_".$key['patientid']."_start_".$key['start']."_end_".$key['end']."/";
        $config["total_rows"] =$this->Reception_model->patient_debtors_count($patientid,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_debtors'] = $this->Reception_model->patient_debtors($patientid,$start,$end,$page,$limit);

        $this->data['title']="Patient Debtors";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='management/patient_debtors';
        $this->load->view('management/template',$this->data);
    }
    
    public function patient_debt_details($patientid,$docType){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Management/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Management/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Management/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Management/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Management/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Management/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Management/prepaid_accounts'),
                           );
            
            if($docType == 1){
                
                $data=$this->Reception_model->patient_debt_details($patientid);
                require_once 'reports/reception/patient_debt_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->patient_debt_details($patientid);
                 require_once 'reports/reception/patient_debt_report_excel.php';
            }
            
               if($this->input->post('clear_debt')){
                   $debts=$this->input->post('debt');
                   
                   if(count($debts) > 0){
                       
                       foreach($debts as $key=>$value){
                           
                           $data=explode('_',$value);
                           $array[]=array(
                               'item_service_id'=>$data[0],
                               'departmentid'=>$data[1],
                               'postpaid_transactionid'=>$data[2],
                            );
                       }
                       
                       if($array <> null){
                           
                           $txngenerate=FALSE;
                           foreach($array as $key=>$value){
                               
                               if($txngenerate==FALSE){
                                   
                                        $txnid=$this->Reception_model->get_transaction_id();
                                        $txngenerate=TRUE;
                                }
                                
                                $bill_txn=array(
                                    'transactionid'=>$txnid,
                                    'paymentmodeid'=>$this->config->item('instant_cash_bill_pmode_code'),
                                    'createdon'=>date('Y-m-d H:i:s'),
                                    'createdby'=>$this->session->userdata('user_id'),
                                    'paymentstatus'=>1,
                                );
                                
                                $debt_bill_txn=array(
                                    'reftransactionid'=>$txnid,
                                    'status'=>1,
                                    'modifiedby'=>$this->session->userdata('user_id'),
                                    'modifiedon'=>date('Y-m-d H:i:s'),
                                );
                                
                                $clear=$this->Reception_model->clear_debt($bill_txn,$debt_bill_txn,$value['item_service_id'],$value['postpaid_transactionid']);
                                
                                if($clear){
                                    
                                    $this->Reception_model->update_service_item_order($txnid,$value['postpaid_transactionid'],$value['item_service_id'],TRUE);
                                    redirect('Reception/service_order_receipt/'.$txnid.'/'.$patientid,'refresh');
                                }
                            }
                       }
                   }else{
                       $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">confirm atleast one debt!</div>';
                   }
               }
          
        $patient=$this->Reception_model->patient($patientid);       
        $patient=$this->Reception_model->patients($patient->id);  
        $this->data['patient_debt_details'] =$this->Reception_model->patient_debt_details($patientid);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['title']="Patient Debt Details";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='management/patient_debt_details';
        $this->load->view('management/template2',$this->data);
    }
    
    public function patient_statement($details){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Management/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Management/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Management/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Management/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Management/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Management/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Management/prepaid_accounts'),
                           );
        
        $details=explode("_",$details);
        $patientid=$details[1];
        $start=$details[3];
        $end=$details[5];
        $docType=$details[7];
        
        if($docType == 1){

            $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end);
            require_once 'reports/reception/patients_statement_report_pdf.php';
        }

        if($docType == 2){

             $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end);
             require_once 'reports/reception/patients_statement_report_excel.php';
        }
        
        $this->form_validation->set_rules('patientid','Patient Id','xss_clean|required|callback_patientid');
        $this->form_validation->set_rules('start','Start Date','xss_clean|required|callback_start');
        $this->form_validation->set_rules('end','End Date','xss_clean|required|callback_end');

        if($this->form_validation->run() == TRUE){
            
            $patientid=$this->input->post('patientid');
            $start=$this->input->post('start');
            $end=$this->input->post('end');
            
            $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end);
        }
           
        $this->data['title']="Patient Statement";
        $this->data['patientid']=$patientid;
        $this->data['start']=$start;
        $this->data['end']=$end;
        $this->data['data']=$data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['statement']='common/statement';
        $this->data['content']='management/patient_statement';
        $this->load->view('management/template',$this->data);
    }
    
    public function patient_transactions(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
        
        $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Management/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Management/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Management/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Management/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Management/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Management/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Management/prepaid_accounts'),
                           );
               
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('start')){
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }

        if ($this->input->post('sponsor')) {
            $key['sponsor'] = $this->input->post('sponsor');
            $this->data['sponsor']=$this->input->post('sponsor');
        }

        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
            $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];

            $key['sponsor'] = $exp[7];
            $this->data['sponsor']=$key['sponsor'];
            
            $docType = $exp[9];
        }

        $start =$key['start'];
        $end =$key['end'];
        $sponsor =$key['sponsor'];
        $patientid =$key['patientid'];
           
        if($docType == 1){
                
            $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end,$sponsor);
            require_once 'reports/reception/patients_transactions_report_pdf.php';
        }

        if($docType == 2){

             $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end,$sponsor);
             require_once 'reports/reception/patients_transactions_report_excel.php';
        }
            
        $config["base_url"] = base_url() . "index.php/Management/patient_transactions/patientid_".$key['patientid']."_start_".$key['start']."_end_".$key['end']."_sponsor_".$key['sponsor']."/";
        $config["total_rows"] =$this->Reception_model->patient_transactions_count($patientid,$start,$end,$sponsor);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_transactions'] = $this->Reception_model->patient_transactions($patientid,$start,$end,$sponsor,$page,$limit);

        $this->data['title']="Patient Transactions";
        $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='management/patient_transactions';
        $this->load->view('management/template',$this->data);
    }
   
    public function prepaid_accounts(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
        $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Management/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Management/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Management/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Management/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Management/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Management/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Management/prepaid_accounts'),
                           );
        
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('subscriptionid')) {
            $key['subscriptionid'] = $this->input->post('subscriptionid');
            $this->data['subscriptionid']=$this->input->post('subscriptionid');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
             $key['subscriptionid'] = $exp[3];
            $this->data['subscriptionid']=$key['subscriptionid'];

        }

        $patientid =$key['patientid'];
        $subscriptionid =$key['subscriptionid'];
        
        $config["base_url"] = base_url() . "index.php/Management/prepaid_accounts/patientid_".$key['patientid']."_subscriptionid_".$key['subscriptionid']."/";
        $config["total_rows"] =$this->Reception_model->patients_prepaid_accounts_count($patientid,$subscriptionid);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_prepaid'] = $this->Reception_model->patients_prepaid_accounts($patientid,$subscriptionid,$page,$limit);

        $this->data['title']="Patient Prepaid Accounts";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='management/patient_prepaid_accounts';
        $this->load->view('management/template',$this->data);       
    }
    
    public function patient_prepaid_account_details($subscriptionid,$patientid,$docType){
        if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Management/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Management/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Management/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Management/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Management/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Management/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Management/prepaid_accounts'),
                           );
         
            
            if($docType == 1){
                
                $data=$this->Reception_model->prepaid_accounts($subscriptionid);
                require_once 'reports/reception/prepaid_account_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->prepaid_accounts($subscriptionid);
                 require_once 'reports/reception/prepaid_account_report_excel.php';
            }
            
        $patient=$this->Reception_model->patient($patientid);       
        $patient=$this->Reception_model->patients($patient->id);       
        $this->data['patient_prepaid_details'] =$this->Reception_model->prepaid_accounts($subscriptionid);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['subscriptionid']=$subscriptionid;
        $this->data['title']=$subscriptionid." Subscription Details";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='management/patient_prepaid_details';
        $this->load->view('management/template2',$this->data);
    }
    
    public function inpatients_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
             $this->data['side_menu']=array(
                               array('title'=>'Admissions','link'=>'Management/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Management/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Management/outpatients_report'),
                               array('title'=>'Service Report','link'=>'Management/service_report'),
                               array('title'=>'Diagnosis Report','link'=>'Management/diagnosis_report'),
                           );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('ward')) {
               $key['ward'] = $this->input->post('ward');
               $this->data['ward']=$this->input->post('ward');
           }
           
           if ($this->input->post('bed')) {
               $key['bed'] = $this->input->post('bed');
               $this->data['bed']=$this->input->post('bed');
           }

           if ($this->input->post('status')) {
               $key['status'] = $this->input->post('status');
               $this->data['status']=$this->input->post('status');
           }
           
           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['ward'] = $exp[5];
               $this->data['ward']=$key['ward'];
               
               $key['bed'] = $exp[7];
               $this->data['bed']=$key['bed'];
               
               $key['status'] = $exp[9];
               $this->data['status']=$key['status'];
               
               $docType = $exp[11];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $ward =$key['ward'];
            $bed =$key['bed'];
            $status =$key['status'];
            
            if($docType == 1){
                
                    $data=$this->Nursing_model->admissions($start,$end,$ward,$bed,$status);
                    require_once 'reports/clinical/admission_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Nursing_model->admissions($start,$end,$ward,$bed,$status);
                 require_once 'reports/clinical/admission_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Management/inpatients_report/start_".$key['start']."_end_".$key['end']."_ward_".$key['ward']."_bed_".$key['bed']."_status_".$status."/";
           $config["total_rows"] =$this->Nursing_model->admission_report_count($start,$end,$ward,$bed,$status);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Nursing_model->admission_report($start,$end,$ward,$bed,$status,$page,$limit);
           
           $this->data['title']="Admission Report";
           $this->data['wards']=$this->Inventory_model->units(null,$this->config->item('ward_unit_category_code'));
           $this->data['admission_status']=$this->config->item('admission_status');
           $this->data['beds']=$this->Administration_model->ward_beds();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='management/admission_report';
           $this->load->view('management/template',$this->data);
   }
    
    public function inpatient_transfers_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                               array('title'=>'Admissions','link'=>'Management/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Management/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Management/outpatients_report'),
                               array('title'=>'Service Report','link'=>'Management/service_report'),
                               array('title'=>'Diagnosis Report','link'=>'Management/diagnosis_report'),
                           );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
            }
           
           if ($this->input->post('bed')) {
               $key['bed'] = $this->input->post('bed');
               $this->data['bed']=$this->input->post('bed');
           }

           if ($this->input->post('ward')) {
               $key['ward'] = $this->input->post('ward');
               $this->data['ward']=$this->input->post('ward');
           }
           
           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['ward'] = $exp[5];
               $this->data['ward']=$key['ward'];
               
               $key['bed'] = $exp[7];
               $this->data['bed']=$key['bed'];
               
               $key['pid'] = $exp[9];
               $this->data['pid']=$key['pid'];
               
               $docType = $exp[11];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $ward =$key['ward'];
            $bed =$key['bed'];
            $pid =$key['pid'];
            
            if($docType == 1){
                
                    $data=$this->Nursing_model->admission_transfers($start,$end,$ward,$bed,$pid);
                    require_once 'reports/clinical/admission_transfer_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Nursing_model->admission_transfers($start,$end,$ward,$bed,$pid);
                 require_once 'reports/clinical/admission_transfer_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Management/inpatient_transfers_report/start_".$start."_end_".$end."_ward_".$ward."_bed_".$bed."_pid_".$pid."/";
           $config["total_rows"] =$this->Nursing_model->admission_transfer_report_count($start,$end,$ward,$bed,$pid);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Nursing_model->admission_transfer_report($start,$end,$ward,$bed,$pid,$page,$limit);
           
           $this->data['title']="In Patient Transfers Report";
           $this->data['wards']=$this->Inventory_model->units(null,$this->config->item('ward_unit_category_code'));
           $this->data['beds']=$this->Administration_model->ward_beds();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='management/admission_transfer_report';
           $this->load->view('management/template',$this->data);
   }
   
    public function outpatients_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                               array('title'=>'Admissions','link'=>'Management/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Management/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Management/outpatients_report'),
                               array('title'=>'Service Report','link'=>'Management/service_report'),
                               array('title'=>'Diagnosis Report','link'=>'Management/diagnosis_report'),
                           );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('visitcategory')) {
               $key['visitcategory'] = $this->input->post('visitcategory');
               $this->data['visitcategory']=$this->input->post('visitcategory');
           }
           
           if ($this->input->post('consultationcategory')) {
               $key['consultationcategory'] = $this->input->post('consultationcategory');
               $this->data['consultationcategory']=$this->input->post('consultationcategory');
           }

           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['visitcategory'] = $exp[5];
               $this->data['visitcategory']=$key['visitcategory'];
               
               $key['consultationcategory'] = $exp[7];
               $this->data['consultationcategory']=$key['consultationcategory'];
               
               $docType = $exp[9];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $visitcategory =$key['visitcategory'];
            $consultationcategory =$key['consultationcategory'];
            
            if($docType == 1){
                
                    $data=$this->Reception_model->outpatients($start,$end,$visitcategory,$consultationcategory);
                    require_once 'reports/reception/outpatients_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->outpatients($start,$end,$visitcategory,$consultationcategory);
                 require_once 'reports/reception/outpatients_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Management/outpatients_report/start_".$key['start']."_end_".$key['end']."_visiticateg_".$key['visitcategory']."_consultationcateg_".$key['consultationcategory']."/";
           $config["total_rows"] =$this->Reception_model->outpatients_report_count($start,$end,$visitcategory,$consultationcategory);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Reception_model->outpatients_report($start,$end,$visitcategory,$consultationcategory,$page,$limit);
           
           //consultation department
           $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
           
           $this->data['title']="Out Patients Report";
           $this->data['consultationcategories']=$this->Administration_model->consultation_categories($dept[0]->id,null);
           $this->data['visitcategories']=$this->Reception_model->visit_categories();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='management/outpatients_report';
           $this->load->view('management/template',$this->data);
   }
   
   public function service_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                               array('title'=>'Admissions','link'=>'Management/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Management/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Management/outpatients_report'),
                               array('title'=>'Service Report','link'=>'Management/service_report'),
                               array('title'=>'Diagnosis Report','link'=>'Management/diagnosis_report'),
                           );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('service')) {
               $key['service'] = $this->input->post('service');
               $this->data['service']=$this->input->post('service');
           }
           
          

           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['service'] = $exp[5];
               $this->data['service']=$key['service'];
               
               $docType = $exp[7];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $service =$key['service'];
            
            if($docType == 1){
                   
                    $data=$this->Reception_model->service_report($start,$end,$service);
                    require_once 'reports/management/service_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->service_report($start,$end,$service);
                 require_once 'reports/management/service_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Management/service_report/start_".$key['start']."_end_".$key['end']."_service_".$key['service']."/";
           $config["total_rows"] =$this->Reception_model->service_report_info_count($start,$end,$service);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Reception_model->service_report_info($start,$end,$service,$page,$limit);
           
           $this->data['title']="Services Report";
           $this->data['services']=$this->Administration_model->services();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='management/service_report';
           $this->load->view('management/template',$this->data);
   }
   
   public function items_consumption(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
       $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Management/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Management/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Management/expired_items'),
                           array('title'=>'Main Stock','link'=>'Management/main_stock'),
                           array('title'=>'Units Stock','link'=>'Management/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Management/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Management/stock_transfer_report'),
                        );
        
       
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
            
            if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            $key['unit'] = $exp[5];
            
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
            $this->data['unit']=$key['unit'];
        }

        $unit=$key['unit'];;
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->items_consumption_report($unit,$item,$start,$end);
            require_once 'reports/inventory/items_consumption_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->items_consumption_report($unit,$item,$start,$end);
            require_once 'reports/inventory/items_consumption_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Management/items_consumption/0_".$unit."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->items_consumption_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['consumptions'] = $this->Inventory_model->items_consumption_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units']=$this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']=' Items Consumption';
        $this->data['content']='management/items_consumption';
        $this->load->view('management/template',$this->data);
    }
    
   public function expired_items(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
       $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Management/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Management/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Management/expired_items'),
                           array('title'=>'Main Stock','link'=>'Management/main_stock'),
                           array('title'=>'Units Stock','link'=>'Management/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Management/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Management/stock_transfer_report'),
                        );
        
        $segment=$this->uri->segment(3);
        $seg=explode('_',$segment);
        $docType=$seg[0];
        $category=$seg[1];
        $item=$seg[2];
        $unit=$seg[3];
        $start=$seg[4];
        $end=$seg[5];
        
        if($this->input->post('search')){
            
            $unit=$this->input->post('unit');
            $category=$this->input->post('category');
            $item=$this->input->post('item');
            $start=$this->input->post('start');
            $end=$this->input->post('end');
        }
        
        $data=$this->Inventory_model->expired_items($category,$item,$unit,$start,$end);
        
        if($docType == 1){
            
            require_once 'reports/inventory/expired_items_pdf.php';
        }
        
        if($docType == 2){
            
            require_once 'reports/inventory/expired_items_excel.php';
        }
        
        $this->data['category']=$category;
        $this->data['item']=$item;
        $this->data['unit']=$unit;
        $this->data['start']=$start;
        $this->data['end']=$end;
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['expired'] = $data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Expired Items';
        $this->data['content']='management/expired_items';
        $this->load->view('management/template',$this->data);
    }
   
    public function disposed_items(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
         $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Management/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Management/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Management/expired_items'),
                           array('title'=>'Main Stock','link'=>'Management/main_stock'),
                           array('title'=>'Units Stock','link'=>'Management/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Management/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Management/stock_transfer_report'),
                        );
        
        if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['unit'] = $exp[1];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['unit']=$key['unit'];
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        
        $unit =$key['unit'];
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->disposed_items_report($unit,$item,$start,$end);
            require_once 'reports/inventory/disposed_items_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->disposed_items_report($unit,$item,$start,$end);
            require_once 'reports/inventory/disposed_items_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Management/disposed_items/0_".$key['unit']."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->disposed_items_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['disposes'] = $this->Inventory_model->disposed_items_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Disposed Items';
        $this->data['content']='management/disposed_items';
        $this->load->view('management/template',$this->data);
    }
    
    public function main_stock($code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
         $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Management/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Management/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Management/expired_items'),
                           array('title'=>'Main Stock','link'=>'Management/main_stock'),
                           array('title'=>'Units Stock','link'=>'Management/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Management/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Management/stock_transfer_report'),
                        );
        
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(4)) {
            $exp = explode("_", $this->uri->segment(4));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];

        }

        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        $config["base_url"] = base_url() . "index.php/Management/main_stock/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =5;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['main_stock_items']='management/main_stock_items';
        $this->data['title']='Main Stock';
        $this->data['content']='management/main_stock';
        $this->load->view('management/template',$this->data);
    }
    
    public function inventory_item_stock_details($itemid,$code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
         $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Management/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Management/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Management/expired_items'),
                           array('title'=>'Main Stock','link'=>'Management/main_stock'),
                           array('title'=>'Units Stock','link'=>'Management/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Management/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Management/stock_transfer_report'),
                        );
         
        $this->data['item_inventory_details']=$this->Inventory_model->item_inventory_details($itemid,null);   
        $item_name=$this->Inventory_model->items($itemid);
        $this->data['title']=$item_name[0]->name.' Main Inventory Details';
        $this->data['code']=$code;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='management/item_main_inventory_details';
        $this->load->view('management/template',$this->data);
    }
    
    public function units_stock($unit,$code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
         $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Management/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Management/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Management/expired_items'),
                           array('title'=>'Main Stock','link'=>'Management/main_stock'),
                           array('title'=>'Units Stock','link'=>'Management/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Management/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Management/stock_transfer_report'),
                        );
        
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(5)) {
            $exp = explode("_", $this->uri->segment(5));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];

        }

        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        $config["base_url"] = base_url() . "index.php/Management/main_stock/$unit/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =6;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['unit']=$unit;
        $this->data['units_stock_items']='management/units_stock_items';
        $this->data['title']='Units Stock';
        $this->data['content']='management/units_stock';
        $this->load->view('management/template',$this->data);
    }
    
    public function inventory_unit_item_stock_details($itemid,$code,$unit){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
        $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Management/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Management/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Management/expired_items'),
                           array('title'=>'Main Stock','link'=>'Management/main_stock'),
                           array('title'=>'Units Stock','link'=>'Management/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Management/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Management/stock_transfer_report'),
                        );
        
        $this->data['item_inventory_details']=$this->Inventory_model->item_inventory_details($itemid,$unit);   
        $item_name=$this->Inventory_model->items($itemid);
        $unit_name=$this->Inventory_model->units($unit);
        $this->data['title']=$item_name[0]->name.' '.$unit_name[0]->name.' Inventory Details';
        $this->data['code']=$code;
        $this->data['unit']=$unit;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='management/item_unit_inventory_details';
        $this->load->view('management/template',$this->data);
    }
    
    public function stock_transfer_report(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
       $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Management/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Management/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Management/expired_items'),
                           array('title'=>'Main Stock','link'=>'Management/main_stock'),
                           array('title'=>'Units Stock','link'=>'Management/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Management/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Management/stock_transfer_report'),
                        );
        
        
            if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['unit'] = $exp[1];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['unit']=$key['unit'];
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        
        $unit =$key['unit'];
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->stock_tranfers_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_transfers_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->stock_tranfers_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_transfers_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Management/stock_transfer_report/0_".$key['unit']."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->stock_transfer_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['transfers'] = $this->Inventory_model->stock_transfer_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Stock Tranfers';
        $this->data['content']='management/stock_transfers_report';
        $this->load->view('management/template',$this->data);
    }
    
    public function stock_taking_report(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
         $this->data['side_menu']=array(
                           array('title'=>'Items Consumption','link'=>'Management/items_consumption'),
                           array('title'=>'Disposed Items','link'=>'Management/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Management/expired_items'),
                           array('title'=>'Main Stock','link'=>'Management/main_stock'),
                           array('title'=>'Units Stock','link'=>'Management/units_stock'),
                           array('title'=>'Stock Takings','link'=>'Management/stock_taking_report'),
                           array('title'=>'Stock Transfers','link'=>'Management/stock_transfer_report'),
                        );
        
            if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['unit'] = $exp[1];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['unit']=$key['unit'];
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        
        $unit =$key['unit'];
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->stock_takings_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_takings_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->stock_takings_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_takings_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Management/stock_taking_report/0_".$key['unit']."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->stock_taking_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['takings'] = $this->Inventory_model->stock_taking_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Stock Takings';
        $this->data['content']='management/stock_takings_report';
        $this->load->view('management/template',$this->data);
    }
    
    public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Management/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Management/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['profile']=$this->SuperAdministration_model->profile_data();
                $this->data['content']='management/profile';
		$this->load->view('management/template',$this->data);
        }
        
    public function edit_profile(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Management/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Management/change_password'),
                                        );
            
            $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
            $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
            $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
            $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
            $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if($this->form_validation->run() == true){

                 $firstname=$this->input->post('first_name');
                 $middlename=$this->input->post('middle_name');
                 $lastname=$this->input->post('last_name');
                 $username=$this->input->post('username');
                 $email=$this->input->post('email');
                 $mobile=$this->input->post('mobile');

                $user=array(
                            'FIRST_NAME'=>$firstname,
                            'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                            'LAST_NAME'=>$lastname,
                            'USERNAME'=>$username,
                            'EMAIL'=>$email<>null?$email:null,
                            'MSISDN'=>$mobile,
                        );

                $user_group=array(
                            'GROUP_ID'=>$this->session->userdata('group')
                        );

                $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                if($username_check){


                    //do registration
                     $resp=$this->SuperAdministration_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                       if($resp){

                           redirect('Clinical/profile','refresh');
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                       }
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                }
            }

            $this->data['title']="Edit Profile";
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['content']='management/edit_profile';
            $this->load->view('management/template',$this->data);
    }
        
    public function change_password(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Management/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Management/change_password'),
                                        );

            $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
            $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 

            if($this->form_validation->run() == true){

                $password=$this->input->post('password');
                $pchange=$this->SuperAdministration_model->change_password($password);

                if($pchange){
                           
                        $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">password change successfull!</div>';
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">password change failed!</div>';
                }
            }

            $this->data['title']="Change Password";
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='management/change_password';
            $this->load->view('management/template',$this->data);
    }
    
    function cur_password($password){
            
            $pf=$this->SuperAdministration_model->profile_data();
            $hashed_old=$this->SuperAdministration_model->hash_db_password($password,$pf);
           
             if($pf->password <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
    }
    
    function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('Manager')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
    
    function username_uniqueness($username,$id){
            
        return $this->SuperAdministration_model->check_username($username,$id);
    }
    
    function patientid($id){
            $patient=$this->Reception_model->patient($id);
           
             if($patient == null){
                 
                 $this->form_validation->set_message("patientid","The %s is incorrect");
                  return FALSE;
             }
            return TRUE;
    }
    
    function start($date){
        
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('start', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('start', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
    }
    
    function end($date){
        
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
    }
    
}