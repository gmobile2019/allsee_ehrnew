<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SuperAdministration extends CI_Controller {

    function __construct() {
        parent::__construct();
       
        $this->load->library('upload');
		
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }

    function index(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Home';
            $this->data['content']='superadministrator/dashboard';
            $this->load->view('superadministrator/template3',$this->data);
    }
    
    public function view_users()
	{
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                $this->data['side_menu']=array(
                                            array('title'=>'Create Users','link'=>'SuperAdministration/create_users'),
                                            array('title'=>'View Users','link'=>'SuperAdministration/view_users'),
                                        );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('group')) {
                    $key['group'] = $this->input->post('group');
                    $this->data['group']=$this->input->post('group');
                }

               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['group'] = $exp[3];
                    $this->data['group']=$key['group'];
                }
                
                $name =$key['name'];
                $group =$key['group'];
                
                $config["base_url"] = base_url() . "index.php/SuperAdministration/view_users/name_".$key['name']."_group_".$key['group']."/";
                $config["total_rows"] =$this->SuperAdministration_model->member_info_count($name,$group);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['members'] = $this->SuperAdministration_model->member_info($name,$group,$page,$limit);
                
                $this->data['title']="System Users";
                $this->data['groups']=$this->SuperAdministration_model->groups();
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='superadministrator/view_users';
		$this->load->view('superadministrator/template',$this->data);
	}
    
    public function create_users($id=null)
   {
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'Create Users','link'=>'SuperAdministration/create_users'),
                                            array('title'=>'View Users','link'=>'SuperAdministration/view_users'),
                                        );
           $error=FALSE;
           $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
           $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
           $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
           $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
           $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
           $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');
           $this->form_validation->set_rules('group','User Group','required|xss_clean');
            if($id <> null){

               if($this->input->post('password') <> null){

                 $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
                 $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');   
               }
           }else{

             $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
             $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');  
           }



            if ($this->form_validation->run() == TRUE)
           {


                $firstname=$this->input->post('first_name');
                $middlename=$this->input->post('middle_name');
                $lastname=$this->input->post('last_name');
                $username=$this->input->post('username');
                $email=$this->input->post('email');
                $mobile=$this->input->post('mobile');
                $group=$this->input->post('group');
                $password=$this->input->post('password');

                

                       $user=array(
                           'first_name'=>$firstname,
                           'middle_name'=>$middlename<>null?$middlename:null,
                           'ip_address'=>$this->input->ip_address(),
                           'last_name'=>$lastname,
                           'username'=>$username,
                           'email'=>$email<>null?$email:null,
                           'msisdn'=>$mobile,
                       );

                       $user_group=array(
                           'group_id'=>$group
                       );

                       //check username uniqueness
                       $username_check=$this->username_uniqueness($username,$id);
                       if($username_check){


                           //do registration
                            $resp=$this->SuperAdministration_model->system_user_registration($user,$user_group,$id,$password);

                              if($resp){

                                  redirect(current_url());
                              }else{
                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                              }
                       }else{
                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                        }
               
           }

           $this->data['title']="Create Users";
           $this->data['id']="$id";
           $this->data['member']=$this->SuperAdministration_model->get_member_info($id);
           $this->data['groups']=$this->SuperAdministration_model->get_registration_groups($this->session->userdata('group'));
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='superadministrator/create_users';
           $this->load->view('superadministrator/template',$this->data);
    }
     
    public function activate_deactivate_users($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_users($id,$status);
        redirect('SuperAdministration/view_users','refresh');
    }
        
    public function fill_institution_profile()
   {
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

            $this->data['side_menu']=array(
                                            array('title'=>'Fill Profile','link'=>'SuperAdministration/fill_institution_profile'),
                                            array('title'=>'View Profile','link'=>'SuperAdministration/view_institution_profile'),
                                            array('title'=>'Institution Config','link'=>'SuperAdministration/institution_config'),
                                            array('title'=>'Add Institution Images','link'=>'SuperAdministration/add_institution_images'),
                                            array('title'=>'Institution Images','link'=>'SuperAdministration/institution_images'),
                                        );
           $error=FALSE;
           $this->form_validation->set_rules('institution_id','Institution Id','required|xss_clean|numeric|exact_length[4]');
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('category','Category','xss_clean|required');
           $this->form_validation->set_rules('phone','Phone','required|xss_clean|numeric');
           $this->form_validation->set_rules('postal','Postal Address','xss_clean|numeric');
           $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
           $this->form_validation->set_rules('city', 'City', 'required|xss_clean');
           $this->form_validation->set_rules('website','Website','xss_clean');
            

           

            if ($this->form_validation->run() == TRUE)
           {
                
                
                
                $institution_id=$this->input->post('institution_id');
                $name=$this->input->post('name');
                $phone=$this->input->post('phone');
                $email=$this->input->post('email');
                $postal=$this->input->post('postal');
                $city=$this->input->post('city');
                $website=$this->input->post('website');
                $category=$this->input->post('category');
                
                $institution=array(
                   'name'=>$name,
                   'institution_id'=>$institution_id,
                   'category'=>$category,
                   'phone'=>$phone,
                   'postal'=>$postal<>null?$postal:null,
                   'city'=>$city,
                   'website'=>$website<>null?$website:null,
                   'email'=>$email,
               );
                        
                if($_FILES['logo']['name'] <> null){
                        $config['upload_path'] = './logo/';
                        $config['allowed_types'] = 'jpeg|jpg|png';
                        $filename=explode('.',$_FILES['logo']['name']);
                        $photo_ext=end($filename);
                        $logoname=$institution_id;
                        $filename=$logoname.".".$photo_ext;
                        $config['file_name']=$filename;
                        $config['overwrite']=true;
                        $this->upload->initialize($config);

                       if($this->upload->do_upload('logo')){



                           $logo=file_get_contents('./logo/'.$filename);
                           $logostring=base64_encode($logo);

                           $institution['logoname']=$filename;
                           $institution['logostring']=$logostring;
                        }else{
                          $this->data['message']=$this->upload->display_errors('<div class="alert alert-danger " role="alert" style="text-align:center;">','</div>');
                          $error=TRUE;
                       }
                   }

                   if(!$error){


                       //do registration
                        $resp=$this->SuperAdministration_model->save_institution_profile($institution);

                          if($resp){

                              redirect(current_url());
                          }else{

                              $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">not saved!</div>';
                          }
                   }
           }

           $this->data['title']="Fill Institution Profile";
           $this->data['id']="$id";
           $this->data['categories']=$this->SuperAdministration_model->institution_categories();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['content']='superadministrator/fill_Institution_profile';
           $this->load->view('superadministrator/template',$this->data);
    }
    
    public function view_institution_profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                 $this->data['side_menu']=array(
                                            array('title'=>'Fill Profile','link'=>'SuperAdministration/fill_institution_profile'),
                                            array('title'=>'View Profile','link'=>'SuperAdministration/view_institution_profile'),
                                            array('title'=>'Institution Config','link'=>'SuperAdministration/institution_config'),
                                            array('title'=>'Add Institution Images','link'=>'SuperAdministration/add_institution_images'),
                                            array('title'=>'Institution Images','link'=>'SuperAdministration/institution_images'),
                                        );
                
                $this->data['title']="Institution Profile";
                $this->data['content']='superadministrator/health_institution_profile';
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
		$this->load->view('superadministrator/template',$this->data);
        }
    
    public function add_institution_images()
   {
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'Fill Profile','link'=>'SuperAdministration/fill_institution_profile'),
                                            array('title'=>'View Profile','link'=>'SuperAdministration/view_institution_profile'),
                                            array('title'=>'Institution Config','link'=>'SuperAdministration/institution_config'),
                                            array('title'=>'Add Institution Images','link'=>'SuperAdministration/add_institution_images'),
                                            array('title'=>'Institution Images','link'=>'SuperAdministration/institution_images'),
                                        );
           
           $this->form_validation->set_rules('count','Image Count','required|xss_clean|numeric');
           
            if ($this->form_validation->run() == TRUE)
           {
                $images=array();
                $image_count=$this->input->post('count');
                $sequence=1;
                for($i=1;$i<=$image_count;$i++){
                    
                    if($_FILES['image-'.$i]['name'] <> null){
                        
                        $config['upload_path'] = './images/cover/';
                        $config['allowed_types'] = 'jpeg|jpg|png';
                        $filename=explode('.',$_FILES['image-'.$i]['name']);
                        $photo_ext=end($filename);
                        $logoname=$institution_id;
                        $filename='Img-'.date('YmdHis').".".$photo_ext;
                        $config['file_name']=$filename;
                        $config['overwrite']=true;
                        $this->upload->initialize($config);

                       if($this->upload->do_upload('image-'.$i)){
                           
                           $imagestring=file_get_contents('./images/cover/'.$filename);
                           $imagestring=base64_encode($imagestring);
                           
                           $images[]=array(
                               'imagename'=>$filename,
                               'imagestring'=>$imagestring,
                               'sequence'=>$sequence,
                               'lastupdate'=>date('Y-m-d H:i:s'),
                               'userid'=>$this->session->userdata('user_id'),
                           );
                           
                           $sequence++;
                        }
                   }
                }
                $sv=$this->SuperAdministration_model->save_cover_photos($images,$id);
                if(!$sv){
                   $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">not saved!</div>'; 
                }
                redirect(current_url());
            }

           $this->data['title']="Add Institution Cover Image(s)";
           $this->data['id']="$id";
           $this->data['categories']=$this->SuperAdministration_model->institution_categories();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['content']='superadministrator/add_institution_cover_images';
           $this->load->view('superadministrator/template',$this->data);
    }
    
    public function change_institution_image($id)
   {
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'Fill Profile','link'=>'SuperAdministration/fill_institution_profile'),
                                            array('title'=>'View Profile','link'=>'SuperAdministration/view_institution_profile'),
                                            array('title'=>'Institution Config','link'=>'SuperAdministration/institution_config'),
                                            array('title'=>'Add Institution Images','link'=>'SuperAdministration/add_institution_images'),
                                            array('title'=>'Institution Images','link'=>'SuperAdministration/institution_images'),
                                        );
           
                    if($_FILES['image']['name'] <> null){
                        
                        $config['upload_path'] = './images/cover/';
                        $config['allowed_types'] = 'jpeg|jpg|png';
                        $filename=explode('.',$_FILES['image']['name']);
                        $photo_ext=end($filename);
                        $logoname=$institution_id;
                        $filename='Img-'.date('YmdHis').".".$photo_ext;
                        $config['file_name']=$filename;
                        $config['overwrite']=true;
                        $this->upload->initialize($config);

                       if($this->upload->do_upload('image')){
                           
                           $imagestring=file_get_contents('./images/cover/'.$filename);
                           $imagestring=base64_encode($imagestring);
                           
                           $image=array(
                               'imagename'=>$filename,
                               'imagestring'=>$imagestring,
                               'lastupdate'=>date('Y-m-d H:i:s'),
                               'userid'=>$this->session->userdata('user_id'),
                           );
                            $sv=$this->SuperAdministration_model->save_cover_photos($image,$id);
                            if(!$sv){
                               $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">not saved!</div>'; 
                            }
                        }else{
                          $this->data['message']=$this->upload->display_errors('<div class="alert alert-danger " role="alert" style="text-align:center;">','</div>');
                       }
                    }
                        
          
           $this->data['title']="Change Institution Cover Image";
           $this->data['id']="$id";
           $this->data['categories']=$this->SuperAdministration_model->institution_categories();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['content']='superadministrator/change_institution_cover_image';
           $this->load->view('superadministrator/template',$this->data);
    }
    
    public function institution_images(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

            $this->data['side_menu']=array(
                                       array('title'=>'Fill Profile','link'=>'SuperAdministration/fill_institution_profile'),
                                       array('title'=>'View Profile','link'=>'SuperAdministration/view_institution_profile'),
                                       array('title'=>'Institution Config','link'=>'SuperAdministration/institution_config'),
                                       array('title'=>'Add Institution Images','link'=>'SuperAdministration/add_institution_images'),
                                       array('title'=>'Institution Images','link'=>'SuperAdministration/institution_images'),
                                   );

           $this->data['title']="Institution Cover Images";
           $this->data['content']='superadministrator/institution_images';
           $this->data['institution_cover_images']=$this->SuperAdministration_model->institution_cover_images();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->load->view('superadministrator/template',$this->data);
   }
   
   public function activate_deactivate_image($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_image($id,$status);
        redirect('SuperAdministration/institution_images','refresh');
    }
   
    public function institution_config($edit)
        {
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

            $this->data['side_menu']=array(
                                            array('title'=>'Fill Profile','link'=>'SuperAdministration/fill_institution_profile'),
                                            array('title'=>'View Profile','link'=>'SuperAdministration/view_institution_profile'),
                                            array('title'=>'Institution Config','link'=>'SuperAdministration/institution_config'),
                                            array('title'=>'Add Institution Images','link'=>'SuperAdministration/add_institution_images'),
                                            array('title'=>'Institution Images','link'=>'SuperAdministration/institution_images'),
                                        );
           $error=FALSE;
           $this->form_validation->set_rules('printer','Printer','required|xss_clean');
           $this->form_validation->set_rules('reg_charge','Registration Charge','xss_clean|required');
           
           if ($this->form_validation->run() == TRUE)
                {
                
                $printer=$this->input->post('printer');
                $reg_charge=$this->input->post('reg_charge');
                
                $inst_config=array(
                   'printer'=>$printer,
                   'registrationcharge'=>$reg_charge,
               );
                

                if(!$error){


                    //do registration
                     $resp=$this->SuperAdministration_model->save_institution_config($inst_config);

                       if($resp){

                           redirect(current_url());
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">not saved!</div>';
                       }
                }
           }

           $this->data['title']="Institution Configs";
           $this->data['id']="$id";
           $this->data['configs']=$this->SuperAdministration_model->institution_configs();
           $this->data['printers']=$this->SuperAdministration_model->printers();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['content']=$edit == 1?'superadministrator/alter_institution_cofigs':'superadministrator/institution_cofigs';
           $this->load->view('superadministrator/template',$this->data);
    }
    
    public function activate_deactivate_institution($status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
            $this->SuperAdministration_model->activate_deactivate_institution($status);
            redirect('SuperAdministration/view_institution_profile','refresh');
    }
    
    public function view_service_charges(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                 $this->data['side_menu']=array(
                                            array('title'=>'Add Service Charge','link'=>'SuperAdministration/add_service_charges'),
                                            array('title'=>'View Service Charges','link'=>'SuperAdministration/view_service_charges'),
                                        );
                
                
                $config["base_url"] = base_url() . "index.php/SuperAdministration/view_service_charges/";
                $config["total_rows"] =$this->SuperAdministration_model->service_charge_info_count();
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =3;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['service_charges'] = $this->SuperAdministration_model->service_charge_info($page,$limit);
                
                $this->data['title']="Service Charges";
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='superadministrator/view_service_charges';
		$this->load->view('superadministrator/template',$this->data);
	}
    
    public function add_service_charges($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'Add Service Charge','link'=>'SuperAdministration/add_service_charges'),
                                            array('title'=>'View Service Charges','link'=>'SuperAdministration/view_service_charges'),
                                        );
           
           $this->form_validation->set_rules('shortcode','Service Charge Code','required|xss_clean|numeric|exact_length[3]');
           $this->form_validation->set_rules('name','Name','xss_clean|required');
           $this->form_validation->set_rules('description','Description','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {


                $shortcode=$this->input->post('shortcode');
                $name=$this->input->post('name');
                $description=nl2br(trim($this->input->post('description')));

                

                       $service_charge=array(
                           'service_code'=>$shortcode,
                           'name'=>$name,
                           'description'=>$description
                       );


                       //name uniqueness
                       $code_check=$this->SuperAdministration_model->service_code_check($shortcode,$id);
                       if($code_check == null){


                           //do saving
                            $resp=$this->SuperAdministration_model->save_service_charge($service_charge,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">service code exists already!</div>';
                       }
               
           }

           $this->data['title']="Add Service Charge";
           $this->data['id']="$id";
           $this->data['service_charge']=$this->SuperAdministration_model->service_charge($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='superadministrator/add_service_charge';
           $this->load->view('superadministrator/template',$this->data);
    }
     
    public function activate_deactivate_service_charges($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_service_charge($status,$id);
        redirect('SuperAdministration/view_service_charges','refresh');
    }
    
    public function view_sponsors(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                $this->data['side_menu']=array(
                                           array('title'=>'Add Sponsor','link'=>'SuperAdministration/add_sponsor'),
                                           array('title'=>'Add Payment Mode','link'=>'SuperAdministration/add_paymentMode'),
                                           array('title'=>'View Sponsor(s)','link'=>'SuperAdministration/view_sponsors'),
                                           array('title'=>'View Payment Mode(s)','link'=>'SuperAdministration/view_paymentModes'),
                                       );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                }
                
                $name =$key['name'];
                
                $config["base_url"] = base_url() . "index.php/SuperAdministration/view_sponsors/name_".$key['name']."/";
                $config["total_rows"] =$this->SuperAdministration_model->sponsor_info_count($name);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['sponsors'] = $this->SuperAdministration_model->sponsor_info($name,$page,$limit);
                
                $this->data['title']="Sponsors";
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='superadministrator/view_sponsors';
		$this->load->view('superadministrator/template',$this->data);
	}
    
    public function add_sponsor($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                           array('title'=>'Add Sponsor','link'=>'SuperAdministration/add_sponsor'),
                                           array('title'=>'Add Payment Mode','link'=>'SuperAdministration/add_paymentMode'),
                                           array('title'=>'View Sponsor(s)','link'=>'SuperAdministration/view_sponsors'),
                                           array('title'=>'View Payment Mode(s)','link'=>'SuperAdministration/view_paymentModes'),
                                       );
           
           $this->form_validation->set_rules('sponsorcode','Sponsor Code','required|xss_clean|numeric|exact_length[3]');
           $this->form_validation->set_rules('shortname','Short Name','required|xss_clean');
           $this->form_validation->set_rules('fullname','Full Name','xss_clean|required');
           $this->form_validation->set_rules('description','Description','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {


                $sponsorcode=$this->input->post('sponsorcode');
                $shortname=$this->input->post('shortname');
                $fullname=$this->input->post('fullname');
                $description=nl2br(trim($this->input->post('description')));

                

                       $sponsor=array(
                           'sponsorcode'=>$sponsorcode,
                           'shortname'=>$shortname,
                           'fullname'=>$fullname,
                           'description'=>$description
                       );


                       //code uniqueness
                       $code_check=$this->SuperAdministration_model->sponsor_code_check($sponsorcode,$id);
                       if($code_check == null){


                           //do saving
                            $resp=$this->SuperAdministration_model->save_sponsor($sponsor,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">sponsor code exists already!</div>';
                       }
               
           }

           $this->data['title']="Add Sponsor";
           $this->data['id']="$id";
           $this->data['sponsor']=$this->SuperAdministration_model->sponsors($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='superadministrator/add_sponsor';
           $this->load->view('superadministrator/template',$this->data);
    }
     
    public function activate_deactivate_sponsor($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_sponsor($id,$status);
        redirect('SuperAdministration/view_sponsors','refresh');
    }
    
    public function view_paymentModes(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                $this->data['side_menu']=array(
                                           array('title'=>'Add Sponsor','link'=>'SuperAdministration/add_sponsor'),
                                           array('title'=>'Add Payment Mode','link'=>'SuperAdministration/add_paymentMode'),
                                           array('title'=>'View Sponsor(s)','link'=>'SuperAdministration/view_sponsors'),
                                           array('title'=>'View Payment Mode(s)','link'=>'SuperAdministration/view_paymentModes'),
                                       );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('sponsor')) {
                    $key['sponsor'] = $this->input->post('sponsor');
                    $this->data['sponsor']=$this->input->post('sponsor');
                }
                
                if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['sponsor'] = $exp[3];
                    $this->data['sponsor']=$key['sponsor'];
                }
                
                $name =$key['name'];
                $sponsor =$key['sponsor'];
                
                $config["base_url"] = base_url() . "index.php/SuperAdministration/view_paymentModes/name_".$key['name']."_sponsor_".$key['sponsor']."/";
                $config["total_rows"] =$this->SuperAdministration_model->paymentMode_info_count($name,$sponsor);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['paymentModes'] = $this->SuperAdministration_model->paymentMode_info($name,$sponsor,$page,$limit);
                
                $this->data['title']="Payment Modes";
                $this->data['sponsors']=$this->SuperAdministration_model->sponsors();
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='superadministrator/view_paymentModes';
		$this->load->view('superadministrator/template',$this->data);
	}
    
    public function add_paymentMode($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                        array('title'=>'Add Sponsor','link'=>'SuperAdministration/add_sponsor'),
                                        array('title'=>'Add Payment Mode','link'=>'SuperAdministration/add_paymentMode'),
                                        array('title'=>'View Sponsor(s)','link'=>'SuperAdministration/view_sponsors'),
                                        array('title'=>'View Payment Mode(s)','link'=>'SuperAdministration/view_paymentModes'),
                                    );
           
           $this->form_validation->set_rules('paymentmodecode','Payment Mode Code','required|xss_clean|numeric|exact_length[3]');
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('sponsor','Sponsor','xss_clean|required');
           $this->form_validation->set_rules('description','Description','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {


                $paymentmodecode=$this->input->post('paymentmodecode');
                $name=$this->input->post('name');
                $sponsor=$this->input->post('sponsor');
                $description=nl2br(trim($this->input->post('description')));

                

                       $paymentMode=array(
                           'paymentmodecode'=>$paymentmodecode,
                           'name'=>$name,
                           'sponsor_id'=>$sponsor,
                           'description'=>$description
                       );


                       //code uniqueness
                       $code_check=$this->SuperAdministration_model->paymentMode_code_check($paymentmodecode,$id);
                       if($code_check == null){


                           //do saving
                            $resp=$this->SuperAdministration_model->save_paymentMode($paymentMode,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">payment mode code exists already!</div>';
                       }
               
           }

           $this->data['title']="Add Payment Mode";
           $this->data['id']="$id";
           $this->data['sponsors']=$this->SuperAdministration_model->sponsors();
           $this->data['paymentMode']=$this->SuperAdministration_model->paymentModes($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='superadministrator/add_paymentMode';
           $this->load->view('superadministrator/template',$this->data);
    }
     
    public function activate_deactivate_paymentMode($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_paymentMode($status,$id);
        redirect('SuperAdministration/view_paymentModes','refresh');
    }
    
    public function item_categories(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
        } 
        
        $this->data['side_menu']=array(
                                        array('title'=>'View Item Categories','link'=>'SuperAdministration/item_categories'),
                                        array('title'=>'Add Item Categories','link'=>'SuperAdministration/add_item_category'),
                                       );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
               
                if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                }
                
                $name =$key['name'];
                
                $config["base_url"] = base_url() . "index.php/SuperAdministration/item_categories/name_".$key['name']."/";
                $config["total_rows"] =$this->SuperAdministration_model->item_categories_info_count($name);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['item_categories'] = $this->SuperAdministration_model->item_categories_info($name,$page,$limit);
                
                $this->data['title']="Item Categories";
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='superadministrator/item_categories';
		$this->load->view('superadministrator/template',$this->data);
    }
    
    public function add_item_category($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                        array('title'=>'View Item Categories','link'=>'SuperAdministration/item_categories'),
                                        array('title'=>'Add Item Categories','link'=>'SuperAdministration/add_item_category'),
                                       );
           
           $this->form_validation->set_rules('code','Category Code','required|xss_clean|numeric|exact_length[3]');
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('description','Description','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {


                $code=$this->input->post('code');
                $name=$this->input->post('name');
                $description=nl2br(trim($this->input->post('description')));

                

                       $itemCategory=array(
                           'code'=>$code,
                           'name'=>$name,
                           'description'=>$description
                       );


                       //code uniqueness
                       $code_check=$this->SuperAdministration_model->item_category_code_check($code,$id);
                       if($code_check == null){


                           //do saving
                            $resp=$this->SuperAdministration_model->save_item_category($itemCategory,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">item category code exists already!</div>';
                       }
               
           }

           $this->data['title']="Add Item Category";
           $this->data['id']="$id";
           $this->data['itemCategory']=$this->SuperAdministration_model->item_categories($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='superadministrator/add_item_category';
           $this->load->view('superadministrator/template',$this->data);
    }
    
    public function activate_deactivate_item_category($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_item_category($status,$id);
        redirect('SuperAdministration/item_categories','refresh');
    }
    
    public function unit_categories(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
        } 
        
        $this->data['side_menu']=array(
                                        array('title'=>'View Unit Categories','link'=>'SuperAdministration/unit_categories'),
                                        array('title'=>'Add Unit Categories','link'=>'SuperAdministration/add_unit_category'),
                                       );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
               
                if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                }
                
                $name =$key['name'];
                
                $config["base_url"] = base_url() . "index.php/SuperAdministration/unit_categories/name_".$key['name']."/";
                $config["total_rows"] =$this->SuperAdministration_model->unit_categories_info_count($name);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['unit_categories'] = $this->SuperAdministration_model->unit_categories_info($name,$page,$limit);
                
                $this->data['title']="Unit Categories";
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='superadministrator/unit_categories';
		$this->load->view('superadministrator/template',$this->data);
    }
    
    public function add_unit_category($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                        array('title'=>'View Unit Categories','link'=>'SuperAdministration/unit_categories'),
                                        array('title'=>'Add Unit Categories','link'=>'SuperAdministration/add_unit_category'),
                                       );
           
           $this->form_validation->set_rules('code','Category Code','required|xss_clean|numeric|exact_length[3]');
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('description','Description','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {


                $code=$this->input->post('code');
                $name=$this->input->post('name');
                $description=nl2br(trim($this->input->post('description')));

                

                       $itemCategory=array(
                           'code'=>$code,
                           'name'=>$name,
                           'description'=>$description
                       );


                       //code uniqueness
                       $code_check=$this->SuperAdministration_model->unit_category_code_check($code,$id);
                       if($code_check == null){


                           //do saving
                            $resp=$this->SuperAdministration_model->save_unit_category($itemCategory,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">unit category code exists already!</div>';
                       }
               
           }

           $this->data['title']="Add Item Category";
           $this->data['id']="$id";
           $this->data['itemCategory']=$this->SuperAdministration_model->item_categories($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='superadministrator/add_unit_category';
           $this->load->view('superadministrator/template',$this->data);
    }
    
    public function activate_deactivate_unit_category($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_unit_category($status,$id);
        redirect('SuperAdministration/unit_categories','refresh');
    }
    
    public function investigation_categories(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
        } 
        
        $this->data['side_menu']=array(
                                        array('title'=>'View Investigation Categories','link'=>'SuperAdministration/investigation_categories'),
                                        array('title'=>'View Investigation Services','link'=>'SuperAdministration/investigation_services'),
                                        array('title'=>'Add Investigation Category','link'=>'SuperAdministration/add_investigation_category'),
                                        array('title'=>'Configure Investigation Service','link'=>'SuperAdministration/configure_investigation_category_service'),
                                       );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
               
                if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                }
                
                $name =$key['name'];
                
                $config["base_url"] = base_url() . "index.php/SuperAdministration/investigation_categories/name_".$key['name']."/";
                $config["total_rows"] =$this->SuperAdministration_model->investigation_categories_info_count($name);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['investigation_categories'] = $this->SuperAdministration_model->investigation_categories_info($name,$page,$limit);
                
                $this->data['title']="Investigation Categories";
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='superadministrator/investigation_categories';
		$this->load->view('superadministrator/template',$this->data);
    }
    
    public function add_investigation_category($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

            $this->data['side_menu']=array(
                                        array('title'=>'View Investigation Categories','link'=>'SuperAdministration/investigation_categories'),
                                        array('title'=>'View Investigation Services','link'=>'SuperAdministration/investigation_services'),
                                        array('title'=>'Add Investigation Category','link'=>'SuperAdministration/add_investigation_category'),
                                        array('title'=>'Configure Investigation Service','link'=>'SuperAdministration/configure_investigation_category_service'),
                                       );
           
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('description','Description','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {


                $name=$this->input->post('name');
                $description=nl2br(trim($this->input->post('description')));

                

                $investigationCategory=array(
                    'name'=>$name,
                    'description'=>$description
                );

                //do saving
                 $resp=$this->SuperAdministration_model->save_investigation_category($investigationCategory,$id);

                   if($resp){

                       redirect(current_url());
                   }else{

                       $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                   }
           }

           $this->data['title']="Add Investigation Category";
           $this->data['id']="$id";
           $this->data['investigationCategory']=$this->SuperAdministration_model->investigation_categories($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='superadministrator/add_investigation_category';
           $this->load->view('superadministrator/template',$this->data);
    }
    
    public function activate_deactivate_investigation_category($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_investigation_category($status,$id);
        redirect('SuperAdministration/investigation_categories','refresh');
    }
    
    public function investigation_services(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
        } 
        
        $this->data['side_menu']=array(
                                        array('title'=>'View Investigation Categories','link'=>'SuperAdministration/investigation_categories'),
                                        array('title'=>'View Investigation Services','link'=>'SuperAdministration/investigation_services'),
                                        array('title'=>'Add Investigation Category','link'=>'SuperAdministration/add_investigation_category'),
                                        array('title'=>'Configure Investigation Service','link'=>'SuperAdministration/configure_investigation_category_service'),
                                       );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('category')) {
                    $key['category'] = $this->input->post('category');
                    $this->data['category']=$this->input->post('category');
                }
               
                if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['category'] = $exp[1];
                    $this->data['category']=$key['category'];
                    
                }
                
                $name =$key['name'];
                $category =$key['category'];
                
                $config["base_url"] = base_url() . "index.php/SuperAdministration/investigation_services/name_".$key['name']."_category_".$key['category']."/";
                $config["total_rows"] =$this->SuperAdministration_model->investigation_category_services_info_count($name.$category);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['investigation_services'] = $this->SuperAdministration_model->investigation_category_services_info($name,$category,$page,$limit);
                
                $this->data['title']="Investigation Services";
                $this->data['categories']=$this->SuperAdministration_model->investigation_categories();
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='superadministrator/investigation_services';
		$this->load->view('superadministrator/template',$this->data);
    }
    
    public function configure_investigation_category_service($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

            $this->data['side_menu']=array(
                                        array('title'=>'View Investigation Categories','link'=>'SuperAdministration/investigation_categories'),
                                        array('title'=>'View Investigation Services','link'=>'SuperAdministration/investigation_services'),
                                        array('title'=>'Add Investigation Category','link'=>'SuperAdministration/add_investigation_category'),
                                        array('title'=>'Configure Investigation Service','link'=>'SuperAdministration/configure_investigation_category_service'),
                                       );
           
           $this->form_validation->set_rules('department','Department','required|xss_clean');
           $this->form_validation->set_rules('subdepartment','Sub Department','required|xss_clean');
           
           if($this->input->post('configure')){
               $srv=$this->input->post('srv');
               $count=count($srv);
               
               if($count > 0){
                   $i=0;
                   foreach($srv as $key=>$value){
                        
                        $this->form_validation->set_rules('investigationcategory','Investigation Category','required|xss_clean');
                        $this->form_validation->set_rules('department','Department','required|xss_clean');
                        $this->form_validation->set_rules('subdepartment','Sub Department','required|xss_clean');
                        $this->form_validation->set_rules('srv['.$i.']','','xss_clean');
                        
                        if ($this->form_validation->run() == TRUE)
                            {
                                 $inv_service=array(
                                     'serviceid'=>$value,
                                     'investigationcategory'=>$this->input->post('investigationcategory')
                                 );
                                 
                                 //do saving
                                  $this->SuperAdministration_model->save_investigation_category_service($inv_service,$id);

                            }
                   }
               }
           }
           
           $this->data['title']="Configure Investigation Category Service(s)";
           $this->data['id']="$id";
           $this->data['categories']=$this->SuperAdministration_model->investigation_categories(null,null,1);
           $this->data['departments']=$this->Administration_model->active_departments();
           $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
           $this->data['services']=$this->Administration_model->active_services();
           $this->data['investigationCategorySrv']=$this->SuperAdministration_model->investigation_category_services($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='superadministrator/add_investigation_category_services';
           $this->load->view('superadministrator/template',$this->data);
    }
    
    public function activate_deactivate_investigation_category_service($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->activate_deactivate_investigation_category_service($status,$id);
        redirect('SuperAdministration/investigation_services','refresh');
    }
    
    public function remove_investigation_category_service($id){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->SuperAdministration_model->remove_investigation_category_service($id);
        redirect('SuperAdministration/investigation_services','refresh');
    }
    
    public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'SuperAdministration/edit_profile'),
                                           array('title'=>'Change Password','link'=>'SuperAdministration/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['content']='superadministrator/profile';
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['profile']=$this->SuperAdministration_model->profile_data();
		$this->load->view('superadministrator/template',$this->data);
        }
        
    public function edit_profile(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'SuperAdministration/edit_profile'),
                                           array('title'=>'Change Password','link'=>'SuperAdministration/change_password'),
                                        );
            
            $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
            $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
            $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
            $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
            $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if($this->form_validation->run() == true){

                 $firstname=$this->input->post('first_name');
                 $middlename=$this->input->post('middle_name');
                 $lastname=$this->input->post('last_name');
                 $username=$this->input->post('username');
                 $email=$this->input->post('email');
                 $mobile=$this->input->post('mobile');

                $user=array(
                            'FIRST_NAME'=>$firstname,
                            'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                            'LAST_NAME'=>$lastname,
                            'USERNAME'=>$username,
                            'EMAIL'=>$email<>null?$email:null,
                            'MSISDN'=>$mobile,
                        );

                $user_group=array(
                            'GROUP_ID'=>$this->session->userdata('group')
                        );

                $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                if($username_check){


                    //do registration
                     $resp=$this->SuperAdministration_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                       if($resp){

                           redirect('SuperAdministration/profile','refresh');
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                       }
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                }
            }

            $this->data['title']="Edit Profile";
            $this->data['content']='superadministrator/edit_profile';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->load->view('superadministrator/template',$this->data);
    }
        
    public function change_password(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'SuperAdministration/edit_profile'),
                                           array('title'=>'Change Password','link'=>'SuperAdministration/change_password'),
                                        );

            $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
            $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 

            if($this->form_validation->run() == true){

                $password=$this->input->post('password');
                $pchange=$this->SuperAdministration_model->change_password($password);

                if($pchange){
                           
                        $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">password change successfull!</div>';
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">password change failed!</div>';
                }
            }

            $this->data['title']="Change Password";
            $this->data['content']='superadministrator/change_password';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->load->view('superadministrator/template',$this->data);
    }
        
    function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('SuperAdministrator')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
    
    function username_uniqueness($username,$id){
            
        return $this->SuperAdministration_model->check_username($username,$id);
    }
    
    function cur_password($password){
            
            $pf=$this->SuperAdministration_model->profile_data();
            $hashed_old=$this->SuperAdministration_model->hash_db_password($password,$pf);
           
             if($pf->password <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
    }
    
}