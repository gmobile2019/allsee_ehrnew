<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Investigation extends CI_Controller {

    function __construct() {
        parent::__construct();
       	$this->load->model('Reception_model');	
       	$this->load->model('Investigation_model');	
       	$this->load->model('Clinical_model');	
		
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Home';
            $this->data['content']='investigation/dashboard';
            $this->load->view('investigation/template3',$this->data);
    }
    
    public function walk_ins(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'Out Patients','link'=>'Investigation/out_patients'),
                                      array('title'=>'In Patients','link'=>'Investigation/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Investigation/walk_ins'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Reception/walk_ins/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->walk_ins_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->walk_ins_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="Walk In Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='investigation/all_walkin_patients';
           $this->load->view('investigation/template',$this->data);
   }
   
    public function out_patients(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'Out Patients','link'=>'Investigation/out_patients'),
                                      array('title'=>'In Patients','link'=>'Investigation/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Investigation/walk_ins'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Reception/out_patients/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->out_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->out_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="Out Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='investigation/all_out_patients';
           $this->load->view('investigation/template',$this->data);
   }
   
    public function in_patients(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                      array('title'=>'Out Patients','link'=>'Investigation/out_patients'),
                                      array('title'=>'In Patients','link'=>'Investigation/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Investigation/walk_ins'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Reception/in_patients/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->in_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->in_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="In Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='investigation/all_in_patients';
           $this->load->view('investigation/template',$this->data);
   }
    
    public function patient_overview($patientid,$patientvisitid,$export){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
           $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Investigation/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Process Tests','link'=>'Investigation/process_patient_tests/'.$patientid.'/'.$patientvisitid),
                              );
            
            if($export){
                $data=$this->Investigation_model->patient_investigation_tests($patientid,$patientvisitid,2,$inv_grp[0]->investigationcategory);
                require_once 'reports/investigation/patient_investigation_results.php';
            }
            
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['supervisor']=$inv_grp[0]->supervisor == 0?FALSE:TRUE;
            $this->data['tests']=$this->Investigation_model->patient_investigation_tests($patientid,$patientvisitid,null,$inv_grp[0]->investigationcategory);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Patient Visit Overview<br/>Visit Id : '.$patientvisitid;
            $this->data['content']='investigation/patient_overview';
            $this->load->view('investigation/template2',$this->data);
    }
    
    public function process_patient_tests($patientid,$patientvisitid){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           
            $this->data['side_menu']=array(
                                  array('title'=>'Patient Overview','link'=>'Investigation/patient_overview/'.$patientid.'/'.$patientvisitid),
                                  array('title'=>'Process Tests','link'=>'Investigation/process_patient_tests/'.$patientid.'/'.$patientvisitid),
                              );
            
            
            if($this->input->post('process')){
                $confirmed=$this->input->post('investigate');
                
                foreach($confirmed as $key=>$value){
                    
                    $this->form_validation->set_rules('result_'.$value,'Result','xss_clean|required');
            
                    if($this->form_validation->run() == true){
                        
                        $details=explode('_',$value);
                        
                        $orderid=$details[0];
                        $serviceid=$details[1];
                        $testid=$details[2];
                        $result=$this->input->post('result_'.$value);
                        
                        
                        $array=array(
                            'patientid'=>$patientid,
                            'patientvisitid'=>$patientvisitid,
                            'serviceid'=>$serviceid,
                            'orderid'=>$orderid,
                            'testid'=>$testid,
                            'result'=>$result,
                            'status'=>1,
                        );
                       
                        $insrt=$this->Investigation_model->process_investigation_result($array);
                        if(!$insrt){
                            $error=TRUE;
                        }
                    }
                }
                
                if(!$error){

                    redirect('Investigation/patient_overview/'.$patientid.'/'.$patientvisitid,'refresh');
                }

                $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
            }
            
        
            $this->data['title']='Process Tests';
            $patient=$this->Reception_model->patient($patientid);
            $this->data['patient']=$this->Reception_model->patients($patient->id);
            $this->data['patientid']=$patientid;
            $this->data['patientvisitid']=$patientvisitid;
            $this->data['services']=$this->Investigation_model->patient_investigation_services($patientid,$patientvisitid,'pending',$inv_grp[0]->investigationcategory);
            $this->data['patient_diagnosis']=$this->Clinical_model->patient_diagnosis($patientid,$patientvisitid);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['diagnosis']='investigation/patient_diagnosis';
            $this->data['content']='investigation/process_patient_investigation_test';
            $this->load->view('investigation/template2',$this->data);
    }
    
    public function reject_approve_patient_test_result($patientid,$patientvisitid,$id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           
            if($inv_grp[0]->supervisor == 1){
                
                $this->Investigation_model->reject_approve_patient_test_result($status,$id);
            }
        
        redirect('Investigation/patient_overview/'.$patientid.'/'.$patientvisitid,'refresh');
    }
    
    public function services(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           
            $this->data['side_menu']=array(
                                      array('title'=>'Services','link'=>'Investigation/services'),
                                      array('title'=>'Tests','link'=>'Investigation/service_tests'),
                                      array('title'=>'Test Result Sets','link'=>'Investigation/test_results'),
                                      array('title'=>'Test Normal Ranges','link'=>'Investigation/test_normal_ranges'),
                                      
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
            }

           $name =$key['name'];
           $category=$inv_grp[0]->investigationcategory;
           
           $config["base_url"] = base_url() . "index.php/Investigation/services/name_".$key['name']."/";
           $config["total_rows"] =$this->Investigation_model->services_info_count($name,$category);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['services'] = $this->Investigation_model->services_info($name,$category,$page,$limit);

           $this->data['title']="Services";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='investigation/services';
           $this->load->view('investigation/template',$this->data);
   }
   
    public function service_tests(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           
            $this->data['side_menu']=array(
                                      array('title'=>'Services','link'=>'Investigation/services'),
                                      array('title'=>'Tests','link'=>'Investigation/service_tests'),
                                      array('title'=>'Test Result Sets','link'=>'Investigation/test_results'),
                                      array('title'=>'Test Normal Ranges','link'=>'Investigation/test_normal_ranges'),
                                      
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('service')) {
               $key['service'] = $this->input->post('service');
               $this->data['service']=$this->input->post('service');
           }
           
           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['service'] = $exp[3];
               $this->data['service']=$key['service'];
               
            }

           $name =$key['name'];
           $service =$key['service'];
           $category=$inv_grp[0]->investigationcategory;
           
           $config["base_url"] = base_url() . "index.php/Investigation/service_tests/name_".$key['name']."_service_".$key['service']."/";
           $config["total_rows"] =$this->Investigation_model->tests_info_count($name,$service,$category);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['service_tests'] = $this->Investigation_model->tests_info($name,$service,$category,$page,$limit);

           $this->data['title']="Tests";
           $this->data['supervisor']=$inv_grp[0]->supervisor == 0?FALSE:TRUE;
           $this->data['services']=$this->Investigation_model->services(null,$category);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='investigation/service_tests';
           $this->load->view('investigation/template',$this->data);
   }
   
    public function add_investigation_service_test($id=null){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           
            if($inv_grp[0]->supervisor == 0){
                
                redirect('Investigation/service_tests/','refresh');
            }
            
            $this->data['side_menu']=array(
                                      array('title'=>'Services','link'=>'Investigation/services'),
                                      array('title'=>'Tests','link'=>'Investigation/service_tests'),
                                      array('title'=>'Test Result Sets','link'=>'Investigation/test_results'),
                                      array('title'=>'Test Normal Ranges','link'=>'Investigation/test_normal_ranges'),
                                      
                                  );
            
            $this->form_validation->set_rules('service','Service','xss_clean|required');
            $this->form_validation->set_rules('test','Test','xss_clean|required');
            $this->form_validation->set_rules('resultcategory','Result Category','xss_clean|required');
            $this->form_validation->set_rules('unit','Unit','xss_clean');
            
            if($this->form_validation->run() == true){
                
                $service=$this->input->post('service');
                $test=$this->input->post('test');
                $resultcategory=$this->input->post('resultcategory');
                $unit=$this->input->post('unit');

                $array=array(
                    'serviceid'=>$service,
                    'name'=>$test,
                    'resultcategory'=>$resultcategory,
                    'unit'=>$unit<> null?$unit:null,
                );

                $insrt=$this->Investigation_model->add_investigation_service_test($array,$id);

                if($insrt){
                    
                    redirect('Investigation/service_tests/','refresh');
                }
                
                $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
            }
        
            $this->data['title']='Add/Edit Test';
            $this->data['id']=$id;
            $this->data['test']=$this->Investigation_model->tests($id);
            $this->data['services']=$this->Investigation_model->services(null,$inv_grp[0]->investigationcategory);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='investigation/add_investigation_service_test';
            $this->load->view('investigation/template',$this->data);
    }
    
    public function activate_deactivate_investigation_service_test($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           
            if($inv_grp[0]->supervisor == 1){
                
                $this->Investigation_model->activate_deactivate_investigation_service_test($status,$id);
            }
        
        redirect('Investigation/service_tests','refresh');
    }
    
    public function test_results(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           
            $this->data['side_menu']=array(
                                      array('title'=>'Services','link'=>'Investigation/services'),
                                      array('title'=>'Tests','link'=>'Investigation/service_tests'),
                                      array('title'=>'Test Result Sets','link'=>'Investigation/test_results'),
                                      array('title'=>'Test Normal Ranges','link'=>'Investigation/test_normal_ranges'),
                                      
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               
            }

           $name =$key['name'];
           $category=$inv_grp[0]->investigationcategory;
           
           $config["base_url"] = base_url() . "index.php/Investigation/test_results/name_".$key['name']."/";
           $config["total_rows"] =$this->Investigation_model->test_results_info_count($name,$category);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['test_results'] = $this->Investigation_model->test_results_info($name,$category,$page,$limit);

           $this->data['title']="Test Result Sets";
           $this->data['supervisor']=$inv_grp[0]->supervisor == 0?FALSE:TRUE;
           $this->data['services']=$this->Investigation_model->services(null,$category);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='investigation/test_results';
           $this->load->view('investigation/template',$this->data);
   }
   
    public function add_test_result($id=null){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           
            if($inv_grp[0]->supervisor == 0){
                
                redirect('Investigation/service_tests/','refresh');
            }
            
           $this->data['side_menu']=array(
                                      array('title'=>'Services','link'=>'Investigation/services'),
                                      array('title'=>'Tests','link'=>'Investigation/service_tests'),
                                      array('title'=>'Test Result Sets','link'=>'Investigation/test_results'),
                                      array('title'=>'Test Normal Ranges','link'=>'Investigation/test_normal_ranges'),
                                      
                                  );
            
            $this->form_validation->set_rules('test','Test','xss_clean|required');
            $this->form_validation->set_rules('name','Name','xss_clean|required');
            
            if($this->form_validation->run() == true){
                
                $test=$this->input->post('test');
                $name=$this->input->post('name');

                $array=array(
                    'testid'=>$test,
                    'name'=>$name,
                );

                $insrt=$this->Investigation_model->add_test_result($array,$id);

                if($insrt){
                    
                    redirect('Investigation/test_results/','refresh');
                }
                
                $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
            }
        
            $this->data['title']='Add/Edit Test Result';
            $this->data['id']=$id;
            $this->data['tests']=$this->Investigation_model->tests(null,null,null,null,1,'Fixed');
            $this->data['testresult']=$this->Investigation_model->test_results($id,null,$inv_grp[0]->investigationcategory);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='investigation/add_test_result';
            $this->load->view('investigation/template',$this->data);
    }
    
    public function activate_deactivate_test_result($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           
            if($inv_grp[0]->supervisor == 1){
                
                $this->Investigation_model->activate_deactivate_test_result($status,$id);
            }
        
        redirect('Investigation/test_results','refresh');
    }
    
    public function test_normal_ranges(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           
            $this->data['side_menu']=array(
                                      array('title'=>'Services','link'=>'Investigation/services'),
                                      array('title'=>'Tests','link'=>'Investigation/service_tests'),
                                      array('title'=>'Test Result Sets','link'=>'Investigation/test_results'),
                                      array('title'=>'Test Normal Ranges','link'=>'Investigation/test_normal_ranges'),
                                      
                                  );

           if ($this->input->post('test')) {
               $key['test'] = $this->input->post('test');
               $this->data['test']=$this->input->post('test');
           }
           
           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['test'] = $exp[1];
               $this->data['test']=$key['test'];
               
               
            }

           $test =$key['test'];
           $category=$inv_grp[0]->investigationcategory;
           
           $config["base_url"] = base_url() . "index.php/Investigation/test_normal_ranges/name_".$key['name']."/";
           $config["total_rows"] =$this->Investigation_model->test_normal_ranges_info_count($test,$category);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['test_ranges'] = $this->Investigation_model->test_normal_ranges_info($test,$category,$page,$limit);

           $this->data['title']="Test Normal Ranges";
           $this->data['supervisor']=$inv_grp[0]->supervisor == 0?FALSE:TRUE;
           $this->data['tests']=$this->Investigation_model->tests(null,null,null,null,1);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='investigation/test_normal_ranges';
           $this->load->view('investigation/template',$this->data);
   }
   
    public function add_test_normal_range($id=null){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           
            if($inv_grp[0]->supervisor == 0){
                
                redirect('Investigation/service_tests/','refresh');
            }
            
           $this->data['side_menu']=array(
                                      array('title'=>'Services','link'=>'Investigation/services'),
                                      array('title'=>'Tests','link'=>'Investigation/service_tests'),
                                      array('title'=>'Test Result Sets','link'=>'Investigation/test_results'),
                                      array('title'=>'Test Normal Ranges','link'=>'Investigation/test_normal_ranges'),
                                      
                                  );
            
            $this->form_validation->set_rules('test','Test','xss_clean|required');
            $this->form_validation->set_rules('name','Name','xss_clean');
            $this->form_validation->set_rules('range','Range','xss_clean|required');
            
            if($this->form_validation->run() == true){
                
                $test=$this->input->post('test');
                $name=$this->input->post('name');
                $range=$this->input->post('range');

                $array=array(
                    'testid'=>$test,
                    'name'=>$name,
                    'normalrange'=>$range,
                );

                $insrt=$this->Investigation_model->add_test_normal_range($array,$id);

                if($insrt){
                    
                    redirect('Investigation/test_normal_ranges/','refresh');
                }
                
                $this->data['message'] = '<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed!</div>';
            }
        
            $this->data['title']='Add/Edit Test Normal Range';
            $this->data['id']=$id;
            $this->data['tests']=$this->Investigation_model->tests(null,null,null,null,1);
            $this->data['testrange']=$this->Investigation_model->test_normal_ranges($id,null,$inv_grp[0]->investigationcategory);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='investigation/add_test_normal_range';
            $this->load->view('investigation/template',$this->data);
    }
    
    public function activate_deactivate_test_normal_range($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           
            if($inv_grp[0]->supervisor == 1){
                
                $this->Investigation_model->activate_deactivate_test_normal_range($status,$id);
            }
        
        redirect('Investigation/test_normal_ranges','refresh');
    }
/********************************************************************reports*****************************************************************************/
    
    public function patient_medical_results($patientid=null,$from=null,$to=null,$export=null){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
           $this->data['inv_grp']=$inv_grp;
            $this->data['side_menu']=array(
                                      array('title'=>'Investigation Results','link'=>'Investigation/patient_medical_results'),
                                      array('title'=>'Tests Report','link'=>'Investigation/tests_report'),
                                      array('title'=>'Patients Report','link'=>'Investigation/patients_report'),
                                   );
            
            if($patientid <> null){
                
                $data=$this->Reception_model->patient_visits($patientid,null,$from,$to);
               
                if($export == TRUE){
                   
                    require_once 'reports/investigation/investigation_results.php';
                }
                
            }
            
            $this->form_validation->set_rules('patientid','Patient Id','xss_clean|required');
            $this->form_validation->set_rules('start','Start Date','xss_clean');
            $this->form_validation->set_rules('end','End Date','xss_clean');

            if($this->form_validation->run() == true){
               
                $patientid=$this->input->post('patientid');
                $from=$this->input->post('start');
                $to=$this->input->post('end');
                $data=$this->Reception_model->patient_visits($patientid,null,$from,$to);
                
            }
           
          
           $this->data['title']="Patient Investigation Results";
           $this->data['patientid'] =$patientid;
           $this->data['start']=$from;
           $this->data['end']=$to;
           $this->data['visits']=$data;
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
          $this->data['content']='investigation/investigation_results';
           $this->load->view('investigation/template',$this->data);
   }
   
    public function patients_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $inv_grp=$this->Investigation_model->examiner_users($this->session->userdata('user_id'));
            $this->data['inv_grp']=$inv_grp;
            $this->data['side_menu']=array(
                                      array('title'=>'Investigation Results','link'=>'Investigation/patient_medical_results'),
                                      array('title'=>'Tests Report','link'=>'Investigation/tests_report'),
                                      array('title'=>'Patients Report','link'=>'Investigation/patients_report'),
                                   );

           
           
            if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
            }
            
            if ($this->input->post('srv')) {
               $key['srv'] = $this->input->post('srv');
               $this->data['srv']=$this->input->post('srv');
            }
            
            if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
            }
            
            if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
            }
            
           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['pid'] = $exp[1];
               $this->data['pid']=$key['pid'];
               
               $key['srv'] = $exp[3];
               $this->data['srv']=$key['srv'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
               
                $docType = $exp[9];
            }

           $pid =$key['pid'];
           $srv =$key['srv'];
           $start =$key['start'];
           $end =$key['end'];
           
           if($docType == 1){
                
                $data=$this->Investigation_model->patients_tests_report(null,$pid,$srv,$start,$end);
               
                require_once 'reports/investigation/patients_report.php';
            }
            
           $category=$inv_grp[0]->investigationcategory;
           
           $config["base_url"] = base_url() . "index.php/Investigation/patients_report/pid_".$key['pid']."_srv_".$key['srv']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Investigation_model->patients_tests_report_info_count($pid,$srv,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['report'] = $this->Investigation_model->patients_tests_report_info($pid,$srv,$start,$end,$page,$limit);

           $this->data['title']="Patients Investigation Report";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='investigation/patients_report';
           $this->load->view('investigation/template',$this->data);
   }
   
    public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Investigation/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Investigation/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['profile']=$this->SuperAdministration_model->profile_data();
                $this->data['content']='investigation/profile';
		$this->load->view('investigation/template',$this->data);
        }
        
    public function edit_profile(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Investigation/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Investigation/change_password'),
                                        );
            
            $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
            $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
            $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
            $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
            $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if($this->form_validation->run() == true){

                 $firstname=$this->input->post('first_name');
                 $middlename=$this->input->post('middle_name');
                 $lastname=$this->input->post('last_name');
                 $username=$this->input->post('username');
                 $email=$this->input->post('email');
                 $mobile=$this->input->post('mobile');

                $user=array(
                            'FIRST_NAME'=>$firstname,
                            'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                            'LAST_NAME'=>$lastname,
                            'USERNAME'=>$username,
                            'EMAIL'=>$email<>null?$email:null,
                            'MSISDN'=>$mobile,
                        );

                $user_group=array(
                            'GROUP_ID'=>$this->session->userdata('group')
                        );

                $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                if($username_check){


                    //do registration
                     $resp=$this->SuperAdministration_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                       if($resp){

                           redirect('Clinical/profile','refresh');
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                       }
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                }
            }

            $this->data['title']="Edit Profile";
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['content']='investigation/edit_profile';
            $this->load->view('investigation/template',$this->data);
    }
        
    public function change_password(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Clinical/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Clinical/change_password'),
                                        );

            $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
            $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 

            if($this->form_validation->run() == true){

                $password=$this->input->post('password');
                $pchange=$this->SuperAdministration_model->change_password($password);

                if($pchange){
                           
                        $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">password change successfull!</div>';
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">password change failed!</div>';
                }
            }

            $this->data['title']="Change Password";
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='investigation/change_password';
            $this->load->view('investigation/template',$this->data);
    }
    
    function cur_password($password){
            
            $pf=$this->SuperAdministration_model->profile_data();
            $hashed_old=$this->SuperAdministration_model->hash_db_password($password,$pf);
           
             if($pf->password <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
    }
    
    function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('Examiner')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
    
    function username_uniqueness($username,$id){
            
        return $this->SuperAdministration_model->check_username($username,$id);
    }
    
    function appointment_date($date){
        
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
    }
    
    function appointment_time($time) {
        
            if (is_object(DateTime::createFromFormat('H:i', $time))) {
                return TRUE;
            } else {
                $this->form_validation->set_message('appointment_time', "The %s must contain hours:minutes in 24 hours");
                return FALSE;
            }
    }
   
}