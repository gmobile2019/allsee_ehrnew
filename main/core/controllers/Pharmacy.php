<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pharmacy extends CI_Controller {

    function __construct() {
        parent::__construct();
       
	$this->load->model('Inventory_model');	
	$this->load->model('Reception_model');	
	$this->load->model('Pharmacy_model');	
        
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Home';
            $this->data['content']='pharmacy/dashboard';
            $this->load->view('pharmacy/template3',$this->data);
    }
    
    public function all_patients(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'All Patients','link'=>'Pharmacy/all_patients'),
                                      array('title'=>'Out Patients','link'=>'Pharmacy/out_patients'),
                                      array('title'=>'In Patients','link'=>'Pharmacy/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Pharmacy/walk_ins'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('gender')) {
               $key['gender'] = $this->input->post('gender');
               $this->data['gender']=$this->input->post('gender');
           }
           
           if ($this->input->post('marital')) {
               $key['marital'] = $this->input->post('marital');
               $this->data['marital']=$this->input->post('marital');
           }
           
           if ($this->input->post('phone')) {
               $key['phone'] = $this->input->post('phone');
               $this->data['phone']=$this->input->post('phone');
           }

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['gender'] = $exp[3];
               $this->data['gender']=$key['gender'];
               
               $key['marital'] = $exp[5];
               $this->data['marital']=$key['marital'];
               
               $key['phone'] = $exp[7];
               $this->data['phone']=$key['phone'];
               
               $docType = $exp[9];
               $this->data['docType']=$docType;
           }

           $name =$key['name'];
           $gender =$key['gender'];
           $marital =$key['marital'];
           $phone =$key['phone'];
           
           if($docType == 1){
            $data=$this->Reception_model->all_patients($name,$gender,$marital,$phone);
            require_once 'reports/reception/all_patients_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->all_patients($name,$gender,$marital,$phone);
                 require_once 'reports/reception/all_patients_excel.php';
            }
        
           $config["base_url"] = base_url() . "index.php/Pharmacy/all_patients/name_".$key['name']."_gender_".$key['gender']."_marital_".$key['marital']."_phone_".$key['phone']."/";
           $config["total_rows"] =$this->Reception_model->patient_info_count($name,$gender,$marital,$phone);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->patient_info($name,$gender,$marital,$phone,$page,$limit);

           $this->data['title']="All Patients";
           $this->data['genders']=$this->Reception_model->genders();
           $this->data['maritals']=$this->Reception_model->maritals();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='pharmacy/all_patients';
           $this->load->view('pharmacy/template',$this->data);
   }
   
    public function walk_ins(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'All Patients','link'=>'Pharmacy/all_patients'),
                                      array('title'=>'Out Patients','link'=>'Pharmacy/out_patients'),
                                      array('title'=>'In Patients','link'=>'Pharmacy/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Pharmacy/walk_ins'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Reception/walk_ins/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->walk_ins_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->walk_ins_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="Walk In Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='pharmacy/all_walkin_patients';
           $this->load->view('pharmacy/template',$this->data);
   }
   
    public function out_patients(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'All Patients','link'=>'Pharmacy/all_patients'),
                                      array('title'=>'Out Patients','link'=>'Pharmacy/out_patients'),
                                      array('title'=>'In Patients','link'=>'Pharmacy/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Pharmacy/walk_ins'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Pharmacy/out_patients/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->out_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->out_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="Out Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='pharmacy/all_out_patients';
           $this->load->view('pharmacy/template',$this->data);
   }
    
    public function in_patients(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'All Patients','link'=>'Pharmacy/all_patients'),
                                      array('title'=>'Out Patients','link'=>'Pharmacy/out_patients'),
                                      array('title'=>'In Patients','link'=>'Pharmacy/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Pharmacy/walk_ins'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Pharmacy/in_patients/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->in_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->in_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="In Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='pharmacy/all_in_patients';
           $this->load->view('pharmacy/template',$this->data);
   }
   
    public function create_dispense_order($patientid,$patientvisitid){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit
        $patient=$this->Reception_model->patient($patientid);
        $patient=$this->Reception_model->patients($patient->id);
        
        if($this->input->post('add_dispense_order')){
            
             $this->form_validation->set_rules('item_category','Item Category','required|xss_clean');
             $this->form_validation->set_rules('item','Item','required|xss_clean');
             
             if($this->input->post('item_category') == $this->config->item('drug_code')){
                $this->form_validation->set_rules('dosage','Dosage','required|xss_clean');
                $this->form_validation->set_rules('frequency','Frequency','required|xss_clean');
                $this->form_validation->set_rules('no_days','No Days','required|xss_clean');
             }else{
                 
                 $this->form_validation->set_rules('quantity','Quantity','required|xss_clean');
             }
            
             if ($this->form_validation->run() == TRUE)
                {
                 $itemid=$this->input->post('item');
                 
                 $dispense_array=array(
                     'patientid'=>$patientid,
                     'patientvisitid'=>$patientvisitid,
                     'itemid'=>$itemid,
                     'createdby'=>$this->session->userdata('user_id')
                 );
                 
                 if($this->input->post('item_category') == $this->config->item('drug_code')){
                    $dosage=$this->input->post('dosage');
                    $frequency=$this->input->post('frequency');
                    $no_days=$this->input->post('no_days');
                    $quantity=$dosage*$frequency*$no_days;
                    
                    $dispense_array['dosageid']=$dosage;
                    $dispense_array['frequencyid']=$frequency;
                    $dispense_array['days']=$no_days;
                 }else{

                     $quantity=$this->input->post('quantity');
                 }
                 
                 $dispense_array['quantity']=$quantity;
                 
                    $check=$this->Pharmacy_model->check_pending_order($patientid,$patientvisitid,$itemid);
                    
                    if(!$check){
                        $sv=$this->Pharmacy_model->save_dispense_order($dispense_array);
                    
                        if($sv){

                            redirect(current_url());
                        }
                        
                        $this->data['disp_msg']='<div class="alert alert-danger " role="alert" style="text-align:center;">posting failed!</div>';
                    }else{
                        $this->data['disp_msg']='<div class="alert alert-danger " role="alert" style="text-align:center;">clear pending item order!</div>';
                    }
                    
                }
             }
             
             if($this->input->post('process_payment')){
                 
                 $confirm=$this->input->post('confirm');
                
                if(count($confirm) > 0){
                    
                    $error=TRUE;
                    $array=array();
                    $i=0;
                    $blc=array();
                    foreach($confirm as $value){
                        
                        $itm=$this->Inventory_model->items($this->input->post('itm'.$value));
                        $this->form_validation->set_rules('confirm['.$i++.']','Service','xss_clean|required');
                        $this->form_validation->set_rules('itm'.$value,'Item','xss_clean|required');
                        $this->form_validation->set_rules('qty'.$value,'Quantity','required|xss_clean|numeric');
                        
                        if($itm[0]->consumptioncategory == 1){
                                $this->form_validation->set_rules('spnsr'.$value, 'Sponsor', 'required|xss_clean');
                                $this->form_validation->set_rules('dpt'.$value,'Department','xss_clean|required');
                                $this->form_validation->set_rules('subdpt'.$value, 'Sub Department', 'required|xss_clean');
                                $this->form_validation->set_rules('pmode'.$value,'Payment Mode','required|xss_clean');
                                $this->form_validation->set_rules('cost'.$value,'Cost','xss_clean|required');
                                
                                if($this->input->post('pmode'.$value) == $this->config->item('package_bill_pmode_code')){
                                    $this->form_validation->set_rules('package','Package','xss_clean|required');
                                }
                        }
                        
                        if($this->form_validation->run() == TRUE){
                            $package=$this->input->post('package');
                            $package=explode("_", $package);
                            //verify the patients balance for prepaid billing
                            if($this->input->post('pmode'.$value) == $this->config->item('prepaid_bill_pmode_code') && $itm[0]->consumptioncategory == 1){
                                
                                $balance=$this->Reception_model->patient_prepaid_status(null,$patientid,1);
                               
                                if($balance->balance < $this->input->post('cost'.$value)){
                                    $blc[]=$value;
                                    continue;
                                }
                            }
                            
                            //verify the patients balance for package billing
                            if($this->input->post('pmode'.$value) == $this->config->item('package_bill_pmode_code')){

                                $pckg=$this->Reception_model->patient_package_item_status($this->input->post('dpt'.$value),$this->input->post('subdpt'.$value),$this->input->post('itm'.$value),$package[1]);
                                $pckg_conf=$this->Administration_model->package_service_item($package[0],$this->input->post('itm'.$value),$this->input->post('subdpt'.$value),$this->input->post('dpt'.$value),1);


                                if(($pckg->quantity+$this->input->post('qty'.$value)) >= $pckg_conf[0]->quantity){
                                    $pck[]=$value;
                                    continue;
                                }
                            }
                            
                            //destocking process
                            $available_stock=$this->Inventory_model->inventory_stock($unit[0]->inventoryunit,$this->input->post('itm'.$value),$this->input->post('qty'.$value));
                           
                            if($available_stock == null){
                                continue;
                                
                            }
                            
                            $destocking=array();
                            foreach($available_stock as $ky=>$val){

                                $destocking[]=array(
                                    'itemid'=>$this->input->post('itm'.$value),
                                    'batch'=>$val['batch'],
                                    'storebatch'=>$val['storebatch'],
                                    'expiredate'=>$val['expiredate'],
                                    'supplierid'=>$val['supplierid'],
                                    'quantity'=>-$val['quantity'],
                                    'totalvalue'=>$val['totalvalue'],
                                    'unitprice'=>$val['unitprice'],
                                    'inventoryunit'=>$unit[0]->inventoryunit,
                                    'status'=>'DISPENSE',
                                    'lastupdate'=>Date('Y-m-d H:i:s'),
                                    'userid'=>$this->session->userdata('user_id'),
                                );

                            }
                                
                            $this->Pharmacy_model->process_destocking($destocking);
                           
                             $dispense_data=array(
                                'dispenseinventoryunit'=>$unit[0]->inventoryunit,
                                'modifiedby'=>$this->session->userdata('user_id'),
                                'modifiedon'=>date('Y-m-d H:i:s'),
                                'status'=>1,
                            );
                            
                             //update dispense order
                            $this->Pharmacy_model->update_dispense_order($dispense_data,$value);
                            
                            
                            //if an item is not for selling
                            if($itm[0]->consumptioncategory <> 1){
                                continue;
                            }
                            
                            $paymentstatus=1;
                            if(in_array($this->input->post('pmode'.$value),$this->config->item('completed_prepaid_payment_modes'))){

                                $paymentstatus=4;
                            }
                            
                            if(!in_array($this->input->post('pmode'.$value),$this->config->item('completed_payment_modes'))){

                                $paymentstatus=2;
                            }
                            
                            $bill[]=array(
                                        'patientid'=>$this->input->post('patientid'.$value),
                                        'patientvisitid'=>$this->input->post('patientvisitid'.$value),
                                        'departmentid'=>$this->input->post('dpt'.$value),
                                        'subdepartmentid'=>$this->input->post('subdpt'.$value),
                                        'service_item_id'=>$this->input->post('itm'.$value),
                                        'sponsorid'=>$this->input->post('spnsr'.$value),
                                        'paymentmodeid'=>$this->input->post('pmode'.$value),
                                        'amount'=>$this->input->post('cost'.$value),
                                        'paymentstatus'=>$paymentstatus,
                                        'orderid'=>$value,
                                        'createdby'=>$this->session->userdata('user_id'),
                                     );
                        }
                    }
                    
                    if($bill <> null){
                        
                       
                        $txngenerate=FALSE;
                        foreach($bill as $value){
                            
                            //check if payment mode is not postpaid billing so as to generate txn id
                            if($value['paymentmodeid'] <> $this->config->item('postpaid_bill_pmode_code') && $txngenerate==FALSE){
                                    $txnid=$this->Reception_model->get_transaction_id();
                                    $txngenerate=TRUE;
                            }
                            
                            //check if payment mode is postpaid billing so as to generate txn id
                            if($value['paymentmodeid'] == $this->config->item('postpaid_bill_pmode_code') && $txngenerate==FALSE){
                                   $pst_txnid=$this->Reception_model->get_postpaid_transaction_id();
                                    $txngenerate=TRUE;
                            }
                            
                            //check if payment mode is not postpaid billing so as to assign a txn id value
                            if($value['paymentmodeid'] <> $this->config->item('postpaid_bill_pmode_code')){
                                    $value['transactionid']=$txnid;
                                    $dispense_order=array('transactionid'=>$txnid);
                            }
                            
                            //check if payment mode is postpaid billing so as to assign a txn id value
                            if($value['paymentmodeid'] == $this->config->item('postpaid_bill_pmode_code')){
                                    $value['postpaid_transactionid']=$pst_txnid;
                                    $dispense_order=array('postpaid_transactionid'=>$pst_txnid);
                            }
                            
                            $resp=$this->Reception_model->process_bill($value);
                            
                            if($resp){
                                $error=FALSE;
                                
                                //check if payment mode is not postpaid billing so as to assign a txn id value
                                if($value['paymentmodeid'] == $this->config->item('postpaid_bill_pmode_code')){
                                    
                                        $postpaid=array(
                                                        'transactionid'=>$pst_txnid,
                                                        'patientid'=>$value['patientid'],
                                                        'patientvisitid'=>$value['patientvisitid'],
                                                        'amount'=>$value['amount'],
                                                        'departmentid'=>$value['departmentid'],
                                                        'subdepartmentid'=>$value['subdepartmentid'],
                                                        'service_item_id'=>$value['service_item_id'],
                                                        'createdby'=>$this->session->userdata('user_id'),
                                                    );
                                        
                                        $this->Reception_model->save_postpaid_bill($postpaid);
                                }
                                
                                //check if payment mode is prepaid billing so as to further process the debit
                                if($value['paymentmodeid'] == $this->config->item('prepaid_bill_pmode_code')){
                                    
                                    $prepaid=array(
                                        'patientid'=>$value['patientid'],
                                        'patientvisitid'=>$value['patientvisitid'],
                                        'subscriptionid'=>$balance->subscriptionid,
                                        'amount'=>-$value['amount'],
                                        'status'=>'DEBIT',
                                        'transactionid'=>$value['transactionid'],
                                        'departmentid'=>$value['departmentid'],
                                        'subdepartmentid'=>$value['subdepartmentid'],
                                        'service_item_id'=>$value['service_item_id'],
                                        'createdby'=>$this->session->userdata('user_id'),
                                    );
                                    
                                    $this->Reception_model->credit_debit_prepaid_account($prepaid);
                                }
                                
                                //check if payment mode is package billing so as to further process the debit
                                    if($value['paymentmodeid'] == $this->config->item('package_bill_pmode_code')){
                                        
                                        $qty=$this->Pharmacy_model->item_orders($value['patientid'],$value['patientvisitid'],$value['orderid'],$value['service_item_id']);
                                        $pckg_arr=array(
                                            'patientid'=>$value['patientid'],
                                            'patientvisitid'=>$value['patientvisitid'],
                                            'packageid'=>$package[0],
                                            'subscriptionid'=>$package[1],
                                            'amount'=>$value['amount'],
                                            'transactionid'=>$value['transactionid'],
                                            'departmentid'=>$value['departmentid'],
                                            'subdepartmentid'=>$value['subdepartmentid'],
                                            'service_item_id'=>$value['service_item_id'],
                                            'quantity'=>$qty[0]->quantity,
                                            'transactiontype'=>'DEBIT',
                                            'createdby'=>$this->session->userdata('user_id'),
                                        );

                                        $this->Reception_model->credit_debit_package_account($pckg_arr);
                                    }
                                
                                //check if sponsor is not private so as to save cerdit bill details
                                if($value['sponsorid'] <> $this->config->item('instant_cash_code')){
                                    
                                    $credit=array(
                                        'patientid'=>$value['patientid'],
                                        'patientvisitid'=>$value['patientvisitid'],
                                        'sponsorid'=>$value['sponsorid'],
                                        'amount'=>$value['amount'],
                                        'reftransactionid'=>$value['transactionid'],
                                        'departmentid'=>$value['departmentid'],
                                        'subdepartmentid'=>$value['subdepartmentid'],
                                        'service_item_id'=>$value['service_item_id'],
                                        'createdby'=>$this->session->userdata('user_id'),
                                    );
                                    
                                    $this->Reception_model->save_credit_bill($credit);
                                }
                                
                                //update service order status
                                $this->Pharmacy_model->update_dispense_order($dispense_order,$value['orderid']);
                            }
                        }
                        
                        if(!$error){
                            $bill=$this->Reception_model->bill_transaction($txnid);
                            $pvisit=$this->Reception_model->patient_visit_details($patientvisitid);
                            
                            if($bill <> null){
                                
                                if($txnid <> null){
                                    redirect('Pharmacy/dispense_order_receipt/'.$txnid.'/'.$patientid,'refresh');
                                }
                                
                                redirect('Pharmacy/postpaid_dispense_order_receipt/'.$pst_txnid.'/'.$patientid,'refresh');
                            }
                            
                            $this->data['message2']='<div class="alert alert-success " role="alert" style="text-align:center;">payment processed</div>';
                        }
                    }
                }else{
                    
                    $this->data['message2']='<div class="alert alert-danger " role="alert" style="text-align:center;">confirm atleast one item!</div>';
                }
             }
        
        $this->data['blc']=$blc;
        $this->data['pck']=$pck;
        $this->data['title']="Dispensing Order";
        $this->data['unit']=$unit[0]->inventoryunit;
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['patientvisitid']=$patientvisitid;
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['items'] = $this->Inventory_model->active_items();
        $this->data['drug_frequencies'] = $this->Pharmacy_model->active_drug_frequencies();
        $this->data['drug_dosages'] = $this->Pharmacy_model->active_drug_dosages();
        $this->data['items'] = $this->Inventory_model->active_items();
        $this->data['pending_orders'] = $this->Pharmacy_model->pending_dispense_orders($patientid,$patientvisitid);
        $this->data['packages']=$this->Reception_model->patient_package_status(null,$patientid,null,1);
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['profile']=$this->SuperAdministration_model->profile_data();
        $this->data['dispense_confirm']='pharmacy/dispense_confirm';
        $this->data['content']='pharmacy/dispense_order';
        $this->load->view('pharmacy/template2',$this->data);
    }
    
    public function dispense_order_receipt($transactionid,$patientid){
            if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
            
            $patient=$this->Reception_model->patient($patientid);
            $patient=$this->Reception_model->patients($patient->id);
            
            $this->data['title']="Dispense Receipt";
            $this->data['patient']=$patient;
            $this->data['bill_data']=$this->Reception_model->bill_transaction($transactionid);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='pharmacy/dispense_order_receipt';
            $this->load->view('pharmacy/template2',$this->data);
    }
    
    function download_dispense_receipt($transactionid){
        
        $bill_data=$this->Reception_model->bill_transaction($transactionid);
        $institution=$this->SuperAdministration_model->institution_information();
        require_once 'receipts/dispense_receipt.php';
    }
    
    public function postpaid_dispense_order_receipt($transactionid,$patientid){
            if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
            
            $patient=$this->Reception_model->patient($patientid);
            $patient=$this->Reception_model->patients($patient->id);
            
            $this->data['title']="Post Paid Dispense Receipt";
            $this->data['patient']=$patient;
            $this->data['bill_data']=$this->Reception_model->bill_transaction(null,$transactionid);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='pharmacy/postpaid_dispense_order_receipt';
            $this->load->view('pharmacy/template2',$this->data);
    }
    
    function download_postpaid_dispense_receipt($transactionid){
        
        $bill_data=$this->Reception_model->bill_transaction(null,$transactionid);
        $institution=$this->SuperAdministration_model->institution_information();
        require_once 'receipts/postpaid_dispense_receipt.php';
    }
    
    public function remove_dispense_order($id,$patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->Pharmacy_model->remove_dispense_order($id);
           redirect('Pharmacy/create_dispense_order/'.$patientid.'/'.$patientvisitid,'refresh');
    }
    
    public function view_sponsors(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                      array('title'=>'View Sponsor(s)','link'=>'Pharmacy/view_sponsors'),
                                      array('title'=>'View Payment Mode(s)','link'=>'Pharmacy/view_paymentModes'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
           }

           $name =$key['name'];

           $config["base_url"] = base_url() . "index.php/Pharmacy/view_sponsors/name_".$key['name']."/";
           $config["total_rows"] =$this->SuperAdministration_model->sponsor_info_count($name);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['sponsors'] = $this->SuperAdministration_model->sponsor_info($name,$page,$limit);

           $this->data['title']="Sponsors";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='pharmacy/view_sponsors';
           $this->load->view('pharmacy/template',$this->data);
   }
   
    public function view_paymentModes(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                      array('title'=>'View Sponsor(s)','link'=>'Pharmacy/view_sponsors'),
                                      array('title'=>'View Payment Mode(s)','link'=>'Pharmacy/view_paymentModes'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

           if ($this->input->post('sponsor')) {
               $key['sponsor'] = $this->input->post('sponsor');
               $this->data['sponsor']=$this->input->post('sponsor');
           }

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];

               $key['sponsor'] = $exp[3];
               $this->data['sponsor']=$key['sponsor'];
           }

           $name =$key['name'];
           $sponsor =$key['sponsor'];

           $config["base_url"] = base_url() . "index.php/Pharmacy/view_paymentModes/name_".$key['name']."_sponsor_".$key['sponsor']."/";
           $config["total_rows"] =$this->SuperAdministration_model->paymentMode_info_count($name,$sponsor);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['paymentModes'] = $this->SuperAdministration_model->paymentMode_info($name,$sponsor,$page,$limit);

           $this->data['title']="Payment Modes";
           $this->data['sponsors']=$this->SuperAdministration_model->sponsors();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='pharmacy/view_paymentModes';
           $this->load->view('pharmacy/template',$this->data);
   }
   
    public function drug_dosages(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
         $this->data['side_menu']=array(
                                   array('title'=>'Drug Dosages','link'=>'Pharmacy/drug_dosages'),
                                   array('title'=>'Drug Frequencies','link'=>'Pharmacy/drug_frequencies'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
        }

        $name =$key['name'];

        $config["base_url"] = base_url() . "index.php/Pharmacy/drug_dosages/name_".$key['name']."/";
        $config["total_rows"] =$this->Pharmacy_model->drug_dosages_info_count($name);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['drug_dosages'] = $this->Pharmacy_model->drug_dosages_info($name,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Drug Dosages';
        $this->data['content']='pharmacy/drug_dosages';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function drug_frequencies(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'Drug Dosages','link'=>'Pharmacy/drug_dosages'),
                                   array('title'=>'Drug Frequencies','link'=>'Pharmacy/drug_frequencies'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
        }

        $name =$key['name'];

        $config["base_url"] = base_url() . "index.php/Pharmacy/drug_frequencies/name_".$key['name']."/";
        $config["total_rows"] =$this->Pharmacy_model->drug_frequencies_info_count($name);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['drug_frequencies'] = $this->Pharmacy_model->drug_frequencies_info($name,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Drug Frequencies';
        $this->data['content']='pharmacy/drug_frequencies';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function add_drug_dosage($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                   array('title'=>'Drug Dosages','link'=>'Pharmacy/drug_dosages'),
                                   array('title'=>'Drug Frequencies','link'=>'Pharmacy/drug_frequencies'),
                                );
           
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('factor','Factor','xss_clean|required|numeric');
           


            if ($this->form_validation->run() == TRUE)
           {
                $name=$this->input->post('name');
                $factor=$this->input->post('factor');

                

                       $drug_dosage=array(
                           'name'=>$name,
                           'factor'=>$factor
                       );


                       //code uniqueness
                       $name_check=$this->Pharmacy_model->drug_dosage_name_check($name,$id);
                       if($name_check == null){


                           //do saving
                            $resp=$this->Pharmacy_model->save_drug_dosage($drug_dosage,$id);

                              if($resp){

                                  redirect('Pharmacy/drug_dosages','refresh');
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">drug dosage name exists already!</div>';
                       }
               
           }

           $this->data['title']="Add Drug Dosage";
           $this->data['id']="$id";
           $this->data['drug_dosage']=$this->Pharmacy_model->drug_dosages($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='pharmacy/add_drug_dosage';
           $this->load->view('pharmacy/template',$this->data);
    }
    
    public function add_drug_frequency($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                   array('title'=>'Drug Dosages','link'=>'Pharmacy/drug_dosages'),
                                   array('title'=>'Drug Frequencies','link'=>'Pharmacy/drug_frequencies'),
                                );
           
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('factor','Factor','xss_clean|required|numeric');
           


            if ($this->form_validation->run() == TRUE)
           {
                $name=$this->input->post('name');
                $factor=$this->input->post('factor');

                

                       $drug_frequency=array(
                           'name'=>$name,
                           'factor'=>$factor
                       );


                       //code uniqueness
                       $name_check=$this->Pharmacy_model->drug_frequency_name_check($name,$id);
                       if($name_check == null){


                           //do saving
                            $resp=$this->Pharmacy_model->save_drug_frequency($drug_frequency,$id);

                              if($resp){

                                  redirect('Pharmacy/drug_frequencies','refresh');
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">drug frequency name exists already!</div>';
                       }
               
           }

           $this->data['title']="Add Drug Frequency";
           $this->data['id']="$id";
           $this->data['drug_frequency']=$this->Pharmacy_model->drug_frequencies($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='pharmacy/add_drug_frequency';
           $this->load->view('pharmacy/template',$this->data);
    }
    
    public function activate_deactivate_drugDosage($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Pharmacy_model->activate_deactivate_drugDosage($status,$id);
        redirect('pharmacy/drug_dosages','refresh');
    }
    
    public function activate_deactivate_drugFrequency($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Pharmacy_model->activate_deactivate_drugFrequency($status,$id);
        redirect('pharmacy/drug_frequencies','refresh');
    }
    
    public function drug_types(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'Drug Groups','link'=>'Pharmacy/drug_groups'),
                                   array('title'=>'Drug Types','link'=>'Pharmacy/drug_types'),
                                   array('title'=>'Inventory Items','link'=>'Pharmacy/view_items'),
                                   array('title'=>'Unit Stock','link'=>'Pharmacy/units_stock'),
                                   array('title'=>'Stock Transfer','link'=>'Pharmacy/stock_transfer'),
                                   array('title'=>'Requesition Order','link'=>'Pharmacy/requesition_order'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
        }

        $name =$key['name'];

        $config["base_url"] = base_url() . "index.php/Pharmacy/drug_types/name_".$key['name']."/";
        $config["total_rows"] =$this->Inventory_model->drug_types_info_count($name);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['drug_types'] = $this->Inventory_model->drug_types_info($name,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Drug Types';
        $this->data['content']='pharmacy/drug_types';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function drug_groups(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'Drug Groups','link'=>'Pharmacy/drug_groups'),
                                   array('title'=>'Drug Types','link'=>'Pharmacy/drug_types'),
                                   array('title'=>'Inventory Items','link'=>'Pharmacy/view_items'),
                                   array('title'=>'Unit Stock','link'=>'Pharmacy/units_stock'),
                                   array('title'=>'Stock Transfer','link'=>'Pharmacy/stock_transfer'),
                                   array('title'=>'Requesition Order','link'=>'Pharmacy/requesition_order'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
        }

        $name =$key['name'];

        $config["base_url"] = base_url() . "index.php/Pharmacy/drug_groups/name_".$key['name']."/";
        $config["total_rows"] =$this->Inventory_model->drug_groups_info_count($name);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['drug_groups'] = $this->Inventory_model->drug_groups_info($name,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Drug Groups';
        $this->data['content']='pharmacy/drug_groups';
        $this->load->view('pharmacy/template',$this->data);
    }
    
     public function view_packages(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $this->data['side_menu']=array(
                                    array('title'=>'View Packages(s)','link'=>'Pharmacy/view_packages'),
                                 );
        
        
            if($this->input->post('name')) {
                $key['name'] = $this->input->post('name');
                $this->data['name']=$this->input->post('name');
            }
            
           
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            
            $this->data['name']=$key['name'];
        }

        
        $name =$key['name'];

        
        $config["base_url"] = base_url() . "index.php/Pharmacy/view_packages/name_".$key['name']."/";
        $config["total_rows"] =$this->Administration_model->packages_info_count($name);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['packages'] = $this->Administration_model->packages_info($name,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Packages';
        $this->data['content']='pharmacy/view_packages';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function package_details($id){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $this->data['side_menu']=array(
                                    array('title'=>'View Packages(s)','link'=>'Pharmacy/view_packages'),
                                );
        
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/package_details/$id/";
        $config["total_rows"] =$this->Administration_model->package_items_services_info_count($id);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['pckg_details'] = $this->Administration_model->package_details_info($id,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Package Details';
        $this->data['content']='pharmacy/view_package_details';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function view_items($code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
       $this->data['side_menu']=array(
                                   array('title'=>'Drug Groups','link'=>'Pharmacy/drug_groups'),
                                   array('title'=>'Drug Types','link'=>'Pharmacy/drug_types'),
                                   array('title'=>'Inventory Items','link'=>'Pharmacy/view_items'),
                                   array('title'=>'Items Costs','link'=>'Pharmacy/view_items_cost'),
                                   array('title'=>'Unit Stock','link'=>'Pharmacy/units_stock'),
                                   array('title'=>'Stock Transfer','link'=>'Pharmacy/stock_transfer'),
                                   array('title'=>'Requesition Order','link'=>'Pharmacy/requesition_order'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(4)) {
            $exp = explode("_", $this->uri->segment(4));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];
            
            $docType = $exp[7];

        }
        
        
        
        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        if($docType == 1){
            $data=$this->Inventory_model->inventory_items($code,$name,$type,$grp);
            require_once 'reports/inventory/inventory_items_pdf.php';
        }

        if($docType == 2){
            
             $data=$this->Inventory_model->inventory_items($code,$name,$type,$grp);
             require_once 'reports/inventory/inventory_items_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/view_items/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =5;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['items_view']='pharmacy/items';
        $this->data['title']='View Items';
        $this->data['content']='pharmacy/view_items';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function view_items_cost($code){
           
           $this->data['side_menu']=array(
                                   array('title'=>'Drug Groups','link'=>'Pharmacy/drug_groups'),
                                   array('title'=>'Drug Types','link'=>'Pharmacy/drug_types'),
                                   array('title'=>'Inventory Items','link'=>'Pharmacy/view_items'),
                                   array('title'=>'Items Costs','link'=>'Pharmacy/view_items_cost'),
                                   array('title'=>'Unit Stock','link'=>'Pharmacy/units_stock'),
                                   array('title'=>'Stock Transfer','link'=>'Pharmacy/stock_transfer'),
                                   array('title'=>'Requesition Order','link'=>'Pharmacy/requesition_order'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('sponsor')) {
            $key['sponsor'] = $this->input->post('sponsor');
            $this->data['sponsor']=$this->input->post('sponsor');
        }
        
        if ($this->input->post('min')) {
            $key['min'] = $this->input->post('min');
            $this->data['min']=$this->input->post('min');
        }
        
        if ($this->input->post('max')) {
            $key['max'] = $this->input->post('max');
            $this->data['max']=$this->input->post('max');
        }
         
            
            
        if ($this->uri->segment(4)) {
            $exp = explode("_", $this->uri->segment(4));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['sponsor'] = $exp[3];
            $this->data['sponsor']=$key['sponsor'];
            
            $key['min'] = $exp[5];
            $this->data['min']=$key['min'];
            
            $key['max'] = $exp[7];
            $this->data['max']=$key['max'];
            
            $docType= $exp[9];
            $this->data['docType']=$docType;

        }

        
        
        $name =$key['name'];
        $sponsor =$key['sponsor'];
        $min =$key['min'];
        $max =$key['max'];
        
        if($docType == 1){
            
            $data=$this->Administration_model->items_cost($code,$name,$sponsor,$min,$max);
           require_once 'reports/administrator/items_cost_pdf.php';
        }
        
        if($docType == 2){
            $data=$this->Administration_model->items_cost($code,$name,$sponsor,$min,$max);
            require_once 'reports/administrator/items_cost_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Administration/view_items_cost/$code/name_".$key['name']."_sponsor_".$key['sponsor']."_min_".$key['min']."_max_".$key['max']."/";
        $config["total_rows"] =$this->Administration_model->items_cost_info_count($code,$name,$sponsor,$mincost,$maxcost);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =5;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Administration_model->items_cost_info($code,$name,$sponsor,$mincost,$maxcost,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['sponsors'] = $this->SuperAdministration_model->active_sponsors();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['items_view']='pharmacy/items_cost';
        $this->data['title']='View Items Cost';
        $this->data['content']='pharmacy/view_items_cost';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function units_stock($unit,$code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit  
        
        $this->data['side_menu']=array(
                                   array('title'=>'Drug Groups','link'=>'Pharmacy/drug_groups'),
                                   array('title'=>'Drug Types','link'=>'Pharmacy/drug_types'),
                                   array('title'=>'Inventory Items','link'=>'Pharmacy/view_items'),
                                   array('title'=>'Items Costs','link'=>'Pharmacy/view_items_cost'),
                                   array('title'=>'Unit Stock','link'=>'Pharmacy/units_stock'),
                                   array('title'=>'Stock Transfer','link'=>'Pharmacy/stock_transfer'),
                                   array('title'=>'Requesition Order','link'=>'Pharmacy/requesition_order'),
                                );
        
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(5)) {
            $exp = explode("_", $this->uri->segment(5));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];

        }

        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/main_stock/".$unit[0]->inventoryunit."/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =6;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['unit']=$unit[0]->inventoryunit;
        $this->data['units_stock_items']='pharmacy/units_stock_items';
        $unit_name=$this->Inventory_model->units($unit[0]->inventoryunit);
        $this->data['title']=$unit_name[0]->name.' Unit Stock';
        $this->data['content']='pharmacy/units_stock';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function inventory_unit_item_stock_details($itemid,$code,$unit){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit      
        $this->data['side_menu']=array(
                                   array('title'=>'Drug Groups','link'=>'Pharmacy/drug_groups'),
                                   array('title'=>'Drug Types','link'=>'Pharmacy/drug_types'),
                                   array('title'=>'Inventory Items','link'=>'Pharmacy/view_items'),
                                   array('title'=>'Items Costs','link'=>'Pharmacy/view_items_cost'),
                                   array('title'=>'Unit Stock','link'=>'Pharmacy/units_stock'),
                                   array('title'=>'Stock Transfer','link'=>'Pharmacy/stock_transfer'),
                                   array('title'=>'Requesition Order','link'=>'Pharmacy/requesition_order'),
                                );
        
        $this->data['item_inventory_details']=$this->Inventory_model->item_inventory_details($itemid,$unit[0]->inventoryunit);   
        $item_name=$this->Inventory_model->items($itemid);
        $unit_name=$this->Inventory_model->units($unit[0]->inventoryunit);
        $this->data['title']=$item_name[0]->name.' '.$unit_name[0]->name.' Inventory Details';
        $this->data['code']=$code;
        $this->data['unit']=$unit[0]->inventoryunit;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='pharmacy/item_unit_inventory_details';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function stock_transfer($code){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit   
        
        $this->data['side_menu']=array(
                                   array('title'=>'Drug Groups','link'=>'Pharmacy/drug_groups'),
                                   array('title'=>'Drug Types','link'=>'Pharmacy/drug_types'),
                                   array('title'=>'Inventory Items','link'=>'Pharmacy/view_items'),
                                   array('title'=>'Items Costs','link'=>'Pharmacy/view_items_cost'),
                                   array('title'=>'Unit Stock','link'=>'Pharmacy/units_stock'),
                                   array('title'=>'Stock Transfer','link'=>'Pharmacy/stock_transfer'),
                                   array('title'=>'Requesition Order','link'=>'Pharmacy/requesition_order'),
                                );
        
        //process form if button pressed
        if($this->input->post('save')){
            
            $stocktransfer=$this->input->post('stocktransfer');
            
            if(count($stocktransfer) > 0){
                
                foreach($stocktransfer as $value){
                    $error=TRUE;
                    $this->form_validation->set_rules('stocktransfer['.$value.']','Confirm','required|xss_clean');
                    $this->form_validation->set_rules('destination_'.$value,'Destination','required|xss_clean');
                    $this->form_validation->set_rules('quantity_'.$value,'Quantity','xss_clean|numeric|required');

                    if ($this->form_validation->run() == TRUE){

                        $itemid=$this->input->post('itemid_'.$value);
                        $destination=$this->input->post('destination_'.$value);
                        $quantity=$this->input->post('quantity_'.$value);
                        $available_stock=$this->Inventory_model->inventory_stock($unit[0]->inventoryunit,$itemid,$quantity);

                        if($available_stock <> null){
                            foreach($available_stock as $ky=>$val){

                                $stocking=array(
                                    'itemid'=>$itemid,
                                    'batch'=>$val['batch'],
                                    'storebatch'=>$val['storebatch'],
                                    'expiredate'=>$val['expiredate'],
                                    'supplierid'=>$val['supplierid'],
                                    'quantity'=>$val['quantity'],
                                    'totalvalue'=>$val['totalvalue'],
                                    'unitprice'=>$val['unitprice'],
                                    'inventoryunit'=>$destination,
                                    'source_inventoryunit'=>$unit[0]->inventoryunit,
                                    'status'=>'STOCKING',
                                    'lastupdate'=>Date('Y-m-d H:i:s'),
                                    'userid'=>$this->session->userdata('user_id'),
                                );

                                $destocking=array(
                                    'itemid'=>$itemid,
                                    'batch'=>$val['batch'],
                                    'storebatch'=>$val['storebatch'],
                                    'expiredate'=>$val['expiredate'],
                                    'supplierid'=>$val['supplierid'],
                                    'quantity'=>-$val['quantity'],
                                    'totalvalue'=>$val['totalvalue'],
                                    'unitprice'=>$val['unitprice'],
                                    'inventoryunit'=>$unit[0]->inventoryunit,
                                    'status'=>'TRANSFER',
                                    'lastupdate'=>Date('Y-m-d H:i:s'),
                                    'userid'=>$this->session->userdata('user_id'),
                                );
                                
                                $sv=$this->Inventory_model->process_stocking_destocking($stocking,$destocking);

                                if($sv){
                                    $error=FALSE;
                                }
                            }
                        }else{
                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">no enough stock available</div>'; 
                        }
                    }
                }

                if(!$error){
                    redirect(current_url());
                }
            }else{
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">please confirm atleast one transfer!</div>';
            }
            
        }
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(4)) {
            $exp = explode("_", $this->uri->segment(4));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];

        }

        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/stock_transfer/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =5;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['units']=$this->Inventory_model->active_units();
        $this->data['unit']=$unit[0]->inventoryunit;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['transfer_items']='pharmacy/transfer_items';
        $unit_name=$this->Inventory_model->units($unit[0]->inventoryunit);
        $this->data['title']=$unit_name[0]->name.' Stock Transfer';
        $this->data['content']='pharmacy/stock_transfer';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function requesition_order($code){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit
            $institution=$this->SuperAdministration_model->institution_information();
           $this->data['side_menu']=array(
                                   array('title'=>'Drug Groups','link'=>'Pharmacy/drug_groups'),
                                   array('title'=>'Drug Types','link'=>'Pharmacy/drug_types'),
                                   array('title'=>'Inventory Items','link'=>'Pharmacy/view_items'),
                                   array('title'=>'Items Costs','link'=>'Pharmacy/view_items_cost'),
                                   array('title'=>'Unit Stock','link'=>'Pharmacy/units_stock'),
                                   array('title'=>'Stock Transfer','link'=>'Pharmacy/stock_transfer'),
                                   array('title'=>'Requesition Order','link'=>'Pharmacy/requesition_order'),
                                );
        
            //process form if button pressed
        if($this->input->post('save')){
            
            $requesitionorder=$this->input->post('requesitionorder');
            
            if(count($requesitionorder) > 0){
                foreach($requesitionorder as $value){
                    $error=TRUE;
                    $this->form_validation->set_rules('requesitionorder['.$value.']','Confirm','required|xss_clean');
                    $this->form_validation->set_rules('destination_'.$value,'Destination','xss_clean');
                    $this->form_validation->set_rules('quantity_'.$value,'Quantity','xss_clean|numeric|required');

                    if ($this->form_validation->run() == TRUE){

                        $itemid=$this->input->post('itemid_'.$value);
                        $destination=$this->input->post('destination_'.$value)<>null?$this->input->post('destination_'.$value):null;
                        $quantity=$this->input->post('quantity_'.$value);
                        
                        $req_id=$this->get_requestionid($institution->institution_id,$unit[0]->inventoryunit,$destination);
                        
                        $req_order=array(
                                    'itemid'=>$itemid,
                                    'requesitionid'=>$req_id,
                                    'inventoryunit'=>$unit[0]->inventoryunit,
                                    'supplyunit'=>$destination,
                                    'requestqty'=>$quantity,
                                    'createdby'=>$this->session->userdata('user_id'),
                                );

                               
                        $sv=$this->Pharmacy_model->post_requesition_order($req_order);

                        if($sv){
                            $error=FALSE;
                        }
                    }
                }

                if(!$error){
                    redirect(current_url());
                }
            }else{
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">please confirm atleast one transfer!</div>';
            }
            
        }
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(4)) {
            $exp = explode("_", $this->uri->segment(4));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];

        }

        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/requesition_order/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =5;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
        
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['units']=$this->Inventory_model->active_units();
        $this->data['unit']=$unit[0]->inventoryunit;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$institution;
        $this->data['code']=$code;
        $this->data['transfer_items']='pharmacy/items_requesition_order';
        $unit_name=$this->Inventory_model->units($unit[0]->inventoryunit);
        $this->data['title']=$unit_name[0]->name.' Stock Requesition Order';
        $this->data['content']='pharmacy/stock_requesition_order';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function aggregated_collection(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
           $this->data['side_menu']=array(
                               array('title'=>'Aggregated Collection','link'=>'Pharmacy/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Pharmacy/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Pharmacy/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Pharmacy/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Pharmacy/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Pharmacy/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Pharmacy/prepaid_accounts'),
                           );
        
        $start=$this->input->post('start') <> null?$this->input->post('start'):date('Y-m-d');
        $end=$this->input->post('end') <> null?$this->input->post('end'):date('Y-m-d');
        $department=$this->input->post('department');
        $subdepartment=$this->input->post('subdepartment');
        $sponsor=$this->input->post('sponsor');
        $userid=$this->session->userdata('user_id');
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $start = $exp[1];
            $end = $exp[3];
            $sponsor = $exp[5];
            $department = $exp[7];
            $subdepartment = $exp[9];
            $docType = $exp[11];
        }
        
        $data=$this->Reception_model->aggregated_collection($start,$end,$sponsor,$department,$subdepartment,$userid);
        if($docType == 1){
                
            require_once 'reports/reception/aggregated_collection_report_pdf.php';
        }

        if($docType == 2){

             require_once 'reports/reception/aggegated_collection_report_excel.php';
        }
        
        $this->data['title']="Aggregated Collections";
        $this->data['departments']=$this->Administration_model->active_departments();
        $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
        $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
        $this->data['sponsor']=$sponsor;
        $this->data['subdepartment']=$subdepartment;
        $this->data['department']=$department;
        $this->data['end']=$end;
        $this->data['start']=$start;
        $this->data['aggregated_collections']=$data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='pharmacy/aggregated_collection';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function general_collection(){
        if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Collection','link'=>'Pharmacy/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Pharmacy/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Pharmacy/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Pharmacy/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Pharmacy/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Pharmacy/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Pharmacy/prepaid_accounts'),
                           );
               
        $start=$this->input->post('start') <> null?$this->input->post('start'):date('Y-m-d');
        $end=$this->input->post('end') <> null?$this->input->post('end'):date('Y-m-d');
        $department=$this->input->post('department');
        $subdepartment=$this->input->post('subdepartment');
        $sponsor=$this->input->post('sponsor');
        $userid=$this->session->userdata('user_id');
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $start = $exp[1];
            $end = $exp[3];
            $sponsor = $exp[5];
            $department = $exp[7];
            $subdepartment = $exp[9];
            $docType = $exp[11];
        }
        
        $data=$this->Reception_model->general_collection($start,$end,$sponsor,$department,$subdepartment,$userid);
        if($docType == 1){
                
            require_once 'reports/reception/general_collection_report_pdf.php';
        }

        if($docType == 2){

             require_once 'reports/reception/general_collection_report_excel.php';
        }
        
        $this->data['title']="General Collections";
        $this->data['departments']=$this->Administration_model->active_departments();
        $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
        $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
        $this->data['sponsor']=$sponsor;
        $this->data['subdepartment']=$subdepartment;
        $this->data['department']=$department;
        $this->data['end']=$end;
        $this->data['start']=$start;
        $this->data['general_collections']=$data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='pharmacy/general_collection';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function package_accounts(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
        $this->data['side_menu']=array(
                               array('title'=>'Aggregated Collection','link'=>'Pharmacy/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Pharmacy/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Pharmacy/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Pharmacy/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Pharmacy/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Pharmacy/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Pharmacy/prepaid_accounts'),
                           );
        
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('package')){
            $key['package'] = $this->input->post('package');
            $this->data['package']=$this->input->post('package');
        }

        if ($this->input->post('subscriptionid')) {
            $key['subscriptionid'] = $this->input->post('subscriptionid');
            $this->data['subscriptionid']=$this->input->post('subscriptionid');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
             $key['subscriptionid'] = $exp[3];
            $this->data['subscriptionid']=$key['subscriptionid'];

            $key['package'] = $exp[5];
            $this->data['package']=$key['package'];
        }

        $patientid =$key['patientid'];
        $subscriptionid =$key['subscriptionid'];
        $package =$key['package'];
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/package_accounts/patientid_".$key['patientid']."_subscriptionid_".$key['subscriptionid']."_package_".$key['package']."/";
        $config["total_rows"] =$this->Reception_model->patient_packages_count($patientid,$subscriptionid,$package);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_packages'] = $this->Reception_model->patient_packages($patientid,$subscriptionid,$package,$page,$limit);

        $this->data['title']="Patient Packages";
        $this->data['packages']=$this->Administration_model->packages();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='pharmacy/patient_packages';
        $this->load->view('pharmacy/template',$this->data);       
    }
    
    public function patient_package_details($subscriptionid,$patientid,$docType){
        if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Collection','link'=>'Pharmacy/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Pharmacy/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Pharmacy/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Pharmacy/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Pharmacy/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Pharmacy/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Pharmacy/prepaid_accounts'),
                           );
        if($docType == 1){
                
                $data=$this->Reception_model->subscriptions($subscriptionid);
                require_once 'reports/reception/package_account_report_pdf.php';
        }

        if($docType == 2){

             $data=$this->Reception_model->subscriptions($subscriptionid);
             require_once 'reports/reception/package_account_report_excel.php';
        }    
        $patient=$this->Reception_model->patient($patientid);       
        $patient=$this->Reception_model->patients($patient->id);       
        $this->data['patient_package_details'] =$this->Reception_model->subscriptions($subscriptionid);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['subscriptionid']=$subscriptionid;
        $this->data['title']=$subscriptionid." Subscription Details";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='pharmacy/patient_package_details';
        $this->load->view('pharmacy/template2',$this->data);
    }
    
    public function patients_debtors(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Collection','link'=>'Pharmacy/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Pharmacy/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Pharmacy/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Pharmacy/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Pharmacy/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Pharmacy/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Pharmacy/prepaid_accounts'),
                           );
               
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('start')){
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
             $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];
        }

        $patientid =$key['patientid'];
        $start =$key['start'];
        $end =$key['end'];
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/patients_debtors/patientid_".$key['patientid']."_start_".$key['start']."_end_".$key['end']."/";
        $config["total_rows"] =$this->Reception_model->patient_debtors_count($patientid,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_debtors'] = $this->Reception_model->patient_debtors($patientid,$start,$end,$page,$limit);

        $this->data['title']="Patient Debtors";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='pharmacy/patient_debtors';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function patient_debt_details($patientid,$docType){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Collection','link'=>'Pharmacy/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Pharmacy/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Pharmacy/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Pharmacy/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Pharmacy/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Pharmacy/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Pharmacy/prepaid_accounts'),
                           );
            
            if($docType == 1){
                
                $data=$this->Reception_model->patient_debt_details($patientid);
                require_once 'reports/reception/patient_debt_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->patient_debt_details($patientid);
                 require_once 'reports/reception/patient_debt_report_excel.php';
            }
            
               if($this->input->post('clear_debt')){
                   $debts=$this->input->post('debt');
                   
                   if(count($debts) > 0){
                       
                       foreach($debts as $key=>$value){
                           
                           $data=explode('_',$value);
                           $array[]=array(
                               'item_service_id'=>$data[0],
                               'departmentid'=>$data[1],
                               'postpaid_transactionid'=>$data[2],
                            );
                       }
                       
                       if($array <> null){
                           
                           $txngenerate=FALSE;
                           foreach($array as $key=>$value){
                               
                               if($txngenerate==FALSE){
                                   
                                        $txnid=$this->Reception_model->get_transaction_id();
                                        $txngenerate=TRUE;
                                }
                                
                                $bill_txn=array(
                                    'transactionid'=>$txnid,
                                    'paymentmodeid'=>$this->config->item('instant_cash_bill_pmode_code'),
                                    'createdon'=>date('Y-m-d H:i:s'),
                                    'createdby'=>$this->session->userdata('user_id'),
                                    'paymentstatus'=>1,
                                );
                                
                                $debt_bill_txn=array(
                                    'reftransactionid'=>$txnid,
                                    'status'=>1,
                                    'modifiedby'=>$this->session->userdata('user_id'),
                                    'modifiedon'=>date('Y-m-d H:i:s'),
                                );
                                
                                $clear=$this->Reception_model->clear_debt($bill_txn,$debt_bill_txn,$value['item_service_id'],$value['postpaid_transactionid']);
                                
                                if($clear){
                                    
                                    $this->Reception_model->update_service_item_order($txnid,$value['postpaid_transactionid'],$value['item_service_id'],FALSE);
                                    redirect('Pharmacy/dispense_order_receipt/'.$txnid.'/'.$patientid,'refresh');
                                }
                            }
                       }
                   }else{
                       $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">confirm atleast one debt!</div>';
                   }
               }
          
        $patient=$this->Reception_model->patient($patientid);       
        $patient=$this->Reception_model->patients($patient->id);  
        $this->data['patient_debt_details'] =$this->Reception_model->patient_debt_details($patientid);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['title']="Patient Debt Details";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='pharmacy/patient_debt_details';
        $this->load->view('pharmacy/template2',$this->data);
    }
    
    public function patient_statement($details){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Collection','link'=>'Pharmacy/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Pharmacy/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Pharmacy/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Pharmacy/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Pharmacy/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Pharmacy/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Pharmacy/prepaid_accounts'),
                           );
               
                $details=explode("_",$details);
                $patientid=$details[1];
                $start=$details[3];
                $end=$details[5];
                $docType=$details[7];
                $institution=$this->SuperAdministration_model->institution_information();
                if($docType == 1){
                   
                    $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end);
                    require_once 'reports/reception/patients_statement_report_pdf.php';
                }

                if($docType == 2){

                     $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end);
                     require_once 'reports/reception/patients_statement_report_excel.php';
                }
        
        
        $this->form_validation->set_rules('patientid','Patient Id','xss_clean|required|callback_patientid');
        $this->form_validation->set_rules('start','Start Date','xss_clean|required|callback_start');
        $this->form_validation->set_rules('end','End Date','xss_clean|required|callback_end');

        if($this->form_validation->run() == TRUE){
            
            $patientid=$this->input->post('patientid');
            $start=$this->input->post('start');
            $end=$this->input->post('end');
            
            $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end);
        }
           
        $this->data['title']="Patient Statement";
        $this->data['patientid']=$patientid;
        $this->data['start']=$start;
        $this->data['end']=$end;
        $this->data['data']=$data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$institution;
        $this->data['statement']='common/statement';
        $this->data['content']='pharmacy/patient_statement';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function patient_transactions(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
        
        $this->data['side_menu']=array(
                               array('title'=>'Aggregated Collection','link'=>'Pharmacy/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Pharmacy/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Pharmacy/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Pharmacy/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Pharmacy/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Pharmacy/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Pharmacy/prepaid_accounts'),
                           );
               
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('start')){
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }

        if ($this->input->post('sponsor')) {
            $key['sponsor'] = $this->input->post('sponsor');
            $this->data['sponsor']=$this->input->post('sponsor');
        }

        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
            $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];

            $key['sponsor'] = $exp[7];
            $this->data['sponsor']=$key['sponsor'];
            
            $docType = $exp[9];
        }

        $start =$key['start'];
        $end =$key['end'];
        $sponsor =$key['sponsor'];
        $patientid =$key['patientid'];
           
        if($docType == 1){
                
            $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end,$sponsor);
            require_once 'reports/reception/patients_transactions_report_pdf.php';
        }

        if($docType == 2){

             $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end,$sponsor);
             require_once 'reports/reception/patients_transactions_report_excel.php';
        }
            
        $config["base_url"] = base_url() . "index.php/Pharmacy/patient_transactions/patientid_".$key['patientid']."_start_".$key['start']."_end_".$key['end']."_sponsor_".$key['sponsor']."/";
        $config["total_rows"] =$this->Reception_model->patient_transactions_count($patientid,$start,$end,$sponsor);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_transactions'] = $this->Reception_model->patient_transactions($patientid,$start,$end,$sponsor,$page,$limit);

        $this->data['title']="Patient Transactions";
        $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='pharmacy/patient_transactions';
        $this->load->view('pharmacy/template',$this->data);
    }
   
    public function prepaid_accounts(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
        $this->data['side_menu']=array(
                               array('title'=>'Aggregated Collection','link'=>'Pharmacy/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Pharmacy/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Pharmacy/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Pharmacy/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Pharmacy/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Pharmacy/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Pharmacy/prepaid_accounts'),
                           );
        
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('subscriptionid')) {
            $key['subscriptionid'] = $this->input->post('subscriptionid');
            $this->data['subscriptionid']=$this->input->post('subscriptionid');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
             $key['subscriptionid'] = $exp[3];
            $this->data['subscriptionid']=$key['subscriptionid'];

        }

        $patientid =$key['patientid'];
        $subscriptionid =$key['subscriptionid'];
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/package_accounts/patientid_".$key['patientid']."_subscriptionid_".$key['subscriptionid']."/";
        $config["total_rows"] =$this->Reception_model->patients_prepaid_accounts_count($patientid,$subscriptionid);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_prepaid'] = $this->Reception_model->patients_prepaid_accounts($patientid,$subscriptionid,$page,$limit);

        $this->data['title']="Patient Prepaid Accounts";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='pharmacy/patient_prepaid_accounts';
        $this->load->view('pharmacy/template',$this->data);       
    }
    
     public function patient_prepaid_account_details($subscriptionid,$patientid,$docType){
        if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Collection','link'=>'Pharmacy/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Pharmacy/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Pharmacy/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Pharmacy/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Pharmacy/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Pharmacy/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Pharmacy/prepaid_accounts'),
                           );
         
            
            if($docType == 1){
                
                $data=$this->Reception_model->prepaid_accounts($subscriptionid);
                require_once 'reports/reception/prepaid_account_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->prepaid_accounts($subscriptionid);
                 require_once 'reports/reception/prepaid_account_report_excel.php';
            }
            
        $patient=$this->Reception_model->patient($patientid);       
        $patient=$this->Reception_model->patients($patient->id);       
        $this->data['patient_prepaid_details'] =$this->Reception_model->prepaid_accounts($subscriptionid);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['subscriptionid']=$subscriptionid;
        $this->data['title']=$subscriptionid." Subscription Details";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='pharmacy/patient_prepaid_details';
        $this->load->view('pharmacy/template2',$this->data);
    }
    
    public function disposed_items(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit
       $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Pharmacy/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Pharmacy/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Pharmacy/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Pharmacy/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Pharmacy/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Pharmacy/stock_taking_report'),
                           array('title'=>'Requesition Orders','link'=>'Pharmacy/requesition_orders_report'),
                        );
        
       
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        $unit=$unit[0]->inventoryunit;
        $this->data['unit']=$unit;
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->disposed_items_report($unit,$item,$start,$end);
            require_once 'reports/inventory/disposed_items_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->disposed_items_report($unit,$item,$start,$end);
            require_once 'reports/inventory/disposed_items_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/disposed_items/0_".$unit."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->disposed_items_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['disposes'] = $this->Inventory_model->disposed_items_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $unit_name=$this->Inventory_model->units($unit);
        $this->data['title']=$unit_name[0]->name.' Disposed Items';
        $this->data['content']='pharmacy/disposed_items';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function items_consumption(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit
        $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Pharmacy/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Pharmacy/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Pharmacy/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Pharmacy/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Pharmacy/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Pharmacy/stock_taking_report'),
                           array('title'=>'Requesition Orders','link'=>'Pharmacy/requesition_orders_report'),
                        );
        
       
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        $unit=$unit[0]->inventoryunit;
        $this->data['unit']=$unit;
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->items_consumption_report($unit,$item,$start,$end);
            require_once 'reports/inventory/items_consumption_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->items_consumption_report($unit,$item,$start,$end);
            require_once 'reports/inventory/items_consumption_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/items_consumption/0_".$unit."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->items_consumption_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['consumptions'] = $this->Inventory_model->items_consumption_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $unit_name=$this->Inventory_model->units($unit);
        $this->data['title']=$unit_name[0]->name.' Items Consumption';
        $this->data['content']='pharmacy/items_consumption';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function expired_items(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit
        $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Pharmacy/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Pharmacy/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Pharmacy/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Pharmacy/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Pharmacy/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Pharmacy/stock_taking_report'),
                           array('title'=>'Requesition Orders','link'=>'Pharmacy/requesition_orders_report'),
                        );
        
        $segment=$this->uri->segment(3);
        $seg=explode('_',$segment);
        $docType=$seg[0];
        $category=$seg[1];
        $item=$seg[2];
        $unit=$unit[0]->inventoryunit;
        $start=$seg[4];
        $end=$seg[5];
        
        if($this->input->post('search')){
            
            $category=$this->input->post('category');
            $item=$this->input->post('item');
            $start=$this->input->post('start');
            $end=$this->input->post('end');
        }
        
        $data=$this->Inventory_model->expired_items($category,$item,$unit,$start,$end);
        
        if($docType == 1){
            
            require_once 'reports/inventory/expired_items_pdf.php';
        }
        
        if($docType == 2){
            
            require_once 'reports/inventory/expired_items_excel.php';
        }
        
        $this->data['category']=$category;
        $this->data['item']=$item;
        $this->data['unit']=$unit;
        $this->data['start']=$start;
        $this->data['end']=$end;
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['expired'] = $data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $unit_name=$this->Inventory_model->units($unit);
        $this->data['title']=$unit_name[0]->name.' Expired Items';
        $this->data['content']='pharmacy/expired_items';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function stock_reconcilliation(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit    
        $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Pharmacy/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Pharmacy/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Pharmacy/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Pharmacy/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Pharmacy/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Pharmacy/stock_taking_report'),
                           array('title'=>'Requesition Orders','link'=>'Pharmacy/requesition_orders_report'),
                        );
        
            if($this->input->post('category')) {
                $key['category'] = $this->input->post('category');
                $this->data['category']=$this->input->post('category');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['category'] = $exp[2];
            $key['item'] = $exp[3];
            $this->data['category']=$key['category'];
            $this->data['item']=$key['item'];
        }

        
        $unit =$unit[0]->inventoryunit;
        $this->data['unit']=$unit;
        $category =$key['category'];
        $item =$key['item'];
        $data=$this->Inventory_model->stock_reconcilliation($unit,$category,$item);

        if($docType == 1){
           
            require_once 'reports/inventory/stock_reconcilliation_pdf.php';
        }
        
        if($docType == 2){
            
           
            require_once 'reports/inventory/stock_reconcilliation_excel.php';
        }
        
        $this->data['stock'] =$data;
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $unit_name=$this->Inventory_model->units($unit);
        $this->data['title']=$unit_name[0]->name.' Stock Reconcilliation';
        $this->data['content']='pharmacy/stock_reconcilliation';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function stock_transfer_report(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
       $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit
      $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Pharmacy/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Pharmacy/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Pharmacy/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Pharmacy/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Pharmacy/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Pharmacy/stock_taking_report'),
                           array('title'=>'Requesition Orders','link'=>'Pharmacy/requesition_orders_report'),
                        );
        
        
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        
        $unit=$unit[0]->inventoryunit;
        $this->data['unit']=$unit;
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->stock_tranfers_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_transfers_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->stock_tranfers_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_transfers_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/stock_transfer_report/0_".$unit."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->stock_transfer_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['transfers'] = $this->Inventory_model->stock_transfer_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $unit_name=$this->Inventory_model->units($unit);
        $this->data['title']=$unit_name[0]->name.' Stock Tranfers';
        $this->data['content']='pharmacy/stock_transfers_report';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function stock_taking_report(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit
       $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Pharmacy/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Pharmacy/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Pharmacy/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Pharmacy/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Pharmacy/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Pharmacy/stock_taking_report'),
                           array('title'=>'Requesition Orders','link'=>'Pharmacy/requesition_orders_report'),
                        );
        
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        $unit=$unit[0]->inventoryunit;
        $this->data['unit']=$unit;
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->stock_takings_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_takings_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->stock_takings_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_takings_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/stock_taking_report/0_".$unit."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->stock_taking_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['takings'] = $this->Inventory_model->stock_taking_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $unit_name=$this->Inventory_model->units($unit);
        $this->data['title']=$unit_name[0]->name.' Stock Takings';
        $this->data['content']='pharmacy/stock_takings_report';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function requesition_orders_report(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
       $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit    
       $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Pharmacy/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Pharmacy/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Pharmacy/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Pharmacy/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Pharmacy/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Pharmacy/stock_taking_report'),
                           array('title'=>'Requesition Orders','link'=>'Pharmacy/requesition_orders_report'),
                        );
        
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
            
            if($this->input->post('status')) {
                $key['status'] = $this->input->post('status');
                $this->data['status']=$this->input->post('status');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            $key['status'] = $exp[5];
            
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
            $this->data['status']=$key['status'];
        }

        $unit =$unit[0]->inventoryunit;
        $this->data['unit']=$unit;
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];
        $status =$key['status'];

        if($docType == 1){
            
            $data= $this->Inventory_model->stock_requesition_orders_report($unit,$item,$start,$end,$status);
            require_once 'reports/inventory/stock_requesition_orders_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->stock_requesition_orders_report($unit,$item,$start,$end,$status);
            require_once 'reports/inventory/stock_requesition_orders_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Pharmacy/requesition_orders_report/0_".$key['unit']."_".$key['item']."_".$key['start']."_".$key['end']."_".$key['status']."/";
        $config["total_rows"] =$this->Inventory_model->stock_requesition_orders_info_count($unit,$item,$start,$end,$status);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['orders'] = $this->Inventory_model->stock_requesition_orders_info($unit,$item,$start,$end,$status,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $unit_name=$this->Inventory_model->units($unit);
        $this->data['title']=$unit_name[0]->name.' Stock Requesition Orders';
        $this->data['content']='pharmacy/stock_requesition_orders_report';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function process_requesition_order($reqid,$itemid,$action){
        if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
               $unit=$this->Administration_model->inventory_unit_users($this->session->userdata('user_id'));//getting the exact user unit
               $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Pharmacy/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Pharmacy/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Pharmacy/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Pharmacy/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Pharmacy/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Pharmacy/stock_taking_report'),
                           array('title'=>'Requesition Orders','link'=>'Pharmacy/requesition_orders_report'),
                        );
        
                if($action == 2){
                    $data=array(
                        'modifiedby'=>$this->session->userdata('user_id'),
                        'modifiedon'=>date('Y-m-d H:i:s'),
                        'status'=>2,
                    );
                    
                    $this->Inventory_model->process_requesition_order($data,$reqid,$itemid);
                    redirect('Pharmacy/requesition_orders_report','refresh');
                }
                
                    $this->form_validation->set_rules('r_unit','R/Inventory Unit','required|xss_clean');
                    $this->form_validation->set_rules('itemid','Item','xss_clean|required');
                    $this->form_validation->set_rules('s_qty','S/Quantity','xss_clean|numeric|required');
                    
                    if ($this->form_validation->run() == TRUE){
                        $itemid=$this->input->post('itemid');
                        $destination=$this->input->post('r_unit');
                        $quantity=$this->input->post('s_qty');
                        $reqid=$this->input->post('reqid');
                        
                        $available_stock=$this->Inventory_model->inventory_stock(null,$itemid,$quantity);

                        if($available_stock <> null){
                            foreach($available_stock as $ky=>$val){

                                $stocking=array(
                                    'itemid'=>$itemid,
                                    'batch'=>$val['batch'],
                                    'storebatch'=>$val['storebatch'],
                                    'expiredate'=>$val['expiredate'],
                                    'supplierid'=>$val['supplierid'],
                                    'quantity'=>$val['quantity'],
                                    'totalvalue'=>$val['totalvalue'],
                                    'unitprice'=>$val['unitprice'],
                                    'inventoryunit'=>$destination,
                                    'status'=>'STOCKING',
                                    'requesitionid'=>$reqid,
                                    'lastupdate'=>Date('Y-m-d H:i:s'),
                                    'userid'=>$this->session->userdata('user_id'),
                                );

                                $destocking=array(
                                    'itemid'=>$itemid,
                                    'batch'=>$val['batch'],
                                    'storebatch'=>$val['storebatch'],
                                    'expiredate'=>$val['expiredate'],
                                    'supplierid'=>$val['supplierid'],
                                    'quantity'=>-$val['quantity'],
                                    'totalvalue'=>$val['totalvalue'],
                                    'unitprice'=>$val['unitprice'],
                                    'status'=>'TRANSFER',
                                    'inventoryunit'=>$unit[0]->inventoryunit,
                                    'requesitionid'=>$reqid,
                                    'lastupdate'=>Date('Y-m-d H:i:s'),
                                    'userid'=>$this->session->userdata('user_id'),
                                );

                                $sv=$this->Inventory_model->process_stocking_destocking($stocking,$destocking);

                                if($sv){
                                    $error=FALSE;
                                }
                            }
                            
                            if(!$error){
                                
                                $data=array(
                                        'modifiedby'=>$this->session->userdata('user_id'),
                                        'modifiedon'=>date('Y-m-d H:i:s'),
                                        'supplyqty'=>$quantity,
                                        'status'=>1,
                                    );
                                
                                $this->Inventory_model->process_requesition_order($data,$reqid,$itemid);
                                redirect('Pharmacy/requesition_orders_report','refresh');
                            }
                            
                        }else{
                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">no enough stock available</div>'; 
                        }
                    }
                    
                    
                
        $this->data['order'] = $this->Inventory_model->stock_requesition_order($reqid,$itemid);
        $this->data['reqid'] = $reqid;
        $this->data['itemid'] = $itemid;
        $this->data['action'] = $action;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Process Stock Requesition Order';
        $this->data['content']='pharmacy/process_requesition_order';
        $this->load->view('pharmacy/template',$this->data);
    }
    
    public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Pharmacy/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Pharmacy/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['content']='pharmacy/profile';
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['profile']=$this->SuperAdministration_model->profile_data();
		$this->load->view('pharmacy/template',$this->data);
    }
        
    public function edit_profile(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Pharmacy/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Pharmacy/change_password'),
                                        );
            
            $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
            $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
            $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
            $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
            $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if($this->form_validation->run() == true){

                 $firstname=$this->input->post('first_name');
                 $middlename=$this->input->post('middle_name');
                 $lastname=$this->input->post('last_name');
                 $username=$this->input->post('username');
                 $email=$this->input->post('email');
                 $mobile=$this->input->post('mobile');

                $user=array(
                            'FIRST_NAME'=>$firstname,
                            'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                            'LAST_NAME'=>$lastname,
                            'USERNAME'=>$username,
                            'EMAIL'=>$email<>null?$email:null,
                            'MSISDN'=>$mobile,
                        );

                $user_group=array(
                            'GROUP_ID'=>$this->session->userdata('group')
                        );

                $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                if($username_check){


                    //do registration
                     $resp=$this->SuperAdministration_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                       if($resp){

                           redirect('Pharmacy/profile','refresh');
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                       }
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                }
            }

            $this->data['title']="Edit Profile";
            $this->data['content']='pharmacy/edit_profile';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->load->view('pharmacy/template',$this->data);
    }
        
    public function change_password(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Pharmacy/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Pharmacy/change_password'),
                                        );

            $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
            $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 

            if($this->form_validation->run() == true){

                $password=$this->input->post('password');
                $pchange=$this->SuperAdministration_model->change_password($password);

                if($pchange){
                           
                        $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">password change successfull!</div>';
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">password change failed!</div>';
                }
            }

            $this->data['title']="Change Password";
            $this->data['content']='pharmacy/change_password';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->load->view('pharmacy/template',$this->data);
    }
    
    function cur_password($password){
            
            $pf=$this->SuperAdministration_model->profile_data();
            $hashed_old=$this->SuperAdministration_model->hash_db_password($password,$pf);
           
             if($pf->password <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
    }
    
    function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('Pharmacist')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
    
    function username_uniqueness($username,$id){
            
        return $this->SuperAdministration_model->check_username($username,$id);
    }
    
    function get_requestionid($institutionid,$invunit,$supplyunit){
        
        while(true){
            
            $reqid=$institutionid.'-'.$invunit.'-'.$supplyunit.'-'.date('is');
            
            $check=$this->Pharmacy_model->check_requesitionid($reqid);
            
            if($check == null){
                break;
            }
        }
        
        return $reqid;
    }
    
    function patientid($id){
            $patient=$this->Reception_model->patient($id);
           
             if($patient == null){
                 
                 $this->form_validation->set_message("patientid","The %s is incorrect");
                  return FALSE;
             }
            return TRUE;
    }
    
    function start($date){
        
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('start', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('start', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
    }
    
    function end($date){
        
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
    }
}