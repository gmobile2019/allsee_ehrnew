<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {

    function __construct() {
        parent::__construct();
       
	$this->load->model('Inventory_model');	
		
        
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Home';
            $this->data['content']='inventory/dashboard';
            $this->load->view('inventory/template3',$this->data);
    }
    
    public function view_units(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'View Units','link'=>'Inventory/view_units'),
                                   array('title'=>'Add Unit','link'=>'Inventory/add_unit'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('category')) {
            $key['category'] = $this->input->post('category');
            $this->data['category']=$this->input->post('category');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['category'] = $exp[3];
            $this->data['category']=$key['category'];

        }

        $name =$key['name'];
        $category =$key['category'];

        $config["base_url"] = base_url() . "index.php/Inventory/view_units/name_".$key['name']."_category_".$key['category']."/";
        $config["total_rows"] =$this->Inventory_model->units_info_count($name,$category);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['units'] = $this->Inventory_model->units_info($name,$category,$page,$limit);
        
        $this->data['unit_categories']=$this->Inventory_model->active_unit_categories();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='View Units';
        $this->data['content']='inventory/view_units';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function add_unit($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                        array('title'=>'View Units','link'=>'Inventory/view_units'),
                                        array('title'=>'Add Unit','link'=>'Inventory/add_unit'),
                                       );
           
           $this->form_validation->set_rules('category','Category','required|xss_clean');
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('description','Description','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {


                $category=$this->input->post('category');
                $name=$this->input->post('name');
                $description=nl2br(trim($this->input->post('description')));

                

                       $unit=array(
                           'category'=>$category,
                           'name'=>$name,
                           'description'=>$description
                       );


                       //code uniqueness
                       $name_check=$this->Inventory_model->unit_name_check($name,$id);
                       if($name_check == null){


                           //do saving
                            $resp=$this->Inventory_model->save_unit($unit,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">unit name exists already!</div>';
                       }
               
           }

           $this->data['title']="Add Unit";
           $this->data['id']="$id";
           $this->data['unit']=$this->Inventory_model->units($id);
            $this->data['unit_categories']=$this->Inventory_model->active_unit_categories();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='inventory/add_unit';
           $this->load->view('inventory/template',$this->data);
    }
    
    public function activate_deactivate_unit($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Inventory_model->activate_deactivate_unit($status,$id);
        redirect('Inventory/view_units','refresh');
    }
    
    public function view_items($code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'View Items','link'=>'Inventory/view_items'),
                                   array('title'=>'Add Item','link'=>'Inventory/add_item'),
                                   array('title'=>'Drug Types','link'=>'Inventory/drug_types'),
                                   array('title'=>'Drug Groups','link'=>'Inventory/drug_groups'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if($this->uri->segment(4)) {
            $exp = explode("_", $this->uri->segment(4));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];
            
            $docType = $exp[7];
        }
        
        
        
        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        if($docType == 1){
            $data=$this->Inventory_model->inventory_items($code,$name,$type,$grp);
            require_once 'reports/inventory/inventory_items_pdf.php';
        }

        if($docType == 2){
            
             $data=$this->Inventory_model->inventory_items($code,$name,$type,$grp);
             require_once 'reports/inventory/inventory_items_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Inventory/view_items/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =5;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['items_view']='inventory/items';
        $this->data['title']='View Items';
        $this->data['content']='inventory/view_items';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function add_item($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

            $this->data['side_menu']=array(
                                   array('title'=>'View Items','link'=>'Inventory/view_items'),
                                   array('title'=>'Add Item','link'=>'Inventory/add_item'),
                                   array('title'=>'Drug Types','link'=>'Inventory/drug_types'),
                                   array('title'=>'Drug Groups','link'=>'Inventory/drug_groups'),
                                );
           
            
           $this->form_validation->set_rules('additionType','Addition Type','required|xss_clean');
           $this->form_validation->set_rules('consumptioncategory','Consumption Category','required|xss_clean');
           $this->form_validation->set_rules('category','Category','required|xss_clean');
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           
           if($this->input->post('category') == $this->config->item('drug_code')){
               
             $this->form_validation->set_rules('drug_type','Drug Type','xss_clean|required');
             $this->form_validation->set_rules('drug_group','Drug Group','xss_clean|required');  
           }
           
           if ($this->form_validation->run() == TRUE)
           {
               $error=TRUE;
                if($this->input->post('additionType') == 1){
                    
                        $category=$this->input->post('category');
                        $name=$this->input->post('name');
                        $consumptioncategory=$this->input->post('consumptioncategory');
                        
                        $item=array(
                           'category'=>$category,
                           'name'=>$name,
                           'consumptioncategory'=>$consumptioncategory,
                        );

                       //code uniqueness
                       $name_check=$this->Inventory_model->item_name_check($name,$id);
                       if($name_check == null){

                           if($this->input->post('category') == $this->config->item('drug_code')){
                                
                                        $item['drug_group']=$this->input->post('drug_group');
                                        $item['drug_type']=$this->input->post('drug_type');
                            }
                           //do saving
                            $resp=$this->Inventory_model->save_item($item,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">item name exists already!</div>';
                       }
                }else{
                        
                    $config['upload_path'] = './excel/';
                    $config['allowed_types'] = 'xls';
                    $filename=explode('.',$_FILES['upload']['name']);
                    $excel_ext=end($filename);
                    $excelname=date('YmdHis');
                    $filename='InventoryItems'.$excelname.".".$excel_ext;
                    $config['file_name']=$filename;
                    $config['overwrite']=true;
                    $this->upload->initialize($config);
                      
                    if($this->upload->do_upload('upload')){

                        $this->excel_reader->read('./excel/'.$filename);
                        $data = $this->excel_reader->worksheets[0];


                        //loop through the worksheet
                        foreach($data as $value){

                            $id=$this->Inventory_model->get_inventory_item_by_name($value[0]);
                            
                             $item=array(
                                        'name'=>$value[0],
                                        'category'=>$this->input->post('category'),
                                     );
                             
                            if($this->input->post('category') == $this->config->item('drug_code')){
                                
                                        $item['drug_group']=$this->input->post('drug_group');
                                        $item['drug_type']=$this->input->post('drug_type');
                            }
                            //save item details
                            $resp=$this->Administration_model->save_item($item,$id->id); 
                            if($resp){
                                
                                $error=FALSE;
                            }
                        }
                        
                        if($error == FALSE){
                            
                            redirect(current_url());
                        }
                    }else{
                        
                        $this->data['message']=$this->upload->display_errors('<div class="alert alert-danger " role="alert" style="text-align:center;">','</div>');
                       
                    }
                }
           }

           $this->data['title']="Add Item";
           $this->data['id']="$id";
           $this->data['item']=$this->Inventory_model->items($id);
           $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
           $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
           $this->data['item_categories']=$this->Inventory_model->active_item_categories();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='inventory/add_item';
           $this->load->view('inventory/template',$this->data);
    }
    
    public function activate_deactivate_item($id,$status,$code){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Inventory_model->activate_deactivate_item($status,$id);
        redirect('Inventory/view_items/'.$code,'refresh');
    }
    
    public function drug_types(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'View Items','link'=>'Inventory/view_items'),
                                   array('title'=>'Add Item','link'=>'Inventory/add_item'),
                                   array('title'=>'Drug Types','link'=>'Inventory/drug_types'),
                                   array('title'=>'Drug Groups','link'=>'Inventory/drug_groups'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
        }

        $name =$key['name'];

        $config["base_url"] = base_url() . "index.php/Inventory/drug_types/name_".$key['name']."/";
        $config["total_rows"] =$this->Inventory_model->drug_types_info_count($name);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['drug_types'] = $this->Inventory_model->drug_types_info($name,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Drug Types';
        $this->data['content']='inventory/drug_types';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function drug_groups(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'View Items','link'=>'Inventory/view_items'),
                                   array('title'=>'Add Item','link'=>'Inventory/add_item'),
                                   array('title'=>'Drug Types','link'=>'Inventory/drug_types'),
                                   array('title'=>'Drug Groups','link'=>'Inventory/drug_groups'),
                                );
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
        }

        $name =$key['name'];

        $config["base_url"] = base_url() . "index.php/Inventory/drug_groups/name_".$key['name']."/";
        $config["total_rows"] =$this->Inventory_model->drug_groups_info_count($name);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['drug_groups'] = $this->Inventory_model->drug_groups_info($name,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Drug Groups';
        $this->data['content']='inventory/drug_groups';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function add_drug_type($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                   array('title'=>'View Items','link'=>'Inventory/view_items'),
                                   array('title'=>'Add Item','link'=>'Inventory/add_item'),
                                   array('title'=>'Drug Types','link'=>'Inventory/drug_types'),
                                   array('title'=>'Drug Groups','link'=>'Inventory/drug_groups'),
                                );
           
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('description','Description','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {
                $name=$this->input->post('name');
                $description=nl2br(trim($this->input->post('description')));

                

                       $drug_type=array(
                           'name'=>$name,
                           'description'=>$description
                       );


                       //code uniqueness
                       $name_check=$this->Inventory_model->drug_type_name_check($name,$id);
                       if($name_check == null){


                           //do saving
                            $resp=$this->Inventory_model->save_drug_type($drug_type,$id);

                              if($resp){

                                  redirect('Inventory/drug_types','refresh');
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">drug type name exists already!</div>';
                       }
               
           }

           $this->data['title']="Add Drug Type";
           $this->data['id']="$id";
           $this->data['drug_type']=$this->Inventory_model->drug_types($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='inventory/add_drug_type';
           $this->load->view('inventory/template',$this->data);
    }
    
    public function add_drug_group($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                   array('title'=>'View Items','link'=>'Inventory/view_items'),
                                   array('title'=>'Add Item','link'=>'Inventory/add_item'),
                                   array('title'=>'Drug Types','link'=>'Inventory/drug_types'),
                                   array('title'=>'Drug Groups','link'=>'Inventory/drug_groups'),
                                );
           
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('description','Description','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {


                $name=$this->input->post('name');
                $description=nl2br(trim($this->input->post('description')));

                

                       $drug_group=array(
                           'name'=>$name,
                           'description'=>$description
                       );


                       //code uniqueness
                       $name_check=$this->Inventory_model->drug_group_name_check($name,$id);
                       if($name_check == null){


                           //do saving
                            $resp=$this->Inventory_model->save_drug_group($drug_group,$id);

                              if($resp){

                                  redirect('Inventory/drug_groups','refresh');
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">drug group name exists already!</div>';
                       }
               
           }

           $this->data['title']="Add Drug Group";
           $this->data['id']="$id";
           $this->data['drug_group']=$this->Inventory_model->drug_groups($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='inventory/add_drug_group';
           $this->load->view('inventory/template',$this->data);
    }
    
    public function activate_deactivate_drugType($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Inventory_model->activate_deactivate_drugType($status,$id);
        redirect('Inventory/drug_types','refresh');
    }
    
    public function activate_deactivate_drugGroup($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Inventory_model->activate_deactivate_drugGroup($status,$id);
        redirect('Inventory/drug_groups','refresh');
    }
    
    public function main_stock($code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'Main Stock','link'=>'Inventory/main_stock'),
                                   array('title'=>'Units Stock','link'=>'Inventory/units_stock'),
                                );
        
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(4)) {
            $exp = explode("_", $this->uri->segment(4));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];

        }

        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        $config["base_url"] = base_url() . "index.php/Inventory/main_stock/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =5;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['main_stock_items']='inventory/main_stock_items';
        $this->data['title']='Main Stock';
        $this->data['content']='inventory/main_stock';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function inventory_item_stock_details($itemid,$code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $this->data['side_menu']=array(
                                   array('title'=>'Main Stock','link'=>'Inventory/main_stock'),
                                   array('title'=>'Units Stock','link'=>'Inventory/units_stock'),
                                );
        $this->data['item_inventory_details']=$this->Inventory_model->item_inventory_details($itemid,null);   
        $item_name=$this->Inventory_model->items($itemid);
        $this->data['title']=$item_name[0]->name.' Main Inventory Details';
        $this->data['code']=$code;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='inventory/item_main_inventory_details';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function units_stock($unit,$code){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'Main Stock','link'=>'Inventory/main_stock'),
                                   array('title'=>'Units Stock','link'=>'Inventory/units_stock'),
                                );
        
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(5)) {
            $exp = explode("_", $this->uri->segment(5));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];

        }

        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        $config["base_url"] = base_url() . "index.php/Inventory/main_stock/$unit/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =6;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['unit']=$unit;
        $this->data['units_stock_items']='inventory/units_stock_items';
        $this->data['title']='Units Stock';
        $this->data['content']='inventory/units_stock';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function inventory_unit_item_stock_details($itemid,$code,$unit){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $this->data['side_menu']=array(
                                   array('title'=>'Main Stock','link'=>'Inventory/main_stock'),
                                   array('title'=>'Units Stock','link'=>'Inventory/units_stock'),
                                );
        $this->data['item_inventory_details']=$this->Inventory_model->item_inventory_details($itemid,$unit);   
        $item_name=$this->Inventory_model->items($itemid);
        $unit_name=$this->Inventory_model->units($unit);
        $this->data['title']=$item_name[0]->name.' '.$unit_name[0]->name.' Inventory Details';
        $this->data['code']=$code;
        $this->data['unit']=$unit;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='inventory/item_unit_inventory_details';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function stock_transfer($code){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'Stock Transfer','link'=>'Inventory/stock_transfer'),
                                   array('title'=>'Stock Taking','link'=>'Inventory/stock_receiving'),
                                   array('title'=>'Stock Dispose','link'=>'Inventory/stock_dispose'),
                                   array('title'=>'Purchase Orders','link'=>'Inventory/purchase_orders'),
                                   array('title'=>'Request Orders','link'=>'Inventory/request_orders'),
                                );
        
        //process form if button pressed
        if($this->input->post('save')){
            
            $stocktransfer=$this->input->post('stocktransfer');
            
            if(count($stocktransfer) > 0){
                
                foreach($stocktransfer as $value){
                    $error=TRUE;
                    $this->form_validation->set_rules('stocktransfer['.$value.']','Confirm','required|xss_clean');
                    $this->form_validation->set_rules('destination_'.$value,'Destination','required|xss_clean');
                    $this->form_validation->set_rules('quantity_'.$value,'Quantity','xss_clean|numeric|required');

                    if ($this->form_validation->run() == TRUE){

                        $itemid=$this->input->post('itemid_'.$value);
                        $destination=$this->input->post('destination_'.$value);
                        $quantity=$this->input->post('quantity_'.$value);
                        $available_stock=$this->Inventory_model->inventory_stock(null,$itemid,$quantity);

                        if($available_stock <> null){
                            foreach($available_stock as $ky=>$val){

                                $stocking=array(
                                    'itemid'=>$itemid,
                                    'batch'=>$val['batch'],
                                    'storebatch'=>$val['storebatch'],
                                    'expiredate'=>$val['expiredate'],
                                    'supplierid'=>$val['supplierid'],
                                    'quantity'=>$val['quantity'],
                                    'totalvalue'=>$val['totalvalue'],
                                    'unitprice'=>$val['unitprice'],
                                    'inventoryunit'=>$destination,
                                    'status'=>'STOCKING',
                                    'lastupdate'=>Date('Y-m-d H:i:s'),
                                    'userid'=>$this->session->userdata('user_id'),
                                );

                                $destocking=array(
                                    'itemid'=>$itemid,
                                    'batch'=>$val['batch'],
                                    'storebatch'=>$val['storebatch'],
                                    'expiredate'=>$val['expiredate'],
                                    'supplierid'=>$val['supplierid'],
                                    'quantity'=>-$val['quantity'],
                                    'totalvalue'=>$val['totalvalue'],
                                    'unitprice'=>$val['unitprice'],
                                    'status'=>'TRANSFER',
                                    'lastupdate'=>Date('Y-m-d H:i:s'),
                                    'userid'=>$this->session->userdata('user_id'),
                                );
                                
                                $sv=$this->Inventory_model->process_stocking_destocking($stocking,$destocking);

                                if($sv){
                                    $error=FALSE;
                                }
                            }
                        }else{
                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">no enough stock available</div>'; 
                        }
                    }
                }

                if(!$error){
                    redirect(current_url());
                }
            }else{
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">please confirm atleast one transfer!</div>';
            }
            
        }
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(4)) {
            $exp = explode("_", $this->uri->segment(4));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];

        }

        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        $config["base_url"] = base_url() . "index.php/Inventory/stock_transfer/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =5;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['units']=$this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['transfer_items']='inventory/transfer_items';
        $this->data['title']='Stock Transfer';
        $this->data['content']='inventory/stock_transfer';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function stock_receiving($code){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->data['side_menu']=array(
                                   array('title'=>'Stock Transfer','link'=>'Inventory/stock_transfer'),
                                   array('title'=>'Stock Taking','link'=>'Inventory/stock_receiving'),
                                   array('title'=>'Stock Dispose','link'=>'Inventory/stock_dispose'),
                                   array('title'=>'Purchase Orders','link'=>'Inventory/purchase_orders'),
                                   array('title'=>'Request Orders','link'=>'Inventory/request_orders'),
                                );
        
        //process form if button pressed
        if($this->input->post('save')){
            
            $stocktaking=$this->input->post('stocktaking');
            if(count($stocktaking) > 0){
                foreach($stocktaking as $value){
                $error=TRUE;
                $this->form_validation->set_rules('stocktaking['.$value.']','Supplier','required|xss_clean');
                $this->form_validation->set_rules('supplier_'.$value,'Supplier','required|xss_clean');
                $this->form_validation->set_rules('expire_'.$value,'Expire Date','xss_clean|required');
                $this->form_validation->set_rules('batch_'.$value,'Batch','xss_clean|required');//|callback_check_store_batch['.$value.']
                $this->form_validation->set_rules('totalvalue_'.$value,'T/Value','xss_clean|required');
                $this->form_validation->set_rules('quantity_'.$value,'Quantity','xss_clean|numeric|required');
                
                if ($this->form_validation->run() == TRUE){
                    
                    $itemid=$this->input->post('itemid_'.$value);
                    $supplier=$this->input->post('supplier_'.$value);
                    $expire=$this->input->post('expire_'.$value);
                    $storebatch=trim($this->input->post('batch_'.$value));
                    $totalvalue=$this->input->post('totalvalue_'.$value);
                    $quantity=$this->input->post('quantity_'.$value);
                    $batch=$this->inventory_batch();
                    
                    $check=$this->Inventory_model->check_store_batch($storebatch);
           
                    if($check <> null){

                        $this->form_validation->set_message("batch_".$value,"The batch exists");
                         continue;
                    }
                    
                    $stocking=array(
                        'itemid'=>$itemid,
                        'batch'=>$batch,
                        'storebatch'=>$storebatch,
                        'expiredate'=>$expire,
                        'supplierid'=>$supplier,
                        'totalvalue'=>$totalvalue,
                        'quantity'=>$quantity,
                        'unitprice'=>((float)ceil($totalvalue/$quantity)),
                        'status'=>'STOCKING',
                        'lastupdate'=>Date('Y-m-d H:i:s'),
                        'userid'=>$this->session->userdata('user_id'),
                    );
                    
                    $sv=$this->Inventory_model->process_stocking_destocking($stocking,null);
                    
                        if($sv){
                            $error=FALSE;
                        }
                    }
                }

                if(!$error){
                    redirect(current_url());
                }
            }else{
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">please confirm atleast one transfer!</div>';
            }
            
        }
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
         
        if ($this->input->post('drug_grp')) {
            $key['drug_grp'] = $this->input->post('drug_grp');
            $this->data['drug_grp']=$this->input->post('drug_grp');
        }
        
        if ($this->input->post('drug_type')) {
            $key['drug_type'] = $this->input->post('drug_type');
            $this->data['drug_type']=$this->input->post('drug_type');
        }
         
            
            
        if ($this->uri->segment(4)) {
            $exp = explode("_", $this->uri->segment(4));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['drug_grp'] = $exp[3];
            $this->data['drug_grp']=$key['drug_grp'];
            
            $key['drug_type'] = $exp[5];
            $this->data['drug_type']=$key['drug_type'];

        }

        $name =$key['name'];
        $grp =$key['drug_grp'];
        $type =$key['drug_type'];
        
        $config["base_url"] = base_url() . "index.php/Inventory/stock_receiving/$code/name_".$key['name']."_grp_".$key['drug_grp']."_type_".$key['drug_type']."/";
        $config["total_rows"] =$this->Inventory_model->items_info_count($code,$name,$type,$grp);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =5;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['items'] = $this->Inventory_model->items_info($code,$name,$type,$grp,$page,$limit);
       
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['drug_types'] = $this->Inventory_model->active_drug_types();
        $this->data['drug_groups'] = $this->Inventory_model->active_drug_groups();
        $this->data['suppliers']=$this->Inventory_model->active_suppliers(null,$code);
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['code']=$code;
        $this->data['receive_items']='inventory/receive_items';
        $this->data['title']='Stock Taking';
        $this->data['content']='inventory/stock_receiving';
        $this->load->view('inventory/template',$this->data);
    }
    
     public function stock_dispose(){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
                $this->data['side_menu']=array(
                                   array('title'=>'Stock Transfer','link'=>'Inventory/stock_transfer'),
                                   array('title'=>'Stock Taking','link'=>'Inventory/stock_receiving'),
                                   array('title'=>'Stock Dispose','link'=>'Inventory/stock_dispose'),
                                   array('title'=>'Purchase Orders','link'=>'Inventory/purchase_orders'),
                                   array('title'=>'Request Orders','link'=>'Inventory/request_orders'),
                                );
        
        
                $this->form_validation->set_rules('source','Source','required|xss_clean');
                if($this->input->post('source') == 2){
                    $this->form_validation->set_rules('unit','Unit','required|xss_clean');
                }
                
                $this->form_validation->set_rules('item_category','Item Category','required|xss_clean');
                $this->form_validation->set_rules('item','Item','required|xss_clean');
                $this->form_validation->set_rules('batch','Batch','required|xss_clean');
                $this->form_validation->set_rules('reason','Reason','xss_clean|required');
                $this->form_validation->set_rules('quantity','Quantity','xss_clean|required|numeric|callback_check_positive_integer');
                
                if ($this->form_validation->run() == TRUE){
                    $error=FALSE;
                    $itemid=$this->input->post('item');
                    $batch=$this->input->post('batch');
                    $unit=$this->input->post('source') == 2?$this->input->post('unit'):null;
                    $reason=$this->input->post('reason');
                    $quantity=$this->input->post('quantity');
                    $batch_details=$this->Inventory_model->get_batch_details($batch);
                    
                    if($reason == $this->config->item('expiry_reason')){

                        $error=date('Y-m-d') < $batch_details->expiredate?2:FALSE;
                    }
                    
                    $check=$this->Inventory_model->check_disposal($itemid,$batch,$unit);
                   
                    if($check->qty == null){
                        $error=3;
                    }else if($check->qty < $quantity){
                        $error=4;
                    }else{
                        $error=FALSE;
                    }
                    
                    if($error == FALSE){
                        
                        $stocking=array(
                        'itemid'=>$itemid,
                        'batch'=>$batch_details->batch,
                        'storebatch'=>$batch,
                        'expiredate'=>$batch_details->expiredate,
                        'supplierid'=>$batch_details->supplierid,
                        'totalvalue'=>ceil($batch_details->unitprice*$quantity),
                        'quantity'=>-$quantity,
                        'inventoryunit'=>$unit,
                        'unitprice'=>$batch_details->unitprice,
                        'status'=>'DISPOSE',
                        'lastupdate'=>Date('Y-m-d H:i:s'),
                        'userid'=>$this->session->userdata('user_id'),
                        );
                    
                        $dispose=array(
                            'itemid'=>$itemid,
                            'systembatch'=>$batch_details->batch,
                            'storebatch'=>$batch,
                            'reason'=>$reason,
                            'quantity'=>$quantity,
                            'source'=>$unit,
                            'actiondate'=>Date('Y-m-d H:i:s'),
                            'userid'=>$this->session->userdata('user_id'),
                        );
                        
                        
                        $sv=$this->Inventory_model->process_stock_disposal($stocking,$dispose);
                    
                        if($sv){
                            
                           redirect(current_url());
                        }
                        
                        $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">operation failed</div>';
                    }
                    
                    $name=$this->Inventory_model->items($itemid);
                    
                    if($error == 2){
                        
                        $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">'.$name[0]->name.' is yet to expire('.$batch_details->expiredate.')</div>';
                    }
                    
                    if($error == 3){
                        
                        $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">'.$name[0]->name.' stock is zero</div>';
                    }
                    
                    if($error == 4){
                        
                        $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">'.$name[0]->name.' quantity is greater than the existing quantity('.$check->qty.')</div>';
                    }
            }
        
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['reasons'] = $this->Administration_model->active_dispose_reasons();
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['items'] = $this->Inventory_model->active_items();
        $this->data['batches'] = $this->Inventory_model->batches();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Stock Disposing';
        $this->data['content']='inventory/stock_disposing';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function suppliers(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                   array('title'=>'View Suppliers','link'=>'Inventory/suppliers'),
                                   array('title'=>'Add Supplier','link'=>'Inventory/add_supplier'),
                                );
            if($this->input->post('name')) {
                $key['name'] = $this->input->post('name');
                $this->data['name']=$this->input->post('name');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
        }

        $name =$key['name'];

        $config["base_url"] = base_url() . "index.php/Inventory/suppliers/name_".$key['name']."/";
        $config["total_rows"] =$this->Inventory_model->suppliers_info_count($name);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['suppliers'] = $this->Inventory_model->suppliers_info($name,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Inventory Suppliers';
        $this->data['content']='inventory/suppliers';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function add_supplier($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                   array('title'=>'View Suppliers','link'=>'Inventory/suppliers'),
                                   array('title'=>'Add Supplier','link'=>'Inventory/add_supplier'),
                                );
           
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('phone','Phone','xss_clean|required');
           $this->form_validation->set_rules('physical','Physical Address','xss_clean|required');
           $this->form_validation->set_rules('email','Email','xss_clean|valid_email');
           $this->form_validation->set_rules('category','Category','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {


                $name=$this->input->post('name');
                $phone=$this->input->post('phone');
                $physical=$this->input->post('physical');
                $email=$this->input->post('email')<>null?$this->input->post('email'):null;
                $category=$this->input->post('category')<>null?$this->input->post('category'):null;

                

                       $supplier=array(
                           'name'=>$name,
                           'phone'=>$phone,
                           'email'=>$email,
                           'physicaladdress'=>$physical,
                           'category'=>$category
                       );


                       //name uniqueness
                       $name_check=$this->Inventory_model->supplier_name_check($name,$id);
                       if($name_check == null){


                           //do saving
                            $resp=$this->Inventory_model->save_supplier($supplier,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">supplier name exists already!</div>';
                       }
               
           }

           $this->data['title']="Add Supplier";
           $this->data['id']="$id";
           $this->data['item_categories']=$this->Inventory_model->active_item_categories();
           $this->data['supplier']=$this->Inventory_model->suppliers($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='inventory/add_supplier';
           $this->load->view('inventory/template',$this->data);
    }
    
    public function activate_deactivate_supplier($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Inventory_model->activate_deactivate_supplier($status,$id);
        redirect('Inventory/suppliers','refresh');
    }
    
    public function expired_items(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
       $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Inventory/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Inventory/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Inventory/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Inventory/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Inventory/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Inventory/stock_taking_report'),
                           array('title'=>'Purchase Orders','link'=>'Inventory/purchase_orders_report'),
                           array('title'=>'Requesition Orders','link'=>'Inventory/requesition_orders_report'),
                        );
        
        $segment=$this->uri->segment(3);
        $seg=explode('_',$segment);
        $docType=$seg[0];
        $category=$seg[1];
        $item=$seg[2];
        $unit=$seg[3];
        $start=$seg[4];
        $end=$seg[5];
        
        if($this->input->post('search')){
            
            $unit=$this->input->post('unit');
            $category=$this->input->post('category');
            $item=$this->input->post('item');
            $start=$this->input->post('start');
            $end=$this->input->post('end');
        }
        
        $data=$this->Inventory_model->expired_items($category,$item,$unit,$start,$end);
        
        if($docType == 1){
            
            require_once 'reports/inventory/expired_items_pdf.php';
        }
        
        if($docType == 2){
            
            require_once 'reports/inventory/expired_items_excel.php';
        }
        
        $this->data['category']=$category;
        $this->data['item']=$item;
        $this->data['unit']=$unit;
        $this->data['start']=$start;
        $this->data['end']=$end;
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['expired'] = $data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Expired Items';
        $this->data['content']='inventory/expired_items';
        $this->load->view('inventory/template',$this->data);
    }
   
    public function disposed_items(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Inventory/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Inventory/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Inventory/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Inventory/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Inventory/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Inventory/stock_taking_report'),
                           array('title'=>'Purchase Orders','link'=>'Inventory/purchase_orders_report'),
                           array('title'=>'Requesition Orders','link'=>'Inventory/requesition_orders_report'),
                        );
        
        if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['unit'] = $exp[1];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['unit']=$key['unit'];
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        
        $unit =$key['unit'];
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->disposed_items_report($unit,$item,$start,$end);
            require_once 'reports/inventory/disposed_items_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->disposed_items_report($unit,$item,$start,$end);
            require_once 'reports/inventory/disposed_items_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Inventory/disposed_items/0_".$key['unit']."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->disposed_items_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['disposes'] = $this->Inventory_model->disposed_items_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Disposed Items';
        $this->data['content']='inventory/disposed_items';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function items_consumption(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Inventory/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Inventory/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Inventory/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Inventory/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Inventory/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Inventory/stock_taking_report'),
                           array('title'=>'Purchase Orders','link'=>'Inventory/purchase_orders_report'),
                           array('title'=>'Requesition Orders','link'=>'Inventory/requesition_orders_report'),
                        );
        
        if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['unit'] = $exp[1];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['unit']=$key['unit'];
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        
        $unit =$key['unit'];
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->items_consumption_report($unit,$item,$start,$end);
            require_once 'reports/inventory/items_consumption_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->items_consumption_report($unit,$item,$start,$end);
            require_once 'reports/inventory/items_consumption_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Inventory/items_consumption/0_".$key['unit']."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->items_consumption_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['consumptions'] = $this->Inventory_model->items_consumption_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Items Consumption';
        $this->data['content']='inventory/items_consumption';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function stock_reconcilliation(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Inventory/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Inventory/expired_items'),
                           array('title'=>'Stock Reconcilliation','link'=>'Inventory/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Inventory/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Inventory/stock_taking_report'),
                           array('title'=>'Purchase Orders','link'=>'Inventory/purchase_orders_report'),
                           array('title'=>'Requesition Orders','link'=>'Inventory/requesition_orders_report'),
                        );
        
            if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
            
            if($this->input->post('category')) {
                $key['category'] = $this->input->post('category');
                $this->data['category']=$this->input->post('category');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['unit'] = $exp[1];
            $key['category'] = $exp[2];
            $key['item'] = $exp[3];
            
            $this->data['unit']=$key['unit'];
            $this->data['category']=$key['category'];
            $this->data['item']=$key['item'];
        }

        
        $unit =$key['unit'];
        $category =$key['category'];
        $item =$key['item'];
        $data=$this->Inventory_model->stock_reconcilliation($unit,$category,$item);

        if($docType == 1){
           
            require_once 'reports/inventory/stock_reconcilliation_pdf.php';
        }
        
        if($docType == 2){
            
           
            require_once 'reports/inventory/stock_reconcilliation_excel.php';
        }
        
        $this->data['stock'] =$data;
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Stock Reconcilliation';
        $this->data['content']='inventory/stock_reconcilliation';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function stock_transfer_report(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
       $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Inventory/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Inventory/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Inventory/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Inventory/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Inventory/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Inventory/stock_taking_report'),
                           array('title'=>'Purchase Orders','link'=>'Inventory/purchase_orders_report'),
                           array('title'=>'Requesition Orders','link'=>'Inventory/requesition_orders_report'),
                        );
        
        
            if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['unit'] = $exp[1];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['unit']=$key['unit'];
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        
        $unit =$key['unit'];
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->stock_tranfers_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_transfers_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->stock_tranfers_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_transfers_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Inventory/stock_transfer_report/0_".$key['unit']."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->stock_transfer_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['transfers'] = $this->Inventory_model->stock_transfer_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Stock Tranfers';
        $this->data['content']='inventory/stock_transfers_report';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function stock_taking_report(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Inventory/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Inventory/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Inventory/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Inventory/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Inventory/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Inventory/stock_taking_report'),
                           array('title'=>'Purchase Orders','link'=>'Inventory/purchase_orders_report'),
                           array('title'=>'Requesition Orders','link'=>'Inventory/requesition_orders_report'),
                        );
        
            if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['unit'] = $exp[1];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            
            $this->data['unit']=$key['unit'];
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
        }

        
        $unit =$key['unit'];
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];

        if($docType == 1){
            $data= $this->Inventory_model->stock_takings_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_takings_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->stock_takings_report($unit,$item,$start,$end);
            require_once 'reports/inventory/stock_takings_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Inventory/stock_taking_report/0_".$key['unit']."_".$key['item']."_".$key['start']."_".$key['end']."/";
        $config["total_rows"] =$this->Inventory_model->stock_taking_info_count($unit,$item,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['takings'] = $this->Inventory_model->stock_taking_info($unit,$item,$start,$end,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['item_categories'] = $this->Inventory_model->active_item_categories();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Stock Takings';
        $this->data['content']='inventory/stock_takings_report';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function requesition_orders_report(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
        $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Inventory/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Inventory/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Inventory/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Inventory/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Inventory/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Inventory/stock_taking_report'),
                           array('title'=>'Purchase Orders','link'=>'Inventory/purchase_orders_report'),
                           array('title'=>'Requesition Orders','link'=>'Inventory/requesition_orders_report'),
                        );
        
            if($this->input->post('unit')) {
                $key['unit'] = $this->input->post('unit');
                $this->data['unit']=$this->input->post('unit');
            }
            
            if($this->input->post('item')) {
                $key['item'] = $this->input->post('item');
                $this->data['item']=$this->input->post('item');
            }
            
            if($this->input->post('start')) {
                $key['start'] = $this->input->post('start');
                $this->data['start']=$this->input->post('start');
            }
            
            if($this->input->post('end')) {
                $key['end'] = $this->input->post('end');
                $this->data['end']=$this->input->post('end');
            }
            
            if($this->input->post('status')) {
                $key['status'] = $this->input->post('status');
                $this->data['status']=$this->input->post('status');
            }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $docType = $exp[0];
            $key['unit'] = $exp[1];
            $key['item'] = $exp[2];
            $key['start'] = $exp[3];
            $key['end'] = $exp[4];
            $key['status'] = $exp[5];
            
            $this->data['unit']=$key['unit'];
            $this->data['item']=$key['item'];
            $this->data['start']=$key['start'];
            $this->data['end']=$key['end'];
            $this->data['status']=$key['status'];
        }

        
        $unit =$key['unit'];
        $item =$key['item'];
        $start =$key['start'];
        $end =$key['end'];
        $status =$key['status'];

        if($docType == 1){
            
            $data= $this->Inventory_model->stock_requesition_orders_report($unit,$item,$start,$end,$status);
            require_once 'reports/inventory/stock_requesition_orders_pdf.php';
        }
        
        if($docType == 2){
            
            $data= $this->Inventory_model->stock_requesition_orders_report($unit,$item,$start,$end,$status);
            require_once 'reports/inventory/stock_requesition_orders_excel.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Inventory/requesition_orders_report/0_".$key['unit']."_".$key['item']."_".$key['start']."_".$key['end']."_".$key['status']."/";
        $config["total_rows"] =$this->Inventory_model->stock_requesition_orders_info_count($unit,$item,$start,$end,$status);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['orders'] = $this->Inventory_model->stock_requesition_orders_info($unit,$item,$start,$end,$status,$page,$limit);
        
        $this->data['units'] = $this->Inventory_model->active_units();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Stock Requesition Orders';
        $this->data['content']='inventory/stock_requesition_orders_report';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function process_requesition_order($reqid,$itemid,$action){
        if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                           array('title'=>'Disposed Items','link'=>'Inventory/disposed_items'),
                           array('title'=>'Expired Items','link'=>'Inventory/expired_items'),
                           array('title'=>'Items Consumption','link'=>'Inventory/items_consumption'),
                           array('title'=>'Stock Reconcilliation','link'=>'Inventory/stock_reconcilliation'),
                           array('title'=>'Stock Transfers','link'=>'Inventory/stock_transfer_report'),
                           array('title'=>'Stock Takings','link'=>'Inventory/stock_taking_report'),
                           array('title'=>'Purchase Orders','link'=>'Inventory/purchase_orders_report'),
                           array('title'=>'Requesition Orders','link'=>'Inventory/requesition_orders_report'),
                        );
        
                if($action == 2){
                    $data=array(
                        'modifiedby'=>$this->session->userdata('user_id'),
                        'modifiedon'=>date('Y-m-d H:i:s'),
                        'status'=>2,
                    );
                    
                    $this->Inventory_model->process_requesition_order($data,$reqid,$itemid);
                    redirect('Inventory/requesition_orders_report','refresh');
                }
                
                    $this->form_validation->set_rules('r_unit','R/Inventory Unit','required|xss_clean');
                    $this->form_validation->set_rules('itemid','Item','xss_clean|required');
                    $this->form_validation->set_rules('s_qty','S/Quantity','xss_clean|numeric|required');
                    
                    if ($this->form_validation->run() == TRUE){
                        $itemid=$this->input->post('itemid');
                        $destination=$this->input->post('r_unit');
                        $quantity=$this->input->post('s_qty');
                        $reqid=$this->input->post('reqid');
                        
                        $available_stock=$this->Inventory_model->inventory_stock(null,$itemid,$quantity);

                        if($available_stock <> null){
                            foreach($available_stock as $ky=>$val){

                                $stocking=array(
                                    'itemid'=>$itemid,
                                    'batch'=>$val['batch'],
                                    'storebatch'=>$val['storebatch'],
                                    'expiredate'=>$val['expiredate'],
                                    'supplierid'=>$val['supplierid'],
                                    'quantity'=>$val['quantity'],
                                    'totalvalue'=>$val['totalvalue'],
                                    'unitprice'=>$val['unitprice'],
                                    'inventoryunit'=>$destination,
                                    'status'=>'STOCKING',
                                    'requesitionid'=>$reqid,
                                    'lastupdate'=>Date('Y-m-d H:i:s'),
                                    'userid'=>$this->session->userdata('user_id'),
                                );

                                $destocking=array(
                                    'itemid'=>$itemid,
                                    'batch'=>$val['batch'],
                                    'storebatch'=>$val['storebatch'],
                                    'expiredate'=>$val['expiredate'],
                                    'supplierid'=>$val['supplierid'],
                                    'quantity'=>-$val['quantity'],
                                    'totalvalue'=>$val['totalvalue'],
                                    'unitprice'=>$val['unitprice'],
                                    'status'=>'TRANSFER',
                                    'requesitionid'=>$reqid,
                                    'lastupdate'=>Date('Y-m-d H:i:s'),
                                    'userid'=>$this->session->userdata('user_id'),
                                );

                                $sv=$this->Inventory_model->process_stocking_destocking($stocking,$destocking);

                                if($sv){
                                    $error=FALSE;
                                }
                            }
                            
                            if(!$error){
                                
                                $data=array(
                                        'modifiedby'=>$this->session->userdata('user_id'),
                                        'modifiedon'=>date('Y-m-d H:i:s'),
                                        'supplyqty'=>$quantity,
                                        'status'=>1,
                                    );
                                
                                $this->Inventory_model->process_requesition_order($data,$reqid,$itemid);
                                redirect('Inventory/requesition_orders_report','refresh');
                            }
                            
                        }else{
                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">no enough stock available</div>'; 
                        }
                    }
                    
                    
                
        $this->data['order'] = $this->Inventory_model->stock_requesition_order($reqid,$itemid);
        $this->data['reqid'] = $reqid;
        $this->data['itemid'] = $itemid;
        $this->data['action'] = $action;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Process Stock Requesition Order';
        $this->data['content']='inventory/process_requesition_order';
        $this->load->view('inventory/template',$this->data);
    }
    
    public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Inventory/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Inventory/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['content']='inventory/profile';
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['profile']=$this->SuperAdministration_model->profile_data();
		$this->load->view('inventory/template',$this->data);
        }
        
    public function edit_profile(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Inventory/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Inventory/change_password'),
                                        );
            
            $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
            $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
            $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
            $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
            $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if($this->form_validation->run() == true){

                 $firstname=$this->input->post('first_name');
                 $middlename=$this->input->post('middle_name');
                 $lastname=$this->input->post('last_name');
                 $username=$this->input->post('username');
                 $email=$this->input->post('email');
                 $mobile=$this->input->post('mobile');

                $user=array(
                            'FIRST_NAME'=>$firstname,
                            'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                            'LAST_NAME'=>$lastname,
                            'USERNAME'=>$username,
                            'EMAIL'=>$email<>null?$email:null,
                            'MSISDN'=>$mobile,
                        );

                $user_group=array(
                            'GROUP_ID'=>$this->session->userdata('group')
                        );

                $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                if($username_check){


                    //do registration
                     $resp=$this->SuperAdministration_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                       if($resp){

                           redirect('Administration/profile','refresh');
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                       }
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                }
            }

            $this->data['title']="Edit Profile";
            $this->data['content']='inventory/edit_profile';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->load->view('inventory/template',$this->data);
    }
        
    public function change_password(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Inventory/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Inventory/change_password'),
                                        );

            $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
            $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 

            if($this->form_validation->run() == true){

                $password=$this->input->post('password');
                $pchange=$this->SuperAdministration_model->change_password($password);

                if($pchange){
                           
                        $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">password change successfull!</div>';
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">password change failed!</div>';
                }
            }

            $this->data['title']="Change Password";
            $this->data['content']='inventory/change_password';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->load->view('inventory/template',$this->data);
    }
    
    function cur_password($password){
            
            $pf=$this->SuperAdministration_model->profile_data();
            $hashed_old=$this->SuperAdministration_model->hash_db_password($password,$pf);
           
             if($pf->password <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
    }
    
    function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('StoreKeeper')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
    
    function username_uniqueness($username,$id){
            
        return $this->SuperAdministration_model->check_username($username,$id);
    }
    
    function modal_supplier_details(){
        $id=$this->input->post('id');
        $supplier=$this->Inventory_model->suppliers($id);
        $data="";
       
        $ct=$supplier[0]->category <> null?$this->Inventory_model->get_item_category_by_code($supplier[0]->category):"";
        $ct=$ct <> null?$ct->name:'All Categories';
        $data .=$supplier[0]->name.'=_'.$supplier[0]->phone.'=_'.$supplier[0]->email.'=_'.$supplier[0]->physicaladdress.'=_'.$ct;
        
        echo $data;
        exit;
    }
    
    function inventory_batch(){
        while(true){
            
            $institution_id=$this->SuperAdministration_model->institution_information();
            $batch  =$institution_id->institution_id;
            $batch .="-".mt_rand(10,99);
            $batch .="-".date('is');
            
            $check=$this->Inventory_model->check_batch($batch);
            
            if($check == null){
                break;
            }
        }
        
        return $batch;
    }
    
    function check_store_batch($batch,$id){
            $check=$this->Inventory_model->check_store_batch($batch);
            if($check <> null){
                 $this->form_validation->set_message("batch_$id","The %s exists");
                  return FALSE;
             }
            
            return TRUE;
    }
    
    function check_positive_integer($qty){
        
             if($qty < 0){
                 
                 $this->form_validation->set_message("quantity","The %s needs to be greater than zero");
                  return FALSE;
             }
             
             return TRUE;
    }
    
    function item_batches(){
        $itemid=$this->input->post('itemid');
        $batches=$this->Inventory_model->batches($itemid);
        $data="";
        foreach($batches as $key=>$value){
            
            if($value->storebatch <> null){
                 $data .=$value->storebatch."||";
            }
           
        }
        
        $data=rtrim($data,"||");
        echo $data;
        exit;
    }
}