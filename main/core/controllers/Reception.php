<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reception extends CI_Controller {

    function __construct() {
        parent::__construct();
       
	$this->load->model('Reception_model');	
	$this->load->model('Inventory_model');	
	$this->load->model('Nursing_model');	
        
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['title']='Home';
            $this->data['content']='reception/dashboard';
            $this->load->view('reception/template3',$this->data);
    }
    
    public function new_patient($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                      array('title'=>'New Patient','link'=>'Reception/new_patient'),
                                      array('title'=>'All Patients','link'=>'Reception/all_patients'),
                                      array('title'=>'Out Patients','link'=>'Reception/out_patients'),
                                      array('title'=>'In Patients','link'=>'Reception/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Reception/walk_ins'),
                                  );
           
           $this->form_validation->set_rules('patientType','Patient Type','xss_clean|required');
           $this->form_validation->set_rules('title','Title','xss_clean|required');
           $this->form_validation->set_rules('name','Full Name','required|xss_clean');
           $this->form_validation->set_rules('gender','Gender','required|xss_clean');
           $this->form_validation->set_rules('phone', 'Phone no', 'required|xss_clean');
           
           if($this->input->post('patientType') == 1){
                
                $this->form_validation->set_rules('marital','Marital Status','xss_clean');
                $this->form_validation->set_rules('tribe','Tribe','required|xss_clean');
                $this->form_validation->set_rules('dob','Date of Birth','required|xss_clean');

                $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
                

                $this->form_validation->set_rules('region','Region','required|xss_clean');
                $this->form_validation->set_rules('district','District','required|xss_clean');
                $this->form_validation->set_rules('street','Street','xss_clean');

                $this->form_validation->set_rules('occupation','Occupation','xss_clean');
           }
           
           
            if ($this->form_validation->run() == TRUE)
           {


                $patientType=$this->input->post('patientType');
                $name=$this->input->post('name');
                $gender=$this->input->post('gender');
                $marital=$this->input->post('marital');
                $tribe=$this->input->post('tribe');
                $dob=$this->input->post('dob');
                $title=$this->input->post('title');
                
                $email=$this->input->post('email');
                $phone=$this->input->post('phone');
                
                $region=$this->input->post('region');
                $district=$this->input->post('district');
                $street=$this->input->post('street');
                
                $occupation=$this->input->post('occupation');
                
                if($id == null){
                    //generate a unique patient id
                    $institution_id=$this->SuperAdministration_model->institution_information();
                    $init=$patientType == 1?'P':'W';
                    $ptid=$this->Reception_model->get_patientid($patientType); 

                    if($ptid <> null){

                        $patientid=$init.$institution_id->institution_id.'-'.$ptid;
                    }
                }else{
                    $patientid=$this->input->post('patientid');
                }
                
                
                       $patient=array(
                           'patientid'=>$patientid<>null?$patientid:null,
                           'name'=>$name,
                           'titleid'=>$title,
                           'genderid'=>$gender,
                           'maritalid'=>$marital,
                           'tribe'=>$tribe,
                           'dob'=>$dob,
                           'email'=>$email<>null?$email:null,
                           'phone'=>$phone,
                           'region'=>$region,
                           'district'=>$district,
                           'street'=>$street,
                           'occupation'=>$occupation,
                           'patienttype'=>$patientType,
                       );
                       
                            //do registration
                            $resp=$this->Reception_model->patient_registration($patient,$id);

                            if($resp){

                                redirect(current_url());
                            }else{
                                
                                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                            }
            }

           $this->data['title']="Register Patient";
           $this->data['id']="$id";
           $this->data['patient']=$this->Reception_model->patients($id);
           $this->data['titles']=$this->Reception_model->active_titles();
           $this->data['genders']=$this->Reception_model->active_genders();
           $this->data['maritals']=$this->Reception_model->active_maritals();
           $this->data['regions']=$this->Reception_model->regions();
           $this->data['districts']=$this->Reception_model->districts();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/register_patient';
           $this->load->view('reception/template',$this->data);
    }
    
    public function all_patients(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'New Patient','link'=>'Reception/new_patient'),
                                      array('title'=>'All Patients','link'=>'Reception/all_patients'),
                                      array('title'=>'Out Patients','link'=>'Reception/out_patients'),
                                      array('title'=>'In Patients','link'=>'Reception/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Reception/walk_ins'),
                                      array('title'=>'Appointments','link'=>'Reception/appointments'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('gender')) {
               $key['gender'] = $this->input->post('gender');
               $this->data['gender']=$this->input->post('gender');
           }
           
           if ($this->input->post('marital')) {
               $key['marital'] = $this->input->post('marital');
               $this->data['marital']=$this->input->post('marital');
           }
           
           if ($this->input->post('phone')) {
               $key['phone'] = $this->input->post('phone');
               $this->data['phone']=$this->input->post('phone');
           }

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['gender'] = $exp[3];
               $this->data['gender']=$key['gender'];
               
               $key['marital'] = $exp[5];
               $this->data['marital']=$key['marital'];
               
               $key['phone'] = $exp[7];
               $this->data['phone']=$key['phone'];
               
               $docType = $exp[9];
               $this->data['docType']=$docType;
           }

           $name =$key['name'];
           $gender =$key['gender'];
           $marital =$key['marital'];
           $phone =$key['phone'];

           if($docType == 1){
            $data=$this->Reception_model->all_patients($name,$gender,$marital,$phone);
            require_once 'reports/reception/all_patients_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->all_patients($name,$gender,$marital,$phone);
                 require_once 'reports/reception/all_patients_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Reception/all_patients/name_".$key['name']."_gender_".$key['gender']."_marital_".$key['marital']."_phone_".$key['phone']."/";
           $config["total_rows"] =$this->Reception_model->patient_info_count($name,$gender,$marital,$phone);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->patient_info($name,$gender,$marital,$phone,$page,$limit);

           $this->data['title']="All Patients";
           $this->data['genders']=$this->Reception_model->genders();
           $this->data['maritals']=$this->Reception_model->maritals();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/all_patients';
           $this->load->view('reception/template',$this->data);
   }
   
    public function walk_ins(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'New Patient','link'=>'Reception/new_patient'),
                                      array('title'=>'All Patients','link'=>'Reception/all_patients'),
                                      array('title'=>'Out Patients','link'=>'Reception/out_patients'),
                                      array('title'=>'In Patients','link'=>'Reception/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Reception/walk_ins'),
                                      array('title'=>'Appointments','link'=>'Reception/appointments'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Reception/walk_ins/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->walk_ins_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->walk_ins_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="Walk In Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/all_walkin_patients';
           $this->load->view('reception/template',$this->data);
   }
   
    public function out_patients(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'New Patient','link'=>'Reception/new_patient'),
                                      array('title'=>'All Patients','link'=>'Reception/all_patients'),
                                      array('title'=>'Out Patients','link'=>'Reception/out_patients'),
                                      array('title'=>'In Patients','link'=>'Reception/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Reception/walk_ins'),
                                      array('title'=>'Appointments','link'=>'Reception/appointments'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Reception/out_patients/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->out_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->out_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="Out Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/all_out_patients';
           $this->load->view('reception/template',$this->data);
   }
   
    public function in_patients(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                      array('title'=>'New Patient','link'=>'Reception/new_patient'),
                                      array('title'=>'All Patients','link'=>'Reception/all_patients'),
                                      array('title'=>'Out Patients','link'=>'Reception/out_patients'),
                                      array('title'=>'In Patients','link'=>'Reception/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Reception/walk_ins'),
                                      array('title'=>'Appointments','link'=>'Reception/appointments'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }
           
           if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
               
               $key['pid'] = $exp[3];
               $this->data['pid']=$key['pid'];
               
               $key['start'] = $exp[5];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[7];
               $this->data['end']=$key['end'];
            }

           $name =$key['name'];
           $pid =$key['pid'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Reception/in_patients/name_".$key['name']."_pid_".$key['pid']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Reception_model->in_patient_info_count($pid,$name,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['patients'] = $this->Reception_model->in_patient_info($pid,$name,$start,$end,$page,$limit);

           $this->data['title']="In Patients";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/all_in_patients';
           $this->load->view('reception/template',$this->data);
   }
   
    public function appointments(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $this->data['side_menu']=array(
                                      array('title'=>'New Patient','link'=>'Reception/new_patient'),
                                      array('title'=>'All Patients','link'=>'Reception/all_patients'),
                                      array('title'=>'Out Patients','link'=>'Reception/out_patients'),
                                      array('title'=>'In Patients','link'=>'Reception/in_patients'),
                                      array('title'=>'Walk In Patients','link'=>'Reception/walk_ins'),
                                      array('title'=>'Appointments','link'=>'Reception/appointments'),
                                  );
            if ($this->input->post('name')) {
                $key['name'] = $this->input->post('name');
                $this->data['name']=$this->input->post('name');
            }
           
            if ($this->input->post('pid')) {
                $key['pid'] = $this->input->post('pid');
                $this->data['pid']=$this->input->post('pid');
            }
            
            if($this->input->post('start')){
                $start=$this->input->post('start');
                $this->data['start']=$start;
            }
            
            if($this->input->post('end')){
                $end=$this->input->post('end');
                $this->data['end']=$end;
            }
            
            if($this->input->post('status')){
                $status=$this->input->post('status');
                $this->data['status']=$status;
            }
            
             if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));
               
               $name = $exp[1];
               $this->data['name']=$name;
               
               $pid = $exp[3];
               $this->data['pid']=$pid;
               
               $start = $exp[5];
               $this->data['start']=$start;
               
               $end = $exp[7];
               $this->data['end']=$end;
               
               $status = $exp[9];
               $this->data['status']=$status;
               
               $docType=$exp[11];
            }

          
            if($docType == 1){
            $data=$this->Reception_model->appointments($name,$pid,$start,$end,$status);
            require_once 'reports/reception/appointments_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->all_patients($name,$pid,$start,$end);
                 require_once 'reports/reception/appointments_excel.php';
            }
            
            $config["base_url"] = base_url() . "index.php/Reception/appointments/name_".$name."_pid_".$pid."_start_".$start."_end_".$end."_status_".$status."/";
            $config["total_rows"] =$this->Reception_model->appointments_info_count($name,$pid,$start,$end,$status);
            $config["per_page"] =10;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =6;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['appointments'] = $this->Reception_model->appointments_info($name,$pid,$start,$end,$status,$page,$limit);
           
            $this->data['title']='Patients Appointments';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='reception/appointments';
            $this->load->view('reception/template',$this->data);
    }
    
    public function register_patient_visit($id,$appointment){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

            $patient=$this->Reception_model->patients($id);
            if($patient[0]->patienttype == $this->config->item('walkin_patient_id')){
                
                   //generate a unique patient id
                    $institution_id=$this->SuperAdministration_model->institution_information();
                    $init='W';
                    $ptvid=$this->Reception_model->get_patientvisitid($patient[0]->patienttype); 

                    if($ptvid <> null){

                        $patientvisitid=$init.$institution_id->institution_id.'-'.$ptvid;
                    }
                    
                $patient_visit=array(
                                'patientid'=>$patient[0]->patientid,
                                'patientvisitid'=>$patientvisitid,
                                'patientcategory'=>$patient[0]->patienttype,
                                'visitcategory'=>$this->config->item('OPD_visit_category_id'),
                                'sponsorid'=>$this->config->item('instant_cash_code'),
                                'createdby'=>$this->session->userdata('user_id'),
                             );
                
                    //do registration
                    $resp=$this->Reception_model->save_patient_visit($patient_visit);

                    if($resp){


                        redirect('Reception/walk_ins','refresh');
                    }else{

                        $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                    }
                
            }
           
           $this->form_validation->set_rules('visit_category','Visit Category','xss_clean|required');
           $this->form_validation->set_rules('consultationcategory','Consultation Category','xss_clean|required');
           $this->form_validation->set_rules('service', 'Service', 'required|xss_clean');
           $this->form_validation->set_rules('doctor','Doctor','xss_clean|required');
           $this->form_validation->set_rules('cost', 'Cost', 'required|xss_clean');
           $this->form_validation->set_rules('sponsor','Sponsor','required|xss_clean');
           $this->form_validation->set_rules('payment_mode','Payment Mode','required|xss_clean');
           
           if($payment_mode == $this->config->item('package_bill_pmode_code')){
                $this->form_validation->set_rules('package','Package','xss_clean|required');
            }
           
            if ($this->form_validation->run() == TRUE)
           {
                $error=FALSE;
                $doctor=$this->input->post('doctor');
                $consultationcategory=$this->input->post('consultationcategory');
                $visit_category=$this->input->post('visit_category');
                $sponsor=$this->input->post('sponsor');
                $payment_mode=$this->input->post('payment_mode');
                $service=$this->input->post('service');
                $cost=$this->input->post('cost');
                $package=$this->input->post('package');
                 
                $package=explode("_", $package);
              
                $dept=$this->Administration_model->subdepartments($consultationcategory);  
                
                //verify the patients balance for prepaid billing
                if($payment_mode == $this->config->item('prepaid_bill_pmode_code')){

                    $balance=$this->Reception_model->patient_prepaid_status(null,$patient[0]->patientid,1);

                    if($balance->balance < $cost){
                        $msg="patient's account has minimum low amount/suspended!";
                        $error=TRUE;
                    }
                }
                
                //verify the patients balance for package billing
                if($payment_mode == $this->config->item('package_bill_pmode_code')){

                    $pckg=$this->Reception_model->patient_package_item_status($dept[0]->departmentid,$consultationcategory,$service,$package[1]);
                    $pckg_conf=$this->Administration_model->package_service_item($package[0],$service,$consultationcategory,$dept[0]->departmentid,1);
                    
                    
                    if($pckg->quantity >= $pckg_conf[0]->quantity){
                        $msg="In active service/item for the patient's package!";
                        $error=TRUE;
                    }
                }
                
                    if(!$error){
                        //generate a unique patient id
                        $institution_id=$this->SuperAdministration_model->institution_information();
                        $init=$patient[0]->patienttype == 1?'P':'W';
                        $ptvid=$this->Reception_model->get_patientvisitid($patient[0]->patienttype); 

                        if($ptvid <> null){

                            $patientvisitid=$init.$institution_id->institution_id.'-'.$ptvid;
                        }

                        $quenumber=$this->Reception_model->get_quenumber();
                    
                            $patient_visit=array(
                                'patientid'=>$patient[0]->patientid,
                                'patientvisitid'=>$patientvisitid,
                                'patientcategory'=>$patient[0]->patienttype,
                                'consultationcategory'=>$consultationcategory,
                                'visitcategory'=>$visit_category,
                                'assigneddoctor'=>$doctor,
                                'sponsorid'=>$sponsor,
                                'quenumber'=>$quenumber,
                                'createdby'=>$this->session->userdata('user_id'),
                             );
                       
                            //do registration
                            $resp=$this->Reception_model->save_patient_visit($patient_visit,$appointment);

                            if($resp){
                                
                               
                                
                                $paymentstatus=1;
                                if(in_array($payment_mode,$this->config->item('completed_prepaid_payment_modes'))){

                                    $paymentstatus=4;
                                }
                                
                                if(!in_array($payment_mode,$this->config->item('completed_payment_modes'))){
                                    
                                    $paymentstatus=2;
                                }
                                
                                //process billing data
                                $bill=array(
                                    'patientid'=>$patient[0]->patientid,
                                    'patientvisitid'=>$patientvisitid,
                                    'departmentid'=>$dept[0]->departmentid,
                                    'subdepartmentid'=>$consultationcategory,
                                    'service_item_id'=>$service,
                                    'sponsorid'=>$sponsor,
                                    'paymentmodeid'=>$payment_mode,
                                    'amount'=>$cost,
                                    'paymentstatus'=>$paymentstatus,
                                    'createdon'=>date('Y-m-d H:i:s'),
                                    'createdby'=>$this->session->userdata('user_id'),
                                );
                                
                                //check if payment mode is not postpaid billing so as to generate txn id
                                if($payment_mode <> $this->config->item('postpaid_bill_pmode_code')){
                                 
                                    $txnid=$this->Reception_model->get_transaction_id();
                                    $bill['transactionid']=$txnid;
                                }
                                
                                //check if payment mode is postpaid billing so as to generate txn id
                                if($payment_mode == $this->config->item('postpaid_bill_pmode_code')){
                                 
                                    $pst_txnid=$this->Reception_model->get_postpaid_transaction_id();
                                    $bill['postpaid_transactionid']=$pst_txnid;
                                }
                                
                                $save=$this->Reception_model->process_bill($bill);
                                
                                if($save){
                                    //check if payment mode is not postpaid billing so as to assign a txn id value
                                    if($payment_mode == $this->config->item('postpaid_bill_pmode_code')){
                                    
                                        $postpaid=array(
                                                        'patientid'=>$patient[0]->patientid,
                                                        'transactionid'=>$pst_txnid,
                                                        'patientvisitid'=>$patientvisitid,
                                                        'amount'=>$cost,
                                                        'departmentid'=>$dept[0]->departmentid,
                                                        'subdepartmentid'=>$consultationcategory,
                                                        'service_item_id'=>$service,
                                                        'createdby'=>$this->session->userdata('user_id'),
                                                    );
                                        
                                        $this->Reception_model->save_postpaid_bill($postpaid);
                                    }
                                
                                    //check if payment mode is prepaid billing so as to further process the debit
                                    if($bill['paymentmodeid'] == $this->config->item('prepaid_bill_pmode_code')){

                                        $prepaid=array(
                                            'patientid'=>$patient[0]->patientid,
                                            'patientvisitid'=>$patientvisitid,
                                            'subscriptionid'=>$balance->subscriptionid,
                                            'amount'=>-$cost,
                                            'status'=>'DEBIT',
                                            'transactionid'=>$txnid,
                                            'departmentid'=>$dept[0]->departmentid,
                                            'subdepartmentid'=>$consultationcategory,
                                            'service_item_id'=>$service,
                                            'createdby'=>$this->session->userdata('user_id'),
                                        );

                                        $this->Reception_model->credit_debit_prepaid_account($prepaid);
                                    }
                                    
                                    //check if payment mode is package billing so as to further process the debit
                                    if($bill['paymentmodeid'] == $this->config->item('package_bill_pmode_code')){

                                        $pckg_arr=array(
                                            'patientid'=>$patient[0]->patientid,
                                            'patientvisitid'=>$patientvisitid,
                                            'packageid'=>$package[0],
                                            'subscriptionid'=>$package[1],
                                            'transactionid'=>$txnid,
                                            'departmentid'=>$dept[0]->departmentid,
                                            'subdepartmentid'=>$consultationcategory,
                                            'service_item_id'=>$service,
                                            'quantity'=>1,
                                            'transactiontype'=>'DEBIT',
                                            'amount'=>$cost,
                                            'createdby'=>$this->session->userdata('user_id'),
                                        );

                                        $this->Reception_model->credit_debit_package_account($pckg_arr);
                                    }
                                    
                                    //check if sponsor is not private so as to save cerdit bill details
                                    if($bill['sponsorid'] <> $this->config->item('instant_cash_code')){

                                        $credit=array(
                                            'patientid'=>$patient[0]->patientid,
                                            'patientvisitid'=>$patientvisitid,
                                            'sponsorid'=>$sponsor,
                                            'amount'=>$cost,
                                            'reftransactionid'=>$txnid,
                                            'departmentid'=>$dept[0]->departmentid,
                                            'subdepartmentid'=>$consultationcategory,
                                            'service_item_id'=>$service,
                                            'createdby'=>$this->session->userdata('user_id'),
                                        );

                                        $this->Reception_model->save_credit_bill($credit);
                                    }
                                    
                                    $bill=$this->Reception_model->bill_transaction($txnid,$pst_txnid);
                                    if($bill <> null){
                                         
                                        if($txnid <> null){
                                            redirect('Reception/service_order_receipt/'.$txnid.'/'.$patient[0]->patientid,'refresh');
                                        }
                                        
                                        redirect('Reception/postpaid_service_order_receipt/'.$pst_txnid.'/'.$patient[0]->patientid,'refresh');
                                    }
                                    $this->data['message2']='<div class="alert alert-success " role="alert" style="text-align:center;">payment processed</div>';
                                }
                            }else{
                                
                                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                            }
                    }else{
                        
                        $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">'.$msg.'</div>';
                    }
                    
            }
            
            //consultation department
           $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
           
           $this->data['title']="Register Patient Visit";
           $this->data['patient']=$patient;
           $this->data['id']=$id;
           $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
           $this->data['paymentModes']=$this->SuperAdministration_model->active_paymentModes();
           $this->data['visitCategories']=$this->Reception_model->active_visit_categories();
           $this->data['doctor_categs']=$this->Administration_model->consultation_categories($dept[0]->id,null);
           $this->data['services']=$this->Reception_model->consultation_services($dept[0]->id);
           $this->data['doctors']=$this->Reception_model->available_doctors();
           $this->data['packages']=$this->Reception_model->patient_package_status(null,$patient[0]->patientid,null,1);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/register_patient_visit';
           $this->load->view('reception/template2',$this->data);
    }
    
    public function create_service_order($patientid,$patientvisitid){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $patient=$this->Reception_model->patient($patientid);
           $patient=$this->Reception_model->patients($patient->id);
         
           
            if($this->input->post('add_service')){
                $order=$this->input->post('order');
               
                if(count($order) > 0){
                    $error=TRUE;
                    foreach($order as $value){
                        $orderinfo=explode('_',$value);
                        $array=array(
                            'patientid'=>$patientid,
                            'patientvisitid'=>$patientvisitid,
                            'departmentid'=>$orderinfo[2],
                            'subdepartmentid'=>$orderinfo[1],
                            'serviceid'=>$orderinfo[0],
                            'createdby'=>$this->session->userdata('user_id'),
                        );
                        
                        $check=$this->Reception_model->check_pending_order($patientid,$patientvisitid,$orderinfo[0]);
                        
                        if(!$check){
                            $resp=$this->Reception_model->save_order($array);
                            if($resp){

                                $error=FALSE;
                            }
                        }
                        
                    }
                    
                    if($error){
                        $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">service order addtion failed!</div>';
                    }
                }else{
                    
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">select atleast one service!</div>';
                }
            }
            
            
            //consultation department
           $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
           $dept2=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
           
           $this->data['title']="Investigation/Diagnosis/Other";
           $this->data['patient']=$patient;
           $this->data['patientid']=$patientid;
           $this->data['patientvisitid']=$patientvisitid;
           $this->data['packages']=$this->Reception_model->patient_package_status(null,$patientid,null,1);
           $this->data['pending_orders']=$this->Reception_model->pending_orders($patientid,$patientvisitid);
           $this->data['subdepartments']=$this->Reception_model->non_counsulation_active_subdepartments($dept[0]->id,$dept2[0]->id);
           $this->data['services']=$this->Reception_model->non_consultation_services($dept[0]->id);
           $this->data['patient_visit']=$this->Reception_model->patient_visit_details($patientvisitid);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/create_service_order';
           $this->load->view('reception/template2',$this->data);
    }
    
    public function process_order_payment($patientid,$patientvisitid){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $patient=$this->Reception_model->patient($patientid);
           $patient=$this->Reception_model->patients($patient->id);
         
           
            if($this->input->post('process_payment')){
                $confirm=$this->input->post('confirm');
                
                if(count($confirm) > 0){
                    $error=TRUE;
                    $array=array();
                    $blc=array();
                    $pck=array();
                    $i=0;
                    foreach($confirm as $value){
                        
                        $this->form_validation->set_rules('confirm['.$i++.']','Service','xss_clean|required');
                        $this->form_validation->set_rules('srv'.$value,'Service','xss_clean|required');
                        $this->form_validation->set_rules('cost'.$value,'Cost','xss_clean|required');
                        $this->form_validation->set_rules('spnsr'.$value, 'Sponsor', 'required|xss_clean');
                        $this->form_validation->set_rules('dpt'.$value,'Department','xss_clean|required');
                        $this->form_validation->set_rules('subdpt'.$value, 'Sub Department', 'required|xss_clean');
                        $this->form_validation->set_rules('qty'.$value,'Qty','required|xss_clean|numeric');
                        $this->form_validation->set_rules('pmode'.$value,'Payment Mode','required|xss_clean');
                        
                        if($this->input->post('pmode'.$value) == $this->config->item('package_bill_pmode_code')){
                            $this->form_validation->set_rules('package','Package','xss_clean|required');
                        }
                        
                        
                        if($this->form_validation->run() == TRUE){
                            $package=$this->input->post('package');
                            $package=explode("_", $package);
                            //verify the patients balance for prepaid billing
                            if($this->input->post('pmode'.$value) == $this->config->item('prepaid_bill_pmode_code')){
                                
                                $balance=$this->Reception_model->patient_prepaid_status(null,$patientid,1);
                                
                                if($balance->balance < $this->input->post('cost'.$value)){
                                    $blc[]=$value;
                                    continue;
                                }
                            }
                            
                            //verify the patients balance for package billing
                            if($this->input->post('pmode'.$value) == $this->config->item('package_bill_pmode_code')){
                               
                               
                                    $pckg=$this->Reception_model->patient_package_item_status($this->input->post('dpt'.$value),$this->input->post('subdpt'.$value),$this->input->post('srv'.$value),$package[1]);
                                    $pckg_conf=$this->Administration_model->package_service_item($package[0],$this->input->post('srv'.$value),$this->input->post('subdpt'.$value),$this->input->post('dpt'.$value),1);

                                    if($pckg->quantity >= $pckg_conf[0]->quantity && $pckg->quantity <> null){
                                        $pck[]=$value;
                                        continue;
                                    }
                            }
                            
                            $paymentstatus=1;
                            if(in_array($this->input->post('pmode'.$value),$this->config->item('completed_prepaid_payment_modes'))){

                                $paymentstatus=4;
                            }
                            
                            if(!in_array($this->input->post('pmode'.$value),$this->config->item('completed_payment_modes'))){

                                $paymentstatus=2;
                            }
                            
                            $array[]=array(
                                        'patientid'=>$this->input->post('patientid'.$value),
                                        'patientvisitid'=>$this->input->post('patientvisitid'.$value),
                                        'departmentid'=>$this->input->post('dpt'.$value),
                                        'subdepartmentid'=>$this->input->post('subdpt'.$value),
                                        'service_item_id'=>$this->input->post('srv'.$value),
                                        'sponsorid'=>$this->input->post('spnsr'.$value),
                                        'paymentmodeid'=>$this->input->post('pmode'.$value),
                                        'amount'=>$this->input->post('cost'.$value),
                                        'orderid'=>$value,
                                        'paymentstatus'=>$paymentstatus,
                                        'createdby'=>$this->session->userdata('user_id'),
                                     );
                        }
                    }
                    
                    if($array <> null){
                        
                        $txngenerate=FALSE;
                        foreach($array as $value){
                            
                            //check if payment mode is not postpaid billing so as to generate txn id
                            if($value['paymentmodeid'] <> $this->config->item('postpaid_bill_pmode_code') && $txngenerate==FALSE){
                                    $txnid=$this->Reception_model->get_transaction_id();
                                    $txngenerate=TRUE;
                            }
                            
                            //check if payment mode is postpaid billing so as to generate post paid txn id
                            if($value['paymentmodeid'] == $this->config->item('postpaid_bill_pmode_code') && $txngenerate==FALSE){
                                    $pst_txnid=$this->Reception_model->get_postpaid_transaction_id();
                                    $txngenerate=TRUE;
                            }
                            
                            $service_order=array(
                                'status'=>1,
                                'modifiedby'=>$this->session->userdata('user_id'),
                                'modifiedon'=>date('Y-m-d H:i:s'),
                            );
                            
                            //check if payment mode is not postpaid billing so as to assign a txn id value
                            if($value['paymentmodeid'] <> $this->config->item('postpaid_bill_pmode_code')){
                                    $value['transactionid']=$txnid;
                                    $service_order['transactionid']=$txnid;
                            }
                            
                             //check if payment mode is postpaid billing so as to assign a txn id value
                            if($value['paymentmodeid'] == $this->config->item('postpaid_bill_pmode_code')){
                                    $value['postpaid_transactionid']=$pst_txnid; 
                                    $service_order['postpaid_transactionid']=$pst_txnid;
                            }
                            
                            $resp=$this->Reception_model->process_bill($value);
                            
                            if($resp){
                                $error=FALSE;
                                
                                //check if payment mode is not postpaid billing so as to assign a txn id value
                                if($value['paymentmodeid'] == $this->config->item('postpaid_bill_pmode_code')){
                                    
                                        $postpaid=array(
                                                        'patientid'=>$value['patientid'],
                                                        'transactionid'=>$pst_txnid,
                                                        'patientvisitid'=>$value['patientvisitid'],
                                                        'amount'=>$value['amount'],
                                                        'departmentid'=>$value['departmentid'],
                                                        'subdepartmentid'=>$value['subdepartmentid'],
                                                        'service_item_id'=>$value['service_item_id'],
                                                        'createdby'=>$this->session->userdata('user_id'),
                                                    );
                                        
                                        $this->Reception_model->save_postpaid_bill($postpaid);
                                }
                                
                                //check if payment mode is prepaid billing so as to further process the debit
                                if($value['paymentmodeid'] == $this->config->item('prepaid_bill_pmode_code')){
                                    
                                    $prepaid=array(
                                        'patientid'=>$value['patientid'],
                                        'patientvisitid'=>$value['patientvisitid'],
                                        'subscriptionid'=>$balance->subscriptionid,
                                        'amount'=>-$value['amount'],
                                        'status'=>'DEBIT',
                                        'transactionid'=>$value['transactionid'],
                                        'departmentid'=>$value['departmentid'],
                                        'subdepartmentid'=>$value['subdepartmentid'],
                                        'service_item_id'=>$value['service_item_id'],
                                        'createdby'=>$this->session->userdata('user_id'),
                                    );
                                    
                                    $this->Reception_model->credit_debit_prepaid_account($prepaid);
                                }
                                
                                //check if payment mode is package billing so as to further process the debit
                                    if($value['paymentmodeid'] == $this->config->item('package_bill_pmode_code')){

                                        $pckg_arr=array(
                                            'patientid'=>$value['patientid'],
                                            'patientvisitid'=>$value['patientvisitid'],
                                            'packageid'=>$package[0],
                                            'subscriptionid'=>$package[1],
                                            'amount'=>$value['amount'],
                                            'transactionid'=>$value['transactionid'],
                                            'departmentid'=>$value['departmentid'],
                                            'subdepartmentid'=>$value['subdepartmentid'],
                                            'service_item_id'=>$value['service_item_id'],
                                            'quantity'=>1,
                                            'transactiontype'=>'DEBIT',
                                            'createdby'=>$this->session->userdata('user_id'),
                                        );

                                        $this->Reception_model->credit_debit_package_account($pckg_arr);
                                    }
                                    
                                
                                //check if sponsor is not private so as to save cerdit bill details
                                if($value['sponsorid'] <> $this->config->item('instant_cash_code')){
                                    
                                    $credit=array(
                                        'patientid'=>$value['patientid'],
                                        'patientvisitid'=>$value['patientvisitid'],
                                        'sponsorid'=>$value['sponsorid'],
                                        'amount'=>$value['amount'],
                                        'reftransactionid'=>$value['transactionid'],
                                        'departmentid'=>$value['departmentid'],
                                        'subdepartmentid'=>$value['subdepartmentid'],
                                        'service_item_id'=>$value['service_item_id'],
                                        'createdby'=>$this->session->userdata('user_id'),
                                    );
                                    
                                    $this->Reception_model->save_credit_bill($credit);
                                }
                                
                                //update service order status
                                $this->Reception_model->update_service_order($service_order,$value['patientid'],$value['patientvisitid'],$value['service_item_id']);
                            }
                        }
                        
                        if(!$error){
                            $bill=$this->Reception_model->bill_transaction($txnid,$pst_txnid);
                            $pvisit=$this->Reception_model->patient_visit_details($patientvisitid);
                            
                            if($bill <> null){
                                
                                if($txnid <> null){
                                    
                                    redirect('Reception/service_order_receipt/'.$txnid.'/'.$patientid,'refresh');
                                }
                                
                                redirect('Reception/postpaid_service_order_receipt/'.$pst_txnid.'/'.$patientid,'refresh');
                            }
                            
                            $this->data['message2']='<div class="alert alert-success " role="alert" style="text-align:center;">payment processed</div>';
                        }
                    }
                }else{
                    
                    $this->data['message2']='<div class="alert alert-danger " role="alert" style="text-align:center;">confirm atleast one service!</div>';
                }
            }
            
            
            //none consultation department
           $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
           $dept2=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
           
           $this->data['blc']=$blc;
           $this->data['pck']=$pck;
           $this->data['title']="Investigation/Diagnosis/Other";
           $this->data['patient']=$patient;
           $this->data['patientid']=$patientid;
           $this->data['patientvisitid']=$patientvisitid;
           
           $this->data['pending_orders']=$this->Reception_model->pending_orders($patientid,$patientvisitid);
           $this->data['subdepartments']=$this->Reception_model->non_counsulation_active_subdepartments($dept[0]->id,$dept2[0]->id);
           $this->data['services']=$this->Reception_model->non_consultation_services($dept[0]->id);
           $this->data['patient_visit']=$this->Reception_model->patient_visit_details($patientvisitid);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/create_service_order';
           $this->load->view('reception/template2',$this->data);
    }
    
    public function remove_service_order($id,$patientid,$patientvisitid){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->Reception_model->remove_service_order($id);
           redirect('Reception/create_service_order/'.$patientid.'/'.$patientvisitid,'refresh');
    }
    
    public function service_order_receipt($transactionid,$patientid){
            if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
            
            $patient=$this->Reception_model->patient($patientid);
            $patient=$this->Reception_model->patients($patient->id);
            
            $this->data['title']="Service Receipt";
            $this->data['patient']=$patient;
            $this->data['bill_data']=$this->Reception_model->bill_transaction($transactionid);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='reception/service_order_receipt';
            $this->load->view('reception/template2',$this->data);
    }
    
    public function postpaid_service_order_receipt($postapaid_transactionid,$id){
            if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
            
        $patient=$this->Reception_model->patient($patientid);
        $patient=$this->Reception_model->patients($patient->id);

        $this->data['title']="Post Paid Service Receipt";
        $this->data['patient']=$patient;
        $this->data['bill_data']=$this->Reception_model->bill_transaction(null,$postapaid_transactionid);
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='reception/postpaid_service_order_receipt';
        $this->load->view('reception/template2',$this->data);
    }
    
    public function view_sponsors(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                      array('title'=>'View Sponsor(s)','link'=>'Reception/view_sponsors'),
                                      array('title'=>'View Payment Mode(s)','link'=>'Reception/view_paymentModes'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
           }

           $name =$key['name'];

           $config["base_url"] = base_url() . "index.php/Reception/view_sponsors/name_".$key['name']."/";
           $config["total_rows"] =$this->SuperAdministration_model->sponsor_info_count($name);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['sponsors'] = $this->SuperAdministration_model->sponsor_info($name,$page,$limit);

           $this->data['title']="Sponsors";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/view_sponsors';
           $this->load->view('reception/template',$this->data);
   }
   
    public function view_paymentModes(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                      array('title'=>'View Sponsor(s)','link'=>'Reception/view_sponsors'),
                                      array('title'=>'View Payment Mode(s)','link'=>'Reception/view_paymentModes'),
                                  );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

           if ($this->input->post('sponsor')) {
               $key['sponsor'] = $this->input->post('sponsor');
               $this->data['sponsor']=$this->input->post('sponsor');
           }

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];

               $key['sponsor'] = $exp[3];
               $this->data['sponsor']=$key['sponsor'];
           }

           $name =$key['name'];
           $sponsor =$key['sponsor'];

           $config["base_url"] = base_url() . "index.php/Reception/view_paymentModes/name_".$key['name']."_sponsor_".$key['sponsor']."/";
           $config["total_rows"] =$this->SuperAdministration_model->paymentMode_info_count($name,$sponsor);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['paymentModes'] = $this->SuperAdministration_model->paymentMode_info($name,$sponsor,$page,$limit);

           $this->data['title']="Payment Modes";
           $this->data['sponsors']=$this->SuperAdministration_model->sponsors();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/view_paymentModes';
           $this->load->view('reception/template',$this->data);
   }
   
    public function view_packages(){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $this->data['side_menu']=array(
                                    array('title'=>'View Packages(s)','link'=>'Reception/view_packages'),
                                 );
        
        
            if($this->input->post('name')) {
                $key['name'] = $this->input->post('name');
                $this->data['name']=$this->input->post('name');
            }
            
           
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            
            $this->data['name']=$key['name'];
        }

        
        $name =$key['name'];

        
        $config["base_url"] = base_url() . "index.php/Reception/view_packages/name_".$key['name']."/";
        $config["total_rows"] =$this->Administration_model->packages_info_count($name);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['packages'] = $this->Administration_model->packages_info($name,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Packages';
        $this->data['content']='reception/view_packages';
        $this->load->view('reception/template',$this->data);
    }
    
    public function package_details($id){
        if(!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
        $this->data['side_menu']=array(
                                    array('title'=>'View Packages(s)','link'=>'Reception/view_packages'),
                                );
        
        
        $config["base_url"] = base_url() . "index.php/Reception/package_details/$id/";
        $config["total_rows"] =$this->Administration_model->package_items_services_info_count($id);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['pckg_details'] = $this->Administration_model->package_details_info($id,$page,$limit);
        
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['title']='Package Details';
        $this->data['content']='reception/view_package_details';
        $this->load->view('reception/template',$this->data);
    }
    
    public function view_departments(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                $this->data['side_menu']=array(
                                       array('title'=>'View Department(s)','link'=>'Reception/view_departments'),
                                       array('title'=>'View Sub Department(s)','link'=>'Reception/view_subdepartments'),
                                       array('title'=>'View Service(s)','link'=>'Reception/view_services'),
                                       array('title'=>'View Service(s) Cost','link'=>'Reception/view_service_costs')
                                   );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                

               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                }
                
                $name =$key['name'];
                
                $config["base_url"] = base_url() . "index.php/Reception/view_departments/name_".$key['name']."/";
                $config["total_rows"] =$this->Administration_model->department_info_count($name);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['departments'] = $this->Administration_model->department_info($name,$page,$limit);
                
                $this->data['title']="Department(s)";
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['content']='reception/view_departments';
		$this->load->view('reception/template',$this->data);
	}
        
    public function view_subdepartments(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
            $this->data['side_menu']=array(
                                       array('title'=>'View Department(s)','link'=>'Reception/view_departments'),
                                       array('title'=>'View Sub Department(s)','link'=>'Reception/view_subdepartments'),
                                       array('title'=>'View Service(s)','link'=>'Reception/view_services'),
                                       array('title'=>'View Service(s) Cost','link'=>'Reception/view_service_costs')
                                   );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

           if ($this->input->post('department')) {
               $key['department'] = $this->input->post('department');
               $this->data['department']=$this->input->post('department');
           }


          if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];

               $key['department'] = $exp[3];
               $this->data['department']=$key['department'];

           }

           $name =$key['name'];
           $department =$key['department'];

           $config["base_url"] = base_url() . "index.php/Reception/view_subdepartments/name_".$key['name']."_department_".$key['department']."/";
           $config["total_rows"] =$this->Administration_model->subdepartment_info_count($name,$department);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['subdepartments'] = $this->Administration_model->subdepartment_info($name,$department,$page,$limit);

           $this->data['title']="Sub Department(s)";
           $this->data['departments']=$this->Administration_model->active_departments();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/view_subdepartments';
           $this->load->view('reception/template',$this->data);
   }
      
    public function view_services(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                       array('title'=>'View Department(s)','link'=>'Reception/view_departments'),
                                       array('title'=>'View Sub Department(s)','link'=>'Reception/view_subdepartments'),
                                       array('title'=>'View Service(s)','link'=>'Reception/view_services'),
                                       array('title'=>'View Service(s) Cost','link'=>'Reception/view_service_costs')
                                   );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

           if ($this->input->post('department')) {
               $key['department'] = $this->input->post('department');
               $this->data['department']=$this->input->post('department');
           }

           if ($this->input->post('subdepartment')) {
               $key['subdepartment'] = $this->input->post('subdepartment');
               $this->data['subdepartment']=$this->input->post('subdepartment');
           }

          if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];

               $key['department'] = $exp[3];
               $this->data['department']=$key['department'];

               $key['subdepartment'] = $exp[5];
               $this->data['subdepartment']=$key['subdepartment'];

               $docType = $exp[7];
           }

           $name =$key['name'];
           $department =$key['department'];
           $subdepartment =$key['subdepartment'];

            if($docType == 1){
                    $data=$this->Administration_model->service_info_report($name,$department,$subdepartment);
                    require_once 'reports/administrator/services_pdf.php';
                }

            if($docType == 2){

                 $data=$this->Administration_model->service_info_report($name,$department,$subdepartment);
                 require_once 'reports/administrator/services_excel.php';
            }
           $config["base_url"] = base_url() . "index.php/Reception/view_services/name_".$key['name']."_department_".$key['department']."_subdepartment_".$key['subdepartment']."/";
           $config["total_rows"] =$this->Administration_model->service_info_count($name,$department,$subdepartment);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['services'] = $this->Administration_model->service_info($name,$department,$subdepartment,$page,$limit);

           $this->data['title']="Service(s)";
           $this->data['departments']=$this->Administration_model->active_departments();
           $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/view_services';
           $this->load->view('reception/template',$this->data);
   }
     
    public function view_service_costs(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                       array('title'=>'View Department(s)','link'=>'Reception/view_departments'),
                                       array('title'=>'View Sub Department(s)','link'=>'Reception/view_subdepartments'),
                                       array('title'=>'View Service(s)','link'=>'Reception/view_services'),
                                       array('title'=>'View Service(s) Cost','link'=>'Reception/view_service_costs')
                                   );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

           if ($this->input->post('department')) {
               $key['department'] = $this->input->post('department');
               $this->data['department']=$this->input->post('department');
           }

           if ($this->input->post('subdepartment')) {
               $key['subdepartment'] = $this->input->post('subdepartment');
               $this->data['subdepartment']=$this->input->post('subdepartment');
           }

           if ($this->input->post('sponsor')) {
               $key['sponsor'] = $this->input->post('sponsor');
               $this->data['sponsor']=$this->input->post('sponsor');
           }

          if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];

               $key['department'] = $exp[3];
               $this->data['department']=$key['department'];

               $key['subdepartment'] = $exp[5];
               $this->data['subdepartment']=$key['subdepartment'];

               $key['sponsor'] = $exp[7];
               $this->data['sponsor']=$key['sponsor'];

               $docType = $exp[9];
           }

           $name =$key['name'];
           $department =$key['department'];
           $subdepartment =$key['subdepartment'];
           $sponsor =$key['sponsor'];

           if($docType == 1){
                    $data=$this->Administration_model->services_costs_info_report($name,$department,$subdepartment,$sponsor);
                    require_once 'reports/administrator/services_cost_pdf.php';
                }

            if($docType == 2){

                 $data=$this->Administration_model->services_costs_info_report($name,$department,$subdepartment,$sponsor);
                 require_once 'reports/administrator/services_cost_excel.php';
            }
                
           $config["base_url"] = base_url() . "index.php/Reception/view_service_costs/name_".$key['name']."_department_".$key['department']."_subdepartment_".$key['subdepartment']."_sponsor_".$key['sponsor']."/";
           $config["total_rows"] =$this->Administration_model->service_cost_info_count($name,$department,$subdepartment,$sponsor);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['service_costs'] = $this->Administration_model->service_cost_info($name,$department,$subdepartment,$sponsor,$page,$limit);

           $this->data['title']="Service Cost(s)";
           $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
           $this->data['departments']=$this->Administration_model->active_departments();
           $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/view_service_costs';
           $this->load->view('reception/template',$this->data);
   }
    
    public function view_titles(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
            $this->data['side_menu']=array(
                                       array('title'=>'View Title(s)','link'=>'Reception/view_titles'),
                                       array('title'=>'Add Title','link'=>'Reception/add_title'),
                                    );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

          

          if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];


           }

           $name =$key['name'];

           $config["base_url"] = base_url() . "index.php/Reception/view_titles/name_".$key['name']."/";
           $config["total_rows"] =$this->Reception_model->titles_info_count($name);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['titles'] = $this->Reception_model->titles_info($name,$page,$limit);

           $this->data['title']="Title(s)";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/view_titles';
           $this->load->view('reception/template',$this->data);
   }
   
    public function add_title($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                       array('title'=>'View Title(s)','link'=>'Reception/view_titles'),
                                       array('title'=>'Add Title','link'=>'Reception/add_title'),
                                    );
           
           $this->form_validation->set_rules('fullname','Full Name','required|xss_clean');
           $this->form_validation->set_rules('shortname','Short Name','required|xss_clean');
           
         if($this->form_validation->run() == TRUE)
            {


                $shortname=$this->input->post('shortname');
                $fullname=$this->input->post('fullname');
                

                       $title=array(
                          'shortname'=>$shortname,
                           'fullname'=>$fullname,
                       );


                          //do saving
                            $resp=$this->Reception_model->save_title($title,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                }

           $this->data['title']="Add Title";
           $this->data['id']="$id";
           $this->data['titles']=$this->Reception_model->titles($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/add_title';
           $this->load->view('reception/template',$this->data);
    }
    
    public function activate_deactivate_title($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Reception_model->activate_deactivate_title($status,$id);
        redirect('Reception/view_titles','refresh');
    }
    
    public function view_genders(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
            $this->data['side_menu']=array(
                                       array('title'=>'View Gender(s)','link'=>'Reception/view_genders'),
                                       array('title'=>'Add Gender','link'=>'Reception/add_gender'),
                                    );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

          

          if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];


           }

           $name =$key['name'];

           $config["base_url"] = base_url() . "index.php/Reception/view_genders/name_".$key['name']."/";
           $config["total_rows"] =$this->Reception_model->genders_info_count($name);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['genders'] = $this->Reception_model->genders_info($name,$page,$limit);

           $this->data['title']="Gender(s)";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/view_genders';
           $this->load->view('reception/template',$this->data);
   }
   
    public function add_gender($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                       array('title'=>'View Gender(s)','link'=>'Reception/view_genders'),
                                       array('title'=>'Add Gender','link'=>'Reception/add_gender'),
                                    );
           
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('description','Description','xss_clean');
           
         if($this->form_validation->run() == TRUE)
            {


                $name=$this->input->post('name');
                $description= nl2br(trim($this->input->post('description')));
                

                       $title=array(
                          'name'=>$name,
                          'description'=>$description,
                       );


                          //do saving
                            $resp=$this->Reception_model->save_gender($title,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                }

           $this->data['title']="Add Gender";
           $this->data['id']="$id";
           $this->data['gender']=$this->Reception_model->genders($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/add_gender';
           $this->load->view('reception/template',$this->data);
    }
    
    public function activate_deactivate_gender($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Reception_model->activate_deactivate_gender($status,$id);
        redirect('Reception/view_genders','refresh');
    }
    
    public function view_maritals(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
            $this->data['side_menu']=array(
                                       array('title'=>'View Marital Status','link'=>'Reception/view_maritals'),
                                       array('title'=>'Add Marital Status','link'=>'Reception/add_marital'),
                                    );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

          

          if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];


           }

           $name =$key['name'];

           $config["base_url"] = base_url() . "index.php/Reception/view_maritals/name_".$key['name']."/";
           $config["total_rows"] =$this->Reception_model->maritals_info_count($name);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['maritals'] = $this->Reception_model->maritals_info($name,$page,$limit);

           $this->data['title']="Marital Status";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/view_maritals';
           $this->load->view('reception/template',$this->data);
   }
   
    public function add_marital($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                       array('title'=>'View Marital Status','link'=>'Reception/view_maritals'),
                                       array('title'=>'Add Marital Status','link'=>'Reception/add_marital'),
                                    );
           
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('description','Description','xss_clean');
           
         if($this->form_validation->run() == TRUE)
            {


                $name=$this->input->post('name');
                $description= nl2br(trim($this->input->post('description')));
                

                       $title=array(
                          'name'=>$name,
                          'description'=>$description,
                       );


                          //do saving
                            $resp=$this->Reception_model->save_marital($title,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                }
             
           $this->data['title']="Add Marital Status";
           $this->data['id']="$id";
           $this->data['marital']=$this->Reception_model->maritals($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/add_marital';
           $this->load->view('reception/template',$this->data);
    }
    
    public function activate_deactivate_marital($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Reception_model->activate_deactivate_marital($status,$id);
        redirect('Reception/view_maritals','refresh');
    }
    
    public function view_visit_categories(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
            $this->data['side_menu']=array(
                                       array('title'=>'View Visit Categories','link'=>'Reception/view_visit_categories'),
                                       array('title'=>'Add Visit Category','link'=>'Reception/add_visit_category'),
                                    );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

          

          if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];


           }

           $name =$key['name'];

           $config["base_url"] = base_url() . "index.php/Reception/view_visit_categories/name_".$key['name']."/";
           $config["total_rows"] =$this->Reception_model->visit_categories_info_count($name);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['visit_categories'] = $this->Reception_model->visit_categories_info($name,$page,$limit);

           $this->data['title']="Visit Categories";
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/view_visit_categories';
           $this->load->view('reception/template',$this->data);
   }
   
    public function add_visit_category($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                       array('title'=>'View Visit Categories','link'=>'Reception/view_visit_categories'),
                                       array('title'=>'Add Visit Category','link'=>'Reception/add_visit_category'),
                                    );
           
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('description','Description','xss_clean');
           
         if($this->form_validation->run() == TRUE)
            {


                $name=$this->input->post('name');
                $description= nl2br(trim($this->input->post('description')));
                

                       $visit_category=array(
                          'name'=>$name,
                          'description'=>$description,
                       );


                          //do saving
                            $resp=$this->Reception_model->save_visit_category($visit_category,$id);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                              }
                }
             
           $this->data['title']="Add Visit Category";
           $this->data['id']="$id";
           $this->data['visit_category']=$this->Reception_model->visit_categories($id);
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/add_visit_category';
           $this->load->view('reception/template',$this->data);
    }
    
    public function activate_deactivate_visit_category($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Reception_model->activate_deactivate_visit_category($status,$id);
        redirect('Reception/view_visit_categories','refresh');
    }
    
    public function package_subscription(){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'Package Service','link'=>'Reception/package_subscription'),
                                      array('title'=>'Prepaid Service','link'=>'Reception/prepaid_subscription'),
                                   );
           
           $this->form_validation->set_rules('patientid','Patient Id','xss_clean|required');
           $this->form_validation->set_rules('package','Package','xss_clean|required');
           $this->form_validation->set_rules('amount','Amount','xss_clean|required|numeric');
          
           if($this->form_validation->run() == true){

                $patientid=$this->input->post('patientid');
                $package=$this->input->post('package');
                $subscriptionid=$this->get_subscription_id('PACKAGE');
                $amount=$this->input->post('amount');
                
                $check_patient=$this->Reception_model->patient($patientid);
                
                if($check_patient <> null){
                    
                    $chk_pckg_patient=$this->Reception_model->patient_package_status(null,$patientid,$package,1);
                    
                    if($chk_pckg_patient == null){
                        $array=array(
                            'patientid'=>$patientid,
                            'transactionid'=>$this->Reception_model->get_transaction_id(),
                            'packageid'=>$package,
                            'subscriptionid'=>$subscriptionid,
                            'transactiontype'=>'CREDIT',
                            'amount'=>$amount,
                            'createdby'=>$this->session->userdata('user_id'),
                        );

                        $extra=array(
                            'subscriptionid'=>$subscriptionid,
                            'patientid'=>$patientid,
                            'packageid'=>$package,
                            'lastupdate'=>date('Y-m-d H:i:s'),
                        );

                        $sv=$this->Reception_model->save_package_subscription($array,$extra);

                        if($sv){

                            redirect('Reception/package_subscription_receipt/'.$array['subscriptionid'].'/'.$array['patientid'],'refresh');
                        }

                        $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">subscription failed!</div>';
                    }else{
                        $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">package is still active for the patient!</div>';
                    }
                    
                }else{
                     $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">patient does not exist!</div>';
                }
            }
            
        $this->data['title']="Package Subscription";
        $this->data['packages']=$this->Administration_model->packages();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='reception/package_subscription';
        $this->load->view('reception/template',$this->data);
    }
    
    public function package_subscription_receipt($subscriptionid,$patientid){
            if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
            $this->data['side_menu']=array(
                                      array('title'=>'Package Service','link'=>'Reception/package_subscription'),
                                      array('title'=>'Prepaid Service','link'=>'Reception/prepaid_subscription'),
                                   );
            
            $patient=$this->Reception_model->patient($patientid);
            $patient=$this->Reception_model->patients($patient->id);
            
            $this->data['title']="Package Subscription Receipt";
            $this->data['patient']=$patient;
            $this->data['bill_data']=$this->Reception_model->package_subscriptions($subscriptionid);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='reception/package_subscription_receipt';
            $this->load->view('reception/template2',$this->data);
    }
    
    public function prepaid_subscription(){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'Package Service','link'=>'Reception/package_subscription'),
                                      array('title'=>'Prepaid Service','link'=>'Reception/prepaid_subscription'),
                                   );
           
           $this->form_validation->set_rules('patientid','Patient Id','xss_clean|required');
           $this->form_validation->set_rules('amount','Amount','xss_clean|required|numeric');
          
           if($this->form_validation->run() == true){

                $patientid=$this->input->post('patientid');
                $amount=$this->input->post('amount');
                
                $check_patient=$this->Reception_model->patient($patientid);
                
                if($check_patient <> null){
                    $balanceUpdate=TRUE;
                    $p_subscription=$this->Reception_model->patient_prepaid_status(null,$patientid);
                    $subscriptionid=$p_subscription->subscriptionid;
                    
                    if($p_subscription == null){
                        $balanceUpdate=FALSE;
                        $subscriptionid=$this->get_subscription_id('PREPAID');
                    }
                    
                    $array=array(
                        'patientid'=>$patientid,
                        'transactionid'=>$this->Reception_model->get_transaction_id(),
                        'subscriptionid'=>$subscriptionid,
                        'status'=>'CREDIT',
                        'amount'=>$amount,
                        'createdby'=>$this->session->userdata('user_id'),
                    );
                    
                    $extra=array(
                        'patientid'=>$patientid,
                        'subscriptionid'=>$subscriptionid,
                        'balance'=>$amount,
                        'lastupdate'=>date('Y-m-d H:i:s'),
                    );
                    
                    $sv=$this->Reception_model->save_prepaid_subscription($array,$extra,$balanceUpdate);
                    
                    if($sv){
                        
                        redirect('Reception/prepaid_subscription_receipt/'.$array['subscriptionid'].'/'.$array['patientid'].'/'.$array['transactionid'],'refresh');
                    }
                    
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">subscription failed!</div>';
                }else{
                     $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">patient does not exist!</div>';
                }
            }
            
        $this->data['title']="Prepaid Subscription";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='reception/prepaid_subscription';
        $this->load->view('reception/template',$this->data);
    }
    
    public function prepaid_subscription_receipt($subscriptionid,$patientid,$transactionid){
            if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
            
            $this->data['side_menu']=array(
                                   array('title'=>'Package Service','link'=>'Reception/package_subscription'),
                                   array('title'=>'Prepaid Service','link'=>'Reception/prepaid_subscription'),
                                );
               
            $patient=$this->Reception_model->patient($patientid);
            $patient=$this->Reception_model->patients($patient->id);
            
            $this->data['title']="Prepaid Subscription Receipt";
            $this->data['patient']=$patient;
            $this->data['bill_data']=$this->Reception_model->prepaid_subscriptions(null,$transactionid);
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['content']='reception/prepaid_subscription_receipt';
            $this->load->view('reception/template2',$this->data);
    }
    
    public function aggregated_collection(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
            $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                               array('title'=>'Admissions','link'=>'Reception/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Reception/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Reception/outpatients_report'),
                           );
        
        $start=$this->input->post('start') <> null?$this->input->post('start'):date('Y-m-d');
        $end=$this->input->post('end') <> null?$this->input->post('end'):date('Y-m-d');
        $department=$this->input->post('department');
        $subdepartment=$this->input->post('subdepartment');
        $sponsor=$this->input->post('sponsor');
        $userid=$this->session->userdata('user_id');
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $start = $exp[1];
            $end = $exp[3];
            $sponsor = $exp[5];
            $department = $exp[7];
            $subdepartment = $exp[9];
            $docType = $exp[11];
        }
        
        $data=$this->Reception_model->aggregated_collection($start,$end,$sponsor,$department,$subdepartment,$userid);
        if($docType == 1){
                
            require_once 'reports/reception/aggregated_collection_report_pdf.php';
        }

        if($docType == 2){

             require_once 'reports/reception/aggegated_collection_report_excel.php';
        }
        
        $this->data['title']="Aggregated Collections";
        $this->data['departments']=$this->Administration_model->active_departments();
        $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
        $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
        $this->data['sponsor']=$sponsor;
        $this->data['subdepartment']=$subdepartment;
        $this->data['department']=$department;
        $this->data['end']=$end;
        $this->data['start']=$start;
        $this->data['aggregated_collections']=$data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='reception/aggregated_collection';
        $this->load->view('reception/template',$this->data);
    }
    
    public function general_collection(){
        if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                               array('title'=>'Admissions','link'=>'Reception/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Reception/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Reception/outpatients_report'),
                           );
               
        $start=$this->input->post('start') <> null?$this->input->post('start'):date('Y-m-d');
        $end=$this->input->post('end') <> null?$this->input->post('end'):date('Y-m-d');
        $department=$this->input->post('department');
        $subdepartment=$this->input->post('subdepartment');
        $sponsor=$this->input->post('sponsor');
        $userid=$this->session->userdata('user_id');
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $start = $exp[1];
            $end = $exp[3];
            $sponsor = $exp[5];
            $department = $exp[7];
            $subdepartment = $exp[9];
            $docType = $exp[11];
        }
        
        $data=$this->Reception_model->general_collection($start,$end,$sponsor,$department,$subdepartment,$userid);
        if($docType == 1){
                
            require_once 'reports/reception/general_collection_report_pdf.php';
        }

        if($docType == 2){

             require_once 'reports/reception/general_collection_report_excel.php';
        }
        
        $this->data['title']="General Collections";
        $this->data['departments']=$this->Administration_model->active_departments();
        $this->data['subdepartments']=$this->Administration_model->active_subdepartments();
        $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
        $this->data['sponsor']=$sponsor;
        $this->data['subdepartment']=$subdepartment;
        $this->data['department']=$department;
        $this->data['end']=$end;
        $this->data['start']=$start;
        $this->data['general_collections']=$data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='reception/general_collection';
        $this->load->view('reception/template',$this->data);
    }
   
    public function package_accounts(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
        $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                               array('title'=>'Admissions','link'=>'Reception/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Reception/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Reception/outpatients_report'),
                           );
        
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('package')){
            $key['package'] = $this->input->post('package');
            $this->data['package']=$this->input->post('package');
        }

        if ($this->input->post('subscriptionid')) {
            $key['subscriptionid'] = $this->input->post('subscriptionid');
            $this->data['subscriptionid']=$this->input->post('subscriptionid');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
             $key['subscriptionid'] = $exp[3];
            $this->data['subscriptionid']=$key['subscriptionid'];

            $key['package'] = $exp[5];
            $this->data['package']=$key['package'];
        }

        $patientid =$key['patientid'];
        $subscriptionid =$key['subscriptionid'];
        $package =$key['package'];
        
        $config["base_url"] = base_url() . "index.php/Reception/package_accounts/patientid_".$key['patientid']."_subscriptionid_".$key['subscriptionid']."_package_".$key['package']."/";
        $config["total_rows"] =$this->Reception_model->patient_packages_count($patientid,$subscriptionid,$package);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_packages'] = $this->Reception_model->patient_packages($patientid,$subscriptionid,$package,$page,$limit);

        $this->data['title']="Patient Packages";
        $this->data['packages']=$this->Administration_model->packages();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='reception/patient_packages';
        $this->load->view('reception/template',$this->data);       
    }
    
    public function patient_package_details($subscriptionid,$patientid,$docType){
        if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                               array('title'=>'Admissions','link'=>'Reception/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Reception/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Reception/outpatients_report'),
                           );
               
        if($docType == 1){
                
                $data=$this->Reception_model->subscriptions($subscriptionid);
                require_once 'reports/reception/package_account_report_pdf.php';
        }

        if($docType == 2){

             $data=$this->Reception_model->subscriptions($subscriptionid);
             require_once 'reports/reception/package_account_report_excel.php';
        }    
        $patient=$this->Reception_model->patient($patientid);       
        $patient=$this->Reception_model->patients($patient->id);       
        $this->data['patient_package_details'] =$this->Reception_model->subscriptions($subscriptionid);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['subscriptionid']=$subscriptionid;
        $this->data['title']=$subscriptionid." Subscription Details";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='reception/patient_package_details';
        $this->load->view('reception/template2',$this->data);
    }
    
    public function patients_debtors(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                               array('title'=>'Admissions','link'=>'Reception/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Reception/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Reception/outpatients_report'),
                           );
               
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('start')){
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
             $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];
        }

        $patientid =$key['patientid'];
        $start =$key['start'];
        $end =$key['end'];
        
        $config["base_url"] = base_url() . "index.php/Reception/patients_debtors/patientid_".$key['patientid']."_start_".$key['start']."_end_".$key['end']."/";
        $config["total_rows"] =$this->Reception_model->patient_debtors_count($patientid,$start,$end);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_debtors'] = $this->Reception_model->patient_debtors($patientid,$start,$end,$page,$limit);

        $this->data['title']="Patient Debtors";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='reception/patient_debtors';
        $this->load->view('reception/template',$this->data);
    }
    
    public function patient_debt_details($patientid,$docType){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                               array('title'=>'Admissions','link'=>'Reception/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Reception/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Reception/outpatients_report'),
                           );
            
            if($docType == 1){
                
                $data=$this->Reception_model->patient_debt_details($patientid);
                require_once 'reports/reception/patient_debt_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->patient_debt_details($patientid);
                 require_once 'reports/reception/patient_debt_report_excel.php';
            }
            
               if($this->input->post('clear_debt')){
                   $debts=$this->input->post('debt');
                   
                   if(count($debts) > 0){
                       
                       foreach($debts as $key=>$value){
                           
                           $data=explode('_',$value);
                           $array[]=array(
                               'item_service_id'=>$data[0],
                               'departmentid'=>$data[1],
                               'postpaid_transactionid'=>$data[2],
                            );
                       }
                       
                       if($array <> null){
                           
                           $txngenerate=FALSE;
                           foreach($array as $key=>$value){
                               
                               if($txngenerate==FALSE){
                                   
                                        $txnid=$this->Reception_model->get_transaction_id();
                                        $txngenerate=TRUE;
                                }
                                
                                $bill_txn=array(
                                    'transactionid'=>$txnid,
                                    'paymentmodeid'=>$this->config->item('instant_cash_bill_pmode_code'),
                                    'createdon'=>date('Y-m-d H:i:s'),
                                    'createdby'=>$this->session->userdata('user_id'),
                                    'paymentstatus'=>1,
                                );
                                
                                $debt_bill_txn=array(
                                    'reftransactionid'=>$txnid,
                                    'status'=>1,
                                    'modifiedby'=>$this->session->userdata('user_id'),
                                    'modifiedon'=>date('Y-m-d H:i:s'),
                                );
                                
                                $clear=$this->Reception_model->clear_debt($bill_txn,$debt_bill_txn,$value['item_service_id'],$value['postpaid_transactionid']);
                                
                                if($clear){
                                    
                                    $this->Reception_model->update_service_item_order($txnid,$value['postpaid_transactionid'],$value['item_service_id'],TRUE);
                                    redirect('Reception/service_order_receipt/'.$txnid.'/'.$patientid,'refresh');
                                }
                            }
                       }
                   }else{
                       $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">confirm atleast one debt!</div>';
                   }
               }
          
        $patient=$this->Reception_model->patient($patientid);       
        $patient=$this->Reception_model->patients($patient->id);  
        $this->data['patient_debt_details'] =$this->Reception_model->patient_debt_details($patientid);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['title']="Patient Debt Details";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='reception/patient_debt_details';
        $this->load->view('reception/template2',$this->data);
    }
    
    public function patient_statement($details){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                               array('title'=>'Admissions','link'=>'Reception/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Reception/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Reception/outpatients_report'),
                           );
        
        $details=explode("_",$details);
        $patientid=$details[1];
        $start=$details[3];
        $end=$details[5];
        $docType=$details[7];
        $institution=$this->SuperAdministration_model->institution_information();
        if($docType == 1){
            
            $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end);
            require_once 'reports/reception/patients_statement_report_pdf.php';
        }

        if($docType == 2){

             $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end);
             require_once 'reports/reception/patients_statement_report_excel.php';
        }
        
        $this->form_validation->set_rules('patientid','Patient Id','xss_clean|required|callback_patientid');
        $this->form_validation->set_rules('start','Start Date','xss_clean|required|callback_start');
        $this->form_validation->set_rules('end','End Date','xss_clean|required|callback_end');

        if($this->form_validation->run() == TRUE){
            
            $patientid=$this->input->post('patientid');
            $start=$this->input->post('start');
            $end=$this->input->post('end');
            
            $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end);
        }
           
        $this->data['title']="Patient Statement";
        $this->data['patientid']=$patientid;
        $this->data['start']=$start;
        $this->data['end']=$end;
        $this->data['data']=$data;
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$institution;
        $this->data['statement']='common/statement';
        $this->data['content']='reception/patient_statement';
        $this->load->view('reception/template',$this->data);
    }
    
    public function patient_transactions(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
        
        $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'General Visit','link'=>'Reception/general_visit'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                           );
               
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('start')){
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }

        if ($this->input->post('sponsor')) {
            $key['sponsor'] = $this->input->post('sponsor');
            $this->data['sponsor']=$this->input->post('sponsor');
        }

        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
            $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];

            $key['sponsor'] = $exp[7];
            $this->data['sponsor']=$key['sponsor'];
            
            $docType = $exp[9];
        }

        $start =$key['start'];
        $end =$key['end'];
        $sponsor =$key['sponsor'];
        $patientid =$key['patientid'];
           
        if($docType == 1){
                
            $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end,$sponsor);
            require_once 'reports/reception/patients_transactions_report_pdf.php';
        }

        if($docType == 2){

             $data=$this->Reception_model->patient_transactions_details($patientid,$start,$end,$sponsor);
             require_once 'reports/reception/patients_transactions_report_excel.php';
        }
            
        $config["base_url"] = base_url() . "index.php/Reception/patient_transactions/patientid_".$key['patientid']."_start_".$key['start']."_end_".$key['end']."_sponsor_".$key['sponsor']."/";
        $config["total_rows"] =$this->Reception_model->patient_transactions_count($patientid,$start,$end,$sponsor);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_transactions'] = $this->Reception_model->patient_transactions($patientid,$start,$end,$sponsor,$page,$limit);

        $this->data['title']="Patient Transactions";
        $this->data['sponsors']=$this->SuperAdministration_model->active_sponsors();
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='reception/patient_transactions';
        $this->load->view('reception/template',$this->data);
    }
   
    public function prepaid_accounts(){
         if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
        $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'General Visit','link'=>'Reception/general_visit'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                           );
        
        if ($this->input->post('patientid')){
            $key['patientid'] = $this->input->post('patientid');
            $this->data['patientid']=$this->input->post('patientid');
        }
        
        if ($this->input->post('subscriptionid')) {
            $key['subscriptionid'] = $this->input->post('subscriptionid');
            $this->data['subscriptionid']=$this->input->post('subscriptionid');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['patientid'] = $exp[1];
            $this->data['patientid']=$key['patientid'];
            
             $key['subscriptionid'] = $exp[3];
            $this->data['subscriptionid']=$key['subscriptionid'];

        }

        $patientid =$key['patientid'];
        $subscriptionid =$key['subscriptionid'];
        
        $config["base_url"] = base_url() . "index.php/reception/prepaid_accounts/patientid_".$key['patientid']."_subscriptionid_".$key['subscriptionid']."/";
        $config["total_rows"] =$this->Reception_model->patients_prepaid_accounts_count($patientid,$subscriptionid);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['patient_prepaid'] = $this->Reception_model->patients_prepaid_accounts($patientid,$subscriptionid,$page,$limit);

        $this->data['title']="Patient Prepaid Accounts";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='reception/patient_prepaid_accounts';
        $this->load->view('reception/template',$this->data);       
    }
    
    public function patient_prepaid_account_details($subscriptionid,$patientid,$docType){
        if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }
               
               $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                               array('title'=>'Admissions','link'=>'Reception/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Reception/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Reception/outpatients_report'),
                           );
         
            
            if($docType == 1){
                
                $data=$this->Reception_model->prepaid_accounts($subscriptionid);
                require_once 'reports/reception/prepaid_account_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->prepaid_accounts($subscriptionid);
                 require_once 'reports/reception/prepaid_account_report_excel.php';
            }
            
        $patient=$this->Reception_model->patient($patientid);       
        $patient=$this->Reception_model->patients($patient->id);       
        $this->data['patient_prepaid_details'] =$this->Reception_model->prepaid_accounts($subscriptionid);
        $this->data['patient']=$patient;
        $this->data['patientid']=$patientid;
        $this->data['subscriptionid']=$subscriptionid;
        $this->data['title']=$subscriptionid." Subscription Details";
        $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
        $this->data['institution']=$this->SuperAdministration_model->institution_information();
        $this->data['content']='reception/patient_prepaid_details';
        $this->load->view('reception/template2',$this->data);
    }
    
    public function inpatients_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                               array('title'=>'Admissions','link'=>'Reception/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Reception/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Reception/outpatients_report'),
                           );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('ward')) {
               $key['ward'] = $this->input->post('ward');
               $this->data['ward']=$this->input->post('ward');
           }
           
           if ($this->input->post('bed')) {
               $key['bed'] = $this->input->post('bed');
               $this->data['bed']=$this->input->post('bed');
           }

           if ($this->input->post('status')) {
               $key['status'] = $this->input->post('status');
               $this->data['status']=$this->input->post('status');
           }
           
           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['ward'] = $exp[5];
               $this->data['ward']=$key['ward'];
               
               $key['bed'] = $exp[7];
               $this->data['bed']=$key['bed'];
               
               $key['status'] = $exp[9];
               $this->data['status']=$key['status'];
               
               $docType = $exp[11];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $ward =$key['ward'];
            $bed =$key['bed'];
            $status =$key['status'];
            
            if($docType == 1){
                
                    $data=$this->Nursing_model->admissions($start,$end,$ward,$bed,$status);
                    require_once 'reports/clinical/admission_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Nursing_model->admissions($start,$end,$ward,$bed,$status);
                 require_once 'reports/clinical/admission_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Reception/inpatients_report/start_".$key['start']."_end_".$key['end']."_ward_".$key['ward']."_bed_".$key['bed']."_status_".$status."/";
           $config["total_rows"] =$this->Nursing_model->admission_report_count($start,$end,$ward,$bed,$status);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Nursing_model->admission_report($start,$end,$ward,$bed,$status,$page,$limit);
           
           $this->data['title']="Admission Report";
           $this->data['wards']=$this->Inventory_model->units(null,$this->config->item('ward_unit_category_code'));
           $this->data['admission_status']=$this->config->item('admission_status');
           $this->data['beds']=$this->Administration_model->ward_beds();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/admission_report';
           $this->load->view('reception/template',$this->data);
   }
    
    public function inpatient_transfers_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                               array('title'=>'Admissions','link'=>'Reception/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Reception/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Reception/outpatients_report'),
                           );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('pid')) {
               $key['pid'] = $this->input->post('pid');
               $this->data['pid']=$this->input->post('pid');
            }
           
           if ($this->input->post('bed')) {
               $key['bed'] = $this->input->post('bed');
               $this->data['bed']=$this->input->post('bed');
           }

           if ($this->input->post('ward')) {
               $key['ward'] = $this->input->post('ward');
               $this->data['ward']=$this->input->post('ward');
           }
           
           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['ward'] = $exp[5];
               $this->data['ward']=$key['ward'];
               
               $key['bed'] = $exp[7];
               $this->data['bed']=$key['bed'];
               
               $key['pid'] = $exp[9];
               $this->data['pid']=$key['pid'];
               
               $docType = $exp[11];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $ward =$key['ward'];
            $bed =$key['bed'];
            $pid =$key['pid'];
            
            if($docType == 1){
                
                    $data=$this->Nursing_model->admission_transfers($start,$end,$ward,$bed,$pid);
                    require_once 'reports/clinical/admission_transfer_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Nursing_model->admission_transfers($start,$end,$ward,$bed,$pid);
                 require_once 'reports/clinical/admission_transfer_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Reception/inpatient_transfers_report/start_".$start."_end_".$end."_ward_".$ward."_bed_".$bed."_pid_".$pid."/";
           $config["total_rows"] =$this->Nursing_model->admission_transfer_report_count($start,$end,$ward,$bed,$pid);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Nursing_model->admission_transfer_report($start,$end,$ward,$bed,$pid,$page,$limit);
           
           $this->data['title']="In Patient Transfers Report";
           $this->data['wards']=$this->Inventory_model->units(null,$this->config->item('ward_unit_category_code'));
           $this->data['beds']=$this->Administration_model->ward_beds();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/admission_transfer_report';
           $this->load->view('reception/template',$this->data);
   }
   
    public function outpatients_report(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                               array('title'=>'Aggregated Report','link'=>'Reception/aggregated_collection'),
                               array('title'=>'General Collection','link'=>'Reception/general_collection'),
                               array('title'=>'Package Accounts','link'=>'Reception/package_accounts'),
                               array('title'=>'Patient Debtors','link'=>'Reception/patients_debtors'),
                               array('title'=>'Patient Statement','link'=>'Reception/patient_statement'),
                               array('title'=>'Patient Transactions','link'=>'Reception/patient_transactions'),
                               array('title'=>'Prepaid Accounts','link'=>'Reception/prepaid_accounts'),
                               array('title'=>'Admissions','link'=>'Reception/inpatients_report'),
                               array('title'=>'In Patient Transfers','link'=>'Reception/inpatient_transfers_report'),
                               array('title'=>'Out Patients','link'=>'Reception/outpatients_report'),
                           );
 
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
            if ($this->input->post('visitcategory')) {
               $key['visitcategory'] = $this->input->post('visitcategory');
               $this->data['visitcategory']=$this->input->post('visitcategory');
           }
           
           if ($this->input->post('consultationcategory')) {
               $key['consultationcategory'] = $this->input->post('consultationcategory');
               $this->data['consultationcategory']=$this->input->post('consultationcategory');
           }

           if ($this->uri->segment(3)) {
               
               $exp = explode("_", $this->uri->segment(3));
               
               $key['start'] = $exp[1];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[3];
               $this->data['end']=$key['end'];
               
               $key['visitcategory'] = $exp[5];
               $this->data['visitcategory']=$key['visitcategory'];
               
               $key['consultationcategory'] = $exp[7];
               $this->data['consultationcategory']=$key['consultationcategory'];
               
               $docType = $exp[9];
            }
            
            $start =$key['start'];
            $end =$key['end'];
            $visitcategory =$key['visitcategory'];
            $consultationcategory =$key['consultationcategory'];
            
            if($docType == 1){
                
                    $data=$this->Reception_model->outpatients($start,$end,$visitcategory,$consultationcategory);
                    require_once 'reports/reception/outpatients_report_pdf.php';
            }

            if($docType == 2){

                 $data=$this->Reception_model->outpatients($start,$end,$visitcategory,$consultationcategory);
                 require_once 'reports/reception/outpatients_report_excel.php';
            }
            
           $config["base_url"] = base_url() . "index.php/Reception/outpatients_report/start_".$key['start']."_end_".$key['end']."_visiticateg_".$key['visitcategory']."_consultationcateg_".$key['consultationcategory']."/";
           $config["total_rows"] =$this->Reception_model->outpatients_report_count($start,$end,$visitcategory,$consultationcategory);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['data'] =$this->Reception_model->outpatients_report($start,$end,$visitcategory,$consultationcategory,$page,$limit);
           
           //consultation department
           $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
           
           $this->data['title']="Out Patients Report";
           $this->data['consultationcategories']=$this->Administration_model->consultation_categories($dept[0]->id,null);
           $this->data['visitcategories']=$this->Reception_model->visit_categories();
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institution']=$this->SuperAdministration_model->institution_information();
           $this->data['content']='reception/outpatients_report';
           $this->load->view('reception/template',$this->data);
   }
   
    public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Reception/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Reception/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['content']='reception/profile';
                $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
                $this->data['institution']=$this->SuperAdministration_model->institution_information();
                $this->data['profile']=$this->SuperAdministration_model->profile_data();
		$this->load->view('reception/template',$this->data);
    }
        
    public function edit_profile(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Reception/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Reception/change_password'),
                                        );
            
            $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
            $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
            $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
            $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
            $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if($this->form_validation->run() == true){

                 $firstname=$this->input->post('first_name');
                 $middlename=$this->input->post('middle_name');
                 $lastname=$this->input->post('last_name');
                 $username=$this->input->post('username');
                 $email=$this->input->post('email');
                 $mobile=$this->input->post('mobile');

                $user=array(
                            'FIRST_NAME'=>$firstname,
                            'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                            'LAST_NAME'=>$lastname,
                            'USERNAME'=>$username,
                            'EMAIL'=>$email<>null?$email:null,
                            'MSISDN'=>$mobile,
                        );

                $user_group=array(
                            'GROUP_ID'=>$this->session->userdata('group')
                        );

                $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                if($username_check){


                    //do registration
                     $resp=$this->SuperAdministration_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                       if($resp){

                           redirect('Reception/profile','refresh');
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                       }
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                }
            }

            $this->data['title']="Edit Profile";
            $this->data['content']='reception/edit_profile';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->load->view('reception/template',$this->data);
    }
        
    public function change_password(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Reception/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Reception/change_password'),
                                        );

            $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
            $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 

            if($this->form_validation->run() == true){

                $password=$this->input->post('password');
                $pchange=$this->SuperAdministration_model->change_password($password);

                if($pchange){
                           
                        $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">password change successfull!</div>';
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">password change failed!</div>';
                }
            }

            $this->data['title']="Change Password";
            $this->data['content']='reception/change_password';
            $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
            $this->data['profile']=$this->SuperAdministration_model->profile_data();
            $this->data['institution']=$this->SuperAdministration_model->institution_information();
            $this->load->view('reception/template',$this->data);
    }
    
    function cur_password($password){
            
            $pf=$this->SuperAdministration_model->profile_data();
            $hashed_old=$this->SuperAdministration_model->hash_db_password($password,$pf);
           
             if($pf->password <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
    }
    
    function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('Receptionist')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
    
    function username_uniqueness($username,$id){
            
        return $this->SuperAdministration_model->check_username($username,$id);
    }
    
    function patientid($id){
            $patient=$this->Reception_model->patient($id);
           
             if($patient == null){
                 
                 $this->form_validation->set_message("patientid","The %s is incorrect");
                  return FALSE;
             }
            return TRUE;
    }
    
    function start($date){
        
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('start', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('start', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
    }
    
    function end($date){
        
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
    }
    
    function service_cost(){
        
        $subdepartment=$this->input->post('subdepartment');
        $service=$this->input->post('service');
        $sponsor=$this->input->post('sponsor');
        
        $cost=$this->Reception_model->get_active_service_cost($subdepartment,$service,$sponsor);
        
        echo $cost->cost;
        exit;
    }
    
    function download_postpaid_service_receipt($postpaid_transactionid){
        
        $bill_data=$this->Reception_model->bill_transaction(null,$postpaid_transactionid);
        $institution=$this->SuperAdministration_model->institution_information();
        require_once 'receipts/postpaid_service_receipt.php';
    }
    
    function download_service_receipt($transactionid){
        
        $bill_data=$this->Reception_model->bill_transaction($transactionid,null);
        $institution=$this->SuperAdministration_model->institution_information();
        require_once 'receipts/service_receipt.php';
    }
    
    function download_package_subscription_receipt($subscriptionid){
        
        $bill_data=$this->Reception_model->package_subscriptions($subscriptionid,null);
        $institution=$this->SuperAdministration_model->institution_information();
        require_once 'receipts/package_subscription_receipt.php';
    }
    
    function download_prepaid_subscription_receipt($transactionid){
        
        $bill_data=$this->Reception_model->prepaid_subscriptions(null,$transactionid);
        $institution=$this->SuperAdministration_model->institution_information();
        require_once 'receipts/prepaid_subscription_receipt.php';
    }
    
    function modal_patient_details(){
        $id=$this->input->post('patientid');
        $patient=$this->Reception_model->patient($id);
        $data="";
       
        $title=$this->Reception_model->titles($patient->titleid);
        $gender=$this->Reception_model->genders($patient->genderid);
        $marital=$this->Reception_model->maritals($patient->maritalid);
        $region=$this->Reception_model->regions($patient->region);
        $district=$this->Reception_model->districts($patient->district,null);
        
        $data .=$patient->patientid.'=_'.$title[0]->shortname.'.'.$patient->name.'=_'.$gender[0]->name.'=_'.$marital[0]->name.'=_'.$patient->tribe.'=_'.$patient->dob;
        $data .='=_'.$patient->email.'=_'.$patient->phone.'=_'.$region[0]->name.'=_'.$district[0]->name.'=_'.$patient->street.'=_'.$patient->occupation;
        
        echo $data;
        exit;
    }
    
    function modal_patient_visit_details(){
        $id=$this->input->post('patientvisitid');
        $patientvisit=$this->Reception_model->patient_visit_details($id);
        $data="";
       
        $visit_category=$this->Reception_model->visit_categories($patientvisit->visitcategory);
        
        $subdpt=$this->Administration_model->subdepartments($patientvisit->consultationcategory);
        
        $doc=$this->ion_auth->user($patientvisit->assigneddoctor)->row();
        $doc='Dr. '.$doc->first_name . ' ' . $doc->middle_name . ' ' . $doc->last_name;
        
        $sp=$this->SuperAdministration_model->get_sponsor_by_code($patientvisit->sponsorid);
        
        
        if($patientvisit->patientcategory ==1){
            $pctg='Out Patient';
        }else if($patientvisit->patientcategory ==2){
             $pctg='Walk In Patient';
        }else{
            $pctg='In Patient';
        }
        
        $cstatus=$patientvisit->consultationstatus ==0?'Not Seen':'Seen';
        
        $data .=$patientvisit->patientid.'=_'.$patientvisit->patientvisitid.'=_'.$visit_category[0]->name.'=_'.$pctg.'=_'.$subdpt[0]->name.'=_'.$doc.'=_'.$cstatus;
        $data .='=_'.$sp[0]->shortname.'=_'.$patientvisit->quenumber;
        
        echo $data;
        exit;
    }
    
    function modal_patient_bill_details(){
        $bid=$this->input->post('bid');
       
        $bill=$this->Reception_model->transaction($bid);
        $data="";
       
        $dpt=$this->Administration_model->departments($bill->departmentid);
        $subdpt=$this->Administration_model->subdepartments($bill->subdepartmentid);
        $srv=$this->Inventory_model->items($bill->service_item_id);
        $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
                    
        if($dept[0]->id <> $bill->departmentid){
            $srv=$this->Administration_model->services($bill->service_item_id);
        }
        
        $sp=$this->SuperAdministration_model->get_sponsor_by_code($bill->sponsorid);
        $pmode=$this->SuperAdministration_model->get_paymentMode_by_code($bill->paymentmodeid);
       
        if($bill->paymentstatus == 1 || $bill->paymentstatus == 4){
            $pstatus="Paid";
        }

        if($bill->paymentstatus == 2){
            $pstatus="Pending";
        }

        if($bill->paymentstatus == 3){
            $pstatus="Cancelled";
        }
        
        
        $data .=$bill->patientid.'=_'.$bill->patientvisitid.'=_'.$dpt[0]->name.'=_'.$subdpt[0]->name.'=_'.$srv[0]->name.'=_'.$sp[0]->shortname.'=_'.$pmode->name.'=_'.$bill->amount.'=_'.$bill->transactionid;
        $data .='=_'.$pstatus;
        
        echo $data;
        exit;
    }
    
    function get_subscription_id($subscriptionType){
        
        $inst=$this->SuperAdministration_model->institution_information();
        
        if($subscriptionType =='PACKAGE'){
            while(true){
                $sbcrpt_id=$inst->institution_id.mt_rand(1000,9999);
                $chk=$this->Reception_model->subscriptions($sbcrpt_id);

                if($chk == null){
                    break;
                }
            }
        }
        
        if($subscriptionType =='PREPAID'){
            while(true){
                $sbcrpt_id=$inst->institution_id.mt_rand(1000,9999);
                $chk=$this->Reception_model->prepaid_subscriptions($sbcrpt_id);

                if($chk == null){
                    break;
                }
            }
        }
        
        return $sbcrpt_id;
    }
    
    function packages(){
        $package=$this->input->post('package');
        $pckg=$this->Administration_model->packages($package);
        $data="";
        
        if($pckg <> null){
            
            $data .=$pckg[0]->packageid.'=_'.$pckg[0]->name.'=_'.$pckg[0]->cost.'=_'.$pckg[0]->status;
           
        }
        
        echo $data;
        exit;
    }
    
    function patient_ids(){
        $id=$this->input->post('patientid');
       
        $patients=$this->Reception_model->patient_ids($id);
        
        $data="";
       
        foreach($patients as $key=>$value){
            
            $data .=$value->patientid."=_";
        }
        
        $data=rtrim($data,"=_");
        echo $data;
        exit;
    }
    
    function services(){
        $service=$this->input->post('srv');
       
        $srvs=$this->Administration_model->services(null,$service);
        
        $data="";
       
        foreach($srvs as $key=>$value){
            
            $data .=$value->name."=_";
        }
        
        $data=rtrim($data,"=_");
        echo $data;
        exit;
    }
}