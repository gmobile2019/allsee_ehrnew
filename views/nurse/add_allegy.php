<script type="text/javascript">
    $(document).ready(function(){
       
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Nursing/add_allegy/'.$patientid.'/'.$patientvisitid.'/'.$inpatientCare); 
                ?>
                <div class="form-group row">
                    <label for="allegy" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Allergy&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="allegy" id="allegy" placeholder="Allergy" value="<?php echo set_value('allegy'); ?> " />
                        <?php echo form_error('allegy'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="allegystatus" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Status&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="allegystatus" id="allegystatus" class="form-control" >
                            <option value="Current" <?php echo set_select('allegystatus','Current')?>>Current</option>
                            <option value="Past" <?php echo set_select('allegystatus','Past')?>>Past</option>
                        </select>
                        <?php echo form_error('allegystatus'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="remarks" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Remarks</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="remarks" id="remarks" ><?php echo set_value('remarks'); ?></textarea>
                        <?php echo form_error('remarks'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
