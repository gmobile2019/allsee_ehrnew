<script type="text/javascript">
    $(document).ready(function(){
      
    });
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Nursing/allocate_reallocate_nurse/'.$id); 
                ?>
                <div class="form-group row">
                    <label for="ward" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Ward&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="ward" id="ward" class="form-control" >
                        <option></option>
                        <?php foreach($wards as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo set_select('ward',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('ward'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save Allocation</button>
                    </div>
                </div>
        <?php echo form_close(); ?>        
</div>
