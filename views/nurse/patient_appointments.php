<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open("Nursing/patient_appointments/$patientid/$patientvisitid",$attributes); 
                ?>
                  
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start (Yyyy-mm-dd)" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End (Yyyy-mm-dd)" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                        <select name="status" id="status" class="form-control" >
                            <option value="" >Status</option>
                            <option value="1" <?php echo $status == 1?"selected='selected'":""?>>Pending</option>
                            <option value="2" <?php echo $status == 2?"selected='selected'":""?>>Confirmed</option>
                            <option value="3" <?php echo $status == 3?"selected='selected'":""?>>Cancelled</option>
                            
                        </select>
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Nursing/patient_appointments/$patientid/$patientvisitid/".$start."_".$end.'_'.$status."_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">export</span>'); ?>
        &nbsp;&nbsp;
        <?php echo anchor("Nursing/add_appointment/$patientid/$patientvisitid/",'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Record">Record Appointment</span>'); ?>
        
    </div>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Visit Id</th>
                <th style="text-align:center;">Appointment Doctor</th>
                <th style="text-align:center;">Appointment Date</th>
                <th style="text-align:center;">Appointment Time</th>
                <th style="text-align:center;">Appointment Status</th>
                <th style="text-align:center;">Action Date</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($appointments != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($appointments as $key=>$value){
                    $doc=$this->SuperAdministration_model->get_member_info($value->appointmentdoctor);
                    $doc=$doc[0]->first_name.' '.$doc[0]->last_name;
                    
                    if($value->status == 1){
                        $status='Pending';
                    }
                    
                    if($value->status == 2){
                        
                        $status='Confirmed';
                    }
                    
                    if($value->status == 3){
                        
                        $status='Cancelled';
                    }
                    
                    
                    
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->patientvisitid; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $doc; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->appointmentdate; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->appointmenttime; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $status; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                        <td>
                            &nbsp;&nbsp;<?php echo $value->status == 1?anchor('Nursing/cancel_appointment/'.$value->id.'/'.$patientid.'/'.$patientvisitid,'<span class="glyphicon glyphicon-remove" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cancel"></span>'):''; ?>
                       </td>
                    </tr>  
                <?php } ?>
                     
                <?php }else{ ?>
            <tr>
                <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
     <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
