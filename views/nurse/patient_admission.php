<script type="text/javascript">
    $(document).ready(function(){
        var ward;
        var bed;
        
        ward=$('select[name=ward]').val();
        
        $('select[name=bed]').find('option').each(function(){
            
            bed=$(this).attr('class');
            
            if(bed == 'ward_'+ward){
                $(this).show();
            }else{
                $(this).hide();
            }
        });
       
       
       $('select[name=ward]').change(function(){
           ward=$(this).val();
           $('select[name=bed]').val('');
           $('select[name=bed]').find('option').each(function(){
            
                bed=$(this).attr('class');

                if(bed == 'ward_'+ward){
                    $(this).show();
                }else{
                    $(this).hide();
                }
            });
       });
    });
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Nursing/patient_admission/'.$patientid.'/'.$patientvisitid.'/'.$admissionid); 
                ?>
                <div class="form-group row">
                    <label for="ward" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Ward&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="ward" id="ward" class="form-control" >
                        <option></option>
                        <?php foreach($wards as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo set_select('ward',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('ward'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="bed" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Bed&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="bed" id="bed" class="form-control" >
                        <option></option>
                        <?php foreach($beds as $key=>$value){ ?>
                        
                        <option class="ward_<?php echo $value->wardid?>" value="<?php echo $value->id; ?>" <?php echo set_select('bed',$value->id); ?>><?php echo $value->bedidentity; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('bed'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="admissiontype" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Admission Type&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="admissiontype" id="admissiontype" class="form-control" >
                        <option></option>
                        <?php foreach($admissiontypes as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo set_select('admissiontype',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('admissiontype'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="admissionsource" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Admission Source&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="admissionsource" id="admissionsource" class="form-control" >
                        <option></option>
                        <?php foreach($admissionsources as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo set_select('admissionsource',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('admissionsource'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Process Admission</button>
                    </div>
                </div>
        <?php echo form_close(); ?>        
</div>
