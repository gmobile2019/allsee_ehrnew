<script type="text/javascript">
    $(document).ready(function(){
       
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Nursing/add_physical_examination/'.$patientid.'/'.$patientvisitid.'/'.$inpatientCare); 
                ?>
                <div class="form-group row">
                    <label for="cardiovascular_system" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Cardiovascular System</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="cardiovascular_system" id="cardiovascular_system" ><?php echo set_value('cardiovascular_system'); ?></textarea>
                        <?php echo form_error('cardiovascular_system'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="respiratory_system" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Respiratory System</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="respiratory_system" id="respiratory_system" ><?php echo set_value('respiratory_system'); ?></textarea>
                        <?php echo form_error('respiratory_system'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="abdomen" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Abdomen</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="abdomen" id="abdomen" ><?php echo set_value('abdomen'); ?></textarea>
                        <?php echo form_error('abdomen'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="central_nervous_system" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Central Nervous System</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="central_nervous_system" id="central_nervous_system" ><?php echo set_value('central_nervous_system'); ?></textarea>
                        <?php echo form_error('central_nervous_system'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="muscular_skeletal_system" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Muscular Skeletal System</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="muscular_skeletal_system" id="muscular_skeletal_system" ><?php echo set_value('muscular_skeletal_system'); ?></textarea>
                        <?php echo form_error('muscular_skeletal_system'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
