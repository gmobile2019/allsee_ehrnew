<script type="text/javascript">
    $(document).ready(function(){
       
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Nursing/add_vitals_examination/'.$patientid.'/'.$patientvisitid.'/'.$inpatientCare); 
                ?>
                <div class="form-group row">
                    <label for="weight" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Weight (Kg)</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="weight" id="weight" placeholder="Weight" value="<?php echo set_value('weight'); ?>"/>
                        <?php echo form_error('weight'); ?>
                    </div>
                </div>
               <div class="form-group row">
                    <label for="height" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Height (m)</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="height" id="height" placeholder="Height" value="<?php echo set_value('height'); ?>"/>
                        <?php echo form_error('height'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="temperature" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Temperature (C)</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="temperature" id="temperature" placeholder="Temperature" value="<?php echo set_value('temperature'); ?>"/>
                        <?php echo form_error('temperature'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pulserate" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Pulse Rate (b/m)</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="pulserate" id="pulserate" placeholder="Pulse Rate" value="<?php echo set_value('pulserate'); ?>"/>
                        <?php echo form_error('pulserate'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="bloodpressure" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Blood Pressure (mmHg)</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="bloodpressure" id="bloodpressure" placeholder="Blood Pressure" value="<?php echo set_value('weight'); ?>"/>
                        <?php echo form_error('bloodpressure'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="respirationrate" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Respiration Rate (c/m</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="respirationrate" id="respirationrate" placeholder="Respiration Rate" value="<?php echo set_value('respirationrate'); ?>"/>
                        <?php echo form_error('respirationrate'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="spo2" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">SPO2 (%)</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="spo2" id="spo2" placeholder="SPO2" value="<?php echo set_value('spo2'); ?>"/>
                        <?php echo form_error('spo2'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
