<script type="text/javascript">
    $(document).ready(function(){
       
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Nursing/write_nurse_notes/'.$patientid.'/'.$patientvisitid); 
                ?>
                <div class="form-group row">
                    <label for="admissionid" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Admission Id</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" id="admissionid" readonly="true" value="<?php echo $admissionid; ?>"/>
                        <input type="hidden" class="form-control" name="admissionid" id="admissionid" value="<?php echo set_value('admissionid',$admissionid); ?>"/>
                        <?php echo form_error('admissionid'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="currentdiagnosis" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Current Diagnosis&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="currentdiagnosis" id="currentdiagnosis" ><?php echo set_value('currentdiagnosis'); ?></textarea>
                        <?php echo form_error('currentdiagnosis'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="treatment" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Treatment&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="treatment" id="treatment" ><?php echo set_value('treatment'); ?></textarea>
                        <?php echo form_error('treatment'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="procedures" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Procedures&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="procedures" id="procedures" ><?php echo set_value('procedures'); ?></textarea>
                        <?php echo form_error('procedures'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="medications" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Medications&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="medications" id="medications" ><?php echo set_value('medications'); ?></textarea>
                        <?php echo form_error('medications'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="medicationeffect" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Medication Effects&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7">
                        <label class="checkbox-inline">
                          <input type="radio" id="medicationeffect" name="medicationeffect" value="Positive" <?php echo set_radio('medicationeffect',"Positive"); ?>> Positive
                      </label>
                      <label class="checkbox-inline">
                           <input type="radio" id="medicationeffect" name="medicationeffect" value="Negative" <?php echo set_radio('medicationeffect',"Negative")?>> Negative
                      </label>
                      <label class="checkbox-inline">
                           <input type="radio" id="medicationeffect" name="medicationeffect" value="Observing" <?php echo set_radio('medicationeffect',"Observing")?>> Observing
                      </label>
                      <label class="checkbox-inline">
                           <input type="radio" id="medicationeffect" name="medicationeffect" value="Not Applicable" <?php echo set_radio('medicationeffect',"Not Applicable")?>> Not Applicable
                      </label>
                          <?php echo form_error('medicationeffect'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="medicationcomments" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Medication Comments&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="medicationcomments" id="medicationcomments" ><?php echo set_value('medicationcomments'); ?></textarea>
                        <?php echo form_error('medicationcomments'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="intakes" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Intakes&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="intakes" id="intakes" ><?php echo set_value('intakes'); ?></textarea>
                        <?php echo form_error('intakes'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="outputs" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Outputs&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="outputs" id="overallprogress" ><?php echo set_value('outputs'); ?></textarea>
                        <?php echo form_error('outputs'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="overallprogress" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Overall Progress&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="overallprogress" id="overallprogress" ><?php echo set_value('overallprogress'); ?></textarea>
                        <?php echo form_error('overallprogress'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="remarks" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Remarks&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="remarks" id="remarks" ><?php echo set_value('remarks'); ?></textarea>
                        <?php echo form_error('remarks'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success btn-block">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
