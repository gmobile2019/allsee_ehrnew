
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Nursing/patient_medical_file/'); 
                ?>
       
                <div class="form-group row">
                    <label for="patientid" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Patient Id&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="patientid" id="patientid" placeholder="Patient Id" value="<?php echo set_value('patientid',$patientid); ?>" />
                        <?php echo form_error('patientid'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Generate</button>
                    </div>
                </div>
        
        <?php 
        echo form_close(); 
        if($visits <> null){
            
            $this->load->view($patientFile);
            ?>
            
        <?php } ?>        
</div>
