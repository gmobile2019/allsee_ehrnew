<script type="text/javascript">
    $(document).ready(function(){
            var fileid;
            var divid;
            var id;
           
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
        
        $('div.filedata').find('span#show').show();
        $('div.filedata').find('span#hide').hide();
        
        $('div.filedata').each(function(){
            
            $(this).find('table').hide();
            
            $(this).find('span#show').find('a').click(function(){
                divid=$(this).parent().parent().parent().attr('id');
                id=divid.split("_");
                fileid=id[1];
                $('table#table_'+fileid).show('slow');
                $(this).parent().parent().find('span#show').hide();
                $(this).parent().parent().find('span#hide').show();
            });
            
            $(this).find('span#hide').find('a').click(function(){
                divid=$(this).parent().parent().parent().attr('id');
                id=divid.split("_");
                fileid=id[1];
                $('table#table_'+fileid).hide('slow');
                $(this).parent().parent().find('span#show').show();
                $(this).parent().parent().find('span#hide').hide();
            });
        });
    });
</script>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Nursing/patient_medical_file/$patientid/".TRUE,'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">export</span>'); ?>
    </div>
</div>
<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
        <?php foreach($visits as $key=>$value){ ?>
            <div  id="div_<?php echo $value->patientvisitid?>" class="well filedata">
                <div style="text-align:right">
                   <span style="font-style: italic;font-weight: bold;text-transform: capitalize"><a href="#">Visit Timestamp : <?php echo $value->createdon; ?></a></span>
                    &nbsp;&nbsp;<span id="show" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="show"><a href="#" class="glyphicon glyphicon-menu-down"></a></span>
                    &nbsp;&nbsp;<span id="hide" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="hide"><a href="#" class="glyphicon glyphicon-menu-up"></a></span> 
                
                </div>
               <table class="table table-condensed table-hover table-striped table-bordered" id="table_<?php echo $value->patientvisitid ?>">

                <tbody>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo anchor('Nursing/patient_file_details/'.$value->patientid.'/'.$value->patientvisitid.'/PTOO1','Physical Examinations')?></td>
                    </tr>
                    <tr>
                         <td>&nbsp;&nbsp;<?php echo anchor('Nursing/patient_file_details/'.$value->patientid.'/'.$value->patientvisitid.'/PTOO2','Vital Examinations')?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo anchor('Nursing/patient_file_details/'.$value->patientid.'/'.$value->patientvisitid.'/PTOO3','Diagnosis')?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo anchor('Nursing/patient_file_details/'.$value->patientid.'/'.$value->patientvisitid.'/PTOO4','Prescriptions')?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo anchor('Nursing/patient_file_details/'.$value->patientid.'/'.$value->patientvisitid.'/PTOO5','Services')?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo anchor('Nursing/patient_file_details/'.$value->patientid.'/'.$value->patientvisitid.'/PTOO6','Nurse Notes')?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo anchor('Nursing/patient_file_details/'.$value->patientid.'/'.$value->patientvisitid.'/PTOO7','Doctor Notes')?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo anchor('Nursing/patient_file_details/'.$value->patientid.'/'.$value->patientvisitid.'/PTOO8','Allegies')?></td>
                    </tr>
            </tbody>
        </table>
            </div>
        <?php } ?>
    </div>
    
</div>
