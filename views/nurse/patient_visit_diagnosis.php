
<div style="padding-top:10px" class="row">
  <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;" colspan="7">Diagnosis</th>
            </tr>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Complains</th>
                <th style="text-align:center;">Provisional Diagnosis</th>
                <th style="text-align:center;">Final Diagnosis</th>
                <th style="text-align:center;">Comments</th>
                <th style="text-align:center;">Action Date</th>
                <th style="text-align:center;">Doctor</th>
             </tr>
        </thead>
        <tbody>
            <?php if($diagnosis != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($diagnosis as $key=>$value){
                    $doc1=$this->SuperAdministration_model->get_member_info($value->createdby);
                    $doc1=$doc1[0]->first_name.' '.$doc1[0]->last_name;
                    
                    $doc2=$this->SuperAdministration_model->get_member_info($value->modifiedby);
                    $doc2=$doc2[0]->first_name.' '.$doc2[0]->last_name;
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->complains; ?></td>
                        <td>
                            <ul>
                            <?php 
                            $provisional=explode($this->config->item('multi_diagnosis_glue'),$value->provisionaldiagnosis);
                            foreach($provisional as $val){
                                $name=$this->Clinical_model->standard_complains(null,$val);
                                echo "<li>".$name[0]->name."</li>";
                            }
                            ?>
                            </ul>
                        </td>
                        <td>
                            <ul>
                            <?php 
                            $final=explode($this->config->item('multi_diagnosis_glue'),$value->finaldiagnosis);
                            foreach($final as $val){
                                $name=$this->Clinical_model->standard_complains(null,$val);
                                echo "<li>".$name[0]->name."</li>";
                            }
                            ?>
                            </ul>
                        </td>
                        <td>&nbsp;&nbsp;<?php echo $value->finalcomment; ?></td>
                        <td>
                            <ul>
                                <li>Provisional : <?php echo $value->createdon; ?></li>
                                <li>Final : <?php echo $value->modifiedon; ?></li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li>Provisional : <?php echo $doc1; ?></li>
                                <li>Final : <?php echo $doc2; ?></li>
                            </ul>
                        </td>
                    </tr>  
                <?php } ?>
                     
                <?php }else{ ?>
            <tr>
                <td colspan="7" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
</div>
