<script type="text/javascript">
    $(document).ready(function(){
       
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
                    if($pending_debts == null){
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Nursing/inpatient_discharge/'.$patientid.'/'.$patientvisitid.'/'); 
                ?>
                <div class="form-group row">
                    <label for="patientid" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Patient Id</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" readonly="true" value="<?php echo $patientid; ?>"/>
                        <input type="hidden" name="patientid" value="<?php echo $patientid; ?>"/>
                        <?php echo form_error('patientid'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="patientvisitid" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Patient Visit Id</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" readonly="true" value="<?php echo $patientvisitid; ?>"/>
                        <input type="hidden" name="patientvisitid" value="<?php echo $patientvisitid; ?>"/>
                        <?php echo form_error('patientvisitid'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="admissionid" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Admission Id</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" readonly="true" value="<?php echo $admissionid; ?>"/>
                        <input type="hidden" name="admissionid" value="<?php echo $admissionid; ?>"/>
                        <?php echo form_error('admissionid'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="reason" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Discharge Reason</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="reason" id="reason" ><?php echo set_value('reason'); ?></textarea>
                        <?php echo form_error('reason'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="dischargenotes" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Discharge Notes</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="dischargenotes" id="dischargenotes" ><?php echo set_value('dischargenotes'); ?></textarea>
                        <?php echo form_error('dischargenotes'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
                    <?php echo form_close(); } else{ ?>
    
                        <div class="alert alert-danger " role="alert" style="text-align:center;">clear pending debts first before discharge!</div>
                        
                    <?php } ?>        
</div>
