
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Nursing/nurses',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="ward"></label>
                        <select name="ward" id="ward" class="form-control" >
                        <option></option>
                        <?php foreach($wards as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo $ward == $value->id?"selected='selected'":""; ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                        </select>
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Ward</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($data != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($data as $key=>$value){
                    $wrd=$this->Inventory_model->units($value->inventoryunit);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->first_name.' '.$value->middle_name.' '.$value->last_name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->inventoryunit<>null?$wrd[0]->name:''; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->active == 1?'Active':'Suspended'; ?></td>
                        <td style="text-align:center">
                            &nbsp;&nbsp;
                            <?php 
                                $incharge=$this->SuperAdministration_model->nurses_incharge($value->userid,1);
                                $locate=$value->inventoryunit <> null?'Reallocate':'Allocate'; 
                                echo $incharge?'':anchor('Nursing/allocate_reallocate_nurse/'.$value->userid,'<span class="glyphicon glyphicon-share" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="'.$locate.'"></span>'); 
                            ?>
                            &nbsp;&nbsp;
                        </td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="5" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
