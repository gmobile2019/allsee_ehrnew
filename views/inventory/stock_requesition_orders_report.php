<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Inventory/requesition_orders_report',$attributes); 
                ?>
                    
                    <div class="form-group ">
                        <label class="sr-only" for="unit"></label>
                        <select name="unit" id="unit" class="form-control" >
                                <option value="" >Unit</option>
                                <option value="main" <?php echo ($unit == 'main' )?'selected="selected"':''; ?>>Main Store</option>
                                <?php foreach($units as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($unit == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="item"></label>
                        <input type="text" class="form-control" name="item" id="item" placeholder="Item" value="<?php echo $item; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start (yyyy-mm-dd)" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End (yyyy-mm-dd)" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="status"></label>
                        <select name="status" id="unit" class="form-control" >
                                <option value="" >Status</option>
                                <option value="1" <?php echo ($status == 1 )?'selected="selected"':''; ?>>Confirmed</option>
                                <option value="2" <?php echo ($status == 2 )?'selected="selected"':''; ?>>Declined</option>
                                <option value="3" <?php echo ($status == 3 )?'selected="selected"':''; ?>>Pending</option>
                        </select>
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Inventory/requesition_orders_report/1_".$unit."_".$item."_".$start."_".$end."_".$status."/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php echo anchor("Inventory/requesition_orders_report/2_".$unit."_".$item."_".$start."_".$end."_".$status."/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
    </div>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Item</th>
                <th style="text-align:center;">R/Inventory Unit</th>
                <th style="text-align:center;">R/Quantity</th>
                <th style="text-align:center;">R/Action Timestamp</th>
                <th style="text-align:center;">S/Inventory Unit</th>
                <th style="text-align:center;">S/Quantity</th>
                <th style="text-align:center;">S/Action Timestamp</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($orders != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($orders as $key=>$value){
                    $p_unit='Main Store';
                    if($value->supplyunit <> null){
                        //supplier
                        $p_unit=$this->Inventory_model->units($value->supplyunit);
                        $p_unit=$p_unit[0]->name;
                    }
                    
                    $r_unit=$this->Inventory_model->units($value->inventoryunit);
                    
                    
                    if($value->status == 1){
                        $r_status='Confirmed';
                    }
                    if($value->status == 2){
                        $r_status='Declined';
                    }
                    if($value->status == 3){
                        $r_status='Pending';
                    }
                    
                    
                    
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $r_unit[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->requestqty; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $p_unit; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->supplyqty; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->modifiedon; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $r_status; ?></td>
                        <td style="text-align:center">
                            <?php
                            if($value->status == 3 && $value->supplyunit == null){
                                 echo anchor('Inventory/process_requesition_order/'.$value->requesitionid.'/'.$value->itemid.'/1','<span class="glyphicon glyphicon-ok" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Confirm Order"></span>')
                                    ."&nbsp;&nbsp;".
                                    anchor('Inventory/process_requesition_order/'.$value->requesitionid.'/'.$value->itemid.'/2','<span class="glyphicon glyphicon-remove" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Decline Order"></span>');
                            }
                            ?>
                        
                        </td>
                        
                    </tr>  
                <?php } 
                }else{ ?>
            <tr>
                <td colspan="10" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
