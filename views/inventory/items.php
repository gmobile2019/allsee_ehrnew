
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Inventory/view_items/'.$code,$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                    </div>
    
                    <?php if($code == $this->config->item('drug_code')){ ?>
                    <div class="form-group ">
                        <label class="sr-only" for="drug_type"></label>
                        <select name="drug_type" id="drug_type" class="form-control" >
                                <option value="" ></option>
                                <?php foreach($drug_types as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($drug_type == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="drug_grp"></label>
                        <select name="drug_grp" id="drug_grp" class="form-control" >
                                <option value="" ></option>
                                <?php foreach($drug_groups as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($drug_grp == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Inventory/view_items/$code/name_".$name."_grp_".$drug_grp."_type_".$drug_type."_doctype_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php echo anchor("Inventory/view_items/$code/name_".$name."_grp_".$drug_grp."_type_".$drug_type."_doctype_2/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
</div>
<div style="padding-top:10px" class="row">
	<table class="table table-condensed table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Category</th>
                <th style="text-align:center;">Drug Group</th>
                <th style="text-align:center;">Drug Type</th>
                <th style="text-align:center;">Consumption Category</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($items != null){
               
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($items as $key=>$value){
                    $ct=$this->Inventory_model->get_item_category_by_code($value->category);
                    if($code == $this->config->item('drug_code')){
                        $drug_grp=$this->Inventory_model->drug_groups($value->drug_group);
                        $drug_type=$this->Inventory_model->drug_types($value->drug_type);
                    }
                    
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $ct->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $drug_grp[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $drug_type[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->consumptioncategory == 1?'Selling':'Not Selling'; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status == 1?'Active':'Suspended'; ?></td>
                        <?php 
                        
                        $active_status=$value->status == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 
                        
                        ?>
                        <td>&nbsp;&nbsp;
                            <?php echo anchor('Inventory/add_item/'.$value->id,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('Inventory/activate_deactivate_item/'.$value->id.'/'.$value->status.'/'.$code,$active_status); ?>
                        </td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>

    
</div>
