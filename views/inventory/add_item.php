<script type="text/javascript">
    $(document).ready(function(){
        var category;
        var drug_code;
        var additionType;
        
        drug_code=$('input[name=drug_code').val();
        category=$('select[name=category]').val();
        
        if(category == drug_code){
            $('div.drug').show();
        }else{
            $('div.drug').hide();
        }
        
        $('select[name=category]').change(function(){
           category=$(this).val();
        
           if(category == drug_code){
                $('div.drug').show();
            }else{
                $('div.drug').hide();
            }
       });
       
    /*
        * addType radio button
       */
       $('input[name=additionType]').each(function(){
          additionType=$(this).val();
          
          if($(this).is(':checked')){
              
               if(additionType == 1){
                
                    $('div.single').show();
                    $('div.bulk').hide();
                }else{

                    $('div.bulk').show();
                    $('div.single').hide();
                }
          }
       });
       
       
       $('input[name=additionType]').change(function(){
            
             additionType=$(this).val();
             if($(this).is(':checked')){
                 
                if(additionType == 1){
                
                    $('div.single').show('slow');
                    $('div.bulk').hide('slow');
                }else{

                    $('div.bulk').show('slow');
                    $('div.single').hide('slow');
                }
             }
        });
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Inventory/add_item/'.$id); 
                ?>
                <div class="form-group row">
                    <label for="additionType" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Addition Type &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <label class="checkbox-inline">
                          <input type="radio" id="additionType" name="additionType" value="1" <?php echo set_radio('additionType',"1"); ?> checked="checked"> Single
                      </label>
                       <label class="checkbox-inline">
                           <input type="radio" id="additionType" name="additionType" value="2" <?php echo set_radio('additionType',"2")?>> Bulk
                      </label>
                    </div>
                </div>
                <div class="form-group row single">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $id != null?$item[0]->name:set_value('name'); ?>" />
                        <?php echo form_error('name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="category" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Category&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="category" id="category" class="form-control" >
                            <option></option>
                            <?php foreach($item_categories as $key=>$value){ ?>

                            <option value="<?php echo $value->code; ?>" <?php echo ($id != null && trim($value->code) == trim($item[0]->category))?'selected="selected"':set_select('category',$value->code); ?>><?php echo $value->name; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('category'); ?>
                    </div>
                </div>
                <input type="hidden" value="<?php echo $this->config->item('drug_code'); ?>" name="drug_code"/>
                <div class="form-group row drug">
                    <label for="drug_group" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Drug Group&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="drug_group" id="drug_group" class="form-control" >
                            <option></option>
                            <?php foreach($drug_groups as $key=>$value){ ?>

                            <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($item[0]->drug_group))?'selected="selected"':set_select('drug_group',$value->id); ?>><?php echo $value->name; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('drug_group'); ?>
                    </div>
                </div>
                <div class="form-group row drug">
                    <label for="drug_type" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Drug Type&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="drug_type" id="drug_type" class="form-control" >
                            <option></option>
                            <?php foreach($drug_types as $key=>$value){ ?>

                            <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($item[0]->drug_type))?'selected="selected"':set_select('drug_type',$value->id); ?>><?php echo $value->name; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('drug_type'); ?>
                    </div>
                </div>
                <div class="form-group row bulk">
                    <label for="upload" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Excel Upload</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="upload" id="upload" value="Upload"/>
                        <?php echo form_error('upload'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="consumptioncategory" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Consumption Category &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <label class="checkbox-inline">
                          <input type="radio" id="consumptioncategory" name="consumptioncategory" value="1" <?php echo $id != null && $item[0]->consumptioncategory ==1?'checked="checked"':set_radio('consumptioncategory',"1"); ?> > Selling
                      </label>
                       <label class="checkbox-inline">
                           <input type="radio" id="consumptioncategory" name="consumptioncategory" value="2" <?php echo $id != null && $item[0]->consumptioncategory == 2?'checked="checked"':set_radio('consumptioncategory',"2")?>> Not Selling
                      </label>
                        <?php echo form_error('consumptioncategory'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
