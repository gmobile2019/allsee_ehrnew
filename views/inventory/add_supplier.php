<script type="text/javascript">
    $(document).ready(function(){
        
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Inventory/add_supplier/'.$id); 
                ?>
                <div class="form-group row">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $id != null?$supplier[0]->name:set_value('name'); ?>" />
                        <?php echo form_error('name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Phone&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo $id != null?$supplier[0]->phone:set_value('phone'); ?>" />
                        <?php echo form_error('phone'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Email&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $id != null?$supplier[0]->email:set_value('email'); ?>" />
                        <?php echo form_error('email'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="physical" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Physical Address&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="physical" id="physical" placeholder="Physical Address" value="<?php echo $id != null?$supplier[0]->physicaladdress:set_value('physical'); ?>" />
                        <?php echo form_error('physical'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="category" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Category&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="category" id="category" class="form-control" >
                            <option></option>
                            <?php foreach($item_categories as $key=>$value){ ?>

                            <option value="<?php echo $value->code; ?>" <?php echo ($id != null && trim($value->code) == trim($supplier[0]->category))?'selected="selected"':set_select('category',$value->code); ?>><?php echo $value->name; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('category'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
