<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<div class="row">
            <div class="col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <?php echo anchor('Inventory/main_stock/'.$code,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back"></span>'); ?>
            </div>
        <?php 
        foreach($item_inventory_details as $key=>$value){ 
                $sp=$this->Inventory_model->suppliers($value['supplierid']);
        ?>
     

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table table-condensed table-hover table-striped table-bordered">
                    <tbody>
                        <tr>
                            <td>&nbsp;&nbsp;System Batch</td>
                            <td style="font-weight: bold">&nbsp;&nbsp;<?php echo $value['batch']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;Store Batch</td>
                            <td style="font-weight: bold">&nbsp;&nbsp;<?php echo $value['storebatch']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;Expire Date</td>
                            <td style="font-weight: bold">&nbsp;&nbsp;<?php echo $value['expiredate']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;Supplier</td>
                            <td style="font-weight: bold">&nbsp;&nbsp;<?php echo $sp[0]->name; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;Quantity</td>
                            <td style="font-weight: bold">&nbsp;&nbsp;<?php echo $value['quantity']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;Unit Cost</td>
                            <td style="font-weight: bold">&nbsp;&nbsp;<?php echo number_format($value['unitprice'], 2); ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;Stock Value</td>
                            <td style="font-weight: bold">&nbsp;&nbsp;<?php echo number_format($value['totalvalue'], 2); ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;Latest Stocking</td>
                            <td style="font-weight: bold">&nbsp;&nbsp;<?php echo $value['stocking']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;Latest De-Stocking</td>
                            <td style="font-weight: bold">&nbsp;&nbsp;<?php echo $value['destocking']; ?></td>
                        </tr>
                   </tbody>
                </table>
            </div>
                    
        <?php } ?>
        </div>
    </div>
    
</div>
