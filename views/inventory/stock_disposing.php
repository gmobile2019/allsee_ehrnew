<script type="text/javascript">
    $(document).ready(function(){
        var category;
        var source;
        var item_class;
        var item;
        var batch;
       
        category=$('select[name=item_category]').val();
       
        $('select[name=item]').find('option').each(function(){
           item_class=$(this).attr('class');

           if(item_class == "category_"+category){
               $(this).show();
           }else{
               $(this).hide();
           }

        });
             
        $('select[name=item_category]').change(function(){
           
            category=$(this).val();
           
           $('select[name=item]').find('option').each(function(){
                item_class=$(this).attr('class');

                if(item_class == "category_"+category){
                    $(this).show();
                }else{
                    $(this).hide();
                }
                
             });
       });
       
        $('select[name=batch]').find('option').hide();
        item=$('select[name=item]').val();
        
        $.ajax({
               type:'POST',
                url:'<?php echo site_url('Inventory/item_batches'); ?>',
                data:{itemid:item},
                success:function(data){
                    
                    var storebatch=data.split("||");
                    var len=storebatch.length;
                    var i;
                    for(i=0;i< len;i++){
                        
                        $('select[name=batch]').find('option').each(function(){
                            
                            batch=$(this).attr('class');
                            if(batch == 'batch_'+storebatch[i]){
                                
                                $(this).show();
                            }
                        });
                    }
                }
           });
           
       $('select[name=item]').change(function(){
            item=$(this).val();
           
            $.ajax({
               type:'POST',
                url:'<?php echo site_url('Inventory/item_batches'); ?>',
                data:{itemid:item},
                success:function(data){
                    
                    var storebatch=data.split("||");
                    var len=storebatch.length;
                   // alert(len)
                    var i;
                    for(i=0;i< len;i++){
                        
                        $('select[name=batch]').find('option').each(function(){
                            
                            batch=$(this).attr('class');
                            if(batch == 'batch_'+storebatch[i]){
                                
                                $(this).show();
                            }
                        });
                    }
             }
           });
       });
    
       
    /*
        * source radio button
       */
       $('input[name=source]').each(function(){
          source=$(this).val();
          
          if($(this).is(':checked')){
              
               if(source == 1){
                
                    $('div.unit').hide('slow');
                }else{

                    $('div.unit').show('slow');
                }
          }
       });
       
       
       $('input[name=source]').change(function(){
            
             source=$(this).val();
             if($(this).is(':checked')){
                 
                if(source == 1){
                
                    $('div.unit').hide('slow');
                }else{

                    $('div.unit').show('slow');
                }
             }
        });
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Inventory/stock_dispose/'.$id); 
                ?>
                <div class="form-group row">
                    <label for="source" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Source &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <label class="checkbox-inline">
                          <input type="radio" id="source" name="source" value="1" <?php echo set_radio('source',"1"); ?> checked="checked"> Main Store
                      </label>
                       <label class="checkbox-inline">
                           <input type="radio" id="source" name="source" value="2" <?php echo set_radio('source',"2")?>> Units
                      </label>
                    </div>
                </div>
                <div class="form-group row unit">
                    <label for="unit" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Unit&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="unit" id="unit" class="form-control" >
                            <option></option>
                            <?php foreach($units as $key=>$value){ ?>

                            <option value="<?php echo $value->id; ?>" <?php echo set_select('unit',$value->id); ?>><?php echo $value->name; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('unit'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="item_category" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Item Category&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="item_category" id="item_category" class="form-control" >
                            <option></option>
                            <?php foreach($item_categories as $key=>$value){ ?>

                            <option value="<?php echo $value->code; ?>" <?php echo set_select('item_category',$value->code); ?>><?php echo $value->name; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('item_category'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="item" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Item&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="item" id="item" class="form-control" >
                            <option></option>
                            <?php foreach($items as $key=>$value){ ?>

                            <option value="<?php echo $value->id; ?>" class="category_<?php echo $value->category; ?>" <?php echo set_select('item',$value->id); ?>><?php echo $value->name; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('item'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="batch" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Batch&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="batch" id="batch" class="form-control" >
                            <option></option>
                            <?php foreach($batches as $key=>$value){ ?>

                            <option value="<?php echo $value->storebatch; ?>" class="batch_<?php echo $value->storebatch; ?>" <?php echo set_select('batch',$value->storebatch); ?>><?php echo $value->storebatch; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('batch'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="reason" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Reason&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="reason" id="reason" class="form-control" >
                            <option></option>
                            <?php foreach($reasons as $key=>$value){ ?>

                            <option value="<?php echo $value->name; ?>" <?php echo set_select('reason',$value->name); ?>><?php echo $value->name; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('reason'); ?>
                    </div>
                </div>
               
                <div class="form-group row single">
                    <label for="quantity" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Quantity&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="number" class="form-control" name="quantity" id="quantity" placeholder="Quantity" value="<?php echo set_value('quantity'); ?>" />
                        <?php echo form_error('quantity'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
