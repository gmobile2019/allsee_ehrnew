<script type="text/javascript">
    $(document).ready(function(){
        var itemid;
     
        
        $('input[type=checkbox]').each(function(){

            itemid=$(this).val();

            $("#expire_"+itemid).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
        });
       
    });
</script>
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Inventory/stock_receiving/'.$code,$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                    </div>
    
                    <?php if($code == $this->config->item('drug_code')){ ?>
                    <div class="form-group ">
                        <label class="sr-only" for="drug_type"></label>
                        <select name="drug_type" id="drug_type" class="form-control" >
                                <option value="" ></option>
                                <?php foreach($drug_types as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($drug_type == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="drug_grp"></label>
                        <select name="drug_grp" id="drug_grp" class="form-control" >
                                <option value="" ></option>
                                <?php foreach($drug_groups as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($drug_grp == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Inventory/stock_receiving/'.$code,$attributes); 
                ?>
	<table class="table table-condensed table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Supplier</th>
                <th style="text-align:center;">Expire Date</th>
                <th style="text-align:center;">Quantity</th>
                <th style="text-align:center;">T/Value</th>
                <th style="text-align:center;">Batch</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($items != null){
               
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                    $j=0;
                foreach($items as $key=>$value){
                    
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;
                            <?php echo $i++; ?>
                        </td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?><input type="hidden" name="itemid_<?php echo $value->id?>" value="<?php echo $value->id?>"/></td>
                        <td>
                            &nbsp;&nbsp;
                            <select name="supplier_<?php echo $value->id; ?>" id="supplier_<?php echo $value->id; ?>" class="form-control" >
                                <option></option>
                                <?php foreach($suppliers as $ky=>$val){ ?>

                                <option value="<?php echo $val->id; ?>" <?php echo set_select('supplier_'.$value->id,$val->id); ?>><?php echo $val->name; ?></option>

                                <?php } ?>
                            </select>
                            <?php echo form_error('supplier_'.$value->id); ?>
                        </td>
                        <td>&nbsp;&nbsp;
                            <input type="text" class="form-control" name="expire_<?php echo $value->id; ?>" id="expire_<?php echo $value->id; ?>" placeholder="Yyyy-mm-dd" value="<?php echo set_value('expire_'.$value->id); ?>" />
                            <?php echo form_error('expire_'.$value->id); ?>
                        </td>
                        <td>&nbsp;&nbsp;
                            <input type="number" class="form-control" name="quantity_<?php echo $value->id; ?>" id="quantity_<?php echo $value->id; ?>" placeholder="Quantity" value="<?php echo set_value('quantity_'.$value->id); ?>" />
                            <?php echo form_error('quantity_'.$value->id); ?>
                        </td>
                        <td>&nbsp;&nbsp;
                            <input type="text" class="form-control" name="totalvalue_<?php echo $value->id; ?>" id="totalvalue_<?php echo $value->id; ?>" placeholder="Total Value" value="<?php echo set_value('totalvalue_'.$value->id); ?>" />
                            <?php echo form_error('totalvalue_'.$value->id); ?>
                        </td>
                        <td>&nbsp;&nbsp;
                            <input type="text" class="form-control" name="batch_<?php echo $value->id; ?>" id="batch_<?php echo $value->id; ?>" placeholder="Batch" value="<?php echo set_value('batch_'.$value->id); ?>" />
                            <?php echo form_error('batch_'.$value->id); ?>
                        </td>
                        <td>&nbsp;&nbsp;
                            <input type="checkbox" name="stocktaking[<?php echo $value->id; ?>]" id="stocktaking_<?php echo $value->id; ?>" value="<?php echo $value->id; ?>" <?php echo set_checkbox('stocktaking['.$value->id.']', $value->id)?>>
                        </td>
                    </tr>  
                <?php $j++; } ?>
                    <tr>
                        <td colspan="7" style="text-align:center"> <input type="submit" class="btn btn-success" name="save" value="Save"></td>
                    </tr>
              <?php  }else{ ?>
            <tr>
                <td colspan="7" style="text-align:center">NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
<?php echo form_close(); ?>
    
</div>
