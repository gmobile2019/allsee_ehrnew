<script type="text/javascript">
    $(document).ready(function(){
       
       $('div.display_content').find('table').find('tbody').find('tr').find('td.supplierdetails').find('a').click(function(){
                
                var id=$(this).attr('id');
               
                $.ajax({
                type:'POST',
                url:'<?php echo site_url('Inventory/modal_supplier_details'); ?>',
                data:{id:id},
                success:function(data){

                    var supplier=data.split("=_");
                    
                     $('div#myModal').find('table').find('td#name').text(supplier[0]);
                     $('div#myModal').find('table').find('td#phone').text(supplier[1]);
                     $('div#myModal').find('table').find('td#email').text(supplier[2]);
                     $('div#myModal').find('table').find('td#physical').text(supplier[3]);
                     $('div#myModal').find('table').find('td#category').text(supplier[4]);
                     
                     $('#myModal').modal({
                        keyboard:true,
                       show:true,
                       })
                 }

                });
 
            });
        });
</script>
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Inventory/suppliers',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Phone</th>
                <th style="text-align:center;">Category</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($suppliers != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($suppliers as $key=>$value){
                    
                    $ct=$value->category <> null?$this->Inventory_model->get_item_category_by_code($value->category):"";
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td class="supplierdetails">&nbsp;&nbsp;<a type="button" href="#" data-toggle="modal" id="<?php echo $value->id ?>"><?php echo $value->name ?></a></td>
                        <td>&nbsp;&nbsp;<?php echo $value->phone; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $ct->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status == 1?'Active':'Suspended'; ?></td>
                        <?php 
                        
                        $active_status=$value->status == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 
                        
                        ?>
                        <td style="text-align:center">
                            &nbsp;&nbsp;
                            <?php echo anchor('Inventory/add_supplier/'.$value->id,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('Inventory/activate_deactivate_supplier/'.$value->id.'/'.$value->status,$active_status); ?>
                        </td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="6" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    <!-- Modal -->
       <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="gridSystemModalLabel">Supplier Details</h4>
            </div>
            <div class="modal-body">
              <div class="row container">
                  <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                      <table class="table table-condensed table-hover">
                          <tr>
                              <td>Name</td>
                              <td id="name"></td>
                          </tr>
                          <tr>
                              <td>Phone</td>
                              <td id="phone"></td>
                          </tr>
                          <tr>
                              <td>Email</td>
                              <td id="email"></td>
                          </tr>
                          <tr>
                              <td>Physical Address</td>
                              <td id="physical"></td>
                          </tr>
                          <tr>
                              <td>Category</td>
                              <td id="category"></td>
                          </tr>
                      </table>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div> 
</div>
