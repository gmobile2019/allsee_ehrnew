<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Inventory/stock_reconcilliation',$attributes); 
                ?>
                    
                    <div class="form-group ">
                        <label class="sr-only" for="unit"></label>
                        <select name="unit" id="unit" class="form-control" >
                                <option value="" >Source Unit</option>
                                <option value="main" <?php echo ($unit == 'main' )?'selected="selected"':''; ?>>Main Store</option>
                                <?php foreach($units as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($unit == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="category"></label>
                        <select name="category" id="category" class="form-control" >
                                <option value="" >All Categories</option>
                                <?php foreach($item_categories as $key=>$value){ ?>

                                <option value="<?php echo $value->code; ?>" <?php echo ($category == $value->code )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="item"></label>
                        <input type="text" class="form-control" name="item" id="item" placeholder="Item" value="<?php echo $item; ?>" />
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Inventory/stock_reconcilliation/1_".$unit.'_'.$category."_".$item."/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php echo anchor("Inventory/stock_reconcilliation/2_".$unit.'_'.$category."_".$item."/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
    </div>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Inventory Unit</th>
                <th style="text-align:center;">Item</th>
                <th style="text-align:center;">Store Batch</th>
                <th style="text-align:center;">Current Stock</th>
                <th style="text-align:center;">Fresh Stock</th>
                <th style="text-align:center;">Expired Stock</th>
                <th style="text-align:center;">Disposed Stock</th>
             </tr>
        </thead>
        <tbody>
            <?php if($stock != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($stock as $key=>$value){
                    //$cat=$this->Inventory_model->get_item_category_by_code($value['category']);
                    $total_stock +=$value['total_stock'];
                    $total_value +=$value['total_value'];
                    $fresh_stock +=$value['fresh_stock'];
                    $fresh_value +=$value['fresh_value'];
                    $exp_stock +=$value['exp_stock'];
                    $exp_value +=$value['exp_value'];
                    $disp_stock +=$value['disp_stock'];
                    $disp_value +=$value['disp_value'];
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value['unit']; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value['item']; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value['storebatch']; ?></td>
                        <td style="text-align:right"><?php echo $value['total_stock'].'('.number_format($value['total_value'],2).')'; ?>&nbsp;&nbsp;</td>
                        <td style="text-align:right"><?php echo $value['fresh_stock'].'('.number_format($value['fresh_value'],2).')'; ?>&nbsp;&nbsp;</td>
                        <td style="text-align:right"><?php echo $value['exp_stock'].'('.number_format($value['exp_value'],2).')'; ?>&nbsp;&nbsp;</td>
                        <td style="text-align:right"><?php echo $value['disp_stock'].'('.number_format($value['disp_value'],2).')'; ?>&nbsp;&nbsp;</td>
                    </tr>  
                <?php } ?>
                    <tr>
                        <th colspan="4" style="text-align:center">Total</th>
                        <th style="text-align:right"><?php echo $total_stock.'('.number_format($total_value, 2).')'?></th>
                        <th style="text-align:right"><?php echo $fresh_stock.'('.number_format($fresh_value, 2).')'?></th>
                        <th style="text-align:right"><?php echo $exp_stock.'('.number_format($exp_value, 2).')'?></th>
                        <th style="text-align:right"><?php echo $disp_stock.'('.number_format($disp_value, 2).')'?></th>
                    </tr>  
                <?php }else{ ?>
            <tr>
                <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
