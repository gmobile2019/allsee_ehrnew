<script type="text/javascript">
    $(document).ready(function(){
             var department;
             var subdepartment;
             
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            department=$('select[name=department]').val();
            $('select[name=subdepartment]').find('option').each(function(){
                subdepartment=$(this).attr('class');

                if(subdepartment == "dpt_"+department){
                    $(this).show();
                }else{
                    $(this).hide();
                }
            });         
                     
            $('select[name=department]').change(function(){
                 department=$(this).val();
                
                 $('select[name=subdepartment]').find('option').each(function(){
                     subdepartment=$(this).attr('class');
                     
                     if(subdepartment == "dpt_"+department){
                         $(this).show();
                     }else{
                         $(this).hide();
                     }
                 });
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Administration/aggregated_collection/',$attributes); 
                  
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="sponsor"></label>
                        <select name="sponsor" id="sponsor" class="form-control" >
                                <option value="" >All Sponsors</option>
                                <?php foreach($sponsors as $key=>$value){ ?>

                                <option value="<?php echo $value->sponsorcode; ?>" <?php echo ($sponsor == $value->sponsorcode )?'selected="selected"':''; ?>><?php echo $value->shortname; ?></option>

                                <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="department"></label>
                        <select name="department" id="department" class="form-control" >
                        <option value="">All</option>
                        <?php foreach($departments as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($department == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="department"></label>
                       <select name="subdepartment" id="subdepartment" class="form-control" >
                        <option value="">All</option>
                        <?php foreach($subdepartments as $key=>$value){ ?>
                        
                        <option class="dpt_<?php echo $value->departmentid; ?>" value="<?php echo $value->id; ?>" <?php echo ($subdepartment == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                        </select>
                    </div>
                   <div class="form-group">
                       <button type="submit" class="btn btn-success">Generate</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Administration/aggregated_collection/start_".$start."_end_".$end."_sponsor_".$sponsor."_department_".$department."_subdepartment_".$subdepartment."_docType_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php //echo anchor("Reception/aggregated_collection/start_".$start."_end_".$end."_sponsor_".$sponsor."_department_".$department."_subdepartment_".$subdepartment."_docType_2/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
     
       <?php if($aggregated_collections['service_items'] <> null || (($aggregated_collections['prepaid_subscriptions'] <> null || $aggregated_collections['package_subscriptions'] <> null) && ($sponsor == null || $sponsor==$this->config->item('instant_cash_code')) )){
           
                if($aggregated_collections['service_items'] <> null){
                        if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    ?>
                        
                    <table class="table table-condensed table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center;" colspan="4">Service/Item Aggregated Collections</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;">S/NO</th>
                                <th style="text-align:center;">Department</th>
                                <th style="text-align:center;">Sub Department</th>
                                <th style="text-align:center;">Amount</th>
                             </tr>
                        </thead>
                        <tbody>
                        <?php foreach($aggregated_collections['service_items'] as $ky=>$val){ 
                                $dept=$this->Administration_model->departments($val->departmentid);
                                $sub_dept=$this->Administration_model->subdepartments($val->subdepartmentid);

                                $srv_itm_total +=$val->amount;
                            ?>
                         
            
                            <tr>
                                <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                                <td>&nbsp;&nbsp;<?php echo $dept[0]->name ?></td>
                                <td>&nbsp;&nbsp;<?php echo $sub_dept[0]->name; ?></td>
                                <td style="text-align: right"><?php echo number_format($val->amount, 2); ?>&nbsp;&nbsp;</td>
                            </tr>    
                        
                        <?php }?>
                            <tr>
                                <th style="text-align: right" colspan="3">Total&nbsp;&nbsp;</th>
                                <th style="text-align: right"><?php echo number_format($srv_itm_total, 2); ?>&nbsp;&nbsp;</th>
                            </tr>
                        </tbody>
                    </table> 
                <?php }
                
                if($aggregated_collections['prepaid_subscriptions'] <> null && ($sponsor == null || $sponsor==$this->config->item('instant_cash_code'))){ 
                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    ?>
                    <div>
                        <table class="table table-condensed table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;" colspan="3">Prepaid Subscriptions Aggregated Collections</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">S/NO</th>
                                    <th style="text-align:center;">Patient</th>
                                    <th style="text-align:center;">Amount</th>
                                 </tr>
                            </thead>
                            <tbody>
                                <?php foreach($aggregated_collections['prepaid_subscriptions'] as $ky=>$val){ 
                                $pr_total +=$val->amount;
                                $pt=$this->Reception_model->patient($val->patientid);
                                
                                ?>
                                <tr>
                                    <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                                    <td>&nbsp;&nbsp;<?php echo $pt->name; ?></td>
                                    <td style="text-align: right"><?php echo number_format($val->amount, 2); ?>&nbsp;&nbsp;</td>
                                </tr>    
                                <?php }?>
                                <tr>
                                    <th style="text-align: right" colspan="2">Total&nbsp;&nbsp;</th>
                                    <th style="text-align: right"><?php echo number_format($pr_total, 2); ?>&nbsp;&nbsp;</th>
                                </tr>
                            </tbody>
                        </table> 
                    </div>
                <?php }
                
                  if($aggregated_collections['package_subscriptions'] <> null && ($sponsor == null || $sponsor==$this->config->item('instant_cash_code'))){ 
                      if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                      ?>
                      <div>
                        <table class="table table-condensed table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;" colspan="3">Package Subscriptions Aggregated Collections</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">S/NO</th>
                                    <th style="text-align:center;">Package</th>
                                    <th style="text-align:center;">Amount</th>
                                 </tr>
                            </thead>
                            <tbody>
                                <?php foreach($aggregated_collections['package_subscriptions'] as $ky=>$val){ 
                                        $srv=$this->Administration_model->packages($val->packageid);
                                        $pckg_total +=$val->amount;
                                ?>
                                <tr>
                                    <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                                    <td>&nbsp;&nbsp;<?php echo $srv[0]->name; ?></td>
                                    <td style="text-align: right"><?php echo number_format($val->amount, 2); ?>&nbsp;&nbsp;</td>
                                </tr>    
                                <?php }?>
                                <tr>
                                    <th style="text-align: right" colspan="2">Total&nbsp;&nbsp;</th>
                                    <th style="text-align: right"><?php echo number_format($pckg_total, 2); ?>&nbsp;&nbsp;</th>
                                </tr>
                            </tbody>
                        </table> 
                    </div>
                    <div>
                           <?php  echo "<h4 style='color:black;text-align:center;font-weight:bold'>Total Collections : ".number_format(($srv_itm_total + $pr_total + $pckg_total), 2)."</h4>"; ?>
                    </div>
                <?php  }
            }else{
                echo "<h4 style='color:red;text-align:center'>No data found</h4>";
            } ?>  
</div>
   
</div>
