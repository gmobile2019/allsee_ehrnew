<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Administration/service_report',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End" value="<?php echo $end; ?>" />
                    </div>
                     <div class="form-group ">
                        <label class="sr-only" for="service"></label>
                        <select name="service" id="service" class="form-control" >
                        <option></option>
                        <?php foreach($services as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo $service == $value->id?"selected='selected'":""; ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                        </select>
                    </div>
                    
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Administration/service_report/start_".$start."_end_".$end."_srv_".$service."_docType_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php echo anchor("Administration/service_report/start_".$start."_end_".$end."_srv_".$service."_docType_2/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
    </div>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Patient Id</th>
                <th style="text-align:center;">Visit Id</th>
                <th style="text-align:center;">Service</th>
                <th style="text-align:center;">Ordered By</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Visit Timestamp</th>
             </tr>
        </thead>
        <tbody>
            <?php if($data != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($data as $key=>$value){
                    
                    $patient=$this->Reception_model->patient($value->patientid);
                    $user=$this->SuperAdministration_model->get_member_info($value->createdby);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $patient->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->patientid; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->patientvisitid; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $user[0]->first_name.' '.$user[0]->last_name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status ==1?'Paid':'Not Paid'; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                    </tr>  
                <?php } 
                }else{ ?>
            <tr>
                <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
