
<div style="padding-top:10px" class="display_content">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<table class="table table-condensed table-hover table-striped table-bordered">
        <thead>
            
        </thead>
        <tbody>
            <?php 
               
                    $category=$this->SuperAdministration_model->institution_categories($institution->category);
                    ?>
                    <tr>
                        <td>Institution Id</td>
                        <td>&nbsp;&nbsp;<?php echo $institution->institution_id; ?></td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>&nbsp;&nbsp;<?php echo $institution->name; ?></td>
                    </tr>
                    <tr>
                       <td>Category</td>
                       <td>&nbsp;&nbsp;<?php echo $category[0]->name; ?></td> 
                    </tr>
                    <tr>
                       <td>Phone</td>
                       <td>&nbsp;&nbsp;<?php echo $institution->phone; ?>
                       </td> 
                    </tr>
                    <tr>
                      <td>Email Address</td>
                      <td>&nbsp;&nbsp;<?php echo $institution->email; ?></td>  
                    </tr>
                    <tr>
                        <td>Postal Address</td>
                       <td>&nbsp;&nbsp;<?php echo $institution->postal; ?>
                           
                       </td> 
                    </tr>
                    <tr>
                        <td>City</td>
                       <td>&nbsp;&nbsp;<?php echo $institution->city; ?></td> 
                    </tr>
                    <tr>
                        <td>Website</td>
                        <td>&nbsp;&nbsp;<?php echo $institution->website; ?></td>
                    </tr>
                    <tr>
                        <td>Logo</td>
                        <td>&nbsp;&nbsp;<img height='50px' width="100px" src="<?php echo base_url().'logo/'.$institution->logoname ?>" alt="LogoName"/></td>
                    </tr>
                    
        </tbody>
    </table>
</div>
    
</div>
