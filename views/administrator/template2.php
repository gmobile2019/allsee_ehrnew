<!DOCTYPE html>
<html lang="en">
<head>
<title>Allsee-EHR</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap-theme.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/custom.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-ui.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-1.3.2.min.js" language="javascript" ></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>

<script type="text/javascript">
    $(document).ready(function(){
       
      
//          $(".blink").animate({opacity:0},200,"linear",function(){
//            $(this).animate({opacity:1},200);
//          }); 
       
         $('.blink').blink({delay:1000});
        
    });
</script>

<style type="text/css">
   
</style>
    
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid" id="body-div-temp">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
              <a class="navbar-brand" href="#"> <img class="logo" alt="HRO" src="<?php echo base_url()."logo/".$institution->logoname ?>" /></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active" style="color:#163B04"><?php echo anchor('Administration/','Home');?></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Users <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><?php echo anchor('Administration/create_users','Create Users');?></li>
                  <li><?php echo anchor('Administration/view_users','View Users');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Institution <span class="caret"></span></a>
                 <ul class="dropdown-menu">
                     <li><?php echo anchor('Administration/fill_institution_profile','Fill Profile');?></li>
                    <li><?php echo anchor('Administration/view_institution_profile','View Profile');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Payment Configurations <span class="caret"></span></a>
                 <ul class="dropdown-menu">
                    <li><?php echo anchor('Administration/view_sponsors','View Sponsor(s)');?></li>
                    <li><?php echo anchor('Administration/view_paymentModes','View Payment Mode(s)');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
                 <ul class="dropdown-menu">
                    <li><?php echo anchor('Administration/view_departments','Departments');?></li>
                    <li><?php echo anchor('Administration/view_subdepartments','Sub Departments');?></li>
                    <li><?php echo anchor('Administration/view_services','Services');?></li>
                    <li><?php echo anchor('Administration/investigation_services','Investigation Services');?></li>
                    <li><?php echo anchor('Administration/view_items','Items');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Packages <span class="caret"></span></a>
                 <ul class="dropdown-menu">
                    <li><?php echo anchor('Administration/view_packages','View Packages');?></li>
                    <li><?php echo anchor('Administration/add_package','Create Packages');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Inpatient<span class="caret"></span></a>
                 <ul class="dropdown-menu">
                    <li><?php echo anchor('Administration/view_wards','Wards');?></li>
                    <li><?php echo anchor('Administration/view_beds','Beds');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Inventory<span class="caret"></span></a>
                 <ul class="dropdown-menu">
                    <li><?php echo anchor('Administration/items_consumption','Items Consumption');?></li>
                    <li><?php echo anchor('Administration/disposed_items','Disposed Stock');?></li>
                    <li><?php echo anchor('Administration/expired_items','Expired Stock');?></li>
                    <li><?php echo anchor('Administration/main_stock','Main Stock');?></li>
                    <li><?php echo anchor('Administration/units_stock','Units Stock');?></li>
                    <li><?php echo anchor('Administration/stock_taking_report','Stock Takings');?></li>
                    <li><?php echo anchor('Administration/stock_transfer_report','Stock Transfers');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Collections<span class="caret"></span></a>
                 <ul class="dropdown-menu">
                    <li><?php echo anchor('Administration/aggregated_collection','Aggregated Report');?></li>
                    <li><?php echo anchor('Administration/general_collection','General Collection');?></li>
                    <li><?php echo anchor('Administration/package_accounts','Package Accounts');?></li>
                    <li><?php echo anchor('Administration/patients_debtors','Patient Debtors');?></li>
                    <li><?php echo anchor('Administration/patient_statement','Patient Statement');?></li>
                    <li><?php echo anchor('Administration/patient_transactions','Patient Transactions');?></li>
                    <li><?php echo anchor('Administration/prepaid_accounts','Prepaid Accounts');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Patients<span class="caret"></span></a>
                 <ul class="dropdown-menu">
                    <li><?php echo anchor('Administration/inpatients_report','Admissions');?></li>
                    <li><?php echo anchor('Administration/inpatient_transfers_report','In Patient Transfers');?></li>
                    <li><?php echo anchor('Administration/outpatients_report','Out Patients');?></li>
                    <li><?php echo anchor('Administration/service_report','Service Report');?></li>
                    <li><?php echo anchor('Administration/diagnosis_report','Diagnosis Report');?></li>
                </ul>
              </li>
            </ul>
              
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $userInfo->first_name.' '.$userInfo->last_name; ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><?php echo anchor('Administration/profile','Profile');?></li>
                    <li><?php echo anchor('Auth/logout','Logout');?></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
      <div class="container-fluid">
            <div id="body-bar" class="row">
                <div class="col-xs-6 col-sm-8 col-md-8" id="topbar">
                    <span id="welcome_word" class="blink">
                        <?php 
                        $category=$this->SuperAdministration_model->institution_categories($institution->category);
                        echo $institution <> null?$institution->name.' '.$category[0]->name:"HOSPITAL/DISPENSARY NAME"?>
                    </span>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 pull-right" id="moduleId">
                    <span  >Administration Module</span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 col-sm-6 col-md-3 col-lg-3 well">
                    <table class="table table-hover table-bordered patient_details_template">
                            <tr>
                                <td colspan="2">
                                    <img class="pt_details" src="<?php echo base_url() ?>images/patient.png"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Patient Id</td>
                                <td><?php echo $patient[0]->patientid; ?></td>
                            </tr>
                            <tr>
                                <td>Full Name</td>
                                <td><?php echo $patient[0]->name; ?></td>
                            </tr>
                            <tr>
                                <td>Gender</td>
                                <td>
                                    <?php 
                                    $gn=$this->Reception_model->genders($patient[0]->genderid);
                                    echo $gn[0]->name; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Date of Birth</td>
                                <td><?php echo $patient[0]->dob; ?></td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td><?php echo $patient[0]->phone; ?></td>
                            </tr>
                        </table>
                       
                </div>
                <div class="col-xs-8 col-sm-6 col-md-9 col-lg-9">
                        <div style="padding-left:0px" class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-primary"><?php echo $title; ?></button>
                        </div>
                         <?php $this->load->view($content); ?>
                </div>
            </div>
            <div id="footer-div-temp" class="row">
                <div class="col-xs-12 col-sm-12 col-md-12" >
                    &copy;&nbsp;<?php echo date('Y')?>&nbsp;Allsee-EHR.&nbsp;All Rights Reserved

                </div>
            </div>
        </div>
    </div>
</body>
</html>