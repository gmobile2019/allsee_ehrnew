
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Administration/add_subdepartment/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $id != null?$subdepartment[0]->name:set_value('name'); ?>" />
                        <?php echo form_error('name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="department" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Department&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="department" id="department" class="form-control" >
                        <option></option>
                        <?php foreach($departments as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($subdepartment[0]->departmentid))?'selected="selected"':set_select('department',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('department'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
