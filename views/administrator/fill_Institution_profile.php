
<div class="display_content">
    
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Administration/fill_institution_profile/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="institution_id" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Institution Id &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input readonly="true" type="text" class="form-control" name="" id="name" value="<?php echo set_value('institution_id',$institution->institution_id); ?>" />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo set_value('name',$institution->name); ?>" />
                        <?php echo form_error('name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="category" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Category&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="category" id="category" class="form-control" >
                        <option></option>
                        <?php foreach($categories as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo (trim($value->id) == trim($institution->category))?'selected="selected"':''; ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('category'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Phone&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo set_value('phone',$institution->phone); ?>"/>
                        
                            <?php echo form_error('phone'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Email Address&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo set_value('email',$institution->email); ?>"/>
                        <?php echo form_error('email'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="postal" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Postal Address</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="postal" id="postal" placeholder="Postal Address" value="<?php echo set_value('postal',$institution->postal); ?>" />
                        <?php echo form_error('postal'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="city" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">City&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="city" id="city" placeholder="City"  value="<?php echo set_value('city',$institution->city); ?>" />
                        <?php echo form_error('city'); ?>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="website" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Website</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="website" id="website" placeholder="Website" value="<?php echo set_value('website',$institution->website); ?>"/>
                        <?php echo form_error('website'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="logo" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Logo</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="logo" id="logo" value="Logo"/>
                        <?php echo form_error('logo'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
