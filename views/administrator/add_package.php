
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Administration/add_package/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="packageid" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Package Id &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" id="packageid"  readonly="true" value="<?php echo $id != null?$packg[0]->packageid:$packageid; ?>" />
                        <input type="hidden" name="packageid" value="<?php echo $id != null?$packg[0]->packageid:$packageid; ?>" />
                        <?php echo form_error('packageid'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $id != null?$packg[0]->name:set_value('name'); ?>" />
                        <?php echo form_error('name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cost" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Cost &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="cost" id="cost" placeholder="Cost" value="<?php echo $id != null?$packg[0]->cost:set_value('cost'); ?>" />
                        <?php echo form_error('cost'); ?>
                    </div>
                </div>
                <div class="form-group row srv">
                    <label for="upload" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 control-label">Services</label>
                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <table class="table table-condensed table-hover">
                             <tr>
                             <?php 
                             $row=0; 
                             $counter=1;
                            foreach($services as $key=>$value){
                                $pckg=$id <> null?$this->Administration_model->package_service_item($packg[0]->packageid,$value->id,$value->subdepartmentid,$value->departmentid):"";
                                 if($row==4){ 
                                     $row=0;
                                     ?>
                                    </tr>
                                    <tr>
                                 <?php } ?>
                                    <td>
                                        <input type="checkbox" name="service[]" id="cost" value="<?php echo $value->id.'_'.$value->subdepartmentid.'_'.$value->departmentid; ?>" <?php echo $pckg <> null?'checked="checked"':""; ?>/>
                                        &nbsp;&nbsp;
                                        <?php echo $value->name; ?>
                                         &nbsp;&nbsp;
                                        <input type="number" class="form-control" style="width:30%" name="qty_<?php echo $value->id.'_'.$value->subdepartmentid.'_'.$value->departmentid; ?>" placeholder="Qty" value="<?php echo $pckg <> null?$pckg[0]->quantity:""; ?>" />
                                    </td>
                                 

                                   
                             <?php 
                             
                                 if($counter==count($services)){ ?>
                                    </tr>
                                 <?php } 
                                 
                             $row++;
                             $counter++; 
                             
                            } ?>
                              
                         </table>
                    </div>
                </div>
                <div class="form-group row itm">
                    <label for="item" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 control-label">Items</label>
                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <table class="table table-condensed table-hover">
                             <tr>
                             <?php 
                             $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
                             $sub_dept=$this->Administration_model->get_subdepartment_by_department($dept[0]->id);
                             $row=0; 
                             $counter=1;
                            foreach($items as $key=>$value){
                                $pckg=$id <> null?$this->Administration_model->package_service_item($packg[0]->packageid,$value->id,$sub_dept[0]->id,$dept[0]->id):"";
                                
                                 if($row==4){ 
                                     $row=0;
                                     ?>
                                    </tr>
                                    <tr>
                                 <?php } ?>
                                    <td>
                                        <input type="checkbox" name="item[]" id="item" value="<?php echo $value->id.'_'.$sub_dept[0]->id.'_'.$dept[0]->id; ?>" <?php echo $pckg <> null?'checked="checked"':""; ?>/>
                                        &nbsp;&nbsp;
                                        <?php echo $value->name; ?>
                                         &nbsp;&nbsp;
                                        <input type="number" class="form-control" style="width:30%" name="qty_<?php echo $value->id.'_'.$sub_dept[0]->id.'_'.$dept[0]->id; ?>" placeholder="Qty" value="<?php echo $pckg <> null?$pckg[0]->quantity:""; ?>" />
                                    </td>
                            <?php 
                             
                                 if($counter==count($items)){ ?>
                                    </tr>
                                 <?php } 
                                 
                             $row++;
                             $counter++; 
                             
                            } ?>
                              
                         </table>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" class="btn btn-success" name="save_package" value="Save Package"/>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
