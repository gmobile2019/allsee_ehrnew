<script type="text/javascript">
    $(document).ready(function(){
        $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Administration/patient_debt_details/".$patientid."/1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php echo anchor("Administration/patient_debt_details/".$patientid."/2/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
               
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
             <tr>
                <th style="text-align:center;" colspan="9"><?php echo anchor('Administration/patients_debtors/','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back"></span>');?></th>
            </tr>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Patient Id</th>
                <th style="text-align:center;">Visit Id</th>
                <th style="text-align:center;">Department</th>
                <th style="text-align:center;">Sub Department</th>
                <th style="text-align:center;">Service</th>
                <th style="text-align:center;">Action Date</th>
                <th style="text-align:center;">Amount</th>
             </tr>
        </thead>
        <tbody>
            <?php if($patient_debt_details != null){
                $total=0;
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($patient_debt_details as $key=>$value){
                    $srv=$this->Inventory_model->items($value->service_item_id);
                    $dpt=$this->Administration_model->departments($value->departmentid);
                    $subdpt=$this->Administration_model->subdepartments($value->subdepartmentid);
                    $clear=FALSE;
                    $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
                    
                    if($dept[0]->id <> $value->departmentid){
                        
                        $srv=$this->Administration_model->services($value->service_item_id);
                        $clear=TRUE;
                    }
                    $total +=$value->amount;
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->patientid ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->patientvisitid ?></td>
                        <td>&nbsp;&nbsp;<?php echo $dpt[0]->name ?></td>
                        <td>&nbsp;&nbsp;<?php echo $subdpt[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $srv[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                        <td style="text-align: right"><?php echo number_format($value->amount,2); ?>&nbsp;&nbsp;</td>
                    </tr>
                <?php } ?>
                    <tr>
                        <th colspan="7" style="text-align: right">Total&nbsp;&nbsp;</th>
                        <th style="text-align: right"><?php echo number_format($total,2); ?>&nbsp;&nbsp;</th>
                    </tr>
                <?php 
                }else{ ?>
            <tr>
                <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>     
</div>
      
</div>
