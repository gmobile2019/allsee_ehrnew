<script type="text/javascript">
    $(document).ready(function(){
        
       var group;
       
       group=$('select[name=group]').val();
       
       if(group == 4){
           
           $('div#doctor_category').show();
           $('div#nurse_incharge').hide();
           $('div#examiner_incharge').hide();
           $('div#nurse_pharmacist_category').hide();
           $('div#investigation_categ').hide();
       }else if(group == 3){
           
           $('div#nurse_pharmacist_category').show();
           $('div#nurse_incharge').show();
            $('div#examiner_incharge').hide();
           $('div#doctor_category').hide();
           $('div#investigation_categ').hide();
       }else if(group == 5){
           
           $('div#nurse_pharmacist_category').show();
           $('div#nurse_incharge').hide();
            $('div#examiner_incharge').hide();
           $('div#doctor_category').hide();
           $('div#investigation_categ').hide();
       }else if(group == 9){

            $('div#nurse_pharmacist_category').hide();
            $('div#nurse_incharge').hide();
            $('div#examiner_incharge').show();
            $('div#doctor_category').hide();
            $('div#investigation_categ').show();
        }else{
           
           $('div#doctor_category').hide();
           $('div#nurse_incharge').hide();
           $('div#examiner_incharge').hide();
           $('div#nurse_pharmacist_category').hide();
           $('div#investigation_categ').hide();
       }
       
       $('select[name=group]').change(function(){
            group=$(this).val();
       
            if(group == 4){
                
                $('div#doctor_category').show();
                $('div#nurse_incharge').hide();
                $('div#examiner_incharge').hide();
                $('div#nurse_pharmacist_category').hide();
                $('div#investigation_categ').hide();
                
            }else if(group == 3){
           
                $('div#nurse_pharmacist_category').show();
                $('div#nurse_incharge').show();
                $('div#examiner_incharge').hide();
                $('div#doctor_category').hide();
                $('div#investigation_categ').hide();
            }else if(group == 5){

                $('div#nurse_pharmacist_category').show();
                $('div#nurse_incharge').hide();
                $('div#examiner_incharge').hide();
                $('div#doctor_category').hide();
                $('div#investigation_categ').hide();
                
            }else if(group == 9){

                $('div#nurse_pharmacist_category').hide();
                $('div#nurse_incharge').hide();
                $('div#examiner_incharge').show();
                $('div#doctor_category').hide();
                $('div#investigation_categ').show();
            }else{
                
                $('div#doctor_category').hide();
                $('div#nurse_incharge').hide();
                $('div#examiner_incharge').hide();
                $('div#nurse_pharmacist_category').hide();
                $('div#investigation_categ').hide();
            }
       });
       
    });
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Administration/create_users/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="first_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">First Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" value="<?php echo $id != null?$member[0]->first_name:set_value('first_name'); ?>" />
                        <?php echo form_error('first_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="middle_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Middle Name</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="middle_name" id="middle_name" placeholder="Middle Name" value="<?php echo $id != null?$member[0]->middle_name:set_value('middle_name'); ?>"/>
                        
                            <?php echo form_error('middle_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="last_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Last Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="<?php echo $id != null?$member[0]->last_name:set_value('last_name'); ?>" />
                        <?php echo form_error('last_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="username" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Username&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="username" id="username" placeholder="Username"  value="<?php echo $id != null?$member[0]->username:set_value('username'); ?>" />
                        <?php echo form_error('username'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Email Address</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $id != null?$member[0]->email:set_value('email'); ?>"/>
                        <?php echo form_error('email'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mobile" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Mobile&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" value="<?php echo $id != null?$member[0]->msisdn:set_value('mobile'); ?>"/>
                        <?php echo form_error('mobile'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Password&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password"/>
                        <?php echo form_error('password'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="conf_password" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Confirm Password&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="password" class="form-control" name="conf_password" id="conf_password" placeholder="Confirm Password"/>
                        <?php echo form_error('conf_password'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="group" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">User Group&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="group" id="group" class="form-control" >
                        <option></option>
                        <?php foreach($groups as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($member[0]->group_id))?'selected="selected"':set_select('group',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('group'); ?>
                    </div>
                </div>
                <div class="form-group row" id="doctor_category">
                    <label for="doctor_categ" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Doctor Category&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <?php $doctor_categ=$this->Administration_model->doctors(null,$id); ?>
                    <select name="doctor_categ" id="doctor_categ" class="form-control" >
                        <option></option>
                        <?php foreach($doctor_categs as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($doctor_categ[0]->doctorcategory))?'selected="selected"':set_select('doctor_categ',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('doctor_categ'); ?>
                    </div>
                </div>
                <div class="form-group row" id="nurse_pharmacist_category">
                    <label for="inv_unit" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Inventory Unit&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <?php $inv_unit=$this->Administration_model->inventory_unit_users($id); ?>
                    <select name="inv_unit" id="inv_unit" class="form-control" >
                        <option value="">None</option>
                        <?php foreach($units as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($inv_unit[0]->inventoryunit))?'selected="selected"':set_select('inv_unit',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                    <?php echo form_error('inv_unit'); ?>
                    </div>
                </div>
                <div class="form-group row" id="investigation_categ">
                    <label for="investigation_categ" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Investigation Category&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                         <?php $investigation_categ=$this->Administration_model->examiners(null,$id); ?>
                    <select name="investigation_categ" id="investigation_categ" class="form-control" >
                        <option></option>
                        <?php foreach($investigation_categs as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->code; ?>" <?php echo ($id != null && trim($value->code) == trim($investigation_categ[0]->investigationcategory))?'selected="selected"':set_select('investigation_categ',$value->code); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('investigation_categ'); ?>
                    </div>
                </div>
                <div class="form-group row" id="nurse_incharge">
                    <label for="nurse_incharge" class="col-xs-12 col-sm-6 col-md-3 col-lg-3 control-label">
                        Nurse Incharge
                    </label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="checkbox" name="nurse_incharge" id="nurse_incharge" value="1" class="form-control">
                    </div>
                </div>
    
                <div class="form-group row" id="examiner_incharge">
                    <label for="examiner_incharge" class="col-xs-12 col-sm-6 col-md-3 col-lg-3 control-label">
                        Examiner Incharge
                    </label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="checkbox" name="examiner_incharge" id="examiner_incharge" <?php ($id <> null && $investigation_categ[0]->supervisor == 1)?"checked='checked'":""?> value="1" class="form-control">
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Register User</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
