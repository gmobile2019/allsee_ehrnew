
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Administration/add_ward_bed/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="identity" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Bed Identity&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="identity" id="identity" placeholder="Bed Identity" value="<?php echo $id != null?$ward_bed[0]->bedidentity:set_value('identity'); ?>" />
                        <?php echo form_error('identity'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ward" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Ward&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="ward" id="ward" class="form-control" >
                            <option></option>
                            <?php foreach($wards as $key=>$value){ ?>

                            <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($ward_bed[0]->wardid))?'selected="selected"':set_select('ward',$value->id); ?>><?php echo $value->name; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('ward'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
