<script type="text/javascript">
    $(document).ready(function(){
             var department;
             var subdepartment;
             
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            department=$('select[name=department]').val();
            $('select[name=subdepartment]').find('option').each(function(){
                subdepartment=$(this).attr('class');

                if(subdepartment == "dpt_"+department){
                    $(this).show();
                }else{
                    $(this).hide();
                }
            });         
                     
            $('select[name=department]').change(function(){
                 department=$(this).val();
                
                 $('select[name=subdepartment]').find('option').each(function(){
                     subdepartment=$(this).attr('class');
                     
                     if(subdepartment == "dpt_"+department){
                         $(this).show();
                     }else{
                         $(this).hide();
                     }
                 });
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Administration/general_collection/',$attributes); 
                  
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="sponsor"></label>
                        <select name="sponsor" id="sponsor" class="form-control" >
                                <option value="" >All Sponsors</option>
                                <?php foreach($sponsors as $key=>$value){ ?>

                                <option value="<?php echo $value->sponsorcode; ?>" <?php echo ($sponsor == $value->sponsorcode )?'selected="selected"':''; ?>><?php echo $value->shortname; ?></option>

                                <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="department"></label>
                        <select name="department" id="department" class="form-control" >
                        <option value="">All</option>
                        <?php foreach($departments as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($department == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="department"></label>
                       <select name="subdepartment" id="subdepartment" class="form-control" >
                        <option value="">All</option>
                        <?php foreach($subdepartments as $key=>$value){ ?>
                        
                        <option class="dpt_<?php echo $value->departmentid; ?>" value="<?php echo $value->id; ?>" <?php echo ($subdepartment == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                        </select>
                    </div>
                   <div class="form-group">
                       <button type="submit" class="btn btn-success">Generate</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Administration/general_collection/start_".$start."_end_".$end."_sponsor_".$sponsor."_department_".$department."_subdepartment_".$subdepartment."_docType_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php //echo anchor("Reception/general_collection/start_".$start."_end_".$end."_sponsor_".$sponsor."_department_".$department."_subdepartment_".$subdepartment."_docType_2/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
     
       <?php if($general_collections['service_items'] <> null || (($general_collections['prepaid_subscriptions'] <> null || $general_collections['package_subscriptions'] <> null) && ($sponsor == null || $sponsor==$this->config->item('instant_cash_code')) )){
           
                if($general_collections['service_items'] <> null){
                        if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    ?>
                        
                    <table class="table table-condensed table-hover table-striped table-bordered">

                        <thead>
                            <tr>
                                <th style="text-align:center;" colspan="8">Service/Item General Collections</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;">S/NO</th>
                                <th style="text-align:center;">Patient Id</th>
                                <th style="text-align:center;">Visit Id</th>
                                <th style="text-align:center;">Service</th>
                                <th style="text-align:center;">Sponsor</th>
                                <th style="text-align:center;">Receipt No</th>
                                <th style="text-align:center;">Action Date</th>
                                <th style="text-align:center;">Amount</th>
                             </tr>
                        </thead>
                        <tbody>
                        <?php foreach($general_collections['service_items'] as $ky=>$val){ 
                                $srv=$this->Inventory_model->items($val->service_item_id);
                                $sp=$this->SuperAdministration_model->get_sponsor_by_code($val->sponsorid);
                                $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));

                                if($dept[0]->id <> $val->departmentid){
                                    $srv=$this->Administration_model->services($val->service_item_id);
                                }
                                
                                $srv_itm_total +=$val->amount;
                            ?>
                         
            
                            <tr>
                                <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                                <td>&nbsp;&nbsp;<?php echo $val->patientid ?></td>
                                <td>&nbsp;&nbsp;<?php echo $val->patientvisitid ?></td>
                                <td>&nbsp;&nbsp;<?php echo $srv[0]->name; ?></td>
                                <td>&nbsp;&nbsp;<?php echo $sp[0]->shortname; ?></td>
                                <td>&nbsp;&nbsp;<?php echo $val->transactionid; ?></td>
                                <td>&nbsp;&nbsp;<?php echo $val->createdon; ?></td>
                                <td style="text-align: right"><?php echo number_format($val->amount, 2); ?>&nbsp;&nbsp;</td>
                            </tr>    
                        
                        <?php }?>
                            <tr>
                                <th style="text-align: right" colspan="7">Total&nbsp;&nbsp;</th>
                                <th style="text-align: right"><?php echo number_format($srv_itm_total, 2); ?>&nbsp;&nbsp;</th>
                            </tr>
                        </tbody>
                    </table> 
                <?php }
                
                if($general_collections['prepaid_subscriptions'] <> null && ($sponsor == null || $sponsor==$this->config->item('instant_cash_code'))){ 
                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    ?>
                    <div>
                        <table class="table table-condensed table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;" colspan="6">Prepaid Subscriptions</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">S/NO</th>
                                    <th style="text-align:center;">Patient Id</th>
                                    <th style="text-align:center;">Service</th>
                                    <th style="text-align:center;">Receipt No</th>
                                    <th style="text-align:center;">Action Date</th>
                                    <th style="text-align:center;">Amount</th>
                                 </tr>
                            </thead>
                            <tbody>
                                <?php foreach($general_collections['prepaid_subscriptions'] as $ky=>$val){ 
                                $pr_total +=$val->amount;
                                ?>
                                <tr>
                                    <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                                    <td>&nbsp;&nbsp;<?php echo $val->patientid ?></td>
                                    <td>&nbsp;&nbsp;<?php echo 'Prepaid'; ?></td>
                                    <td>&nbsp;&nbsp;<?php echo $val->transactionid; ?></td>
                                    <td>&nbsp;&nbsp;<?php echo $val->createdon; ?></td>
                                    <td style="text-align: right"><?php echo number_format($val->amount, 2); ?>&nbsp;&nbsp;</td>
                                </tr>    
                                <?php }?>
                                <tr>
                                    <th style="text-align: right" colspan="5">Total&nbsp;&nbsp;</th>
                                    <th style="text-align: right"><?php echo number_format($pr_total, 2); ?>&nbsp;&nbsp;</th>
                                </tr>
                            </tbody>
                        </table> 
                    </div>
                <?php }
                
                  if($general_collections['package_subscriptions'] <> null && ($sponsor == null || $sponsor==$this->config->item('instant_cash_code'))){ 
                      if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                      ?>
                      <div>
                        <table class="table table-condensed table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;" colspan="6">Package Subscriptions</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">S/NO</th>
                                    <th style="text-align:center;">Patient Id</th>
                                    <th style="text-align:center;">Package</th>
                                    <th style="text-align:center;">Receipt No</th>
                                    <th style="text-align:center;">Action Date</th>
                                    <th style="text-align:center;">Amount</th>
                                 </tr>
                            </thead>
                            <tbody>
                                <?php foreach($general_collections['package_subscriptions'] as $ky=>$val){ 
                                        $srv=$this->Administration_model->packages($val->packageid);
                                        $pckg_total +=$val->amount;
                                ?>
                                <tr>
                                    <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                                    <td>&nbsp;&nbsp;<?php echo $val->patientid ?></td>
                                    <td>&nbsp;&nbsp;<?php echo $srv[0]->name; ?></td>
                                    <td>&nbsp;&nbsp;<?php echo $val->transactionid; ?></td>
                                    <td>&nbsp;&nbsp;<?php echo $val->createdon; ?></td>
                                    <td style="text-align: right"><?php echo number_format($val->amount, 2); ?>&nbsp;&nbsp;</td>
                                </tr>    
                                <?php }?>
                                <tr>
                                    <th style="text-align: right" colspan="5">Total&nbsp;&nbsp;</th>
                                    <th style="text-align: right"><?php echo number_format($pckg_total, 2); ?>&nbsp;&nbsp;</th>
                                </tr>
                            </tbody>
                        </table> 
                    </div>
                    <div>
                           <?php  echo "<h4 style='color:black;text-align:center;font-weight:bold'>Total Collections : ".number_format(($srv_itm_total + $pr_total + $pckg_total), 2)."</h4>"; ?>
                    </div>
                <?php  }
            }else{
                echo "<h4 style='color:red;text-align:center'>No data found</h4>";
            } ?>  
</div>
   
</div>
