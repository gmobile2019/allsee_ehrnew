<script type="text/javascript">
    $(document).ready(function(){
       $('div#add_order').hide();
       var subdepartment;
       var label;
       var department;
       var dptm;
       
       department=$('select[name=department]').val();
       
       $('select#subdepartment').find('option').each(function(){
               dptm=$(this).attr('class');
               
               if(dptm === 'dptm'+department){
                   $(this).show();
               }else{
                   $(this).hide();
               }
               
        });
       
       $('select[name=department]').change(function(){
           
           department=$(this).val();
           $('select#subdepartment').val('');
           
           $('select#subdepartment').find('option').each(function(){
               dptm=$(this).attr('class');
               
               if(dptm === 'dptm'+department){
                   $(this).show();
               }else{
                   $(this).hide();
               }
               
            });
       });
       
       subdepartment=$('select[name=subdepartment]').val();
       
       $('div#services').find('div').find('label').each(function(){
               label=$(this).attr('id');
               
               if(label === 'subdept'+subdepartment){
                   $('div#add_srv').show();
                   $(this).show();
               }else{
                   $(this).hide();
               }
               
        });
       
       $('select[name=subdepartment]').change(function(){
           
           subdepartment=$(this).val();
         
           $('div#services').find('div').find('label').each(function(){
               label=$(this).attr('id');
               
               if(label === 'subdept'+subdepartment){
                   
                   $(this).show();
               }else{
                   $(this).hide();
               }
               
           });
       });
       
    });
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Administration/configure_investigation_category_service/'.$id); 
                   
                ?>
       
                <div class="form-group row">
                    <label for="investigationcategory" class="col-xs-12 col-md-offset-2 col-lg-offset-2 col-sm-3 col-md-3 col-lg-3 control-label">Investigation Category&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="investigationcategory" id="investigationcategory" class="form-control" >
                        <option></option>
                        <?php foreach($categories as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->code; ?>" <?php echo set_select('investigationcategory',$value->code); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('investigationcategory'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="department" class="col-xs-12 col-md-offset-2 col-lg-offset-2 col-sm-3 col-md-3 col-lg-3 control-label">Department&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="department" id="department" class="form-control" >
                        <option></option>
                        <?php foreach($departments as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo set_select('department',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('department'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="subdepartment" class="col-xs-12 col-md-offset-2 col-lg-offset-2 col-sm-3 col-md-3 col-lg-3 control-label">Sub Department&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="subdepartment" id="subdepartment" class="form-control" >
                        <option></option>
                        <?php foreach($subdepartments as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" class="dptm<?php echo $value->departmentid; ?>" <?php echo set_select('subdepartment',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('subdepartment'); ?>
                    </div>
                </div>
                <div class="form-group row " id="services">
                    <label for="subdepartment" class="col-xs-12 col-md-offset-2 col-lg-offset-2 col-sm-3 col-md-3 col-lg-3 control-label">Services&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        
                            <?php 
                            $i=1;
                                  
                            foreach($services as $key=>$value){ 
                                $chk=$this->SuperAdministration_model->investigation_category_services(null,null,$value->id);
                                
                                if($chk == null){ ?>
                                    <label class="checkbox-inline " id="subdept<?php echo $value->subdepartmentid; ?>">
                                        <input type="checkbox" name="srv[]" id="srv<?php echo $value->id; ?>" value="<?php echo $value->id; ?>"> <?php echo $value->name; ?>
                                      </label>
                            <?php }
                            
                                }
                                 ?>
                    </div>
                </div>
                <div class="form-group register_width_padding" id="add_srv">
                    <div class="col-md-offset-3 col-md-3 col-sm-offset-2 col-sm-3">
                        <input type="submit" class="btn btn-success" name="configure" value="Configure Service(s)">
                    </div>
                </div>
        
        <?php echo form_close();
                    ?>
</div>
