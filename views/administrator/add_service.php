<script type="text/javascript">
    $(document).ready(function(){
        var addType;
       
        /*
        * addType radio button
       */
       $('input[name=addType]').each(function(){
          addType=$(this).val();
          
          if($(this).is(':checked')){
               if(addType == 1){
                
                $('div.single').show();
                $('div.bulk').hide();
                
                }else{
                
                    $('div.single').hide();
                    $('div.bulk').show();
                }
          }
       });
       
       
       $('input[name=addType]').change(function(){
            
             addType=$(this).val();
             if($(this).is(':checked')){
                 
                 if(addType == 1){
                
                $('div.single').show('slow');
                $('div.bulk').hide();
                
                }else{

                    $('div.single').hide();
                    $('div.bulk').show('slow');
                }
             }
            
       });
       
    });
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Administration/add_service/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="addType" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Addition Type? &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                       <label class="checkbox-inline">
                           <input type="radio" id="addType" name="addType" value="1" <?php echo $id <> null?"checked='checked'":  set_radio('addType',"1")?>> Single
                      </label>
                      <label class="checkbox-inline">
                        <input type="radio" id="addType" name="addType" value="2" <?php echo set_radio('addType',"2"); ?>> Bulk
                      </label><?php echo form_error('addType'); ?>
                    </div>
                </div>
                <div class="form-group row single">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $id != null?$service[0]->name:set_value('name'); ?>" />
                        <?php echo form_error('name'); ?>
                    </div>
                </div>
                <div class="form-group row bulk">
                    <label for="upload" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Services Excel(xls)&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="upload" id="upload" value="Upload"/>
                        <?php echo form_error('upload'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="subdepartment" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Sub Department&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="subdepartment" id="subdepartment" class="form-control" >
                        <option></option>
                        <?php foreach($subdepartments as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($service[0]->subdepartmentid))?'selected="selected"':set_select('subdepartment',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('subdepartment'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save Service</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
