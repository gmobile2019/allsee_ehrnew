
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Administration/add_department/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $id != null?$department[0]->name:set_value('name'); ?>" />
                        <?php echo form_error('name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="service_charge" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Service Charge&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="service_charge" id="service_charge" class="form-control" >
                        <option></option>
                        <?php foreach($service_charges as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->service_code; ?>" <?php echo ($id != null && trim($value->service_code) == trim($department[0]->servicechargeid))?'selected="selected"':set_select('service_charge',$value->service_code); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('service_charge'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
