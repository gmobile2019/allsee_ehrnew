<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>

<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Package Name</th>
                <th style="text-align:center;">Item/Service</th>
                <th style="text-align:center;">Department</th>
                <th style="text-align:center;">Sub Department</th>
                <th style="text-align:center;">Quantity</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($pckg_details != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($pckg_details as $key=>$value){
                    $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
                    $itm_srv=$dept[0]->id == $value->departmentid?$this->Inventory_model->items($value->item_service_id):$this->Administration_model->services($value->item_service_id);
                    i
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $itm_srv[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->department; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->subdepartment; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->quantity; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status == 1?"Active":"Suspended"; ?></td>
                         <?php 
                        
                        $active_status=$value->status == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 
                        
                        ?>
                        <td style="text-align: center">
                            <?php echo anchor('Administration/activate_deactivate_package_detail/'.$value->packageid.'/'.$value->id.'/'.$value->status,$active_status); ?>
                        </td>
                    </tr>  
                <?php } 
                }else{ ?>
            <tr>
                <td colspan="7" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
