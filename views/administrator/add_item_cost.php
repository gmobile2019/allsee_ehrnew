<script type="text/javascript">
    $(document).ready(function(){
        var addType;
       
        /*
        * addType radio button
       */
       $('input[name=addType]').each(function(){
          addType=$(this).val();
          
          if($(this).is(':checked')){
               if(addType == 1){
                
                $('div.single').show();
                $('div.bulk').hide();
                
                }else{
                
                    $('div.single').hide();
                    $('div.bulk').show();
                }
          }
       });
       
       
       $('input[name=addType]').change(function(){
            
             addType=$(this).val();
             if($(this).is(':checked')){
                 
                 if(addType == 1){
                
                $('div.single').show('slow');
                $('div.bulk').hide();
                
                }else{

                    $('div.single').hide();
                    $('div.bulk').show('slow');
                }
             }
            
       });
       
    });
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Administration/add_item_cost/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="addType" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Addition Type? &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                       <label class="checkbox-inline">
                           <input type="radio" id="addType" name="addType" value="1" <?php echo $id <> null?"checked='checked'":set_radio('addType',"1")?>> Single
                      </label>
                      <label class="checkbox-inline">
                        <input type="radio" id="addType" name="addType" value="2" <?php echo set_radio('addType',"2"); ?>> Bulk
                      </label><?php echo form_error('addType'); ?>
                    </div>
                </div>
                <div class="form-group row single">
                    <label for="item" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Item&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="item" id="item" class="form-control" >
                        <option></option>
                        <?php foreach($items as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($item_cost[0]->serviceid))?'selected="selected"':set_select('item',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('item'); ?>
                    </div>
                </div>
                <div class="form-group row single">
                    <label for="cost" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Cost&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                         <input type="number" class="form-control" name="cost" id="cost" value="<?php echo $id != null?$item_cost[0]->cost:set_value('cost'); ?>"/>
                        <?php echo form_error('cost'); ?>
                    </div>
                </div>
                <div class="form-group row bulk">
                    <label for="upload" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Items Excel(xls)&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="upload" id="upload" value="Upload"/>
                        <?php echo form_error('upload'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sponsor" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Sponsor&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="sponsor" id="sponsor" class="form-control" >
                        <option value="">All Sponsors</option>
                        <?php foreach($sponsors as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->sponsorcode; ?>" <?php echo ($id != null && trim($value->sponsorcode) == trim($item_cost[0]->sponsorid))?'selected="selected"':set_select('sponsor',$value->sponsorcode); ?>><?php echo $value->shortname; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('sponsor'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
