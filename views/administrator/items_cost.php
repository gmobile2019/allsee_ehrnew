
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Administration/view_items_cost/'.$code,$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="sponsor"></label>
                        <select name="sponsor" id="sponsor" class="form-control" >
                                <option value="" ></option>
                                <?php foreach($sponsors as $key=>$value){ ?>

                                <option value="<?php echo $value->sponsorcode; ?>" <?php echo ($sponsor == $value->sponsorcode )?'selected="selected"':''; ?>><?php echo $value->shortname; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="min"></label>
                        <input type="text" class="form-control" name="min" id="min" placeholder="Minimum Cost" value="<?php echo $min; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="max"></label>
                        <input type="text" class="form-control" name="max" id="max" placeholder="Maximum Cost" value="<?php echo $max; ?>" />
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Administration/view_items_cost/$code/name_".$name."_sponsor_".$sponsor."_min_".$min."_max_".$max."_doctype_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php echo anchor("Administration/view_items_cost/$code/name_".$name."_sponsor_".$sponsor."_min_".$min."_max_".$max."_doctype_2/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
</div>
<div style="padding-top:10px" class="row">
	<table class="table table-condensed table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Category</th>
                <th style="text-align:center;">Sponsor</th>
                <th style="text-align:center;">Cost</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($items != null){
               
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($items as $key=>$value){
                    $ct=$this->Inventory_model->get_item_category_by_code($value->category);
                    $sp=$this->SuperAdministration_model->get_sponsor_by_code($value->sponsorid);
                   
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $ct->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $sp[0]->shortname; ?></td>
                        <td>&nbsp;&nbsp;<?php echo number_format($value->cost,2); ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status == 1?'Active':'Suspended'; ?></td>
                         <?php 
                        
                        $active_status=$value->status == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 
                        
                        ?>
                        <td>&nbsp;&nbsp;
                            <?php echo anchor('Administration/add_item_cost/'.$value->id,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('Administration/activate_deactivate_item_cost/'.$value->id.'/'.$value->status.'/'.$code,$active_status); ?>
                        </td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="7" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>

    
</div>
