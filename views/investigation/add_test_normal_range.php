<script type="text/javascript">
    $(document).ready(function(){
       
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Investigation/add_test_normal_range/'.$id); 
                ?>
                <div class="form-group row">
                    <label for="test" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Test&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="test" id="test" class="form-control" >
                            <option></option>
                            <?php foreach($tests as $key=>$value){ ?>

                            <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($testrange[0]->id))?'selected="selected"':set_select('test',$value->id); ?>><?php echo $value->test; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('test'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Name</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $id <> null?$testrange[0]->name:set_value('name'); ?>"/>
                        <?php echo form_error('name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="range" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Normal Range&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="range" id="range" placeholder="Normal Range" value="<?php echo $id <> null?$testrange[0]->normalrange:set_value('range'); ?>"/>
                        <?php echo form_error('range'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
