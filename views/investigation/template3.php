<!DOCTYPE html>
<html lang="en">
<head>
<title>Allsee-EHR</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap-theme.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/custom.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-ui.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-1.3.2.min.js" language="javascript" ></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>

<script type="text/javascript">
    $(document).ready(function(){
       
      
//          $(".blink").animate({opacity:0},200,"linear",function(){
//            $(this).animate({opacity:1},200);
//          }); 
       
         $('.blink').blink({delay:1000});
        
    });
</script>

<style type="text/css">
   
</style>
    
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid" id="body-div-temp">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
              <a class="navbar-brand" href="#"> <img class="logo" alt="HRO" src="<?php echo base_url()."logo/".$institution->logoname ?>" /></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active" style="color:#163B04"><?php echo anchor('Investigation/','Home');?></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Patients <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><?php echo anchor('Investigation/out_patients','Out Patients');?></li>
                    <li><?php echo anchor('Investigation/in_patients','In Patients');?></li>
                    <li><?php echo anchor('Investigation/walk_ins','Walk In Patients');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services/Tests <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><?php echo anchor('Investigation/services','Services');?></li>
                    <li><?php echo anchor('Investigation/service_tests','Tests');?></li>
                    <li><?php echo anchor('Investigation/test_results','Test Result Sets');?></li>
                    <li><?php echo anchor('Investigation/test_normal_ranges','Test Normal Ranges');?></li>
                </ul>
              </li>
             <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
                 <ul class="dropdown-menu">
                   <li><?php echo anchor('Investigation/patient_medical_results','Investigation Results');?></li>
                   <li><?php echo anchor('Investigation/patients_report','Patients Report');?></li>
                   <li><?php echo anchor('Investigation/tests_report','Tests Report');?></li>
                </ul>
              </li>
            </ul>
              
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $userInfo->first_name.' '.$userInfo->last_name; ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><?php echo anchor('Investigation/profile','Profile');?></li>
                    <li><?php echo anchor('Auth/logout','Logout');?></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
      <div class="container-fluid">
            <div id="body-bar" class="row">
                <div class="col-xs-6 col-sm-8 col-md-8" id="topbar">
                    <span id="welcome_word" class="blink">
                        <?php 
                        $pfl=$this->SuperAdministration_model->profile_data();
                        $u_cat=$this->SuperAdministration_model->groups($pfl->group_id);
                        $category=$this->SuperAdministration_model->institution_categories($institution->category);
                        echo $institution <> null?$institution->name.' '.$category[0]->name:"HOSPITAL/DISPENSARY NAME"?>
                    </span>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 pull-right" id="moduleId">
                    <span  >Investigation Module</span>
                </div>
            </div>
            <div class="row">
                  <div class="col-xs-4 col-sm-6 col-md-4 col-lg-4 well display_content">
                    <table class="table table-hover table-bordered patient_details_template">
                            <tr>
                                <td colspan="2">
                                    <img class="pt_details" src="<?php echo base_url() ?>images/users/patient.png"/>
                                </td>
                            </tr>
                            <tr>
                                <td>First Name</td>
                                <td><?php echo $userInfo->first_name; ?></td>
                            </tr>
                            <tr>
                                <td>Middle Name</td>
                                <td><?php echo $userInfo->middle_name; ?></td>
                            </tr>
                            <tr>
                                <td>Last Name</td>
                                <td><?php echo $userInfo->last_name; ?></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><?php echo $userInfo->email; ?></td>
                            </tr>
                            <tr>
                                <td>Mobile</td>
                                <td><?php echo $userInfo->msisdn; ?></td>
                            </tr>
                            <tr>
                                <td>User Group</td>
                                <td><?php echo $u_cat[0]->name; ?></td>
                            </tr>
                        </table>
                </div>
                <div class="col-xs-8 col-sm-6 col-md-8 col-lg-8">
                        <div style="padding-left:0px" class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-primary"><?php echo $title; ?></button>
                        </div>
                         <?php $this->load->view($content); ?>
                </div>
            </div>
            <div id="footer-div-temp" class="row">
                <div class="col-xs-12 col-sm-12 col-md-12" >
                    &copy;&nbsp;<?php echo date('Y')?>&nbsp;Allsee-EHR.&nbsp;All Rights Reserved

                </div>
            </div>
        </div>
    </div>
</body>
</html>