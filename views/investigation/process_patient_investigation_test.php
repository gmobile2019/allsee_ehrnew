<script type="text/javascript">
    $(document).ready(function(){
        
           
    });
</script>
<?php $this->load->view($diagnosis,$this->data);?>
<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
     
        <?php 
                
        
        if($services <> null){
            
        echo $message;
        $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
        echo form_open('Investigation/process_patient_tests/'.$patientid.'/'.$patientvisitid.'/');
        
        foreach($services as $key=>$value){ 
            $srv=$this->Administration_model->services($value->serviceid);
            ?>
            <div class="">
                <table class="table table-condensed table-hover table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center;" colspan="4"><?php echo $srv[0]->name; ?></th>
                         </tr>
                        <tr>
                            <th style="text-align:center;">S/NO</th>
                            <th style="text-align:center;">Test</th>
                            <th style="text-align:center;">Result</th>
                            <th style="text-align:center;">Action</th>
                         </tr>
                    </thead>
                    <tbody>
                          <?php
                                $tests=$this->Investigation_model->tests(null,null,$value->serviceid);
                                if($tests <> null){
                                    
                                $i=1;
                                foreach($tests as $ky=>$val){ 
                                    $result
                                    ?>
                                <tr>
                                    <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                                    <td >&nbsp;&nbsp;<?php echo $val->test ?></td>
                                    <td>&nbsp;&nbsp;
                                        <?php 
                                        
                                        if($val->resultcategory =='Fixed'){ 
                                            $results=$this->Investigation_model->test_results(null,null,null,1,$val->id); 
                                            
                                            ?>
                                            <select name="result_<?php echo $value->id.'_'.$value->serviceid.'_'.$val->id ?>" id="result_<?php echo $value->id.'_'.$value->serviceid.'_'.$val->id; ?>" class="form-control" >
                                                <option></option>
                                                <?php foreach($results as $k=>$v){ ?>

                                                <option value="<?php echo $v->name; ?>" <?php echo set_select('result_'.$value->id.'_'.$value->serviceid.'_'.$val->id,$v->name); ?>><?php echo $v->name; ?></option>

                                                <?php } ?>

                                            </select>
                                        <?php 
                                        }else{ ?>
                                           <textarea class="form-control" name="result_<?php echo $value->id.'_'.$value->serviceid.'_'.$val->id?>" id="result_<?php echo $value->id.'_'.$value->serviceid.'_'.$val->id?>" ><?php echo set_value('result_'.$value->id.'_'.$value->serviceid.'_'.$val->id); ?></textarea> 
                                        
                                        <?php }
                                        echo form_error('result_'.$value->id.'_'.$value->serviceid.'_'.$val->id);
                                        ?>
                                    </td>
                                    <td style="text-align: center">&nbsp;&nbsp;
                                       <label class="checkbox-inline">
                                        <input type="checkbox" name="investigate[]" id="investigate_<?php echo $value->id.'_'.$value->serviceid.'_'.$val->id; ?>" value="<?php echo $value->id.'_'.$value->serviceid.'_'.$val->id; ?>"> <?php echo $value->name; ?>
                                      </label>
                                    </td>
                                </tr>   
                                <?php } 
                                
                                    }else{ ?>
                                    
                                <tr>
                                    <td style="text-align:center;" colspan="4">No Tests For The Service Found</td>
                                 </tr>
                                <?php } ?>
                               
                    </tbody>
                </table>
            </div>
        <?php } ?>
        <div class="form-group register_width_padding">
            <div >
                <input type="submit" class="btn btn-success btn-block" name="process" value="Save"/>
            </div>
        </div>
        <?php echo form_close();  
        
            }else{
                echo 'No Pending Tests';
            }
        ?>  
    </div>    
</div>
