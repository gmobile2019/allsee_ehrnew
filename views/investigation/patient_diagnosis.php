
<div style="padding-top:10px" class="row well">
  <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Complains</th>
                <th style="text-align:center;">Provisional Diagnosis</th>
                <th style="text-align:center;">Action Date</th>
                <th style="text-align:center;">Doctor</th>
             </tr>
        </thead>
        <tbody>
            <?php  if($patient_diagnosis != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($patient_diagnosis as $key=>$value){
                    $doc1=$this->SuperAdministration_model->get_member_info($value->createdby);
                    $doc1=$doc1[0]->first_name.' '.$doc1[0]->last_name;
                    
                    $doc2=$this->SuperAdministration_model->get_member_info($value->modifiedby);
                    $doc2=$doc2[0]->first_name.' '.$doc2[0]->last_name;
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->complains; ?></td>
                        <td>
                            <ul>
                            <?php 
                            $provisional=explode($this->config->item('multi_diagnosis_glue'),$value->provisionaldiagnosis);
                            foreach($provisional as $val){
                                $name=$this->Clinical_model->standard_complains(null,$val);
                                echo "<li>".$name[0]->name."</li>";
                            }
                            ?>
                            </ul>
                        </td>
                        <td><?php echo $value->createdon; ?></td>
                        <td><?php echo $doc1; ?></td>
                    </tr>  
                <?php } ?>
                     
                <?php }else{ ?>
            <tr>
                <td colspan="5" style="text-align:center"> No Diagnosis</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
</div>
