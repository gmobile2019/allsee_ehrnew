<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div style="text-align: center;padding-top: 20px" class="row">
    
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Investigation/patient_overview/$patientid/$patientvisitid/1",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">export</span>'); ?>
        
    </div>
</div>
<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
        <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Service Name</th>
                <th style="text-align:center;">Test</th>
                <th style="text-align:center;">Result</th>
                <th style="text-align:center;">Examiner</th>
                <th style="text-align:center;">Approver</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($tests != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($tests as $key=>$value){
                    $approval=null;
                    $disapproval=null;
                    if($value->status == 1){
                        
                        $approval=anchor('Investigation/reject_approve_patient_test_result/'.$value->patientid.'/'.$value->patientvisitid.'/'.$value->id.'/2','<span class="glyphicon glyphicon-ok" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Approve"></span>');
                        $disapproval=anchor('Investigation/reject_approve_patient_test_result/'.$value->patientid.'/'.$value->patientvisitid.'/'.$value->id.'/3','<span class="glyphicon glyphicon-remove" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Reject"></span>');
                        
                        $status='Pending Approval';
                    }
                    
                    if($value->status == 2){
                        
                        $status='Approved';
                    }
                    
                    if($value->status == 3){
                        
                        $status='Rejected';
                    }
                    
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td >&nbsp;&nbsp;<?php echo $value->name ?></td>
                        <td >&nbsp;&nbsp;<?php echo $value->test ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->result; ?></td>
                        <td>&nbsp;&nbsp;<?php $examiner = $this->ion_auth->user($value->createdby)->row();echo $examiner->first_name.' '.$examiner->last_name; ?></td>
                        <td>&nbsp;&nbsp;<?php $approver = $this->ion_auth->user($value->approvedby)->row();echo $approver->first_name.' '.$approver->last_name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $status; ?></td>
                        <td style="text-align: center">&nbsp;&nbsp;
                            <?php 
                            if($supervisor){
                                echo $approval."&nbsp;&nbsp;".$disapproval;
                            } ?>
                        </td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    </div>    
</div>
