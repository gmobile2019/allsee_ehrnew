<script type="text/javascript">
    $(document).ready(function(){
       
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Investigation/add_investigation_service_test/'.$id); 
                ?>
                <div class="form-group row">
                    <label for="service" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Service&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="service" id="service" class="form-control" >
                            <option></option>
                            <?php foreach($services as $key=>$value){ ?>

                            <option value="<?php echo $value->serviceid; ?>" <?php echo ($id != null && trim($value->serviceid) == trim($test[0]->serviceid))?'selected="selected"':set_select('service',$value->serviceid); ?>><?php echo $value->name; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('service'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="test" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Test Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="test" id="height" placeholder="Test Name" value="<?php echo $id <> null?$test[0]->test:set_value('test'); ?>"/>
                        <?php echo form_error('test'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="resultcategory" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Result Category&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="resultcategory" id="resultcategory" class="form-control" >
                            <option value="Dynamic" <?php echo $id <> null && $test[0]->resultcategory =='Dynamic'?"selected='selected'":set_select('resultcategory','Dynamic');?>>Dynamic</option>
                            <option value="Fixed" <?php echo $id <> null && $test[0]->resultcategory =='Fixed'?"selected='selected'":set_select('resultcategory','Fixed');?>>Fixed</option>
                        </select>
                        <?php echo form_error('resultcategory'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="unit" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Test Unit</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="unit" id="unit" placeholder="Unit" value="<?php echo $id <> null?$test[0]->unit:set_value('unit'); ?>"/>
                        <?php echo form_error('unit'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
