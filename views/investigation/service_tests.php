
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Investigation/service_tests',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="service"></label>
                        <select name="service" id="service" class="form-control" >
                                <option value="" ></option>
                                <?php foreach($services as $key=>$value){ ?>

                                <option value="<?php echo $value->serviceid; ?>" <?php echo ($service == $value->serviceid )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                   <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <?php if($supervisor){?>
            <tr>
                <th style="text-align:center;" colspan="8"><?php echo anchor('Investigation/add_investigation_service_test', '<span class="btn btn-xs btn-primary">Add Test</span>')?></th>
            </tr>
            <?php } ?>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Service Name</th>
                <th style="text-align:center;">Test Name</th>
                <th style="text-align:center;">Investigation Category</th>
                <th style="text-align:center;">Result Category</th>
                <th style="text-align:center;">Unit</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($service_tests != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                    
                foreach($service_tests as $key=>$value){
                    $cat=$this->SuperAdministration_model->investigation_categories(null,$value->investigationcategory);
                   ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->test; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $cat[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->resultcategory; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->unit; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status == 1?'Active':'Suspended'; ?></td>
                        <?php 
                        
                        $active_status=$value->status == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 
                        
                        ?>
                        <td>&nbsp;&nbsp;
                            <?php 
                            if($supervisor){
                                 echo anchor('Investigation/activate_deactivate_investigation_service_test/'.$value->id.'/'.$value->status,$active_status); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('Investigation/add_investigation_service_test/'.$value->id,'<span class="glyphicon glyphicon-edit" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                            <?php } ?>
                        </td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
