<!DOCTYPE html>
<html lang="en">
<head>
<title>Allsee-EHR</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap-theme.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/custom.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-1.3.2.min.js" language="javascript" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>

<script type="text/javascript">
    $(document).ready(function(){
       
      $('#identity').focus();
      $('#show2').hide();
      $('.blink').blink({delay:1000});
        
    });
</script>

<style type="text/css">
    .img {
    position: relative;
    border-radius: 4px;
   
   
}
</style>
</head>
<body >
    <div id="body-div" class="container-fluid">
        <div id="body-bar" class="row" style="padding-top: 10px;padding-bottom: 20px">
            <div class="col-xs-12 col-sm-12 col-md-12 well" id="topbar">
                <span id="welcome_word" class="blink">
                    <?php 
                        $category=$this->SuperAdministration_model->institution_categories($institution->category);
                        echo $institution <> null?$institution->name.' '.$category[0]->name:"HOSPITAL/DISPENSARY NAME"?>
                </span>
            </div>
        </div>
        <div id="content" class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                     <div id="myCarousel" class="carousel slide" data-ride="carousel">
                         <?php 
                         $images=$this->SuperAdministration_model->institution_cover_images(1);
                         $indicators=count($images);
                         ?>
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <?php for($i=0;$i<$indicators;$i++){ 
                                     $active=null;
                                    if($i==0){
                                        $active='class="active"';
                                    }
                                ?>
                                <li data-target="#myCarousel" data-slide-to="<?php echo $i?>" <?php echo $active?>></li>
                            <?php }?>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox" style="height:427px">
                            
                            <?php 
                            
                            $i=0;
                            foreach($images as $key=>$value){ 
                                $active=null;
                                if($i==0){
                                    $active='active';
                                }
                                ?>
                            
                              <div class="item <?php echo $active ;?> img">
                                <img src="<?php echo base_url().'images/cover/'.$value->imagename ?>" height='300px' width="500px" alt="<?php echo $value->imagename ?>">
                              </div>
                            
                            <?php $i++; } ?>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 pull-right" id="display_login">
                    <div style="padding: 20px">
                        <div id="login_into"><img src="<?php echo base_url();?>images/login.jpg" alt="Flower" id="login"/>Users Login</div>
                         <?php 

                            $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                            echo form_open('Auth/login/');
                    ?>  
                        <div class="form-group row">
                          <div class="col-sm-offset-4 col-md-offset-5 col-lg-offset-5 col-xs-12 col-sm-8 col-md-6 col-lg-6">
                            <?php echo $message; ?>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="identity" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Username</label>
                          <div class="col-xs-12 col-sm-8 col-md-8">
                            <input type="text" class="form-control" name="identity" id="identity" placeholder="Username" value="<?php echo set_value('identity')?>">
                            <?php echo form_error('identity'); ?>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="password" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Password</label>
                          <div class="col-xs-12 col-sm-8 col-md-8">
                              <input type="password" class="form-control" name="password" id="password" placeholder="Password"/>
                              <?php echo form_error('password'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-md-offset-6 col-lg-offset-6 col-xs-12 col-sm-8 col-md-6 col-lg-6">
                                 <button type="submit" class="btn btn-success">Login</button>
                            </div>
                        </div>
                      <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
            <div id="footer-div">
                <div class="col-xs-4 col-sm-4 col-md-4">

                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    &copy;&nbsp;<?php echo date('Y')?>&nbsp;Allsee-EHR.&nbsp;All Rights Reserved

                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">

                </div>
            </div>
   </div>
</body>
</html>
