
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Service Code</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Description</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($service_charges != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($service_charges as $key=>$value){
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->service_code; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->description; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status == 1?'Active':'Suspended'; ?></td>
                        <?php 
                        
                        $active_status=$value->status == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 
                        
                        ?>
                        <td>&nbsp;&nbsp;
                            <?php echo anchor('SuperAdministration/add_service_charges/'.$value->id,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('SuperAdministration/activate_deactivate_service_charges/'.$value->id.'/'.$value->status,$active_status); ?></td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="6" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
