
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('SuperAdministration/investigation_services',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="category"></label>
                        <select name="category" id="category" class="form-control" >
                                <option value="" ></option>
                                <?php foreach($categories as $key=>$value){ ?>

                                <option value="<?php echo $value->code; ?>" <?php echo ($category == $value->code )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                   <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Service Name</th>
                <th style="text-align:center;">Investigation Category</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($investigation_services != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                    
                foreach($investigation_services as $key=>$value){
                    $cat=$this->SuperAdministration_model->investigation_categories(null,$value->investigationcategory);
                   ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $cat[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status == 1?'Active':'Suspended'; ?></td>
                        <?php 
                        
                        $active_status=$value->status == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 
                        
                        ?>
                        <td>&nbsp;&nbsp;
                            <?php echo anchor('SuperAdministration/activate_deactivate_investigation_category_service/'.$value->id.'/'.$value->status,$active_status); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('SuperAdministration/remove_investigation_category_service/'.$value->id,'<span class="glyphicon glyphicon-trash" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Remove"></span>'); ?></td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="5" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
