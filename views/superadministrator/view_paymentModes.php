
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('SuperAdministration/view_paymentModes',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="sponsor"></label>
                        <select name="sponsor" id="sponsor" class="form-control" >
                                <option value="" ></option>
                                <?php foreach($sponsors as $key=>$value){ ?>

                                <option value="<?php echo $value->sponsorcode; ?>" <?php echo ($sponsor == $value->sponsorcode )?'selected="selected"':''; ?>><?php echo $value->shortname; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Sponsor</th>
                <th style="text-align:center;">Code</th>
                <th style="text-align:center;">Description</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($paymentModes != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($paymentModes as $key=>$value){
                    $sp=$this->SuperAdministration_model->get_sponsor_by_code($value->sponsor_id);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $sp[0]->shortname; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->paymentmodecode; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->description; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status == 1?'Active':'Suspended'; ?></td>
                        <?php 
                        
                        $active_status=$value->status == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 
                        
                        ?>
                        <td>&nbsp;&nbsp;
                            <?php echo anchor('SuperAdministration/add_paymentMode/'.$value->id,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('SuperAdministration/activate_deactivate_paymentMode/'.$value->id.'/'.$value->status,$active_status); ?></td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="7" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
