
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('SuperAdministration/institution_config/'); 
                ?>
       
                <div class="form-group row">
                    <label for="printer" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Printer &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="printer" id="printer" class="form-control" >
                            <option></option>
                            <?php foreach($printers as $key=>$value){ ?>

                            <option value="<?php echo $value->id; ?>" <?php echo (trim($value->id) == trim($configs->printer))?'selected="selected"':set_select('printer',$value->id); ?>><?php echo $value->model; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('printer'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="reg_charge" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Registration Charge&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="reg_charge" id="reg_charge" value="1" <?php echo $configs->registrationcharge == 1?"checked='checked'":""?>> Yes
                          </label>
                          <label class="checkbox-inline">
                            <input type="checkbox" name="reg_charge" id="reg_charge" value="0" <?php echo $configs->registrationcharge == 0?"checked='checked'":""?>> No
                          </label>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
