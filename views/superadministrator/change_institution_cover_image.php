<div class="display_content">
    
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('SuperAdministration/change_institution_image/'.$id); 
                ?>
                <div class="form-group row">
                    <label for="image" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Image</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="image" id="image" value="Image"/>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
