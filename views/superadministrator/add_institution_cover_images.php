<script type="text/javascript">
    $(document).ready(function(){
          $('div.cover').hide();
          var image_count;
          var i;
          
          image_count=$('input[name=count]').val();
          
          for(i=1;i<=image_count;i++){
                  
                $('div#image'+i).show();
          }
              
          $('input[name=count]').change(function(){
              image_count=$(this).val();
              $('div.cover').hide();
              for(i=1;i<=image_count;i++){
                  
                  $('div#image'+i).show();
              }
          });
          
        });
</script>
<div class="display_content">
    
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('SuperAdministration/add_institution_images/'); 
                ?>
                <div class="form-group row">
                    <label for="count" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Image Count &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="number" class="form-control" name="count" id="count" placeholder="Image Count" value="<?php echo set_value('count'); ?>"/>
                        &nbsp;&nbsp;<span style="font-style: italic;font-weight: bold">maximum 6 images</span>
                        <?php echo form_error('count'); ?>
                    </div>
                </div>
                <div class="form-group row cover" id="image1">
                    <label for="image-1" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Image 1</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="image-1" id="image-1" value="Image 1"/>
                    </div>
                </div>
                <div class="form-group row cover" id="image2">
                    <label for="image-2" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Image 2</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="image-2" id="image-2" value="Image 2"/>
                    </div>
                </div>
                <div class="form-group row cover" id="image3">
                    <label for="image-3" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Image 3</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="image-3" id="image-3" value="Image 3"/>
                    </div>
                </div>
                <div class="form-group row cover" id="image4">
                    <label for="image-4" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Image 4</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="image-4" id="image-4" value="Image 4"/>
                    </div>
                </div>
                <div class="form-group row cover" id="image5">
                    <label for="image-5" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Image 5</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="image-5" id="image-5" value="Image 5"/>
                    </div>
                </div>
                <div class="form-group row cover" id="image6">
                    <label for="image-6" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Image 6</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="image-6" id="image-6" value="Image 6"/>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
