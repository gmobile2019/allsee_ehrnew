
<style type="text/css">
div#display_data {
    overflow: scroll;
}
</style>
<div style="padding-top:10px" class="display_content">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <table class="table table-condensed table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th>S/No</th>
                <th>Details</th>
                <th>Image</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
                    
            <?php if($institution_cover_images <> null){
                    $i=1;
                    foreach($institution_cover_images as $key=>$value){ 
                        $active_status=$value->status == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 
                        $nme=explode('.',$value->imagename);
                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td>
                                <ul style="font-weight: bold">
                                    <li>Sequence : <?php echo $value->sequence; ?></li>
                                    <li>Filename : <?php echo $nme[0]; ?></li>
                                    <li>Extension : <?php echo $nme[1]; ?></li>
                                    <li>Status : <?php echo $value->status ==1?'Active':'Suspended'; ?></li>
                                    <li>Last Update : <?php echo $value->lastupdate; ?></li>
                                </ul>
                            </td>
                            <td>&nbsp;&nbsp;
                                <img height='50px' width="100px" src="<?php echo base_url().'images/cover/'.$value->imagename ?>" alt="Image"/>
                            </td>
                            <td style="text-align: center">
                            <?php echo anchor('SuperAdministration/change_institution_image/'.$value->id,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Change"></span>'); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('SuperAdministration/activate_deactivate_image/'.$value->id.'/'.$value->status,$active_status); ?>
                            </td>
                        </tr>
                    <?php }
            }?>
        </tbody>
    </table>
</div>
    
</div>
