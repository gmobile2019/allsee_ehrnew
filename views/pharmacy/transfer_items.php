<script type="text/javascript">
    $(document).ready(function(){
        var itemid;
     
        
        $('input[type=checkbox]').each(function(){

            itemid=$(this).val();

            $("#expire_"+itemid).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
        });
       
    });
</script>
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Pharmacy/stock_transfer/'.$code,$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                    </div>
    
                    <?php if($code == $this->config->item('drug_code')){ ?>
                    <div class="form-group ">
                        <label class="sr-only" for="drug_type"></label>
                        <select name="drug_type" id="drug_type" class="form-control" >
                                <option value="" ></option>
                                <?php foreach($drug_types as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($drug_type == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="drug_grp"></label>
                        <select name="drug_grp" id="drug_grp" class="form-control" >
                                <option value="" ></option>
                                <?php foreach($drug_groups as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($drug_grp == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Pharmacy/stock_transfer/'.$code,$attributes); 
                ?>
	<table class="table table-condensed table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Stock</th>
                <th style="text-align:center;">Destination</th>
                <th style="text-align:center;">Quantity</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($items != null){
               
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                    $j=0;
                foreach($items as $key=>$value){
                    $in_stock=$this->Inventory_model->inventory_item_stock($value->id,$unit);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;
                            <?php echo $i++; ?>
                        </td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?><input type="hidden" name="itemid_<?php echo $value->id?>" value="<?php echo $value->id?>"/></td>
                        <td>&nbsp;&nbsp;<?php echo $in_stock->quantity > 0?$in_stock->quantity:'No Stock'; ?></td>
                        <td>
                            &nbsp;&nbsp;
                            <select name="destination_<?php echo $value->id; ?>" id="destination__<?php echo $value->id; ?>" class="form-control" >
                                <option></option>
                                <?php foreach($units as $ky=>$val){ 
                                    if($val->id <> $unit){
                                    ?>

                                <option value="<?php echo $val->id; ?>" <?php echo set_select('destination_'.$value->id,$val->id); ?>><?php echo $val->name; ?></option>

                                <?php } } ?>
                            </select>
                            <?php echo form_error('destination__'.$value->id); ?>
                        </td>
                        <td>&nbsp;&nbsp;
                            <input type="number" class="form-control" name="quantity_<?php echo $value->id; ?>" id="quantity_<?php echo $value->id; ?>" placeholder="Quantity" value="<?php echo set_value('quantity_'.$value->id); ?>" />
                            <?php echo form_error('quantity_'.$value->id); ?>
                        </td>
                        <td>&nbsp;&nbsp;
                            <?php if($in_stock->quantity > 0){ ?>
                                <input type="checkbox" name="stocktransfer[<?php echo $value->id; ?>]" id="stocktransfer_<?php echo $value->id; ?>" value="<?php echo $value->id; ?>" <?php echo set_checkbox('stocktransfer['.$value->id.']', $value->id)?>>
                            <?php } ?>
                            
                        </td>
                    </tr>  
                <?php $j++; } ?>
                    <tr>
                        <td colspan="6" style="text-align:center"> <input type="submit" class="btn btn-success" name="save" value="Save"></td>
                    </tr>
              <?php  }else{ ?>
            <tr>
                <td colspan="6" style="text-align:center">NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
<?php echo form_close(); ?>
    
</div>
