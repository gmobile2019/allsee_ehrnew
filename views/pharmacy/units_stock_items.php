
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Pharmacy/units_stock/'.$unit.'/'.$code,$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                    </div>
    
                    <?php if($code == $this->config->item('drug_code')){ ?>
                    <div class="form-group ">
                        <label class="sr-only" for="drug_type"></label>
                        <select name="drug_type" id="drug_type" class="form-control" >
                                <option value="" ></option>
                                <?php foreach($drug_types as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($drug_type == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="drug_grp"></label>
                        <select name="drug_grp" id="drug_grp" class="form-control" >
                                <option value="" ></option>
                                <?php foreach($drug_groups as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($drug_grp == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
	<table class="table table-condensed table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Quantity</th>
                <th style="text-align:center;">Last Stocking</th>
                <th style="text-align:center;">Last De-stocking</th>
                <th style="text-align:center;">More</th>
             </tr>
        </thead>
        <tbody>
            <?php if($items != null){
               
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($items as $key=>$value){
                    
                    $itemstock=$this->Inventory_model->inventory_item_stock($value->id,$unit);
                    $itemstockingdetails=$this->Inventory_model->latest_stocking_details($value->id,$unit);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $itemstock->quantity>0?$itemstock->quantity:0; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $itemstockingdetails['lasteststocking']; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $itemstockingdetails['latestdestocking']; ?></td>
                        <td>&nbsp;&nbsp;
                            <?php echo anchor('Pharmacy/inventory_unit_item_stock_details/'.$value->id.'/'.$code.'/'.$unit,'<span class="glyphicon glyphicon-arrow-right" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="More Details"></span>'); ?>
                        </td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="6" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>

    
</div>
