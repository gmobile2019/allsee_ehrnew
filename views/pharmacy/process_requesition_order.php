<script type="text/javascript">
    $(document).ready(function(){
        var addType;
       
        /*
        * addType radio button
       */
       $('input[name=addType]').each(function(){
          addType=$(this).val();
          
          if($(this).is(':checked')){
               if(addType == 1){
                
                $('div.single').show();
                $('div.bulk').hide();
                
                }else{
                
                    $('div.single').hide();
                    $('div.bulk').show();
                }
          }
       });
       
       
       $('input[name=addType]').change(function(){
            
             addType=$(this).val();
             if($(this).is(':checked')){
                 
                 if(addType == 1){
                
                $('div.single').show('slow');
                $('div.bulk').hide();
                
                }else{

                    $('div.single').hide();
                    $('div.bulk').show('slow');
                }
             }
            
       });
       
    });
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Inventory/process_requesition_order/'); 
                ?>
       
                
                <div class="form-group row">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Item</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                         <input type="text" class="form-control" readonly="true" id="name" value="<?php echo $order->name; ?>"/>
                         <?php echo form_error('itemid'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="r_unit" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">R/Inventory Unit</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                         <input type="text" class="form-control" readonly="true" id="r_unit" value="<?php $r_unt=$this->Inventory_model->units($order->inventoryunit); echo $r_unt[0]->name; ?>"/>
                         <?php echo form_error('r_unit'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="r_qty" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">R/Inventory Unit</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                         <input type="text" class="form-control" readonly="true" id="r_qty" value="<?php echo $order->requestqty; ?>"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="s_qty" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">S/Quantity&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                         <input type="text" class="form-control" name="s_qty" id="s_qty" value="<?php echo set_value('s_qty');; ?>"/>
                         <?php echo form_error('s_qty'); ?>
                    </div>
                </div>
                <input type="hidden" name="reqid" value="<?php echo $reqid; ?>"/>
                <input type="hidden" name="itemid" value="<?php echo $itemid; ?>"/>
                <input type="hidden" name="r_unit" value="<?php echo $order->inventoryunit; ?>"/>
                
               <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
