<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Pharmacy/expired_items',$attributes); 
                ?>
                    
                    <div class="form-group ">
                        <label class="sr-only" for="category"></label>
                        <select name="category" id="category" class="form-control" >
                                <option value="" >Item Category</option>
                                <?php foreach($item_categories as $key=>$value){ ?>

                                <option value="<?php echo $value->code; ?>" <?php echo ($category == $value->code )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="item"></label>
                        <input type="text" class="form-control" name="item" id="item" placeholder="Item" value="<?php echo $item; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start (Yyyy-mm-dd)" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End (Yyyy-mm-dd)" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Pharmacy/expired_items/1_".$category."_".$item."_".$unit."_".$start."_".$end."/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php echo anchor("Pharmacy/expired_items/2_".$category."_".$item."_".$unit."_".$start."_".$end."/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
    </div>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Inventory Unit</th>
                <th style="text-align:center;">Item</th>
                <th style="text-align:center;">Category</th>
                <th style="text-align:center;">Store Batch</th>
                <th style="text-align:center;">Expire Date</th>
                <th style="text-align:center;">Quantity</th>
                <th style="text-align:center;">Value</th>
             </tr>
        </thead>
        <tbody>
            <?php if($expired != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($expired as $key=>$value){
                    $total +=$value['value'];
                    $total_qty +=$value['quantity'];
                    $un=$value['unit'] <> null?$this->Inventory_model->units($value['unit']):null;
                    $cat=$this->Inventory_model->get_item_category_by_code($value['category']);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $un==null?'Main Store':$un[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value['item']; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $cat->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value['batch']; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value['expiry']; ?></td>
                        <td style="text-align:right"><?php echo $value['quantity']; ?>&nbsp;&nbsp;</td>
                        <td style="text-align:right"><?php echo number_format($value['value'],2); ?>&nbsp;&nbsp;</td>
                    </tr>  
                <?php } ?>
                    <tr>
                        <th colspan="6" style="text-align:center">Total</th>
                        <th style="text-align:right"><?php echo $total_qty?>&nbsp;&nbsp;</th>
                        <th style="text-align:right"><?php echo number_format($total, 2)?>&nbsp;&nbsp;</th>
                    </tr>  
                <?php }else{ ?>
            <tr>
                <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
