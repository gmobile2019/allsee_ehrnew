<script type="text/javascript">
    $(document).ready(function(){
       $('div#add_order').hide();
       var subdepartment;
       var label;
       subdepartment=$('select[name=subdepartment]').val();
       
       $('div#services').find('table').find('label').each(function(){
               label=$(this).attr('id');
               
               if(label === 'subdept'+subdepartment){
                   $('div#add_order').show();
                   $(this).show();
               }else{
                   $(this).hide();
               }
               
        });
       
       $('select[name=subdepartment]').change(function(){
           $('div#add_order').hide();
           subdepartment=$(this).val();
         
           $('div#services').find('table').find('label').each(function(){
               label=$(this).attr('id');
               
               if(label === 'subdept'+subdepartment){
                   $('div#add_order').show();
                   $(this).show();
               }else{
                   $(this).hide();
               }
               
           });
       });
       
    });
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Reception/create_service_order/'.$patientid.'/'.$patientvisitid); 
                   
                ?>
       
                
                <div class="form-group row">
                    <label for="subdepartment" class="col-xs-12 col-md-offset-2 col-lg-offset-2 col-sm-3 col-md-3 col-lg-3 control-label">Sub Department</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="subdepartment" id="subdepartment" class="form-control" >
                        <option></option>
                        <?php foreach($subdepartments as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo set_select('subdepartment',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('subdepartment'); ?>
                    </div>
                </div>
                
                <div class="form-group row " id="services">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-condensed table-hover">
                            <tr><td>
                            <?php $i=1;
                                  $count=count($services);
                                  $counter=0;
                            foreach($services as $key=>$value){ 
                                $j=$i%6;
                                $counter++;
                                if($j==0){ 
                                    $i=0;
                                    ?>
                                    </td></tr>
                                    <tr><td>
                                <?php } ?>
                                    <label class="checkbox-inline " id="subdept<?php echo $value->subdepartmentid; ?>">
                                        <input type="checkbox" name="order[]" id="srv<?php echo $value->id; ?>" value="<?php echo $value->id.'_'.$value->subdepartmentid.'_'.$value->departmentid; ?>"> <?php echo $value->name; ?>
                                      </label>
                                     
                            <?php if($counter == $count && $j <> 0){ ?>
                                            
                                        </td></tr>
                            <?php }
                            $i++;
                            } ?>
                        </table>
                    </div>
                </div>
                <div class="form-group register_width_padding" id="add_order">
                    <div class="col-md-offset-3 col-md-3 col-sm-offset-2 col-sm-3">
                        <input type="submit" class="btn btn-success" name="add_service" value="Add Service(s)">
                    </div>
                </div>
        
        <?php echo form_close();
                    if($pending_orders <> null){
                        
                        $this->load->view('reception/service_order_payment');
                    }
             ?>
</div>
