
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Reception/add_title/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="fullname" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Full Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Full Name" value="<?php echo $id != null?$titles[0]->fullname:set_value('fullname'); ?>" />
                        <?php echo form_error('fullname'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="shortname" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Short Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="shortname" id="shortname" placeholder="Short Name" value="<?php echo $id != null?$titles[0]->shortname:set_value('shortname'); ?>" />
                        <?php echo form_error('shortname'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
