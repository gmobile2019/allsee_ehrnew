<script type="text/javascript">
    $(document).ready(function(){
        var patientid;
       
       patientid=$('input[name=patientid').val();
       
       if(patientid != null && patientid != ''){
       
            $.ajax({
            type:'POST',
            url:'<?php echo site_url('Reception/modal_patient_details'); ?>',
            data:{patientid:patientid},
            success:function(data){

                var patient=data.split("=_");

                 $('input#name').val(patient[1]+'. '+patient[2]);
            }

            });
       }
       
                
       $('input[name=patientid').change(function(){
                
                patientid=$(this).val();
               
               if(patientid != null && patientid != ''){
                   $.ajax({
                    type:'POST',
                    url:'<?php echo site_url('reception/modal_patient_details'); ?>',
                    data:{patientid:patientid},
                    success:function(data){
                       
                        var patient=data.split("=_");

                        $('input#name').val(patient[1]+'. '+patient[2]);
                    }

                    });
               }
                
 
        });
    });
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Reception/prepaid_subscription/'); 
                ?>
       
                <div class="form-group row">
                    <label for="patientid" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Patient Id &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="patientid" id="patientid" placeholder="Patient Id" value="<?php echo set_value('patientid'); ?>" />
                        <?php echo form_error('patientid'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Patient Name</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" readonly="true" id="name" value=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="amount" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Amount&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="amount" id="amount" value="<?php echo set_value('amount'); ?>" />
                        <?php echo form_error('amount'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save Subscription</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
