<script type="text/javascript">
    $(document).ready(function(){
        $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
       $('div.display_content').find('table').find('tbody').find('tr').find('td.patientdetails').find('a').click(function(){
                
                var patientid=$(this).attr('id');
               
                $.ajax({
                type:'POST',
                url:'<?php echo site_url('reception/modal_patient_details'); ?>',
                data:{patientid:patientid},
                success:function(data){

                    var patient=data.split("=_");
                    
                     $('div#myModal').find('table').find('td#patientid').text(patient[0]);
                     $('div#myModal').find('table').find('td#name').text(patient[1]);
                     $('div#myModal').find('table').find('td#gender').text(patient[2]);
                     $('div#myModal').find('table').find('td#marital').text(patient[3]);
                     $('div#myModal').find('table').find('td#tribe').text(patient[4]);
                     $('div#myModal').find('table').find('td#dob').text(patient[5]);
                     $('div#myModal').find('table').find('td#email').text(patient[6]);
                     $('div#myModal').find('table').find('td#phone').text(patient[7]);
                     $('div#myModal').find('table').find('td#region').text(patient[8]);
                     $('div#myModal').find('table').find('td#district').text(patient[9]);
                     $('div#myModal').find('table').find('td#street').text(patient[10]);
                     $('div#myModal').find('table').find('td#occupation').text(patient[11]);
                     
                     $('#myModal').modal({
                        keyboard:true,
                       show:true,
                       })
                 }

                });
 
            });
            
            $('div.display_content').find('table').find('tbody').find('tr').find('td.patientvisitdetails').find('a').click(function(){
                
                var patientvisitid=$(this).attr('id');
               
                $.ajax({
                type:'POST',
                url:'<?php echo site_url('reception/modal_patient_visit_details'); ?>',
                data:{patientvisitid:patientvisitid},
                success:function(data){
                    
                    var patientvisitid=data.split("=_");
                    
                     $('div#patientVisitmyModal').find('table').find('td#pid').text(patientvisitid[0]);
                     $('div#patientVisitmyModal').find('table').find('td#pvid').text(patientvisitid[1]);
                     $('div#patientVisitmyModal').find('table').find('td#vctg').text(patientvisitid[2]);
                     $('div#patientVisitmyModal').find('table').find('td#pctg').text(patientvisitid[3]);
                     $('div#patientVisitmyModal').find('table').find('td#cctg').text(patientvisitid[4]);
                     $('div#patientVisitmyModal').find('table').find('td#doc').text(patientvisitid[5]);
                     $('div#patientVisitmyModal').find('table').find('td#cstatus').text(patientvisitid[6]);
                     $('div#patientVisitmyModal').find('table').find('td#sp').text(patientvisitid[7]);
                     $('div#patientVisitmyModal').find('table').find('td#que').text(patientvisitid[8]);
                     
                     $('#patientVisitmyModal').modal({
                        keyboard:true,
                       show:true,
                       })
                 }

                });
 
            });
        });
</script>
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Reception/in_patients',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Full Name" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="pid"></label>
                        <input type="text" class="form-control" name="pid" id="pid" placeholder="Patient Id" value="<?php echo $pid; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Patient Id</th>
                <th style="text-align:center;">Patient Visit Id</th>
                <th style="text-align:center;">Full Name</th>
                <th style="text-align:center;">Timestamp</th>
                <th style="text-align:center;">Que Number</th>
                <th style="text-align:center;">Assigned Doctor</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($patients != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($patients as $key=>$value){
                    $sp=$this->SuperAdministration_model->sponsors($value->sponsorid);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td class="patientdetails">&nbsp;&nbsp;<a type="button" href="#" data-toggle="modal" id="<?php echo $value->patientid ?>"><?php echo $value->patientid ?></a></td>
                        <td class="patientvisitdetails">&nbsp;&nbsp;<a type="button" href="#" data-toggle="modal" id="<?php echo $value->patientvisitid ?>"><?php echo $value->patientvisitid ?></a></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->quenumber; ?></td>
                        <td>&nbsp;&nbsp;<?php $doctor = $this->ion_auth->user($value->assigneddoctor)->row();echo $doctor->first_name.' '.$doctor->last_name; ?></td>
                        <td style="text-align: center">&nbsp;&nbsp;
                            <?php
                            echo anchor('Reception/create_service_order/'.$value->patientid.'/'.$value->patientvisitid,'<span class="glyphicon glyphicon-arrow-right" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Process Services"></span>'); ?>
                        </td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
 <!-- Modal -->
       <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="gridSystemModalLabel">Patient Details</h4>
            </div>
            <div class="modal-body">
              <div class="row container">
                  <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                      <table class="table table-condensed table-hover">
                          <tr>
                              <td>Patient Id</td>
                              <td id="patientid"></td>
                          </tr>
                          <tr>
                              <td>Name</td>
                              <td id="name"></td>
                          </tr>
                          <tr>
                              <td>Gender</td>
                              <td id="gender"></td>
                          </tr>
                          <tr>
                              <td>Marital Status</td>
                              <td id="marital"></td>
                          </tr>
                          <tr>
                              <td>Tribe</td>
                              <td id="tribe"></td>
                          </tr>
                          <tr>
                              <td>Date of Birth</td>
                              <td id="dob"></td>
                          </tr>
                          <tr>
                              <td>Email</td>
                              <td id="email"></td>
                          </tr>
                          <tr>
                              <td>Phone</td>
                              <td id="phone"></td>
                          </tr>
                          <tr>
                              <td>Region</td>
                              <td id="region"></td>
                          </tr>
                          <tr>
                              <td>District</td>
                              <td id="district"></td>
                          </tr>
                          <tr>
                              <td>Street</td>
                              <td id="street"></td>
                          </tr>
                          <tr>
                              <td>Occupation</td>
                              <td id="occupation"></td>
                          </tr>
                      </table>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div> 
 
  <!-- Patient Visit Modal -->
       <div class="modal fade" id="patientVisitmyModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="gridSystemModalLabel">Patient Visit Details</h4>
            </div>
            <div class="modal-body">
              <div class="row container">
                  <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                      <table class="table table-condensed table-hover">
                          <tr>
                              <td>Patient Id</td>
                              <td id="pid"></td>
                          </tr>
                          <tr>
                              <td>Patient Visit Id</td>
                              <td id="pvid"></td>
                          </tr>
                          <tr>
                              <td>Visit Category</td>
                              <td id="vctg"></td>
                          </tr>
                          <tr>
                              <td>Que Number</td>
                              <td id="que"></td>
                          </tr>
                          <tr>
                              <td>Patient Category</td>
                              <td id="pctg"></td>
                          </tr>
                          <tr>
                              <td>Consultation Category</td>
                              <td id="cctg"></td>
                          </tr>
                          <tr>
                              <td>Assigned Doctor</td>
                              <td id="doc"></td>
                          </tr>
                          <tr>
                              <td>Consultation Status</td>
                              <td id="cstatus"></td>
                          </tr>
                          <tr>
                              <td>Sponsor</td>
                              <td id="sp"></td>
                          </tr>
                      </table>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div> 
</div>
