<script type="text/javascript">
    $(document).ready(function(){
        
       var consultation_category;
       var sponsor;
       var service;
       var doctor;
       var payment_mode;
       var cost;
       var srv;
       var subdpt;
       var spnsr;
       
       consultation_category=$('select[name=consultationcategory]').val();
       
       $('select[name=service]').find('option').each(function(){
           service=$(this).attr('class');
           
           if(service === "srvg"+consultation_category){
               $(this).show();
           }else{
               $(this).hide();
           }
       });
       
       $('select[name=doctor]').find('option').each(function(){
           doctor=$(this).attr('class');
           
        if(doctor === "dcg"+consultation_category){
                 $(this).show();
             }else{
                 $(this).hide();
             }
         });

        $('select[name=consultationcategory]').change(function(){
            consultation_category=$(this).val();

            $('select[name=service]').find('option').each(function(){
            service=$(this).attr('class');

                if(service === "srvg"+consultation_category){
                    $(this).show();
                }else{
                    $(this).hide();
                }
            });

            $('select[name=doctor]').find('option').each(function(){
                doctor=$(this).attr('class');

                if(doctor === "dcg"+consultation_category){
                    $(this).show();
                }else{
                    $(this).hide();
                }
            });
       });
       
       //sponsor and payment modes
       sponsor=$('select[name=sponsor]').val();
       
       $('select[name=payment_mode]').find('option').each(function(){
           payment_mode=$(this).attr('class');
           
           if(payment_mode === "pmd"+sponsor){
               $(this).show();
           }else{
               $(this).hide();
           }
       });
       
       $('select[name=sponsor]').change(function(){
            sponsor=$(this).val();

            $('select[name=payment_mode]').find('option').each(function(){
              payment_mode=$(this).attr('class');

                if(payment_mode === "pmd"+sponsor){
                    $(this).show();
                }else{
                    $(this).hide();
                }
            });

        });
       
       payment_mode=$('select[name=payment_mode]').val();
       
       if(payment_mode == '<?php echo $this->config->item('package_bill_pmode_code'); ?>'){
            $('div.package').show();
        }else{
           $('div.package').hide(); 
        }
            
       $('select[name=payment_mode]').change(function(){
            payment_mode=$(this).val();

            if(payment_mode == '<?php echo $this->config->item('package_bill_pmode_code'); ?>'){
                $('div.package').show();
            }else{
               $('div.package').hide(); 
            }

        });
        
       //service cost
       srv=$('select[name=service]').val();
       subdpt=$('select[name=consultationcategory]').val();
       spnsr=$('select[name=sponsor]').val();
       
       $.ajax({
        type:'POST',
        url:'<?php echo site_url('reception/service_cost'); ?>',
        data:{subdepartment:subdpt,service:srv,sponsor:spnsr},
        success:function(data){

             $('input[type=text]#rd_cost').val(data)
             $('input[type=hidden]#cost').val(data)
         }

        });
        
        $('select[name=sponsor]').change(function(){
            spnsr=$(this).val();

            $.ajax({
            type:'POST',
            url:'<?php echo site_url('reception/service_cost'); ?>',
            data:{subdepartment:subdpt,service:srv,sponsor:spnsr},
            success:function(data){

                 $('input[type=text]#rd_cost').val(data)
                 $('input[type=hidden]#cost').val(data)
             }

            });

        });
        
        $('select[name=consultationcategory]').change(function(){
            subdpt=$(this).val();
            $.ajax({
            type:'POST',
            url:'<?php echo site_url('reception/service_cost'); ?>',
            data:{subdepartment:subdpt,service:srv,sponsor:spnsr},
            success:function(data){

                 $('input[type=text]#rd_cost').val(data)
                 $('input[type=hidden]#cost').val(data)
             }

            });
            
       });
       
       $('select[name=service]').change(function(){
            srv=$(this).val();
            $.ajax({
            type:'POST',
            url:'<?php echo site_url('reception/service_cost'); ?>',
            data:{subdepartment:subdpt,service:srv,sponsor:spnsr},
            success:function(data){

                 $('input[type=text]#rd_cost').val(data)
                 $('input[type=hidden]#cost').val(data)
             }

            });
            
       });
    });
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Reception/register_patient_visit/'.$id); 
                ?>
       
                
                <div class="form-group row">
                    <label for="visit_category" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Visit Category&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="visit_category" id="visit_category" class="form-control" >
                        <option></option>
                        <?php foreach($visitCategories as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo set_select('visit_category',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('visit_category'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="consultationcategory" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Consultation Category&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="consultationcategory" id="visit_category" class="form-control" >
                        <option></option>
                        <?php foreach($doctor_categs as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo set_select('consultationcategory',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('consultationcategory'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="service" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Service&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="service" id="service" class="form-control" >
                        <option></option>
                        <?php foreach($services as $key=>$value){ ?>
                        
                        <option class="srvg<?php echo $value->subdepartmentid ?>" value="<?php echo $value->id; ?>" <?php echo set_select('service',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('service'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="doctor" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Doctor&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="doctor" id="doctor" class="form-control" >
                        <option></option>
                        <?php foreach($doctors as $key=>$value){ ?>
                        
                        <option class="dcg<?php echo $value->doctorcategory ?>" value="<?php echo $value->userid; ?>" <?php echo set_select('doctor',$value->userid); ?>><?php echo $value->first_name." ".$value->last_name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('doctor'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sponsor" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Sponsor&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="sponsor" id="sponsor" class="form-control" >
                        <option></option>
                        <?php foreach($sponsors as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->sponsorcode; ?>" <?php echo set_select('sponsor',$value->sponsorcode); ?>><?php echo $value->shortname; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('sponsor'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="payment_mode" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Payment Mode&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="payment_mode" id="payment_mode" class="form-control" >
                        <option></option>
                        <?php foreach($paymentModes as $key=>$value){ ?>
                        
                        <option class="pmd<?php echo $value->sponsor_id ?>" value="<?php echo $value->paymentmodecode; ?>" <?php echo set_select('payment_mode',$value->paymentmodecode); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('payment_mode'); ?>
                    </div>
                </div>
                <div class="form-group row package">
                    <label for="package" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Package&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="package" id="package" class="form-control" >
                        <?php foreach($packages as $key=>$value){ 
                            $pckg=$this->Administration_model->packages($value->packageid);
                            ?>
                        
                        <option  value="<?php echo $value->packageid.'_'.$value->subscriptionid; ?>" <?php echo set_select('package',$value->packageid.'_'.$value->subscriptionid); ?>><?php echo $pckg[0]->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('package'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cost" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Cost &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" readonly="true" id="rd_cost" placeholder="Cost" value="" />
                        <input type="hidden" class="form-control" name="cost" id="cost"/>
                        <?php echo form_error('cost'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Process Visit</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
