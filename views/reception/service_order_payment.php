<script type="text/javascript">
    $(document).ready(function(){
        var orderid;
        var qty;
        var uprice;
        var cost;
        var pmode;
        
        $('table').find('tbody').find('tr').find('td.qty').each(function(){
            
           orderid=$(this).attr('id');
           qty=$('input[name=qty'+orderid+']').val();
           uprice=$('input[name=uprice'+orderid+']').val();
           
           if($.isNumeric(qty) && $.isNumeric(uprice)){
                cost=qty*uprice;
                $('input#cost'+orderid).val(cost); 
                $('input[name=cost'+orderid+']').val(cost);
           }else{
               
               $('input#cost'+orderid).val(cost); 
               $('input[name=cost'+orderid+']').val(cost);
           }
           
           
           $('input[name=qty'+orderid+']').keyup(function(){
              
               qty=$('input[name=qty'+orderid+']').val();
               uprice=$('input[name=uprice'+orderid+']').val();
           
                if($.isNumeric(qty) && $.isNumeric(uprice)){
                     cost=qty*uprice;
                     $('input#cost'+orderid).val(cost); 
                     $('input[name=cost'+orderid+']').val(cost);
                }else{

                    $('input#cost'+orderid).val(""); 
                    $('input[name=cost'+orderid+']').val("");
                }
           });
        });
        
        pmode=$('select.pmode').val();
        $('select.pmode').val(pmode);
        
        if(pmode == '<?php echo $this->config->item('package_bill_pmode_code'); ?>'){
            $('div.package').show();
        }else{
           $('div.package').hide(); 
        }
            
        $('select.pmode').change(function(){
            
            pmode=$(this).val();
            $('select.pmode').val(pmode);
            
            if(pmode == '<?php echo $this->config->item('package_bill_pmode_code'); ?>'){
                $('div.package').show();
            }else{
               $('div.package').hide(); 
            }
        });
    });
</script>
<div style="padding-top:10px" class="row">
        <?php 
                    echo $message2;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Reception/process_order_payment/'.$patientid.'/'.$patientvisitid); 
                   
                ?>
        <div class="col-xs-12 col-md-offset-3 col-md-4 col-lg-offset-3 col-lg-4 col-sm-offset-3 col-sm-4" style="font-style: italic;font-weight: bold">
            Service Payment
        </div>
        <div class="form-group package" align="center">
            <label for="package" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">&nbsp;&nbsp;</label>
            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
            <select name="package" id="package" class="form-control" >
                <?php foreach($packages as $key=>$value){ 
                    $pckg=$this->Administration_model->packages($value->packageid);
                    ?>

                <option  value="<?php echo $value->packageid.'_'.$value->subscriptionid; ?>" <?php echo set_select('package',$value->packageid.'_'.$value->subscriptionid); ?>><?php echo $pckg[0]->name; ?></option>

                    <?php } ?>

            </select>
                <?php echo form_error('package'); ?>
            </div>
        </div>
	<table class="table table-condensed table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Service</th>
                <th style="text-align:center;">U/price</th>
                <th style="text-align:center;">Qty</th>
                <th style="text-align:center;">Payment Mode</th>
                <th style="text-align:center;">Cost</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php 
             
                $sp=$this->Reception_model->patient_visit_details($patientvisitid);
                $pmodes=$this->SuperAdministration_model->get_paymentModes_by_sponsor($sp->sponsorid);
                $i=1;
                $conf=0;
                foreach($pending_orders as $key=>$value){
                    
                    $srv=$this->Administration_model->services($value->serviceid);
                    $cost=$this->Reception_model->get_active_service_cost($value->subdepartmentid,$value->serviceid,$sp->sponsorid);
                    
                    ?>
                    <tr>
                        <td>
                            &nbsp;&nbsp;
                            <?php echo $i++; ?>
                        </td>
                        <td>
                            &nbsp;&nbsp;
                            <?php echo $srv[0]->name; ?>
                        </td>
                        <td>
                            &nbsp;&nbsp;
                            <?php 
                                  echo $cost->cost;
                                  if(in_array($value->id, $blc)){
                                      echo "<div style='color:red'>balance not enough!</div>";
                                  };
                                  
                                  if(in_array($value->id, $pck)){
                                      echo "<div style='color:red'>item Inactive for package!</div>";
                                  };
                            ?>
                           
                        </td>
                        <td style="width:100px;padding-bottom: 10px" id="<?php echo $value->id; ?>" class="qty">
                            &nbsp;&nbsp;
                            <input type="text"  class="form-control"  name="qty<?php echo $value->id; ?>" value="<?php echo set_value('qty'.$value->id,1); ?>"/>
                        </td>
                        <td style="width:150px">
                            &nbsp;&nbsp;
                            <select name="pmode<?php echo $value->id; ?>" id="pmode<?php echo $value->id; ?>" class="form-control pmode">
                                <option></option>
                                <?php foreach($pmodes as $ky=>$val){ ?>
                                <option class="pmode_<?php echo $val->paymentmodecode?>" value="<?php echo $val->paymentmodecode?>" <?php echo set_select('pmode'.$value->id, $val->paymentmodecode)?>><?php echo $val->name; ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('pmode'.$value->id); ?>
                            
                        </td>
                        <td style="width:100px">&nbsp;&nbsp;
                            <input type="text" readonly="true" class="form-control" id="cost<?php echo $value->id; ?>" value=""/>
                            
                        </td>
                        <td style="width:100px">
                          
                            <?php if($cost<>null){ ?>
                                <input type="checkbox" name="confirm[]" value="<?php echo $value->id; ?>" <?php echo set_checkbox('confirm['.$conf++.']',$value->id); ?>>
                                 <?php  }?> &nbsp;&nbsp;
                                <?php echo anchor('Reception/remove_service_order/'.$value->id.'/'.$value->patientid.'/'.$value->patientvisitid,'<span class="glyphicon glyphicon-trash" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Remove"></span>'); ?>
                            
                        </td>
                    </tr>
                    
                    <input type="hidden" name="patientid<?php echo $value->id; ?>" value="<?php echo $value->patientid; ?>"/>
                    <input type="hidden" name="patientvisitid<?php echo $value->id; ?>" value="<?php echo $value->patientvisitid; ?>"/>
                    <input type="hidden" name="srv<?php echo $value->id; ?>" value="<?php echo $value->serviceid; ?>"/>
                    <input type="hidden" name="uprice<?php echo $value->id; ?>" value="<?php echo $cost->cost; ?>"/>
                    <input type="hidden" name="cost<?php echo $value->id; ?>"/>
                    <input type="hidden" name="spnsr<?php echo $value->id; ?>" value="<?php echo $sp->sponsorid; ?>"/>
                    <input type="hidden" name="dpt<?php echo $value->id; ?>" value="<?php echo $value->departmentid; ?>"/>
                    <input type="hidden" name="subdpt<?php echo $value->id; ?>" value="<?php echo $value->subdepartmentid; ?>"/>
                <?php } ?>
                    <tr>
                        <td colspan="7" align="center">
                            <input type="submit" class="btn btn-success" name="process_payment" value="Process Payment">
                        </td>
                    </tr>
        </tbody>
    </table>
    
    <?php echo form_close();?>
</div>
