<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Reception/outpatients_report',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End" value="<?php echo $end; ?>" />
                    </div>
                     <div class="form-group ">
                        <label class="sr-only" for="visitcategory"></label>
                        <select name="visitcategory" id="visitcategory" class="form-control" >
                        <option></option>
                        <?php foreach($visitcategories as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo $visitcategory == $value->id?"selected='selected'":""; ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                        </select>
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="consultationcategory"></label>
                        <select name="consultationcategory" id="consultationcategory" class="form-control" >
                        <option></option>
                        <?php foreach($consultationcategories as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo $consultationcategory == $value->id?"selected='selected'":""; ?>><?php echo $value->name; ?></option>
                        
                        <?php } ?>
                        
                    </select>
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Reception/outpatients_report/start_".$start."_end_".$end."_visitcateg_".$visitcategory."_consultationcateg_".$consultationcategory."_docType_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php echo anchor("Reception/outpatients_report/start_".$start."_end_".$end."_visitcateg_".$visitcategory."_consultationcateg_".$consultationcategory."_docType_2/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
    </div>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Patient Id</th>
                <th style="text-align:center;">Visit Id</th>
                <th style="text-align:center;">Visit/C</th>
                <th style="text-align:center;">Consultation/C</th>
                <th style="text-align:center;">Assigned Doctor</th>
                <th style="text-align:center;">Consultation Status</th>
                <th style="text-align:center;">Visit Timestamp</th>
             </tr>
        </thead>
        <tbody>
            <?php if($data != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($data as $key=>$value){
                    $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('consultation_servicecode'));
                    $consultationcateg=$this->Administration_model->consultation_categories($dept[0]->id,$value->consultationcategory);
                    $vstC=$this->Reception_model->visit_categories($value->visitcategory);
                    $patient=$this->Reception_model->patient($value->patientid);
                    $doc=$this->SuperAdministration_model->get_member_info($value->assigneddoctor);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $patient->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->patientid; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->patientvisitid; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $vstC[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $consultationcateg[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $doc[0]->first_name.' '.$doc[0]->last_name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->consultationstatus ==1?'Seen':'Not Seen'; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                    </tr>  
                <?php } 
                }else{ ?>
            <tr>
                <td colspan="9" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
