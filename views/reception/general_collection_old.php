<script type="text/javascript">
    $(document).ready(function(){
        var department;
        var subdepartment;
        
        $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            
            department=$('select[name=department]').val();
            $('select[name=subdepartment]').find('option').each(function(){
                subdepartment=$(this).attr('class');

                if(subdepartment == "dpt_"+department){
                    $(this).show();
                }else{
                    $(this).hide();
                }
            });         
                     
            $('select[name=department]').change(function(){
                 department=$(this).val();
                
                 $('select[name=subdepartment]').find('option').each(function(){
                     subdepartment=$(this).attr('class');
                     
                     if(subdepartment == "dpt_"+department){
                         $(this).show();
                     }else{
                         $(this).hide();
                     }
                 });
            });
    });
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Reception/general_collection/'); 
                ?>
       
                <div class="form-group row">
                     <label for="pdf_excel" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label"></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                       <label class="checkbox-inline">
                           <input type="radio" id="pdf_excel" name="pdf_excel" value="1" <?php echo set_radio('pdf_excel',"1")?>> Pdf
                      </label>
                      <label class="checkbox-inline">
                        <input type="radio" id="pdf_excel" name="pdf_excel" value="2" <?php echo set_radio('pdf_excel',"2"); ?>> Excel
                      </label><?php echo form_error('pdf_excel'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="start" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Start Date&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="start" id="start" value="<?php echo set_value('start'); ?>" />
                        <?php echo form_error('start'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="end" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">End Date&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                       <input type="text" class="form-control" name="end" id="end" value="<?php echo set_value('end'); ?>" />
                        <?php echo form_error('end'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="department" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Department</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="department" id="department" class="form-control" >
                        <option value="">All</option>
                        <?php foreach($departments as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo set_select('department',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('department'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="subdepartment" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Sub Department</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="subdepartment" id="subdepartment" class="form-control" >
                        <option value="">All</option>
                        <?php foreach($subdepartments as $key=>$value){ ?>
                        
                        <option class="dpt_<?php echo $value->departmentid; ?>" value="<?php echo $value->id; ?>" <?php echo set_select('subdepartment',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('subdepartment'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Generate</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
