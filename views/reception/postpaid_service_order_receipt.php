<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
     <div id="receipt" class="col-md-offset-3 col-lg-offset-3 col-sm-offset-3 col-xs-12 col-sm-8 col-md-5 col-lg-8 well">
         
         <table class="table table-hover table-condensed" id="receiptheader">
             <tr>
                 <th rowspan="4" style="text-align: center">
                     <img id="logoreceipt" alt="HRO" src="<?php echo base_url()."logo/".$institution->logoname ?>" />
                 </th>
             </tr>
             <tr>
                 <th>
                     Postal : <?php  echo $institution->postal?>
                 </th>
                 
             </tr>
             <tr>
                 <th>
                     City : <?php echo $institution->city;?>
                 </th>
             </tr>
             <tr>
                 <th>
                     Phone No : <?php echo $institution->phone;?>
                 </th>
             </tr>
         </table>
         <table class="table table-hover table-condensed">
             <tr>
                 <th>
                    Date :- <?php $datetime=explode(" ",$bill_data[0]->createdon); echo $datetime[0]?>
                 </th>
                 <th>
                     Time :- <?php echo $datetime[1]?>
                 </th>
             </tr>
             <tr>
                 <th>
                     Patient Id :- <?php  echo $bill_data[0]->patientid?>
                 </th>
                 <th>
                     Name :- <?php $name=$this->Reception_model->patient($bill_data[0]->patientid); echo $name->name;?>
                 </th>
             </tr>
             <tr>
                 <th>
                     Sponsor :- <?php  $sp=$this->SuperAdministration_model->get_sponsor_by_code($bill_data[0]->sponsorid);echo $sp[0]->shortname; ?>
                 </th>
                 <th>
                     Payment Mode :- <?php $pm=$this->SuperAdministration_model->get_paymentMode_by_code($bill_data[0]->paymentmodeid); echo $pm->name;?>
                 </th>
             </tr>
         </table>
        
         <table class="table table-hover table-condensed">
             <tr>
                 <th colspan="2">
                     Post Paid Receipt No : <?php echo $bill_data[0]->postpaid_transactionid; ?>
                 </th>
             </tr>
             <tr>
                 <th>
                     Service
                 </th>
                 <th style="text-align: right">
                     Amount
                 </th>
             </tr>
             <?php foreach($bill_data as $key=>$value){ 
                 $total +=$value->amount
                 ?>
                 <tr>
                    <td>
                        <?php $srv=$this->Administration_model->services($value->service_item_id); echo $srv[0]->name; ?>
                    </td>
                    <td style="text-align: right">
                        <?php  echo number_format($value->amount,2); ?>
                    </td>
                 </tr>
             <?php } ?>
                 <tr>
                     <th>
                         Total
                     </th>
                     <th style="text-align: right">
                         <?php echo number_format($total,2); ?>
                     </th>
                 </tr>
         </table>
     </div>
     <div class="col-xs-12 col-md-offset-5 col-md-4">
         <?php echo anchor('Reception/download_postpaid_service_receipt/'.$bill_data[0]->postpaid_transactionid,'<span class="btn btn-success">Download Receipt</span>')?>
     </div>
</div>
    
</div>
