<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
     <div id="receipt" class="col-md-offset-3 col-lg-offset-3 col-sm-offset-3 col-xs-12 col-sm-8 col-md-5 col-lg-8 well">
         
         <table class="table table-hover table-condensed" id="receiptheader">
             <tr>
                 <th rowspan="4" style="text-align: center">
                     <img id="logoreceipt" alt="HRO" src="<?php echo base_url()."logo/".$institution->logoname ?>" />
                 </th>
             </tr>
             <tr>
                 <th>
                     Postal : <?php  echo $institution->postal?>
                 </th>
                 
             </tr>
             <tr>
                 <th>
                     City : <?php echo $institution->city;?>
                 </th>
             </tr>
             <tr>
                 <th>
                     Phone No : <?php echo $institution->phone;?>
                 </th>
             </tr>
         </table>
         <table class="table table-hover table-condensed">
             <tr>
                 <th>
                    Date :- <?php $datetime=explode(" ",$bill_data[0]->createdon); echo $datetime[0]?>
                 </th>
                 <th>
                     Time :- <?php echo $datetime[1]?>
                 </th>
             </tr>
             <tr>
                 <th>
                     Patient Id :- <?php  echo $bill_data[0]->patientid?>
                 </th>
                 <th>
                     Name :- <?php $name=$this->Reception_model->patient($bill_data[0]->patientid); echo $name->name;?>
                 </th>
             </tr>
         </table>
        
         <table class="table table-hover table-condensed">
             <tr>
                 <th colspan="2">
                     Subscription Id : <?php echo $bill_data[0]->subscriptionid; ?>
                 </th>
             </tr>
             <tr>
                 <th colspan="2">
                     Receipt No : <?php echo $bill_data[0]->transactionid; ?>
                 </th>
             </tr>
             <tr>
                 <th>
                     Service
                 </th>
                 <th style="text-align: right">
                     Amount
                 </th>
             </tr>
             <?php foreach($bill_data as $key=>$value){ 
                 $total +=$value->amount
                 ?>
                 <tr>
                    <td>
                        <?php $srv=$this->Administration_model->packages($value->packageid); echo $srv[0]->name; ?>
                    </td>
                    <td style="text-align: right">
                        <?php  echo number_format($value->amount,2); ?>
                    </td>
                 </tr>
             <?php } ?>
                 <tr>
                     <th>
                         Total
                     </th>
                     <th style="text-align: right">
                         <?php echo number_format($total,2); ?>
                     </th>
                 </tr>
         </table>
     </div>
     <div class="col-xs-12 col-md-offset-5 col-md-4">
         <?php echo anchor('Reception/download_package_subscription_receipt/'.$bill_data[0]->subscriptionid,'<span class="btn btn-success">Download Receipt</span>')?>
     </div>
</div>
    
</div>
