<script type="text/javascript">
    $(document).ready(function(){
         var region;
         var attribute;
         var patientType;
         
        $("#dob").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
        region=$('select[name=region]').val();
        
        $('select[name=district]').find('option').each(function(){
            attribute=$(this).attr('class');
           if(attribute == 'opt'+region){
               $(this).show();
           }else{
               $(this).hide();
           } 
        });
        
        $('select[name=region]').change(function(){
            region=$(this).val();
             $('select[name=district]').find('option').each(function(){
                attribute=$(this).attr('class');
               if(attribute == 'opt'+region){
                   $(this).show();
               }else{
                   $(this).hide();
               } 
            });
        });
        
        
       
        /*
        * addType radio button
       */
       $('input[name=patientType]').each(function(){
          patientType=$(this).val();
          
          if($(this).is(':checked')){
               if(patientType == 1){
                
                $('div.not_walkin').show('slow');
                
                }else{

                    $('div.not_walkin').hide('slow');
                 }
          }
       });
       
       
       $('input[name=patientType]').change(function(){
            
             patientType=$(this).val();
             if($(this).is(':checked')){
                 
                if(patientType == 1){
                
                $('div.not_walkin').show('slow');
                
                }else{

                    $('div.not_walkin').hide('slow');
                 }
             }
            
       });
    });
    
    function agetobday(){
        var age = document.getElementById('age');
        var date = new Date();
        var y = date.getFullYear()-age.value;
        var m= Math.floor((Math.random()*12)+1)
        if(m < 10 ){
        p = '0'+m;
        m=p;
        }

        var d= Math.floor((Math.random()*28)+1)
        if(d < 10 ){
        p = '0'+d;
        d=p;
        }
        var age2 = document.getElementById('dob');
        age2.value = y+'-'+m+'-'+d;
    }
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Reception/new_patient/'.$id); 
                    if($id == null){
                ?>
                <div class="form-group row">
                    <label for="patientType" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Patient Category &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <label class="checkbox-inline">
                          <input type="radio" id="patientType" name="patientType" value="1" <?php echo set_radio('patientType',"1"); ?> checked="checked"> Not Walk in
                      </label><?php echo form_error('patientType'); ?>
                       <label class="checkbox-inline">
                           <input type="radio" id="patientType" name="patientType" value="2" <?php echo set_radio('patientType',"2")?>> Walk in
                      </label>
                    </div>
                </div>
                    <?php }else{ ?>
                        <input type="hidden" id="patientType" name="patientType" value="<?php echo $patient[0]->patienttype; ?>" >
                        <input type="hidden" id="patientid" name="patientid" value="<?php echo $patient[0]->patientid; ?>" >
                        <div class="form-group row">
                            <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Patient Id</label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                <input type="text" readonly="true" class="form-control" value="<?php echo $patient[0]->patientid; ?>" >
                            </div>
                        </div>
                        
                    <?php } ?>
                <table class="table table-condensed table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="text-align: center">
                                Basic Details
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="form-group row">
                                    <label for="title" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Title &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <select name="title" id="title" class="form-control" >
                                            <option></option>
                                            <?php foreach($titles as $key=>$value){ ?>

                                            <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($patient[0]->titleid))?'selected="selected"':set_select('title',$value->id); ?>><?php echo $value->fullname; ?></option>

                                                <?php } ?>

                                        </select>
                                        <?php echo form_error('title'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Full Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Full Name" value="<?php echo $id != null?$patient[0]->name:set_value('name'); ?>" />
                                        <?php echo form_error('name'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="gender" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Gender &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <select name="gender" id="gender" class="form-control" >
                                            <option></option>
                                            <?php foreach($genders as $key=>$value){ ?>

                                            <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($patient[0]->genderid))?'selected="selected"':set_select('gender',$value->id); ?>><?php echo $value->name; ?></option>

                                                <?php } ?>

                                        </select>
                                        <?php echo form_error('gender'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="marital" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Marital Status</label>
                                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <select name="marital" id="marital" class="form-control" >
                                            <option></option>
                                            <?php foreach($maritals as $key=>$value){ ?>

                                            <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($patient[0]->maritalid))?'selected="selected"':set_select('marital',$value->id); ?>><?php echo $value->name; ?></option>

                                                <?php } ?>

                                        </select>
                                        <?php echo form_error('marital'); ?>
                                    </div>
                                </div>
                                <div class="form-group row not_walkin">
                                    <label for="tribe" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Tribe &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <input type="text" class="form-control" name="tribe" id="tribe" placeholder="Tribe" value="<?php echo $id != null?$patient[0]->tribe:set_value('tribe'); ?>" />
                                        <?php echo form_error('tribe'); ?>
                                    </div>
                                </div>
                                <div class="form-group row not_walkin">
                                    <label for="dob" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Date of Birth &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <input type="text" class="form-control" name="dob" id="dob" placeholder="Date of Birth" value="<?php echo $id != null?$patient[0]->dob:set_value('dob'); ?>" />
                                        <?php echo form_error('dob'); ?>
                                    </div>
                                    <label for="age" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Age &nbsp;&nbsp;</label>
                                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <input type="number" class="form-control" name="age" onchange="agetobday()" id="age" placeholder="Age" value="" />
                                        <?php echo form_error('age'); ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-condensed table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="text-align: center">
                                Residence Details
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="form-group row not_walkin">
                                    <label for="region" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Region &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <select name="region" id="region" class="form-control" >
                                            <option></option>
                                            <?php foreach($regions as $key=>$value){ ?>

                                            <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($patient[0]->region))?'selected="selected"':set_select('region',$value->id); ?>><?php echo $value->name; ?></option>

                                                <?php } ?>

                                        </select>
                                        <?php echo form_error('region'); ?>
                                    </div>
                                </div>
                                <div class="form-group row not_walkin">
                                    <label for="district" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">District &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <select name="district" id="district" class="form-control" >
                                            <option></option>
                                            <?php foreach($districts as $key=>$value){ ?>

                                            <option class="opt<?php echo $value->parent ; ?>" value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($patient[0]->district))?'selected="selected"':set_select('district',$value->id); ?>><?php echo $value->name; ?></option>

                                                <?php } ?>

                                        </select>
                                        <?php echo form_error('district'); ?>
                                    </div>
                                </div>
                                <div class="form-group row not_walkin">
                                    <label for="street" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Street </label>
                                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <input type="text" class="form-control" name="street" id="street" placeholder="Street" value="<?php echo $id != null?$patient[0]->street:set_value('street'); ?>" />
                                        <?php echo form_error('street'); ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-condensed table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="text-align: center">
                                Contact Details
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="form-group row">
                                    <label for="phone" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Phone no &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone no" value="<?php echo $id != null?$patient[0]->phone:set_value('phone'); ?>" />
                                        <?php echo form_error('phone'); ?>
                                    </div>
                                </div>
                                <div class="form-group row not_walkin">
                                    <label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Email Address</label>
                                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <input type="text" class="form-control" name="email" id="email" placeholder="Email Address" value="<?php echo $id != null?$patient[0]->email:set_value('email'); ?>" />
                                        <?php echo form_error('email'); ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-condensed table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="text-align: center">
                                Other Details
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="form-group row not_walkin">
                                    <label for="occupation" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Occupation</label>
                                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                        <input type="text" class="form-control" name="occupation" id="occupation" placeholder="Occupation" value="<?php echo $id != null?$patient[0]->occupation:set_value('occupation'); ?>" />
                                        <?php echo form_error('occupation'); ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-md-offset-5 col-md-3 col-sm-10">
                        <button type="submit" class="btn btn-success">Register Patient</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
