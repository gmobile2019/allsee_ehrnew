<script type="text/javascript">
    $(document).ready(function(){
       
       $('div.display_content').find('table').find('tbody').find('tr').find('td.patientdetails').find('a').click(function(){
                
                var patientid=$(this).attr('id');
               
                $.ajax({
                type:'POST',
                url:'<?php echo site_url('Reception/modal_patient_details'); ?>',
                data:{patientid:patientid},
                success:function(data){

                    var patient=data.split("=_");
                    
                     $('div#myModal').find('table').find('td#patientid').text(patient[0]);
                     $('div#myModal').find('table').find('td#name').text(patient[1]);
                     $('div#myModal').find('table').find('td#gender').text(patient[2]);
                     $('div#myModal').find('table').find('td#marital').text(patient[3]);
                     $('div#myModal').find('table').find('td#tribe').text(patient[4]);
                     $('div#myModal').find('table').find('td#dob').text(patient[5]);
                     $('div#myModal').find('table').find('td#email').text(patient[6]);
                     $('div#myModal').find('table').find('td#phone').text(patient[7]);
                     $('div#myModal').find('table').find('td#region').text(patient[8]);
                     $('div#myModal').find('table').find('td#district').text(patient[9]);
                     $('div#myModal').find('table').find('td#street').text(patient[10]);
                     $('div#myModal').find('table').find('td#occupation').text(patient[11]);
                     
                     $('#myModal').modal({
                        keyboard:true,
                       show:true,
                       })
                 }

                });
 
            });
            
             $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
        });
</script>
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Reception/all_patients',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Full Name" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="gender"></label>
                        <select name="gender" id="gender" class="form-control" >
                                <option value="" >All Genders</option>
                                <?php foreach($genders as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($gender == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="marital"></label>
                        <select name="marital" id="marital" class="form-control" >
                                <option value="" >All Marital Status</option>
                                <?php foreach($maritals as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($marital == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="phone"></label>
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo $phone; ?>" />
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
    <?php echo anchor("Reception/all_patients/name_".$name."_gender_".$gender."_marital_".$marital."_phone_".$phone."_docType_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
    &nbsp;&nbsp;&nbsp;
    <?php echo anchor("Reception/all_patients/name_".$name."_gender_".$gender."_marital_".$marital."_phone_".$phone."_docType_2/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>

</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Patient Id</th>
                <th style="text-align:center;">Full Name</th>
                <th style="text-align:center;">Gender</th>
                <th style="text-align:center;">Marital Status</th>
                <th style="text-align:center;">Date of Birth</th>
                <th style="text-align:center;">Phone</th>
                <th style="text-align:center;">Patient Category</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($patients != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($patients as $key=>$value){
                    $check_visit_validity=$this->Reception_model->check_visit_validty($value->patientid);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td class="patientdetails">&nbsp;&nbsp;<a type="button" href="#" data-toggle="modal" id="<?php echo $value->patientid ?>"><?php echo $value->patientid ?></a></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->gender; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->marital; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->dob; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->phone; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->patienttype == 1?"Permanent":"Walk In"; ?></td>
                        <td>&nbsp;&nbsp;
                            <?php
                            echo $check_visit_validity?anchor('Reception/register_patient_visit/'.$value->id,'<span class="glyphicon glyphicon-arrow-right" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Process Visit"></span>'):""; ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('Reception/new_patient/'.$value->id,'<span class="glyphicon glyphicon-edit" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                        </td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="9" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>     
</div>
   <!-- Modal -->
       <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="gridSystemModalLabel">Patient Details</h4>
            </div>
            <div class="modal-body">
              <div class="row container">
                  <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                      <table class="table table-condensed table-hover">
                          <tr>
                              <td>Patient Id</td>
                              <td id="patientid"></td>
                          </tr>
                          <tr>
                              <td>Name</td>
                              <td id="name"></td>
                          </tr>
                          <tr>
                              <td>Gender</td>
                              <td id="gender"></td>
                          </tr>
                          <tr>
                              <td>Marital Status</td>
                              <td id="marital"></td>
                          </tr>
                          <tr>
                              <td>Tribe</td>
                              <td id="tribe"></td>
                          </tr>
                          <tr>
                              <td>Date of Birth</td>
                              <td id="dob"></td>
                          </tr>
                          <tr>
                              <td>Email</td>
                              <td id="email"></td>
                          </tr>
                          <tr>
                              <td>Phone</td>
                              <td id="phone"></td>
                          </tr>
                          <tr>
                              <td>Region</td>
                              <td id="region"></td>
                          </tr>
                          <tr>
                              <td>District</td>
                              <td id="district"></td>
                          </tr>
                          <tr>
                              <td>Street</td>
                              <td id="street"></td>
                          </tr>
                          <tr>
                              <td>Occupation</td>
                              <td id="occupation"></td>
                          </tr>
                      </table>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div> 
</div>
