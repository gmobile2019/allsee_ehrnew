<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
     <div id="receipt" class="col-md-offset-3 col-lg-offset-3 col-sm-offset-3 col-xs-12 col-sm-8 col-md-5 col-lg-8 well">
         
         <table class="table table-hover table-condensed" id="receiptheader">
             <tr>
                 <th rowspan="4" style="text-align: center">
                     <img id="logoreceipt" alt="HRO" src="<?php echo base_url()."logo/".$institution->logoname ?>" />
                 </th>
             </tr>
             <tr>
                 <th>
                     Postal : <?php  echo $institution->postal?>
                 </th>
                 
             </tr>
             <tr>
                 <th>
                     City : <?php echo $institution->city;?>
                 </th>
             </tr>
             <tr>
                 <th>
                     Phone No : <?php echo $institution->phone;?>
                 </th>
             </tr>
         </table>
         <table class="table table-hover table-condensed">
             <tr>
                 <th>
                     Patient Id 
                 </th>
                 <th>
                    <?php  echo $data[0]->patientid; ?>
                 </th>
             </tr>
             <tr>
                 <th>
                     Name
                 </th>
                 <th>
                     <?php $name=$this->Reception_model->patient($data[0]->patientid); echo $name->name;?>
                 </th>
             </tr>
             <tr>
                 <th>
                     Date of Birth
                 </th>
                 <th>
                     <?php echo $name->dob;?>
                 </th>
             </tr>
            <tr>
                 <th>
                     Gender
                 </th>
                 <th>
                     <?php $gender=$this->Reception_model->genders($name->genderid);echo $gender[0]->name;?>
                 </th>
             </tr>
         </table>
        
         <table class="table table-hover table-condensed">
             
             <tr>
                 <th>Department</th>
                 <th>Service</th>
                 <th>Sponsor</th>
                 <th>Payment Mode</th>
                 <th>Timestamp</th>
                 <th>Amount</th>
                 <th>Overdue</th>
             </tr>
             <?php foreach($data as $key=>$value){ 
                    $srv=$this->Inventory_model->items($value->service_item_id);
                    
                    $department=$this->Administration_model->departments($value->departmentid);
                    $subdepartment=$this->Administration_model->subdepartments($value->subdepartmentid);
                    
                    $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));

                    if($dept[0]->id <> $value->departmentid){
                        $srv=$this->Administration_model->services($value->service_item_id);
                    }
                    
                 if($value->paymentstatus == 2){
                     $total_overdue +=$value->amount;
                     $overdue=$value->amount;
                     $amount=null;
                }else{
                     $total_amount +=$value->amount;
                     $amount=$value->amount;
                     $overdue=null;
                }
                 
                 ?>
                 <tr>
                    <td>&nbsp;&nbsp;<?php echo $department[0]->name; ?></td>
                    <td>&nbsp;&nbsp;<?php echo $srv[0]->name; ?></td>
                    <td>&nbsp;&nbsp;<?php echo $value->shortname; ?></td>
                    <td>&nbsp;&nbsp;<?php echo $value->pmode; ?></td>
                    <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                    <td style="text-align: right"> <?php  echo number_format($amount,2); ?></td>
                    <td style="text-align: right"> <?php  echo number_format($overdue,2); ?></td>
                 </tr>
             <?php } ?>
                 <tr>
                     <th colspan="5">
                         Total
                     </th>
                     <th style="text-align: right">
                         <?php echo number_format($total_amount,2); ?>
                     </th>
                     <th style="text-align: right">
                         <?php echo number_format($total_overdue,2); ?>
                     </th>
                 </tr>
         </table>
     </div>
</div>
    
</div>
