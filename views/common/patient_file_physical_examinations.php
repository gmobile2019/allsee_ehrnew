<script type="text/javascript">
    $(document).ready(function(){
        
         
    });
</script>
<div style="padding-top:10px" class="row">
 <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;" colspan="8">Physical Examinations&nbsp;&nbsp;
                    <?php echo $identifier == 'ID01'?anchor('Nursing/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'):
                    anchor('Clinical/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'); ?></th>
            </tr>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Cardiovascular/S</th>
                <th style="text-align:center;">Respiratory/S</th>
                <th style="text-align:center;">Abdomen</th>
                <th style="text-align:center;">Central Nervous/S</th>
                <th style="text-align:center;">Muscular Skeletal/S</th>
                <th style="text-align:center;">Examiner</th>
                <th style="text-align:center;">Timestamp</th>
             </tr>
        </thead>
        <tbody>
            <?php if($filedata != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($filedata as $key=>$value){
                    $examiner=$this->SuperAdministration_model->get_member_info($value->createdby);
                    $examiner=$examiner[0]->first_name.' '.$examiner[0]->last_name;
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->cardiovascularsystem; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->respiratorysystem; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->abdomen; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->centralnervoussystem; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->muscularskeletalsystem; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $examiner; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                    </tr>  
                <?php } ?>
                     
                <?php }else{?>
                    <tr>
                        <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
                    </tr>  
                <?php }?>
        </tbody>
    </table>
</div>
