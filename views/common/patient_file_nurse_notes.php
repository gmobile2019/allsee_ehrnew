<script type="text/javascript">
    $(document).ready(function(){
            var noteid;
            var divid;
            var id;
            
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
        
        $('div.notes').find('span#show').hide();
        $('div.notes').find('span#hide').show();
        
        $('div.notes').each(function(){
            
            
            $(this).find('span#show').find('a').click(function(){
                divid=$(this).parent().parent().parent().attr('id');
                id=divid.split("-");
                noteid=id[1];
                $('table#table-'+noteid).show('slow');
                $(this).parent().parent().find('span#show').hide();
                $(this).parent().parent().find('span#hide').show();
            });
            
            $(this).find('span#hide').find('a').click(function(){
                divid=$(this).parent().parent().parent().attr('id');
                id=divid.split("-");
                noteid=id[1];
                $('table#table-'+noteid).hide('slow');
                $(this).parent().parent().find('span#show').show();
                $(this).parent().parent().find('span#hide').hide();
            });
        });
    });
</script>
<div style="padding-top:10px" class="display_content" >
            
            <div style="text-align:center">
                    Nurse Notes&nbsp;&nbsp;
                <?php echo $identifier == 'ID01'?anchor('Nursing/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'):
                anchor('Clinical/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'); ?></th>
            </div>
            <?php if($filedata != null){
                $rounds=count($filedata); 
                foreach($filedata as $key=>$value){
                    
                    $nurse=$this->SuperAdministration_model->get_member_info($value->createdby);
                    $nurse=$nurse[0]->first_name.' '.$nurse[0]->last_name;
                    
                    ?>
            <div class="well notes" id="div-<?php echo $value->id; ?>">
                <div style="text-align:right">
                   <span style="font-style: italic;font-weight: bold;text-transform: capitalize"><a href="#">Round <?php echo $rounds-- ;?></a></span>&nbsp;&nbsp;<span id="show" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="show"><a href="#" class="glyphicon glyphicon-menu-down"></a></span>&nbsp;&nbsp;<span id="hide" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="hide"><a href="#" class="glyphicon glyphicon-menu-up"></a></span> 
                </div>
            <table class="table table-condensed table-hover table-striped table-bordered" id="table-<?php echo $value->id ?>">

                <tbody>
                    <tr>
                        <td>&nbsp;&nbsp;Current Diagnosis</td>
                        <td>&nbsp;&nbsp;<?php echo $value->currentdiagnosis; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Treatment</td>
                        <td>&nbsp;&nbsp;<?php echo $value->treatment; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Procedures</td>
                        <td>&nbsp;&nbsp;<?php echo $value->procedures; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Medication</td>
                        <td>&nbsp;&nbsp;<?php echo $value->medications; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Medication Effect</td>
                        <td>&nbsp;&nbsp;<?php echo $value->medicationeffect; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Medication Comments</td>
                        <td>&nbsp;&nbsp;<?php echo $value->medicationcomments; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Intakes</td>
                        <td>&nbsp;&nbsp;<?php echo $value->intakes; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Outputs</td>
                        <td>&nbsp;&nbsp;<?php echo $value->outputs; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Overall Progress</td>
                        <td>&nbsp;&nbsp;<?php echo $value->overallprogress; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Remarks</td>
                        <td>&nbsp;&nbsp;<?php echo $value->remarks; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Timestamp</td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Nurse</td>
                        <td>&nbsp;&nbsp;<?php echo $nurse; ?></td>
                    </tr>  
            </tbody>
        </table>
        </div>
            
        <?php } ?>
                     
    <?php } ?>
</div>
