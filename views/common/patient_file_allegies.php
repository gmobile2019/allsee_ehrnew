<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>
<div style="padding-top:10px" class="row">
 <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;" colspan="5">Allergies&nbsp;&nbsp;
                    <?php echo $identifier == 'ID01'?anchor('Nursing/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'):
                    anchor('Clinical/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'); ?></th>
            </tr>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Allergy</th>
                <th style="text-align:center;">Allergy Status</th>
                <th style="text-align:center;">Remarks</th>
                <th style="text-align:center;">Action Date</th>
            </tr>
        </thead>
        <tbody>
            <?php if($filedata != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($filedata as $key=>$value){ ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->allegy; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->allegystatus; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->remarks; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                    </tr>  
                <?php } ?>
                     
                <?php }else{ ?>
            <tr>
                <td colspan="5" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
</div>
