<script type="text/javascript">
    $(document).ready(function(){
             
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
        
    });
</script>
<?php
 if($filedata <> null){ ?>      

<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
        <?php foreach($filedata as $key=>$value){ 
            $invRes=$this->Investigation_model->patient_investigation_tests($value->patientid,$value->patientvisitid,2,null);
    
            if($invRes <> null){ ?>
                <div  id="div_<?php echo $value->patientvisitid?>" class="well filedata">
                    <table class="table table-condensed table-hover table-striped table-bordered" id="table_<?php echo $value->patientvisitid ?>">

                    <thead>
                    <tr>
                        <th style="text-align:center;">S/NO</th>
                        <th style="text-align:center;">Service Name</th>
                        <th style="text-align:center;">Test</th>
                        <th style="text-align:center;">Result</th>
                        <th style="text-align:center;">Examiner</th>
                        <th style="text-align:center;">Approver</th>
                     </tr>
                </thead>
                <tbody>
                <?php if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($invRes as $ky=>$val){
                       ?>
                        <tr>
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td >&nbsp;&nbsp;<?php echo $val->name ?></td>
                            <td >&nbsp;&nbsp;<?php echo $val->test ?></td>
                            <td>&nbsp;&nbsp;<?php echo $val->result; ?></td>
                            <td>&nbsp;&nbsp;<?php $examiner = $this->ion_auth->user($val->createdby)->row();echo $examiner->first_name.' '.$examiner->last_name; ?></td>
                            <td>&nbsp;&nbsp;<?php $approver = $this->ion_auth->user($val->approvedby)->row();echo $approver->first_name.' '.$approver->last_name; ?></td>
                        </tr>  
                    <?php } ?>
                </tbody>
                </table>
            </div>
            <?php }
            } ?>
    </div>
      
</div>
<?php } ?>