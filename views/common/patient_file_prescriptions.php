<script type="text/javascript">
    $(document).ready(function(){
        
           
    });
</script>

<div style="padding-top:10px" class="row">
 <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;" colspan="8">Prescriptions&nbsp;&nbsp;
                    <?php echo $identifier == 'ID01'?anchor('Nursing/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'):
                    anchor('Clinical/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'); ?></th>
            </tr>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Item</th>
                <th style="text-align:center;">Frequency</th>
                <th style="text-align:center;">Dosage</th>
                <th style="text-align:center;">No Days</th>
                <th style="text-align:center;">Quantity</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action Date</th>
             </tr>
        </thead>
        <tbody>
            <?php if($filedata != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($filedata as $key=>$value){
                    
                    $itm=$this->Inventory_model->items($value->itemid);
                    $freq=$this->Pharmacy_model->drug_frequencies($value->frequencyid);
                    $dosage=$this->Pharmacy_model->drug_dosages($value->dosageid);
                    
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $itm[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $freq[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $dosage[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->days; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->quantity; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status ==1?'Confirmed':'Pending'?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                    </tr>  
                <?php } ?>
                     
                <?php }else{ ?>
            <tr>
                <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
</div>
