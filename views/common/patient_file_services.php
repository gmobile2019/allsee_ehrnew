<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
             <tr>
                <th style="text-align:center;" colspan="7">Services&nbsp;&nbsp;
                    <?php echo $identifier == 'ID01'?anchor('Nursing/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'):
                    anchor('Clinical/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'); ?></th>
            </tr>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Visit Id</th>
                <th style="text-align:center;">Department</th>
                <th style="text-align:center;">Sub Department</th>
                <th style="text-align:center;">Service</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action Date</th>
             </tr>
        </thead>
        <tbody>
            <?php if($filedata != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($filedata as $key=>$value){
                    
                    $dp=$this->Administration_model->departments($value->departmentid);
                    $sub_dp=$this->Administration_model->subdepartments($value->subdepartmentid);
                    $srv=$this->Administration_model->services($value->serviceid);
                    
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->patientvisitid; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $dp[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $sub_dp[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $srv[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php  ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                       
                    </tr>  
                <?php } ?>
                     
                <?php }else{ ?>
            <tr>
                <td colspan="7" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
     <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
