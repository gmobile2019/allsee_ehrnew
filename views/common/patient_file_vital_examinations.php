<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>
<div style="padding-top:10px" class="row">
 <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;" colspan="10">Vitals Examination&nbsp;&nbsp;
                    <?php echo $identifier == 'ID01'?anchor('Nursing/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'):
                    anchor('Clinical/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'); ?>
                </th>
             </tr>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Weight(m)</th>
                <th style="text-align:center;">Height(Kg)</th>
                <th style="text-align:center;">Temperature (C)</th>
                <th style="text-align:center;">Pulse Rate (b/m)</th>
                <th style="text-align:center;">Blood Pressure (mmHg)</th>
                <th style="text-align:center;">Respiration Rate (c/m)</th>
                <th style="text-align:center;">SP02 (%)</th>
                <th style="text-align:center;">Examiner</th>
                <th style="text-align:center;">Timestamp</th>
             </tr>
        </thead>
        <tbody>
            <?php if($filedata != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($filedata as $key=>$value){
                    $examiner=$this->SuperAdministration_model->get_member_info($value->createdby);
                    $examiner=$examiner[0]->first_name.' '.$examiner[0]->last_name;
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->weight; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->height; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->temperature; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->pulserate; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->bloodpressure; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->respirationrate; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->spo2; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $examiner; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                    </tr>  
                <?php } ?>
                     
                <?php }else{ ?>
            <tr>
                <td colspan="10" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
</div>
