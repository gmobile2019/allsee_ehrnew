<script type="text/javascript">
    $(document).ready(function(){
            var noteid;
            var divid;
            var id;
            
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
        
        $('div.notes').each(function(){
            
            
            $(this).find('span#show').find('a').click(function(){
                divid=$(this).parent().parent().parent().attr('id');
                id=divid.split("-");
                noteid=id[1];
                $('table#table-'+noteid).show('slow');
            });
            
            $(this).find('span#hide').find('a').click(function(){
                divid=$(this).parent().parent().parent().attr('id');
                id=divid.split("-");
                noteid=id[1];
                $('table#table-'+noteid).hide('slow');
            });
        });
    });
</script>
<div style="padding-top:10px" class="display_content" >
            
            <div style="text-align:center">
                    Doctor Notes&nbsp;&nbsp;
                <?php echo $identifier == 'ID01'?anchor('Nursing/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'):
                anchor('Clinical/patient_medical_file/'.$patientid,'<span id="back" class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Back">back</span>'); ?></th>
            </div>
  
            <?php if($filedata != null){
                $rounds=count($filedata);
                foreach($filedata as $key=>$value){
                    
                    $doc=$this->SuperAdministration_model->get_member_info($value->createdby);
                    $doc=$doc[0]->first_name.' '.$doc[0]->last_name;
                    
                    ?>
        <div class="well notes" id="div-<?php echo $value->id; ?>">
            <div style="text-align:right">
               <span style="font-style: italic;font-weight: bold;text-transform: capitalize"><a href="#">Round <?php echo $rounds-- ;?></a></span>&nbsp;&nbsp;<span id="show"><a href="#">show</a></span>&nbsp;&nbsp;<span id="hide"><a href="#">hide</a></span> 
            </div>
            <table class="table table-condensed table-hover table-striped table-bordered" id="table-<?php echo $value->id ?>">

                <tbody>
                    <tr>
                        <td>&nbsp;&nbsp;Overall Progress</td>
                        <td>&nbsp;&nbsp;<?php echo $value->overallprogress; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Complains</td>
                        <td>&nbsp;&nbsp;<?php echo $value->complains; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Recommendations</td>
                        <td>&nbsp;&nbsp;<?php echo $value->recommendations; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Timestamp</td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Doctor</td>
                        <td>&nbsp;&nbsp;<?php echo $doc; ?></td>
                    </tr>  
                
            </tbody>
        </table>
        </div>
            
        <?php } ?>
                     
    <?php } ?>
</div>
