<script type="text/javascript">
    $(document).ready(function(){
       $("#nextconsultation").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            var patientid;
       
       
            patientid=$('input[name=patientid').val();

            if(patientid != null && patientid != ''){

                 $.ajax({
                 type:'POST',
                 url:'<?php echo site_url('Reception/modal_patient_details'); ?>',
                 data:{patientid:patientid},
                 success:function(data){

                     var patient=data.split("=_");

                      $('input#pname').val(patient[1]);
                 }

                 });
            }
       
            $('input[name=patientid').change(function(){
                
                patientid=$(this).val();
               
               if(patientid != null && patientid != ''){
                   $.ajax({
                    type:'POST',
                    url:'<?php echo site_url('reception/modal_patient_details'); ?>',
                    data:{patientid:patientid},
                    success:function(data){
                       
                        var patient=data.split("=_");

                        $('input#pname').val(patient[1]);
                    }

                    });
               }
                
 
        });
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Clinical/new_family_planning_session/'); 
                ?>
                <div class="form-group row">
                    <label for="patientid" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Patient ID</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="patientid" id="patientid" placeholder="Patient's ID" value="<?php echo set_value('patientid'); ?>"/>
                        <?php echo form_error('patientid'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pname" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Patient's Name</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" readonly="true" class="form-control" name="pname" id="pname" value=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="partner" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Partner's Name</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="partner" id="partner" placeholder="Partner's Name" value="<?php echo set_value('partner'); ?>"/>
                        <?php echo form_error('partner'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="childrencount" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">How many current Children?</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="childrencount" id="childrencount" placeholder="Children Count" value="<?php echo set_value('childrencount'); ?>"/>
                        <?php echo form_error('childrencount'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="agelastchild" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Age of the last Child?</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="agelastchild" id="agelastchild" placeholder="Last Child Age" value="<?php echo set_value('agelastchild'); ?>"/>
                        <?php echo form_error('agelastchild'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="prevpregnancies" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">How many previous pregnancies?</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="prevpregnancies" id="prevpregnancies" placeholder="Previous Pregnancies Count" value="<?php echo set_value('prevpregnancies'); ?>"/>
                        <?php echo form_error('prevpregnancies'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="birthcount" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">How many births?</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="birthcount" id="birthcount" placeholder="Birth count" value="<?php echo set_value('birthcount'); ?>"/>
                        <?php echo form_error('birthcount'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="stillbirthcount" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">How many still birth pregnancies?</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="stillbirthcount" id="stillbirthcount" placeholder="Still Births Count" value="<?php echo set_value('stillbirthcount'); ?>"/>
                        <?php echo form_error('stillbirthcount'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="abortions" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">How many aborted pregnancies?</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="abortions" id="abortions" placeholder="Abortions Count" value="<?php echo set_value('abortions'); ?>"/>
                        <?php echo form_error('abortions'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="daychildrendeathcount" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">How many children have died within seven days of birth?</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="daychildrendeathcount" id="daychildrendeathcount" placeholder="" value="<?php echo set_value('daychildrendeathcount'); ?>"/>
                        <?php echo form_error('daychildrendeathcount'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="contraceptionmethod" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Method(s) of contraception chosen?</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <?php foreach($contraceptions as $key=>$value){ ?>
                        
                          <label>
                              <input type="checkbox" name="contraceptionmethod[]" value="<?php echo $value->name; ?>"> <?php echo ucfirst(strtolower($value->name)); ?>
                          </label>
                        
                        <?php }?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="remarks" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Remarks</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="remarks" id="remarks" ><?php echo set_value('remarks'); ?></textarea>
                        <?php echo form_error('remarks'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nextconsultation" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Recommended date for review consultation</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="nextconsultation" id="nextconsultation" placeholder="yyyy-mm-dd" value="<?php echo set_value('nextconsultation'); ?>"/>
                        <?php echo form_error('nextconsultation'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
