<script type="text/javascript">
    $(document).ready(function(){
      var item_category;
        
         /*
        * addType radio button
       */
       $('input[name=item_category]').each(function(){
          item_category=parseInt($(this).val());
          
          if($(this).is(':checked')){
              
               if(item_category == 601){
                
                $('td.drug').show();
                $('td.other').hide();
                
                }else{

                    $('td.drug').hide();
                    $('td.other').show();
                }
                 
                $('select[name=item]').find('option').each(function(){
                  var cls=$(this).attr('class'); 

                  if(cls == "cat_"+item_category){
                      $(this).show();
                  }else{
                      $(this).hide();
                  }
               });
          }
       });
       
       
       $('input[name=item_category]').change(function(){
            
           item_category=parseInt($(this).val());
          
          if($(this).is(':checked')){
               if(item_category == 601){
                
                $('td.drug').show();
                $('td.other').hide();
                
                }else{

                    $('td.drug').hide();
                    $('td.other').show();
                }
                
                $('select[name=item]').find('option').each(function(){
                   var cls=$(this).attr('class'); 
                  
                   if(cls == "cat_"+item_category){
                       $(this).show();
                   }else{
                       $(this).hide();
                   }
                });
          }
       });
    });
</script>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
     <?php echo $disp_msg; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Clinical/create_dispense_order/'.$patientid.'/'.$patientvisitid.'/'.$inpatientCare); 
                ?>
    <table class="table table-condensed table-striped">
        <tbody>
       
            <tr>
                <td>
                    <ul>
                    <?php foreach($item_categories as $key=>$value){ ?>
                        <li>
                            <?php echo $value->name; ?>&nbsp;<input type="radio" value="<?php echo $value->code ?>" name="item_category" checked="checked" <?php echo set_radio('item_category', $value->code)?>/>
                        </li>
                    <?php } ?>
                   </ul>
                    <?php echo form_error('item_category'); ?>
                    
                </td>
                <td>
                     <select name="item" id="item" class="form-control" >
                            <option value="">Item</option>
                            <?php foreach($items as $key=>$value){ 
                                $stock=$this->Inventory_model->units_inventory_item_stock($value->id);
                                ?>

                            <option value="<?php echo $value->id; ?>" <?php echo set_select('item',$value->id); ?> class="cat_<?php echo $value->category; ?>"><?php echo $value->name.' ('.$stock->quantity.')'; ?></option>

                                <?php } ?>
                        
                    </select>
                    <?php echo form_error('item'); ?>
                </td>
                <td class="drug">
                    <select name="dosage" id="dosage" class="form-control" >
                            <option value="">Dosage</option>
                            <?php foreach($drug_dosages as $key=>$value){ ?>

                            <option value="<?php echo $value->id; ?>" <?php echo set_select('dosage',$value->id); ?>><?php echo $value->name; ?></option>

                                <?php } ?>

                    </select>
                    <?php echo form_error('dosage'); ?>
                </td>
                <td class="drug">
                     <select name="frequency" id="frequency" class="form-control" >
                            <option value="">Frequency</option>
                            <?php foreach($drug_frequencies as $key=>$value){ ?>

                            <option value="<?php echo $value->id; ?>" <?php echo set_select('frequency',$value->id); ?>><?php echo $value->name; ?></option>

                                <?php } ?>
                    </select>
                    <?php echo form_error('frequency'); ?>
                </td>
                <td class="drug">
                     <input type="number" class="form-control" name="no_days" id="no_days" placeholder="No Days" value="<?php echo set_value('no_days'); ?>"/>
                    <?php echo form_error('no_days'); ?>
                </td>
                <td class="other">
                     <input type="number" class="form-control" name="quantity" id="quantity" placeholder="Quantity" value="<?php echo set_value('quantity'); ?>"/>
                    <?php echo form_error('quantity'); ?>
                </td>
            </tr>
            <tr>
                 <td colspan="6">
                     <input type="submit" class="btn btn-block btn-success" name="add_dispense_order" id="add_dispense_order" value="Post Order"/>
                </td>
            </tr>
        </tbody>
    </table>
   <?php echo form_close(); ?>
     
</div>
    
</div>
