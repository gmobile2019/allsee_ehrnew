<script type="text/javascript">
    $(document).ready(function(){
       
        $("#appointment_date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Clinical/add_appointment/'.$patientid.'/'.$patientvisitid); 
                ?>
                <div class="form-group row">
                    <label for="appointment_date" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Appointment Date</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="appointment_date" id="appointment_date" placeholder="Appointment Date" value="<?php echo set_value('appointment_date'); ?>"/>
                        <?php echo form_error('appointment_date'); ?>
                    </div>
                </div>
               <div class="form-group row">
                    <label for="appointment_time" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Appointment Time</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="appointment_time" id="appointment_time" placeholder="Appointment Time" value="<?php echo set_value('appointment_time'); ?>"/>
                        <?php echo form_error('appointment_time'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
