<script type="text/javascript">
    $(document).ready(function(){
            var fileid;
            var divid;
            var id;
           
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
        
        $('div.filedata').find('span#show').show();
        $('div.filedata').find('span#hide').hide();
        
        $('div.filedata').each(function(){
            
            $(this).find('table').hide();
            
            $(this).find('span#show').find('a').click(function(){
                divid=$(this).parent().parent().parent().attr('id');
                id=divid.split("_");
                fileid=id[1];
                $('table#table_'+fileid).show('slow');
                $(this).parent().parent().find('span#show').hide();
                $(this).parent().parent().find('span#hide').show();
            });
            
            $(this).find('span#hide').find('a').click(function(){
                divid=$(this).parent().parent().parent().attr('id');
                id=divid.split("_");
                fileid=id[1];
                $('table#table_'+fileid).hide('slow');
                $(this).parent().parent().find('span#show').show();
                $(this).parent().parent().find('span#hide').hide();
            });
        });
    });
</script>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Clinical/family_planning_session_details/$patientid/$patientvisitid",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">export</span>'); ?>
    </div>
</div>
<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
        <?php foreach($fpsessions as $key=>$value){ ?>
            <div  id="div_<?php echo $value->patientvisitid?>" class="well filedata">
                <div style="text-align:right">
                   <span style="font-style: italic;font-weight: bold;text-transform: capitalize"><a href="#">Session Timestamp : <?php echo $value->createdon; ?></a></span>
                    &nbsp;&nbsp;<span id="show" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="show"><a href="#" class="glyphicon glyphicon-menu-down"></a></span>
                    &nbsp;&nbsp;<span id="hide" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="hide"><a href="#" class="glyphicon glyphicon-menu-up"></a></span> 
                
                </div>
               <table class="table table-condensed table-hover table-striped table-bordered" id="table_<?php echo $value->patientvisitid ?>">

                <tbody>
                    <tr>
                        <td>&nbsp;&nbsp;Partner's Name</td>
                        <td>&nbsp;&nbsp;<?php echo $value->partner?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Current number of Children</td>
                        <td>&nbsp;&nbsp<?php echo $value->childrencount?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Age of the last Child</td>
                        <td>&nbsp;&nbsp;<?php echo $value->agelastchild?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Previous number of pregnancies</td>
                        <td>&nbsp;&nbsp;<?php echo $value->prevpregnancies?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Number of births</td>
                        <td>&nbsp;&nbsp;<?php echo $value->birthcount?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Still births count</td>
                        <td>&nbsp;&nbsp;<?php echo $value->stillbirthcount?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Abortions Count</td>
                        <td>&nbsp;&nbsp;<?php echo $value->abortions?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Seven days after birth Children death count</td>
                        <td>&nbsp;&nbsp;<?php echo $value->sevendaychildrendeathcount?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Contraception Methods</td>
                        <td>&nbsp;&nbsp;<?php $methods=explode("||",$value->contraceptionmethod);
                                if(count($methods) > 0){ ?>
                                <ul>
                                    <?php
                                    foreach($methods as $val){ ?>
                                    
                                    <li><?php echo ucfirst(strtoupper($val)); ?></li>
                                    
                                    <?php } ?> 
                                </ul> 
                               <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Remarks</td>
                        <td>&nbsp;&nbsp;<?php echo $value->remarks?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Next Consultation</td>
                        <td>&nbsp;&nbsp;<?php echo $value->nextconsultation?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Doctor</td>
                        <td>&nbsp;&nbsp;<?php echo $doctor = $this->ion_auth->user($value->createdby)->row();echo $doctor->first_name.' '.$doctor->last_name?></td>
                    </tr>
            </tbody>
        </table>
            </div>
        <?php } ?>
    </div>
    
</div>
