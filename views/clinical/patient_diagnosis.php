<script type="text/javascript">
    $(document).ready(function(){
       
    });
    
</script>

<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Clinical/patient_diagnosis/'.$patientid.'/'.$patientvisitid); 
                    if($patient_diagnosis == null){
                ?>
                <div class="form-group row ">
                    <label for="complains" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Complains</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="complains" id="complains" ><?php echo set_value('complains'); ?></textarea>
                        <?php echo form_error('complains'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="provisional_diagnosis" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Provisional Diagnosis</label>
                     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="provisional_diagnosis[]" id="provisional_diagnosis" class="form-control" multiple="multiple" size="20px">
                        <?php foreach($complaints as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->code; ?>" <?php echo set_select('provisional_diagnosis',$value->code); ?>><?php echo $value->code.':'.$value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('provisional_diagnosis'); ?>
                    </div>
                </div>
                <?php } 
                
                if($patient_diagnosis <> null){ ?>
                <div class="form-group row">
                    <label for="final_diagnosis" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Final Diagnosis</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="final_diagnosis[]" id="final_diagnosis" class="form-control" multiple="multiple" size="20px">
                        <?php foreach($complaints as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->code; ?>" <?php echo set_select('final_diagnosis',$value->code); ?>><?php echo $value->code.':'.$value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('final_diagnosis'); ?>
                    </div>
                </div>
                <div class="form-group row final">
                    <label for="final_comments" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Final Comments</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="final_comments" id="final_comments" ><?php echo set_value('final_comments'); ?></textarea>
                        <?php echo form_error('final_comments'); ?>
                    </div>
                </div>
                
                <?php } 
                if($patient_diagnosis[0]->finaldiagnosis == null){
                ?>
    
                
                <input type="hidden" name="diagnosis" value="<?php echo $patient_diagnosis == null?TRUE:FALSE; ?>">
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" class="btn btn-success btn-block" name="save" value="Save"/>
                    </div>
                </div>
                
                <?php } echo form_close(); ?>        
</div>
