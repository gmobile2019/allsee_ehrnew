<script type="text/javascript">
    $(document).ready(function(){
       
    });
    
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Clinical/write_doctor_notes/'.$patientid.'/'.$patientvisitid); 
                ?>
                <div class="form-group row">
                    <label for="admissionid" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Admission Id</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" id="admissionid" readonly="true" value="<?php echo $admissionid; ?>"/>
                        <input type="hidden" class="form-control" name="admissionid" id="admissionid" value="<?php echo set_value('admissionid',$admissionid); ?>"/>
                        <?php echo form_error('admissionid'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="overallprogress" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Overall Progress&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="overallprogress" id="overallprogress" ><?php echo set_value('overallprogress'); ?></textarea>
                        <?php echo form_error('overallprogress'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="complains" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Complains&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="complains" id="complains" ><?php echo set_value('complains'); ?></textarea>
                        <?php echo form_error('complains'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="recommendations" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Recommendations&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control" name="recommendations" id="recommendations" ><?php echo set_value('recommendations'); ?></textarea>
                        <?php echo form_error('recommendations'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
