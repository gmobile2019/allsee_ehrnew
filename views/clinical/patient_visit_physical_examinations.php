<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<?php if($inpatientCare){?>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open("Clinical/inpatient_physical_examinations/$patientid/$patientvisitid/",$attributes); 
                ?>
                  
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start (Yyyy-mm-dd)" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End (Yyyy-mm-dd)" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Clinical/inpatient_physical_examinations/$patientid/$patientvisitid/".$start."_".$end."_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">export</span>'); ?>
        &nbsp;&nbsp;
        <?php echo anchor("Clinical/add_physical_examination/$patientid/$patientvisitid/$inpatientCare",'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Record">Record Physical Examination</span>'); ?>
        
    </div>
</div>
<?php }?>
<div style="padding-top:10px" class="row">
 <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;" colspan="8">Physical Examinations</th>
            </tr>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Cardiovascular/S</th>
                <th style="text-align:center;">Respiratory/S</th>
                <th style="text-align:center;">Abdomen</th>
                <th style="text-align:center;">Central Nervous/S</th>
                <th style="text-align:center;">Muscular Skeletal/S</th>
                <th style="text-align:center;">Examiner</th>
                <th style="text-align:center;">Timestamp</th>
             </tr>
        </thead>
        <tbody>
            <?php if($physical != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($physical as $key=>$value){
                    $examiner=$this->SuperAdministration_model->get_member_info($value->createdby);
                    $examiner=$examiner[0]->first_name.' '.$examiner[0]->last_name;
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->cardiovascularsystem; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->respiratorysystem; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->abdomen; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->centralnervoussystem; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->muscularskeletalsystem; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $examiner; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                    </tr>  
                <?php } ?>
                     
                <?php }else{?>
                    <tr>
                        <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
                    </tr>  
                <?php }?>
        </tbody>
    </table>
</div>
