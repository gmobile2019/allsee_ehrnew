<script type="text/javascript">
    $(document).ready(function(){
                var fileid;
                var divid;
                var id;
                
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
        
        $('div.filedata').find('span#show').show();
        $('div.filedata').find('span#hide').hide();
        
        $('div.filedata').each(function(){
            
            $(this).find('table').hide();
            
            $(this).find('span#show').find('a').click(function(){
                divid=$(this).parent().parent().parent().attr('id');
                id=divid.split("_");
                fileid=id[1];
                $('table#table_'+fileid).show('slow');
                $(this).parent().parent().find('span#show').hide();
                $(this).parent().parent().find('span#hide').show();
            });
            
            $(this).find('span#hide').find('a').click(function(){
                divid=$(this).parent().parent().parent().attr('id');
                id=divid.split("_");
                fileid=id[1];
                $('table#table_'+fileid).hide('slow');
                $(this).parent().parent().find('span#show').show();
                $(this).parent().parent().find('span#hide').hide();
            });
        });
        
    });
</script>
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Clinical/investigation_results/'.$patientid.'/'.$patientvisitid.'/'.$from.'/'.$to,$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo set_value('start',$start);; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo set_value('end',$end);; ?>" />
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div> 
<?php
 if($visits <> null){ ?>

<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
        <?php foreach($visits as $key=>$value){ 
            $invRes=$this->Investigation_model->patient_investigation_tests($value->patientid,$value->patientvisitid,2,null);
    
            if($invRes <> null){ ?>
                <div  id="div_<?php echo $value->patientvisitid?>" class="well filedata">
                    <div style="text-align:right">
                       <span style="font-style: italic;font-weight: bold;text-transform: capitalize"><a href="#">Visit Timestamp : <?php echo $value->createdon; ?></a></span>
                        &nbsp;&nbsp;<span id="show" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="show"><a href="#" class="glyphicon glyphicon-menu-down"></a></span>
                        &nbsp;&nbsp;<span id="hide" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="hide"><a href="#" class="glyphicon glyphicon-menu-up"></a></span> 

                    </div>
                   <table class="table table-condensed table-hover table-striped table-bordered" id="table_<?php echo $value->patientvisitid ?>">

                    <thead>
                    <tr>
                        <th style="text-align:center;">S/NO</th>
                        <th style="text-align:center;">Service Name</th>
                        <th style="text-align:center;">Test</th>
                        <th style="text-align:center;">Result</th>
                        <th style="text-align:center;">Examiner</th>
                        <th style="text-align:center;">Approver</th>
                     </tr>
                </thead>
                <tbody>
                <?php if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($invRes as $ky=>$val){
                       ?>
                        <tr>
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td >&nbsp;&nbsp;<?php echo $val->name ?></td>
                            <td >&nbsp;&nbsp;<?php echo $val->test ?></td>
                            <td>&nbsp;&nbsp;<?php echo $val->result; ?></td>
                            <td>&nbsp;&nbsp;<?php $examiner = $this->ion_auth->user($val->createdby)->row();echo $examiner->first_name.' '.$examiner->last_name; ?></td>
                            <td>&nbsp;&nbsp;<?php $approver = $this->ion_auth->user($val->approvedby)->row();echo $approver->first_name.' '.$approver->last_name; ?></td>
                        </tr>  
                    <?php } ?>
                </tbody>
                </table>
            </div>
            <?php }
            } ?>
    </div>
      
</div>
<?php } ?>