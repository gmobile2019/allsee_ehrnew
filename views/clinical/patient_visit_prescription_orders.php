<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<?php if($inpatientCare){?>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open("Clinical/inpatient_service_orders/$patientid/$patientvisitid/",$attributes); 
                ?>
                  
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start (Yyyy-mm-dd)" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End (Yyyy-mm-dd)" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Clinical/inpatient_prescriptions/$patientid/$patientvisitid/".$start."_".$end."_".$status."_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">export</span>'); ?>
        &nbsp;&nbsp;
        <?php echo anchor("Clinical/create_dispense_order/$patientid/$patientvisitid/$inpatientCare",'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Create">Create Prescription Order</span>'); ?>
        
    </div>
</div>
<?php }?>
<div style="padding-top:10px" class="row">
 <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;" colspan="<?php echo $inpatientCare?9:8?>">Prescription Orders</th>
            </tr>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Item</th>
                <th style="text-align:center;">Frequency</th>
                <th style="text-align:center;">Dosage</th>
                <th style="text-align:center;">No Days</th>
                <th style="text-align:center;">Quantity</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Action Date</th>
                <?php echo $inpatientCare?'<th style="text-align:center;">Action</th>':''; ?>
             </tr>
        </thead>
        <tbody>
            <?php if($prescriptions != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($prescriptions as $key=>$value){
                    
                    $itm=$this->Inventory_model->items($value->itemid);
                    $freq=$this->Pharmacy_model->drug_frequencies($value->frequencyid);
                    $dosage=$this->Pharmacy_model->drug_dosages($value->dosageid);
                    
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $itm[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $freq[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $dosage[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->days; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->quantity; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status ==1?'Confirmed':'Pending'?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                        <?php $action=$value->status == 0?anchor('Clinical/remove_dispense_order/'.$value->id.'/'.$patientid.'/'.$patientvisitid.'/'.$inpatientCare,'<span class="glyphicon glyphicon-remove" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cancel"></span>'):'';echo $inpatientCare?'<td>'.$action.'</td>':''; ?>
                    </tr>  
                <?php } ?>
                     
                <?php }else{ ?>
            <tr>
                <td colspan="<?php echo $inpatientCare?8:7?>" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
</div>
