<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>

<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<?php
        $this->load->view($physical_content,$data);
        $this->load->view($vitals_content,$data);
        $this->load->view($diagnosis_content,$data);
        $this->load->view($allegies_content,$data);
        $this->load->view($service_orders_content,$data);
        $this->load->view($prescription_orders_content,$data);
        ?>
        
    </div>
    
</div>
