<script type="text/javascript">
    $(document).ready(function(){
            var noteid;
            var divid;
            var id;
            
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
        $('div.notes').find('span#show').hide();
        $('div.notes').find('span#hide').show();
        
        $('div.notes').each(function(){
            
            
            $(this).find('span#show').find('a').click(function(){
                divid=$(this).parent().parent().parent().attr('id');
                id=divid.split("-");
                noteid=id[1];
                $('table#table-'+noteid).show('slow');
                $(this).parent().parent().find('span#show').hide();
                $(this).parent().parent().find('span#hide').show();
            });
            
            $(this).find('span#hide').find('a').click(function(){
                divid=$(this).parent().parent().parent().attr('id');
                id=divid.split("-");
                noteid=id[1];
                $('table#table-'+noteid).hide('slow');
                $(this).parent().parent().find('span#show').show();
                $(this).parent().parent().find('span#hide').hide();
            });
        });
    });
</script>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open("Clinical/inpatient_doctor_notes/$patientid/$patientvisitid/",$attributes); 
                ?>
                  
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start (Yyyy-mm-dd)" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End (Yyyy-mm-dd)" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Clinical/inpatient_doctor_notes/$patientid/$patientvisitid/".$start."_".$end."_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">export</span>'); ?>
        &nbsp;&nbsp;
        <?php echo anchor("Clinical/write_doctor_notes/$patientid/$patientvisitid/",'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Record">Record Notes</span>'); ?>
    
    </div>
</div>
<div style="padding-top:10px" class="display_content" >
  
  
            <?php if($doctorNotes != null){
                $rounds=count($doctorNotes);
                foreach($doctorNotes as $key=>$value){
                    
                    $doc=$this->SuperAdministration_model->get_member_info($value->createdby);
                    $doc=$doc[0]->first_name.' '.$doc[0]->last_name;
                    
                    ?>
        <div class="well notes" id="div-<?php echo $value->id; ?>">
            <div style="text-align:right">
               <span style="font-style: italic;font-weight: bold;text-transform: capitalize"><a href="#">Round <?php echo $rounds-- ;?></a></span>&nbsp;&nbsp;<span id="show" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="show"><a href="#" class="glyphicon glyphicon-menu-down"></a></span>&nbsp;&nbsp;<span id="hide" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="hide"><a href="#" class="glyphicon glyphicon-menu-up"></a></span> 
            </div>
            <table class="table table-condensed table-hover table-striped table-bordered" id="table-<?php echo $value->id ?>">

                <tbody>
                    <tr>
                        <td>&nbsp;&nbsp;Overall Progress</td>
                        <td>&nbsp;&nbsp;<?php echo $value->overallprogress; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Complains</td>
                        <td>&nbsp;&nbsp;<?php echo $value->complains; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Recommendations</td>
                        <td>&nbsp;&nbsp;<?php echo $value->recommendations; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Timestamp</td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Doctor</td>
                        <td>&nbsp;&nbsp;<?php echo $doc; ?></td>
                    </tr>  
                
            </tbody>
        </table>
        </div>
            
        <?php } ?>
                     
    <?php } ?>
</div>
