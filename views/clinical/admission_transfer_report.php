<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Clinical/inpatient_transfers_report',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End" value="<?php echo $end; ?>" />
                    </div>
                     <div class="form-group ">
                        <label class="sr-only" for="ward"></label>
                        <select name="ward" id="ward" class="form-control" >
                        <option></option>
                        <?php foreach($wards as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo $ward == $value->id?"selected='selected'":""; ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                        </select>
                    </div>
                    <div class="form-group ">
                        <label class="sr-only" for="bed"></label>
                        <select name="bed" id="bed" class="form-control" >
                        <option></option>
                        <?php foreach($beds as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo $bed == $value->id?"selected='selected'":""; ?>><?php echo $value->bedidentity; ?></option>
                        
                        <?php } ?>
                        
                    </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="pid"></label>
                        <input type="text" class="form-control" name="pid" id="pid" placeholder="Patient Id" value="<?php echo $pid; ?>" />
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Clinical/inpatient_transfers_report/start_".$start."_end_".$end."_ward_".$ward."_bed_".$bed."_pid_".$pid."_docType_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php echo anchor("Clinical/inpatient_transfers_report/start_".$start."_end_".$end."_ward_".$ward."_bed_".$bed."_pid_".$pid."_docType_2/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
    </div>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Patient Id</th>
                <th style="text-align:center;">Admission Id</th>
                <th style="text-align:center;">Ward</th>
                <th style="text-align:center;">Bed</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Admission Timestamp</th>
                <th style="text-align:center;">Transfer Timestamp</th>
             </tr>
        </thead>
        <tbody>
            <?php if($data != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($data as $key=>$value){
                    
                    $wrd=$this->Inventory_model->units($value->wardid);
                    $bd=$this->Administration_model->ward_beds($value->bedid);
                    $patient=$this->Reception_model->patient($value->patientid);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $patient->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->patientid; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->admissionid; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $wrd[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $bd[0]->bedidentity; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->admittedon; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->transferedon; ?></td>
                    </tr>  
                <?php } 
                }else{ ?>
            <tr>
                <td colspan="9" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
