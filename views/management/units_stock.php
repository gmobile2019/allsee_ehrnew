<script type="text/javascript">
    $(document).ready(function(){
      var code=$('input[name=code]').val();
      var unit=$('input[name=unit]').val();
      
      $('table#linktable').find('tbody').find('tr').find('td#'+code).find('a').css({color: '#ff0000'});
      $('table#linktableunits').find('tbody').find('tr').find('td#'+unit).find('a').css({color: '#ff0000'});                                                                          
    });
</script>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
     
     <input type="hidden" value="<?php echo $unit; ?>" name="unit"/>
     <table class="table" id="linktableunits">
        <tbody>
            <?php if($units != null){
                $code=$code <>null?$code:'null';
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                     ?>
                 <tr>
                <?php foreach($units as $key=>$value){ ?>
                    
                        <td style="text-align: center" id="<?php echo $value->id; ?>">&nbsp;&nbsp;<?php echo anchor('Management/units_stock/'.$value->id.'/'.$code,$value->name); ?></td>
                     
                <?php } ?>
                </tr> 
               <?php }else{ ?>
            <tr>
                <td colspan="2" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <?php if($unit <> null){ ?>
    <input type="hidden" value="<?php echo $code; ?>" name="code"/>
    <table class="table" id="linktable">

    <tbody>
        <?php if($item_categories != null){

            if($per_page == null){
                    $i=1;
                }else{
                    $i=$per_page+1;
                }
                 ?>
             <tr>
            <?php foreach($item_categories as $key=>$value){ ?>

                    <td style="text-align: center" id="<?php echo $value->code; ?>">&nbsp;&nbsp;<?php echo anchor('Management/units_stock/'.$unit.'/'.$value->code,$value->name); ?></td>

            <?php } ?>
            </tr> 
           <?php }else{ ?>
            <tr>
                <td colspan="2" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <?php }?>
     <?php if($code <> null && $unit <> null && $code <> 'null'){ 
         $this->load->view($units_stock_items,$data);
     }?>
    
</div>
    
</div>
