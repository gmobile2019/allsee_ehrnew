<script type="text/javascript">
    $(document).ready(function(){
        var patientid;
       
       
       patientid=$('input[name=patientid').val();
       
       if(patientid != null && patientid != ''){
       
            $.ajax({
            type:'POST',
            url:'<?php echo site_url('Reception/modal_patient_details'); ?>',
            data:{patientid:patientid},
            success:function(data){

                var patient=data.split("=_");

                 $('input#name').val(patient[1]);
            }

            });
       }
       
                
       $('input[name=patientid').change(function(){
                
                patientid=$(this).val();
               
               if(patientid != null && patientid != ''){
                   $.ajax({
                    type:'POST',
                    url:'<?php echo site_url('reception/modal_patient_details'); ?>',
                    data:{patientid:patientid},
                    success:function(data){
                       
                        var patient=data.split("=_");

                        $('input#name').val(patient[1]);
                    }

                    });
               }
                
 
        });
        
        $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
             $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div class="display_content">
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Management/patient_statement/'); 
                ?>
       
                <div class="form-group row">
                    <label for="patientid" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Patient Id &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="patientid" id="patientid" placeholder="Patient Id" value="<?php echo set_value('patientid'); ?>" />
                        <?php echo form_error('patientid'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Patient Name</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" readonly="true" id="name" value=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="start" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Start Date&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="start" id="start" value="<?php echo set_value('start'); ?>" />
                        <?php echo form_error('start'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="end" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">End Date&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                       <input type="text" class="form-control" name="end" id="end" value="<?php echo set_value('end'); ?>" />
                        <?php echo form_error('end'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Generate</button>
                    </div>
                </div>
        
        <?php echo form_close(); 
            
            if($data <> null){ ?>
                <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <?php echo anchor("Management/patient_statement/patientid_".$patientid."_start_".$start."_end_".$end."_docType_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
                        &nbsp;&nbsp;&nbsp;
                        <?php // echo anchor("Management/patient_statement/patientid_".$patientid."_start_".$start."_end_".$end."_docType_2/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>

                </div>
        <?php
                $this->load->view($statement,$this->data);
            }
        ?>        
</div>
