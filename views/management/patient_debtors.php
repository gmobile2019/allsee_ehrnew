<script type="text/javascript">
    $(document).ready(function(){
        
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
        $('div.display_content').find('table').find('tbody').find('tr').find('td.patientdetails').find('a').click(function(){
                
                var patientid=$(this).attr('id');
               
                $.ajax({
                type:'POST',
                url:'<?php echo site_url('reception/modal_patient_details'); ?>',
                data:{patientid:patientid},
                success:function(data){

                    var patient=data.split("=_");
                    
                     $('div#myModal').find('table').find('td#patientid').text(patient[0]);
                     $('div#myModal').find('table').find('td#name').text(patient[1]);
                     $('div#myModal').find('table').find('td#gender').text(patient[2]);
                     $('div#myModal').find('table').find('td#marital').text(patient[3]);
                     $('div#myModal').find('table').find('td#tribe').text(patient[4]);
                     $('div#myModal').find('table').find('td#dob').text(patient[5]);
                     $('div#myModal').find('table').find('td#email').text(patient[6]);
                     $('div#myModal').find('table').find('td#phone').text(patient[7]);
                     $('div#myModal').find('table').find('td#region').text(patient[8]);
                     $('div#myModal').find('table').find('td#district').text(patient[9]);
                     $('div#myModal').find('table').find('td#street').text(patient[10]);
                     $('div#myModal').find('table').find('td#occupation').text(patient[11]);
                     
                     $('#myModal').modal({
                        keyboard:true,
                       show:true,
                       })
                 }

                });
 
            });
    });
</script>
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Management/patients_debtors/',$attributes); 
                  
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="patientid"></label>
                        <input type="text" class="form-control" name="patientid" id="patientid" placeholder="Patient Id" value="<?php echo $patientid; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
     
     <?php if($patientid <> null)?>
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Patient Id</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Gender</th>
                <th style="text-align:center;">Date of Birth</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($patient_debtors != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($patient_debtors as $key=>$value){
                    $patient=$this->Reception_model->patient($value->patientid);
                    $title=$this->Reception_model->titles($patient->titleid);
                    $gender=$this->Reception_model->genders($patient->genderid);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td class="patientdetails">&nbsp;&nbsp;<a type="button" href="#" data-toggle="modal" id="<?php echo $value->patientid ?>"><?php echo $value->patientid ?></a></td>
                        <td>&nbsp;&nbsp;<?php echo $title[0]->shortname.'.'.$patient->name ?></td>
                        <td>&nbsp;&nbsp;<?php echo $gender[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $patient->dob; ?></td>
                        <td class="transaction_details" style="text-align: center">
                            &nbsp;&nbsp;
                            <?php echo anchor('Management/patient_debt_details/'.$value->patientid,'<span class="glyphicon glyphicon-arrow-right" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Debt Details"></span>');?>
                        </td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="9" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>     
</div>
     <!-- Modal -->
       <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="gridSystemModalLabel">Patient Details</h4>
            </div>
            <div class="modal-body">
              <div class="row container">
                  <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                      <table class="table table-condensed table-hover">
                          <tr>
                              <td>Patient Id</td>
                              <td id="patientid"></td>
                          </tr>
                          <tr>
                              <td>Name</td>
                              <td id="name"></td>
                          </tr>
                          <tr>
                              <td>Gender</td>
                              <td id="gender"></td>
                          </tr>
                          <tr>
                              <td>Marital Status</td>
                              <td id="marital"></td>
                          </tr>
                          <tr>
                              <td>Tribe</td>
                              <td id="tribe"></td>
                          </tr>
                          <tr>
                              <td>Date of Birth</td>
                              <td id="dob"></td>
                          </tr>
                          <tr>
                              <td>Email</td>
                              <td id="email"></td>
                          </tr>
                          <tr>
                              <td>Phone</td>
                              <td id="phone"></td>
                          </tr>
                          <tr>
                              <td>Region</td>
                              <td id="region"></td>
                          </tr>
                          <tr>
                              <td>District</td>
                              <td id="district"></td>
                          </tr>
                          <tr>
                              <td>Street</td>
                              <td id="street"></td>
                          </tr>
                          <tr>
                              <td>Occupation</td>
                              <td id="occupation"></td>
                          </tr>
                      </table>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div> 
</div>
