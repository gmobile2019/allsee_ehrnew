<script type="text/javascript">
    $(document).ready(function(){
       
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('div.display_content').find('table').find('tbody').find('tr').find('td.transaction_details').find('a.md').click(function(){
                var bid=$(this).attr('id');
               
                $.ajax({
                type:'POST',
                url:'<?php echo site_url('reception/modal_patient_bill_details'); ?>',
                data:{bid:bid},
                success:function(data){
                    var bill=data.split("=_");
                    
                     $('div#myModal').find('table').find('td#pid').text(bill[0]);
                     $('div#myModal').find('table').find('td#pvid').text(bill[1]);
                     $('div#myModal').find('table').find('td#dpt').text(bill[2]);
                     $('div#myModal').find('table').find('td#subdpt').text(bill[3]);
                     $('div#myModal').find('table').find('td#srv').text(bill[4]);
                     $('div#myModal').find('table').find('td#sp').text(bill[5]);
                     $('div#myModal').find('table').find('td#pmode').text(bill[6]);
                     $('div#myModal').find('table').find('td#amount').text(bill[7]);
                     $('div#myModal').find('table').find('td#recno').text(bill[8]);
                     $('div#myModal').find('table').find('td#pstatus').text(bill[9]);
                     
                     $('#myModal').modal({
                        keyboard:true,
                       show:true,
                       })
                 }

                });
 
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Management/patient_transactions/',$attributes); 
                  
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="patientid"></label>
                        <input type="text" class="form-control" name="patientid" id="patientid" placeholder="Patient Id" value="<?php echo $patientid; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="sponsor"></label>
                        <select name="sponsor" id="sponsor" class="form-control" >
                                <option value="" >All Sponsors</option>
                                <?php foreach($sponsors as $key=>$value){ ?>

                                <option value="<?php echo $value->sponsorcode; ?>" <?php echo ($sponsor == $value->sponsorcode )?'selected="selected"':''; ?>><?php echo $value->shortname; ?></option>

                                <?php } ?>
                        </select>
                    </div>
                   <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Management/patient_transactions/patientid_".$patientid."_start_".$start."_end_".$end."_sponsor_".$sponsor."_docType_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php echo anchor("Management/patient_transactions/patientid_".$patientid."_start_".$start."_end_".$end."_sponsor_".$sponsor."_docType_2/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
     
     <?php if($patientid <> null)?>
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Patient Id</th>
                <th style="text-align:center;">Visit Id</th>
                <th style="text-align:center;">Service</th>
                <th style="text-align:center;">Sponsor</th>
                <th style="text-align:center;">Receipt No</th>
                <th style="text-align:center;">Amount</th>
                <th style="text-align:center;">Action Date</th>
                <th style="text-align:center;">Payment Status</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($patient_transactions != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                    
                foreach($patient_transactions as $key=>$value){
                    $download=FALSE;
                    $srv=$this->Inventory_model->items($value->service_item_id);
                    $dept=$this->Administration_model->get_department_by_servicecharge_code($this->config->item('pharmacy_servicecode'));
                    
                    if($dept[0]->id <> $value->departmentid){
                        $srv=$this->Administration_model->services($value->service_item_id);
                        $download=TRUE;
                    }
                    if($value->paymentstatus == 1){
                        $status="Paid";
                    }
                    
                    if($value->paymentstatus == 2){
                        $status="Pending";
                    }
                    
                    if($value->paymentstatus == 3){
                        $status="Cancelled";
                    }
                    
                    if($value->paymentstatus == 4){
                        $status="Prepaid";
                    }
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->patientid ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->patientvisitid ?></td>
                        <td>&nbsp;&nbsp;<?php echo $srv[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->shortname; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->transactionid; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->amount; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $status; ?></td>
                        <td class="transaction_details" style="text-align: left">&nbsp;&nbsp;<a type="button" href="#" data-toggle="modal" id="<?php echo $value->id ?>" class="md"><span class="glyphicon glyphicon-th" aria-hidden="true" ></span></a>&nbsp;&nbsp;</td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="9" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>     
</div>
    <!-- Modal -->
       <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="gridSystemModalLabel">Transaction Details</h4>
            </div>
            <div class="modal-body">
              <div class="row container">
                  <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                      <table class="table table-condensed table-hover">
                          <tr>
                              <td>Patient Id</td>
                              <td id="pid"></td>
                          </tr>
                          <tr>
                              <td>Visit Id</td>
                              <td id="pvid"></td>
                          </tr>
                          <tr>
                              <td>Department</td>
                              <td id="dpt"></td>
                          </tr>
                          <tr>
                              <td>Sub Department</td>
                              <td id="subdpt"></td>
                          </tr>
                          <tr>
                              <td>Service</td>
                              <td id="srv"></td>
                          </tr>
                          <tr>
                              <td>Sponsor</td>
                              <td id="sp"></td>
                          </tr>
                          <tr>
                              <td>Payment Mode</td>
                              <td id="pmode"></td>
                          </tr>
                          <tr>
                              <td>Amount</td>
                              <td id="amount"></td>
                          </tr>
                          <tr>
                              <td>Receipt No</td>
                              <td id="recno"></td>
                          </tr>
                          <tr>
                              <td>Payment Status</td>
                              <td id="pstatus"></td>
                          </tr>
                      </table>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
</div>
