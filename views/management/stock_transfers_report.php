<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
    });
</script>
<div style="text-align: center;padding-top: 20px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Management/stock_transfer_report',$attributes); 
                ?>
                    
                    <div class="form-group ">
                        <label class="sr-only" for="unit"></label>
                        <select name="unit" id="unit" class="form-control" >
                                <option value="" >Source Unit</option>
                                <option value="main" <?php echo ($unit == 'main' )?'selected="selected"':''; ?>>Main Store</option>
                                <?php foreach($units as $key=>$value){ ?>

                                <option value="<?php echo $value->id; ?>" <?php echo ($unit == $value->id )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="item"></label>
                        <input type="text" class="form-control" name="item" id="item" placeholder="Item" value="<?php echo $item; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="col-md-offset-7 col-lg-offset-7 col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <?php echo anchor("Management/stock_transfer_report/1_".$unit."_".$item."_".$start."_".$end."/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
        &nbsp;&nbsp;&nbsp;
        <?php echo anchor("Management/stock_transfer_report/2_".$unit."_".$item."_".$start."_".$end."/",'<span id="excel" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">excel</span>'); ?>
        
    </div>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Inventory Unit</th>
                <th style="text-align:center;">Destination</th>
                <th style="text-align:center;">Item</th>
                <th style="text-align:center;">Store Batch</th>
                <th style="text-align:center;">Action Timestamp</th>
                <th style="text-align:center;">Quantity</th>
                <th style="text-align:center;">Value</th>
             </tr>
        </thead>
        <tbody>
            <?php if($transfers != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($transfers as $key=>$value){
                    $itemValue=ceil($value->quantity*$value->unitprice);
                    $total +=$itemValue;
                    $total_qty +=$value->quantity;
                    $source=$value->source_inventoryunit <> null?$this->Inventory_model->units($value->source_inventoryunit):'Main Store';
                    $destination=$value->inventoryunit <> null?$this->Inventory_model->units($value->inventoryunit):'Main Store';
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $source=='Main Store'?'Main Store':$source[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $destination==null?null:$destination[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->storebatch; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->lastupdate; ?></td>
                        <td style="text-align:right">&nbsp;&nbsp;<?php echo $value->quantity; ?></td>
                        <td style="text-align:right">&nbsp;&nbsp;<?php echo number_format($itemValue,2); ?></td>
                    </tr>  
                <?php } ?>
                    <tr>
                        <th colspan="6" style="text-align:center">Total</th>
                        <th style="text-align:right"><?php echo $total_qty?></th>
                        <th style="text-align:right"><?php echo number_format($total, 2)?></th>
                    </tr>  
                <?php }else{ ?>
            <tr>
                <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
