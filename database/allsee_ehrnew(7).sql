-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2016 at 03:58 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `allsee_ehrnew`
--
CREATE DATABASE IF NOT EXISTS `allsee_ehrnew` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `allsee_ehrnew`;

-- --------------------------------------------------------

--
-- Table structure for table `bill_transaction_ids`
--

DROP TABLE IF EXISTS `bill_transaction_ids`;
CREATE TABLE IF NOT EXISTS `bill_transaction_ids` (
`id` int(11) NOT NULL,
  `next` int(11) NOT NULL,
  `lastupdate` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bill_transaction_ids`
--

INSERT INTO `bill_transaction_ids` (`id`, `next`, `lastupdate`) VALUES
(1, 15, '2016-04-25 10:56:41');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
`id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `servicechargeid` int(11) NOT NULL,
  `code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `servicechargeid`, `code`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'Consultation', 102, '001', 1, 196, '2016-03-12 08:22:58', 196, '2016-03-20 04:34:54'),
(2, 'Radiology', 103, '002', 1, 196, '2016-03-14 12:35:11', NULL, NULL),
(3, 'Pharmacy', 105, '003', 1, 196, '2016-03-14 12:35:25', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `disposed_inventory`
--

DROP TABLE IF EXISTS `disposed_inventory`;
CREATE TABLE IF NOT EXISTS `disposed_inventory` (
`id` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `reason` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `storebatch` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `systembatch` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `source` int(11) DEFAULT NULL,
  `actiondate` datetime NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `disposed_inventory`
--

INSERT INTO `disposed_inventory` (`id`, `itemid`, `quantity`, `reason`, `storebatch`, `systembatch`, `source`, `actiondate`, `userid`) VALUES
(1, 1, 3, 'EXPIRY', 'XYAWS1D34', '0001-15-0139', NULL, '2016-04-18 12:01:29', 200),
(2, 1, 87, 'EXPIRY', 'XYAWS1D34', '0001-15-0139', NULL, '2016-04-18 16:00:58', 200),
(5, 1, 10, 'EXPIRY', 'XYAWS1D34', '0001-15-0139', 1, '2016-04-25 11:50:23', 200),
(6, 4, 2, 'EXPIRY', '0001-48-3857', '0001-48-3857', NULL, '2016-04-25 11:51:35', 200);

-- --------------------------------------------------------

--
-- Table structure for table `drug_dosages`
--

DROP TABLE IF EXISTS `drug_dosages`;
CREATE TABLE IF NOT EXISTS `drug_dosages` (
`id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `factor` double NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drug_dosages`
--

INSERT INTO `drug_dosages` (`id`, `name`, `factor`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, '1', 1, 1, 202, '2016-04-22 07:16:51', NULL, NULL),
(2, '2', 2, 1, 202, '2016-04-22 07:16:57', NULL, NULL),
(3, '1/2', 0.5, 1, 202, '2016-04-22 07:18:13', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `drug_frequencies`
--

DROP TABLE IF EXISTS `drug_frequencies`;
CREATE TABLE IF NOT EXISTS `drug_frequencies` (
`id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `factor` double NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drug_frequencies`
--

INSERT INTO `drug_frequencies` (`id`, `name`, `factor`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'Once a day', 1, 1, 202, '2016-04-22 07:13:07', NULL, NULL),
(2, 'Twice a day', 2, 1, 202, '2016-04-22 07:15:39', NULL, NULL),
(3, 'Thrice a day', 3, 1, 202, '2016-04-22 07:15:52', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `drug_groups`
--

DROP TABLE IF EXISTS `drug_groups`;
CREATE TABLE IF NOT EXISTS `drug_groups` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drug_groups`
--

INSERT INTO `drug_groups` (`id`, `name`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'ANTIBIOTCS', '', 1, 200, '2016-04-05 07:23:22', NULL, NULL),
(2, 'ANTI AMOEBICIDES', '', 1, 200, '2016-04-05 07:23:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `drug_types`
--

DROP TABLE IF EXISTS `drug_types`;
CREATE TABLE IF NOT EXISTS `drug_types` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drug_types`
--

INSERT INTO `drug_types` (`id`, `name`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'CAPS', '', 1, 200, '2016-04-05 07:21:59', NULL, NULL),
(2, 'SYRUP', '', 1, 200, '2016-04-05 07:22:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

DROP TABLE IF EXISTS `genders`;
CREATE TABLE IF NOT EXISTS `genders` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `name`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'Male', 'Male gender', 1, 197, '2016-03-16 12:07:28', NULL, NULL),
(2, 'Female', 'Female gender', 1, 197, '2016-03-16 12:07:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
`id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'SuperAdministrator', 'Super Administrator user'),
(2, 'Administrator', 'Administrator User'),
(3, 'Nurse', 'Nurse'),
(4, 'Doctor', 'Doctor users'),
(5, 'Pharmacist', 'Pharmacy users'),
(6, 'LabTechnician', 'Performs laboratory tests'),
(7, 'Receptionist', 'Reception users'),
(8, 'Manager', 'Managerial users'),
(9, 'Diagnostic', 'Diagnosis Examiner'),
(10, 'Financial', 'Financial person'),
(11, 'StoreKeeper', 'Store keeper user');

-- --------------------------------------------------------

--
-- Table structure for table `institution`
--

DROP TABLE IF EXISTS `institution`;
CREATE TABLE IF NOT EXISTS `institution` (
`id` int(11) NOT NULL,
  `institution_id` varchar(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `category` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `postal` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `logostring` text NOT NULL,
  `logoname` varchar(50) NOT NULL,
  `printer` int(1) NOT NULL DEFAULT '1',
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `institution`
--

INSERT INTO `institution` (`id`, `institution_id`, `name`, `category`, `email`, `phone`, `fax`, `website`, `postal`, `city`, `logostring`, `logoname`, `printer`, `status`) VALUES
(2, '0001', 'ALLSEE EHR', 1, 'kemixenock@gmail.com', '+255719186759', NULL, NULL, '67414', 'Dar es salaam', '/9j/4AAQSkZJRgABAQEAYABgAAD/7AARRHVja3kAAQAEAAAAPAAA/9sAQwACAQECAQECAgICAgICAgMFAwMDAwMGBAQDBQcGBwcHBgcHCAkLCQgICggHBwoNCgoLDAwMDAcJDg8NDA4LDAwM/9sAQwECAgIDAwMGAwMGDAgHCAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgBJgI+AwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A/fyiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAGGdB1YDNKJVPAINfLn7f3xt/aF+DF9oNx8GPh3onjjRpreVtVeffNdwTBvkVYVkQ7CvO4FiSSMDGT8W+Mf+C9nx2+DWqfYvHHwg8O6Hck7RFqVvfaazN6KzkhufTNcFfMaVGfJO687aH6Jw34X51n2GjiMrdKfNf3fawU1Z21g2mttL9NT9dg4LEAgkUtflL4P/wCDli7Vo11z4PQTBj80um+JcfL6hJLfBP1cV7L8Pv8Ag4d+C3ihkj1zRvHPhWRurXNhHdRKPXdBI5/SlDNMNLaX3pr9DrzDwZ4zwabqYCUkv5HCf4QlJ/gfetFeLfCH/gob8GPjtPHb+GfiJ4Zur2bAS0uboWlyzH+ERy7WJ9gDXsiTOSoZdpNdtOpCavBp+h+eY7LsXgqnscZSlTl2lFxf3NJktFFFWcYUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFACeUo5CqD9Kq63odn4j0ueyv7S1vrS4XbJBcRLLFIPRlYEEfUVbooeu44yad0fKnx6/4I1/Aj48LPM3hIeEdTly323w3ILBgx7+UAYj9ClfCH7Sn/Bvj8SfhpFcah8O9esPiDpyZYWVwi6dqaL1wMsYpG46gpnPCiv2aprxB/avPr5Xh6mvLZ91p/wAA/S+GfF7inJGo0MU6lNfYqe/G3ZX96K/wyR/L18QPh1rHw28S3Gg+KtB1HQdXtsrNYanatBMoB5O1hyue4yD6mvQfgB+2/wDFn9l8wR+CPHWu6bp0Hyppks5utOUdcCCTcijgD5QvU9M1/QT8ef2YPAf7Tng9tC8d+GtN8R2HPlfaY/31qxH3opRh4291Ir8rv23f+CCvif4VrfeIfhBfXHjPQ4maSTQLrA1ezTn/AFT8LcqMYx8sgyMCTmvExGVV6L56Tuu63/r0P6X4X8b+G+Jqay3iKjGlOWlqlp0pPylJWi/KSSXSbZ3X7Mf/AAcUyNPBpnxe8HLGhAB17w4zEdhmWzkJI9S0chz2QCv0X+An7TvgH9prwkus+BPFGl+I7JAPOFtL++tif4ZYzh4z7MBntmv5pbm2lsr+5tbiKW2urOV7e4glQpLBIjFWRlIyrKQQQQCK2Phx8R/EXwc8aW3iTwlrmpeHNes/9Te2M5hlUd1OOGU9CrAqR1FGGzitDSp7y/H+vUvi/wCjtkeYxdbJZfVqj1S1lTb803eN+8XZdIs/p+jkEgJGcClr8wf2EP8Agvva67PZeFPjZbQaVduBDb+K7OPFpO3AH2uEf6knqZEynqqDk/pjoet2/iHTra9sriC8sb2Fbi3uIHEkVxGwDK6MDhlYEEEcEEV9HhsVTrx5qb/zP5E4t4KzfhvFfVc1pct/hktYSXeMtn5rRrqkXaKKK6D5QKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooPQ1wHxw/aX8Ffs46FHqPjPxHYaJDOD5EcjF57kjqI41y7/AIDHNROpGEeabsvMwxOKo4em61eahFbttJL1b0R35OAT6UzzwSAASfYV+ePxW/4Lcal421SfRvg74D1DV5+UF9qMDzyE9MpawbjjvmRweeVFeK+NfDH7Xn7VZY6rpvj46fdHJtEA0izwexjBTI/3smvGq57SvahF1H5LT7/+Afm2YeKmXxbp5TRqYuX/AE7g+X/wK34xUkfp18TP2r/hr8HHKeJvG/hrSJxx5Et8jT5/65qS36V4p4w/4LJfA/w28i22ra5rToMD7BpchVvo0mwV8M6R/wAEgPjtfqzN4X0iw39TNrNupb3IUsfzq9J/wRo+OaAn+zPDDjH3V1lc/Tlf6151TNczlrToW+Tf+X5HyGO4946rXeCyp04/3oTk/wD2z/0k+mtY/wCC7Hw8t5ALHwb40vQTkGQW0B/WQ1Rg/wCC8fgsyqJ/h/4tjXuRcWz4/DcK+Vde/wCCUPx38PI0jeClvlAyDaanbSsfw3g15X8QP2afiH8Lo3k8R+B/FGkQxnDTT6bI0SfV1BXH4159bNc0h7000v8ADp+R8dj/ABC4/wAL7+JoygvOi0vva/U/S3wt/wAFt/g9rMwS/svGGi5H3p9NWVAT7xux/SvbvhZ+3b8I/jLcR2+gePNAnvZfu2lxP9kuD7BJdpJ+ma/CuPybhSUdXUcHacgH046GnbFmQiQBhjoRmnS4oxKfvpNfd+P/AADPLvHfO6TX1qlTqLyTi/vTa/8AJT+in7SvBwxBGQexp6tuUHHWvw0/Z9/bu+KH7M91DH4e8TXV1pMZG7SdUY3dkw9FVjuj4GPkK1+kX7GX/BVHwf8AtQ3NnoOrRDwh4xmwq2VxKHtNQbj/AI95jjJP/PNwG9N2M19Bgc+w+Iag/dl2fX0Z+y8J+K+TZ1OOHk3RrPaM7Wb7Rls/JOzfRH1VSSKWQgdTRvX1H50uc8jkV7h+nnyp/wAFAP8AglL4J/bi0ibU0EPhb4gQR4tfEFtAGNzgfLFdoMedH23Z3qOhx8p/Ev8AaM/Zs8Y/spfFC88IeN9Jk03U7YloJhlrXUYQcCeCQ8PGfzB4YA8V/S/Xk37Yf7HPgr9s74R3HhbxhYtIsb+fp2oQ4F3pFxjaJoW7HBwy/dcZBHp5OPyuFb34aS/B/wDB8z9z8LfGjG8OThl+ZN1cHtbeVPzg+se8Nv5bPf8Am/I6ggc19Tf8E6P+CpXiz9hfWoNFvlu/E3w1upD9p0YyfvdOLtlp7MnhGByTF9x8n7rENXlf7Yn7IHi39if4y3PhDxVGs6spuNL1OFCtvq9qGwsyZ+6wGA8ZJKMcZIIY+W43Lg96+ZjOpQqXjo0f2rjcDk/FGUqFZRr4ask0+j7Si94yXdWad1o7n9NfwW+NHhn47/DTSvFfhLVbfWdC1mITW1xCfXqjDqrqeGU4IPBFddX8+X/BN/8A4KHa5+wX8VxNI13qngLXJVXXdIRt23oBdwA9JkHUcB1+U87SP3x+Hnj7Svid4O0zxDoeoW2qaLrVrHe2N3A2Y7iF13Kw+oPQ8g5B5FfW4DHRxEO0luv1R/Avid4aYvhLHqDbnh6l3TnbfvGXRSXXo1quqW5RRmiu8/MQooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooADyCPWvNfiH+yP8ADr4tfEG28U+J/B2ha/rdrbraxz30PnqEUkqDG3yNgscEg9a9KoqJ04zVpq68znxOEo4iHs68FNXvZpNXWzs+qM3w/wCFLDwnpyWelWNjpdpGflgtIFgiX6KgArSooqkklZG0YqKskFFFFMoKbIjN91ttOooA8o+MX7Enwv8AjzHK/iXwbolxfTZJv7aAWt6D6+dHtZj/ALxIr4n/AGkP+CHOp6KJ9Q+F/iEapCuT/YurlY5kHpHcDCt6AOqn/aNfpfTWjDnJzXnYvKsLiF+8jr3Wj/r1Pj+IOA8jzmL+u4dcz+3H3Zr/ALeW/o7ryP56fHXgHXPhh4nutD8R6TqGh6vZnE1peQmORPcZ4ZTjhgSp7E1lbRKucDIOQehBr94v2mP2VvBX7UngwaP4s0iO7eLP2O/iAS905yPvRS9V91Pyt3Br8if20v2HfFH7GvjEQamV1TwzqUxTStahQpHccFhFIvPlzAA/LkggEqSOB8XmeSVcJ+8i+aHft6/5n8v8d+FuO4fvi8PJ1cP/ADW96P8AjXb+8tH1S0T+sP8AgmB/wU0u/EOq6f8ADP4kXxuLyfbBoWuTvl7huAtrcMernjZIeTja3OCf0PhyIlBBBr+dQ/u8FCyMvzAqcMpHIII6EdQfWv2O/wCCXX7YM/7UfwNSx1m5afxf4P2WOqO2N17GQfJufq6qQ3+0rHuK9vh/NnU/2Ws7tbPv5f5H6n4PeIdXMP8AhEzKfNUir05PeSW8W+sktU92t9Vd/T9FFFfVH76eO/tt/sYeGf24Pgve+E/EUccFzGDcaRqixh7jR7sKQsydMqc4dMgOuQexH8+fxx+CniP9nH4ta54I8V2Zstd8P3BgmAyY50PMc8ZP3opFwyn0ODgggf03Hoa+GP8AgtZ+wIP2mvge3jrw3Yq3j3wHC0w8tfn1bTgC01sfV0/1iH1Vl/jyPHzXA+0j7WC95fiv8z9+8DvE2eR4+OT5hP8A2Ws7JvanN7S8oy2l0TtLSzv+JpGewJHrX6H/APBCP9vaX4Y/EVPgz4nuyPDnieV5vDs8shxp+oE7mthngRzjcRjpIAMfvCa/O6GQSxhlOQwyD61JBez6bdw3NrPPaXVs6ywTwsVkgkUhldT2KkAj3xXzeGxEqNRTif2PxlwpheIspq5VjFZTWkrawmvhkvR791dPRn9Slscwg4xnP4c0+vBv+Ccn7Vq/thfsn+F/Fs7ga4kQ03XI1xtS/hAWZgB0VziRR2Dgc4zXvNfc06kZxU47PU/zJzTLcRl2Mq4DFx5alOTjJdmnZ/Ls+q1CiiirOAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiimyTLFjccbulANjqKZ9oT1zTkkVyQpzigSYtFFFAwooooAK5j4s/CPQfjZ4E1Pw54l0+DVNH1SMpNBIOh/hdT/C6kZDDkGunoIDAgjINTKKknFq6ZnWowqwdKrFOMlZp6pp7prsz8Jv2xP2WNY/ZD+M154Z1CVrzTZkN1pGo7No1C13YDHsJFPyuAfvYPRhXU/8EzvjtL8A/wBr/wANXbzbNL8RN/YOoqWwrRzsPLY/7kwRh7ZGQCc/pT/wUs/ZVh/ae/ZxvobK2RvE/hhZNV0WUDDl0Q+ZBn+7Kgxj+8EPavxbhvJ7LFxbP5V1BieBu6yKQVPthscV+eZlhZZfi4zpfDuv8v09LH8b8Z5BU4O4kpYvBXVJyVSn6Jrmg35becZK+p/RTFL5m4YwVOKdXOfCDxnF8SPhV4a8RwEmDxDpVpqUZIwSs0KSDjtw1dHX6JGSaTR/ZFGrGpBVIO6auvRhTZYzIFw20qc9M06imaM/Az/gr7+x5H+yL+11qJ0q2W38JeOfM1zSUSMJFasz/wCkWyAcAJIwKqMYWRBjpXy1X7q/8Ft/2ak/aA/Yo1jU7WBZNe+Hsv8AwkNk4XLmJEKXMWf7rQszY7tEnpX4UJIsgBU5BGa+LzLDKjXaS0eqP9E/BbjCWf8ADVOVeV61F+zn3dkuWT9YtXfWSkfoZ/wbv/tAv4P/AGifFHw4u5yun+MtP/tSyRmOFvbUgMAOxeByT6+SK/Y6v5rv2N/ivL8Df2sfhz4sjdkXSNetfPwcBoJH8mUfQxyMK/pMtWyW+YsOMd+K9rJKzlScH9l/g/8Ag3P5x+khkEcHxFTzGmrLEQTf+OFov/yXk+dyWiiivaP56CiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAR2K44zk15h+0H+2J8P8A9l+0ifxj4htLC5uF3Q2MQM97Mv8AeESZYLnjc2F963f2ifixD8C/gf4o8YTxrMvh3TpbxImOFmkVf3aE+jOVH41+D3jvx7q3xS8Y6n4i8RXs+p65q8xnu7iYkszHoo/uoo4VRwoAFeHnObvBpQgryffZI/K/EvxFfDdOnQw0FOtUTavflilpd2s229ErrZtvSz/VTTf+C3PwXvb7ypovGdlFn/Xz6Qvl/X5ZGYflXf8AhL/gqN8DPGGwQ+PdOsmfot/DNan83UAfnX4qqp3ZIAx6GhtvI4J9+a+ehxPil8Si/k/8z8awvjvn9N/vqdKa/wAMk/vUv0P3+8HfHbwd8Qig0HxZ4Z1gycKtnqUMzsfTarZrqkkZjypANfzqeQocOI1R15DL8pHvmu18B/tJfEP4Z7f+Ed8deLtHSMcRQapP5PHYoWKEcdMV20uK/wDn5T+5/wCf+Z9XgfpA03ZYzBtecZ3/AAcV/wClH780V+OPgP8A4LBfHDwUIkvNZ0jxLCmPl1LTo97DrkvFsOev/wBevdvh7/wXhRRHH4u+Hk6qD88+i6iHP4RTBf8A0ZXp0eI8FP4m4+q/yufc5b4zcM4qyqVJUn/fi/zjzJfNo/Riivmf4Zf8FYvgh8SPLibxTP4duZePI1uze1Kn0LjdGT9HIr6C8KePdE8d6Yt7oeradrFo4yJbK4SdPzUnFerRxVGr/Cmn6O5+g5dneX4+PNga8Ki/uyT++z0+ZrMcKT6V8Yf8FUv+CgfiX9li40Lwt4NgtINd1y2kvp9SuYvNWzgDbFWNDw0jMCdx4UL0JPH2W1wuxsZJA6Yr8xf+C8PlN8X/AIegAmX+xrn/AL585cfrmuDO686WDlOk7PTX5o+R8Us0xeX8N18VganJNOKut0nOKduzs9/uPJfDv/BWz47aHfCaXxTYanGPl8q80qDZj1+QKcn1zX1b+xJ/wWEb42/EbTfBvjrw/Y6NqOsuLew1TTpne2mnP3YpI3yYy3OGDMM4GBnNfmAuGU8ZB9a7j9mOOST9pf4eiEv5p8Q2IGOuPOXNfGYLNsXTqRXO2rrR6/mfzRwt4jcQ4bMKMHiZ1ISnFOM3zpptJr3rtPXRpr5rQ/fFSSoJGCRS0UV+lH9shRRRQAUUUUANljMhUhipWvw+/wCCiHwaj+Bv7YvjXRbWNYdNvrsavYoq7VSG5HmFFH91ZC6j2HbFfuHX5g/8F4vBqaf8XPAviBEKnU9LuLKVv7xhlVl/SVh+NfPcSUFPCqfWLX3PR/p9x+O+N+VxxHDrxVveozi7+Unytfe0/kfbP/BPLVZNZ/Yh+F80rFnTw/bQDvgRr5YH5KK9lryP9gjRP7A/Ys+FsB4MvhmxuiPQzQrKR+b165XsYJNYempb8q/I/TciUlluHUt/Zwv/AOAoKKKK6T1SnrWh2viDTLuyvYo7izvYXt54pFDJLGylWUg9QQSMV/M/8fvhVN8Cvjz418FTFmPhXW7vTYyQRvijlYRNz/ej2Nnvmv6a3+4fpX4U/wDBdH4dp4H/AOChet38SKkfirSbHVWAXAMgjNu5+p8kH8a8PPKd6cZ9nb7z+lfozZzKjneJy2T92rT5l/ihJW/8llL7j47uJGhhaRGKvGN6kHBBHINf02/AHxUfHfwO8G66dxOtaFY32W+8fNt0fn/vqv5kboZtpB6qf5V/SP8AsL3D3P7FvwkeQEOfBukg56nFnEK5sjl+8kvL9f8Agn2P0oaMXgcvq9VOa++MX+iPVaKKK+lP45CiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA+Qv+C0PxK/4RP8AY7bRkkaObxbq1tYYUkExJmeT6jEaj/gVfkgiBVOeCetfef8AwXd+In9ofE/wB4SR12aTp9xq0yg8l7iQRJn6LA//AH3Xw/4H8Hap8Q/GOm6Dotm99rGsXKWlpAhAMsjthVyeAO5J4ABNfnef1nVx0ox1tZL8/wA2fxl4v46eP4qnh6av7NQppLq7c2nneTRnFxkAHrTeAVJC5J+tfqb8B/8AgiT4B8L6BC/jy/1HxXrbqDMlpcvZ2MDY5WMLh2AOfmYjPoOldlq//BGb4HanGRDpGvacx722szEj/vvdVw4bxkoqTsvJv/gHXh/A7iOrRjVk6cG/syk7r1tFr8T8giwIIz1r1L9ir4DWP7S/7SnhvwZqV9Jp+nak0stzLE22Zo4omkKIezNtAz2zX6BeIP8Aghj8L79QNO8QeNdMx63UNx/6FHXD65/wQZWzuFuPD/xSv7WeE5iN1pS7l9PnikUg+4FKGQYynNSlBSSe11qPC+EHEWDxdKtWw0a8IyTlFVIrmSd3G8rbrTYufE7/AIIQaFqTyT+D/Hep6PIOVt9VsVvYz7eYjRsPqQ30NeF+Nv8Agiz8ZfDLM2mN4T8RxD7v2PUWhkP/AAGZFA/OvW5/+Cb37UPw3if/AIRb42yXsUeNkL6/qFszgdBtcSJ09WxVCaP9uz4QFpGku/EtvEuBt+wamGGeoACyE/UZrtr4PDPWeGnD/DqvzsfW5pw1klR82LyTE0W93Saml8lNxt/26fLHjP8AYN+MngQOdR+HPidogSDJa2ou1wO+Yi3FeeWd94j+DviAS28/iLwdrEXKyRSXGk3a8nkMpR/yr7dj/wCCsHx++DEoi8e/Da2nji+WSS40u605iQeTvXcnT2xXoPhH/gtZ8Kvipp62Pj3wTq2nwPjfm3g1qyB6ZI4f1xiM964XgsC3aFZwfaSt+Oh8j/qtwpUqqOFzKphqq2Vam01/28uRL1ufLvwi/wCCsnxn+FvlR3PiC18XWcQH7rW4BNIVHUecm1yfckn+dcP+2d+13qX7Z3xL0/xJqGkw6EdP0uPT47SC7a5jBDM7urFVxuLDjBxgcmvtvxt8M/2Mf2jfC2o6rp2reF/DVza273Tz6VcNpNxFhc7vszAKxzxjYc5wOa/MdWAckM7oc7SVAJGeCR2zWWYPE0qapTqqcX2d9vXVHmcayz3L8JTy/FZhHFYerrHlnz/BbdtOS30Sk19w9STHnIyfyr3T/gml8PZPiP8AtueBrcIskWlXMmrTHsqQRs2T/wACKj6sK8LPzxnAIJ6V6Z+yh+1Rr37H3xLn8TeHrDSNRubuybTp4dQjZleBpI5GVWUhkJaNeeRwOK4MHOnGvCVX4U038j4zhjE4TD5thsRjm1ShOMpWV3aLvt6qz8j93UOUU5zkUtfNX7Dv/BRzwr+2LcTaOLKfw14usoftMulzyiWO6jGA0lvKMbwpIypAZc5wRzX0pHIJASARiv1DD4inWgqlJ3TP72ynN8HmWGjjMDUU6ctmvLdNPVNdU9ULRRRW56QUUUUAFfnv/wAF4tEm8Q6R8KrS1Qte3up31jAoGSzSpAo/8ex+dfoRXg/7Uv7J2pftD/G/4Sa6t3pcOgeAdUn1LUbedpBPdFhGY1jAUqfmjGdxHHTNedmtCVbDSpRWrt+a/Q+R47yirmmSVsvoxvKo4L5e0i2/krv5HsPgPw7F4O8HaVo8AQQaTZwWUYVdqhY41QYHYYHSteo7eNowd2CTzUleglZWR9ZCCjFRWyCiiimUJIcRsfQV+QH/AAcd6FHbfHn4a6mqgS32g3MDtjlvLuFYD8PMr9gGOFJ54r8mf+DkmdW8b/CWPcDJ9i1Jyo6geZAM/nXmZwv9lfqvzP2PwEqOPG2FS6qov/Kcn+h+ZF5/x6y9sqf5V/S5+yfoUvhf9mH4caZOAtxp3hbTLaUDoHS0jUj8xX84vwq8FT/E34qeF/DdtGZZ9f1i005FHJPmzKh6egJP4V/Tho1hFpVhDaQIEgtY0ijUdFVVwB+QFefkMHzTn5JH6v8ASix0eTL8Gt71JP09xL9fuLdFFFfRn8ihRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUVT13Wrfw9pd1fXcnk2tjC9xM/ZERSzH8ADSbE2krs/Fz/AIKhfERviJ+3B40lV/Ng0aWLSIucgeQgVgP+Bl84/rXT/wDBHf4fjxr+23pl3LErweGdLu9VJK7gHwkCc9junJz7V80eKvFt14+8Tanr16wN7rl5NqM/JI8yZzIwHsC2B7AV+hP/AAQb+Hnl2PxF8XywjM8tpo1s55KhA80w9sl4s/7or85y9vE5kp95N/qfxdwdfO+OKeKkrqVWVX5RvNfikj9FVjCdM9MUtFFfo5/aQUUUUAI8YfGQcimtbqwA5AHocU+igLEclpHNGyOoZG4IPII9Oe1ea/En9jL4UfFcySa/8PfCd9czE77pdPjgujkf89Ywr/8Aj3vXp1FRUpQqK00n66nPicHQxEPZ4iCnHtJJr7mfFnxO/wCCI3wu8W+bL4d1LxH4UuGJZVSYXsAyemyX5sdsbxXzj8Uv+CI3xQ8INLN4Z1jw34xtVOUQs+nXhH/XN90ZP/bWv1horya+Q4Kr9i3pp/wD4HNfCjhnHXbw6pyfWm3H8F7v/kp/P18Vfgt4v+BniJNL8YeHtU8O3soLRLdxFUnUdTG4yrgf7JOMjNcuGGAWDE+38q/bT/go98L9E+J37H/jga3HBu0TTZtVsLhwN9ncwqWRlPbONp9Q5HevxMTO4kDsOM+1fG5vlqwdVQjK6auu5/MXiNwRHhrHwoUqjnTqLmje3MtbNO1l6NJXvtoep/sO6nqel/tg/DSTRnkS9bX7eLbHnLxMSsobH8JjLZ9q/daDABAyRmvyz/4IhfAc+Nvjbrnju8hV7Hwdai0si4GGvLgEFh67IVb6GVa/U2JDGCOCK+o4ZoyhhXN7Seny0P33wNyyrhsgliKj0rTcorySUb/Np/JJjqKKK+jP2YKKKKACiiigAooooAKKKKAEb7p7cV+Of/Bxh4pXUf2l/AOkBtz6X4bknccfKZrhv6R1+xj52HHXFfgh/wAFp/iYvxG/4KKeM4oZRLbeGYLPRI8ZwrRwh5B+EkrA+4ryM6nbD8vdn7x9HTL3X4uVdLSlTnL77Q/9uL//AARA+CI+Mf8AwUA0C+miWSx8C2U/iCcsoK+YuIYBz38yYMO/yE9q/eBUCZx3r8/f+Dfb9m1vhp+zVqvxAvoNuo/EW7VrRmUBlsLcskZB64eRpX9xtPpX6B1plOH9nh03vLU8nx14kjm3FlaFJ3hh0qS9Y3c//J3JfIKKKK9M/HQooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK8L/AOCkHxEPwx/Yy8fagkjx3F3px02EqRnfcMIvUdmPTmvdK+Ff+C7PxH/sb4IeD/CsTETeIdZa9lAPWC1iPHt+9miOf9k1wZpX9lhKk/L89P1PleOMy+oZBi8UnZqEkv8AFL3Y/i0fl0ECInQAda/ZX/gkv8NR4B/Yf8LTvGEufEUs+sy8YJEshCfX92ifga/HLT9KuPEeoW2m2kZkutQlS0gUDJaSRgijH+8RX9Bnw28GW/w5+Huh+HrQKLXQ7CCwiwMArFGqA/jtr5fhWjetOr2Vvv8A+GPwPwByvnzDE49rSEFFes3f8o/ibdNaZEzlgMdaq69rtp4a0qe+v7m2srG1Qyz3FxKIooEAyWZjwAPU1+f37XH/AAX+8B/DG6utH+GOlyfEDWIWaNtRkc22jwtjqr/6yfB/ugL6Oa+vxGKpUVeo7fm/Q/svhrg/OM/r/V8poSqNbtaRj/ik7RXzevQ/QsSqc4YHFO3Dg9jX4keHv2jv20v+CkepTDwZfeIrHRGYgyaCV0LTIAeNpuyQ79D0dmz/AA16j4V/4IH/ABi8dXH27x98bo7S9uE3ObW5v9Ymzno0k0kWfw7+vWuCGZ1Kn8Gk2u70/R/mfouN8JcvylcnEOc0aFTrCEZVZLyajytP5ejZ+sxYDJOQBTfOTgbhzX5lS/8ABGP48/CTTkf4b/tOeIUu4QWW0u7i/sLU4BwMJNMvPTmM14t42/4KL/thfsBeLofDfxJ/s/V2CH7LLrWnR3EF+gXG6K7g2GQAkE5+YEYYDNVPM5Uv49Nx+5/5HNgPCihnE3T4bzSjiZrXkkp0ptLqozi7/fZH7PCVTnDDinV+YP7M/wDwX48SfGP4s+GPBt/8KdOuNT8UajbabBNp2stEsbSMFeRlkjPCjc2Aegxmv0+rsw2Lp103Te3kfEcV8FZvw3Xhh84pqEppuNpRldJ2v7rdte9rhRRRXSfKnyh/wWN+JB8B/sX6tp8U3l3PivULbSUHQshYzSD6bIiPxr8gJXC73bhV5yOOlff/APwXl+JIu/G3gHwhFISNOtbnWblQcgvKwiiz7hY5evZq+T/2Pfggf2if2mvCHhR4vNsr29E9+O32WH95KD7FV2/VhX59nspYjH+yj0tFev8Aw7P4+8WK1XN+L1l2H1ceSlHtzSs/znZ+h+r3/BM34FN8Bv2Q/Ctlc24g1XXYf7b1AFSGEtwA6o2ecpH5akHoVIr6BpIgBGoACgAYA4xS193h6MaVONOOyVj+ssqy6lgMHSwVD4acVFeiVvv7hRRRWx3hRRRQAUUUUAFFFFABRRRQBkePPG2n/DnwVrOv6pMIdP0Oymv7pz/DFEhdj+QNfzqfCb4c+IP+ChH7ZMelwPLHqvxI1651S/m5P2KCSR57iXjp5cZIB6ZCjuBX6q/8F8f2mk+Df7Ja+DbK4Eet/Eq6+wkD70enwgSXLk9gSYo/fzT71jf8EIv2FJPgp8I5/il4jsTbeKvHtoq6bHKhEthpRZXQEH7pnKpIR/dWLPOa8PHQ+s4mNBbR1f8AXp+Z/SHhvmMeD+DcZxRV0r4mXsqC78t/e84qTbl/gS3aPu34f+BtL+G3gfRvD2i2qWWk6DZQ6fZQKTthhiQIi/gqgVs0kalEAJyRS17aSSsj+cqlSU5Oc3dvVt7tvdsKKKKZAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUANmfZGSc8V+T3/Bbf4kHxT+1LpOhI5eHwvosYZN2QstwxkY47HYsY/Kv1guBujx6kCvwn/bc+Iy/Fb9rn4h69FIZbebWZrW2fOQYoD5CEH0IjyPrXzfE9blwyp/zP8ABa/nY/FvHTM/YZDDCp61akU/8Mbyf4qJ0v8AwTQ+F4+Kn7bHgm0kiWa00e5fWbneNw22yb1/8ieXX6w/tVftZ+Dv2OPhPc+LvGWoSW9kjeVaWcGHvNTnIJWCBCQGcgdyFA5JAr4A/wCCSuseHf2evBHxb+NPi+4Fl4f8LWUGkx3AXfJM7EzSxRr3kb/RlAHUydRgmvhL9s79sfxV+238ZLzxb4kme3s4t0Oj6UkpaDRrbIxGueC7AKZH/iYegAHBlmIWEwScfim2/RbX/DQ/a/od+C9biHKfrmJvDDSm5Tl1lb3Ywj5tR5m/sp33av137b//AAUf+I37enixbTUJ7zR/CizBNO8LadO7wOzNhDMF5uZuw3AgEkIBkk/Zn/BOX/ghXa22l2PjP446fHf3F3Gk1l4QZv3NoDyDekffkxj9yCUXoxY5Auf8EN/+Ca8Gj6LZfGzx1p0Uuqaggm8I2U6bhYQkf8fxVh/rXB/dn+FPm4LfL+nMUYiQKCSB69a9PL8vdX/aMTq3sn+v6I/pvxO8UaWUwfCfByVCjSvGc4aNy6xi99PtTvzSeidld0vD3h6y8N6Lb6dYWNpYWFpGsUFrbwrFBAgGAqIoAUAdgMCr2xR/CPypaK99I/mOUnJuUtWxDGp6qPyryb9tP9lDQP2xP2f9a8G6za2zXFxC0ulXrpmTTLxQfKnRh8y4PDY6qzA5BIr1qmy52jHXI/nU1KcZxcJLRnVl+Pr4LE08ZhZONSm1KLW6a1TPwu/4IXfCSXxl/wAFDLCe+tyj+BtMvtQuI3X/AFdwu22A44BDynr/AHfWv3Tr80v+CNngu0/4eBftb6xAqRrpPiKbTrdU4Ajn1XUZGAHp/o6V+lteZk1Plw9+7f4afofrfjxnMsw4od9FClSVu3NBVLf+ThTJTjHJGafWR498W23gLwdqmu3pxZaNZzX0+OuyNC5x74Feq3bVn4vOcYRcpOyR+Mn/AAU4+Jn/AAs79trxxcLIZLbRrtdFhBPyr9nQI4+nmeZ+NfSP/BC34HC4vPGPxGuYA/lKugaaxX1Ky3DD3OIVyPRhxnB/PnW/EE/iXVr/AFa8bzLvU55b+4IyS8kjmRz9SWNfuN+wT8Em/Z9/ZQ8HeHJ4hFqS2S32pDGD9quCZZAfdS2z6IK+FyOk8VjpYiXS7+b2/ryP5T8LsG884ur5zVV4wcqn/b021FfJNtf4T2BBtQDGMCloor7s/q4KKKKACiiigAooooAKKKKACiiigD55/a9/4Jz+Cf2zfi14F8U+LX1OUeCJGzp8TKbTVoWdZDDMp/h3oM7SCQSpyDX0BZ2yWkaxxxrFHGoVVVQqqB0AA4GOlTUVnGlCMnKK1e56GKzbGYnD0cJXqOVOimoRe0VJ8zt6vV/JbJBRRRWh54UUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFJuGSMjIrmvjB8YPDnwG+Huo+KvFur2eh6DpMfmXN3cthVHQKAOWYngKASSQAK/N/4yf8HI1hpnid4/Afw4udX0S2f57zWrz7HNdKOpjhQPszzje2emVHQcuIxtGh/EevbqfYcK8A59xG5f2RhnUjHRyuoxT7c0mlfyTv1sfqNRWB8MPiBa/FP4faD4ksVZbDxDptvqdsG5YRzRrIoPuAwrfrpjJNXR8lUpypzcJqzTs12a3CiiimQFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHKfHL4iRfCb4OeKfE0xATQNLuL4Z/iaONmUfiwA/Gv5/BK8675yXmk+eRifvMTkn8z3r9fP+Cx/wAUB4F/Yw1PTUkRbjxbqFvpSJ3ZNxmk/DbERx6ivyFWSK2UyzAPHFl5B/eVQWI/EAivheKa960aa+yr/f8A8Mj+XfG6vWzHPcHk2DjzzSSjFbudWSSj6vljb1Jf2iv2hZbv4HeD/hRpDNbaNo1xP4g1zy5MjVdRnI8ouPSGDaAP70jHstTf8E3P2RV/bT/ay0PwpexNJ4bsEOr68OQGsoWUNFkdPNd0j+jse1eF3l/Pqt9Nd3LtJcXTtNK3XLMcnHp16fSv2B/4N4fgXH4Q/Zz8TePriBBf+NtU+yQSEZYWlplQASMhWleU8cHap+lZfQdarCnPZLX0S/pH+x+YYDD+F3hlSybLJe/RpwoxktOarJWnU9fimr7WS2R+hmnafFplvHBBDHBbwII4441CpGoGAqgcAAAACrFFFfcH8JNhRRRQAUydDJHheoOafRQJo83+Dn7KPgH9nzxL4m1nwZ4YstC1TxlcC61u4hllkfUJQ8rh38x2Gd08p4xy5r0iiiphCMVyxVl5HTisXXxNV1sTNzk7Xcm29EktXd6JJLskkFfNv/BWL4nj4afsR+JzHKY7rxC8OiW+DgsZn+cf9+lkP4V9IuSFJHBAr85P+C8fxP8ANPw98GQyghZLjXblAehC+RCceuHn6+tednFf2WDqSW9rffofBeIuaLAcOYuvezcHFes/dX3Xv8j5G/Yd+DB+Pn7WPgnw5JF5ti16t9fKckG2gzLID7NsC4/2q/dGBGTduxya/OD/AIIS/BkTat40+IVzEQIVi0DTyVyMnE1wwOPQQLkH++K/SOuHhrDezwvtH9p3+S0/zPlPBPJPqWQfW5q068nL/t1e7H8nL/t4KKKK+hP2AKZJKpJTBLEelE0hRQVwTmvze/4Laf8ABTRvhdolz8H/AAFqZj8VarGB4j1C3fDaPauuRbIwORcSgjJ/gjz/ABOMYYnERo03Un/w59NwhwpjuI80p5XgF70t5PaEVvKXkvxdktWjqP2w/wDgvJ4B/Z48Y3fhrwdolx8RdV0uR47+7gvFtdMtnXIaNZsO0rggg7F2DH3ieK+1vgt8Q1+Lnwg8L+K0sLrS08S6VbaotpcYMtsJolkCNjgkbsZ71/Oj+x/+z9P+05+0x4K8CW8Ttb63qKLelRnyrNP3lwx/7Zq34t71/SVpdlBpGkwWtpFHBbWsYhhiQYWNFGFUAdgABXnZXia9eU5zenReZ+n+M3BeQcLU8FlmWpvEOMpVJuTbktFG8fhV5KVrJWS1uTyzrEVDZy3Svg/9sX/gu38PP2fPGN34Z8H6Re/EXXNOmaG9uLe5S20u0dSQ0fnnc0rg8EIhQd3zxXjX/BZr/gqZf6lq2sfBj4Y6hcxpYLJD4u1mwLCQBflksonXlVXOJn45+TI+bP5cyNHZWrOcRxQqWIAwAACelc+Pzdxk6dB7bv8AyPsvCjwIo5hho5txJF8s1eFJNxbTtaU2rNJrWMU07WbfQ/o1/YH/AGxNN/bj/Z8tPG+n6Xd6JN9ql0++sZ3Egt7iLG4I4xvQhlYHAODggEYr2h2CLkgkCvBP+CYPwP8A+Gf/ANhb4d6HNbi21O70uPVtSQrhluroCeRW903hP+AVrfttftoeGf2Ifgpe+LfEjG5nkc2uk6ZEwE+rXZBKxJnooALO54VVJ64B9elVcaEZ1nrZNn4DnGVUcVxDWy7IKblCVWUaUU+ZtczUbPqra3fTVvdnBf8ABTj/AIKN6R+wb8NLZrWCDWfHXiINHoumO+I0UD57qfByIUOBgcuxCjA3EfKf/BHb9tX4/ftfftga0PFPiuXW/A+m6PPd6paPp9vBaWczyIttHCY41dXLbyAztlI3zk4Nfnb8afjD4x/a9+Od74l1+aXV/FHiq9S3trdCfKg3vtgtIVJO2NchVHXqTkkmv3o/4J5fsY6X+xB+zzpnhS3WC4167QX3iDUEQA318yjfg9TGn3EHZVz1Jz4+GrVcZiedNqEdf+H9fyP3fi/hfJOBeEFgcVShXzDFprmaT5NnJwbvyqCaSas5Sd9k0vdrcFYhnrT6AMAD0or6E/l9BRRRQAUUUUAFFFFABSMwRST0FLXhf/BQj9s/Tf2HP2dNU8W3Sw3es3LjTtB09ut9eyKSgP8AsIoaRz2VCByVBipUjCLnJ6I7sry3E5hi6WBwcHKpUkoxS6t6L0Xd7JavQ9G8f/HzwR8KtX07T/E3izw94fv9XdY7K31C/it5blmOAFVmBOTxnpmutRxIgZTlW5FfzYeOf2xviV8UPFF7rXiPxBYaxq1+SZbm98PaXcy4J4jDyWzMEUfKq7iFAAHFfvD/AME5fipqvxl/Ye+F3iLWpzc6rqOhQi6mKqjTyR5iLkKAAW2ZOABk9B0rzsFmX1ipKFraXX5dz9V8R/CLE8JZdh8bXrRqOpJxko3spWurXitLJ/gz2+iiivUPxwKKKKACiiigApHkCdc0SMVRiMZAr4p/4KMf8FRbz9mjxUfA3guxsb7xakCXF/e3il7bTFcbkQICN8pXDYJAVWB5JxXLi8ZSw1N1arsvz8keLn/EGBybByx2YT5YLTa7beyS6t/8F6Jn2otwrMAM5PtT6/K79mX/AILG/EmP4taJpfjVNJ8SaJq99FYzGCyW1u7UyyKgkjKEK20sMqw5AODnFfqfE5YsCc4rLAZjSxcXKlfTdM4OFeMcu4goTr5e37js1JWavqu616WY6iihuh7V3o+pPgf/AIOINWS3/Yf0SzBIe88X2eBnjCQXLk+/IFfi41nJfqLeAfvrgiKPjPzMcD9TX63/APByHri2/wAG/hhpgkG+7125udmeqx223OPYyCvzD/Zw8Pv4t/aI8B6XGqyPfeIrGHawyrZnTg+3FfH5s+bFSS8vyP728BX9T4Gjipbc1Wf/AIC7f+2n9HfwH8HD4d/BfwhoCoiLoeh2VgAv3R5Vuif+y11tRWyhCVHRRjFS19dCPKlHsfwdWryrVJVZbybb9XqFFFFUZBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRSMwBwWxmgD8xP+C7nxNOrfFLwR4OjceXoenz6tchDkGS4kEceRnqqQsR7SGvhqbwbea98KfHGtQQyPZ+F9MjnupNpKxme4jt4xkdCS7EeyH0r1n/AIKC/FUfGH9srx9qkcvn2tpqDaZasDlfLtf3OR9WVj+Nej6Z8CpdE/4Io/FnxWbeV7vxbqdpcR4BU/ZLO6SNWP8Ash2nP41+dV4/W8dUqdFd/KKsv0P5/wDCmlHiLxrwWIqa0qeKpyfblpTjGPyclH72fnM3CDkAY78V/Qp/wSg8MReFP+Cdnwoto4pYjPoq3jrIcsXmkeVj9CXyPav56pBmMjJAIxn8K/om/wCCZesprX7AHwjuFKAHw3ax4VtwBRdhGfqpr28jX79+j/NH+rH0nJTWRYSK2dbX15JW/U91ooor6o/ichvL6KwjDysqIeNzMFA/Emqcfi/SpnVE1Gwd26KLmMk/rXI/tEfsveCP2rfC9hofj3R313R9OvBfxWhu57eJplVkVn8p1L4DtgEkc5618m/tuf8ABHT4Gx/s4eL9X8J+F08Fa/4c0i41KzvbC6uCjGCNpNksbOyurYIJwG5BzxXJiKteF5Qiml5tP7rfqfVZBluSYydOhj8VUpTnJR92jGcVd2Tb9rGT80oadLn3O+vWUYJa6t1A9ZVH9ajbxRpyqS19ZqBySZ0AH61/LmsxkQEvKCR0Lk/nzX6Ff8E9P+CIVl+1R8CbDx58QPEmuaLp3iNXk0vTNNCCWW23bRNK7g43gHaoH3SDnJwPMoZvWrS5KdO79f8AgH7TxV4E5Tw5g1js3zdwg2oq1C7betklVu9E32VtWfsfHKJE3DJFcH8e/wBqDwF+y94astY8f+JLLwxpuo3f2G2nug7Caby3k2AIGP3UY5xjjryM9xZQC2tFjVVRIxtUKMAADAFfIv8AwU6/4Jv+Jf8AgoPceELaw8bad4W0fwsLiY28+myXT3NxNsXzMrIoAVEwOM/O3PNetiZ1I026avLt/Vj8N4VweV4vNKVDOq7o4d355pNtJJtWSjLVuy2drnXH/gr3+zayH/i6+gkEdfJuf/jdfmN/wUi/az8LftH/ALWGua5ofiGx1Dw9a21vp2mzqSizRpGGZlDAHBkd+teneLv+DdjWfBHhXU9Zv/i7ocVlpFpLeXDnQZBtjjQux/13oK+R/gL+x3dftAfFvw94Q0vWfst34huBCJpLMOtuuCzOwD8hVBY89q+UzavXqxjh665eZ6WW/Tu+59Px/wAE+BeYUqGT5xxBXpOrNOChF80mtLf7rNWvJb21tqfp1+wH+3v+zx+zV+yx4X8Oan8T/D1rrbRtfarGIrhitzM29kJEeCVBVDgkfL3619g/AP8AaX8D/tQeGbvWvAWv2/iPSrK5NpLdQRSJGsoUMVBdVyQCOnrX5nH/AINs/EhBYfFrRD3z/wAI/L/8er7/AP8Agn9+yVF+xH+zRpfgNtSg1i+tbm4vL2/it/s6XcssrNuCEkgBNi8k/dr28u+sx5aU4JRivy+ZtxLwp4fZLklKhwrjqlepDlgoyVkoJO8n+6hd6JaPVu9j2ymS3CxOFIJJ9KZe3aWlrJK8kcUcSl3d2Cqijkkk9AB3r87/ANsL/gqZ4k+PfxHb4KfsvwSeIvGF+z22oeJ7fH2TTYxxI0En3QFJ+a4PyLj5N7EEehiMTCjG8uuyW79D5XhnhXH57iHRwaSjBXnOT5adOK3lOT0S/F7JXOs/4KV/8FS5fgxrA+FPwht5vFHxe1o/Zj9ihF0uhEjptGfMue4jwVQAs5HCt+Ofxd8J+KvBPxO1zTfHNrq1r4vjumm1ZNTYvdyTSgSGSRiSWZw4bOTndX7s/wDBPX/gm34Y/YY8LSXLNH4j+IetoG1vxHcIWmldiGeGBmyyQ78k87pG+Z8kAD8d/wDgp38Srb4pft9fFfWLV0eyg1g6bE6HhxaRJbMwPu0TGvAzSnU5Y1azs29I9l/nsf1Z4FZnlMMyxGSZDR5qVOnzTxEladWfNFK0fsU7OXLF3dld2baf15/wbl/s/jU/GPj74n3kCsmlwReHdMZh/wAtJMTXLj0IUQLkH+NgfWvpX/gsT+3/AHH7Inwjt/C/hG4l/wCFk+OFNvp32f55tMtidj3IA58wsRHEOpdiw+4at/sInw//AME/v+CU/h7xV4rlXT7QaOfFWpbcCWea7AljhUdTIyvFGAe/pivkz/gmH8Otd/4KS/t/eJfj58QLcS6L4WuVntLZzvgS8PFpaRg/8sreIF29X8s87mrog5UqEMLS+Of4J6t/d+R8JmEcNnfE+Z8Y5uubAYGXLFParOHu06a7qUvflurS1VpaeueD/wBirSP+Cef/AASc+KGueJNNs7v4heLfC07a9cyATSxSTptislc9VjeQFiDhpNzc4U1+VnwO8C/8LK+Mvg3w2UEq65rVnYuvUMrzIrD/AL5zX6/f8HCHxPj8FfsU6Z4bglK3HjTxDb2pTJw8ECPPJ+TJEPTmvgD/AIIyfByf4x/8FBPCDLGWsvCMU/iO9cgEIsIWOMc9zNNEAPTJ7Vx46jH28MPTWyS+b/4e5+j+GOe4uPCOacWZnO9SrKpO/T3IJRUeyUrxiuyS6H7x3mq2nhfSJ7m7eKzsrGJpZZGISKCJFJLEnooUE+wFfz8/8FLP22rz9uH9pTUNZt5riPwZoJfT/Ddo/AFuG+a5K9nmI3nuF2L2r7b/AOC8n7fbeGtBX4H+Fb0rqWrRLceK7iNuYLVuY7MHs0vDP/0zAH8ZFflAF2oAAPTOOavOcZzy9hDZb+v/AAPzPP8Ao8+HbweHfE2Pj+8qq1JPeMHvP1nsn/LrqpH19/wRK/ZxT49ftvafql5BHNo3w8tTr9wHXKvc7vLtU6Yz5hMnPaE1+7o9cc18J/8ABA79n1Phh+xpJ4uuIAmpfEbUHv8Afj5jaQkwwL64OJHH/XSvuwcAD0r1cpoezw6b3lr/AJH4j44cTPN+K68YO9PD/uo/9ut87+c3L5JBRRRXpn5CFFFFABRSGRQQCwz0oLgZBIBFFwFopFcN0INLRcBssoiAJBOfSvxH/wCC9n7Rc/xW/bMPgy3uZH0b4cWUdt5WSYze3CJLM+OmQjQpnqCCOOtftvP0Uc5Jr+ab9qzxhJ8Q/wBqT4ma5KzM+p+KtSlycj5RdSKo+gUAfhXh55VcaUYLq/yP6N+jVktPE5/XzCor+wp+75Sm7X/8BUl8zgHfyssByBmv6Iv+CZnhKTwZ+wJ8H7KUKG/4Rq1uTyTjzl87H1/eV/PV4e8MXXjbxLpui2SNJeazdw2ECDqzyyLGo/NhX9Ovw88IQeAfAOhaFbALb6Jp8FhEAABtijVBwPZa5cig3UlPsrff/wAMfcfSgzGMcFgcBfWU5z+UUor7+Z/czZooor6Y/joKKKKACiiigBJFLIQOp/CvkP8Abd/4JQaV+1V4+n8YaN4gPhnxLexxx3wlt/tNpfeWgRHZcgo4RVXIOCFHHevr2iufFYSliIezrK6PIzvIsDm+GeDzGmpwbva7Vmtmmmmn5pnxl+yN/wAEePD3wE8b2PinxVrZ8Ya3pUgnsLdbc29jaTD7spUszSMp5XccA84Jwa+yoYjHnPOffNPopYXB0cND2dGNl/XUnI+H8vyjD/VsupKnG93a7bfdtttv1b00Cg8gj1ooJwCT0rpPZPyW/wCDkXxOZvHvwo0QAhbax1C/JyOfMkij/wDadfIn/BMPwoPGv/BQv4R6eV3ga8LtlxnIt4Jrg/kIv0r2n/gv/wCNV8R/t4QaWjMR4e8N2cDAnI3StLMSPThl/KqX/BBL4YTeOP2/7XXNo+yeCNDvdQd8fdlnUWkY/FZpTx/dr5CsvaY5pdZJfdZP8j+8eHZrKfCb2s3b/Zqsl61Odx+9zR+5H4UUUV9efwcFFFGcdeKACik8xeeRxWBN8VPDNvqyWD+JNBS+f7ts1/EJT/wHdntUuSW7JnOMfidjoKKYk6su7cMHke4p9UVcKKKKACiiigAooooAKKKKACuL/aF+KMfwU+DHijxZKEYaBpk13GrDKvIqny1P1cqPxrtCcda+MP8Agt38W18G/sw6Z4Zt5Qt7401VIXQdTa2482Vvpv8AIX/tpXJj8R7DDzq9l+PT8TweKM3WV5TiMe3Zwg2v8W0V85NI/KPTrS71W6trWFZru/vXWJNzbpZ5nIUZPdmZvxJr9v8AVv2TtP1H9iGf4NqYY7ebwudEEpyI/tBi/wBccc8zHefWvzG/4JafBj/hcv7Z/hoywmaw8LhteusrlT5PEQP1maP/AL5PpX7QQQAKSwOWOa+c4ZwadKpVntL3fl1/ryPyL6PeXVsHSrZ7F2m5xUJdVye82vWTXzify5atot94Z1a80vVLaWy1TTJ5LO8tpMrJbzxsUkRh2KspFfsp/wAEAf2kLT4i/srXfw+uLlBrfw8vXCQNtDPY3LtLE6jqQJDKpOOML6ivLP8Agtb/AMEv9V13xBe/Gj4d6ZNfPcR7/FWkW0RedmUYF9Co5f5eJUHPyhxnL1+eX7M37THiv9k74uab438F3sVrqtipilhmy1tqEDFTJbzKOWjbA6cqQCCCtRRc8Dilzrb8V3X9b6H+umcwwXifwZ/wnyUayakk38FWO8ZdUmm4p7NNS6NH9L1FfLn7Gf8AwVj+Ff7Xmk28A1q28IeLWUC40DWLhIZQ/fyJDhJ09CvzdMqM19P21zHdQJLFJHLHINyurAqw9QR1r6ulXp1I80HdH8OZzkWYZTiZYPMqMqVSPSSt809mn0abT6Mkr5v/AOCsXxgX4OfsCfEbUFlNvdajYf2PasGAYy3TiEAcHnDMcegr0v4yftX/AA2+AulPe+L/AB14Z8PwoCdtzfJ5rkHGFjBLuc8YVSc8V+SP/BXb/gqZ4c/ba0HRvBPgS21dfDGh6t/al1qV6ggOqTLDJFGqQ/fWNfNdsvgk7flGM1x5jjKdOjKKkuZq1up9/wCFfAmZ5znuFrLDy+rwnGU5tNQtF8zV3o27WsrvXY+KfBfgW8+I/jPRfDOmqWv/ABBfQaXbDOPnmkWMHgHABYHpxiv6b/AHgWx+G/g3R9B0uGO30zQ7GGwtY1GAkUUaoo/JRX4X/wDBFb4MN8Xf+CgHhi6eMyWXgu3n8QXBIyqtGojhz7+bKpH+6a/elTlQeeRXFkVK0ZVH10+7/h/wP0j6Teee2zPCZTB6UoOb/wAU3ZX81GN15S8xaKKCwGMnGa98/mI+aP8AgrN8Tz8M/wBirxRHFI0V14mkh0OAq21j5zZkAP8A1ySSvkX/AIIcfB4+KPj74l8ZTxK9t4T0sWduWHC3V033h7rDHIP+2tdX/wAF4vip9s8S+AvBEEoK2kM+uXa5IG5z5MGfcBZz/wACFe9/8EdfhMPh3+xnpmpTRFLzxjdzavJnr5Rby4fzjjDf8Cr5aS+sZwluqa/Hf82vuPwnEU1nXiPGG8MHC77c1rr5qU1/4D5Hrn7RX7Yvw4/ZK8PQX/xA8U6foCXWRbQNulurvH/PKGMNI49wMD1r44+I3/Bx58LdG83/AIRzwR4510RZAnuxbafA59iZHkAPHVAea9I/4KAf8Ed9B/bp+Llt40fxjrXhbWIrOPT50S1S9tpooyxQqrFTG2WOcEg9cZGaufsmf8EXPhF+y/4gtNeu7S98eeJbMhoL7XVWSG0f+/FbgeWrDszbmHYg16lf69Oo4U7Rj30f9elj+1sgp+HWDyiGLzWVbE4prWlG8Ip9uay0/vKbfXlWx4/pnhD9o7/grppUb+KbhvgV8Eb3DNptoGOseJIzyobcA3lY6l9iEkYjkAyv2T+y1+xL8Ov2NvB76P4C0CDS/tIH2y/lYz3+oMBw0szfMR3CDCLn5VAr1aOFQFJXDAU+uqhg4wfPJ80u7/TsfH57xni8dR+oYaMcPhE7qjTuo+s38VSX96bfkktDgf2pfjHB+z3+zp4z8bXBIXwzpM97GAAd0qoRGvPrIVH41/NHqN1LrZuLi+L3U987zXR3YadnYtIc8YLEn05Nftb/AMHAHxbbwL+w/BoEVwUufG2u21iVDYLQxBriT8MxoD/vV+Ka4bOc4Jr5/PKvNWUOiX5/0j+rPo0ZIsPkeIzNr3q9Syf92mrK3/b0pfcfaH/BUr/goja/ttXfgz4efDKHWB4I0UW4jtprdrabV9RYCKFfLPzbIlOxQcZeRjg7VNfq5+wP+yfa/sb/ALK/hnwZGIX1OC3+2a3OnIur+XDTMD/dU/Iv+wi+9fl1/wAEHv2QT8bv2k7n4hatZ+b4c+GoWS3DLlLrVJB+6A9fKTdIfRmjz1r9rVYfZyCwBAOc8Y+tehlFOU28TU3ei9D8n8bcxwOVww/BWT3VHD+/U1u5VJarmdldqLu9PtJWXKkvx4/4OK/iyPE/7RHgfwZDPvg8LaNLfzxA8JPdyLjPv5cCfgx9a6v/AIIez+HP2Zv2UfjD8cfEpijtrSdLAS5xK0Num/yU4+9LNMigdyFr4Z/bc+OQ/aQ/a5+IXjKOUzWOqazPFpzc82ULGG3PtujjVuP71cK/xK8QJ8NX8IHW9SXwk1//AGq+k/aGFm10E2eeydCwXgZ6YB6gGvHeMtiXXSvvb8kf0JhfDmriOBsHww6ns01TdXu1ze0qRXm5Oyb001J/it8UdW+N3xS8SeMdenM+seJ9Rm1K6bnajSOWEa+iIu1FHZUFc5fGX7JIIVDzFSIx6sRgD86+x/ip/wAE6NM+CX/BKnR/iz4iN7bePfE2r2V3ZwmRglvp9yrCO3aPON7JiYkglTheMEV8zfAfwh/wsP44+CtBMZlXWddsrRkzjcrzoGH5ZrkrUpxaUt2r/efeZHxHlmLwNatl38LDSlT2tH90lfl7xS0T62P6Of2avh7D8JP2ePAvha3iMMXhzQLHTghxuBit40JbHViQST3JJrtqitECRlQCApwM+3FS195CHKlHsf5g4nETr1Z16m8m2/Vu7CiikeRYwCzBQfWqMQd9iFiMgVzfxQ+L/hv4K+D7nxB4q1ez0PR7QhXubl8AsQcIoHzO5wcKoLHHSvFv24/+CiPhj9jvS209QniDxpfR77PR4nwsCnpNcOP9XHnoPvN2GMsPij9nb4P/ABB/4K3fGW88SfELXtQi8F6BJtn+zDy4InOCLOzj+4hK8vIQWAxkkkY8jGZqoVFh6C56j6dF6vy7H5/xBx3Tw2MWT5VD2+MltFP3Ydb1JdElq0tbb2umaf7Wn/BZnxV8Rrq50n4YrP4P0MkqNUmjR9Uu0ORuUEMsCn2zIMjlT0+WdC/aV+IvhXxcNfsPHni6DWy/mG8bVZpmkPXDiRmV1z1VwRjIxX7A6V/wTc+CGleEv7JHwz8O3MTRqrzXMLT3bkDr57HzA3uGFfnJ/wAFJ/2DP+GQPG9lqehG7uPBHiWR1svPcvLp9wAWa2dv4htyyMeSqsDkrmvm82wOPhH6zWnzJdm9PRfqj8V8QeGOMMNS/tnG4v2ii1dU5SiqfZqOisnZcy97q+5+j3/BPn9qV/2tv2etO8RXyQw+ILCRtO1iOIbY/tMYH7xV7LIpVwOxYjtXulfmP/wQf+JLad8VPHfhCWQiHU9Nh1iFScgPBJ5UhHbJWZP++RX6bqwYZBBFfUZRi3iMLGpJ67P5f8A/dvD3Pamb5Bh8ZWd52cZPvKLcW/V2u/UZcuI1DHAC5P6V/L541vm1Dxrrt0xJa51K6mJz13TO39a/pw8eaiukeDdVu3O1LWzmmJ6Y2xsf6V/L68gmLuOjsW59zmvPz56wXr+h/dH0W6PvZlVf/Tpf+nGfV3/BFv4Av8d/28fD15NAJdH8Bwv4jvS3ADp+7tl/3jM6sB6RN71+9MX+rXnPHWvhn/ggj+zNH8Jf2QW8aXtuE1j4mzjUQzLh1sY9yWy+uGy8gHpIDX3P8sadgorvyjD+zw6b3lr/AJH5X448Uf2zxTVjTd6eHXso+sW+d/Obav2SForzn4//ALVnw8/Zi0aLUPHfjLRPDNvOP3KXU+Z7n2jiXMkh46Kpr4z+L3/BxV8NfDDS2/gvwr4p8XzJ8qXNyqaZaufUb90hH1QGuutjKNLSpJJ9uv3HxfD/AALxBnaUsrwk6kf5rWh/4HK0fxP0Sor8jR/wcm+LINXEs/wu0D+yVIMkcerTfadvfDlNm7HTK4r9UPhX48i+J/w80DxJBbXlnb+INNt9Sit7pNk0CzRrIEcdmAYAj1qcNjqNe6pvbyOnizw9z3hqFKpnFHkVS6i1KMtVa6fK3Z6rc6KiikeRYxliAK6z4ob53zlSpHPHvSxTCXJHIHfqK/OX/grf/wAFB9U0bxRdfCzwJqs+mtZgf8JFqVnMY597KCLSOReUABBcgg5IXjDV2n/BCbVtT1L4KeN0vLy9ubOHXE+zJPK0ixM0CmQrknG44JHrz3ryIZxTnjPqlNXte780tvP/ADPgMN4hYLE8Rvh3DQcpRUuaafuqUVdq3Xs3fR6WZ90UUUV659+FB6Gisfx34ut/AnhDV9au3VLTR7Ka9lLNtULGjOcnsML1pOVldlQhKUlGKu3oj+fT/gp/8QW+Jv8AwUI+LWqbo3jt9cOlQ7H3rstIo7Xg+5iYn0JNfff/AAbm/CIaR8FvHXjmWNRJ4g1dNLt3IOTFbIGbHsXl/Na/Jzxn4rn8aeLta167Yvc61fXGozHgZeaVpG6Y5y56d81/Qj/wTH+Cv/Ch/wBhH4aaDNCsV+2krqV78u1jcXTNcybvcGXbz0Cgdq+VyqDq4p1H0u/v/wCHP7W8csVHJeBsLkdN2c/Z07f3aUU3+MYfee9UUUV9WfxOFcd8a/jj4d+AXw41LxR4qvl07SNMUF3xvkmcnCRRr1eRjwFH6AE1t+NPFdh4F8OX2s6rew6fpel273V3cSttSGNAWZifYD8a/KbWvjwP+Cn/APwUA8E6Br09xpvw4fUZYdO0t5Cm6GOCWUtJyMTXBjVCRyquFBznPmZjmKw6jCKvObsl5vS78j4zjDiyGUxpYWglLE15KFOLdleTS5pdoptX6vZdWs79oj9tb40ftzapfW3gzS/FmneCYpGSPTdCglZ5lB63U8Yy7cf6sMEGehIyflvxB4bvPDWty2Oq6fe6bqUODJBd2zQzx56EhwG59e9f0H+EfC+leCfD1rpWj2FlpWmWEYht7S0iEMMCAYCqq4AFfLH/AAWU8CeFNW/Y41bxDqltYL4j0a7sxot4wVbkyyXUSPCp6srRGQlemF3fw5rwcyyOo6UsRUq80kr67ei108j8k428KsXVwNbOMbj5Va1OMptSVoWSbcYq/u+XTyV7nyX/AMEz/wDgoH4m+EvxU0DwR4m1W91nwRr88enxJeSmaTRJpG2xPE7nd5ZcqrISVUHcoBBB/XCvxW/4Jufsqar+0p+0TpF0tpPF4U8K3kWoavfYKxgxsHjtlPeSR1XIH3U3E9gf2prv4bnXlh37TWKfu/r8v+CfV+CWKzKvkcpY5uUFK1Ny35bK6Terint80tEFFFFfRH7GFFFFABRRRQAUUUUANnJELEHGOa/JD/gtD8Xf+E7/AGrovD0Exe18FaalrjslxPiaUj32+SP+A1+suvazaaDot7fXsqw2dhA9xcSN92ONFLMT9ACa/n/+KvxD1H43fFfxD4mlhlutR8U6pLeRwqCzM0r/ALuFfXA2IPoK+Y4oxHLQjRW8n+C/4LR+G+O+cewyijl0H71ad2l1jDX/ANKcbeh+jf8AwQs+DZ0D4T+KPHV1CouPE9+un2TEcra2wO4j2aWRgfXy1r7xrgv2avgxD8Afgb4T8IQCMnQdOigndBgTT7cyv/wKQsfyrva9rLsN7DDQpdlr6vV/ifpvB+S/2Tk2Hy9rWEVzf4nrL/yZsZPEZVADbSDnOM18k/tTf8EXfgz+0rq93rkWnXXgrxJduZZ7/QGEEd02OsluQYmbvuVVYnqTX1zRXRWoU6q5aiufe5JxBmWT4j61ldeVKe14tq67NbNeTTR+R3jX/g268SpMD4c+KGhXcXmEhdU0yWFlTPHMbMC2PYCrfhH/AIIAfFiCQW2q/G6zsNPiTai6ab6Q8fw7GdFA6dPev1l8tQchVB+lBjUjBVcH2rz3kuFvez+8/SX48cZSpeyniIu3V06bf/pNvwPz7+EP/Bvb8KfC15FfeONc8T+Prw4Z4ZLg2FtIf4g3lnzSCc9JASOua/NT/gohrPhW9/bD8Zab4H0fSdC8IeFbhdA021063SGHbbL5csny8uzTeadxJLAA57V+7H7bn7QVt+yx+y94x8cTPGk+jadJ9hQkAzXcmI4EHrmRl4HYGv5zPD3hnW/iV4qsdH0uCXVPEfiC8W2to8ktcXUz4GT15dsk9hk15WbUqNHlo0opdX38v1P3HwGzXO87rYvP88xUp06aVOKk7QTdpTkoq0U4pRV0lpJn60f8G6/7PR8NfBXxn8SbyALeeL9QTS7BmUbks7QHcQf9ueR84yD5SelfpGOAB6Vwf7MvwP079nD4CeFfAumlZbXwzp8VmZQu37RIBmSXHYu5Zse9d5X0GCoexoRpvdfm9z+W+POJHn2f4rNb+7Ul7v8Agj7sP/JUvmFMnG4KAMnPFPrkvj18TLf4M/BnxP4ruSvl6Bp016oPRnVCUX8W2j8a6JzUYuT2R8ZXrwo05VajtGKbb7Jav8D8fP29fFl1+0z+3t4lstLc3Lz6vF4X00j7o8pxbgjHVTIXOfxr9lPh34Otfh34M0jw/YII9P0Kwg0+3UDGEijCL+gFfkN/wSc+Fsvxe/bZ0K+vQbpPDMU/iC8kf5i8w+VGOe5mlVs+1fsoFCkkAAmvneHouoqmKnvJ/wDB/W3yPxjwboTxUMdn9Ze9iKrt6K70+cmv+3RaKKK+kP2wKKKKAPyB/wCDjf4ntrHxu+Hfg6OYNBoWkXGqzIO0tzKsak+4SA/99mvzpstPu9Yv7eysbaW9vr6Vbe2t4uZJ5XIVEX3ZiAPc19af8FxptTuv+CjviSO9t7pIjp+nRaaDGxE8Xkg5T1/eFxx3Br27/gjX/wAEsvFM/wAVNJ+LnxG0WbRNC0RftOgaVfRmO71C6IxHcyRnmOKMEsobDMxU4AXn4+vSniMXKCWt/uS0uf3/AMM8Q5bwh4e4PF4mcf4XPGN1ec53nyrq/elZtfCtXax+hH/BP/8AZVtf2N/2X/DXgpEgbVoLcXet3EWSt1qEgDTsCeqhvkXp8qLVL/gpP8bn/Z9/Ym+IviSCYwXyaW1jZsuNwuLkiBCPcGTP4V7ssaqSQoBPtXwJ/wAHFGvT6d+xZ4fsYlYQ6p4wtI52GQNqW1zIAfq6qfwr6LFNUMLJQ6Ky/I/j3hGnU4h4vw39oS5pV6ylN97y5pferryPxgij8pQPQepNe1/8E+P2Vbj9sj9q3w34PNuZ9Fgf+1dfbnZDp8LL5m4j/noxSIDuZPQE14/oeh3/AIr12x0rSrG71PU9TmS2tLS2jMk1zK5AVEUdWJPSv3m/4JUfsAR/sN/AlBq0NvP488ViO71+4QhxbEAmOzRu6RBjkjhnLHpivmcvwjr1FF7Lf/L5n9s+L/iBS4ayWfspf7TWTjTS3V9HPyUE7p9ZWW12vGv+DijX4tF/Y/8ABmixIsI1HxXCyKowFSC0uDtHt8w/Kvy5/ZB1qLw7+1f8Mr6c7YbXxPp7ufQeeg/rX6Nf8HKktyvgT4RwrG4sW1W/d3HK+aLdAgx/e2mQj6GvyihvrnS54bqykMV5aSLPbvnG2RCGQ8dPmAq80lbFSfa35I8HwNy5VeA4UU/4zrXfrJw/KKP6losfNg5yadXD/s4fGGw+PnwG8H+NtOcG18VaTbaiqggmJ5I1LxnHRkfcpHYqa7ivr4yUkpLqfwbicPUw9WVCsrSg2muzTs194Vynxx+I9t8H/hJ4i8VXaLLD4f0+a+MZ6SFEJVfxbA/GurrwH/gqJdXFp+wh8Q2tiwd7KONsddjTxhv0JrHF1XToTqLdJv8AA8bOsXPC5fXxMN4QlJeqi2vyPxi8cePNW+JHizVfEWuXcuoa1rFy97eXEp3GSRzuIHoo4VQOFVVAAAFftt/wT8+Fdh8IP2PvAOmWSASXmkQapePjDTXNygmlY9z8z7RnoqqO1fhs0ZdZFHAZSBjp7fpX7nfsFfEex+J/7H/w81WzlV9miW1hcKG3GK4t0EEqH3Dxt+BB6EGvj+F2niJuW9v11P5u8BqlOrmuLq1neq4Jpvdpy99/N8tz2Cvnn/gqT8O7b4j/ALEvjaOeOJrjRII9Ys5H6wzQSBsg9iULp7hyOhr6FMiqgYnANfPX/BUP4kWHw4/Yq8bSXcsaz65bLo1lGzYa4nnbAVR3KoHkP+zGT0Br6zMLfVqnNtyv8j+heKnR/sbF/WPg9nO9+3K/6Xmfnd/wSM8RPoP7d/hZELhNUs72zcA43K0G8Z9t0an8K/ZSA/ux1zX5Tf8ABEb4H3ni/wDaK1LxvLDINH8H2EltFKRxLeXACqg/3YhIxxyNyetfqyi7IzwR3ryuGoSjhG3s27fgvzR+f+COGrUuGlOqrKdSUo+mkfzTOA/az8UL4K/Zj+IOrMCRp/hy/mznGCLd8frX8ztw7Q6HIwzuWDP4ha/b/wD4Lo/tb6d8Ev2UbrwFbTibxT8SkawhgVvmtbFWBuLhvQHiNc8szkjhWx+I80Ant5Iz0kBX8DxXNndRSrKK6L+v0P8ATz6M+TVsLkmIzCtGyr1Fy36xgrXXlzOS9Uz+mn9nTwra+BvgN4K0WyCLZ6ToFhZwhRhdkdtGoI/AVB+0z8Yk+AX7P3jLxnJAlx/wjWk3F8kTdJXRDsU9OC+B1rkv+CevxdtfjV+xV8MvEMM4lluPD1nbXR3bil1BEIZ0PuJY3/SvKv8AguD48XwZ/wAE4vHESyIk+v3Wn6RGrEgyebdxFwPfy1c/hXvTrcuFdWP8t192h/KeW5JUxfFVPKcUm5TxCpz761OWX6n4h/Ej4n+J/j98R7vxN4o1PUPEfifX7jdLNLmWSR3YBYokHCICwVI0AUDAAHGfqn9nP/ghl8bfjjY22pa9DpHw70acBwdYkaTUZVzwVtYwduf+mrof9k16l/wQK/Yifx58Qb340eILESaP4Zd7Hw5HKmVuL4jEtyAe0KHYp/vuTwUFfr4kShRlRkjmvFy/K1Wh7WtfXZfqz+lfFTxrxGQ43+wOG4Qj7KKUpWTUX/JCOkVyqyd01e8bK2vw3+yv/wAEHfhT8D9astb8V3F98RNbsHWaGPUVEWmwyjBD/ZlJEmD0EpZf9nOCPuG3tBbYC7QoGAAMACpQAowAAKK9+hh6dGPLTVj+V+IOJ81zyusTm1eVWS0V3ol2ilZRXdJJATgE+leS/to/tK2X7K37Pms+LrgxvfwqLTSbdsE3d7ICIkAPUDBdvREY9q9Tvr+Cxt5pJ5Y4YoULySSHakagZLEngAAZzX4w/wDBR/8AbJm/az+N7x6ZcTHwV4Yd7TRo93y3bDiS7I9ZCML6IF6EkVwZxmKwtBtP3nov8/kfkXiNxjDh/KpVYP8AfVLxprz6y9I7+bsup4Druu3vijWb7U9SuJL3UNSuJLy6uZW3PPNIxd3J9WZiSfev1e/4IgaEdM/Y7vLtgu7U/EV3IDjBIRIo/wCaNX5McblKg+3bFfq//wAESfilpniH9la78MpPGureFtVnM8G4bjDcN5sUmO4JLLn1Q18pw419d1fRn8++CNWD4mcq8vedOdr7uV4t+rspP7z7NooVgwBHINFfoJ/YIV8xf8Fhvi03we/4J6/EW9gnaC+1e1h0O028Mz3c6QNj6RvI30U19OswUEnOBX5nf8HIXxUFl8Mvh14Jhl+fVtUm1i4jB+8kERjTPtumbr3FcWY1eTDzflb79D7zwwyf+0+K8BhWrr2kZNf3Ye+181Gx+Yn7Pnw0k+MHx48EeEoI/M/4SDW7OwZfWN5lD/hs3fhX9M2nWkdhYw28KLHDboI40XoqqMAD8BX4I/8ABF3wbH4z/wCCkHgHzo1kh0eK/wBUYH+Ex2siofqHdTX742+fKGR0rzsipWhKfd2+7/hz9b+k5mjqZ1hMAnpTpOXznJp/hBfePpJH2IWxnFLTZxuiYc8+le8fzQfn1/wXE/aVuNI0DQPhfply0Q1pf7V1tVI/eW6PiCFu+1pAXI7+UvYkH84dH1m80HWLPUNPurmx1CxmFxb3EDlJYJFOVdWHRgQCDXtn/BSrx7L8Qv24PiHcs7GHS7xNIgQtkRrbRLEQPq4dvq1dp/wTw/4JuN+2XpuoeI9b1yfRvC2l3n2HZZorXl/MIw7AM2VjRQ6jdhiTnAHWvzfGe2x2PlGnq7tLyS/L/Nn8X8UxzLini+vh8AuaUZOMNbKMabte/RXvK/d6XdjX8If8Fr/i14X8NwWOo2fhPXbyNREl/dWkkU07di6xsELf7oXJ7V13wr/Zj+NH/BTvxfp3iz4tavqGgeArNy9tbeSLNpF7raW2PlB6NPLlsHAL4wPtr4D/ALBXwr/Z1Mc/h3wfpzanGuP7U1Afbb8nuRJJnZnuECg4HHFeyCJRj5VBHtX0uHyevNJY2q5JfZT0+b6n7jk/h5m2IpwhxPj5V6cbfuotqDa255aSml2aWvVo5j4TfB/w58E/Alj4b8KaVaaJoumqVhtoFPJPJd2OWd2PLOxLMTkkmupoVQowAAKK+hjFRSjFWSP1ulRhSgqdKKjFKyS0SS2SXRBRRRVGgUUUUAFFFFABRRSMwXBINAHzP/wVj+MQ+FH7GXiSCKbydQ8Vumg2uDhv3vMp/CJZOfcV+ef/AAS3+CY+M37ZXh5J4lk07wqja/eAjK4hKiIfXznj49jXrf8AwXK+Mv8Awk/xu8NeCrWUNa+FLB766UHG66uiAoPukUYI/wCupre/4IOa5odl4k+JGmzyRR+JLyCxnt0f789pGZhJs9QskiFsf31NfGYqpHE5vCm37sXb1a1/PT5H80Z7iqOeeI2HwVWS9nQaj5OUE6jXq5e611sfpWmdi564pajjuFKqCSCRUlfZn9LJlLxDr9n4X0efUNQu7SwsbVS89xcyrFFCo/iZmIAHuay/AnxO0L4naIup+HNb0jX9Ndii3WnXSXMW4dV3ISMj0r47/wCC9vwt8ZfEL9iuCfwyby40vQdYjv8AX7C1BMl1aCN1DkDlkikZXK8jHJHy1+UP7GP7ZvjD9iP4qweJ/CFyJ7O4UJqejSzMljrMHXa4GQrryUlA3KSeqswPkYrNPYV/Zyj7umvX5eh+48E+DM+JuHKmbYHFL26k0qbWnu20lK/uykneLtbVX3bX9IdJIxSNiOwrwD9mT/gpV8Hv2m/B0Wq6V4y0jSL4Rg3mkazdRWV9YOeqOjsAwB4DoWU+teV/t6/8FlPh1+zV4Pv7DwVrWkeOvH0wMVtaWE4uLLTmx/rbmZMoAp58sMXY8YAyR2zxtCNP2jkrH53geBc/xeYrKaWEqe2vZpxat5yb0UV1k3Y+Yv8Ag4R/a7Xxf4z8PfB3RrsPbeH2GseIPLf5HumXbbwH12IXkPbMid1NR/8ABAr9hqbxf42l+NfiS0H9j6N5ll4ZRwf9LuzlJrrn+GNcxqe7s56oM+PfsE/8EzfH/wDwUK+JU/jfx1JrGn+Cr+8a/wBW1y9yl74ikc73S2DcncTzLwqjhckYH7e+BfA+lfDfwhpegaFp1rpWi6NbpaWVpboEitokG1VUD0Hfqa8fBYeeJrvFVVp0/T5L8z944/4qwHCXDMOBskqqdZq1acdlzazV/wCab0tvGHuvW1taGLylxuLE9zT6KK+iP5bA5wcV8Z/8FsPi+3g/9lSx8N283l3fjXVo7ZwDgm1hUzSt9N4hX/gdfZMkyodpzk+1fkT/AMFk/j5Z/Ev9pmHQ7C7W503wBYm0kKNlBdyESTgHplQsak9ipHavGz7EeywctbOWi+e/4H5v4r51HL+G66TtKr+7X/b3xfdDmf3Hv3/BCz4UDSPhv4y8bzxATa9qMelWjHkiC2BZsf70spz6+WPSvvuvIf2F/hA/wS/ZL8CaBNbiG+i0yO6vUI2stxMPNkU+6s+3/gNevV1ZZh/Y4WEHvbX1erPe4Iyj+zMiwuDas4wTf+KXvS/8mbCiiiu8+qCiiigDK1HwTpOsajb3l5p1hdXloMQTzWyPLD/usRleeeK0lhwoBYsR3PU0+ikklsU5NpJvYK8w/a3/AGSfCn7aHwdu/BXjBL0adPPFeQT2cvlXNncRk7JY2weQCwIIIIYjHNen0VM4RnFxkrpnRgcbXweIhisLNwqQalGSdmmtU0z5t/Y8/wCCVfwm/Yt1iTWvD2m3mseKZFMY1rWJRc3NuhPKQrgRwg92RQzdCxHFfSMaeWgUcgUtFKlShTjywVkdOb5zjs0xLxmY1pVaj3lJtu3ReSXRLRdDwD/gpP8AsfQfttfsxar4Sj8iHxDaMNT8P3Mp2rBfRhgqseySKzxt6B89hX892uaFe+GNdv8AStUtJ7DU9KupLO8tZl2yW00bFHjYdmDAiv6kMA4JAJFfnr/wVP8A+CNs37Tnia4+InwzbTNN8b3ZUatpt1J9ntdawu0TK4BEdxgKCWG1wMnDAE+Rm2AdX97TV2t13P3nwK8UsPkdSeS5vU5cPUfNCTvaE9E0+qjJddoySbsm2vmP/gkZ/wAFXbT9lOBfhz8RJZz4Eu7oy6bqqIZDoEshy6yqBuNuzfNlcmNmJwVJK/sp4X8VWHjPRrXU9KvbPUtMvoxLbXVrKs0M6HoyuuQw+hr+fLxr/wAEt/2hfATyfbvhL4ouY0JHmaaIdRRsdwIHc4+oFZvwv1X9or9mqSWy8IR/F3wctyf3lnZ2N5FDIR38ooULZ7hc9s9c8OEzKrh4qnVjddOjX37n6Fx74UZBxTipZtkGPowrTd5pSjKE31l7rbjJ9dGpb2Tu3/RjXnv7V/w1n+Mf7OHjXwvarvu9Z0i4gt1/vS7NyD8WAFfkR8Hrn9vn436hFDoWrfFm3gbre6u6aZaIO5Lzqu7HooY+1foZ+xZ+zN+0b8O/ENnrHxb+OK+KrKKJw/hy00yB4XdhhS920aSkoeflAye5FepTxrxK5FTdno3pbU/nnjTwuw+VYSrSxeaYacnFr2cJTlN3VrWjBqLfTmcV5n5ADKRkMpR14KnqpHBB+h4r6Z/4J6f8FELr9jrV7vR9atb3WfA+rS+fPbWxU3OnXGADPEGIDBlADISM4BBzwfWP+CkH/BLnxJa/EbVPHnw20eXW9L1yd73U9Gs1BurG4clpJYk/5aRuxLFV+ZWY4BB4+OrL4BeO9V1ZbG28D+L5r7dtEC6PcmTOcdNnH418PKhisBiPdTTWztdNfrfsf5qVMo4h4Tzrmw9OSqQb5ZKLlGcX6JpqS3W68mrn6t33/BYf4EWnhhb2LxNqt5clFYafDod2LrJ/g+dFj3Dv8+Pc18j+NPEHxG/4LJ/HW3sdFsX8NfDzww5Pm3OJINODAZlkZeJbp1wFjQkKCeQu9jH+y7/wRq8efE/UbPUfiDIPBHh9Ssj2gdZdVuhkHaFUlYMjPLEsM/c4r9O/hF8IvD3wO8BWPhnwxpNpo2i6cu2G3hXqcfM7seXdjyzsSzHkmvpsPRxuPiljPcp9krOX5tL7j91yvA8T8WUox4igsPhNG4RTjOrbVKV5SlGN9X8LfRdVnfs/fs/+Hf2a/hhpfhTwxbvBp2nIS0kh3TXkzffnlb+J3PJPQcAAAADt2+6cnAoor6WEIwioRVkj9ow+HpUKUaNGKjGKSSSsklokl2SPxB/4KB/siftOftJ/tHeMfHus/C/xC+lRTy2+lQW9zbXP2PTYWYQKqRyMWJTLttBJZzxXxKysjMjo6OjFWVxtZSDggjsQex5H1r+pSeLegCjBHpxX5Jf8Fq/+CY2taf8AEqf4tfDfw5c6ppuvsD4l03S4DLNZ3fe9WJBuaOX/AJaFASHyxGHY183mOVuEXVg29df8z+y/CLxtp4uvS4fzSlToQUVGlKN4xvFWUZc0payWzury0s20fP8A/wAE6/8Agql4o/YKjvdDn0xPFngfUZ/tTaY8/kTWM5xukgkwQA4A3Iw25AOQc59U+NH7V3jv/guJ8ZvCXwn8JeHR4R8IWV3/AGzqDSSrdS2yxo8bXc8oUKqokrpHGv3pJVznA2/NH7OX/BP34u/tReLLfTfDfg3WLO0dgLnVtUtpLLT7FM8u8jgbiMH5E3McEAdx+3H7A37A/hf9hH4WJo+jhNT1/Uts2t65LEEn1KUDgAc7IVyQiA4HJOWYmowFLEYiPs22qfX/ACT3/wAjv8Us74Q4dxss5wNKFTNZL3bNtQbVvaTim4KSW11zSdnbeR6h8D/hLoXwH+FOheDfDNoljoXhuzjsbSIcttUcu5/idmyzMeWZmJyTXWUAAZwAM0V9RGKilFKyR/GFevUrVJVq0nKUm229W29W2+rbCmTyGJMgEmnnODjrXiX7X37KOu/tUaDaaPafEXX/AAVoyBhfWemxIV1PJ48x8q+0dNm7aepBIGM605xg5QjzPtovzPPx1etSoSqYem6k1tFNRu/WWiXd/cnsfKv/AAVk/wCCiNpcaPefC7wLqsdxLc5h8TalbMDHFFjmzjkHBZv+WhHRcLnJbHjfwf8A+CQPxD+LPwJfxc15p2g6rfRC40fQ75GSW9iwCDI44gLr9wEHjG7bk4+zP2e/+CQXwv8AgX4jtdZvU1Dxlq1jIs1s+rMv2a3cHIcQIAjMDnBfcB1xkZr6uCDA+UcV4EcnqYqrKtj3voknsvX+u5+SU/DrFZ9jamZ8WNarlp0oSdoLu5aXl10um9X0iv54vFXhbUvBPiXUNG1myudN1XSp2t7qzuE2S20gPKsP69DwQSOa639nP9onxN+y78U7PxX4XuES7gUw3NrJk2+o27HLQSqOqnAII5VgCOev6t/t8/8ABOnRf2xdIGp2DWmg+ObFQttqhQ+XeRj/AJYXIXlk/utyyHpkEg/Besf8Ec/jrp160UOheH9QRTgTW+tQqj++JArD8q+exWTYvDVr0U2t01/wOv4H43nnhlxDkWZKrlUJ1IxfNCcFeS7XSu1JddOV/ej7r+BH/BWX4RfFzw3BJqmvw+CtZSNftWn6wDEqNgZMcwHlyJnoQQ2OqqeKufEj/grN8D/hwrJ/wlp8QXa8+RolnLdnHqZMCIfi9fGHw3/4Ih/FbxLOh8Q6p4U8MWpPzATvfTgcZG1FCdv79fU3wF/4I2fC/wCE13b3/iBLrx9qcBDqNUCpYKw7/Zl+Vx7SFh7Zr38Lis3qpRdOMfN3X4XP2TJM68QMbRjTqYSlSfWpUuvnyRle/wAkr9j3X9mz9pPSv2pPhnD4s0LTNe03S7mVoof7VtBbvPt6vHhmDpngMDgkHHSvyH/4OAPHkvir9vdNHMhe38L+GrK3VNuAkkzyzv8AX5TEc1+28FhHY2cVvbwxQQwoI4441CIigcAAcADpgV+PX/Bf/wDZO8R+H/2hoPixp+lXl94X8Q6ZbWWpXdvE0iaddwbo1MpGdqvH5YViAMoRnJxXZmsKn1RJu7TV3b8bdNT+z/o44ilhuKaUcxqR9pKnKMZW5U5vl2Tbs2uayu+yd2eQ/wDBEzxraeCv+CjXg5byRIo9bsr/AEqNmOB5skBeMfVjHtHuwr96LcgxDAxiv5bdF8S3HhvWrPUdMv5LDUtNmS5trmCby5raVCGR1YchgQCDX2Rov/Bfj4/aT4at9Pe88FajcQRhDfz6Tm4m/wBpgkgTJ74UD2rgy3MqdCDhUW7vdf15H7h4zeD+bcS5rSzTKpQ+BQkpNrZyaknZppqVmvJb30/c2hunpivw60z/AIKXftm/tAX8dv4WuvEly0vCx6B4QiZTkZHztC/buWx719w/8ExPg1+1NpPja+8VfHLxxqR0G7snhtvDF/Nb3dzJMzIVmcxrtgCgNhVYklvmAAFetQzONaajTg7Pr0R/P/E3hJisgwU8VmWOw8ZxWlNTk6kn2UeT8dl1a3Pzw/ajgurb9pr4jrdoyXH/AAlGpCRWGGybmTH6YNe8/wDBMT/goJp37Ieo6l4d8WQ3Ung/XrgXn2u2j82TTLgKIzIYx8zxsqqGC5YFAQDk4+i/+CjH/BKrUvjp40vfH3w+lsYvEOoIp1TSLmQQR6jIqhBNFJ91JCoAIbCtjO4HOfgTxr+yR8Uvh1ePBrHw98X2Tocbk06SeNucZDxhkb8DXxtfD4rAYl1YJ6N2drpp9/1P83M2yfiPhXiCpmeGpSa55OMlFyhKMm3aVtrp2abTvqujP21+G37QXg34w6ZFeeF/FOg65BOAY/st2jSH2KEhlPsQDXYNcEAnKjb156V/PpZfC3xXJehbbw14l+1A4Hk6ZOJf0XNegeDv2Pfjf8RFQaX4G8eTRSHCyXCyWsR+rTMij869ijxLVkkvY3fk3/kz9Jy/xozOtFU/7KnOf9xy/L2ba+9n7Wa/8WvDXhOGSXVPEegafHF983F/FFt+uWryH4l/8FQ/gj8LhJHdeOLLVruPgW2jwS37uc4wGjUoD9WFfCHgD/gil8YPGUySa9P4X8MQty32q9N7Oox/diBUn/gYr6D+FP8AwQp8HeHrmC48X+Kta8SlDmSztEWwtpOnylgWkxn0Kn6V1xx2Z1tKVFR85f0mfSUeJ+Nsfb6nlkaKfWrN6esVyy/A+if2Wf24vBn7YTa0PB6a4P7BaIXf9oae1sB5mdu1slWPynIzkccc17LXNfDH4S+Hfgx4RttB8K6Jp2g6Paf6u2tIwik92Y9Xc92YliepNdLXuYeNVU0qzTl1srI/Tsshi4YaEcfKMqtvecU1G/km27LbV672V7IooorY7wooooAKrarqUOk2clzczxW1tbo0s0sjBUjRRlmJPAAAJJ9KsMwRST0FfM3/AAVl+OQ+Dn7H2sW9vM0Op+MJV0G02th8SqzTEd8CJHBP+0PWsMTXVGlKrLZK55mdZpTy7AVsdW+GnFy9bLb5vRH5OfHz4vXHx7+NXinxlciZT4j1CS6gjk4eK3ziFCOxWMID7isHwn4u1XwT4htdY0TU7/SdVsG8y3u7SZoZoG9VZefqOh7g1nxD5ASOe+euaSRyjcYAPFflEqk3Lnk9d7+Z/nxXx1eriJYucn7SUnJtaPmbu3ps7n1B4O/4LF/HTwvGi3WtaD4gWMAf8THSIldsd90Ji5PqQa9I8O/8F4PHVoAuqeB/Ct8Rxut7qe3B/PfXwvJgxAkhSetBkXOQpbPeu+nm+MhtUfz1/O59bhfEnibDq1PGzf8Aial/6Umfo/pf/BenTLmIR6p8NNQ2ucP9m1WNhj6Ogz+dfMnx41D9kj9oHxBc61F4H+J3w51e+kMt0/h42b2cztkljbPLsUk8ny1TODwSa+ecEhNpPIr6h/Yg/wCCYPiH9rvw9J4mvtWXwp4UMjQWl01v59zqTq21zGuQFjUgjex5PABwSOunmGOxklRSU36L89LH6fwD43eJdHMPZ8OYl+2ktbRik4r+b4YuK/vaXdlqzxew/Zh/ZqvblGvviT8VfIRsvE3hK13kdwHEzYPvivob9nfT/wBhb4EX1tqM2meNPGWrW5DxT+ItMmuYbcgn7tsu2H05ZGPHB6592t/+CCfhUJ+++IniaR/VLK3VR+Bz/OsbxP8A8EELTyWfSPiXexyDkfbdJRwfqUcHFd1PA5jTfNGjF/j+cj9zzTxp8e8bh3RxNWEovdRcYX9XCcL/AH7dz1iH/gsp8CbG3iigvfEqxRKERE0GVVRQOABwAPYelOn/AOC0/wAEreNTHd+K58fwpokgI/76Ir8wf2hvgFrn7Mnxd1XwZr7W819pm1lnt2JhuonXKSJnkAg9DyCCO1cSiZIOBtIrmqcRY+EnCSSa8v8Agn864zxs4ow1aeGxFKlGcG004yumnZr4z9YNS/4Lj/CO04t9I8eXn+5p0Cf+hTCuZ1j/AIL0eDIA39n+AvFt0w6faLi2gB/FWevzI2j0pkcRUjIGBWcuI8a+qXyPLreNvE8/hlCPpD/Ns+0P2iP+C1Hjv4maLPpXg3SbXwNb3KlJb9ZftWo7emI2wEiPUbgpI7FetfNn7L3wrf48/tGeDvDMqNdRa1q0QvQ7F2khVvNmLE5zlFbJPXNcGykZIHFfaf8AwQ9+Ep8X/tHeIPFs0O+18J6T5ETsvS5un2qQcYyIo5v++xXPRq1sfi6cazvd/hu9DycpzDM+LeIcJQzSq6l5LR2SUV70rJJJXinsr92fqrbkmPrnB60+mxKUTBAz7U6v0u5/b4UUUUAFBYAgEgE0V4t+3b8OPiP8Y/givhD4Z6xaeGtR8UapZ6fretveSWl1o+is5a+ltGjUsbt4lMMeCuxp9+4bBQB7SWA6kCjI9RXwt8Wvi147/wCCYv7HH7Q/x9+JJ0/xF4hW+J8JeG7TUp59O0jS4mSy0bTyzgYcvIZriRBlnmf5mCrj074Y/sQ6749+Eulaj8Ufi58Wda+IGqWUd1qOoeHvFl74b07T53UO0VnZWTxweTGTtTz0lkZVBd3JNA7H02GB6EGivi64/wCCgln/AME8NG8BfCL4vv8AE74m/FaTwjdalHqmheGW1B/GMliqGdLZYtrS3OJIwVCAAZZ2UZaun1j/AIK6eAbL4fa54rsfCnxH1vw94H02x1PxxdWGkxM3gdLq0ivBFeRPMsks8MEyPPFaLO8IJ3DPFAj6qoyPUcV4N8df+Cgfhr4L+CNU8T2Xhjxr468NeH9Ht/EWr6p4ds4JLWx02aN5UuUe4mhF1+6QyGO182QJglPmUN5L+0Z/wUe1Twn+19+y1ofgbTtS8T/Dv4z6VrXiG+udN0Y3V1q9pFpkdxYi0LOu35pklkBUMFCDIDEUDSPtPcPUUjoJFwSQOvFfHHwy/wCCgml+G4/2mvFerXXxe8TaT8JNbiGo+H7nwtaW9x4XhXTo7iSK0ETiS5h8v9+0kzZHmDbkc1vfDX/grp8P/iH8SfhroUvhD4peG9G+MVtE/grxVrXh8W+heIbiS1F0tnHKsjSxzGLeVM0UccnlP5buNpYE0fU/2dQc859adsAxyQPrXyx+0Z/wV3+G/wCzho/jDXLvQ/HXifwd8N9ci8N+MfEfh/TYruw8M38nlgQyhpUmlKGaJZDbxyiJpFViGyB77p13bfHT4Vq1zY+KPD9pr1uyyW8s8+karbKSV/1kEiywucZDI4bBBBGaLhY6hoVLAknjkc05VC4x1xXwj/wQD1HW/in/AME/JPE/i3xh478Y6/rniTXdOur/AF7xNfajMkFpqV1awLD5srCArEigtEFZmG4knmsf9n79rbw9/wAE/wD4A/E7xv8AEPVfjD4v0KD4w614PS/u9Wv/ABN/YdpHq5srGNhczsYolMsce5Q0jnG4uwFO40j9BHgViSSQfr0oMQJPzNnGOtfOnwp/4KXeF/if8fPEPwwvPAvxU8G+NtJ0GbxRpum+ItBW0k8U6VFKsLXVgVlcN87xjyZvKnXzAGjUhgvG/sef8FB/Cl7+zDo/iHWPF/jzxrqvirxxrHhXQrfWtAtbDX9XvoLu5zYQ2lsdgSBIZV8yQoAkJeUpzRcSR9epbrGQRnin18w6L/wVJ8OXHib4saBq/wAOvip4f8R/BbRbfXvE2mT6daXUqWlwZDA9u1tcyxz744ZpPlbhYmzhhtrRl/4KdeA4vBXwD8RHQ/HB0f8AaOm0y28K3Q0xGigm1C2a6t4rthLtifyUkchS/EbYzilcLH0bRmvnL4z/APBTfwN8H7r4gSw6H4z8XaH8IXjTx9rOgWMVxZ+Ei0SzMJQ8qS3DRQussqWiTPEjAsAeKv8A7eX7dWjfsdf8E/8Axr8d7GK28S6ZoOgx6tpKRykQaq9wY0tBvUE+XI80RJH8JJoHY9+3D1FMaJGYtkgn3r47+HH/AATr8cfFXwNpmu/Fr9of423vjvU7eK9ul8G+JX8L6Jo8jxo7W1paWqqksMb5CvciWRwPnJyRW98Df2irn9l3x/8ACf8AZx+JuveOfiH8VfFGlale2/i6fSYY7DVorNWnk3zJ5YLxRPDEcR7mYqzfeJoE0fU32dfU4HbPFOUCNQAeBXzFpP8AwVk+G2qeD/GWqPpPjfT7zwb44t/hw+kXulpBqGqa7cCIwWtuhk2neJkPmSPGgG5mYKCa4L9vr/gpT4s+C37B3xQ8c+FvAfjLwb498Da1pmgmy8SaPBdxwvd3Vov2lGt7iSCeAw3BCukrASkIwBytAJH23mivCtd/bn0Tw5q3hTwzbeFfHGsfEXxlaXWo6d4OjsYLbWBZWrKk97cCeaOC1tw7xqHmlTe8qqgZsgefeJP+Cznwp8IfBrUfGep6P8Q7WPw34qj8F+KNJ/sPfqfhLVJJooY4r2JZCoR2mi8uWFpUlDgoz0DsfW1FfJVr/wAFfPC13498UeCV+FHx1X4leHbKPV7Xwa3hiJdX1/TJHZBqNrm48gW4Iw3nywyI7LGyCVhGep8Kf8FSvhT46/Zi8C/FPRrjX9Q034lat/wjvhzRU01l1vUdWEs0Umn/AGZiPLmje3n8wyMsaLC7lwg3ECx9GUEgdSBXyV8Sf+Cx3w3+C/w88c634w8M/EXQL/4YaxZaP4v0WTS4bi98Om9KfY7uZopmga0nEilJ45XXqDtZSo9f/Zd/ax0n9rbTPE97o3hjx54at/C+svo//FU6HJpEmpgRRypd28cnzvbSRyoyOyqWB+6KBHqxGQRkjNN8tWzg5r8jdc/aQ+G/7Of/AAUg/ap8J/GD4tfH+Lwv4UTQtQ8J6TYeMPF1yln9o0w3V4qPYu2xWlYFVnfaoIC4UcfWnwU/ay8N/sXfA74IaF481b4ma9pPxTuGOmeOdf36ja6bJqdxNd6dp+p30krSLJ5c0VrHIwZCY0BZARSaGfX6xhOmaWvlv4tf8Favh78GfhrceONW8N/EWfwEviqPwlZeJLHR0uNP1Sd50tvtMBEu82n2hzEs7IqymNjF5iFGa98Nv+CpfgXxj+04PhJ4j8MfEX4X+LtQ0651nQB400ZNMtfFFlbgtPPZyrK4JjjBkeKbypkQEtGuGAYWPpaoL7TYNSt5IriJJoZRteN1DI49CDwRXzb4f/4KpeAfEF54DvV0Hxxa+CfirqL6P4K8YXGnwx6R4mvtsrQ28QMv2mI3CwyG3eeGOOcKCjkMhbm/hT/wWV+H/wAd/HkXhjwt4G+NF9qTazr3h2+mTwk32fRL/SbOW5mt7qUuY45pfJeOGIku7gZVVZWKaBNrVHuup/sefCTXLyS4vPhl4AuriaQyvJJoFqzuxOSxOzJJPrV7w5+y/wDDbwfdSXGk/D/wXpk8ow0lrottExH1VBXzb+x1/wAFFvC93+x78INVm8UfEP4teK/ipqOs6Z4Xj1HQrLTPEfimWyvbsXBe2hMdpbRW8cJBlkeNRGkZc+Y4U7/ir/gsH8M/AvwP13xzrWg/ELT7fwZ4pj8HeLtLOjpLqHhHUZJ7eCNLtElKGKRrq3McsLypIsqlC3IGaoU735V9x6Es2x0oezlWm49uaVvuufUttpdvZW6QwRJBDGAqpGAiqB0AAqVYFVsjORXzx8JP+Cl/g34j/HnX/hvrnhj4g/DLxPo2hy+KbZPGWkJp1vrWjxSCOW/tpUlkURozJvSby5kDjdGp3AXNE/4KF+HdevPCOp2/hLxx/wAK48cW1zeaV4+a1tk0HyILK4vmnnBnF3bwPBbSNHNLbqjlowG+dSdLHntX1PfHjD9aRYVjBAJA+tfNfhj/AIKo/D3WdY8Evqmj+NPC3hT4oWF1qXgrxVrGnJBpXiWG3tXvH2BZGuLdntY3niW6hhMsanYC2Fq78Mv29Jv2h9LsV8GfDnxzaJ4z8L3PiTwTr2uW9vBoWuxosZh8yWCaWa1MhmicJcRRyFCx25VlFJisfRHlLkkEg+o60hgBxksSK+O/2Q/2APiD8IPil8P9V8c/EzxD4usfh94dvJJDLrd1MfFPibU5Wk1DUbqJiF8i3jP2e0hbd5aOx+UqpP2ODkA0htDBAqjHJFPoooEFFFFABRRRQAUUUUANmO2Jj0wK/KD/AILX/G4ePf2jtK8H2s5ey8D2JMyr937ZchXfPusaxD23H1NfqV458Z2Hw/8AB+ra3qkot9O0a0lvbmQnG2ONC7H64FfgL8RviFqHxb+I2u+KdVYtqPiG+m1CcZyEMjlgg9lGFA7Ba+Y4oxXJQjQX2nr6L/g2+4/DPHXP/q2VUsrpv3q8rv8Awwaf4y5bejMXcdoBwGPbOKYGJwcnJHORTwmGzgf40+vhj+TrjDxBk9cZ59aIyAo9T7UsiF1wCBTXJBYjGQKAGXUi29qzjJCKSOea/eT9j74eD4ZfssfD7QnQxzafoVqJV6FZGjDv+O5mz71+Hvwn8JD4gfFPwt4fcAprWr2lg+f7sk6I2f8AgJNf0FWyrHCEVQip8oAGAAOAPyr67hWjrUq+i/X/ACP6O+j9gLzxmNfRQivndv8AKI+mzgGFgRkEU6qmraxb6PYT3N3PDaWtsjSyzzSBI4kUZZmJ4AA5JNfYtn9Kt2Wp+Of/AAVu19Nc/br8TxowK6bZ2Vlx1BEIY/8AodfNmMJgHGK7j9p/4nQ/G/8AaM8a+LLSSSSx1vV557JmUqxtg2yE4PTMaqcds1w5B2kYyTX5TjqqqYidRbNv8z/PrinHRxmc4rFQd4zqTafdOTt+A3J7sD+f9KAzFM85z9KI3PAAyPXpTwcgHsa5jwRgY7wDwD/nFfrf/wAEWvhYPA37JCa5NCY7vxnqE9/k4y0EbeRHz6fIxGf72e9fkpb6dc61fwWVlG015eSJb28aDLSSuwVFA7ksRX7+fA/4bW/we+E3hrwraoqweHtLt7AY6MY41Vm+pYE57kmvp+F8PzV5VX9lfi/+Bc/d/AbKPbZpXzCS0pQSX+Kb/wDkYv7zrKKKK+5P6tCiiigAprRK5yRzTqKAPLv2zf2U/Df7a/7LnjT4U+KvtMWheNdPawnntiPPtGyHinjzxvjkVHGeCVweCa8m/Z/8W/tL/Az4f6J4H8c/DrQPifqGh2yafD418P8AiS30y21eGMbI7i8s7lVltpyiqZFg89NxYpxhR9VUUDPh74tfCz44+I/+CkXwD+J03wx03VtE+HvhvWtH1250vxFaRwxXGrNZ/PbJcFJpY7dbch2ZUZ8kon8J87v/APgmFrnw0+PXx1S++GFz8ZfAHxs8R3fiq1ktPiNe+HG06W8hRLuw1GzE0cUtuXUlJYllcxuVZDgV+ktFAJn5m/G3/gnB8XPEPxh+Iej2XgTwD4r+G+sfC+y8JfDpNV8RzHTvhdJb6dLb3FtBp7x/v5J5TDsuTtcLEm51AKHS0f8AZD+Pngvwx+wx4t0XwFoV/wCLP2efDd54N8UeHtS8Tw2wZbjSINOF/FdRpKjxI9t5pjA80q6gAHdj9IKKAufnBZfs8/HaLR/23raT4NyK37Qkk03hg/8ACWaYcGTSIdK23Hz/ALvBjNxkbvkO37/B1fGv7LXxj1r9lX9iiyg+HSzeJv2f/FWiXviTRj4iskd7ew0a609poJ9/lSBpJUcISGK8EA5x+hVI4JUgHBNAXPyT8N/A3UfiR4z/AGmrfwp4H8Z/Er9m/wAYfE+fxHfaPouv6VHa6/q1lIkmrRpLftBcxxSalbsssMYaJzBiK4EbuD+lP7Lv7Q/h/wDaw/Z58GfE3wkt6nhvxvpEGrWEd3B5FxDFIoPlyJkgOhyp2krlSVJBBPncv/BNDwLbTeL7PSNe+Ifhrwl8QNQuNV8ReFdH8Qy2mkajcXDFrp0ABntROzM0qWs0KuWYkZY59y8DeBtJ+GnhDSfD2gadZaPoWhWcWn6dYWcQht7K3iQJHFGi8KiqoUADgAUBc+Ef2Q/gx+0t/wAE0/BmvfCrwn8LPAvxZ8DN4k1XWfC/iAeNv7EuLS2vr2W5EGoW8ts7GSNpSC8BcMB0zxUH7Wv7DXxbuv8Agm8Phv4e0DT/AB78RfGXxBi8f+Jbmx1WHTNM0+5PiKLW7iGA3LB3iAQ20RI3HYruFyRX6F0UBc+Ofip8Kfije/8ABSP4S/G7TPhzNqmh6X8M9V8O6xpy67YwXuj6heXNpdLEd8gjmVfs7RM8bFdzggkCvl/4O/8ABPb9o/4b/Dz4SeNNL8CeH9P+JHwI+JfirxTbeHNR8TWxsvFuk+IpLpriFLqEOILqCOdEBlUISrEMRgV+s1NdCxByMCgLn5w/s6eFvGXx1/4KH/to6J4t07RfB/ifxl8KvCFh9hsr19Qh0Y3NrrMapJPsQTPGX+Zo1CnGFyOTyWnfspftUx/s5fsj+DdT+E3go2/7L3i7Qbi9TTfGsU914ot9N0u8sPtlv5scUUMZWVG8uRjIzSdEVCW+4vht+w3oPwv/AGs/GXxksfFPje68UePYIbTWrW7vbd9Oube3Mn2SEQiAFFtxNIIyrBiHO9nJJr2ugLn5tz/8ExNd+Gvx3+OK6j8MLr4w+Avjb4ku/FVu9p8R73w+2ly3sSJd2Go2gnjhmt96kpLCsrlGKsh2rX1x8Yf2K/B37QP7DF/8CNc0mLRfBuqeGIfDYsdNmaRdIiiiRIBA7jLeQ0cZRnGT5YyOcV7ZRQFz5R/Z88S/tLfADwDovgTxt8PdE+Kt5oFvHptt430LxLb6dHq8ESiOK4vrS6CyW9wygNKIPPUsWK8YWuS/a2+Anx01n9p/9nf45+GPCXgzxj4j+Glpr+k+IfCsfiN9LhMeqQxpHNb3k0LBxD5KB9yKz7sqozgfbdFAXPzC0D9h34zTeF/2gtB+I/wL8A/EvRvij8VIvG1zpx8SxRWmo6bJaxQvFYys0c9vf20kMTpLIIg4VuYywAteLv8Aglh8UtW/4J5/H34eeGJ7/SLTx1qGjap8P/Afijxhca+vhVLC4tLie2fUZTKyG5lt3IjWSWOIlfnO5sfpnRQFz4W8RfAr45WP7Xfw6/ad07wDotx4mm8E3ngHxv8ADyPxNC1xbWUl8l3a3NlfSItvLNG8amVGMalWIV2I58r/AGo/+CeXxh8UfD74r+ItD8EWWt+P/jt8S/DPjPUdItPEFra2XhrTtBlsGt7Z5ptvn3U0dpJveJSgklxkqm9v09ooBM+LvDHww+LV/wD8FiI/i5ffDGfTfh/f/DKHwVNev4isJbi0u/tqag0rQI5ZolOYcqSxdSwUoQ1fNXgf/glR8Y5P2Wvhzp2peEbC38ZfA34r6540stLbxabKz8Z6Zql1fPNDDeWbedZXKRXSbHkAG+Mqflclf1mooC5+ZX7WX7AvjLx/+yD8StI+GP7Pknh7x38VLzQLfUZ9X+Iqapf3Frpd9Hdg3c9zNMojCo0USxSO2ZcsEAr9Ffh9r2q+JfBWmX2s6Hc+F9WuoFa60qe5hupLB+6GWItG5HHKEjmugooBs+A/gz4I+OXwl/bo/aQ+JGofATUda8NfGSTRLfT7O38X6P5sUOn2DWkjTpJKqgSk7lVSxCnDYPFdL8SP2fviz+2p+zhp3wW+IXgDw14C+HnjKTUIvFLWF/a3M+gaFb3bLpek2cY8xHvpYY7Z3ugFit1EmwGURlfteigLn5NftxeCPjh8Cv8Agk5o/gT4mWvhbxJq3w7+KXg7RfB2t2GpGFvGGmwa3arYy3sXlBbOfyxFHJtMgLK79Mbvqj4kfsX61+3X8ZfBfxC+JGjTfDoeA/DniLRNJ0OC/gv71b3WLY2E97LPDmIxJaNKI41JYtNufYUC163+2L+xX4e/bb8EaX4c8U694t0vRtK1S31lbfRbuG28+7tpkntpZGeJ2PlSorqoIUkfMGAAHrGi6dJpOlW1tLdXF/LbxJE1zcbPOuCqgGR9iqu5jydqqMngAcUBc/Ob9kz/AIJxa58LvAPw08C/Eb4HQeM9R+D8+ntpvitfiZfS6Jff2aoSzv7fTJ5yYLzy1T9y0SwpIzhZFjwK9E/4Jg/Cf4w/s0/DP47QeNfhZPZan4x8feIfiBo1vbeItOuBqCajKrxWRdH2xTqFwzsPL5GGPQfb9FAXPyF+A/8AwSl+NHhD4D/s8aprvgKGTxd8Br3xPp2r+F4fHTaaPE+la3dzXRmsNQspFMNzA0kamOcokoWRSQNrH1r9p/8AYZ8X6/8AsTeOPC/wv+Ap8PeKvib4v0DXtTS88ex6hezLpWo6feme9urqZwZXS0eCNInlC/IWZRkD9IKKAufCn7Yf7HPxA/ai/bH8I+JW8KXFj4O8UfBvxD8PPENzFrNqt74VutWa3bzTHu/0hYlidSYWOSwIyBzc+B3wE+NPir9g8fsy/E/wVpGjWlt4KuPAl3470fxDDPY3tsti9pbXdvabBcCVv3ReKRURf3hDtgKft+igLn5xfs5/8E9/FHhX4f8AgXw74w+Bmj6n42+FVh5el+KtR+JN/q3hm5v7fT5be3vbXTpZi8fmsUVoZIY1jSWQBmCqG0v2Df8AgnV4l+CP7TPg3xn4d8Cah+zt4W0nS9Qi8Z+ELHx1Nrug+Lby4iQQi0sTJJDZwwTb5VkXyZOFTy9rOa/QuigLkaQr2ByvFSDgADtRRQIKKKKACiiigAooooAKKKbNIY1BA6nFAHx5/wAFofjgPhr+zDH4YtZhHqXj28+xbe/2SILJcHPufLX/ALaYr8mH6YQnIr6k/wCCvvxrHxZ/bAvdMt7gy6d4JtF0iJAcos5bzLhh7liik/8ATJfSvluHLEtx6V+bZ7ivbYuVto6L5f8ABufxH4r588z4ircjvCl+7j/278X3ycvlYkzRnPSmMSxKnGKMCJCV5FeQfm45skYHU01MOrZGf89KCxaMEYBNNAJJ2sFH86AN34V+NW+GPxQ8M+JEie5Ph/VbbUTEMgyiKVXZQTwCQCM+9fsNov8AwVN+A+r6Al+PiFYWSyKHa3urO5iuYiRnaYzHnI6HGRnvX4uiQSkDkY/Chdzj39Tya9PL82rYNSjTSafe/wCjR9/wV4iZhw3CrSwsIzjUabUr6NaXVmt1v6I/U34uf8Fw/h34YieHwZpWveL7sg7J5oW06zz7mQeafwjA96+Jv2nf+Ci/xO/artJ9O1jU7fRvDUpz/Y2kx+VBNzx5znMkv+6SF/2a8IllEYO9lA9yBW94O+Gnib4g3Qi0Dw54g1uSTkLY6bNcE/8AfCmqxWb4zFLklLR9Fp/wX95pnfiPxHn18NKo1CWnJTTSfk7Xk15NteRhuChIBI9KczgqecD1r6F8Df8ABLD47eOijjwQ2jwvj97quoW9sFB7lNxk/wDHM/SvZfA//BCPxffhH8SeOPDumAqC0VjaS3jqe43NsH6GsqWVYypZxpv56fnY4Mv8OuJcZ/Bwc0u8lyf+luN/kfCgVQTnJYevamuWXaoBLucIuMlj6Y7n2r9WPAn/AAQ1+F2jBG13XPFniJ06qLhLKJvbEa7se26vd/g9+wV8I/gPqCXvhnwPo1rqKcrfXQe+ukPqsk7OyZ9FIHtXp0OGcVJ/vGor73/XzPuMs8Cc9rSTxlSnSj11cpfckk//AAJHxr/wSy/4JuaxH4s0/wCJ3xC0ifTbbSytx4e0y5G24uJf4buZOqIo5RGwWJ3EAAZ/SgIFOQOTRGmwEE5J5zS19hgcDTwtL2VP5vuz+k+FOFsHkGAjgcGr9ZSdryl1btp5JdFoFFFFdp9KFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU2QZ29Mg0UUmB/O5rfie88cavd67qEnnahrdw+o3T/wB6WZjI/wCGWP4YqGJg2QABiiivx9SbV31P83qs5Tm5yd23diAHJxjFIAGAyWwaKKZEVdIcAGRQCQK7D4H/AAQ1j9oDxNLpWhT6bbXEZUO19K8aAMe2xHJ/SiitaEVKaiz0cooQrYunSqK8W9T7F+H/APwQg8Ramkcvibx9o9lG3JTS7KS4YfRpNg/MV7Z4A/4InfCLwuUk1y78WeK5V4ZLi/FnAx9QtuqP+bkUUV99gcowfJzOmm/O7/M/sXhzw54bp4aFf6pGUn/Nea+6Ta/A9u+H37Evwj+FQB0P4eeF7V1PEstoLqX0+/Lub9a9V06xgsLNIreCK3hXokaBFH4DiiivYpUoQVoRS9FY/RMJgcPhYezw1OMF2ikl9ysTbF64GTQEA6Ac0UVqdVhQAM4GM0UUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH/9k=', '0001.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `institution_categories`
--

DROP TABLE IF EXISTS `institution_categories`;
CREATE TABLE IF NOT EXISTS `institution_categories` (
`id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `institution_categories`
--

INSERT INTO `institution_categories` (`id`, `name`) VALUES
(1, 'HOSPITAL'),
(2, 'HEALTH CENTER'),
(3, 'DISPENSARY');

-- --------------------------------------------------------

--
-- Table structure for table `institution_config`
--

DROP TABLE IF EXISTS `institution_config`;
CREATE TABLE IF NOT EXISTS `institution_config` (
`id` int(11) NOT NULL,
  `printer` int(1) NOT NULL DEFAULT '1',
  `registrationcharge` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `institution_config`
--

INSERT INTO `institution_config` (`id`, `printer`, `registrationcharge`) VALUES
(1, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_dispose_reasons`
--

DROP TABLE IF EXISTS `inventory_dispose_reasons`;
CREATE TABLE IF NOT EXISTS `inventory_dispose_reasons` (
`id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inventory_dispose_reasons`
--

INSERT INTO `inventory_dispose_reasons` (`id`, `name`, `createdby`, `createdon`, `modifiedby`, `modifiedon`, `status`) VALUES
(1, 'EXPIRY', 1, '2016-04-15 09:46:55', NULL, NULL, 1),
(2, 'DAMAGE', 1, '2016-04-15 09:47:06', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_item_categories`
--

DROP TABLE IF EXISTS `inventory_item_categories`;
CREATE TABLE IF NOT EXISTS `inventory_item_categories` (
`id` int(11) NOT NULL,
  `code` int(3) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inventory_item_categories`
--

INSERT INTO `inventory_item_categories` (`id`, `code`, `name`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 601, 'Drug', 'Drug', 1, 1, '2016-04-04 08:26:30', NULL, NULL),
(2, 602, 'Medical Supply', 'Medical Supply', 1, 1, '2016-04-04 08:26:50', NULL, NULL),
(3, 603, 'Medical Equipment', 'Medical Equipment', 1, 1, '2016-04-04 08:27:07', NULL, NULL),
(4, 604, 'Other', 'Other', 1, 1, '2016-04-04 08:27:18', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_items`
--

DROP TABLE IF EXISTS `inventory_items`;
CREATE TABLE IF NOT EXISTS `inventory_items` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `drug_type` int(3) DEFAULT NULL,
  `drug_group` int(3) DEFAULT NULL,
  `category` int(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `consumptioncategory` int(1) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inventory_items`
--

INSERT INTO `inventory_items` (`id`, `name`, `drug_type`, `drug_group`, `category`, `status`, `consumptioncategory`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'Panadol', 1, 1, 601, 1, 1, 200, '2016-04-05 08:01:48', 200, '2016-04-20 12:55:40'),
(2, 'Scissors', NULL, NULL, 602, 1, 0, 200, '2016-04-05 09:23:20', NULL, NULL),
(3, 'Paracetamol', 1, 1, 601, 1, 0, 200, '2016-04-05 09:29:26', NULL, NULL),
(4, 'Paracetamol 25ml', 2, 1, 601, 1, 0, 200, '2016-04-05 09:30:08', NULL, NULL),
(5, 'X-Ray Machine', NULL, NULL, 603, 1, 0, 200, '2016-04-05 09:31:16', NULL, NULL),
(6, 'Ultra Sound Machine', NULL, NULL, 603, 1, 0, 200, '2016-04-05 09:31:27', NULL, NULL),
(7, 'Blood Mixer', NULL, NULL, 603, 1, 0, 200, '2016-04-05 09:31:48', NULL, NULL),
(8, 'Brooms', NULL, NULL, 604, 1, 0, 200, '2016-04-05 09:32:25', NULL, NULL),
(9, 'Rim Papers', NULL, NULL, 604, 1, 0, 200, '2016-04-05 09:32:35', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_requesition_orders`
--

DROP TABLE IF EXISTS `inventory_requesition_orders`;
CREATE TABLE IF NOT EXISTS `inventory_requesition_orders` (
`id` int(11) NOT NULL,
  `requesitionid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `itemid` int(11) NOT NULL,
  `inventoryunit` int(11) NOT NULL,
  `requestqty` int(11) NOT NULL,
  `supplyunit` int(11) DEFAULT NULL,
  `supplyqty` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '3',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inventory_requesition_orders`
--

INSERT INTO `inventory_requesition_orders` (`id`, `requesitionid`, `itemid`, `inventoryunit`, `requestqty`, `supplyunit`, `supplyqty`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, '0001-2-1-2658', 3, 2, 2, 1, NULL, 2, 202, '2016-04-21 11:26:58', 203, '2016-04-25 16:30:59'),
(3, '0001-2--3610', 1, 2, 12, NULL, 10, 1, 202, '2016-04-25 12:36:10', 200, '2016-04-25 15:37:52');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_stock`
--

DROP TABLE IF EXISTS `inventory_stock`;
CREATE TABLE IF NOT EXISTS `inventory_stock` (
`id` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `batch` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `storebatch` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `expiredate` date DEFAULT NULL,
  `supplierid` int(11) DEFAULT NULL,
  `totalvalue` double DEFAULT NULL,
  `unitprice` double DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `inventoryunit` int(11) DEFAULT NULL,
  `source_inventoryunit` int(11) DEFAULT NULL,
  `requesitionid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastupdate` datetime NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inventory_stock`
--

INSERT INTO `inventory_stock` (`id`, `itemid`, `batch`, `storebatch`, `expiredate`, `supplierid`, `totalvalue`, `unitprice`, `quantity`, `inventoryunit`, `source_inventoryunit`, `requesitionid`, `status`, `lastupdate`, `userid`) VALUES
(35, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 89, 89, 1, 2, NULL, NULL, 'STOCKING', '2016-04-21 11:47:25', 200),
(34, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 89, 89, -1, NULL, NULL, NULL, 'TRANSFER', '2016-04-21 11:44:45', 200),
(33, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 89, 89, 1, 2, NULL, NULL, 'STOCKING', '2016-04-21 11:44:45', 200),
(32, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 178, 89, -2, NULL, NULL, NULL, 'TRANSFER', '2016-04-19 15:26:52', 200),
(31, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 178, 89, 2, 2, NULL, NULL, 'STOCKING', '2016-04-19 15:26:52', 200),
(30, 1, '0001-15-0139', 'XYAWS1D34', '2016-04-15', 1, 87000, 1000, -87, NULL, NULL, NULL, 'DISPOSE', '2016-04-18 16:00:58', 200),
(29, 1, '0001-15-0139', 'XYAWS1D34', '2016-04-15', 1, 3000, 1000, -3, NULL, NULL, NULL, 'DISPOSE', '2016-04-18 12:01:29', 200),
(16, 4, '0001-48-3857', '0001-48-3857', '2016-04-12', 1, 1000, 100, 10, NULL, NULL, NULL, 'STOCKING', '2016-04-11 10:38:57', 200),
(17, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 889, 89, 10, NULL, NULL, NULL, 'STOCKING', '2016-04-11 10:40:31', 200),
(19, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 89, 89, -1, NULL, NULL, NULL, 'DISPOSE', '2016-04-15 17:04:23', 200),
(20, 1, '0001-15-0139', 'XYAWS1D34', '2016-04-15', 1, 100000, 1000, 100, NULL, NULL, NULL, 'STOCKING', '2016-04-17 00:01:39', 200),
(24, 1, '0001-15-0139', 'XYAWS1D34', '2016-04-15', 1, 10000, 1000, -10, NULL, NULL, NULL, 'TRANSFER', '2016-04-17 00:05:50', 200),
(23, 1, '0001-15-0139', 'XYAWS1D34', '2016-04-15', 1, 10000, 1000, 10, 1, NULL, NULL, 'STOCKING', '2016-04-17 00:05:50', 200),
(28, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 267, 89, -3, NULL, NULL, NULL, 'TRANSFER', '2016-04-17 11:43:09', 200),
(27, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 267, 89, 3, 1, NULL, NULL, 'STOCKING', '2016-04-17 11:43:09', 200),
(36, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 89, 89, -1, NULL, NULL, NULL, 'TRANSFER', '2016-04-21 11:47:25', 200),
(37, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 89, 89, 1, 1, NULL, NULL, 'STOCKING', '2016-04-21 12:05:06', 202),
(38, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 89, 89, -1, 2, NULL, NULL, 'TRANSFER', '2016-04-21 12:05:06', 202),
(39, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 267, 89, 3, 1, NULL, NULL, 'STOCKING', '2016-04-21 13:22:28', 202),
(40, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 267, 89, -3, 2, NULL, NULL, 'TRANSFER', '2016-04-21 13:22:28', 202),
(41, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 178, 89, 2, 2, NULL, NULL, 'STOCKING', '2016-04-21 13:23:05', 200),
(42, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 178, 89, -2, NULL, NULL, NULL, 'TRANSFER', '2016-04-21 13:23:05', 200),
(46, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 89, 89, -1, 2, NULL, NULL, 'TRANSFER', '2016-04-21 13:28:49', 202),
(45, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 89, 89, 1, 1, NULL, NULL, 'STOCKING', '2016-04-21 13:28:49', 202),
(47, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 534, 89, -6, 1, NULL, NULL, 'DISPENSE', '2016-04-22 17:01:18', 203),
(53, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 10000, 10, 1000, 2, NULL, NULL, 'STOCKING', '2016-04-23 10:27:57', 200),
(49, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 1000000, 10, 100000, NULL, NULL, NULL, 'STOCKING', '2016-04-23 09:37:56', 200),
(50, 3, '0001-67-3756', 'ABC13DE', '2026-04-29', 1, 2000000, 20, 100000, NULL, NULL, NULL, 'STOCKING', '2016-04-23 09:37:56', 200),
(51, 4, '0001-31-3756', 'ABC14DE', '2026-04-13', 1, 10000000, 100, 100000, NULL, NULL, NULL, 'STOCKING', '2016-04-23 09:37:56', 200),
(54, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 10000, 10, -1000, NULL, NULL, NULL, 'TRANSFER', '2016-04-23 10:27:57', 200),
(55, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 0, 89, 0, 2, NULL, NULL, 'STOCKING', '2016-04-23 10:27:57', 200),
(56, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 0, 89, 0, NULL, NULL, NULL, 'TRANSFER', '2016-04-23 10:27:57', 200),
(57, 3, '0001-67-3756', 'ABC13DE', '2026-04-29', 1, 20000, 20, 1000, 2, NULL, NULL, 'STOCKING', '2016-04-23 10:27:57', 200),
(58, 3, '0001-67-3756', 'ABC13DE', '2026-04-29', 1, 20000, 20, -1000, NULL, NULL, NULL, 'TRANSFER', '2016-04-23 10:27:57', 200),
(59, 4, '0001-31-3756', 'ABC14DE', '2026-04-13', 1, 100000, 100, 1000, 2, NULL, NULL, 'STOCKING', '2016-04-23 10:27:57', 200),
(60, 4, '0001-31-3756', 'ABC14DE', '2026-04-13', 1, 100000, 100, -1000, NULL, NULL, NULL, 'TRANSFER', '2016-04-23 10:27:57', 200),
(61, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 10000, 10, 1000, 1, NULL, NULL, 'STOCKING', '2016-04-23 10:28:19', 200),
(62, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 10000, 10, -1000, NULL, NULL, NULL, 'TRANSFER', '2016-04-23 10:28:19', 200),
(63, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 0, 89, 0, 1, NULL, NULL, 'STOCKING', '2016-04-23 10:28:19', 200),
(64, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 0, 89, 0, NULL, NULL, NULL, 'TRANSFER', '2016-04-23 10:28:19', 200),
(65, 3, '0001-67-3756', 'ABC13DE', '2026-04-29', 1, 20000, 20, 1000, 1, NULL, NULL, 'STOCKING', '2016-04-23 10:28:19', 200),
(66, 3, '0001-67-3756', 'ABC13DE', '2026-04-29', 1, 20000, 20, -1000, NULL, NULL, NULL, 'TRANSFER', '2016-04-23 10:28:19', 200),
(67, 4, '0001-31-3756', 'ABC14DE', '2026-04-13', 1, 100000, 100, 1000, 1, NULL, NULL, 'STOCKING', '2016-04-23 10:28:19', 200),
(68, 4, '0001-31-3756', 'ABC14DE', '2026-04-13', 1, 100000, 100, -1000, NULL, NULL, NULL, 'TRANSFER', '2016-04-23 10:28:19', 200),
(69, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 20, 10, -2, 1, NULL, NULL, 'DISPENSE', '2016-04-23 10:29:05', 203),
(70, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 90, 10, -9, 1, NULL, NULL, 'DISPENSE', '2016-04-25 10:24:46', 203),
(71, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 20, 10, -2, 1, NULL, NULL, 'DISPENSE', '2016-04-25 10:56:41', 203),
(74, 1, '0001-15-0139', 'XYAWS1D34', '2016-04-15', 1, 10000, 1000, -10, 1, NULL, NULL, 'DISPOSE', '2016-04-25 11:50:23', 200),
(75, 4, '0001-48-3857', '0001-48-3857', '2016-04-12', 1, 200, 100, -2, NULL, NULL, NULL, 'DISPOSE', '2016-04-25 11:51:35', 200),
(79, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 120, 10, -12, 2, NULL, NULL, 'TRANSFER', '2016-04-25 12:35:28', 202),
(78, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 120, 10, 12, 1, 2, NULL, 'STOCKING', '2016-04-25 12:35:28', 202),
(80, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 120, 10, 12, 2, 1, NULL, 'STOCKING', '2016-04-25 12:37:10', 203),
(81, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 120, 10, -12, 1, NULL, NULL, 'TRANSFER', '2016-04-25 12:37:10', 203),
(88, 3, '0001-67-4031', 'XYZ123456', '2025-04-30', 1, 89, 89, -1, 2, NULL, NULL, 'DISPENSE', '2016-04-26 00:17:13', 202),
(87, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 100, 10, -10, NULL, NULL, '0001-2--3610', 'TRANSFER', '2016-04-25 15:37:52', 200),
(86, 1, '0001-41-3756', 'ABC12DE', '2024-04-04', 1, 100, 10, 10, 2, NULL, '0001-2--3610', 'STOCKING', '2016-04-25 15:37:52', 200);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_suppliers`
--

DROP TABLE IF EXISTS `inventory_suppliers`;
CREATE TABLE IF NOT EXISTS `inventory_suppliers` (
`id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `physicaladdress` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inventory_suppliers`
--

INSERT INTO `inventory_suppliers` (`id`, `name`, `email`, `phone`, `physicaladdress`, `category`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'MSD', 'kemixenock@gmail.com', '+255719186759', 'Wazo Hill', NULL, 1, 200, '2016-04-06 08:48:15', 200, '2016-04-06 13:05:01');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_unit_categories`
--

DROP TABLE IF EXISTS `inventory_unit_categories`;
CREATE TABLE IF NOT EXISTS `inventory_unit_categories` (
`id` int(11) NOT NULL,
  `code` int(3) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inventory_unit_categories`
--

INSERT INTO `inventory_unit_categories` (`id`, `code`, `name`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 601, 'Pharmacy', '', 1, 1, '2016-04-04 09:52:07', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_unit_users`
--

DROP TABLE IF EXISTS `inventory_unit_users`;
CREATE TABLE IF NOT EXISTS `inventory_unit_users` (
`id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `inventoryunit` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastupdate` datetime DEFAULT NULL,
  `lastupdater` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inventory_unit_users`
--

INSERT INTO `inventory_unit_users` (`id`, `userid`, `inventoryunit`, `status`, `createdby`, `createdon`, `lastupdate`, `lastupdater`) VALUES
(1, 202, 1, 0, 196, '2016-04-21 07:25:16', '2016-04-21 10:25:36', 196),
(2, 202, 2, 1, 196, '2016-04-21 07:25:36', NULL, NULL),
(3, 203, 1, 0, 196, '2016-04-22 13:46:00', '2016-04-22 16:46:40', 196),
(4, 203, 1, 1, 196, '2016-04-22 13:46:40', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_units`
--

DROP TABLE IF EXISTS `inventory_units`;
CREATE TABLE IF NOT EXISTS `inventory_units` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(3) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inventory_units`
--

INSERT INTO `inventory_units` (`id`, `name`, `category`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'Out-Patient Pharmacy', 601, '', 1, 200, '2016-04-04 12:37:28', 200, '2016-04-04 15:41:15'),
(2, 'In-Patient Pharmacy', 601, '', 1, 200, '2016-04-04 12:41:35', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
CREATE TABLE IF NOT EXISTS `location` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=181 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `name`, `parent`) VALUES
(1, 'Kilimanjaro', 0),
(2, 'Kigoma', 0),
(3, 'Tanga', 0),
(4, 'Dar es Salaam', 0),
(5, 'Moshi', 1),
(6, 'Kigoma rural', 2),
(7, 'Same', 1),
(8, 'Mwanga', 1),
(9, 'Hai', 1),
(10, 'Siha', 1),
(11, 'Rombo', 1),
(12, 'Ilala', 4),
(13, 'Kinondoni', 4),
(14, 'Temeke', 4),
(15, 'Handeni', 3),
(16, 'Tabora', 0),
(17, 'Igunga', 16),
(18, 'Mwanza', 0),
(19, 'Machame', 1),
(20, 'Mara', 0),
(21, 'Tarime', 20),
(22, 'Bunda', 20),
(23, 'serengeti', 20),
(24, 'Rorya', 20),
(25, 'Butiama', 20),
(26, 'Musoma urban', 20),
(27, 'Musoma rural', 20),
(28, 'Kigoma urban', 2),
(29, 'Kibondo', 2),
(30, 'Kasulu', 2),
(31, 'Lindi', 0),
(32, 'Lindi urban', 31),
(33, 'Lindi rural', 31),
(34, 'Kilwa', 31),
(35, 'Nachingwea', 31),
(36, 'Liwale', 31),
(37, 'Ruangwa', 31),
(38, 'Katavi', 0),
(39, 'Mpanda', 38),
(40, 'Mlele', 38),
(41, 'Manyara', 0),
(42, 'Mbulu', 41),
(43, 'Babati', 41),
(44, 'Hanang', 41),
(45, 'Simanjiro', 41),
(46, 'Kiteto', 41),
(47, 'Arusha', 0),
(48, 'Arumeru', 47),
(49, 'Arusha urban', 47),
(50, 'karatu', 47),
(51, 'Monduli', 47),
(52, 'Longido', 47),
(53, 'Dodoma', 0),
(54, 'Dodoma rural - chamwino', 53),
(55, 'Bahi', 53),
(56, 'Dodoma urban', 53),
(57, 'Kondoa', 53),
(58, 'Mpwapwa', 53),
(59, 'Kongwa', 53),
(60, 'Geita', 0),
(61, 'Bukombe', 60),
(62, 'Chato', 60),
(63, 'Mbogwe', 60),
(64, 'Nyang''hwale', 60),
(65, 'Iringa', 0),
(66, 'Iringa rural', 65),
(67, 'Iringa urban', 65),
(68, 'Mufindi', 65),
(69, 'Kilolo', 65),
(70, 'Mafinga', 65),
(71, 'Makete', 65),
(72, 'Njombe', 0),
(73, 'Ludewa', 65),
(74, 'Kagera', 0),
(75, 'Bukoba rural', 74),
(76, 'Bukoba urban', 74),
(77, 'Misenyi', 74),
(78, 'Muleba', 74),
(79, 'Karagwe', 74),
(80, 'Ngara', 74),
(81, 'Biharamulo', 74),
(82, 'Kyerwa', 74),
(83, 'Mbeya', 0),
(84, 'Mbeya urban', 83),
(85, 'Mbeya rural', 83),
(86, 'Rungwe', 83),
(87, 'Kyela', 83),
(88, 'Ileje', 83),
(89, 'Mbozi', 83),
(90, 'Chunya', 83),
(91, 'Momba', 83),
(92, 'Mbarali', 83),
(93, 'Morogoro', 0),
(94, 'Kilosa', 93),
(95, 'Kilombero', 93),
(96, 'Morogoro urban', 93),
(97, 'Morogoro rural', 93),
(98, 'Mvomero', 93),
(99, 'Ulanga', 93),
(100, 'Gairo', 93),
(101, 'Mtwara', 0),
(102, 'Masasi', 101),
(103, 'Nanyumbu', 101),
(104, 'Newala', 101),
(105, 'Tandahimba', 101),
(106, 'urban - Mikindani', 101),
(107, 'Mtwara rural', 101),
(108, 'Ukerewe', 18),
(109, 'Magu', 18),
(110, 'Sengerema', 18),
(111, 'Misungwi', 18),
(112, 'Kwimba', 18),
(113, 'Nyamagana', 18),
(114, 'Ilemela', 18),
(115, 'Sinyanga', 0),
(116, 'Bariadi', 115),
(117, 'Maswa', 115),
(118, 'Meatu', 115),
(119, 'Kahama', 115),
(120, 'Kishapu', 115),
(121, 'Shinyanga urban', 115),
(122, 'Shinyanga rural', 115),
(123, 'Singida', 0),
(124, 'Iramba', 123),
(125, 'Manyoni', 123),
(126, 'Mkalama', 123),
(127, 'Ikungi', 123),
(128, 'Singida rural', 123),
(129, 'Singida urban', 123),
(130, 'Simiyu', 0),
(131, 'Busega', 130),
(132, 'Itilima', 130),
(133, 'Ruvuma', 0),
(134, 'Songea urban', 133),
(135, 'Songea rural', 133),
(136, 'Tunduru', 133),
(137, 'Mbinga', 133),
(138, 'Namtumbo', 133),
(139, 'Nyasa', 133),
(140, 'Rukwa', 0),
(141, 'Sumbawanga', 140),
(142, 'Nkasi', 140),
(143, 'Kalambo', 140),
(144, 'Pwani', 0),
(145, 'Bagamoyo', 144),
(146, 'Kibaha', 144),
(147, 'Kisarawe', 144),
(148, 'Mkuranga', 144),
(149, 'Rufiji', 144),
(150, 'Mafia', 144),
(151, 'Pemba Kusini', 0),
(152, 'Mkoani', 151),
(153, 'Chakechake', 151),
(154, 'Pemba kaskazini', 0),
(155, 'Wete', 154),
(156, 'Micheweni', 154),
(157, 'Unguja kusini', 0),
(158, 'Unguja A', 157),
(159, 'Unguja B', 157),
(160, 'Mkokoloni', 157),
(161, 'Unguja Magharibi', 0),
(162, 'Kisiwani', 161),
(163, 'Zanzibar urban', 161),
(164, 'Pangani', 3),
(165, 'Lushoto', 3),
(166, 'Korogwe', 3),
(167, 'Mkinga', 3),
(168, 'Kilindi', 3),
(169, 'Muheza', 3),
(170, 'Tanga urban', 3),
(171, 'Nzega', 16),
(172, 'Sikonge', 16),
(173, 'Kaliua', 16),
(174, 'Urambo', 16),
(175, 'Uyui', 16),
(176, 'Tabora urban', 16);

-- --------------------------------------------------------

--
-- Table structure for table `marital_status`
--

DROP TABLE IF EXISTS `marital_status`;
CREATE TABLE IF NOT EXISTS `marital_status` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `marital_status`
--

INSERT INTO `marital_status` (`id`, `name`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'SIngle', '', 1, 197, '2016-03-16 12:31:20', NULL, NULL),
(2, 'Married', '', 1, 197, '2016-03-16 12:31:29', NULL, NULL),
(3, 'Divorced', '', 1, 197, '2016-03-16 12:31:34', NULL, NULL),
(4, 'Widowed', '', 1, 197, '2016-03-16 12:31:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `package_items_services`
--

DROP TABLE IF EXISTS `package_items_services`;
CREATE TABLE IF NOT EXISTS `package_items_services` (
`id` int(11) NOT NULL,
  `packageid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `item_service_id` int(11) NOT NULL,
  `departmentid` int(11) NOT NULL,
  `subdepartmentid` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `package_items_services`
--

INSERT INTO `package_items_services` (`id`, `packageid`, `quantity`, `item_service_id`, `departmentid`, `subdepartmentid`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(8, '0001Wed3625', 10, 1, 1, 1, 1, 196, '2016-04-27 11:48:09', NULL, NULL),
(7, '0001Wed3625', 2, 6, 1, 5, 1, 196, '2016-04-27 11:48:09', NULL, NULL),
(6, '0001Wed3625', 1, 7, 1, 5, 1, 196, '2016-04-27 11:48:09', NULL, NULL),
(9, '0001Wed3625', 100, 1, 3, 6, 1, 196, '2016-04-27 11:48:09', NULL, NULL),
(10, '00010427Wed033451', 19, 6, 1, 5, 1, 196, '2016-04-27 12:35:47', NULL, NULL),
(11, '00010427Wed033451', 10, 1, 1, 1, 1, 196, '2016-04-27 12:35:47', NULL, NULL),
(12, '00010427Wed033451', 10, 10, 2, 4, 1, 196, '2016-04-27 12:35:47', NULL, NULL),
(13, '00010427Wed033451', 200, 1, 3, 6, 1, 196, '2016-04-27 12:35:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
CREATE TABLE IF NOT EXISTS `packages` (
`id` int(11) NOT NULL,
  `packageid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cost` double NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `packageid`, `name`, `cost`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, '0001Wed3625', 'TEST PACKAGE', 500000, 1, 196, '2016-04-27 11:36:44', 196, '2016-04-27 14:48:08'),
(2, '00010427Wed033451', 'MATERNITY', 1000000, 1, 196, '2016-04-27 12:35:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patient_police_details`
--

DROP TABLE IF EXISTS `patient_police_details`;
CREATE TABLE IF NOT EXISTS `patient_police_details` (
`id` int(11) NOT NULL,
  `patientid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `patientvisitid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `policestation` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `accident` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `policename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `policemobile` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastupdate` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patientids_control`
--

DROP TABLE IF EXISTS `patientids_control`;
CREATE TABLE IF NOT EXISTS `patientids_control` (
`id` int(11) NOT NULL,
  `patientidpermanent` int(7) NOT NULL,
  `pidpermlastupdate` datetime NOT NULL,
  `patientidwalkin` int(7) NOT NULL,
  `pidwalklastupdate` datetime NOT NULL,
  `patientvisitpermanent` int(7) NOT NULL,
  `pvidpermlastupdate` datetime NOT NULL,
  `patientvisitidwalkin` int(7) NOT NULL,
  `pvidwalklastupdate` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patientids_control`
--

INSERT INTO `patientids_control` (`id`, `patientidpermanent`, `pidpermlastupdate`, `patientidwalkin`, `pidwalklastupdate`, `patientvisitpermanent`, `pvidpermlastupdate`, `patientvisitidwalkin`, `pvidwalklastupdate`) VALUES
(1, 2, '2016-03-17 13:02:54', 2, '2016-03-17 13:10:55', 6, '2016-03-23 16:29:17', 2, '2016-03-19 14:57:34');

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
CREATE TABLE IF NOT EXISTS `patients` (
`id` int(11) NOT NULL,
  `patientid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `genderid` int(11) NOT NULL,
  `titleid` int(11) NOT NULL,
  `dob` date DEFAULT NULL,
  `maritalid` int(11) DEFAULT NULL,
  `tribe` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `region` int(11) DEFAULT NULL,
  `district` int(11) DEFAULT NULL,
  `street` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `patienttype` int(11) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `patientid`, `name`, `genderid`, `titleid`, `dob`, `maritalid`, `tribe`, `email`, `phone`, `region`, `district`, `street`, `occupation`, `patienttype`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'P0001-0000001', 'Kemix Enock', 1, 3, '1990-12-21', 2, 'Mbena', 'kemixenock@gmail.com', '+255719186759', 4, 13, 'Wazo Hill', 'Software Engineer', 1, 197, '2016-03-17 09:29:05', 197, '2016-03-17 13:08:08'),
(2, 'W0001-0000001', 'Patient Zero', 1, 1, '0000-00-00', 1, '', NULL, '+255719186759', 0, 0, '', '', 2, 197, '2016-03-17 10:10:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patients_bill`
--

DROP TABLE IF EXISTS `patients_bill`;
CREATE TABLE IF NOT EXISTS `patients_bill` (
`id` int(11) NOT NULL,
  `transactionid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `patientid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `patientvisitid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `departmentid` int(11) NOT NULL,
  `subdepartmentid` int(11) NOT NULL,
  `service_item_id` int(11) NOT NULL,
  `orderid` int(11) DEFAULT NULL,
  `sponsorid` int(3) NOT NULL,
  `paymentmodeid` int(3) NOT NULL,
  `amount` double NOT NULL,
  `paymentstatus` int(1) NOT NULL,
  `printstatus` int(1) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patients_bill`
--

INSERT INTO `patients_bill` (`id`, `transactionid`, `patientid`, `patientvisitid`, `departmentid`, `subdepartmentid`, `service_item_id`, `orderid`, `sponsorid`, `paymentmodeid`, `amount`, `paymentstatus`, `printstatus`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, '0000001', 'P0001-0000001', 'P0001-0000003', 1, 2, 8, NULL, 201, 301, 10000, 1, 0, 197, '2016-03-21 07:52:39', NULL, NULL),
(2, '0000002', 'P0001-0000001', 'P0001-0000003', 2, 4, 10, NULL, 201, 301, 20000, 1, 0, 197, '2016-03-23 07:16:34', NULL, NULL),
(3, '0000003', 'P0001-0000001', 'P0001-0000003', 2, 4, 10, NULL, 201, 301, 20000, 1, 0, 197, '2016-03-23 07:17:09', NULL, NULL),
(4, '0000004', 'P0001-0000001', 'P0001-0000003', 2, 4, 10, NULL, 201, 301, 20000, 1, 0, 197, '2016-03-23 07:18:14', NULL, NULL),
(5, '0000005', 'W0001-0000001', 'W0001-0000001', 2, 4, 10, NULL, 201, 301, 20000, 1, 0, 197, '2016-03-23 07:20:46', NULL, NULL),
(6, '0000006', 'W0001-0000001', 'W0001-0000001', 2, 4, 10, NULL, 201, 301, 20000, 1, 0, 197, '2016-03-23 07:21:26', NULL, NULL),
(7, '0000007', 'W0001-0000001', 'W0001-0000001', 2, 3, 2, NULL, 201, 301, 30000, 1, 0, 197, '2016-03-23 07:28:35', NULL, NULL),
(8, '0000007', 'W0001-0000001', 'W0001-0000001', 2, 4, 10, NULL, 201, 301, 20000, 1, 0, 197, '2016-03-23 07:28:35', NULL, NULL),
(9, '0000008', 'P0001-0000001', 'P0001-0000003', 2, 4, 10, NULL, 201, 302, 20000, 1, 0, 197, '2016-03-23 11:48:06', NULL, NULL),
(10, '0000009', 'W0001-0000001', 'W0001-0000001', 2, 3, 2, NULL, 201, 301, 30000, 1, 0, 197, '2016-03-23 13:20:42', NULL, NULL),
(11, '0000010', 'P0001-0000001', 'P0001-0000004', 1, 2, 9, NULL, 201, 302, 5000, 1, 0, 197, '2016-03-23 13:25:40', NULL, NULL),
(12, '0000011', 'P0001-0000001', 'P0001-0000005', 1, 2, 9, NULL, 201, 302, 5000, 1, 0, 197, '2016-03-23 13:29:17', NULL, NULL),
(13, NULL, 'P0001-0000001', 'P0001-0000005', 2, 3, 2, NULL, 201, 303, 30000, 2, 0, 198, '2016-03-24 10:47:19', NULL, NULL),
(14, NULL, 'P0001-0000001', 'P0001-0000005', 2, 4, 10, NULL, 201, 303, 20000, 2, 0, 198, '2016-03-24 10:49:12', NULL, NULL),
(15, '0000012', 'P0001-0000001', 'P0001-0000005', 2, 4, 10, NULL, 201, 301, 20000, 1, 0, 198, '2016-04-19 12:47:21', NULL, NULL),
(16, '0000013', 'P0001-0000001', 'P0001-0000005', 3, 6, 1, 8, 201, 301, 450, 1, 0, 203, '2016-04-25 07:24:46', NULL, NULL),
(17, '0000014', 'P0001-0000001', 'P0001-0000005', 3, 6, 1, 9, 201, 301, 100, 1, 0, 203, '2016-04-25 07:56:41', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patients_credit_bills`
--

DROP TABLE IF EXISTS `patients_credit_bills`;
CREATE TABLE IF NOT EXISTS `patients_credit_bills` (
`id` int(11) NOT NULL,
  `patientid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `patientvisitid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sponsorid` int(3) NOT NULL,
  `departmentid` int(11) NOT NULL,
  `subdepartmentid` int(11) NOT NULL,
  `service_item_id` int(11) NOT NULL,
  `reftransactionid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patients_credit_bills`
--

INSERT INTO `patients_credit_bills` (`id`, `patientid`, `patientvisitid`, `sponsorid`, `departmentid`, `subdepartmentid`, `service_item_id`, `reftransactionid`, `amount`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'P0001-0000001', 'P0001-0000005', 0, 2, 3, 2, '', 30000, 0, 198, '2016-03-24 10:47:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patients_dispense_orders`
--

DROP TABLE IF EXISTS `patients_dispense_orders`;
CREATE TABLE IF NOT EXISTS `patients_dispense_orders` (
`id` int(11) NOT NULL,
  `patientid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `patientvisitid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dispenseinventoryunit` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `frequencyid` int(11) DEFAULT NULL,
  `dosageid` int(11) DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `transactionid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patients_dispense_orders`
--

INSERT INTO `patients_dispense_orders` (`id`, `patientid`, `patientvisitid`, `dispenseinventoryunit`, `itemid`, `frequencyid`, `dosageid`, `days`, `quantity`, `status`, `transactionid`, `createdon`, `createdby`, `modifiedby`, `modifiedon`) VALUES
(5, 'P0001-0000001', 'P0001-0000005', 1, 1, 1, 1, 2, 2, 1, '', '2016-04-22 09:51:32', 202, 203, '2016-04-23 10:29:05'),
(7, 'P0001-0000001', 'P0001-0000005', 1, 3, 3, 1, 2, 6, 1, NULL, '2016-04-22 13:49:32', 203, 203, '2016-04-22 17:01:18'),
(8, 'P0001-0000001', 'P0001-0000005', 1, 1, 3, 1, 3, 9, 1, '0000013', '2016-04-25 07:24:31', 203, 203, '2016-04-25 10:24:46'),
(9, 'P0001-0000001', 'P0001-0000005', 1, 1, 1, 1, 2, 2, 1, '0000014', '2016-04-25 07:54:03', 203, 203, '2016-04-25 10:56:41'),
(10, 'P0001-0000001', 'P0001-0000005', 2, 3, 1, 1, 1, 1, 1, NULL, '2016-04-25 21:17:07', 202, 202, '2016-04-26 00:17:13');

-- --------------------------------------------------------

--
-- Table structure for table `patients_prepaid_accounts`
--

DROP TABLE IF EXISTS `patients_prepaid_accounts`;
CREATE TABLE IF NOT EXISTS `patients_prepaid_accounts` (
`id` int(11) NOT NULL,
  `patientid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `departmentid` int(11) DEFAULT NULL,
  `subdepartmentid` int(11) DEFAULT NULL,
  `service_item_id` int(11) DEFAULT NULL,
  `reftransactionid` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patients_prepaid_accounts`
--

INSERT INTO `patients_prepaid_accounts` (`id`, `patientid`, `departmentid`, `subdepartmentid`, `service_item_id`, `reftransactionid`, `amount`, `status`, `createdby`, `createdon`) VALUES
(3, 'P0001-0000001', 2, 4, 10, '8', -20000, 'Debit', 197, '2016-03-23 11:48:06'),
(2, 'P0001-0000001', NULL, NULL, NULL, NULL, 1000000, 'CREDIT', 1, '2016-03-23 11:47:12'),
(4, 'P0001-0000001', 1, 2, 9, '0000011', -5000, 'Debit', 197, '2016-03-23 13:29:17');

-- --------------------------------------------------------

--
-- Table structure for table `patients_que_control`
--

DROP TABLE IF EXISTS `patients_que_control`;
CREATE TABLE IF NOT EXISTS `patients_que_control` (
`id` int(11) NOT NULL,
  `actiondate` date NOT NULL,
  `next` int(11) NOT NULL,
  `lastupdate` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patients_que_control`
--

INSERT INTO `patients_que_control` (`id`, `actiondate`, `next`, `lastupdate`) VALUES
(1, '2016-03-21', 4, '2016-03-21 10:52:39'),
(2, '2016-03-23', 3, '2016-03-23 16:29:17');

-- --------------------------------------------------------

--
-- Table structure for table `patients_visit`
--

DROP TABLE IF EXISTS `patients_visit`;
CREATE TABLE IF NOT EXISTS `patients_visit` (
`id` int(11) NOT NULL,
  `patientid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `patientvisitid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `visitcategory` int(11) NOT NULL,
  `patientcategory` int(11) NOT NULL,
  `consultationcategory` int(11) DEFAULT NULL,
  `assigneddoctor` int(11) DEFAULT NULL,
  `consultationstatus` int(1) DEFAULT NULL,
  `sponsorid` int(11) NOT NULL,
  `quenumber` int(11) DEFAULT NULL,
  `exitstatus` int(1) DEFAULT '0',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patients_visit`
--

INSERT INTO `patients_visit` (`id`, `patientid`, `patientvisitid`, `visitcategory`, `patientcategory`, `consultationcategory`, `assigneddoctor`, `consultationstatus`, `sponsorid`, `quenumber`, `exitstatus`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'W0001-0000001', 'W0001-0000001', 1, 2, NULL, NULL, NULL, 201, NULL, 1, 197, '2016-03-19 11:57:34', NULL, NULL),
(4, 'P0001-0000001', 'P0001-0000001', 1, 1, 2, 1, NULL, 201, 1, 1, 197, '2016-03-21 07:49:46', NULL, NULL),
(5, 'P0001-0000001', 'P0001-0000002', 1, 1, 2, 1, NULL, 201, 2, 1, 197, '2016-03-21 07:50:15', NULL, NULL),
(6, 'P0001-0000001', 'P0001-0000003', 1, 1, 2, 1, NULL, 201, 3, 1, 197, '2016-03-21 07:52:39', NULL, NULL),
(7, 'P0001-0000001', 'P0001-0000004', 3, 1, 2, 1, NULL, 201, 1, 1, 197, '2016-03-23 13:25:40', NULL, NULL),
(8, 'P0001-0000001', 'P0001-0000005', 1, 1, 2, 1, NULL, 201, 2, 0, 197, '2016-03-23 13:29:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_modes`
--

DROP TABLE IF EXISTS `payment_modes`;
CREATE TABLE IF NOT EXISTS `payment_modes` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `paymentmodecode` int(3) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `sponsor_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) NOT NULL,
  `modifiedon` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_modes`
--

INSERT INTO `payment_modes` (`id`, `name`, `paymentmodecode`, `description`, `sponsor_id`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'Instant Cash', 301, 'Instant payment', 201, 1, 1, '2016-03-11 12:04:06', 0, '0000-00-00 00:00:00'),
(2, 'Prepaid Bill', 302, 'All payments are done before hand', 201, 1, 1, '2016-03-16 08:11:22', 0, '0000-00-00 00:00:00'),
(3, 'Postpaid Bill', 303, 'All payments are done at the end of all services', 201, 1, 1, '2016-03-16 08:12:08', 0, '0000-00-00 00:00:00'),
(4, 'Mpesa', 304, 'Payments are done through the M-pesa', 201, 1, 1, '2016-03-16 08:13:16', 0, '0000-00-00 00:00:00'),
(5, 'Package', 305, 'Pre defined payments for some of the services for a predefined fixed amount', 201, 1, 1, '2016-03-16 08:16:25', 0, '0000-00-00 00:00:00'),
(6, 'Credit Bill', 306, '', 202, 1, 1, '2016-03-18 10:05:53', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `payment_status`
--

DROP TABLE IF EXISTS `payment_status`;
CREATE TABLE IF NOT EXISTS `payment_status` (
`id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_status`
--

INSERT INTO `payment_status` (`id`, `name`) VALUES
(1, 'Completed'),
(2, 'Pending Debt'),
(3, 'Cancelled');

-- --------------------------------------------------------

--
-- Table structure for table `postpaid_bills`
--

DROP TABLE IF EXISTS `postpaid_bills`;
CREATE TABLE IF NOT EXISTS `postpaid_bills` (
`id` int(11) NOT NULL,
  `patientid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `patientvisitid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `departmentid` int(11) NOT NULL,
  `subdepartmentid` int(11) NOT NULL,
  `service_item_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `reftransactionid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `postpaid_bills`
--

INSERT INTO `postpaid_bills` (`id`, `patientid`, `patientvisitid`, `departmentid`, `subdepartmentid`, `service_item_id`, `amount`, `reftransactionid`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'P0001-0000001', 'P0001-0000005', 2, 4, 10, 20000, NULL, 0, 198, '2016-03-24 10:49:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `printers`
--

DROP TABLE IF EXISTS `printers`;
CREATE TABLE IF NOT EXISTS `printers` (
`id` int(11) NOT NULL,
  `model` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `printers`
--

INSERT INTO `printers` (`id`, `model`) VALUES
(1, 'Normal'),
(2, 'Dot Matrix');

-- --------------------------------------------------------

--
-- Table structure for table `service_charges`
--

DROP TABLE IF EXISTS `service_charges`;
CREATE TABLE IF NOT EXISTS `service_charges` (
`id` int(11) NOT NULL,
  `service_code` int(3) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `service_charges`
--

INSERT INTO `service_charges` (`id`, `service_code`, `name`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 101, 'Registration', 'Registration Charges', 1, 1, '2016-03-11 08:49:42', 1, '2016-03-11 11:50:47'),
(2, 102, 'Cosultation', 'Consultation Charges', 1, 1, '2016-03-11 09:01:30', NULL, NULL),
(3, 103, 'Diagnosis', 'Diagnosis charges', 1, 1, '2016-03-14 12:33:33', NULL, NULL),
(4, 104, 'Laboratory', 'Laboratory charges', 1, 1, '2016-03-14 12:33:54', NULL, NULL),
(5, 105, 'Pharmaceutical', 'Pharmaceutical charges', 1, 1, '2016-03-14 12:34:16', NULL, NULL),
(6, 106, 'Surgical', 'Surgical Charges', 1, 1, '2016-03-14 12:34:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_costs`
--

DROP TABLE IF EXISTS `service_costs`;
CREATE TABLE IF NOT EXISTS `service_costs` (
`id` int(11) NOT NULL,
  `departmentid` int(11) NOT NULL,
  `subdepartmentid` int(11) NOT NULL,
  `serviceid` int(11) NOT NULL,
  `sponsorid` int(11) NOT NULL,
  `cost` double NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `service_costs`
--

INSERT INTO `service_costs` (`id`, `departmentid`, `subdepartmentid`, `serviceid`, `sponsorid`, `cost`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 1, 1, 1, 201, 500, 1, 196, '2016-03-14 12:15:02', 196, '2016-03-14 15:16:33'),
(2, 1, 2, 8, 202, 10000, 1, 196, '2016-03-18 10:37:14', NULL, NULL),
(3, 1, 2, 8, 201, 10000, 1, 196, '2016-03-18 10:37:14', NULL, NULL),
(4, 1, 2, 9, 202, 5000, 1, 196, '2016-03-18 10:37:23', NULL, NULL),
(5, 1, 2, 9, 201, 5000, 1, 196, '2016-03-18 10:37:23', NULL, NULL),
(6, 2, 4, 10, 202, 20000, 1, 196, '2016-03-22 06:53:52', NULL, NULL),
(7, 2, 4, 10, 201, 20000, 1, 196, '2016-03-22 06:53:52', NULL, NULL),
(8, 2, 3, 2, 202, 30000, 1, 196, '2016-03-23 07:27:27', 196, '2016-03-23 10:27:33'),
(9, 2, 3, 2, 201, 30000, 1, 196, '2016-03-23 07:27:27', 196, '2016-03-23 10:27:33'),
(10, 3, 6, 1, 202, 50, 1, 196, '2016-04-20 12:10:00', NULL, NULL),
(11, 3, 6, 1, 201, 50, 1, 196, '2016-04-20 12:10:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_orders`
--

DROP TABLE IF EXISTS `service_orders`;
CREATE TABLE IF NOT EXISTS `service_orders` (
`id` int(11) NOT NULL,
  `patientid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `patientvisitid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `departmentid` int(11) NOT NULL,
  `subdepartmentid` int(11) NOT NULL,
  `serviceid` int(11) NOT NULL,
  `transactionid` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `service_orders`
--

INSERT INTO `service_orders` (`id`, `patientid`, `patientvisitid`, `departmentid`, `subdepartmentid`, `serviceid`, `transactionid`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(12, 'P0001-0000001', 'P0001-0000003', 2, 4, 10, '0000004', 1, 197, '2016-03-23 07:18:10', 197, '2016-03-23 10:18:14'),
(13, 'W0001-0000001', 'W0001-0000001', 2, 4, 10, '0000005', 1, 197, '2016-03-23 07:20:40', 197, '2016-03-23 10:20:46'),
(14, 'W0001-0000001', 'W0001-0000001', 2, 4, 10, '0000006', 1, 197, '2016-03-23 07:21:19', 197, '2016-03-23 10:21:26'),
(15, 'W0001-0000001', 'W0001-0000001', 2, 3, 2, '0000007', 1, 197, '2016-03-23 07:28:20', 197, '2016-03-23 10:28:35'),
(9, 'P0001-0000001', 'P0001-0000003', 2, 4, 10, '0000003', 1, 197, '2016-03-22 11:15:07', 197, '2016-03-23 10:17:09'),
(16, 'W0001-0000001', 'W0001-0000001', 2, 4, 10, '0000007', 1, 197, '2016-03-23 07:28:20', 197, '2016-03-23 10:28:35'),
(17, 'P0001-0000001', 'P0001-0000003', 2, 4, 10, '0000008', 1, 197, '2016-03-23 09:47:34', 197, '2016-03-23 14:48:06'),
(18, 'W0001-0000001', 'W0001-0000001', 2, 3, 2, '0000009', 1, 197, '2016-03-23 13:20:17', 197, '2016-03-23 16:20:42'),
(21, 'P0001-0000001', 'P0001-0000005', 2, 3, 2, NULL, 1, 198, '2016-03-24 09:01:44', 198, '2016-03-24 13:47:19'),
(22, 'P0001-0000001', 'P0001-0000005', 2, 4, 10, NULL, 1, 198, '2016-03-24 10:48:58', 198, '2016-03-24 13:49:12'),
(27, 'P0001-0000001', 'P0001-0000005', 2, 4, 10, '0000012', 1, 198, '2016-04-19 12:47:13', 198, '2016-04-19 15:47:21');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
`id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `departmentid` int(11) NOT NULL,
  `subdepartmentid` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `departmentid`, `subdepartmentid`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'General First Visit', 1, 1, 1, 196, '2016-03-14 08:45:29', 196, '2016-04-27 09:40:20'),
(2, 'SKULL', 2, 3, 1, 196, '2016-03-14 12:49:30', 196, '2016-03-14 15:50:21'),
(3, 'WRIST', 2, 3, 1, 196, '2016-03-14 12:49:30', 196, '2016-03-14 15:50:21'),
(4, 'SACRO-ILIAC JOINTS PA AND LIQUE', 2, 3, 1, 196, '2016-03-14 12:49:30', 196, '2016-03-14 15:50:21'),
(5, 'SHOULDER', 2, 3, 1, 196, '2016-03-14 12:49:30', 196, '2016-03-14 15:50:21'),
(6, 'Cardiology Second Visit', 1, 5, 1, 196, '2016-03-17 11:52:35', 196, '2016-03-17 14:56:12'),
(7, 'Cardiology First Visit', 1, 5, 1, 196, '2016-03-17 11:55:48', NULL, NULL),
(8, 'Specialist First Visit', 1, 2, 1, 196, '2016-03-18 09:58:23', NULL, NULL),
(9, 'Specialist Re Visit', 1, 2, 1, 196, '2016-03-18 09:58:42', NULL, NULL),
(10, 'Ultra Sound', 2, 4, 1, 196, '2016-03-22 06:50:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

DROP TABLE IF EXISTS `sponsors`;
CREATE TABLE IF NOT EXISTS `sponsors` (
`id` int(11) NOT NULL,
  `sponsorcode` int(3) NOT NULL,
  `shortname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) NOT NULL,
  `modifiedon` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sponsors`
--

INSERT INTO `sponsors` (`id`, `sponsorcode`, `shortname`, `fullname`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 201, 'Private', 'Private', 'Private sponsor', 1, 1, '2016-03-11 11:03:56', 0, '0000-00-00 00:00:00'),
(2, 202, 'NHIF', 'National Health Insurance Fund', '', 1, 1, '2016-03-18 10:03:39', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sub_departments`
--

DROP TABLE IF EXISTS `sub_departments`;
CREATE TABLE IF NOT EXISTS `sub_departments` (
`id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `departmentid` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_departments`
--

INSERT INTO `sub_departments` (`id`, `name`, `code`, `departmentid`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'General', '001', 1, 1, 196, '2016-03-12 09:03:44', NULL, NULL),
(2, 'Specialist', '002', 1, 1, 196, '2016-03-12 09:03:59', NULL, NULL),
(3, 'X-Ray', '003', 2, 1, 196, '2016-03-14 12:45:33', NULL, NULL),
(4, 'Ultra Sound', '004', 2, 1, 196, '2016-03-14 12:45:42', NULL, NULL),
(5, 'Cardiology', '005', 1, 1, 196, '2016-03-17 11:42:57', NULL, NULL),
(6, 'Pharmacy', '006', 3, 1, 196, '2016-04-20 10:00:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

DROP TABLE IF EXISTS `titles`;
CREATE TABLE IF NOT EXISTS `titles` (
`id` int(11) NOT NULL,
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `shortname` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `titles`
--

INSERT INTO `titles` (`id`, `fullname`, `shortname`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'Mr', 'Mr', 1, 197, '2016-03-16 11:45:54', NULL, NULL),
(2, 'Professor', 'Prof', 1, 197, '2016-03-16 11:46:03', NULL, NULL),
(3, 'Engineer', 'Eng', 1, 197, '2016-03-16 11:46:10', 197, '2016-03-16 15:50:25'),
(4, 'Mrs', 'Mrs', 1, 197, '2016-03-16 11:53:18', NULL, NULL),
(5, 'Honerable', 'Hon', 1, 197, '2016-03-16 11:53:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) unsigned NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `msisdn` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `ip_address` int(10) unsigned NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `login_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=logged in,0=logged out',
  `active` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedon` datetime NOT NULL,
  `modifiedby` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=204 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `username`, `email`, `msisdn`, `password`, `ip_address`, `salt`, `activation_code`, `forgotten_password_code`, `remember_code`, `last_login`, `login_status`, `active`, `createdon`, `createdby`, `modifiedon`, `modifiedby`) VALUES
(1, 'Emmanuel', NULL, 'Mfikwa', 'manuel', 'kemixenock@gmail.com', '0719186759', 'c63134a5227f984d2790fb0a6ceda4bf06b65871', 0, '0', NULL, NULL, NULL, 1460356875, 0, 1, '0000-00-00 00:00:00', 0, '2016-03-11 12:33:38', 1),
(196, 'Administrator', NULL, 'Administrator', 'administrator', 'administrator@gmobile.co.tz', '+255719186759', '8231f19cd721c2c68902cab55c74d2d4d9651306', 0, '0', NULL, NULL, NULL, 1461737601, 1, 1, '2016-03-10 17:03:00', 1, '2016-03-11 16:55:00', 196),
(197, 'Reception', NULL, 'Reception', 'receptionist', NULL, '+255719186759', '06a09bce8d28926f029e91a8e65bbaed65e3bca9', 0, '0', NULL, NULL, NULL, 1461760801, 1, 1, '2016-03-11 16:38:10', 196, '2016-03-16 12:45:53', 197),
(198, 'Reception', NULL, 'Reception', 'reception', NULL, '+255719186759', 'd8592bf51a7e78343bac7d36758de34c279ec15f', 0, '0', NULL, NULL, NULL, 1461243759, 0, 1, '2016-03-16 10:56:16', 196, '2016-03-16 12:42:15', 198),
(199, 'Doctor', NULL, 'Mfikwa', 'mfikwa', NULL, '255719186759', '71d2c9e2f82bfec613aa177d2f91079f0cdd6802', 0, '0', NULL, NULL, NULL, NULL, 0, 1, '2016-03-18 11:32:42', 196, '2016-03-18 11:33:55', 196),
(200, 'Inventory', NULL, 'Administrator', 'Inventory', NULL, '+255719186759', '9c4f3738e1f34a435d4dac021650599051344f14', 0, '0', NULL, NULL, NULL, 1461617957, 0, 1, '2016-04-04 13:52:49', 196, '0000-00-00 00:00:00', 0),
(203, 'Emmanuel', NULL, 'PharmacyB', 'pharmacist2', NULL, '0719186759', '6283a3b1718c102318fcc7527927aab6ba71cc36', 0, '0', NULL, NULL, NULL, 1461617972, 0, 1, '2016-04-22 16:46:00', 196, '2016-04-22 16:46:40', 196),
(202, 'Emmanuel', 'Enock', 'Pharmacy', 'pharmacist', 'emmanuel.mfikwa@gmobile.co.tz', '+255719186759', '89cc696f0351fdaad7a22eaff20ee6d3aab4877e', 0, '0', NULL, NULL, NULL, 1461619003, 0, 1, '2016-04-21 10:24:33', 196, '2016-04-21 11:08:25', 202);

-- --------------------------------------------------------

--
-- Table structure for table `users_doctors`
--

DROP TABLE IF EXISTS `users_doctors`;
CREATE TABLE IF NOT EXISTS `users_doctors` (
`id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `doctorcategory` int(11) NOT NULL,
  `availability` int(1) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `lastupdate` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_doctors`
--

INSERT INTO `users_doctors` (`id`, `userid`, `doctorcategory`, `availability`, `status`, `lastupdate`) VALUES
(1, 199, 2, 1, 1, '2016-03-18 11:33:55');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
`id` mediumint(8) unsigned NOT NULL,
  `user_id` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=205 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(199, 198, 7),
(198, 197, 7),
(197, 196, 2),
(196, 195, 2),
(195, 194, 2),
(200, 199, 4),
(201, 200, 11),
(202, 201, 5),
(203, 202, 5),
(204, 203, 5);

-- --------------------------------------------------------

--
-- Table structure for table `visit_categories`
--

DROP TABLE IF EXISTS `visit_categories`;
CREATE TABLE IF NOT EXISTS `visit_categories` (
`id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `visit_categories`
--

INSERT INTO `visit_categories` (`id`, `name`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'OPD', 'OPD Visit', 1, 197, '2016-03-18 06:49:32', NULL, NULL),
(2, 'PF3', 'PF3', 1, 197, '2016-03-18 06:49:45', NULL, NULL),
(3, 'Maternity', 'Maternal Visit', 1, 197, '2016-03-18 06:50:06', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bill_transaction_ids`
--
ALTER TABLE `bill_transaction_ids`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disposed_inventory`
--
ALTER TABLE `disposed_inventory`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drug_dosages`
--
ALTER TABLE `drug_dosages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drug_frequencies`
--
ALTER TABLE `drug_frequencies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drug_groups`
--
ALTER TABLE `drug_groups`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drug_types`
--
ALTER TABLE `drug_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institution`
--
ALTER TABLE `institution`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institution_categories`
--
ALTER TABLE `institution_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institution_config`
--
ALTER TABLE `institution_config`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_dispose_reasons`
--
ALTER TABLE `inventory_dispose_reasons`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `inventory_item_categories`
--
ALTER TABLE `inventory_item_categories`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `inventory_items`
--
ALTER TABLE `inventory_items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_requesition_orders`
--
ALTER TABLE `inventory_requesition_orders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_stock`
--
ALTER TABLE `inventory_stock`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_suppliers`
--
ALTER TABLE `inventory_suppliers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_unit_categories`
--
ALTER TABLE `inventory_unit_categories`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `inventory_unit_users`
--
ALTER TABLE `inventory_unit_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_units`
--
ALTER TABLE `inventory_units`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marital_status`
--
ALTER TABLE `marital_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_items_services`
--
ALTER TABLE `package_items_services`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `packageid` (`packageid`);

--
-- Indexes for table `patient_police_details`
--
ALTER TABLE `patient_police_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patientids_control`
--
ALTER TABLE `patientids_control`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `patientid` (`patientid`);

--
-- Indexes for table `patients_bill`
--
ALTER TABLE `patients_bill`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients_credit_bills`
--
ALTER TABLE `patients_credit_bills`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients_dispense_orders`
--
ALTER TABLE `patients_dispense_orders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients_prepaid_accounts`
--
ALTER TABLE `patients_prepaid_accounts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients_que_control`
--
ALTER TABLE `patients_que_control`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients_visit`
--
ALTER TABLE `patients_visit`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `patientvisitid` (`patientvisitid`);

--
-- Indexes for table `payment_modes`
--
ALTER TABLE `payment_modes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_status`
--
ALTER TABLE `payment_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `postpaid_bills`
--
ALTER TABLE `postpaid_bills`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `printers`
--
ALTER TABLE `printers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_charges`
--
ALTER TABLE `service_charges`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `service_code` (`service_code`);

--
-- Indexes for table `service_costs`
--
ALTER TABLE `service_costs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_orders`
--
ALTER TABLE `service_orders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsors`
--
ALTER TABLE `sponsors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_departments`
--
ALTER TABLE `sub_departments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_doctors`
--
ALTER TABLE `users_doctors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit_categories`
--
ALTER TABLE `visit_categories`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bill_transaction_ids`
--
ALTER TABLE `bill_transaction_ids`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `disposed_inventory`
--
ALTER TABLE `disposed_inventory`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `drug_dosages`
--
ALTER TABLE `drug_dosages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `drug_frequencies`
--
ALTER TABLE `drug_frequencies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `drug_groups`
--
ALTER TABLE `drug_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `drug_types`
--
ALTER TABLE `drug_types`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `institution`
--
ALTER TABLE `institution`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `institution_categories`
--
ALTER TABLE `institution_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `institution_config`
--
ALTER TABLE `institution_config`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `inventory_dispose_reasons`
--
ALTER TABLE `inventory_dispose_reasons`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `inventory_item_categories`
--
ALTER TABLE `inventory_item_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `inventory_items`
--
ALTER TABLE `inventory_items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `inventory_requesition_orders`
--
ALTER TABLE `inventory_requesition_orders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `inventory_stock`
--
ALTER TABLE `inventory_stock`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `inventory_suppliers`
--
ALTER TABLE `inventory_suppliers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `inventory_unit_categories`
--
ALTER TABLE `inventory_unit_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `inventory_unit_users`
--
ALTER TABLE `inventory_unit_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `inventory_units`
--
ALTER TABLE `inventory_units`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=181;
--
-- AUTO_INCREMENT for table `marital_status`
--
ALTER TABLE `marital_status`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `package_items_services`
--
ALTER TABLE `package_items_services`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `patient_police_details`
--
ALTER TABLE `patient_police_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `patientids_control`
--
ALTER TABLE `patientids_control`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `patients_bill`
--
ALTER TABLE `patients_bill`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `patients_credit_bills`
--
ALTER TABLE `patients_credit_bills`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `patients_dispense_orders`
--
ALTER TABLE `patients_dispense_orders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `patients_prepaid_accounts`
--
ALTER TABLE `patients_prepaid_accounts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `patients_que_control`
--
ALTER TABLE `patients_que_control`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `patients_visit`
--
ALTER TABLE `patients_visit`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `payment_modes`
--
ALTER TABLE `payment_modes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `payment_status`
--
ALTER TABLE `payment_status`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `postpaid_bills`
--
ALTER TABLE `postpaid_bills`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `printers`
--
ALTER TABLE `printers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `service_charges`
--
ALTER TABLE `service_charges`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `service_costs`
--
ALTER TABLE `service_costs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `service_orders`
--
ALTER TABLE `service_orders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sponsors`
--
ALTER TABLE `sponsors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sub_departments`
--
ALTER TABLE `sub_departments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=204;
--
-- AUTO_INCREMENT for table `users_doctors`
--
ALTER TABLE `users_doctors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=205;
--
-- AUTO_INCREMENT for table `visit_categories`
--
ALTER TABLE `visit_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
